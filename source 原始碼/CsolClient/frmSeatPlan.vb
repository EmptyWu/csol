﻿Public Class frmSeatPlan
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private dtSchoolType As New DataTable
    Private dtClassRoomInfo As New DataTable
    Private dtSeatNaList As New DataTable
    Private dtSeatTakenList As New DataTable
    Private dsSeatRegInfo As New DataSet
    Private dtClassSeatMap As New DataTable
    Private dtSchool As New DataTable
    Private lstSch As New ArrayList
    Private lstClass As New ArrayList
    Private lstSc As New ArrayList

    Private intRowCnt As Integer = 0
    Private intColCnt As Integer = 0
    Private intPwCnt As Integer = 0
    Private intState As Integer = -1
    Private intDisStyle As Integer = -1

    Private lstColumn As New ArrayList
    Private lstRow As New ArrayList
    Private lstPw As New ArrayList
    Private lstConvex As New ArrayList
    Private lstNaCol As New ArrayList
    Private lstNaRow As New ArrayList
    Private lstTk As New ArrayList
    Private lstName As New ArrayList

    Private lstAbc() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ".Split(",")
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmSeatPlan
    Private sUserName As String = frmMain.GetUsrName

    Private pages As String = ""                    '100316
    Private flag1 As Integer = 0                    '100316

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSeatPlan
        tboxSeatNa.Text = c_SeatNaText
        RefreshSchLst()
        RefreshClassLst()

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvSeat.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        'If nPageNo = 1 Then

        For Each oColumn As DataGridViewColumn In dgvSeat.Columns
            If oColumn.Visible = True Then
                nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 20         '100225

                oColumnLefts.Add(nLeft)
                oColumnWidths.Add(nWidth)
                oColumnTypes.Add(oColumn.GetType)
                nLeft += nWidth
            End If
        Next

        'End If

        Do While nRowPos < dgvSeat.Rows.Count

            Dim oRow As DataGridViewRow = dgvSeat.Rows(nRowPos)
            Dim HearderClass As String
            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    '20100407 sherry issue 105
                    HearderClass = Header & " - " & cboxClass.Text.Trim
                    e.Graphics.DrawString(HearderClass, New Font(dgvSeat.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(HearderClass, New Font(dgvSeat.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    'Header = Header & " - " & cboxClass.Text.Trim
                    'e.Graphics.DrawString(Header, New Font(dgvSeat.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvSeat.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvSeat.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                '20100407 sherry issue 105
                e.Graphics.DrawString(oRow.HeaderCell.Value.ToString, oRow.HeaderCell.InheritedStyle.Font, New SolidBrush(oRow.HeaderCell.InheritedStyle.ForeColor), oColumnLefts(i) - 40, nTop + 16, oStringFormat)
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            If oCell.Value Is Nothing Then
                                e.Graphics.DrawString("", oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            Else
                                If Not oCell.Value.ToString.Substring(1, 1) = "0" And Not oCell.Value.ToString.Substring(1, 1) = "1" And Not oCell.Value.ToString.Substring(1, 1) = "2" And Not oCell.Value.ToString.Substring(1, 1) = "3" Then         '100316
                                    e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                                End If                                                                                                                                                                                                                  '100316
                            End If

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            If flag1 = 0 Then                                                                           '100316
                sPageNo = ""
                sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvSeat.Rows.Count / RowsPerPage).ToString
                pages = Math.Ceiling(dgvSeat.Rows.Count / RowsPerPage).ToString
                flag1 = 1
            Else
                sPageNo = ""
                sPageNo = nPageNo.ToString + " of " + pages
            End If                                                                                      '100316

        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvSeat.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvSeat.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub RefreshSchLst()
        dtSchoolType = frmMain.GetSchTypes
        dtSchool = frmMain.GetSchList
        cboxSchool.Items.Clear()
        For index As Integer = 0 To dtSchoolType.Rows.Count - 1
            cboxSchool.Items.Add(dtSchoolType.Rows(index).Item(c_NameColumnName))
            lstSch.Add(dtSchoolType.Rows(index).Item(c_IDColumnName))
        Next
        If dtSchoolType.Rows.Count > 0 Then
            cboxSchool.SelectedIndex = 0
        End If
    End Sub

    Private Sub RefreshClassLst()
        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        cboxClass.Items.Clear()
        cboxSubClass.Items.Clear()
        lstClass = New ArrayList
        lstSc = New ArrayList

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        If lstClass.Count > 0 Then
            cboxClass.SelectedIndex = -1
            cboxClass.Text = ""
            cboxSubClass.Text = ""
        End If

    End Sub

    Private Sub frmSeatPlan_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSeatPlan_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim strCompute1 As String = "COUNT(" + c_ColumnNumColumnName + ")"
        Dim strCompute2 As String = c_StateColumnName & "=0"
        If cboxSubClass.SelectedIndex > -1 Then
            Try
                dsSeatRegInfo = objCsol.GetSeatRegInfo(lstSc(cboxSubClass.SelectedIndex))
                dtClassRoomInfo = dsSeatRegInfo.Tables(c_ClassRoomInfoTableName)
                dtSeatNaList = dsSeatRegInfo.Tables(c_SeatNaListTableName)
                dtSeatTakenList = dsSeatRegInfo.Tables(c_SeatTakenListTableName)
                dtClassSeatMap = objCsol.GetSeatMap(lstSc(cboxSubClass.SelectedIndex))

                If dtClassRoomInfo.Rows.Count > 0 Then
                    intRowCnt = dtClassRoomInfo.Rows(0).Item(c_LineCntColumnName)
                    intColCnt = dtClassRoomInfo.Rows(0).Item(c_ColumnCntColumnName)
                    intDisStyle = dtClassRoomInfo.Rows(0).Item(c_DisStyleColumnName)
                    intPwCnt = dtSeatNaList.Compute(strCompute1, strCompute2)
                    lstPw = New ArrayList
                    lstConvex = New ArrayList
                    lstNaCol = New ArrayList
                    lstNaRow = New ArrayList
                    lstTk = New ArrayList
                    lstName = New ArrayList

                    Dim intS = 0
                    For index As Integer = 0 To dtSeatNaList.Rows.Count - 1
                        intS = dtSeatNaList.Rows(index).Item(c_StateColumnName)
                        If intS = 0 Then
                            lstPw.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                        ElseIf intS = 2 Then
                            lstConvex.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                        ElseIf intS = 1 Then
                            lstNaCol.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                            lstNaRow.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                        End If
                    Next

                    For index As Integer = 0 To dtSeatTakenList.Rows.Count - 1
                        lstTk.Add(dtSeatTakenList.Rows(index).Item(c_SeatNumColumnName).ToString.Trim)
                        lstName.Add(dtSeatTakenList.Rows(index).Item(c_StuNameColumnName))
                    Next

                    If intRowCnt > 0 And intColCnt + intPwCnt > 0 Then
                        InitTable()
                        RefreshTable()
                    End If
                End If

            Catch ex As ApplicationException
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub InitTable()
        dgvSeat.Columns.Clear()
        dgvSeat.Rows.Clear()
        dgvSeat.ColumnCount = intColCnt + intPwCnt
        dgvSeat.RowCount = intRowCnt

        FillArrayList(1, intColCnt + intPwCnt, lstColumn)
        FillArrayList(1, intRowCnt, lstRow)

        Dim i As Integer = 0

        Select Case intDisStyle
            Case 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        lstColumn(index) = lstAbc(i).ToString
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next

            Case 2
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i + 1 < 10 Then
                            lstColumn(index) = "0" & (i + 1).ToString
                        Else
                            lstColumn(index) = (i + 1).ToString
                        End If
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    lstRow(index) = lstAbc(index).ToString
                Next

            Case 3
                i = lstAbc.Count - 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i > 0 Then
                            lstColumn(index) = lstAbc(i).ToString
                            i = i - 1
                        End If
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next
        End Select

        For index As Integer = 0 To dgvSeat.ColumnCount - 1
            If index < lstColumn.Count Then
                dgvSeat.Columns(index).HeaderText = lstColumn(index)
                If lstColumn(index) = "" Then
                    dgvSeat.Columns(index).DefaultCellStyle.BackColor = Color.Silver
                End If
                dgvSeat.Columns(index).Width = 40
                dgvSeat.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            End If
        Next

        For index As Integer = 0 To dgvSeat.RowCount - 1
            If index < lstRow.Count Then
                dgvSeat.Rows(index).HeaderCell.Value = lstRow(index)
            End If
            If lstConvex.IndexOf(index) > -1 Then
                dgvSeat.Rows(index).HeaderCell.Style.BackColor = Color.Aqua
            End If
        Next

        If lstNaCol.Count > 0 Then
            For index As Integer = 0 To lstNaCol.Count - 1
                dgvSeat.Rows(lstNaRow(index)).Cells(lstNaCol(index)).value = c_SeatNaText
            Next
        End If

    End Sub

    Private Sub RefreshTable()
        Dim strSeat As String = ""
        Dim idx As Integer = -1

        For index As Integer = 0 To dgvSeat.Columns.Count - 1
            For j As Integer = 0 To dgvSeat.Rows.Count - 1
                Dim s As String = ""
                If Not dgvSeat.Columns(index).HeaderText = "" And _
                    Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
                    Select Case intDisStyle
                        Case 1, 3
                            strSeat = dgvSeat.Columns(index).HeaderText & _
                                    dgvSeat.Rows(j).HeaderCell.Value
                        Case 2
                            strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
                                    dgvSeat.Columns(index).HeaderText
                    End Select
                    idx = lstTk.IndexOf(strSeat)
                    If idx > -1 Then 'taken
                        dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxTk.BackColor
                        If chkboxName.Checked = True Then
                            s = lstName(idx) + vbNewLine
                        End If
                        If chkboxID.Checked = True Then
                            s = s & GetID(strSeat) + vbNewLine
                        End If
                        If chkboxSeatNum.Checked = True Then
                            s = s & strSeat + vbNewLine
                        End If
                        If chkboxTel.Checked = True Then
                            s = s & GetTel(strSeat) + vbNewLine
                        End If
                        If chkboxSchool.Checked = True Then
                            s = s & GetSch(strSeat) + vbNewLine
                        End If
                        dgvSeat.Rows(j).Cells(index).Value = s
                    Else 'available
                        dgvSeat.Rows(j).Cells(index).Value = strSeat
                    End If
                End If
            Next
        Next

    End Sub

    Private Function GetID(ByVal s As String) As String
        Dim r As String = ""
        If dtClassSeatMap.Rows.Count > 0 Then
            For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                    r = dtClassSeatMap.Rows(index).Item(c_StuIDColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Function GetTel(ByVal s As String) As String
        Dim r As String = ""
        If dtClassSeatMap.Rows.Count > 0 Then
            For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                    r = dtClassSeatMap.Rows(index).Item(c_Tel1ColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Function GetSch(ByVal s As String) As String
        Dim r As String = ""
        If dtClassSeatMap.Rows.Count > 0 And cboxSchool.SelectedIndex > -1 Then
            Select Case lstSch(cboxSchool.SelectedIndex)
                Case 1
                    For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                        If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                            r = GetSchoolName(dtClassSeatMap.Rows(index).Item(c_PrimarySchColumnName))
                            Exit For
                        End If
                    Next
                Case 2
                    For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                        If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                            r = GetSchoolName(dtClassSeatMap.Rows(index).Item(c_JuniorSchColumnName))
                            Exit For
                        End If
                    Next
                Case 3
                    For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                        If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                            r = GetSchoolName(dtClassSeatMap.Rows(index).Item(c_HighSchColumnName))
                            Exit For
                        End If
                    Next
                Case 4
                    For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                        If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s.Trim Then
                            r = GetSchoolName(dtClassSeatMap.Rows(index).Item(c_UniversityColumnName))
                            Exit For
                        End If
                    Next
            End Select

        End If
        Return r
    End Function

    Private Function GetSchoolName(ByVal i As Integer) As String
        Dim r As String = ""
        If dtSchool.Rows.Count > 0 Then
            For index As Integer = 0 To dtSchool.Rows.Count - 1
                If dtSchool.Rows(index).Item(c_IDColumnName) = i Then
                    r = dtSchool.Rows(index).Item(c_NameColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Sub LoadColumnText()
        'To be added
    End Sub


    Private Sub InitSelections()

    End Sub

    Private Sub frmSeatPlan_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub chkboxID_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxID.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub chkboxName_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxName.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub chkboxSeatNum_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxSeatNum.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub chkboxSchool_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxSchool.CheckedChanged
        If cboxSchool.SelectedIndex > -1 Then
            RefreshTable()
        End If
    End Sub

    Private Sub cboxSchool_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSchool.SelectedIndexChanged
        If cboxSchool.SelectedIndex > -1 And chkboxSchool.Checked Then
            RefreshTable()
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassLst()
        RefreshData()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        If cboxClass.SelectedIndex > -1 Then
            lstSc = New ArrayList

            If lstClass.Count > 0 Then
                Dim c As Integer = lstClass(cboxClass.SelectedIndex)
                For index As Integer = 0 To dtSubClass.Rows.Count - 1
                    If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = c Then
                        cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                        lstSc.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxSubClass.Items.Count > 0 Then
                    cboxSubClass.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            RefreshData()
        End If
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(3) Then
            'cboxClass.SelectedIndex = -1
            cboxSubClass.SelectedIndex = -1
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(13) Then
            ExportDgvToExcel2(dgvSeat)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub chkboxTel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxTel.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub dgvSeat_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSeat.CellContentClick



    End Sub
End Class