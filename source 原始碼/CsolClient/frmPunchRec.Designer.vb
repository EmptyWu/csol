﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPunchRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPunchRec))
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.mnuModify = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkboxDate = New System.Windows.Forms.CheckBox
        Me.tboxItem = New System.Windows.Forms.TextBox
        Me.cboxItem = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.butFilter = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'mnuModify
        '
        Me.mnuModify.Name = "mnuModify"
        resources.ApplyResources(Me.mnuModify, "mnuModify")
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuDelete, Me.mnuModify, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.chkboxDate)
        Me.GroupBox1.Controls.Add(Me.tboxItem)
        Me.GroupBox1.Controls.Add(Me.cboxItem)
        Me.GroupBox1.Controls.Add(Me.dtpickFrom)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dtpickTo)
        Me.GroupBox1.Controls.Add(Me.butFilter)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'chkboxDate
        '
        resources.ApplyResources(Me.chkboxDate, "chkboxDate")
        Me.chkboxDate.Checked = True
        Me.chkboxDate.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxDate.Name = "chkboxDate"
        Me.chkboxDate.UseVisualStyleBackColor = True
        '
        'tboxItem
        '
        resources.ApplyResources(Me.tboxItem, "tboxItem")
        Me.tboxItem.Name = "tboxItem"
        '
        'cboxItem
        '
        Me.cboxItem.FormattingEnabled = True
        Me.cboxItem.Items.AddRange(New Object() {resources.GetString("cboxItem.Items"), resources.GetString("cboxItem.Items1")})
        resources.ApplyResources(Me.cboxItem, "cboxItem")
        Me.cboxItem.Name = "cboxItem"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'butFilter
        '
        resources.ApplyResources(Me.butFilter, "butFilter")
        Me.butFilter.Name = "butFilter"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'frmPunchRec
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgv)
        Me.Name = "frmPunchRec"
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuModify As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxDate As System.Windows.Forms.CheckBox
    Friend WithEvents tboxItem As System.Windows.Forms.TextBox
    Friend WithEvents cboxItem As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
