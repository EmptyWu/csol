﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAssignDoneMul
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label4 = New System.Windows.Forms.Label
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.lstboxClass = New System.Windows.Forms.ListBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.chkboxShowPastAssign = New System.Windows.Forms.CheckBox
        Me.chklstAssignment = New System.Windows.Forms.CheckedListBox
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.Blue
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(224, 34)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(293, 12)
        Me.Label4.TabIndex = 143
        Me.Label4.Text = "點一下最後一欄設定為有通過，再點一下設定為沒通過"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'PrintDocument1
        '
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(228, 51)
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(768, 542)
        Me.dgv.TabIndex = 146
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(68, 20)
        Me.mnuPrint.Text = "列印表格"
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出表格"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuExport, Me.mnuSelectCol, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 137
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        Me.mnuSelectCol.Size = New System.Drawing.Size(68, 20)
        Me.mnuSelectCol.Text = "選擇欄位"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.lstboxClass)
        Me.GroupBox7.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox7.Location = New System.Drawing.Point(2, 34)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(216, 186)
        Me.GroupBox7.TabIndex = 147
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "班級"
        '
        'lstboxClass
        '
        Me.lstboxClass.FormattingEnabled = True
        Me.lstboxClass.ItemHeight = 12
        Me.lstboxClass.Location = New System.Drawing.Point(12, 27)
        Me.lstboxClass.Name = "lstboxClass"
        Me.lstboxClass.Size = New System.Drawing.Size(197, 136)
        Me.lstboxClass.TabIndex = 18
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(125, 0)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPast.TabIndex = 16
        Me.chkboxShowPast.Text = "顯示過時"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chklstSubClass)
        Me.GroupBox1.Location = New System.Drawing.Point(8, 215)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(216, 163)
        Me.GroupBox1.TabIndex = 148
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "班別"
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        Me.chklstSubClass.Location = New System.Drawing.Point(8, 17)
        Me.chklstSubClass.Name = "chklstSubClass"
        Me.chklstSubClass.Size = New System.Drawing.Size(197, 140)
        Me.chklstSubClass.TabIndex = 17
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chkboxShowPastAssign)
        Me.GroupBox5.Controls.Add(Me.chklstAssignment)
        Me.GroupBox5.Location = New System.Drawing.Point(5, 384)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(217, 228)
        Me.GroupBox5.TabIndex = 149
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "作業"
        '
        'chkboxShowPastAssign
        '
        Me.chkboxShowPastAssign.AutoSize = True
        Me.chkboxShowPastAssign.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPastAssign.Location = New System.Drawing.Point(136, 0)
        Me.chkboxShowPastAssign.Name = "chkboxShowPastAssign"
        Me.chkboxShowPastAssign.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPastAssign.TabIndex = 18
        Me.chkboxShowPastAssign.Text = "顯示過時"
        Me.chkboxShowPastAssign.UseVisualStyleBackColor = True
        '
        'chklstAssignment
        '
        Me.chklstAssignment.CheckOnClick = True
        Me.chklstAssignment.FormattingEnabled = True
        Me.chklstAssignment.Location = New System.Drawing.Point(11, 17)
        Me.chklstAssignment.Name = "chklstAssignment"
        Me.chklstAssignment.Size = New System.Drawing.Size(197, 191)
        Me.chklstAssignment.TabIndex = 17
        '
        'frmAssignDoneMul
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmAssignDoneMul"
        Me.Text = "通過作業輸入(多筆)"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstAssignment As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkboxShowPastAssign As System.Windows.Forms.CheckBox
    Friend WithEvents lstboxClass As System.Windows.Forms.ListBox
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
End Class
