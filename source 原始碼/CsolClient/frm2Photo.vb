﻿Imports System.IO

Public Class frm2Photo
    Const WM_CAP As Short = &H400S
    Const WM_CAP_DRIVER_CONNECT As Integer = WM_CAP + 10
    Const WM_CAP_DRIVER_DISCONNECT As Integer = WM_CAP + 11
    Const WM_CAP_EDIT_COPY As Integer = WM_CAP + 30
    Const WM_CAP_SET_PREVIEW As Integer = WM_CAP + 50
    Const WM_CAP_SET_PREVIEWRATE As Integer = WM_CAP + 52
    Const WM_CAP_SET_SCALE As Integer = WM_CAP + 53
    Const WS_CHILD As Integer = &H40000000
    Const WS_VISIBLE As Integer = &H10000000
    Const SWP_NOMOVE As Short = &H2S
    Const SWP_NOSIZE As Short = 1
    Const SWP_NOZORDER As Short = &H4S
    Const HWND_BOTTOM As Short = 1

    Dim iDevice As Integer = 0  ' Normal device ID 
    Dim hHwnd As Integer  ' Handle value to preview window
    Dim strId As String = ""

    ' Declare function from AVI capture DLL.

    Declare Function SendMessage Lib "user32" Alias "SendMessageA" _
        (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Integer, _
         ByVal lParam As Object) As Integer

    Declare Function SetWindowPos Lib "user32" Alias "SetWindowPos" (ByVal hwnd As Integer, _
        ByVal hWndInsertAfter As Integer, ByVal x As Integer, ByVal y As Integer, _
        ByVal cx As Integer, ByVal cy As Integer, ByVal wFlags As Integer) As Integer

    Declare Function DestroyWindow Lib "user32" (ByVal hndw As Integer) As Boolean

    Declare Function capCreateCaptureWindowA Lib "avicap32.dll" _
        (ByVal lpszWindowName As String, ByVal dwStyle As Integer, _
        ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, _
        ByVal nHeight As Short, ByVal hWndParent As Integer, _
        ByVal nID As Integer) As Integer

    Declare Function capGetDriverDescriptionA Lib "avicap32.dll" (ByVal wDriver As Short, _
        ByVal lpszName As String, ByVal cbName As Integer, ByVal lpszVer As String, _
        ByVal cbVer As Integer) As Boolean

    ' Connect to the device.

    Private Sub LoadDeviceList()
        Dim strName As String = Space(100)
        Dim strVer As String = Space(100)
        Dim bReturn As Boolean
        Dim x As Integer = 0

        ' Load name of all avialable devices into the cboxDevices .

        Do
            '   Get Driver name and version
            bReturn = capGetDriverDescriptionA(x, strName, 100, strVer, 100)
            ' If there was a device add device name to the list 
            If bReturn Then cboxDevices.Items.Add(strName.Trim)
            x += 1
        Loop Until bReturn = False
    End Sub

    ' To display the output from a video capture device, you need to create a capture window.

    Private Sub OpenPreviewWindow()
        Dim iHeight As Integer = picbox.Height
        Dim iWidth As Integer = picbox.Width

        ' Open Preview window in picturebox .
        ' Create a child window with capCreateCaptureWindowA so you can display it in a picturebox.

        hHwnd = capCreateCaptureWindowA(iDevice, WS_VISIBLE Or WS_CHILD, 0, 0, 640, _
            480, picbox.Handle.ToInt32, 0)

        ' Connect to device
        If SendMessage(hHwnd, WM_CAP_DRIVER_CONNECT, iDevice, 0) Then

            ' Set the preview scale
            SendMessage(hHwnd, WM_CAP_SET_SCALE, True, 0)

            ' Set the preview rate in milliseconds
            SendMessage(hHwnd, WM_CAP_SET_PREVIEWRATE, 66, 0)

            ' Start previewing the image from the camera 
            SendMessage(hHwnd, WM_CAP_SET_PREVIEW, True, 0)

            ' Resize window to fit in picturebox 
            SetWindowPos(hHwnd, HWND_BOTTOM, 0, 0, picbox.Width, picbox.Height, _
                                   SWP_NOMOVE Or SWP_NOZORDER)
            butSave.Enabled = True
        Else
            ' Error connecting to device close window 
            DestroyWindow(hHwnd)
            butSave.Enabled = False
        End If
    End Sub

    ' Use SendMessage to copy the data to the clipboard Then transfer the image to the picture box. 

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If strId.Length < 8 Then
            Exit Sub
        End If

        Dim data As IDataObject
        Dim bmap As Image
        ' Copy image to clipboard 
        SendMessage(hHwnd, WM_CAP_EDIT_COPY, 0, 0)

        ' Get image from clipboard and convert it to a bitmap 
        data = Clipboard.GetDataObject()
        If data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
            bmap = CType(data.GetData(GetType(System.Drawing.Bitmap)), Image)

            picbox.Image = bmap
            Dim bm_source As New Bitmap(picbox.Image)

            ' Make a bitmap for the result.
            Dim bm_dest As New Bitmap( _
                CInt(bm_source.Width * 0.25), _
                CInt(bm_source.Height * 0.25))

            ' Make a Graphics object for the result Bitmap.
            Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

            ' Copy the source image into the destination bitmap.
            gr_dest.DrawImage(bm_source, 0, 0, _
                bm_dest.Width + 1, _
                bm_dest.Height + 1)

            Dim f As String = GetPhotoPath() & strId & ".bmp"
            If CheckFileExist(f) Then
                Try
                    My.Computer.FileSystem.DeleteFile(f)

                Catch ex As Exception
                    AddErrorLog(ex.ToString)
                End Try
            End If
            bm_dest.Save(f, Imaging.ImageFormat.Bmp)

            ClosePreviewWindow()
            '20100424 imgdb by sherry start


            Dim fs As FileStream = Nothing
            Try
                '#Region "Reading file"
                fs = New FileStream(f, FileMode.Open)
                '
                ' Finding out the size of the file to be uploaded
                '
                Dim fi As FileInfo = New FileInfo(f)
                Dim temp As Long = fi.Length
                Dim lung As Integer = Convert.ToInt32(temp)
                ' ------------------------------------------
                '
                ' Reading the content of the file into an array of bytes.
                '
                Dim picture As Byte() = New Byte(lung - 1) {}
                fs.Read(picture, 0, lung)
                fs.Close()
                objCsol.UploadImg(strId, picture)

            Catch ec As Exception
                AddErrorLog(ec.ToString)
            End Try
            '20100424 imgdb by sherry end
            frmMain.SetOkState(True)
            Me.Close()
        End If
    End Sub

    ' Finally, to close the preview window, disconnect from the device and destroy the preview window. 

    Private Sub ClosePreviewWindow()
        ' Disconnect from device
        SendMessage(hHwnd, WM_CAP_DRIVER_DISCONNECT, iDevice, 0)

        ' close window 
        DestroyWindow(hHwnd)
    End Sub

    Public Sub New(ByVal id As String)

        InitializeComponent()

        strId = id
    End Sub

    Private Sub frm2Photo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        LoadDeviceList()
        If cboxDevices.Items.Count > 0 Then
            cboxDevices.SelectedIndex = 0
        End If
        frmMain.SetOkState(False)
    End Sub

    Private Sub cboxDevices_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxDevices.SelectedIndexChanged
        If cboxDevices.SelectedIndex > -1 Then
            OpenPreviewWindow()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class