﻿Public Class frmStuOweSta
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dtPaySta As New DataTable
    Private dtDisc As New DataTable
    Private dtStu As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dtDetails As New DataTable
    Private dateNow As Date = Now
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private lstClassFee As New ArrayList
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuOweSta
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmStuOweSta
        InitClassList()
        ShowClassList()
        'RefreshData()
        InitColSelections()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(9) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuOweSta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuOweSta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intSubClass As Integer
        Dim intClass As Integer
        Dim strStuId As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim intTotalAll As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"

        Try

            '------------------------------------------------------------
            'dtPaySta = objCsol.ListStuOweByClass(lstClass)
            'dtDisc = objCsol.ListStuDiscByClass(lstClass)
            'dtStu = objCsol.ListStuBySubClass(lstSubClass)

            dtPaySta = ListStuOweByClass(lstClass)
            dtDisc = ListStuDiscByClass(lstClass)
            dtStu = ListStuBySubClass(lstSubClass)
            Debug.Print("")

            '------------------------------------------------------------

            dtDetails = New DataTable()
            'dtDetails = dtStu.DefaultView.ToTable(True, c_IDColumnName, _
            '                    c_NameColumnName, c_SubClassNameColumnName, c_ClassIDColumnName, _
            '                    c_SeatNumColumnName, c_SalesPersonColumnName, _
            '                    c_SchoolGradeColumnName, c_SubClassIDColumnName, _
            '                    c_EngNameColumnName, c_BirthdayColumnName, c_DadNameColumnName, _
            '                    c_MumNameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, _
            '                    c_DadMobileColumnName, c_MumMobileColumnName, c_MobileColumnName, _
            '                    c_SchoolColumnName, c_SchoolClassColumnName, c_GraduateFromColumnName, _
            '                    c_IntroIDColumnName, c_IntroNameColumnName, c_ICColumnName, _
            '                    c_EmailColumnName, c_PrimarySchColumnName, c_JuniorSchColumnName, _
            '                    c_HighSchColumnName, c_UniversityColumnName, c_CancelColumnName)


            dtDetails = dtStu.DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_SubClassNameColumnName, c_ClassIDColumnName, _
                                c_SeatNumColumnName, c_SalesPersonColumnName, _
                                c_SchoolGradeColumnName, c_SubClassIDColumnName, _
                                c_EngNameColumnName, c_BirthdayColumnName, c_DadNameColumnName, _
                                c_MumNameColumnName, c_DadTitleColumnName, c_MumTitleColumnName, _
                                c_Tel1ColumnName, c_Tel2ColumnName, _
                                c_DadMobileColumnName, c_MumMobileColumnName, c_MobileColumnName, _
                                c_SchoolColumnName, c_SchoolClassColumnName, c_GraduateFromColumnName, _
                                c_IntroIDColumnName, c_IntroNameColumnName, c_ICColumnName, _
                                c_EmailColumnName, c_PrimarySchColumnName, c_JuniorSchColumnName, _
                                c_HighSchColumnName, c_UniversityColumnName, c_CancelColumnName, _
                                c_AddressColumnName, c_Address2ColumnName, c_PostalCodeColumnName, _
                                c_PostalCode2ColumnName, c_OfficeTelColumnName, c_CardNumColumnName, _
                                c_CurrentSchColumnName, c_SchGroupColumnName, c_CreateDateColumnName, _
                                c_RemarksColumnName, _
                                "ComeInfo", "ClassNum", "Accommodation", c_EnrollSchColumnName, _
                                c_PunchCardNumColumnName, c_HseeMarkColumnName, c_RegDateColumnName, c_SexColumnName)


            InitTable()

            lstClassFee = New ArrayList
            For index As Integer = 0 To lstClass.Count - 1
                lstClassFee.Add(frmMain.GetClassFee2(lstClass(index)))
            Next

            For i = 0 To dtDetails.Rows.Count - 1
                If dtDetails.Rows(i).Item(c_CancelColumnName).ToString = "0" Then
                    intSubClass = dtDetails.Rows(i).Item(c_SubClassIDColumnName)
                    intClass = dtDetails.Rows(i).Item(c_ClassIDColumnName)
                    If lstSubClass.Contains(intSubClass) Then
                        strStuId = dtDetails.Rows(i).Item(c_IDColumnName)
                        intTotal = GetClassFeeFromList(intClass)
                        strCompute2 = c_SubClassIDColumnName & "=" & intSubClass.ToString & _
                                        " AND " & c_StuIDColumnName & "='" & strStuId & "'"
                        If dtPaySta.Compute(strCompute3, strCompute2) > 0 Then
                            intTotal2 = dtPaySta.Compute(strCompute5, strCompute2) 'discount
                            intTotal3 = dtPaySta.Compute(strCompute1, strCompute2) 'pay
                            intTotal4 = dtPaySta.Compute(strCompute4, strCompute2) 'back
                        Else
                            intTotal2 = 0
                            intTotal3 = 0
                            intTotal4 = 0
                        End If

                        If dtDisc.Compute(strCompute6, strCompute2) > 0 Then
                            intTotal2 = intTotal2 + dtDisc.Compute(strCompute5, strCompute2) 'discount
                        End If

                        dtDetails.Rows(i).Item(c_AmountColumnName) = intTotal - intTotal2
                        dtDetails.Rows(i).Item(c_PayAmountColumnName) = intTotal3 - intTotal4
                        dtDetails.Rows(i).Item(c_FeeOweColumnName) = intTotal - intTotal2 - _
                                        intTotal3 + intTotal4
                        dtDetails.Rows(i).Item(c_DiscountColumnName) = intTotal2
                        intTotalAll = intTotalAll + dtDetails.Rows(i).Item(c_FeeOweColumnName)
                    End If
                End If
            Next

            Dim f As String = ""
            If lstSubClass.Count > 0 Then
                f = c_SubClassIDColumnName & "=" & lstSubClass(0).ToString
            End If
            For index As Integer = 1 To lstSubClass.Count - 1
                f = f & " OR " & c_SubClassIDColumnName & "=" & lstSubClass(index).ToString
            Next
            If f = "" Then
                dtDetails.DefaultView.RowFilter = "Cancel=0"
            Else
                dtDetails.DefaultView.RowFilter = f + " And Cancel=0"
            End If

            dtDetails.DefaultView.RowFilter = "not FeeOwe=0"
            dgvDetails.DataSource = dtDetails.DefaultView
            tboxTotal.Text = Format(intTotalAll, "$#,##0")
            dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_FeeOweColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    'ListStuBySubClass
    Private Shared Function ListStuBySubClass(ByVal ClassID As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To ClassID.Count - 1
            list.Add(ClassID(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("StuOweSta", "ListStuBySubClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuBySubClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuBySubClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function


    Private Shared Function ListStuDiscByClass(ByVal ClassID As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To ClassID.Count - 1
            list.Add(ClassID(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("StuOweSta", "ListStuDiscByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuDiscByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuDiscByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function ListStuOweByClass(ByVal ClassID As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To ClassID.Count - 1
            list.Add(ClassID(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("StuOweSta", "ListStuOweByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuOweByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuOweByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Function GetClassFee(ByVal intId As Integer) As Integer
        GetClassFee = 0
        For index As Integer = 0 To dtSubClassList.Rows.Count - 1
            If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intId Then
                GetClassFee = dtSubClassList.Rows(index).Item(c_FeeColumnName)
                Exit For
            End If
        Next

    End Function

    Private Function GetClassFeeFromList(ByVal intId As Integer) As Integer
        GetClassFeeFromList = 0
        Dim index As Integer = -1
        index = lstClass.IndexOf(intId)
        If index > -1 And index < lstClassFee.Count Then
            GetClassFeeFromList = lstClassFee(index)
        End If

    End Function

    Private Sub InitClassList()
        Try
            dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
            dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        chklstSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()
            RefreshClassList()
            RefreshSubClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtDetails.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_Cancel2ColumnName, GetType(System.String))

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvDetails.Columns
            col.visible = False
        Next
        If dgvDetails.Rows.Count > 0 Then

            dgvDetails.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgvDetails.Columns.Item(c_IDColumnName).Visible = True
            dgvDetails.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgvDetails.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvDetails.Columns.Item(c_NameColumnName).Visible = True
            dgvDetails.Columns.Item(c_NameColumnName).HeaderText = My.Resources.stuName
            dgvDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgvDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvDetails.Columns.Item(c_SeatNumColumnName).DisplayIndex = 3
            dgvDetails.Columns.Item(c_SeatNumColumnName).Visible = True
            dgvDetails.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 4
            dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.shouldPay
            dgvDetails.Columns.Item(c_PayAmountColumnName).DisplayIndex = 5
            dgvDetails.Columns.Item(c_PayAmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_PayAmountColumnName).HeaderText = My.Resources.paid
            dgvDetails.Columns.Item(c_DiscountColumnName).DisplayIndex = 6
            dgvDetails.Columns.Item(c_DiscountColumnName).Visible = True
            dgvDetails.Columns.Item(c_DiscountColumnName).HeaderText = My.Resources.discount
            dgvDetails.Columns.Item(c_FeeOweColumnName).DisplayIndex = 7
            dgvDetails.Columns.Item(c_FeeOweColumnName).Visible = True
            dgvDetails.Columns.Item(c_FeeOweColumnName).HeaderText = My.Resources.feeOwe
            dgvDetails.Columns.Item(c_SalesPersonColumnName).DisplayIndex = 8
            dgvDetails.Columns.Item(c_SalesPersonColumnName).Visible = True
            dgvDetails.Columns.Item(c_SalesPersonColumnName).HeaderText = My.Resources.salesPerson
            If chkboxShowCancelCol.Checked Then
                dgvDetails.Columns.Item(c_Cancel2ColumnName).DisplayIndex = 9
                dgvDetails.Columns.Item(c_Cancel2ColumnName).Visible = True
                dgvDetails.Columns.Item(c_Cancel2ColumnName).HeaderText = My.Resources.cancel
            End If
        End If
        Dim i As Integer = 9
        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow(index) = 1 Then
                dgvDetails.Columns.Item(lstColName(index)).DisplayIndex = i
                dgvDetails.Columns.Item(lstColName(index)).Visible = True
                dgvDetails.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                i = i + 1
            End If
        Next
        If dgvDetails.Columns.Item(c_SchoolColumnName).Visible = True Then
            Dim id As Integer
            Select Case intColSchType
                Case 0
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_PrimarySchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 1
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_JuniorSchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 2
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_HighSchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 3
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_UniversityColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
            End Select
        End If

        If dgvDetails.Columns.Item(c_Cancel2ColumnName).Visible Then
            For index As Integer = 0 To dgvDetails.Rows.Count - 1
                If dgvDetails.Rows(index).Cells(c_CancelColumnName).Value = 1 Then
                    dgvDetails.Rows(index).Cells(c_Cancel2ColumnName).Value = My.Resources.cancel
                Else
                    dgvDetails.Rows(index).Cells(c_Cancel2ColumnName).Value = ""
                End If
            Next
        End If


    End Sub

    Private Sub InitColSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)

        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)

        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)


        lstColName.Add(c_ICColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_CardNumColumnName)                                                 '100225
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)


        lstColName.Add("ComeInfo")
        lstColName.Add("ClassNum")
        lstColName.Add("Accommodation")
        lstColName.Add(c_EnrollSchColumnName)
        lstColName.Add(c_PunchCardNumColumnName)
        lstColName.Add(c_HseeMarkColumnName)
        lstColName.Add(c_RegDateColumnName)

        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvDetails.Columns.Count > 7 Then
            Dim lst As New ArrayList
            For index As Integer = 8 To dgvDetails.Columns.Count - 1
                If dgvDetails.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            'chklstClass.SetItemChecked(0, True)
            chklstClass.SelectedItem = chklstClass.Items(0)

            If chklstSubClass.Items.Count > 0 Then
                'chklstSubClass.SetItemChecked(0, True)
                chklstSubClass.SelectedItem = chklstSubClass.Items(0)
            End If
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmStuOweSta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        Else
            lstClass.Remove(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    lstSubClass.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        End If
    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        Dim intC As Integer
        intC = lstSubClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstSubClass.Add(intC)
        Else
            lstSubClass.Remove(intC)
        End If
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(18) Then
            ExportDgvToExcel(dgvDetails)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgvDetails.SelectedRows.Count > 0 Then
            Dim id As String = dgvDetails.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub
End Class