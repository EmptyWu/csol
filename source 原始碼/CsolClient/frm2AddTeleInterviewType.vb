﻿Public Class frm2AddTeleInterviewType

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim s As String = tboxName.Text.Trim
        If s.Length > 0 Then
            objCsol.AddTeleInterviewType(s)
            frmMain.SetOkState(True)
            Me.Close()
        Else
            MsgBox(My.Resources.msgInvalidTeleTypeName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class