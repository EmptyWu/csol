﻿Public Class frm2DiscDictType
    Private dt As New DataTable

    Public Sub New()

        InitializeComponent()

        frmMain.SetOkState(False)
        RefreshData()

    End Sub

    Private Sub RefreshData()
        dt = objCsol.ListDiscountDictType

        dgv.DataSource = dt
        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        If dgv.Columns.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_TypeColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_TypeColumnName).HeaderText = My.Resources.type
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            objCsol.UpdateDiscDictType(CType(dgv.DataSource, DataTable).GetChanges)
            frmMain.RefreshDictInfo()
            frmMain.SetOkState(True)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(My.Resources.msgGroupSaveError, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End Try
        
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Try
                dgv.Rows.Remove(dgv.SelectedRows(0))
            Catch ex As Exception
            End Try
        End If
    End Sub
End Class