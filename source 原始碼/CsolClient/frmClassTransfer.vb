﻿Public Class frmClassTransfer
    Private dtStu As New DataTable
    Private dtStu2 As New DataTable
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmClassTransfer
        dtStu = objCsol.GetStuChangeClassList
        RefreshData()
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub frmClassTransfer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmClassTransfer_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim kw As String = tboxFilter.Text.Trim
        Dim f As String = ""
        Select Case cboxItem.SelectedIndex
            Case 0
                f = c_StuIDColumnName & "='" & kw & "'"
            Case 1
                f = c_NameColumnName & "='" & kw & "'"
            Case 2
                f = c_ClassNameColumnName & "='" & kw & "'"
            Case 3
                f = c_OldSubClassNameColumnName & "='" & kw & "'"
            Case 4
                f = c_NewSubClassNameColumnName & "='" & kw & "'"
            Case 5
                f = c_ChgdOnDateColumnName & "='" & kw & "'"
        End Select
        dtStu.DefaultView.RowFilter = f
        dgv.DataSource = dtStu.DefaultView
        LoadColumnText()

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        'If dgv.Rows.Count > 0 Then
        dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
        dgv.Columns.Item(c_StuIDColumnName).Visible = True
        dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
        dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
        dgv.Columns.Item(c_NameColumnName).Visible = True
        dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
        dgv.Columns.Item(c_OldSubClassNameColumnName).DisplayIndex = 2
        dgv.Columns.Item(c_OldSubClassNameColumnName).Visible = True
        dgv.Columns.Item(c_OldSubClassNameColumnName).HeaderText = My.Resources.oldSubClassName
        dgv.Columns.Item(c_OldSeatNumColumnName).DisplayIndex = 3
        dgv.Columns.Item(c_OldSeatNumColumnName).Visible = True
        dgv.Columns.Item(c_OldSeatNumColumnName).HeaderText = My.Resources.oldSeatNum
        dgv.Columns.Item(c_NewSubClassNameColumnName).DisplayIndex = 4
        dgv.Columns.Item(c_NewSubClassNameColumnName).Visible = True
        dgv.Columns.Item(c_NewSubClassNameColumnName).HeaderText = My.Resources.newSubClassName
        dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 5
        dgv.Columns.Item(c_SeatNumColumnName).Visible = True
        dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.newSeatNum
        dgv.Columns.Item(c_ChgdOnDateColumnName).DisplayIndex = 6
        dgv.Columns.Item(c_ChgdOnDateColumnName).Visible = True
        dgv.Columns.Item(c_ChgdOnDateColumnName).HeaderText = My.Resources.classChangeDate

        'End If
    End Sub

    Private Sub frmClassTransfer_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(7)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub butFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butFilter.Click
        RefreshData()
    End Sub

    Private Sub mnuExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        ExportDgvToExcel(dgv)
    End Sub
End Class