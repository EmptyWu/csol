﻿Public Class frm2TeleInterviewType
    Private dt As New DataTable

    Public Sub New()

        InitializeComponent()

        RefreshData()

    End Sub

    Private Sub RefreshData()
        dt = objCsol.ListTeleInterviewType

        dgv.DataSource = dt
        loadcolumntext()
    End Sub

    Private Sub loadcolumntext()
        dgv.Columns.Item(c_IDColumnName).Visible = False
        dgv.Columns.Item(c_TypeColumnName).HeaderText = My.Resources.type

    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        objCsol.UpdateTeleInterviewType(CType(dgv.DataSource, DataTable).GetChanges)
        frmMain.RefreshTeleInterviewType()
        Me.Close()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            dgv.Rows.Remove(dgv.SelectedRows(0))
        End If
    End Sub
End Class