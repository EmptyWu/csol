﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2PayNotice
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2PayNotice))
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butDict = New System.Windows.Forms.Button
        Me.tboxRptRemarks = New System.Windows.Forms.TextBox
        Me.tboxDiscRemarks = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.butPrint = New System.Windows.Forms.Button
        Me.butDisDict = New System.Windows.Forms.Button
        Me.prtdocReceipt = New System.Drawing.Printing.PrintDocument
        Me.butCancel = New System.Windows.Forms.Button
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'butDict
        '
        resources.ApplyResources(Me.butDict, "butDict")
        Me.butDict.Name = "butDict"
        Me.butDict.UseVisualStyleBackColor = True
        '
        'tboxRptRemarks
        '
        resources.ApplyResources(Me.tboxRptRemarks, "tboxRptRemarks")
        Me.tboxRptRemarks.Name = "tboxRptRemarks"
        '
        'tboxDiscRemarks
        '
        resources.ApplyResources(Me.tboxDiscRemarks, "tboxDiscRemarks")
        Me.tboxDiscRemarks.Name = "tboxDiscRemarks"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoCheck = False
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'butPrint
        '
        resources.ApplyResources(Me.butPrint, "butPrint")
        Me.butPrint.Name = "butPrint"
        Me.butPrint.UseVisualStyleBackColor = True
        '
        'butDisDict
        '
        resources.ApplyResources(Me.butDisDict, "butDisDict")
        Me.butDisDict.Name = "butDisDict"
        Me.butDisDict.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2PayNotice
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butDisDict)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butDict)
        Me.Controls.Add(Me.tboxRptRemarks)
        Me.Controls.Add(Me.tboxDiscRemarks)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.butPrint)
        Me.Name = "frm2PayNotice"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butDict As System.Windows.Forms.Button
    Friend WithEvents tboxRptRemarks As System.Windows.Forms.TextBox
    Friend WithEvents tboxDiscRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents butPrint As System.Windows.Forms.Button
    Friend WithEvents butDisDict As System.Windows.Forms.Button
    Friend WithEvents prtdocReceipt As System.Drawing.Printing.PrintDocument
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
