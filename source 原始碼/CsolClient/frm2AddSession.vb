﻿Public Class frm2AddSession
    Private intId As Integer
    Private intScId As Integer = 0
    Private intDw As Integer = 0
    Private dt As New DataTable
    Private lst As New ArrayList
    Private dtStart As Date
    Private dtEnd As Date
    Private intTurn As Integer = 0

    Public Sub New(ByVal ID As Integer, _
                   ByVal SubClassID As Integer, _
                         ByVal DayOfWeek As Integer, ByVal dtS As Date, _
                         ByVal dtE As Date)

        InitializeComponent()

        intScId = SubClassID
        intDw = DayOfWeek
        intId = ID
        dtStart = dtS
        dtEnd = dtE

        If intId = -1 Then 'add new
            Me.Text = My.Resources.addSession
            cboxWd.SelectedIndex = 0
        Else 'update
            tboxTime1.Text = GetTimePart(dtStart.Hour)
            tboxTime2.Text = GetTimePart(dtStart.Minute)
            tboxTime3.Text = GetTimePart(dtEnd.Hour)
            tboxTime4.Text = GetTimePart(dtEnd.Minute)
            cboxWd.SelectedIndex = intDw - 1
            Me.Text = My.Resources.updSession
        End If

        RefreshData()
    End Sub

    Private Function GetTimePart(ByVal d As Integer) As String
        Dim s As String
        s = d.ToString
        If s.Length < 2 Then
            s = "0" & s
        End If
        Return s
    End Function

    Private Sub RefreshData()
        dt = objCsol.ListSampleTimes

        dgvTime.Rows.Clear()
        Dim c As Integer = 0
        Dim r As Integer = 0
        Dim s As String
        dgvTime.RowCount = 6

        For index As Integer = 0 To dt.Rows.Count - 1
            s = GetTimePart(CDate(dt.Rows(index).Item(c_DateTimeColumnName)).Hour) & ":" & _
                GetTimePart(CDate(dt.Rows(index).Item(c_DateTimeColumnName)).Minute)
            If c = 2 Then
                If r < 6 Then
                    dgvTime.Rows(r).Cells(c).Value = s
                    c = 0
                    r = r + 1
                Else
                    Exit For
                End If
            Else
                dgvTime.Rows(r).Cells(c).Value = s
                c = c + 1
            End If
        Next

    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim dt As Date
        If TimeCheckCorrect() And cboxWd.SelectedIndex > -1 Then
            frmMain.SetOkState(True)
            If intId = -1 Then
                objCsol.AddClassSession(intScId, cboxWd.SelectedIndex + 1, _
                                    GetTime1, GetTime2)
            Else
                objCsol.ModifyClassSession(intId, intScId, cboxWd.SelectedIndex + 1, _
                                    GetTime1, GetTime2)
            End If
            frmMain.RefreshClassRegInfo()
            frmMain.RefreshClassSessionTb()
        Else
            ShowMsg()
            lst = New ArrayList

            For index As Integer = 0 To dgvTime.Rows.Count - 1
                For j As Integer = 0 To 2
                    If Not dgvTime.Rows(index).Cells(j).Value Is Nothing Then
                        dt = GetTime3(dgvTime.Rows(index).Cells(j).Value.ToString.Trim)
                        If Not dt = Nothing Then
                            lst.Add(dt)
                        End If
                    End If
                Next
            Next

            objCsol.UpdateSampleTimes(lst)
            Exit Sub
        End If
        lst = New ArrayList

        For index As Integer = 0 To dgvTime.Rows.Count - 1
            For j As Integer = 0 To 2
                If Not dgvTime.Rows(index).Cells(j).Value Is Nothing Then
                    dt = GetTime3(dgvTime.Rows(index).Cells(j).Value.ToString.Trim)
                    If Not dt = Nothing Then
                        lst.Add(dt)
                    End If
                End If
            Next
        Next

        objCsol.UpdateSampleTimes(lst)
        frmMain.BackgroundWorker7.RunWorkerAsync()
        Me.Close()
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgWrongSession, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Function GetTime1() As Date
        Return New Date(2009, 1, 1, CInt(tboxTime1.Text), CInt(tboxTime2.Text), 0)
    End Function

    Private Function GetTime2() As Date
        Return New Date(2009, 1, 1, CInt(tboxTime3.Text), CInt(tboxTime4.Text), 0)
    End Function

    Private Function GetTime3(ByVal s As String) As Date
        Dim s1 As String
        Dim s2 As String

        If s.Length > 3 And s.IndexOf(":") > -1 Then
            s1 = s.Split(":")(0)
            s2 = s.Split(":")(1)
            If IsNumeric(s1) And IsNumeric(s2) Then
                If CInt(s1) > -1 And CInt(s1) < 24 Then
                    If CInt(s2) > -1 And CInt(s2) < 60 Then
                        Return New Date(2009, 1, 1, CInt(s1), CInt(s2), 0)
                    Else
                        Return Nothing
                    End If
                Else
                    Return Nothing
                End If
            Else
                Return Nothing
            End If
        Else
            Return Nothing
        End If
    End Function

    Private Sub GetTime4(ByVal s As String)
        Dim s1 As String
        Dim s2 As String
        Dim a As Array

        If s.Length > 3 Then
            a = s.Split(":")
            If a.Length < 2 Then
                Exit Sub
            End If
            s1 = a(0)
            s2 = a(1)
            If IsNumeric(s1) And IsNumeric(s2) Then
                If CInt(s1) > -1 And CInt(s1) < 24 Then
                    If CInt(s2) > -1 And CInt(s2) < 60 Then
                        If intTurn = 0 Then
                            tboxTime1.Text = GetTimePart(s1)
                            tboxTime2.Text = GetTimePart(s2)
                            intTurn = -1
                        ElseIf intTurn = 1 Then
                            tboxTime3.Text = GetTimePart(s1)
                            tboxTime4.Text = GetTimePart(s2)
                            intTurn = -1
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function TimeCheckCorrect() As Boolean
        Dim b As Boolean = False
        If IsNumeric(tboxTime1.Text) And IsNumeric(tboxTime2.Text) And _
            IsNumeric(tboxTime3.Text) And IsNumeric(tboxTime4.Text) Then
            Dim t1 As Integer
            Dim t2 As Integer
            Dim t3 As Integer
            Dim t4 As Integer

            t1 = CInt(tboxTime1.Text)
            t2 = CInt(tboxTime2.Text)
            t3 = CInt(tboxTime3.Text)
            t4 = CInt(tboxTime4.Text)

            If t1 > -1 And t1 < 24 And t2 > -1 And t2 < 60 And _
                t3 > -1 And t3 < 24 And t4 > -1 And t4 < 60 Then
                b = True
            End If
        End If

        Return b
    End Function

    Private Sub dgvTime_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvTime.CellClick
        If Not dgvTime.CurrentCell.Value Is Nothing Then
            If Not dgvTime.CurrentCell.Value.ToString.Trim = "" Then
                GetTime4(dgvTime.CurrentCell.Value.ToString.Trim)
            End If
        End If
        dgvTime.CurrentCell.ReadOnly = False
    End Sub

    Private Sub tboxTime1_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tboxTime1.MouseClick
        intTurn = 0
    End Sub

    Private Sub tboxTime2_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tboxTime2.MouseClick
        intTurn = 0
    End Sub

    Private Sub tboxTime3_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tboxTime3.MouseClick
        intTurn = 1
    End Sub

    Private Sub tboxTime4_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles tboxTime4.MouseClick
        intTurn = 1
    End Sub
End Class