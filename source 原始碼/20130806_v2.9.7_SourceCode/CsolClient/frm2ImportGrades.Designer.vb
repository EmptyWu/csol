﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ImportGrades
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.cboxTable = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.butFile = New System.Windows.Forms.Button
        Me.tboxFile = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.cboxMark = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboxName = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboxID = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.radbutUseNew = New System.Windows.Forms.RadioButton
        Me.radbutKeepOld = New System.Windows.Forms.RadioButton
        Me.butImport = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.butClose = New System.Windows.Forms.Button
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.cboxPaper = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.dtPickInput = New System.Windows.Forms.DateTimePicker
        Me.radbutSelect = New System.Windows.Forms.RadioButton
        Me.radbutNow = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.cboxTable)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.butFile)
        Me.GroupBox1.Controls.Add(Me.tboxFile)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(479, 94)
        Me.GroupBox1.TabIndex = 8
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "來源檔案"
        '
        'cboxTable
        '
        Me.cboxTable.FormattingEnabled = True
        Me.cboxTable.Location = New System.Drawing.Point(93, 58)
        Me.cboxTable.Name = "cboxTable"
        Me.cboxTable.Size = New System.Drawing.Size(281, 20)
        Me.cboxTable.TabIndex = 12
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(33, 58)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 12)
        Me.Label3.TabIndex = 11
        Me.Label3.Text = "資料表: "
        '
        'butFile
        '
        Me.butFile.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFile.Location = New System.Drawing.Point(380, 18)
        Me.butFile.Name = "butFile"
        Me.butFile.Size = New System.Drawing.Size(72, 25)
        Me.butFile.TabIndex = 10
        Me.butFile.Text = "瀏覽..."
        Me.butFile.UseVisualStyleBackColor = True
        '
        'tboxFile
        '
        Me.tboxFile.Location = New System.Drawing.Point(93, 21)
        Me.tboxFile.Name = "tboxFile"
        Me.tboxFile.Size = New System.Drawing.Size(281, 22)
        Me.tboxFile.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(19, 24)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(61, 12)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "Excel檔案: "
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.cboxMark)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.cboxName)
        Me.GroupBox6.Controls.Add(Me.Label4)
        Me.GroupBox6.Controls.Add(Me.cboxID)
        Me.GroupBox6.Controls.Add(Me.Label6)
        Me.GroupBox6.Location = New System.Drawing.Point(12, 115)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(479, 77)
        Me.GroupBox6.TabIndex = 13
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "對應欄位(學號跟姓名擇一)"
        '
        'cboxMark
        '
        Me.cboxMark.FormattingEnabled = True
        Me.cboxMark.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboxMark.Location = New System.Drawing.Point(60, 41)
        Me.cboxMark.Name = "cboxMark"
        Me.cboxMark.Size = New System.Drawing.Size(146, 20)
        Me.cboxMark.TabIndex = 6
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(19, 44)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 12)
        Me.Label7.TabIndex = 5
        Me.Label7.Text = "分數: "
        '
        'cboxName
        '
        Me.cboxName.FormattingEnabled = True
        Me.cboxName.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboxName.Location = New System.Drawing.Point(284, 15)
        Me.cboxName.Name = "cboxName"
        Me.cboxName.Size = New System.Drawing.Size(146, 20)
        Me.cboxName.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(231, 18)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "姓名: "
        '
        'cboxID
        '
        Me.cboxID.FormattingEnabled = True
        Me.cboxID.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboxID.Location = New System.Drawing.Point(60, 15)
        Me.cboxID.Name = "cboxID"
        Me.cboxID.Size = New System.Drawing.Size(146, 20)
        Me.cboxID.TabIndex = 2
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(19, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 12)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "學號: "
        '
        'radbutUseNew
        '
        Me.radbutUseNew.AutoSize = True
        Me.radbutUseNew.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutUseNew.Location = New System.Drawing.Point(196, 16)
        Me.radbutUseNew.Name = "radbutUseNew"
        Me.radbutUseNew.Size = New System.Drawing.Size(119, 16)
        Me.radbutUseNew.TabIndex = 2
        Me.radbutUseNew.TabStop = True
        Me.radbutUseNew.Text = "使用新的取代舊的"
        Me.radbutUseNew.UseVisualStyleBackColor = True
        '
        'radbutKeepOld
        '
        Me.radbutKeepOld.AutoSize = True
        Me.radbutKeepOld.Checked = True
        Me.radbutKeepOld.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutKeepOld.Location = New System.Drawing.Point(21, 16)
        Me.radbutKeepOld.Name = "radbutKeepOld"
        Me.radbutKeepOld.Size = New System.Drawing.Size(95, 16)
        Me.radbutKeepOld.TabIndex = 1
        Me.radbutKeepOld.TabStop = True
        Me.radbutKeepOld.Text = "保留舊的分數"
        Me.radbutKeepOld.UseVisualStyleBackColor = True
        '
        'butImport
        '
        Me.butImport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butImport.Location = New System.Drawing.Point(323, 419)
        Me.butImport.Name = "butImport"
        Me.butImport.Size = New System.Drawing.Size(82, 25)
        Me.butImport.TabIndex = 14
        Me.butImport.Text = "匯入"
        Me.butImport.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.radbutUseNew)
        Me.GroupBox5.Controls.Add(Me.radbutKeepOld)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 294)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(479, 44)
        Me.GroupBox5.TabIndex = 12
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "如果已有分數"
        '
        'butClose
        '
        Me.butClose.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butClose.Location = New System.Drawing.Point(411, 419)
        Me.butClose.Name = "butClose"
        Me.butClose.Size = New System.Drawing.Size(82, 25)
        Me.butClose.TabIndex = 15
        Me.butClose.Text = "關閉"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox2.Controls.Add(Me.cboxPaper)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.cboxClass)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 201)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(479, 84)
        Me.GroupBox2.TabIndex = 16
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "匯入到"
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(60, 2)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 145
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'cboxPaper
        '
        Me.cboxPaper.FormattingEnabled = True
        Me.cboxPaper.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboxPaper.Location = New System.Drawing.Point(60, 51)
        Me.cboxPaper.Name = "cboxPaper"
        Me.cboxPaper.Size = New System.Drawing.Size(329, 20)
        Me.cboxPaper.TabIndex = 6
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(19, 54)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 12)
        Me.Label9.TabIndex = 5
        Me.Label9.Text = "試卷: "
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Items.AddRange(New Object() {"1", "2", "3", "4"})
        Me.cboxClass.Location = New System.Drawing.Point(60, 25)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(329, 20)
        Me.cboxClass.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(19, 28)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 12)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "班級: "
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dtPickInput)
        Me.GroupBox3.Controls.Add(Me.radbutSelect)
        Me.GroupBox3.Controls.Add(Me.radbutNow)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 347)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(479, 55)
        Me.GroupBox3.TabIndex = 130
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "輸入日期"
        '
        'dtPickInput
        '
        Me.dtPickInput.Location = New System.Drawing.Point(311, 18)
        Me.dtPickInput.Name = "dtPickInput"
        Me.dtPickInput.Size = New System.Drawing.Size(149, 22)
        Me.dtPickInput.TabIndex = 2
        '
        'radbutSelect
        '
        Me.radbutSelect.AutoSize = True
        Me.radbutSelect.Location = New System.Drawing.Point(196, 21)
        Me.radbutSelect.Name = "radbutSelect"
        Me.radbutSelect.Size = New System.Drawing.Size(95, 16)
        Me.radbutSelect.TabIndex = 1
        Me.radbutSelect.TabStop = True
        Me.radbutSelect.Text = "使用這個日期"
        Me.radbutSelect.UseVisualStyleBackColor = True
        '
        'radbutNow
        '
        Me.radbutNow.AutoSize = True
        Me.radbutNow.Checked = True
        Me.radbutNow.Location = New System.Drawing.Point(21, 21)
        Me.radbutNow.Name = "radbutNow"
        Me.radbutNow.Size = New System.Drawing.Size(155, 16)
        Me.radbutNow.TabIndex = 0
        Me.radbutNow.TabStop = True
        Me.radbutNow.Text = "使用這台電腦現在的日期"
        Me.radbutNow.UseVisualStyleBackColor = True
        '
        'frm2ImportGrades
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(504, 456)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.butImport)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.butClose)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ImportGrades"
        Me.ShowInTaskbar = False
        Me.Text = "匯入成績"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxMark As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboxName As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents radbutUseNew As System.Windows.Forms.RadioButton
    Friend WithEvents radbutKeepOld As System.Windows.Forms.RadioButton
    Friend WithEvents butImport As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents cboxTable As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents butFile As System.Windows.Forms.Button
    Friend WithEvents tboxFile As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxPaper As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPickInput As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutSelect As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNow As System.Windows.Forms.RadioButton
End Class
