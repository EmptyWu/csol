﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetClassPaper
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuMod = New System.Windows.Forms.ToolStripMenuItem
        Me.tboxIndex = New System.Windows.Forms.TextBox
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        Me.mnuDelete.Size = New System.Drawing.Size(68, 20)
        Me.mnuDelete.Text = "刪除試卷"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(15, 64)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(976, 513)
        Me.dgv.TabIndex = 136
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(61, 35)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(261, 20)
        Me.cboxClass.TabIndex = 135
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(13, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 134
        Me.Label1.Text = "班級："
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(344, 37)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 139
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出清單"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuMod, Me.mnuDelete, Me.mnuExport, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 138
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.Size = New System.Drawing.Size(68, 20)
        Me.mnuNew.Text = "新增試卷"
        '
        'mnuMod
        '
        Me.mnuMod.Name = "mnuMod"
        Me.mnuMod.Size = New System.Drawing.Size(44, 20)
        Me.mnuMod.Text = "修改"
        '
        'tboxIndex
        '
        Me.tboxIndex.BackColor = System.Drawing.Color.Silver
        Me.tboxIndex.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxIndex.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.tboxIndex.Location = New System.Drawing.Point(915, 35)
        Me.tboxIndex.Name = "tboxIndex"
        Me.tboxIndex.Size = New System.Drawing.Size(76, 22)
        Me.tboxIndex.TabIndex = 140
        Me.tboxIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'frmSetClassPaper
        '
        Me.AllowDrop = True
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.tboxIndex)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmSetClassPaper"
        Me.Text = "設定各班考卷"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMod As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxIndex As System.Windows.Forms.TextBox
End Class
