﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmHandlerPaySta
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmHandlerPaySta))
        Me.chkboxNewStuSta = New System.Windows.Forms.CheckBox
        Me.dgvClassDetails = New System.Windows.Forms.DataGridView
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.radbutThisMonth = New System.Windows.Forms.RadioButton
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.dgvMaster = New System.Windows.Forms.DataGridView
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvClassMaster = New System.Windows.Forms.DataGridView
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgvDetails = New System.Windows.Forms.DataGridView
        Me.radbutCustDate = New System.Windows.Forms.RadioButton
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrintAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.butListSta = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.radbutByDate = New System.Windows.Forms.RadioButton
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblEmployee = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvClassDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvClassMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkboxNewStuSta
        '
        resources.ApplyResources(Me.chkboxNewStuSta, "chkboxNewStuSta")
        Me.chkboxNewStuSta.Checked = True
        Me.chkboxNewStuSta.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxNewStuSta.Name = "chkboxNewStuSta"
        Me.chkboxNewStuSta.UseVisualStyleBackColor = True
        '
        'dgvClassDetails
        '
        Me.dgvClassDetails.AllowUserToAddRows = False
        Me.dgvClassDetails.AllowUserToDeleteRows = False
        Me.dgvClassDetails.AllowUserToResizeRows = False
        Me.dgvClassDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassDetails, "dgvClassDetails")
        Me.dgvClassDetails.MultiSelect = False
        Me.dgvClassDetails.Name = "dgvClassDetails"
        Me.dgvClassDetails.RowHeadersVisible = False
        Me.dgvClassDetails.RowTemplate.Height = 15
        Me.dgvClassDetails.RowTemplate.ReadOnly = True
        Me.dgvClassDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassDetails.ShowCellToolTips = False
        Me.dgvClassDetails.ShowEditingIcon = False
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'radbutThisMonth
        '
        resources.ApplyResources(Me.radbutThisMonth, "radbutThisMonth")
        Me.radbutThisMonth.Name = "radbutThisMonth"
        Me.radbutThisMonth.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvMaster)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvMaster, "dgvMaster")
        Me.dgvMaster.MultiSelect = False
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowTemplate.Height = 15
        Me.dgvMaster.RowTemplate.ReadOnly = True
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.ShowCellToolTips = False
        Me.dgvMaster.ShowEditingIcon = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvClassMaster)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'dgvClassMaster
        '
        Me.dgvClassMaster.AllowUserToAddRows = False
        Me.dgvClassMaster.AllowUserToDeleteRows = False
        Me.dgvClassMaster.AllowUserToResizeRows = False
        Me.dgvClassMaster.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassMaster, "dgvClassMaster")
        Me.dgvClassMaster.MultiSelect = False
        Me.dgvClassMaster.Name = "dgvClassMaster"
        Me.dgvClassMaster.RowHeadersVisible = False
        Me.dgvClassMaster.RowTemplate.Height = 15
        Me.dgvClassMaster.RowTemplate.ReadOnly = True
        Me.dgvClassMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassMaster.ShowCellToolTips = False
        Me.dgvClassMaster.ShowEditingIcon = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvDetails)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'dgvDetails
        '
        Me.dgvDetails.AllowUserToAddRows = False
        Me.dgvDetails.AllowUserToDeleteRows = False
        Me.dgvDetails.AllowUserToResizeRows = False
        Me.dgvDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvDetails, "dgvDetails")
        Me.dgvDetails.MultiSelect = False
        Me.dgvDetails.Name = "dgvDetails"
        Me.dgvDetails.RowHeadersVisible = False
        Me.dgvDetails.RowTemplate.Height = 15
        Me.dgvDetails.RowTemplate.ReadOnly = True
        Me.dgvDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetails.ShowCellToolTips = False
        Me.dgvDetails.ShowEditingIcon = False
        '
        'radbutCustDate
        '
        resources.ApplyResources(Me.radbutCustDate, "radbutCustDate")
        Me.radbutCustDate.Name = "radbutCustDate"
        Me.radbutCustDate.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrintAll, Me.mnuPrintDetails, Me.mnuExportAll, Me.mnuExportDetails, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrintAll
        '
        Me.mnuPrintAll.Name = "mnuPrintAll"
        resources.ApplyResources(Me.mnuPrintAll, "mnuPrintAll")
        '
        'mnuPrintDetails
        '
        Me.mnuPrintDetails.Name = "mnuPrintDetails"
        resources.ApplyResources(Me.mnuPrintDetails, "mnuPrintDetails")
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        resources.ApplyResources(Me.mnuExportAll, "mnuExportAll")
        '
        'mnuExportDetails
        '
        Me.mnuExportDetails.Name = "mnuExportDetails"
        resources.ApplyResources(Me.mnuExportDetails, "mnuExportDetails")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvClassDetails)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'radbutByDate
        '
        resources.ApplyResources(Me.radbutByDate, "radbutByDate")
        Me.radbutByDate.Checked = True
        Me.radbutByDate.Name = "radbutByDate"
        Me.radbutByDate.TabStop = True
        Me.radbutByDate.UseVisualStyleBackColor = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'lblEmployee
        '
        resources.ApplyResources(Me.lblEmployee, "lblEmployee")
        Me.lblEmployee.Name = "lblEmployee"
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'frmHandlerPaySta
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.chkboxNewStuSta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lblEmployee)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.radbutThisMonth)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.radbutCustDate)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radbutByDate)
        Me.Name = "frmHandlerPaySta"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        CType(Me.dgvClassDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvClassMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkboxNewStuSta As System.Windows.Forms.CheckBox
    Friend WithEvents dgvClassDetails As System.Windows.Forms.DataGridView
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents radbutThisMonth As System.Windows.Forms.RadioButton
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvClassMaster As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDetails As System.Windows.Forms.DataGridView
    Friend WithEvents radbutCustDate As System.Windows.Forms.RadioButton
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrintAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents radbutByDate As System.Windows.Forms.RadioButton
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblEmployee As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
End Class
