﻿Public Class frm2JournalGrp
    Private dtGrp As New DataTable

    Public Sub New()

        InitializeComponent()

        RefreshData()

    End Sub

    Private Sub RefreshData()
        dtGrp = frmMain.GetJournalGrp

        dgvGrp.DataSource = dtGrp
        LoadColumnText()
    End Sub


    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            objCsol.UpdateJournalGrp(CType(dgvGrp.DataSource, DataTable).GetChanges)
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(My.Resources.msgGroupSaveError, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If dgvGrp.SelectedRows.Count > 0 Then
            dgvGrp.Rows.Remove(dgvGrp.SelectedRows(0))
        End If
    End Sub

    Private Sub LoadColumnText()
        If dgvGrp.Columns.Count > 0 Then
            dgvGrp.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgvGrp.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvGrp.Columns.Item(c_NameColumnName).HeaderText = My.Resources.group
        End If
    End Sub

End Class