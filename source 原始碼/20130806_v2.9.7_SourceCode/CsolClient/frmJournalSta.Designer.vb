﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJournalSta
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJournalSta))
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.butListSta = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.radbutNoDate = New System.Windows.Forms.RadioButton
        Me.radbutDateCust = New System.Windows.Forms.RadioButton
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrintAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.tboxTotal = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvMaster = New System.Windows.Forms.DataGridView
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvDetails = New System.Windows.Forms.DataGridView
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutSta2 = New System.Windows.Forms.RadioButton
        Me.radbutSta1 = New System.Windows.Forms.RadioButton
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'radbutNoDate
        '
        resources.ApplyResources(Me.radbutNoDate, "radbutNoDate")
        Me.radbutNoDate.Name = "radbutNoDate"
        Me.radbutNoDate.UseVisualStyleBackColor = True
        '
        'radbutDateCust
        '
        resources.ApplyResources(Me.radbutDateCust, "radbutDateCust")
        Me.radbutDateCust.Checked = True
        Me.radbutDateCust.Name = "radbutDateCust"
        Me.radbutDateCust.TabStop = True
        Me.radbutDateCust.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrintAll, Me.mnuPrintDetails, Me.mnuExportAll, Me.mnuExportDetails, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrintAll
        '
        Me.mnuPrintAll.Name = "mnuPrintAll"
        resources.ApplyResources(Me.mnuPrintAll, "mnuPrintAll")
        '
        'mnuPrintDetails
        '
        Me.mnuPrintDetails.Name = "mnuPrintDetails"
        resources.ApplyResources(Me.mnuPrintDetails, "mnuPrintDetails")
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        resources.ApplyResources(Me.mnuExportAll, "mnuExportAll")
        '
        'mnuExportDetails
        '
        Me.mnuExportDetails.Name = "mnuExportDetails"
        resources.ApplyResources(Me.mnuExportDetails, "mnuExportDetails")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'tboxTotal
        '
        resources.ApplyResources(Me.tboxTotal, "tboxTotal")
        Me.tboxTotal.Name = "tboxTotal"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvMaster)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvMaster, "dgvMaster")
        Me.dgvMaster.MultiSelect = False
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowTemplate.Height = 15
        Me.dgvMaster.RowTemplate.ReadOnly = True
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.ShowCellToolTips = False
        Me.dgvMaster.ShowEditingIcon = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvDetails)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'dgvDetails
        '
        Me.dgvDetails.AllowUserToAddRows = False
        Me.dgvDetails.AllowUserToDeleteRows = False
        Me.dgvDetails.AllowUserToResizeRows = False
        Me.dgvDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvDetails, "dgvDetails")
        Me.dgvDetails.MultiSelect = False
        Me.dgvDetails.Name = "dgvDetails"
        Me.dgvDetails.RowHeadersVisible = False
        Me.dgvDetails.RowTemplate.Height = 15
        Me.dgvDetails.RowTemplate.ReadOnly = True
        Me.dgvDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetails.ShowCellToolTips = False
        Me.dgvDetails.ShowEditingIcon = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutSta2)
        Me.GroupBox3.Controls.Add(Me.radbutSta1)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'radbutSta2
        '
        resources.ApplyResources(Me.radbutSta2, "radbutSta2")
        Me.radbutSta2.Name = "radbutSta2"
        Me.radbutSta2.UseVisualStyleBackColor = True
        '
        'radbutSta1
        '
        resources.ApplyResources(Me.radbutSta1, "radbutSta1")
        Me.radbutSta1.Checked = True
        Me.radbutSta1.Name = "radbutSta1"
        Me.radbutSta1.TabStop = True
        Me.radbutSta1.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'frmJournalSta
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.tboxTotal)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.radbutNoDate)
        Me.Controls.Add(Me.radbutDateCust)
        Me.Name = "frmJournalSta"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents radbutNoDate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutDateCust As System.Windows.Forms.RadioButton
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrintAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDetails As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutSta2 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutSta1 As System.Windows.Forms.RadioButton
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
End Class
