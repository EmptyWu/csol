﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddTeleInterviewStu
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label5 = New System.Windows.Forms.Label
        Me.butAddType = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.tboxContent = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboxType = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(53, 219)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(192, 12)
        Me.Label5.TabIndex = 148
        Me.Label5.Text = "最長255個中文字，Enter算兩個字。"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butAddType
        '
        Me.butAddType.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAddType.Location = New System.Drawing.Point(229, 9)
        Me.butAddType.Name = "butAddType"
        Me.butAddType.Size = New System.Drawing.Size(82, 25)
        Me.butAddType.TabIndex = 138
        Me.butAddType.Text = "新增類別"
        Me.butAddType.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(336, 242)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 142
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(229, 242)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(82, 25)
        Me.butSave.TabIndex = 141
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(55, 38)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(165, 22)
        Me.dtpickDate.TabIndex = 139
        '
        'tboxContent
        '
        Me.tboxContent.Location = New System.Drawing.Point(55, 67)
        Me.tboxContent.MaxLength = 255
        Me.tboxContent.Multiline = True
        Me.tboxContent.Name = "tboxContent"
        Me.tboxContent.Size = New System.Drawing.Size(363, 149)
        Me.tboxContent.TabIndex = 140
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(11, 67)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 147
        Me.Label4.Text = "內容: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(11, 43)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 146
        Me.Label3.Text = "日期: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(10, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 145
        Me.Label2.Text = "類別: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxType
        '
        Me.cboxType.FormattingEnabled = True
        Me.cboxType.Location = New System.Drawing.Point(55, 12)
        Me.cboxType.Name = "cboxType"
        Me.cboxType.Size = New System.Drawing.Size(165, 20)
        Me.cboxType.TabIndex = 137
        '
        'frm2AddTeleInterviewStu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(431, 278)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.butAddType)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxContent)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboxType)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddTeleInterviewStu"
        Me.Text = "輸入電訪"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents butAddType As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxContent As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboxType As System.Windows.Forms.ComboBox
End Class
