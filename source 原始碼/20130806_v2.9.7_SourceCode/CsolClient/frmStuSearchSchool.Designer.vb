﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuSearchSchool
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuSearchSchool))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportPhoto = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPostalTag = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tboxClass = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboxGrade = New System.Windows.Forms.ComboBox
        Me.chkboxShowCancel = New System.Windows.Forms.CheckBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.butSch = New System.Windows.Forms.Button
        Me.cboxSch = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.radbutUni = New System.Windows.Forms.RadioButton
        Me.radbutHig = New System.Windows.Forms.RadioButton
        Me.radbutJun = New System.Windows.Forms.RadioButton
        Me.radbutPri = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chkboxNotInClass = New System.Windows.Forms.CheckBox
        Me.chkboxInClass = New System.Windows.Forms.CheckBox
        Me.radbutType = New System.Windows.Forms.RadioButton
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.radbutClass = New System.Windows.Forms.RadioButton
        Me.tboxCount = New System.Windows.Forms.TextBox
        Me.butFilter = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.Label2 = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuSelectCol, Me.mnuDelete, Me.mnuPrint, Me.mnuExportData, Me.mnuExportPhoto, Me.mnuPostalTag, Me.mnuNote, Me.mnuClose})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        resources.ApplyResources(Me.mnuExportData, "mnuExportData")
        '
        'mnuExportPhoto
        '
        Me.mnuExportPhoto.Name = "mnuExportPhoto"
        resources.ApplyResources(Me.mnuExportPhoto, "mnuExportPhoto")
        '
        'mnuPostalTag
        '
        Me.mnuPostalTag.Name = "mnuPostalTag"
        resources.ApplyResources(Me.mnuPostalTag, "mnuPostalTag")
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        resources.ApplyResources(Me.mnuNote, "mnuNote")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tboxClass)
        Me.GroupBox1.Controls.Add(Me.Label16)
        Me.GroupBox1.Controls.Add(Me.cboxGrade)
        Me.GroupBox1.Controls.Add(Me.chkboxShowCancel)
        Me.GroupBox1.Controls.Add(Me.Label14)
        Me.GroupBox1.Controls.Add(Me.butSch)
        Me.GroupBox1.Controls.Add(Me.cboxSch)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.radbutUni)
        Me.GroupBox1.Controls.Add(Me.radbutHig)
        Me.GroupBox1.Controls.Add(Me.radbutJun)
        Me.GroupBox1.Controls.Add(Me.radbutPri)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'tboxClass
        '
        resources.ApplyResources(Me.tboxClass, "tboxClass")
        Me.tboxClass.Name = "tboxClass"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'cboxGrade
        '
        Me.cboxGrade.FormattingEnabled = True
        Me.cboxGrade.Items.AddRange(New Object() {resources.GetString("cboxGrade.Items"), resources.GetString("cboxGrade.Items1"), resources.GetString("cboxGrade.Items2"), resources.GetString("cboxGrade.Items3"), resources.GetString("cboxGrade.Items4"), resources.GetString("cboxGrade.Items5"), resources.GetString("cboxGrade.Items6")})
        resources.ApplyResources(Me.cboxGrade, "cboxGrade")
        Me.cboxGrade.Name = "cboxGrade"
        '
        'chkboxShowCancel
        '
        resources.ApplyResources(Me.chkboxShowCancel, "chkboxShowCancel")
        Me.chkboxShowCancel.Name = "chkboxShowCancel"
        Me.chkboxShowCancel.UseVisualStyleBackColor = True
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'butSch
        '
        resources.ApplyResources(Me.butSch, "butSch")
        Me.butSch.Name = "butSch"
        Me.butSch.UseVisualStyleBackColor = True
        '
        'cboxSch
        '
        Me.cboxSch.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSch, "cboxSch")
        Me.cboxSch.Name = "cboxSch"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'radbutUni
        '
        resources.ApplyResources(Me.radbutUni, "radbutUni")
        Me.radbutUni.Name = "radbutUni"
        Me.radbutUni.TabStop = True
        Me.radbutUni.UseVisualStyleBackColor = True
        '
        'radbutHig
        '
        resources.ApplyResources(Me.radbutHig, "radbutHig")
        Me.radbutHig.Name = "radbutHig"
        Me.radbutHig.TabStop = True
        Me.radbutHig.UseVisualStyleBackColor = True
        '
        'radbutJun
        '
        resources.ApplyResources(Me.radbutJun, "radbutJun")
        Me.radbutJun.Name = "radbutJun"
        Me.radbutJun.TabStop = True
        Me.radbutJun.UseVisualStyleBackColor = True
        '
        'radbutPri
        '
        resources.ApplyResources(Me.radbutPri, "radbutPri")
        Me.radbutPri.Name = "radbutPri"
        Me.radbutPri.TabStop = True
        Me.radbutPri.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chkboxNotInClass)
        Me.GroupBox2.Controls.Add(Me.chkboxInClass)
        Me.GroupBox2.Controls.Add(Me.radbutType)
        Me.GroupBox2.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox2.Controls.Add(Me.cboxSubClass)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.cboxClass)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.radbutClass)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'chkboxNotInClass
        '
        resources.ApplyResources(Me.chkboxNotInClass, "chkboxNotInClass")
        Me.chkboxNotInClass.Name = "chkboxNotInClass"
        Me.chkboxNotInClass.UseVisualStyleBackColor = True
        '
        'chkboxInClass
        '
        resources.ApplyResources(Me.chkboxInClass, "chkboxInClass")
        Me.chkboxInClass.Checked = True
        Me.chkboxInClass.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxInClass.Name = "chkboxInClass"
        Me.chkboxInClass.UseVisualStyleBackColor = True
        '
        'radbutType
        '
        resources.ApplyResources(Me.radbutType, "radbutType")
        Me.radbutType.Name = "radbutType"
        Me.radbutType.TabStop = True
        Me.radbutType.UseVisualStyleBackColor = True
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'radbutClass
        '
        resources.ApplyResources(Me.radbutClass, "radbutClass")
        Me.radbutClass.Checked = True
        Me.radbutClass.Name = "radbutClass"
        Me.radbutClass.TabStop = True
        Me.radbutClass.UseVisualStyleBackColor = True
        '
        'tboxCount
        '
        resources.ApplyResources(Me.tboxCount, "tboxCount")
        Me.tboxCount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tboxCount.Name = "tboxCount"
        Me.tboxCount.ReadOnly = True
        '
        'butFilter
        '
        resources.ApplyResources(Me.butFilter, "butFilter")
        Me.butFilter.Name = "butFilter"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'frmStuSearchSchool
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.butFilter)
        Me.Controls.Add(Me.tboxCount)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmStuSearchSchool"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportPhoto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostalTag As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radbutUni As System.Windows.Forms.RadioButton
    Friend WithEvents radbutHig As System.Windows.Forms.RadioButton
    Friend WithEvents radbutJun As System.Windows.Forms.RadioButton
    Friend WithEvents radbutPri As System.Windows.Forms.RadioButton
    Friend WithEvents butSch As System.Windows.Forms.Button
    Friend WithEvents cboxSch As System.Windows.Forms.ComboBox
    Friend WithEvents cboxGrade As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tboxClass As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents radbutClass As System.Windows.Forms.RadioButton
    Friend WithEvents chkboxNotInClass As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxInClass As System.Windows.Forms.CheckBox
    Friend WithEvents radbutType As System.Windows.Forms.RadioButton
    Friend WithEvents chkboxShowCancel As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxCount As System.Windows.Forms.TextBox
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
