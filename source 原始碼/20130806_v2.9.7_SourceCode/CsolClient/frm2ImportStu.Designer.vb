﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ImportStu
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2ImportStu))
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.tboxStuYear = New System.Windows.Forms.TextBox
        Me.lblCreateDate = New System.Windows.Forms.Label
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.cboxClassID = New System.Windows.Forms.ComboBox
        Me.cboxClassInfo = New System.Windows.Forms.ComboBox
        Me.cboxMumJob = New System.Windows.Forms.ComboBox
        Me.cboxDadJob = New System.Windows.Forms.ComboBox
        Me.cboxRemarks = New System.Windows.Forms.ComboBox
        Me.cboxIntro = New System.Windows.Forms.ComboBox
        Me.cboxIC = New System.Windows.Forms.ComboBox
        Me.cboxGraduate = New System.Windows.Forms.ComboBox
        Me.cboxType = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.cboxGrade = New System.Windows.Forms.ComboBox
        Me.cboxUni = New System.Windows.Forms.ComboBox
        Me.cboxHigh = New System.Windows.Forms.ComboBox
        Me.cboxJunior = New System.Windows.Forms.ComboBox
        Me.cboxPri = New System.Windows.Forms.ComboBox
        Me.cboxEmail = New System.Windows.Forms.ComboBox
        Me.cboxAddr = New System.Windows.Forms.ComboBox
        Me.cboxPostalCode = New System.Windows.Forms.ComboBox
        Me.cboxStuMobile = New System.Windows.Forms.ComboBox
        Me.cboxMumMobile = New System.Windows.Forms.ComboBox
        Me.cboxDadMobile = New System.Windows.Forms.ComboBox
        Me.cboxMum = New System.Windows.Forms.ComboBox
        Me.cboxDad = New System.Windows.Forms.ComboBox
        Me.cboxOfficeTel = New System.Windows.Forms.ComboBox
        Me.cboxTel2 = New System.Windows.Forms.ComboBox
        Me.cboxTel1 = New System.Windows.Forms.ComboBox
        Me.cboxBirth = New System.Windows.Forms.ComboBox
        Me.cboxSex = New System.Windows.Forms.ComboBox
        Me.cboxEngName = New System.Windows.Forms.ComboBox
        Me.cboxName = New System.Windows.Forms.ComboBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label34 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboxID = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.radbutPotential = New System.Windows.Forms.RadioButton
        Me.radbutFormal = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.butImport = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.butClose = New System.Windows.Forms.Button
        Me.tboxFile = New System.Windows.Forms.TextBox
        Me.butFile = New System.Windows.Forms.Button
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.radbutUpdate = New System.Windows.Forms.RadioButton
        Me.radbutIgnore = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.butPreview = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cboxTable = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvRaw = New System.Windows.Forms.DataGridView
        Me.GroupBox6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvRaw, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.tboxStuYear)
        Me.GroupBox6.Controls.Add(Me.lblCreateDate)
        Me.GroupBox6.Controls.Add(Me.Panel1)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'tboxStuYear
        '
        resources.ApplyResources(Me.tboxStuYear, "tboxStuYear")
        Me.tboxStuYear.Name = "tboxStuYear"
        '
        'lblCreateDate
        '
        resources.ApplyResources(Me.lblCreateDate, "lblCreateDate")
        Me.lblCreateDate.Name = "lblCreateDate"
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Controls.Add(Me.cboxClassID)
        Me.Panel1.Controls.Add(Me.cboxClassInfo)
        Me.Panel1.Controls.Add(Me.cboxMumJob)
        Me.Panel1.Controls.Add(Me.cboxDadJob)
        Me.Panel1.Controls.Add(Me.cboxRemarks)
        Me.Panel1.Controls.Add(Me.cboxIntro)
        Me.Panel1.Controls.Add(Me.cboxIC)
        Me.Panel1.Controls.Add(Me.cboxGraduate)
        Me.Panel1.Controls.Add(Me.cboxType)
        Me.Panel1.Controls.Add(Me.cboxClass)
        Me.Panel1.Controls.Add(Me.cboxGrade)
        Me.Panel1.Controls.Add(Me.cboxUni)
        Me.Panel1.Controls.Add(Me.cboxHigh)
        Me.Panel1.Controls.Add(Me.cboxJunior)
        Me.Panel1.Controls.Add(Me.cboxPri)
        Me.Panel1.Controls.Add(Me.cboxEmail)
        Me.Panel1.Controls.Add(Me.cboxAddr)
        Me.Panel1.Controls.Add(Me.cboxPostalCode)
        Me.Panel1.Controls.Add(Me.cboxStuMobile)
        Me.Panel1.Controls.Add(Me.cboxMumMobile)
        Me.Panel1.Controls.Add(Me.cboxDadMobile)
        Me.Panel1.Controls.Add(Me.cboxMum)
        Me.Panel1.Controls.Add(Me.cboxDad)
        Me.Panel1.Controls.Add(Me.cboxOfficeTel)
        Me.Panel1.Controls.Add(Me.cboxTel2)
        Me.Panel1.Controls.Add(Me.cboxTel1)
        Me.Panel1.Controls.Add(Me.cboxBirth)
        Me.Panel1.Controls.Add(Me.cboxSex)
        Me.Panel1.Controls.Add(Me.cboxEngName)
        Me.Panel1.Controls.Add(Me.cboxName)
        Me.Panel1.Controls.Add(Me.Label32)
        Me.Panel1.Controls.Add(Me.Label26)
        Me.Panel1.Controls.Add(Me.Label24)
        Me.Panel1.Controls.Add(Me.Label25)
        Me.Panel1.Controls.Add(Me.Label23)
        Me.Panel1.Controls.Add(Me.Label22)
        Me.Panel1.Controls.Add(Me.Label28)
        Me.Panel1.Controls.Add(Me.Label29)
        Me.Panel1.Controls.Add(Me.Label17)
        Me.Panel1.Controls.Add(Me.Label20)
        Me.Panel1.Controls.Add(Me.Label18)
        Me.Panel1.Controls.Add(Me.Label19)
        Me.Panel1.Controls.Add(Me.Label21)
        Me.Panel1.Controls.Add(Me.Label13)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.Label10)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.Label34)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label35)
        Me.Panel1.Controls.Add(Me.Label36)
        Me.Panel1.Controls.Add(Me.Label27)
        Me.Panel1.Controls.Add(Me.Label30)
        Me.Panel1.Controls.Add(Me.Label31)
        Me.Panel1.Controls.Add(Me.Label16)
        Me.Panel1.Controls.Add(Me.Label15)
        Me.Panel1.Controls.Add(Me.Label14)
        Me.Panel1.Controls.Add(Me.Label12)
        Me.Panel1.Controls.Add(Me.Label11)
        Me.Panel1.Controls.Add(Me.cboxID)
        Me.Panel1.Controls.Add(Me.Label9)
        Me.Panel1.Name = "Panel1"
        '
        'cboxClassID
        '
        Me.cboxClassID.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClassID, "cboxClassID")
        Me.cboxClassID.Name = "cboxClassID"
        Me.cboxClassID.Tag = "班級編號"
        '
        'cboxClassInfo
        '
        Me.cboxClassInfo.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClassInfo, "cboxClassInfo")
        Me.cboxClassInfo.Name = "cboxClassInfo"
        Me.cboxClassInfo.Tag = "來班資訊"
        '
        'cboxMumJob
        '
        Me.cboxMumJob.FormattingEnabled = True
        resources.ApplyResources(Me.cboxMumJob, "cboxMumJob")
        Me.cboxMumJob.Name = "cboxMumJob"
        Me.cboxMumJob.Tag = "媽媽職業"
        '
        'cboxDadJob
        '
        Me.cboxDadJob.FormattingEnabled = True
        resources.ApplyResources(Me.cboxDadJob, "cboxDadJob")
        Me.cboxDadJob.Name = "cboxDadJob"
        Me.cboxDadJob.Tag = "爸爸職業"
        '
        'cboxRemarks
        '
        Me.cboxRemarks.FormattingEnabled = True
        resources.ApplyResources(Me.cboxRemarks, "cboxRemarks")
        Me.cboxRemarks.Name = "cboxRemarks"
        Me.cboxRemarks.Tag = "備註"
        '
        'cboxIntro
        '
        Me.cboxIntro.FormattingEnabled = True
        resources.ApplyResources(Me.cboxIntro, "cboxIntro")
        Me.cboxIntro.Name = "cboxIntro"
        Me.cboxIntro.Tag = "介紹人姓名"
        '
        'cboxIC
        '
        Me.cboxIC.FormattingEnabled = True
        resources.ApplyResources(Me.cboxIC, "cboxIC")
        Me.cboxIC.Name = "cboxIC"
        Me.cboxIC.Tag = "身分證"
        '
        'cboxGraduate
        '
        Me.cboxGraduate.FormattingEnabled = True
        resources.ApplyResources(Me.cboxGraduate, "cboxGraduate")
        Me.cboxGraduate.Name = "cboxGraduate"
        Me.cboxGraduate.Tag = "畢業國中"
        '
        'cboxType
        '
        Me.cboxType.FormattingEnabled = True
        resources.ApplyResources(Me.cboxType, "cboxType")
        Me.cboxType.Name = "cboxType"
        Me.cboxType.Tag = "學校類組"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Tag = "學校班級"
        '
        'cboxGrade
        '
        Me.cboxGrade.FormattingEnabled = True
        resources.ApplyResources(Me.cboxGrade, "cboxGrade")
        Me.cboxGrade.Name = "cboxGrade"
        Me.cboxGrade.Tag = "學校年級"
        '
        'cboxUni
        '
        Me.cboxUni.FormattingEnabled = True
        resources.ApplyResources(Me.cboxUni, "cboxUni")
        Me.cboxUni.Name = "cboxUni"
        Me.cboxUni.Tag = "大學"
        '
        'cboxHigh
        '
        Me.cboxHigh.FormattingEnabled = True
        resources.ApplyResources(Me.cboxHigh, "cboxHigh")
        Me.cboxHigh.Name = "cboxHigh"
        Me.cboxHigh.Tag = "高中"
        '
        'cboxJunior
        '
        Me.cboxJunior.FormattingEnabled = True
        resources.ApplyResources(Me.cboxJunior, "cboxJunior")
        Me.cboxJunior.Name = "cboxJunior"
        Me.cboxJunior.Tag = "國中"
        '
        'cboxPri
        '
        Me.cboxPri.FormattingEnabled = True
        resources.ApplyResources(Me.cboxPri, "cboxPri")
        Me.cboxPri.Name = "cboxPri"
        Me.cboxPri.Tag = "國小"
        '
        'cboxEmail
        '
        Me.cboxEmail.FormattingEnabled = True
        resources.ApplyResources(Me.cboxEmail, "cboxEmail")
        Me.cboxEmail.Name = "cboxEmail"
        Me.cboxEmail.Tag = "Email"
        '
        'cboxAddr
        '
        Me.cboxAddr.FormattingEnabled = True
        resources.ApplyResources(Me.cboxAddr, "cboxAddr")
        Me.cboxAddr.Name = "cboxAddr"
        Me.cboxAddr.Tag = "地址"
        '
        'cboxPostalCode
        '
        Me.cboxPostalCode.FormattingEnabled = True
        resources.ApplyResources(Me.cboxPostalCode, "cboxPostalCode")
        Me.cboxPostalCode.Name = "cboxPostalCode"
        Me.cboxPostalCode.Tag = "郵遞區號"
        '
        'cboxStuMobile
        '
        Me.cboxStuMobile.FormattingEnabled = True
        resources.ApplyResources(Me.cboxStuMobile, "cboxStuMobile")
        Me.cboxStuMobile.Name = "cboxStuMobile"
        Me.cboxStuMobile.Tag = "學生手機"
        '
        'cboxMumMobile
        '
        Me.cboxMumMobile.FormattingEnabled = True
        resources.ApplyResources(Me.cboxMumMobile, "cboxMumMobile")
        Me.cboxMumMobile.Name = "cboxMumMobile"
        Me.cboxMumMobile.Tag = "媽媽手機"
        '
        'cboxDadMobile
        '
        Me.cboxDadMobile.FormattingEnabled = True
        resources.ApplyResources(Me.cboxDadMobile, "cboxDadMobile")
        Me.cboxDadMobile.Name = "cboxDadMobile"
        Me.cboxDadMobile.Tag = "爸爸手機"
        '
        'cboxMum
        '
        Me.cboxMum.FormattingEnabled = True
        resources.ApplyResources(Me.cboxMum, "cboxMum")
        Me.cboxMum.Name = "cboxMum"
        Me.cboxMum.Tag = "媽媽姓名"
        '
        'cboxDad
        '
        Me.cboxDad.FormattingEnabled = True
        resources.ApplyResources(Me.cboxDad, "cboxDad")
        Me.cboxDad.Name = "cboxDad"
        Me.cboxDad.Tag = "爸爸姓名"
        '
        'cboxOfficeTel
        '
        Me.cboxOfficeTel.FormattingEnabled = True
        resources.ApplyResources(Me.cboxOfficeTel, "cboxOfficeTel")
        Me.cboxOfficeTel.Name = "cboxOfficeTel"
        Me.cboxOfficeTel.Tag = "公司電話"
        '
        'cboxTel2
        '
        Me.cboxTel2.FormattingEnabled = True
        resources.ApplyResources(Me.cboxTel2, "cboxTel2")
        Me.cboxTel2.Name = "cboxTel2"
        Me.cboxTel2.Tag = "電話2"
        '
        'cboxTel1
        '
        Me.cboxTel1.FormattingEnabled = True
        resources.ApplyResources(Me.cboxTel1, "cboxTel1")
        Me.cboxTel1.Name = "cboxTel1"
        Me.cboxTel1.Tag = "電話1"
        '
        'cboxBirth
        '
        Me.cboxBirth.FormattingEnabled = True
        resources.ApplyResources(Me.cboxBirth, "cboxBirth")
        Me.cboxBirth.Name = "cboxBirth"
        Me.cboxBirth.Tag = "生日"
        '
        'cboxSex
        '
        Me.cboxSex.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSex, "cboxSex")
        Me.cboxSex.Name = "cboxSex"
        Me.cboxSex.Tag = "性別"
        '
        'cboxEngName
        '
        Me.cboxEngName.FormattingEnabled = True
        resources.ApplyResources(Me.cboxEngName, "cboxEngName")
        Me.cboxEngName.Name = "cboxEngName"
        Me.cboxEngName.Tag = "英文姓名"
        '
        'cboxName
        '
        Me.cboxName.FormattingEnabled = True
        resources.ApplyResources(Me.cboxName, "cboxName")
        Me.cboxName.Name = "cboxName"
        Me.cboxName.Tag = "姓名"
        '
        'Label32
        '
        resources.ApplyResources(Me.Label32, "Label32")
        Me.Label32.Name = "Label32"
        '
        'Label26
        '
        resources.ApplyResources(Me.Label26, "Label26")
        Me.Label26.Name = "Label26"
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.Name = "Label24"
        '
        'Label25
        '
        resources.ApplyResources(Me.Label25, "Label25")
        Me.Label25.Name = "Label25"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'Label28
        '
        resources.ApplyResources(Me.Label28, "Label28")
        Me.Label28.Name = "Label28"
        '
        'Label29
        '
        resources.ApplyResources(Me.Label29, "Label29")
        Me.Label29.Name = "Label29"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'Label34
        '
        resources.ApplyResources(Me.Label34, "Label34")
        Me.Label34.Name = "Label34"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label35
        '
        resources.ApplyResources(Me.Label35, "Label35")
        Me.Label35.Name = "Label35"
        '
        'Label36
        '
        resources.ApplyResources(Me.Label36, "Label36")
        Me.Label36.Name = "Label36"
        '
        'Label27
        '
        resources.ApplyResources(Me.Label27, "Label27")
        Me.Label27.Name = "Label27"
        '
        'Label30
        '
        resources.ApplyResources(Me.Label30, "Label30")
        Me.Label30.Name = "Label30"
        '
        'Label31
        '
        resources.ApplyResources(Me.Label31, "Label31")
        Me.Label31.Name = "Label31"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'cboxID
        '
        Me.cboxID.FormattingEnabled = True
        resources.ApplyResources(Me.cboxID, "cboxID")
        Me.cboxID.Name = "cboxID"
        Me.cboxID.Tag = "學號"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'radbutPotential
        '
        resources.ApplyResources(Me.radbutPotential, "radbutPotential")
        Me.radbutPotential.Name = "radbutPotential"
        Me.radbutPotential.TabStop = True
        Me.radbutPotential.UseVisualStyleBackColor = True
        '
        'radbutFormal
        '
        resources.ApplyResources(Me.radbutFormal, "radbutFormal")
        Me.radbutFormal.Checked = True
        Me.radbutFormal.Name = "radbutFormal"
        Me.radbutFormal.TabStop = True
        Me.radbutFormal.UseVisualStyleBackColor = True
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'butImport
        '
        resources.ApplyResources(Me.butImport, "butImport")
        Me.butImport.Name = "butImport"
        Me.butImport.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.radbutPotential)
        Me.GroupBox5.Controls.Add(Me.radbutFormal)
        Me.GroupBox5.Controls.Add(Me.Label5)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'butClose
        '
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'tboxFile
        '
        resources.ApplyResources(Me.tboxFile, "tboxFile")
        Me.tboxFile.Name = "tboxFile"
        '
        'butFile
        '
        resources.ApplyResources(Me.butFile, "butFile")
        Me.butFile.Name = "butFile"
        Me.butFile.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.radbutUpdate)
        Me.GroupBox4.Controls.Add(Me.radbutIgnore)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.butPreview)
        Me.GroupBox4.Controls.Add(Me.dgv)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = ""
        '
        'radbutUpdate
        '
        resources.ApplyResources(Me.radbutUpdate, "radbutUpdate")
        Me.radbutUpdate.Name = "radbutUpdate"
        Me.radbutUpdate.TabStop = True
        Me.radbutUpdate.UseVisualStyleBackColor = True
        '
        'radbutIgnore
        '
        resources.ApplyResources(Me.radbutIgnore, "radbutIgnore")
        Me.radbutIgnore.Checked = True
        Me.radbutIgnore.Name = "radbutIgnore"
        Me.radbutIgnore.TabStop = True
        Me.radbutIgnore.UseVisualStyleBackColor = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Name = "Label1"
        '
        'butPreview
        '
        resources.ApplyResources(Me.butPreview, "butPreview")
        Me.butPreview.Name = "butPreview"
        Me.butPreview.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboxTable)
        Me.GroupBox3.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'cboxTable
        '
        Me.cboxTable.FormattingEnabled = True
        resources.ApplyResources(Me.cboxTable, "cboxTable")
        Me.cboxTable.Name = "cboxTable"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.butFile)
        Me.GroupBox2.Controls.Add(Me.tboxFile)
        Me.GroupBox2.Controls.Add(Me.Label2)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvRaw)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = ""
        '
        'dgvRaw
        '
        Me.dgvRaw.AllowUserToAddRows = False
        Me.dgvRaw.AllowUserToDeleteRows = False
        Me.dgvRaw.AllowUserToResizeRows = False
        Me.dgvRaw.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader
        Me.dgvRaw.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells
        Me.dgvRaw.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvRaw, "dgvRaw")
        Me.dgvRaw.Name = "dgvRaw"
        Me.dgvRaw.ReadOnly = True
        Me.dgvRaw.RowHeadersVisible = False
        Me.dgvRaw.RowTemplate.Height = 24
        '
        'frm2ImportStu
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.butImport)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ImportStu"
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvRaw, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutPotential As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFormal As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents butImport As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents tboxFile As System.Windows.Forms.TextBox
    Friend WithEvents butFile As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxTable As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvRaw As System.Windows.Forms.DataGridView
    Friend WithEvents radbutUpdate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutIgnore As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butPreview As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tboxStuYear As System.Windows.Forms.TextBox
    Friend WithEvents lblCreateDate As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboxID As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents cboxGraduate As System.Windows.Forms.ComboBox
    Friend WithEvents cboxType As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxGrade As System.Windows.Forms.ComboBox
    Friend WithEvents cboxUni As System.Windows.Forms.ComboBox
    Friend WithEvents cboxHigh As System.Windows.Forms.ComboBox
    Friend WithEvents cboxJunior As System.Windows.Forms.ComboBox
    Friend WithEvents cboxPri As System.Windows.Forms.ComboBox
    Friend WithEvents cboxEmail As System.Windows.Forms.ComboBox
    Friend WithEvents cboxAddr As System.Windows.Forms.ComboBox
    Friend WithEvents cboxPostalCode As System.Windows.Forms.ComboBox
    Friend WithEvents cboxStuMobile As System.Windows.Forms.ComboBox
    Friend WithEvents cboxMumMobile As System.Windows.Forms.ComboBox
    Friend WithEvents cboxDadMobile As System.Windows.Forms.ComboBox
    Friend WithEvents cboxMum As System.Windows.Forms.ComboBox
    Friend WithEvents cboxDad As System.Windows.Forms.ComboBox
    Friend WithEvents cboxOfficeTel As System.Windows.Forms.ComboBox
    Friend WithEvents cboxTel2 As System.Windows.Forms.ComboBox
    Friend WithEvents cboxTel1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboxBirth As System.Windows.Forms.ComboBox
    Friend WithEvents cboxSex As System.Windows.Forms.ComboBox
    Friend WithEvents cboxEngName As System.Windows.Forms.ComboBox
    Friend WithEvents cboxName As System.Windows.Forms.ComboBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents cboxClassID As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClassInfo As System.Windows.Forms.ComboBox
    Friend WithEvents cboxMumJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboxDadJob As System.Windows.Forms.ComboBox
    Friend WithEvents cboxRemarks As System.Windows.Forms.ComboBox
    Friend WithEvents cboxIntro As System.Windows.Forms.ComboBox
    Friend WithEvents cboxIC As System.Windows.Forms.ComboBox
End Class
