﻿Public Class frm2AddSchool
    Dim intId As Integer = -1
    Dim dtType As New DataTable
    Dim lstType As New ArrayList

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Public Sub New(ByVal ID As Integer, _
                              ByVal Name As String, ByVal TypeID As Integer, _
                           ByVal Phone As String, ByVal Address As String, _
                           ByVal PersonContact As String)

        InitializeComponent()

        InitList()

        If ID > -1 Then
            intId = ID
            Me.Text = My.Resources.modSchool
            tboxName.Text = Name
            tboxPhone.Text = Phone
            tboxContact.Text = PersonContact
            tboxAddress.Text = Address
            Dim idx As Integer = lstType.IndexOf(TypeID)
            If idx > -1 Then
                cboxType.SelectedIndex = idx
            End If
        Else
            Me.Text = My.Resources.addSchool
            If lstType.Count > 0 Then
                cboxType.SelectedIndex = 0
            End If
        End If
        frmMain.SetOkState(False)
    End Sub

    Private Sub InitList()
        dtType = frmMain.GetSchTypes

        For index As Integer = 0 To dtType.Rows.Count - 1
            cboxType.Items.Add(dtType.Rows(index).Item(c_NameColumnName).trim)
            lstType.Add(dtType.Rows(index).Item(c_IDColumnName))
        Next
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If tboxName.Text.Trim = "" Then
            ShowMsg()
            Exit Sub
        End If
        Dim intType As Integer = cboxType.SelectedIndex
        If intType < 0 Then
            intType = 0
        End If

        If intId = -1 Then
            objCsol.AddSchool(tboxName.Text.Trim, intType + 1, tboxPhone.Text.Trim, _
                        tboxAddress.Text.Trim, tboxContact.Text.Trim)
        Else
            objCsol.ModifySchool(intId, tboxName.Text.Trim, intType + 1, tboxPhone.Text.Trim, _
                        tboxAddress.Text.Trim, tboxContact.Text.Trim)
        End If
        frmMain.RefreshSchool()
        Me.Close()
    End Sub
End Class