﻿Public Class frmPunchBoard
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private dtBook As New DataTable
    Private lstBook As New ArrayList
    Private lstBookShown As New ArrayList
    Private dtSession As New DataTable
    Private lstSession As New ArrayList
    Private dtStu As New DataTable
    Dim WithEvents myComPort As New System.IO.Ports.SerialPort
    Private strCardNum As String
    Private intContent As Integer
    Private intSubClass As Integer
    Private intSession As Integer
    Private intClass As Integer
    Private strStuId As String
    Private intResult As Integer
    Private lstMulti As New ArrayList
    Private lstMultiShown As New ArrayList
    Private dtPay As New DataTable
    Private strCardReader As String

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPunchBoard
        RefreshData()
        LoadColumnText()
        strCardReader = GetConfigCardReader()
    End Sub


    Private Sub frmPunchBoard_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If strCardReader = c_CardReaderComPort Then
            If myComPort.IsOpen Then
                myComPort.Close()
                Dispose()
            End If
        End If
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPunchBoard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        dtClass = frmMain.GetClassList
        dtSubClass = frmMain.GetSubClassList
        dtContent = frmMain.GetContents
        dtSession = frmMain.GetSessions
        dtBook = frmMain.GetBooks
        dtPay = objCsol.ListPayRec
        cboxClass.Items.Clear()
        lstClass.Clear()
        For index As Integer = 0 To dtClass.Rows.Count - 1
            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        Next
        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1
            cboxBook.SelectedIndex = -1
        End If
    End Sub

    Private Sub LoadColumnText()
        'To be added
    End Sub

    Private Sub frmPunchBoard_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        cboxContent.Items.Clear()
        lstContent.Clear()
        cboxSession.Items.Clear()
        lstSession.Clear()
        If cboxSubClass.SelectedIndex > -1 Then
            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            For index As Integer = 0 To dtContent.Rows.Count - 1
                If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                    cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName).trim)
                    lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                End If
            Next
            For index As Integer = 0 To dtSession.Rows.Count - 1
                If dtSession.Rows(index).Item(c_SubClassIDColumnName) = i Then
                    cboxSession.Items.Add(GetWeekDayName(dtSession.Rows(index).Item(c_DayOfWeekColumnName), 0) & _
                                          "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_StartColumnName))) & _
                                          "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_EndColumnName))))
                    lstSession.Add(dtSession.Rows(index).Item(c_IDColumnName))
                End If
            Next
            If cboxContent.Items.Count > 0 Then
                cboxContent.SelectedIndex = 0
            End If
            If cboxSession.Items.Count > 0 Then
                cboxSession.SelectedIndex = 0
            End If
            dtStu = objCsol.ListStuBySubClass(i)
        End If
    End Sub

    Private Sub cboxBook_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxBook.SelectedIndexChanged
        If cboxBook.SelectedIndex = 1 And cboxSubClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxSubClass.SelectedIndex)
            lstBook.Clear()
            lstBookShown.Clear()
            lstMulti.Clear()
            lstMultiShown.Clear()
            For index As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(index).Item(c_SubClassIDColumnName) = i Then
                    If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDateStart(Now) Then
                        lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                        chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).trim)
                        lstMulti.Add(dtBook.Rows(index).Item(c_MultiClassColumnName))
                    End If
                End If
            Next
            chklstBook.Visible = True
        Else
            lstBook.Clear()
            lstBookShown.Clear()
            lstMultiShown.Clear()
            chklstBook.Items.Clear()
            chklstBook.Visible = False
        End If
    End Sub

    Private Sub timerClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerClock.Tick
        lblTime.Text = GetTimeNumNow()
    End Sub

    Private Sub butStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butStart.Click
        timerClock.Start()
        'And cboxSession.SelectedIndex > -1 
        'cboxContent.SelectedIndex > -1 And _
        If cboxSubClass.SelectedIndex > -1 And _
            cboxClass.SelectedIndex > -1 Then
            intSubClass = lstSubClass(cboxSubClass.SelectedIndex)
            intClass = lstClass(cboxClass.SelectedIndex)
            If cboxContent.SelectedIndex > -1 Then
                intContent = lstContent(cboxContent.SelectedIndex)
            Else
                intContent = -1
            End If
            If cboxSession.SelectedIndex > -1 Then
                intSession = lstSession(cboxSession.SelectedIndex)
            Else
                intSession = -1
            End If
            If strCardReader = c_CardReaderComPort Then
                If Not My.Computer.Ports.SerialPortNames.Count = 0 Then
                    Try
                        If Not myComPort.IsOpen Then
                            myComPort.BaudRate = 2400
                            myComPort.PortName = My.Computer.Ports.SerialPortNames(0)
                            myComPort.Parity = IO.Ports.Parity.None
                            myComPort.DataBits = 8
                            myComPort.StopBits = IO.Ports.StopBits.One
                            myComPort.Handshake = IO.Ports.Handshake.None
                            myComPort.ReadTimeout = 3000
                            myComPort.ReceivedBytesThreshold = 1
                            myComPort.DtrEnable = True

                            myComPort.Open()
                            If radbutBeginClass.Checked Then
                                lblName.Text = My.Resources.classInPunchStart
                            ElseIf radbutEndClass.Checked Then
                                lblName.Text = My.Resources.classOutPunchStart
                            End If
                        End If
                    Catch ex As Exception
                        MsgBox(My.Resources.msgComportError, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                    End Try
                Else
                    MsgBox("Select a valid COM Port", MsgBoxStyle.Information)
                End If
            ElseIf strCardReader = c_CardReaderUsb Then
                MsgBox(My.Resources.msgCheckInputLang, MsgBoxStyle.Information)
                If radbutBeginClass.Checked Then
                    lblName.Text = My.Resources.classInPunchStart
                ElseIf radbutEndClass.Checked Then
                    lblName.Text = My.Resources.classOutPunchStart
                End If
                tboxCardNum.ReadOnly = False
                tboxCardNum.Focus()
            End If

        End If
    End Sub

    Private Sub chklstBook_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstBook.ItemCheck
        Dim intC As Integer
        Dim intM As Integer
        Dim i As Integer
        intC = lstBookShown(e.Index)
        intM = lstMultiShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstBook.Add(intC)
            lstMulti.Add(intM)
        Else
            i = lstBook.IndexOf(intC)
            If i > -1 Then
                lstBook.RemoveAt(i)
                lstMulti.RemoveAt(i)
            End If
        End If
    End Sub

    Private Sub butEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEnd.Click
        If radbutBeginClass.Checked Then
            lblName.Text = My.Resources.classInPunchEnd
        ElseIf radbutEndClass.Checked Then
            lblName.Text = My.Resources.classOutPunchEnd
        End If
        tboxID.Text = ""
        lblSeatNum.Text = ""
        tboxSeatNum.Text = ""
        lblMsg.Text = ""
        tboxStatus.Text = ""
        If strCardReader = c_CardReaderComPort Then
            If myComPort.IsOpen Then
                myComPort.Close()
                myComPort.Dispose()
            End If
        ElseIf strCardReader = c_CardReaderUsb Then
            tboxCardNum.ReadOnly = True
        End If
    End Sub

    Private Sub DataReceived(ByVal sender As Object, ByVal e As System.IO.Ports.SerialDataReceivedEventArgs) Handles myComPort.DataReceived
        tboxID.Invoke(New myDelegate(AddressOf updateInfo), New Object() {})
    End Sub
    Public Delegate Sub myDelegate()
    Public Sub updateInfo()
        Dim buffer As Byte() = New Byte(10) {}
        Dim dtNote As New DataTable
        Dim ds As New DataSet
        Dim dis As Byte
        Dim time As Integer
        Dim content As String = ""
        myComPort.Read(buffer, 0, 10)
        strCardNum = buffer(0).ToString & buffer(1).ToString & buffer(2).ToString
        ds = objCsol.GetPunchData(strCardNum, intClass)
        dtStu = ds.Tables(c_StuInfoTableName)
        dtNote = ds.Tables(c_StuNoteDataTableName)

        If dtStu.Rows.Count > 0 Then
            strStuId = dtStu.Rows(0).Item(c_IDColumnName)
            tboxID.Text = strStuId
            lblName.Text = dtStu.Rows(0).Item(c_NameColumnName).trim
            lblSeatNum.Text = dtStu.Rows(0).Item(c_SeatNumColumnName)
            tboxSeatNum.Text = dtStu.Rows(0).Item(c_SeatNumColumnName)
            If strStuId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                End If
            End If

            myComPort.DtrEnable = False
            Beep()

            Dim timeOut As DateTimeOffset = Now.AddMilliseconds(1500)
            Do
                Application.DoEvents()
            Loop Until Now > timeOut
            myComPort.DiscardInBuffer()
            myComPort.DtrEnable = True
            If dtNote.Rows.Count > 0 Then
                dis = dtNote.Rows(0).Item(c_StyleColumnName)
                time = dtNote.Rows(0).Item(c_DisplayForColumnName)
                content = dtNote.Rows(0).Item(c_ContentColumnName).trim
                If content.Length > 0 Then
                    Dim frm As New frm2NoteDisplay(dis, time, content, _
                                    dtStu.Rows(0).Item(c_NameColumnName).trim)
                    frm.MdiParent = frmMain
                    frm.Show()
                End If
            End If

            If cboxBook.SelectedIndex = 1 Then
                Dim i As Integer = CheckIssue()
                If i = 0 Then
                    i = objCsol.AddBookIssueRec(strStuId, _
                     intClass, lstBook, Now, lstMulti)
                End If
                ShowMsg(i)
            End If
            If radbutBeginClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassIn)
            ElseIf radbutEndClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassOut)
            End If


        End If
    End Sub

    Private Sub UpdateInforUsb()
        Dim dtNote As New DataTable
        Dim ds As New DataSet
        Dim dis As Byte
        Dim time As Integer
        Dim content As String = ""
        strCardNum = tboxCardNum.Text.Trim
        ds = objCsol.GetPunchData(strCardNum, intClass)
        dtStu = ds.Tables(c_StuInfoTableName)
        dtNote = ds.Tables(c_StuNoteDataTableName)

        If dtStu.Rows.Count > 0 Then
            strStuId = dtStu.Rows(0).Item(c_IDColumnName)
            tboxID.Text = strStuId
            lblName.Text = dtStu.Rows(0).Item(c_NameColumnName).trim
            lblSeatNum.Text = dtStu.Rows(0).Item(c_SeatNumColumnName)
            tboxSeatNum.Text = dtStu.Rows(0).Item(c_SeatNumColumnName)
            If strStuId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                End If
            End If

            tboxCardNum.Clear()

            If dtNote.Rows.Count > 0 Then
                dis = dtNote.Rows(0).Item(c_StyleColumnName)
                time = dtNote.Rows(0).Item(c_DisplayForColumnName)
                content = dtNote.Rows(0).Item(c_ContentColumnName).trim
                If content.Length > 0 Then
                    Dim frm As New frm2NoteDisplay(dis, time, content, _
                                    dtStu.Rows(0).Item(c_NameColumnName).trim)
                    frm.MdiParent = frmMain
                    frm.Show()
                End If
            End If

            If cboxBook.SelectedIndex = 1 Then
                Dim i As Integer = CheckIssue()
                If i = 0 Then
                    i = objCsol.AddBookIssueRec(strStuId, intClass, lstBook, Now, lstMulti)
                End If
                ShowMsg(i)
            End If
            If radbutBeginClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassIn)
            ElseIf radbutEndClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassOut)
            End If


        End If
    End Sub

    Private Sub ShowMsg(ByVal i As Integer)
        Select Case i
            Case 0
                tboxStatus.Text = My.Resources.bookIssueOk
            Case 1
                tboxStatus.Text = My.Resources.msgBookIssue1
            Case 2
                tboxStatus.Text = My.Resources.msgBookIssue2
            Case 3
                tboxStatus.Text = My.Resources.msgBookIssue3
            Case 4
                tboxStatus.Text = My.Resources.msgBookIssue4
            Case 5
                tboxStatus.Text = My.Resources.msgBookIssue5
        End Select
    End Sub

    Private Function CheckIssue() As Integer
        Dim r As Integer = 0
        Dim book As Integer
        Dim FeeClear As Byte
        Dim FeeClearDate As Date
        Dim MultiClass As Byte
        Dim CreateDate As Date
        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        Dim c2 As String = c_StuIDColumnName & "='"
        Dim c3 As String = c_SubClassIDColumnName & "="
        Dim c4 As String = c_DateTimeColumnName & "> '"

        If dtStu.Rows.Count = 0 Then
            r = 5
            Return r
        End If
        For index As Integer = 0 To lstBook.Count - 1
            book = lstBook(index)
            Dim idx As Integer = -1
            For j As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(j).Item(c_IDColumnName) = book Then
                    idx = j
                    Exit For
                End If
            Next
            If idx > -1 Then
                FeeClear = dtBook.Rows(idx).Item(c_FeeClearColumnName)
                FeeClearDate = dtBook.Rows(idx).Item(c_FeeClearDateColumnName)
                MultiClass = dtBook.Rows(idx).Item(c_MultiClassColumnName)
                CreateDate = dtBook.Rows(idx).Item(c_CreateDateColumnName)
            Else
                r = 5
                Exit For
            End If
            If CreateDate.Year <> 1911 Then
                If dtStu.Rows(0).Item(c_RegDateColumnName) > CreateDate Then
                    r = 1
                    Exit For
                End If
            End If

            If FeeClear = 0 Then
                If FeeClearDate.Year <> 1911 Then

                    If dtPay.Compute(c1, c2 & strStuId & "' AND " & c3 & _
                                     intSubClass.ToString & " AND " & c4 & _
                                        FeeClearDate.ToString & "'") > 0 Then
                        r = 3
                        Exit For
                    End If
                End If
                If dtStu.Rows(0).Item(c_FeeOweColumnName) > 0 Then
                    r = 2
                    Exit For
                End If
            End If
        Next
        Return r
    End Function

    Private Sub tboxCardNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxCardNum.TextChanged
        If tboxCardNum.Text.Trim.Length = 10 Then
            UpdateInforUsb()
        End If
    End Sub
End Class