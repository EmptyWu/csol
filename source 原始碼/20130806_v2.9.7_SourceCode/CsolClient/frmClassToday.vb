﻿Public Class frmClassToday
    Private dtClass As New DataTable
    Private intWd As Integer

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmClassToday

        RefreshData()

    End Sub

    Private Sub frmClassToday_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmClassToday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intH As Integer

        intWd = dtpickDate.Value.DayOfWeek
        dtClass = objCsol.GetClassToday(GetDateEnd(dtpickDate.Value))
        For index As Integer = 0 To dtClass.Rows.Count - 1
            intH = CDate(dtClass.Rows(index).Item(c_StartColumnName)).Hour
            If intH < 12 Then
                dtClass.Rows(index).Item(c_SlotColumnName) = My.Resources.morning
            ElseIf intH >= 12 And intH < 18 Then
                dtClass.Rows(index).Item(c_SlotColumnName) = My.Resources.noon
            ElseIf intH >= 18 Then
                dtClass.Rows(index).Item(c_SlotColumnName) = My.Resources.evening
            End If
        Next
        dtClass.Columns.Add(c_DayOfWeekNameColumnName, GetType(System.String))
        Dim wd As Integer = 0
        For Each row As DataRow In dtClass.Rows
            wd = CInt(row.Item(c_DayOfWeekColumnName).ToString)
            row.Item(c_DayOfWeekNameColumnName) = GetWeekDayName(wd, 0)
        Next

        dtClass.DefaultView.RowFilter = c_DayOfWeekColumnName & "=" & _
                intWd.ToString
        dgvSta.DataSource = dtClass.DefaultView
        dgvSta.Columns(c_StartColumnName).DefaultCellStyle.Format = "t"
        dgvSta.Columns(c_EndColumnName).DefaultCellStyle.Format = "t"
        LoadColumnText()
    End Sub

    Private Sub InitTable()

    End Sub

    Private Sub LoadColumnText()
        dgvSta.Columns.Item(c_SlotColumnName).DisplayIndex = 0
        dgvSta.Columns.Item(c_ClassNameColumnName).DisplayIndex = 1
        dgvSta.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
        dgvSta.Columns.Item(c_DayOfWeekNameColumnName).DisplayIndex = 3
        dgvSta.Columns.Item(c_StartColumnName).DisplayIndex = 4
        dgvSta.Columns.Item(c_EndColumnName).DisplayIndex = 5
        dgvSta.Columns.Item(c_ClassRoomNameColumnName).DisplayIndex = 6
        dgvSta.Columns.Item(c_SlotColumnName).HeaderText = My.Resources.timeSession
        dgvSta.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
        dgvSta.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
        dgvSta.Columns.Item(c_DayOfWeekColumnName).Visible = False
        dgvSta.Columns.Item(c_DayOfWeekNameColumnName).HeaderText = My.Resources.weekDay
        dgvSta.Columns.Item(c_StartColumnName).HeaderText = My.Resources.sessionStart
        dgvSta.Columns.Item(c_EndColumnName).HeaderText = My.Resources.sessionEnd
        dgvSta.Columns.Item(c_ClassRoomNameColumnName).HeaderText = My.Resources.classRoomName
    End Sub


    Private Sub InitSelections()

    End Sub

    Private Sub frmClassToday_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub
End Class