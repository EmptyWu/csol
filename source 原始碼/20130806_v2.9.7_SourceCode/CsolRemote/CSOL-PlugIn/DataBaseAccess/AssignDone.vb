﻿Namespace DataBaseAccess
    Public Class AssignDone
        Public Shared Function GetClassAssignmentList(ByVal ClassID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrClassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT DISTINCT  EP.ID, EP.Name, PC.SubClassID, EP.EndDate ")
            sq.Append("FROM             Assignment AS EP INNER JOIN ")
            sq.Append("AssignmentOfClass AS PC ON EP.ID = PC.AssignmentID inner join ")
            sq.Append("SubClass as SC on PC.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE            SC.ClassID = '{0}' ", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetStuAssignByAssignId(ByVal assignID As Integer, ByVal sc As Integer) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT DISTINCT  SI.ID, SI.Name, ST.SeatNum, SC.Name as SubClassName, ")
            sq.Append("(SELECT COUNT(ID) FROM StuAssignment ")
            sq.Append("WHERE(AssignmentID = SG.AssignmentID) and StuID = SI.ID) AS Mark, ")
            sq.Append("SG.AssignmentID, ST.SubClassID, SI.School,si.EngName,si.Tel1,si.Tel2, ")
            sq.Append("si.Email,si.GraduateFrom,si.DadName,si.MumName,si.DadMobile,si.MumMobile, ")
            sq.Append("SI.IntroID,SI.IntroName,si.SchoolGrade,si.SchoolClass , SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sq.Append("FROM StuClass As ST  ")
            sq.Append("inner join SubClass As SC on ST.SubClassID = SC.ID ")
            sq.Append("inner join StuInfo as SI on ST.StuID = SI.ID ")
            sq.Append("left outer join	StuAssignment as SG on ST.StuID = ST.StuID  ")

            sq.AppendFormat("AND SG.AssignmentID =  '{0}' ", assignID)
            sq.AppendFormat("WHERE ST.SubClassID =  '{0}'  ", sc)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function
    End Class
End Namespace
