﻿Namespace HTTPHandlers
    Public Class StuOweSta
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "ListStuOweByClass"
                    ListStuOweByClass()
                Case "ListStuDiscByClass"
                    ListStuDiscByClass()
                Case "ListStuBySubClass"
                    ListStuBySubClass()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/StuOweSta"
            End Get
        End Property
        Private Sub ListStuBySubClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuBySubClass As DataTable = DataBaseAccess.StuOweSta.ListStuBySubClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuBySubClass", CSOL.Convert.DataTableToXmlString(ListStuBySubClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuBySubClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub ListStuDiscByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuDiscByClass As DataTable = DataBaseAccess.StuOweSta.ListStuDiscByClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuDiscByClass", CSOL.Convert.DataTableToXmlString(ListStuDiscByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuDiscByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub ListStuOweByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuOweByClass As DataTable = DataBaseAccess.StuOweSta.ListStuOweByClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuOweByClass", CSOL.Convert.DataTableToXmlString(ListStuOweByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuOweByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

    End Class
End Namespace
