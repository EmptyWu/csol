﻿Namespace DataBaseAccess
    Public Class ClassInfo
        Public Shared Function GetBookListByUsr2(ByVal ClassName As String) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.AppendLine("Select ")
            sq.AppendLine("BK.ID, BK.Name, BK.Stock, BK.Price, BK.BeforeDate, BK.FeeClear, ")
            sq.AppendLine("BK.FeeClearDate, BK.MultiClass, BK.CreateDate, BC.SubClassID, ")
            sq.AppendLine("CL.ID AS ClassID, ")
            sq.AppendLine("(SELECT COUNT(ID) FROM BookIssueRec WHERE (BookID = BK.ID)) AS IssuedCnt ")
            sq.AppendLine("FROM Book AS BK INNER JOIN ")
            sq.AppendLine("BookOfClass AS BC ON BK.ID = BC.BookID LEFT OUTER JOIN ")
            sq.AppendLine("SubClass AS SC ON BC.SubClassID = SC.ID INNER JOIN ")
            sq.AppendLine("Class AS CL ON SC.ClassID = CL.ID")
            sq.AppendFormat("WHERE CL.Name = '{0}' ", ClassName)


            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function

        

        Public Shared Function GetBookListByUsr(ByVal usrId As String) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.AppendLine("Select ")
            sq.AppendLine("BK.ID, BK.Name, BK.Stock, BK.Price, BK.BeforeDate, BK.FeeClear, ")
            sq.AppendLine("BK.FeeClearDate, BK.MultiClass, BK.CreateDate, BC.SubClassID, ")
            sq.AppendLine("CL.ID AS ClassID, ")
            sq.AppendLine("(SELECT COUNT(ID) FROM BookIssueRec WHERE (BookID = BK.ID)) AS IssuedCnt ")
            sq.AppendLine("FROM Book AS BK INNER JOIN ")
            sq.AppendLine("BookOfClass AS BC ON BK.ID = BC.BookID LEFT OUTER JOIN ")
            sq.AppendLine("SubClass AS SC ON BC.SubClassID = SC.ID INNER JOIN ")
            sq.AppendLine("Class AS CL ON SC.ClassID = CL.ID")
            'sq.AppendFormat("WHERE BK.ID = {0} ", BookId)


            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function
    End Class

End Namespace