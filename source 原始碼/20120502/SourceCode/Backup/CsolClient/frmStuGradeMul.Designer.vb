﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuGradeMul
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.linklblAddPaper = New System.Windows.Forms.LinkLabel
        Me.chklstPaper = New System.Windows.Forms.CheckedListBox
        Me.mnuNewPaper = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.dtPickInput = New System.Windows.Forms.DateTimePicker
        Me.radbutSelect = New System.Windows.Forms.RadioButton
        Me.radbutNow = New System.Windows.Forms.RadioButton
        Me.radbutEngName = New System.Windows.Forms.RadioButton
        Me.radbutChineseName = New System.Windows.Forms.RadioButton
        Me.cboxSchool = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.chkboxShowRemarks = New System.Windows.Forms.CheckBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(6, 0)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPast.TabIndex = 16
        Me.chkboxShowPast.Text = "顯示過時"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.linklblAddPaper)
        Me.GroupBox3.Controls.Add(Me.chklstPaper)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 125)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(237, 364)
        Me.GroupBox3.TabIndex = 73
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "試卷"
        '
        'linklblAddPaper
        '
        Me.linklblAddPaper.AutoSize = True
        Me.linklblAddPaper.Location = New System.Drawing.Point(160, 2)
        Me.linklblAddPaper.Name = "linklblAddPaper"
        Me.linklblAddPaper.Size = New System.Drawing.Size(53, 12)
        Me.linklblAddPaper.TabIndex = 19
        Me.linklblAddPaper.TabStop = True
        Me.linklblAddPaper.Text = "新增試卷"
        '
        'chklstPaper
        '
        Me.chklstPaper.CheckOnClick = True
        Me.chklstPaper.FormattingEnabled = True
        Me.chklstPaper.Location = New System.Drawing.Point(3, 17)
        Me.chklstPaper.Name = "chklstPaper"
        Me.chklstPaper.Size = New System.Drawing.Size(232, 344)
        Me.chklstPaper.TabIndex = 17
        '
        'mnuNewPaper
        '
        Me.mnuNewPaper.Name = "mnuNewPaper"
        Me.mnuNewPaper.Size = New System.Drawing.Size(68, 20)
        Me.mnuNewPaper.Text = "新增試卷"
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        Me.mnuSearch.Size = New System.Drawing.Size(68, 20)
        Me.mnuSearch.Text = "搜尋學生"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.cboxSubClass)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cboxClass)
        Me.GroupBox1.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 31)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(237, 88)
        Me.GroupBox1.TabIndex = 63
        Me.GroupBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(3, 55)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 12)
        Me.Label3.TabIndex = 113
        Me.Label3.Text = "班別:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Location = New System.Drawing.Point(34, 51)
        Me.cboxSubClass.Name = "cboxSubClass"
        Me.cboxSubClass.Size = New System.Drawing.Size(201, 20)
        Me.cboxSubClass.TabIndex = 114
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(3, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 12)
        Me.Label1.TabIndex = 111
        Me.Label1.Text = "班級:"
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(34, 21)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(201, 20)
        Me.cboxClass.TabIndex = 112
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNewPaper, Me.mnuSearch, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 62
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dtPickInput)
        Me.GroupBox5.Controls.Add(Me.radbutSelect)
        Me.GroupBox5.Controls.Add(Me.radbutNow)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 495)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(229, 108)
        Me.GroupBox5.TabIndex = 130
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "輸入日期"
        '
        'dtPickInput
        '
        Me.dtPickInput.Location = New System.Drawing.Point(29, 69)
        Me.dtPickInput.Name = "dtPickInput"
        Me.dtPickInput.Size = New System.Drawing.Size(149, 22)
        Me.dtPickInput.TabIndex = 2
        '
        'radbutSelect
        '
        Me.radbutSelect.AutoSize = True
        Me.radbutSelect.Location = New System.Drawing.Point(12, 45)
        Me.radbutSelect.Name = "radbutSelect"
        Me.radbutSelect.Size = New System.Drawing.Size(95, 16)
        Me.radbutSelect.TabIndex = 1
        Me.radbutSelect.Text = "使用這個日期"
        Me.radbutSelect.UseVisualStyleBackColor = True
        '
        'radbutNow
        '
        Me.radbutNow.AutoSize = True
        Me.radbutNow.Checked = True
        Me.radbutNow.Location = New System.Drawing.Point(11, 21)
        Me.radbutNow.Name = "radbutNow"
        Me.radbutNow.Size = New System.Drawing.Size(155, 16)
        Me.radbutNow.TabIndex = 0
        Me.radbutNow.TabStop = True
        Me.radbutNow.Text = "使用這台電腦現在的日期"
        Me.radbutNow.UseVisualStyleBackColor = True
        '
        'radbutEngName
        '
        Me.radbutEngName.AutoSize = True
        Me.radbutEngName.Location = New System.Drawing.Point(356, 36)
        Me.radbutEngName.Name = "radbutEngName"
        Me.radbutEngName.Size = New System.Drawing.Size(95, 16)
        Me.radbutEngName.TabIndex = 132
        Me.radbutEngName.Text = "顯示英文姓名"
        Me.radbutEngName.UseVisualStyleBackColor = True
        '
        'radbutChineseName
        '
        Me.radbutChineseName.AutoSize = True
        Me.radbutChineseName.Checked = True
        Me.radbutChineseName.Location = New System.Drawing.Point(255, 36)
        Me.radbutChineseName.Name = "radbutChineseName"
        Me.radbutChineseName.Size = New System.Drawing.Size(95, 16)
        Me.radbutChineseName.TabIndex = 131
        Me.radbutChineseName.TabStop = True
        Me.radbutChineseName.Text = "顯示中文姓名"
        Me.radbutChineseName.UseVisualStyleBackColor = True
        '
        'cboxSchool
        '
        Me.cboxSchool.FormattingEnabled = True
        Me.cboxSchool.Location = New System.Drawing.Point(500, 34)
        Me.cboxSchool.Name = "cboxSchool"
        Me.cboxSchool.Size = New System.Drawing.Size(206, 20)
        Me.cboxSchool.TabIndex = 134
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(465, 39)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 133
        Me.Label4.Text = "學校: "
        '
        'chkboxShowRemarks
        '
        Me.chkboxShowRemarks.AutoSize = True
        Me.chkboxShowRemarks.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowRemarks.Location = New System.Drawing.Point(723, 36)
        Me.chkboxShowRemarks.Name = "chkboxShowRemarks"
        Me.chkboxShowRemarks.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowRemarks.TabIndex = 135
        Me.chkboxShowRemarks.Text = "顯示備註"
        Me.chkboxShowRemarks.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(254, 60)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.Size = New System.Drawing.Size(768, 543)
        Me.dgv.TabIndex = 136
        '
        'frmStuGradeMul
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkboxShowRemarks)
        Me.Controls.Add(Me.cboxSchool)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.radbutEngName)
        Me.Controls.Add(Me.radbutChineseName)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmStuGradeMul"
        Me.Text = "輸入學生成績(多張)"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstPaper As System.Windows.Forms.CheckedListBox
    Friend WithEvents mnuNewPaper As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents linklblAddPaper As System.Windows.Forms.LinkLabel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPickInput As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutSelect As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNow As System.Windows.Forms.RadioButton
    Friend WithEvents radbutEngName As System.Windows.Forms.RadioButton
    Friend WithEvents radbutChineseName As System.Windows.Forms.RadioButton
    Friend WithEvents cboxSchool As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowRemarks As System.Windows.Forms.CheckBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
End Class
