﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetClassRoomStyle
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SetClassRoomStyle))
        Me.radbutstyle1 = New System.Windows.Forms.RadioButton
        Me.radbutstyle3 = New System.Windows.Forms.RadioButton
        Me.radbutstyle2 = New System.Windows.Forms.RadioButton
        Me.butCancel = New System.Windows.Forms.Button
        Me.butConfirm = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'radbutstyle1
        '
        resources.ApplyResources(Me.radbutstyle1, "radbutstyle1")
        Me.radbutstyle1.Checked = True
        Me.radbutstyle1.Name = "radbutstyle1"
        Me.radbutstyle1.TabStop = True
        Me.radbutstyle1.UseVisualStyleBackColor = True
        '
        'radbutstyle3
        '
        resources.ApplyResources(Me.radbutstyle3, "radbutstyle3")
        Me.radbutstyle3.Name = "radbutstyle3"
        Me.radbutstyle3.UseVisualStyleBackColor = True
        '
        'radbutstyle2
        '
        resources.ApplyResources(Me.radbutstyle2, "radbutstyle2")
        Me.radbutstyle2.Name = "radbutstyle2"
        Me.radbutstyle2.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'frm2SetClassRoomStyle
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.radbutstyle2)
        Me.Controls.Add(Me.radbutstyle3)
        Me.Controls.Add(Me.radbutstyle1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SetClassRoomStyle"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents radbutstyle1 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutstyle3 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutstyle2 As System.Windows.Forms.RadioButton
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butConfirm As System.Windows.Forms.Button
End Class
