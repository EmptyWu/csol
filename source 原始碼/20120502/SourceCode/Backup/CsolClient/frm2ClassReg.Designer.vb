﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ClassReg
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2ClassReg))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chklstClass = New System.Windows.Forms.CheckedListBox
        Me.chkboxPast = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.chkboxOpened = New System.Windows.Forms.CheckBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lstboxSales = New System.Windows.Forms.ListBox
        Me.chkboxOpenNot = New System.Windows.Forms.CheckBox
        Me.chkboxShowSeat = New System.Windows.Forms.CheckBox
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chklstClass)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'chklstClass
        '
        Me.chklstClass.CheckOnClick = True
        Me.chklstClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstClass, "chklstClass")
        Me.chklstClass.Name = "chklstClass"
        '
        'chkboxPast
        '
        resources.ApplyResources(Me.chkboxPast, "chkboxPast")
        Me.chkboxPast.Name = "chkboxPast"
        Me.chkboxPast.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.chklstSubClass)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstSubClass, "chklstSubClass")
        Me.chklstSubClass.Name = "chklstSubClass"
        '
        'chkboxOpened
        '
        resources.ApplyResources(Me.chkboxOpened, "chkboxOpened")
        Me.chkboxOpened.Checked = True
        Me.chkboxOpened.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxOpened.Name = "chkboxOpened"
        Me.chkboxOpened.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lstboxSales)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'lstboxSales
        '
        Me.lstboxSales.FormattingEnabled = True
        resources.ApplyResources(Me.lstboxSales, "lstboxSales")
        Me.lstboxSales.Name = "lstboxSales"
        '
        'chkboxOpenNot
        '
        resources.ApplyResources(Me.chkboxOpenNot, "chkboxOpenNot")
        Me.chkboxOpenNot.Checked = True
        Me.chkboxOpenNot.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxOpenNot.Name = "chkboxOpenNot"
        Me.chkboxOpenNot.UseVisualStyleBackColor = True
        '
        'chkboxShowSeat
        '
        resources.ApplyResources(Me.chkboxShowSeat, "chkboxShowSeat")
        Me.chkboxShowSeat.Checked = True
        Me.chkboxShowSeat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxShowSeat.Name = "chkboxShowSeat"
        Me.chkboxShowSeat.UseVisualStyleBackColor = True
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2ClassReg
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.chkboxShowSeat)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.chkboxOpenNot)
        Me.Controls.Add(Me.chkboxOpened)
        Me.Controls.Add(Me.chkboxPast)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ClassReg"
        Me.ShowInTaskbar = False
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkboxPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxOpened As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxOpenNot As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowSeat As System.Windows.Forms.CheckBox
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents lstboxSales As System.Windows.Forms.ListBox
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
End Class
