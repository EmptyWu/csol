﻿Public Class frm2Subjects
    Private dtSbj As New DataTable

    Public Sub New()

        InitializeComponent()

        RefreshData()

    End Sub

    Private Sub RefreshData()
        dtSbj = objCsol.ListSubjects

        dgvS.DataSource = dtSbj
        loadcolumntext()
    End Sub

    Private Sub loadcolumntext()
        dgvS.Columns.Item(c_SubjectColumnName).HeaderText = My.Resources.subject

    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        objCsol.UpdateSubjects(CType(dgvS.DataSource, DataTable).GetChanges)
        frmMain.RefreshClassRegInfo()
        Me.Close()
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If dgvS.SelectedRows.Count > 0 Then
            dgvS.Rows.Remove(dgvS.SelectedRows(0))
        End If
    End Sub
End Class