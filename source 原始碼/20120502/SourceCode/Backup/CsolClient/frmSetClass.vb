﻿Public Class frmSetClass
    Private dsClass As New DataSet
    Private dtClassList As New DataTable
    Private dtSubClassList As New DataTable
    Private dtSessionList As New DataTable
    Private dtSessionList2 As New DataTable
    Private dtClassList2 As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmSetClass
    Private sUserName As String = frmMain.GetUsrName
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private intSelId As Integer = -1

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSetClass

        RefreshData()

    End Sub

    Public Sub SearchClass(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'Name
                For index As Integer = intSearchIndex To dgvClass.Rows.Count - 1
                    If dgvClass.Rows(index).Cells(c_NameColumnName).Value.ToString.Contains(strKw) Then
                        intSearchIndex = index
                        dgvClass.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub frmSetClass_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetClass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Try
            dsClass = objCsol.ListClasses(frmMain.GetUsrId)
            dtClassList = dsClass.Tables(c_ClassListDataTableName)
            dtSubClassList = dsClass.Tables(c_SubClassListDataTableName)
            dtSessionList2 = dsClass.Tables(c_SessionListDataTableName)
            dtClassList2 = dtClassList.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, _
                                                           c_StartColumnName, c_EndColumnName, c_FeeColumnName)
            dtSessionList = dtSessionList2.DefaultView.ToTable(True, c_IDColumnName, c_SubClassIDColumnName, _
                                                                 c_DayOfWeekColumnName, _
                                                           c_StartColumnName, c_EndColumnName)
            dtClassList2.Columns.Add(c_StuRegCntColumnName, GetType(System.Int32))
            dtClassList2.Columns.Add(c_CancelCntolumnName, GetType(System.Int32))
            Dim t1 As Integer = 0
            Dim t2 As Integer = 0
            For Each row As DataRow In dtClassList2.Rows
                t1 = 0
                t2 = 0
                For Each row2 As DataRow In dtClassList.Rows
                    If row.Item(c_IDColumnName) = row2.Item(c_IDColumnName) Then
                        t1 = t1 + row2.Item(c_StuRegCntColumnName)
                        t2 = t2 + row2.Item(c_CancelCntolumnName)
                    End If
                Next
                row.Item(c_StuRegCntColumnName) = t1
                row.Item(c_CancelCntolumnName) = t2
            Next

            dtSessionList.Columns.Add(c_DayOfWeekNameColumnName, GetType(System.String))
            Dim wd As Integer = 0
            For Each row As DataRow In dtSessionList.Rows
                wd = CInt(row.Item(c_DayOfWeekColumnName).ToString)
                row.Item(c_DayOfWeekNameColumnName) = GetWeekDayName(wd, 0)
            Next

            If dtClassList2.Rows.Count > 0 Then
                If chkboxShowPast.Checked Then
                    dtClassList2.DefaultView.RowFilter = ""
                Else
                    dtClassList2.DefaultView.RowFilter = c_EndColumnName & _
                        ">='" & Now.Date.ToString & "'"
                End If
                dgvClass.DataSource = dtClassList2

                If dgvClass.RowCount > 0 Then
                    If intSelId > -1 Then
                        For Each row As DataGridViewRow In dgvClass.Rows
                            If Not row.Cells(c_IDColumnName).Value Is Nothing Then
                                If CInt(row.Cells(c_IDColumnName).Value) = intSelId Then
                                    dgvClass.CurrentCell = row.Cells(c_NameColumnName)
                                    dtSubClassList.DefaultView.RowFilter = c_ClassIDColumnName & _
                                    "=" & row.Cells(c_IDColumnName).Value.ToString
                                    Exit For
                                End If
                            End If
                        Next
                    Else
                        dgvClass.CurrentCell = dgvClass.Rows.Item(0).Cells(c_NameColumnName)
                        dtSubClassList.DefaultView.RowFilter = c_ClassIDColumnName & _
                        "=" & dgvClass.Rows.Item(0).Cells(c_IDColumnName).Value.ToString
                    End If
                   
                    dgvSubClass.DataSource = dtSubClassList
                End If

                If dgvSubClass.RowCount > 0 Then
                    dgvSubClass.CurrentCell = dgvSubClass.Rows.Item(0).Cells(c_NameColumnName)
                    dtSessionList.DefaultView.RowFilter = c_SubClassIDColumnName & _
                    "=" & dgvSubClass.Rows.Item(0).Cells(c_IDColumnName).Value.ToString
                    dgvSession.DataSource = dtSessionList
                End If
            Else
                dgvClass.DataSource = dtClassList2
                dgvSubClass.DataSource = dtSubClassList
                dgvSession.DataSource = dtSessionList
            End If
            LoadColumnText()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadColumnText()
        If dgvClass.Columns.Count = 0 Then
            Exit Sub
        End If
        For Each col As DataGridViewColumn In dgvClass.Columns
            col.Visible = False
        Next
        dgvClass.Columns.Item(c_NameColumnName).DisplayIndex = 0
        dgvClass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.className
        dgvClass.Columns.Item(c_NameColumnName).Visible = True
        dgvClass.Columns.Item(c_StartColumnName).DisplayIndex = 1
        dgvClass.Columns.Item(c_StartColumnName).HeaderText = My.Resources.classStart
        dgvClass.Columns.Item(c_StartColumnName).Visible = True
        dgvClass.Columns.Item(c_StartColumnName).DefaultCellStyle.Format = "d"
        dgvClass.Columns.Item(c_EndColumnName).DisplayIndex = 2
        dgvClass.Columns.Item(c_EndColumnName).HeaderText = My.Resources.classEnd
        dgvClass.Columns.Item(c_EndColumnName).DefaultCellStyle.Format = "d"
        dgvClass.Columns.Item(c_EndColumnName).Visible = True
        dgvClass.Columns.Item(c_FeeColumnName).DisplayIndex = 3
        dgvClass.Columns.Item(c_FeeColumnName).HeaderText = My.Resources.fee
        dgvClass.Columns.Item(c_FeeColumnName).Visible = True
        If frmMain.CheckAuth(54) Then
            dgvClass.Columns.Item(c_StuRegCntColumnName).DisplayIndex = 4
            dgvClass.Columns.Item(c_StuRegCntColumnName).HeaderText = My.Resources.stuCnt
            dgvClass.Columns.Item(c_StuRegCntColumnName).Visible = True
            dgvClass.Columns.Item(c_CancelCntolumnName).DisplayIndex = 5
            dgvClass.Columns.Item(c_CancelCntolumnName).HeaderText = My.Resources.cancel
            dgvClass.Columns.Item(c_CancelCntolumnName).Visible = True
        Else
            dgvClass.Columns.Item(c_CancelCntolumnName).DisplayIndex = 4
            dgvClass.Columns.Item(c_CancelCntolumnName).HeaderText = My.Resources.cancel
            dgvClass.Columns.Item(c_CancelCntolumnName).Visible = True
        End If
        

        If dgvSubClass.Columns.Count > 0 Then
            dgvSubClass.Columns.Item(c_IDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_ClassIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_SubjectIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_ClassRoomIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
            dgvSubClass.Columns.Item(c_ClassRoomNameColumnName).HeaderText = My.Resources.classRoom
            dgvSubClass.Columns.Item(c_SubjectColumnName).HeaderText = My.Resources.subject
            dgvSubClass.Columns.Item(c_StuRegCntColumnName).HeaderText = My.Resources.headCnt
            dgvSubClass.Columns.Item(c_SequenceColumnName).HeaderText = My.Resources.classSequence
        End If
        If dgvSession.Columns.Count > 0 Then
            dgvSession.Columns.Item(c_IDColumnName).Visible = False
            dgvSession.Columns.Item(c_DayOfWeekColumnName).Visible = False
            dgvSession.Columns.Item(c_SubClassIDColumnName).Visible = False
            dgvSession.Columns.Item(c_DayOfWeekNameColumnName).DisplayIndex = 0
            dgvSession.Columns.Item(c_DayOfWeekNameColumnName).HeaderText = My.Resources.weekDay
            dgvSession.Columns.Item(c_StartColumnName).DisplayIndex = 1
            dgvSession.Columns.Item(c_StartColumnName).HeaderText = My.Resources.sessionStart
            dgvSession.Columns.Item(c_StartColumnName).DefaultCellStyle.Format = "t"
            dgvSession.Columns.Item(c_EndColumnName).DisplayIndex = 2
            dgvSession.Columns.Item(c_EndColumnName).HeaderText = My.Resources.sessionEnd
            dgvSession.Columns.Item(c_EndColumnName).DefaultCellStyle.Format = "t"
        End If
    End Sub

    Private Sub LoadColumnText2()

        If dgvSubClass.Columns.Count > 0 Then
            dgvSubClass.Columns.Item(c_IDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_ClassIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_SubjectIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_ClassRoomIDColumnName).Visible = False
            dgvSubClass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
            dgvSubClass.Columns.Item(c_ClassRoomNameColumnName).HeaderText = My.Resources.classRoom
            dgvSubClass.Columns.Item(c_SubjectColumnName).HeaderText = My.Resources.subject
            dgvSubClass.Columns.Item(c_StuRegCntColumnName).HeaderText = My.Resources.headCnt
            dgvSubClass.Columns.Item(c_SequenceColumnName).HeaderText = My.Resources.classSequence
        End If
        
    End Sub

    Private Sub LoadColumnText3()

        If dgvSession.Columns.Count > 0 Then
            dgvSession.Columns.Item(c_IDColumnName).Visible = False
            dgvSession.Columns.Item(c_DayOfWeekColumnName).Visible = False
            dgvSession.Columns.Item(c_SubClassIDColumnName).Visible = False
            dgvSession.Columns.Item(c_DayOfWeekNameColumnName).DisplayIndex = 0
            dgvSession.Columns.Item(c_DayOfWeekNameColumnName).HeaderText = My.Resources.weekDay
            dgvSession.Columns.Item(c_StartColumnName).DisplayIndex = 1
            dgvSession.Columns.Item(c_StartColumnName).HeaderText = My.Resources.sessionStart
            dgvSession.Columns.Item(c_StartColumnName).DefaultCellStyle.Format = "t"
            dgvSession.Columns.Item(c_EndColumnName).DisplayIndex = 2
            dgvSession.Columns.Item(c_EndColumnName).HeaderText = My.Resources.sessionEnd
            dgvSession.Columns.Item(c_EndColumnName).DefaultCellStyle.Format = "t"
        End If
    End Sub

    Private Sub InitSelections()

    End Sub

    Private Sub frmSetClass_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub dgvClass_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvClass.CellClick
        Dim intId As Integer
        If dgvClass.SelectedRows.Count > 0 Then
            intId = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            If intId > -1 Then
                intSelId = intId
            Else
               intSelId = -1
            End If
        Else
            intSelId = -1
        End If
    End Sub

    'Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
    '    Me.Close()
    'End Sub

    Private Sub dgvClass_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClass.SelectionChanged
        Dim intId As Integer
        If dgvClass.SelectedRows.Count > 0 And dtSubClassList.Rows.Count > 0 Then
            intId = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            If intId > -1 Then
                dtSubClassList.DefaultView.RowFilter = c_ClassIDColumnName & _
                    "=" & intId.ToString
                dgvSubClass.DataSource = dtSubClassList
            Else
                dtSubClassList.DefaultView.RowFilter = ""
                dgvSubClass.DataSource = dtSubClassList
            End If
            LoadColumnText2()
        Else
            dgvSubClass.DataSource = Nothing
            dgvSession.DataSource = Nothing
        End If
    End Sub


    Private Sub dgvSubClass_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSubClass.SelectionChanged
        Dim intId As Integer
        If dgvSubClass.SelectedRows.Count > 0 And dtSessionList.Rows.Count > 0 Then
            intId = dgvSubClass.SelectedRows(0).Cells(c_IDColumnName).Value
            If intId > -1 Then
                dtSessionList.DefaultView.RowFilter = c_SubClassIDColumnName & _
                "=" & intId.ToString
                dgvSession.DataSource = dtSessionList
            Else
                dtSessionList.DefaultView.RowFilter = ""
                dgvSession.DataSource = dtSessionList
            End If
            LoadColumnText3()
        Else
            dgvSession.DataSource = Nothing
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshData()
    End Sub

    Private Sub mnuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim str As String = "7e80f4fbc85c9c47afbc66dca9c46225"
        Dim bs As New List(Of Byte)
        For i As Integer = 0 To str.Length - 1 Step 2
            bs.Add(Byte.Parse(str.Substring(i, 2), Globalization.NumberStyles.HexNumber))
        Next





        If Not frmMain.CheckAuth(74) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        frmMain.SetOkState(False)
        Dim frm As New frm2AddClass
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()
            frmMain.RefreshClassRegInfo()
        End If

    End Sub

    Private Sub mnuModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuModify.Click
        If Not frmMain.CheckAuth(75) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvClass.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvClass.SelectedRows(0).Index
            Dim id As Integer = dgvClass.Rows(idx).Cells(c_IDColumnName).Value
            Dim name As String = dgvClass.Rows(idx).Cells(c_NameColumnName).Value
            Dim dateStart As Date = dgvClass.Rows(idx).Cells(c_StartColumnName).Value
            Dim dateEnd As Date = dgvClass.Rows(idx).Cells(c_EndColumnName).Value
            Dim Fee As Integer = dgvClass.Rows(idx).Cells(c_FeeColumnName).Value

            frmMain.SetOkState(False)
            Dim frm As New frm2UpdClass(id, name, _
                           dateStart, _
                         dateEnd, Fee)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
                frmMain.RefreshClassRegInfo()
            End If
        End If
    End Sub

    Private Sub mnuRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemove.Click
        If Not frmMain.CheckAuth(76) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvClass.SelectedRows.Count > 0 Then
            Dim result As MsgBoxResult = _
            MsgBox(My.Resources.msgConfirmDeleteClass, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
            If result = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim idx As Integer = dgvClass.SelectedRows(0).Index
            Dim id As Integer = dgvClass.Rows(idx).Cells(c_IDColumnName).Value
            objCsol.DeleteClass(id)
            frmMain.RefreshClassRegInfo()
            RefreshData()
            frmMain.BackgroundWorker7.RunWorkerAsync()
        End If
    End Sub

    Private Sub butAddSc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAddSc.Click
        If Not frmMain.CheckAuth(74) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvClass.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvClass.SelectedRows(0).Index
            Dim id As Integer = dgvClass.Rows(idx).Cells(c_IDColumnName).Value
            Dim s As Integer = 0
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = id Then
                    If dtSubClassList.Rows(index).Item(c_SequenceColumnName) > s Then
                        s = dtSubClassList.Rows(index).Item(c_SequenceColumnName)
                    End If
                End If
            Next
            s = s + 1
            frmMain.SetOkState(False)
            Dim frm As New frm2AddSubClass(-1, "", id, s, 0, 0)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
                Dim result As MsgBoxResult = MsgBox(My.Resources.addContentorNot, MsgBoxStyle.YesNo, My.Resources.msgTitle)
                If result = MsgBoxResult.Yes Then
                    Dim frm2 As New frm2SetClassContent
                    frm2.ShowDialog()
                End If
            End If
        End If
    End Sub

    Private Sub butModSc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butModSc.Click
        If Not frmMain.CheckAuth(75) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvSubClass.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvSubClass.SelectedRows(0).Index
            Dim id As Integer = dgvSubClass.Rows(idx).Cells(c_IDColumnName).Value
            Dim classId As Integer = dgvSubClass.Rows(idx).Cells(c_ClassIDColumnName).Value
            Dim s As Integer = dgvSubClass.Rows(idx).Cells(c_SequenceColumnName).Value
            Dim name As String = dgvSubClass.Rows(idx).Cells(c_NameColumnName).Value
            Dim subjectId As Integer = dgvSubClass.Rows(idx).Cells(c_SubjectIDColumnName).Value
            Dim crId As Integer = dgvSubClass.Rows(idx).Cells(c_ClassRoomIDColumnName).Value
            frmMain.SetOkState(False)
            Dim frm As New frm2AddSubClass(id, name, classId, s, subjectId, crId)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
            End If
        End If
    End Sub

    Private Sub butDelSc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDelSc.Click
        If Not frmMain.CheckAuth(76) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvSubClass.SelectedRows.Count > 0 Then
            Dim result As MsgBoxResult = _
            MsgBox(My.Resources.msgConfirmDeleteClass, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
            If result = MsgBoxResult.No Then
                Exit Sub
            End If
            Dim idx As Integer = dgvSubClass.SelectedRows(0).Index
            Dim id As Integer = dgvSubClass.Rows(idx).Cells(c_IDColumnName).Value
            objCsol.DeleteSubClass(id)
            RefreshData()
            frmMain.RefreshClassRegInfo()
            frmMain.BackgroundWorker7.RunWorkerAsync()
        End If
    End Sub

    Private Sub butDelSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDelSession.Click
        If dgvSubClass.SelectedRows.Count > 0 Then
            Dim id As Integer = dgvSession.SelectedRows(0).Cells(c_IDColumnName).Value
            objCsol.DeleteClassSession(id)
            RefreshData()
            frmMain.RefreshClassRegInfo()
            frmMain.BackgroundWorker7.RunWorkerAsync()
        End If
    End Sub

    Private Sub butAddSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAddSession.Click
        If dgvSubClass.SelectedRows.Count > 0 Then
            Dim id As Integer = dgvSubClass.SelectedRows(0).Cells(c_IDColumnName).Value
            frmMain.SetOkState(False)
            Dim frm As New frm2AddSession(-1, id, 1, Now, Now)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
            End If
        End If
    End Sub

    Private Sub butModSession_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butModSession.Click
        If dgvSubClass.SelectedRows.Count > 0 Then
            Dim idSc As Integer = dgvSession.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim id As Integer = dgvSession.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim dw As Integer = dgvSession.SelectedRows(0).Cells(c_DayOfWeekColumnName).Value
            Dim ds As Date = dgvSession.SelectedRows(0).Cells(c_StartColumnName).Value
            Dim de As Date = dgvSession.SelectedRows(0).Cells(c_EndColumnName).Value
            frmMain.SetOkState(False)
            Dim frm As New frm2AddSession(id, idSc, dw, ds, de)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
            End If
        End If
    End Sub

    Private Sub mnuContent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuContent.Click
        Dim frm As New frm2SetClassContent
        frm.ShowDialog()
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        frmMain.RefreshClassRegInfo()
        Me.Close()
    End Sub

    Private Sub mnuSequence_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSequence.Click
        If dgvClass.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvClass.SelectedRows(0).Index
            Dim id As Integer = dgvClass.Rows(idx).Cells(c_IDColumnName).Value
            Dim name As String = dgvClass.Rows(idx).Cells(c_NameColumnName).Value
            Dim lstId As New ArrayList
            Dim lstName As New ArrayList
            Dim lstSeq As New ArrayList
            Dim s As Integer = 0

            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = id Then
                    lstId.Add(dtSubClassList.Rows(index).Item(c_IDColumnName))
                    lstName.Add(dtSubClassList.Rows(index).Item(c_NameColumnName))
                    lstSeq.Add(dtSubClassList.Rows(index).Item(c_SequenceColumnName))
                End If
            Next
            s = s + 1
            Dim frm As New frm2SetClassSequence(name, lstId, lstName, lstSeq)
            frm.ShowDialog()
            RefreshData()
        End If
    End Sub

    Private Sub mnuSubject_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSubject.Click
        Dim frm As New frm2Subjects
        frm.ShowDialog()

    End Sub

    Private Sub mnuTodayClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTodayClass.Click
        frmMain.ShowClassToday()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If Not frmMain.CheckAuth(77) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvClass.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvClass.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvClass.Rows.Count

            Dim oRow As DataGridViewRow = dgvClass.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvClass.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvClass.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvClass.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvClass.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvClass.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvClass.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvClass.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvClass.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvClass.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If Not frmMain.CheckAuth(78) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        ExportDgvToExcel(dgvClass)
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchClass(1)
    End Sub

End Class