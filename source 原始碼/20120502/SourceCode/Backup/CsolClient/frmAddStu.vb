﻿Public Class frmAddStu
    Dim dtSch As New DataTable
    Dim dtSchGrp As New DataTable
    Dim dtSibType As New DataTable
    Dim strStuId As String = ""
    Dim dtStuOldRec As New DataTable
    Dim dtClassReg As New DataTable
    Dim blStuSaved As Boolean = False

    Dim lstPrimary As New ArrayList
    Dim lstJunior As New ArrayList
    Dim lstHigh As New ArrayList
    Dim lstUniversity As New ArrayList
    Dim lstSchGrp As New ArrayList
    Dim lstSibType As New ArrayList
    Dim subfrmReg As frm2ClassReg

    Dim lstSibRank As New ArrayList
    Dim lstSibTypeID As New ArrayList
    Dim lstSibName As New ArrayList
    Dim lstSibBirth As New ArrayList
    Dim lstSibSchool As New ArrayList
    Dim lstSibRemarks As New ArrayList

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmAddStu
        RefreshData()
    End Sub

    Private Sub frmAddStu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not blStuSaved And Not strStuId = "" Then
            MsgBox(My.Resources.msgNewStuNotSaved, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            If strStuId.Length = 8 Then
                objCsol.DeleteStudent(strStuId)
            End If
        End If
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmAddStu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.NumUDStuYear.Maximum = GetROCYear(Now) + c_StuYearBoundary - 1
        Me.NumUDStuYear.Minimum = GetROCYear(Now) - c_StuYearBoundary + 1
        Me.NumUDStuYear.Value = GetROCYear(Now)

    End Sub

    Friend Sub RefreshData()
        dtSch = frmMain.GetSchList
        dtSchGrp = frmMain.GetSchGrpList
        dtSibType = frmMain.GetSibTypeList
        For index As Integer = 0 To dtSch.Rows.Count - 1
            Select Case dtSch.Rows(index).Item(c_TypeIdColumnName)
                Case 1
                    cboxPrimarySch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                    lstPrimary.Add(dtSch.Rows(index).Item(c_IDColumnName))
                Case 2
                    cboxJuniorSch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                    lstJunior.Add(dtSch.Rows(index).Item(c_IDColumnName))
                Case 3
                    cboxHighSch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                    lstHigh.Add(dtSch.Rows(index).Item(c_IDColumnName))
                Case 4
                    cboxUniversity.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                    lstUniversity.Add(dtSch.Rows(index).Item(c_IDColumnName))
            End Select
        Next

        For index As Integer = 0 To dtSchGrp.Rows.Count - 1
            cboxGroup.Items.Add(dtSchGrp.Rows(index).Item(c_GroupColumnName).trim)
            lstSchGrp.Add(dtSchGrp.Rows(index).Item(c_IDColumnName))
        Next

        For index As Integer = 0 To dtSibType.Rows.Count - 1
            cboxFamily1.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
            cboxFamily2.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
            cboxFamily3.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
            cboxFamily4.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
            lstSibType.Add(dtSibType.Rows(index).Item(c_IDColumnName))
        Next
        cboxSex.SelectedIndex = -1

        dtClassReg = New DataTable
        InitTable()
        dgvClass.DataSource = dtClassReg
        LoadColumnText()
    End Sub

    Private Sub InitSelection(ByRef cbox As ComboBox)
        If cbox.Items.Count > 0 Then
            cbox.SelectedIndex = 0
        End If
    End Sub

    Private Sub InitTable()
        dtClassReg.Columns.Add(c_SubClassIDColumnName, GetType(System.Int32))
        dtClassReg.Columns.Add(c_ClassNameColumnName, GetType(System.String))
        dtClassReg.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
        dtClassReg.Columns.Add(c_SeatNumColumnName, GetType(System.String))
        dtClassReg.Columns.Add(c_SalesIDColumnName, GetType(System.String))
        dtClassReg.Columns.Add(c_SalesPersonColumnName, GetType(System.String))
        dtClassReg.Columns.Add(c_DateTimeColumnName, GetType(System.DateTime))
    End Sub

    Private Sub LoadColumnText()
        dgvClass.Columns.Item(c_SubClassIDColumnName).Visible = False
        dgvClass.Columns.Item(c_SalesIDColumnName).Visible = False
        dgvClass.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
        dgvClass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
        dgvClass.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
        dgvClass.Columns.Item(c_SalesPersonColumnName).HeaderText = My.Resources.sales
        dgvClass.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.regDate


    End Sub

    Private Sub frmAddStu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        If Not blStuSaved Then
            Dim result As MsgBoxResult = _
            MsgBox(My.Resources.msgConfirmNotSaveStu, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
            If result = MsgBoxResult.Yes Then
                If strStuId.Length = 8 Then
                    objCsol.DeleteStudent(strStuId)
                    strStuId = ""
                End If
                Me.Close()
            End If
        Else
            Me.Close()
        End If
    End Sub

    Private Sub GenStuId()
        Dim year As String = NumUDStuYear.Value
        If IsNumeric(year) Then
            Dim intY As Integer = CInt(year)
            If intY < GetROCYear(Now) + c_StuYearBoundary And _
                intY > GetROCYear(Now) - c_StuYearBoundary Then
                strStuId = objCsol.GenStuId(year)
                lblStuId.Text = strStuId
            Else
                MsgBox(My.Resources.msgWrongStuYear, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        Else
            MsgBox(My.Resources.msgWrongStuYear, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Public Function GetStuId() As String
        GetStuId = strStuId
    End Function

    Private Sub butReg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReg.Click
        If strStuId.Length = 8 Then
            If blStuSaved = False Then
                If tboxStuName.Text = "" Or tboxTel1.Text = "" Then
                    Dim result As MsgBoxResult = _
                    MsgBox(My.Resources.msgNoStuBasic, _
                            MsgBoxStyle.YesNo, My.Resources.msgRemindTitle)
                    If Not result = MsgBoxResult.Yes Then
                        Exit Sub
                    End If
                End If
                objCsol.UpdateStuBasic(strStuId, tboxStuName.Text.Trim, tboxTel1.Text.Trim)
                blStuSaved = True
            End If
            subfrmReg = New frm2ClassReg(strStuId)
            subfrmReg.ShowDialog()
            If frmMain.GetOkState Then
                dtClassReg = objCsol.GetClassRegByStuId(strStuId)
                dgvClass.DataSource = dtClassReg
            End If
        Else
            MsgBox(My.Resources.msgNoStuId, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Public Sub RefreshTable()
        If Not subfrmReg Is Nothing Then
            If subfrmReg.Created Then
                subfrmReg.AddClassReg()
                dtClassReg = objCsol.GetClassRegByStuId(strStuId)
                dgvClass.DataSource = dtClassReg
            End If
        End If
        
    End Sub



    'Private Sub tboxStuYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxStuYear.TextChanged

    '    If Integer.TryParse(tboxStuYear.Text, Nothing) Then
    '        Dim intY As Integer = Integer.Parse(StrConv(tboxStuYear.Text, VbStrConv.Narrow))

    '        If intY < GetROCYear(Now) + c_StuYearBoundary And _
    '            intY > GetROCYear(Now) - c_StuYearBoundary Then
    '            If Not blStuSaved Then
    '                If strStuId.Length = 8 Then
    '                    objCsol.DeleteStudent(strStuId)
    '                End If
    '                GenStuId()
    '            End If
    '        End If
    '    End If
    'End Sub
    Private Sub NumUDStuYear_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles NumUDStuYear.Leave
        If Integer.TryParse(NumUDStuYear.Value, Nothing) Then
            Dim intY As Integer = Integer.Parse(NumUDStuYear.Value)
            If intY < GetROCYear(Now) + c_StuYearBoundary And _
                intY > GetROCYear(Now) - c_StuYearBoundary Then
                If Not blStuSaved Then
                    If strStuId.Length = 8 Then
                        objCsol.DeleteStudent(strStuId)
                    End If
                    GenStuId()
                End If
            End If
        End If
    End Sub


    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If strStuId.Length = 8 Then
            If tboxStuName.Text.Trim = "" Or tboxTel1.Text.Trim = "" Then
                ShowMsg()
                Exit Sub
            End If

            FillSibList()

            Dim d As Date
            If tboxBirthY.Text.Length = 0 Or tboxBirthM.Text.Length = 0 Or tboxBirthD.Text.Length = 0 Then
                d = GetMinDate()
            Else
                d = GetBirthday(tboxBirthY.Text, tboxBirthM.Text, tboxBirthD.Text)
            End If

            objCsol.ModifyStudent(strStuId, tboxStuName.Text, tboxEngName.Text, _
                    GetSex, d, tboxIc.Text, tboxAdd1b.Text, tboxAdd1a.Text, tboxAdd2b.Text, _
                    tboxAdd2a.Text, tboxTel1.Text, tboxTel2.Text, tboxOfficeTel.Text, _
                    tboxMobile.Text, tboxEmail.Text, GetStuType, GetSchool, tboxClass.Text, _
                    GetSchoolGrade, GetPrimary, GetJunior, GetHigh, GetUniversity, _
                    GetCurrent, GetGroup, tboxGradJunior.Text, tboxDadName.Text, _
                    tboxMumName.Text, tboxDadTitle.Text, tboxMumTitle.Text, _
                    tboxDadMobile.Text, tboxMumMobile.Text, tboxIntroId.Text, _
                    tboxIntroName.Text, rtboxRemarks.Text, tboxRegInfo.Text, _
                    tboxClassId.Text, tboxStay.Text, "", "", lstSibRank, _
                    lstSibTypeID, lstSibName, lstSibBirth, lstSibSchool, lstSibRemarks, tboxEnrollSch.Text.Trim, _
                    tboxPunchCardNum.Text.Trim, tboxHseeMark.Text.Trim)

            If dtClassReg.Rows.Count > 0 Then
                frmMain.DisplayStuInfo(strStuId, c_DisplayStuInfoActionPay)
            Else
                frmMain.DisplayStuInfo(strStuId, c_DisplayStuInfoActionReg)
            End If
            blStuSaved = True
            Me.Close()
        End If
    End Sub

    Private Sub FillSibList()
        lstSibTypeID = New ArrayList
        lstSibName = New ArrayList
        lstSibBirth = New ArrayList
        lstSibSchool = New ArrayList
        lstSibRemarks = New ArrayList
        lstSibRank = New ArrayList

        If Not cboxFamily1.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily1.SelectedIndex))
            lstSibName.Add(tboxName1.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth1y.Text, tboxBirth1m.Text, tboxBirth1d.Text))
            lstSibSchool.Add(tboxSchool1.Text)
            lstSibRemarks.Add(tboxRemark1.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily2.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily2.SelectedIndex))
            lstSibName.Add(tboxName2.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth2y.Text, tboxBirth2m.Text, tboxBirth2d.Text))
            lstSibSchool.Add(tboxSchool2.Text)
            lstSibRemarks.Add(tboxRemark2.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily3.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily3.SelectedIndex))
            lstSibName.Add(tboxName3.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth3y.Text, tboxBirth3m.Text, tboxBirth3d.Text))
            lstSibSchool.Add(tboxSchool3.Text)
            lstSibRemarks.Add(tboxRemark3.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily4.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily4.SelectedIndex))
            lstSibName.Add(tboxName4.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth4y.Text, tboxBirth4m.Text, tboxBirth4d.Text))
            lstSibSchool.Add(tboxSchool4.Text)
            lstSibRemarks.Add(tboxRemark4.Text)
            lstSibRank.Add(0)
        End If
    End Sub

    Private Function GetSibType(ByVal idx As Integer) As Integer
        If idx > -1 Then
            Return lstSibType(idx)
        Else
            Return 0
        End If
    End Function

    Private Function GetCurrent() As Integer
        If radbutNowPri.Checked Then
            GetCurrent = 1
        ElseIf radbutNowJun.Checked Then
            GetCurrent = 2
        ElseIf radbutNowHig.Checked Then
            GetCurrent = 3
        ElseIf radbutNowUni.Checked Then
            GetCurrent = 4
        Else
            If cboxPrimarySch.SelectedIndex > -1 Then
                GetCurrent = 1
            ElseIf cboxJuniorSch.SelectedIndex > -1 Then
                GetCurrent = 2
            ElseIf cboxHighSch.SelectedIndex > -1 Then
                GetCurrent = 3
            ElseIf cboxUniversity.SelectedIndex > -1 Then
                GetCurrent = 4
            Else
                GetCurrent = 0
            End If
        End If
    End Function

    Private Function GetGroup() As Integer
        If cboxGroup.SelectedIndex > -1 Then
            Return lstSchGrp(cboxGroup.SelectedIndex)
        Else
            Return -1
        End If
    End Function

    Private Function GetPrimary() As Integer
        If cboxPrimarySch.SelectedIndex > -1 Then
            Return lstPrimary(cboxPrimarySch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetUniversity() As Integer
        If cboxUniversity.SelectedIndex > -1 Then
            Return lstUniversity(cboxUniversity.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetJunior() As Integer
        If cboxJuniorSch.SelectedIndex > -1 Then
            Return lstJunior(cboxJuniorSch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetHigh() As Integer
        If cboxHighSch.SelectedIndex > -1 Then
            Return lstHigh(cboxHighSch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetSchoolGrade() As Integer
        If cboxGrade.SelectedIndex > -1 Then
            GetSchoolGrade = cboxGrade.SelectedIndex
        Else
            GetSchoolGrade = -1
        End If
    End Function

    Private Function GetSchool() As String
        If radbutNowPri.Checked Then
            GetSchool = cboxPrimarySch.Text
        ElseIf radbutNowJun.Checked Then
            GetSchool = cboxJuniorSch.Text
        ElseIf radbutNowHig.Checked Then
            GetSchool = cboxHighSch.Text
        Else
            GetSchool = cboxUniversity.Text
        End If
    End Function

    Private Function GetStuType() As Byte
        If radbutFormalStu.Checked Then
            GetStuType = c_StuTypeFormal
        Else
            GetStuType = c_StuTypePotential
        End If
    End Function

    Private Function GetSex() As String
        If cboxSex.SelectedIndex = 0 Then
            GetSex = My.Resources.male
        ElseIf cboxSex.SelectedIndex = 1 Then
            GetSex = My.Resources.female
        Else
            GetSex = ""
        End If
    End Function

    Private Function GetBirthday(ByVal str1 As String, ByVal str2 As String, _
                                 ByVal str3 As String) As Date
        Dim year As Integer
        Dim month As Integer
        Dim day As Integer
        Dim d As Date = GetMinDate()

        If IsNumeric(str1) And IsNumeric(str2) And IsNumeric(str3) Then
            If CInt(str1) > 0 Then
                year = CInt(str1) + 1911
            Else
                year = d.Year
            End If

            If CInt(str2) > 0 And CInt(str2) < 13 Then
                month = CInt(str2)
            Else
                month = d.Month
            End If

            If CInt(str3) > 0 And CInt(str3) < 32 Then
                day = CInt(str3)
            Else
                day = d.Day
            End If
            GetBirthday = New Date(year, month, day)
        Else
            GetBirthday = d
        End If
    End Function

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoNameAndTel, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Sub butPri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPri.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxPrimarySch.Items.Count - 1
                If cboxPrimarySch.Items(index).ToString.Contains(name) Then
                    cboxPrimarySch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub


    Private Sub butJun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butJun.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxJuniorSch.Items.Count - 1
                If cboxJuniorSch.Items(index).ToString.Contains(name) Then
                    cboxJuniorSch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butHig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHig.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxHighSch.Items.Count - 1
                If cboxHighSch.Items(index).ToString.Contains(name) Then
                    cboxHighSch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butUni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUni.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxUniversity.Items.Count - 1
                If cboxUniversity.Items(index).ToString.Contains(name) Then
                    cboxUniversity.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub tboxStuName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxStuName.TextChanged
        If tboxStuName.Text.Trim.Length > 1 Then
            Dim strN As String = tboxStuName.Text.Trim
            dtStuOldRec = objCsol.GetStuRecByName(strN)
            If dtStuOldRec.Rows.Count > 0 Then
                dgvOldRec.DataSource = dtStuOldRec
                dgvOldRec.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
                dgvOldRec.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
                dgvOldRec.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            End If
        End If
    End Sub

    Private Sub dgvOldRec_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvOldRec.CellDoubleClick
        If dgvOldRec.SelectedRows.Count > 0 Then
            Dim s As String = dgvOldRec.SelectedRows(0).Cells(c_IDColumnName).Value
            frmMain.DisplayStuInfo(s, c_DisplayStuInfoActionNo)
        End If
    End Sub

    Private Sub butSeatNum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSeatNum.Click
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim intSubClassId As Integer = dgvClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim seat As String = dgvClass.SelectedRows(0).Cells(c_SeatNumColumnName).Value
            frmMain.SetOkState(False)
            Dim subfrmSeatMap As New frm2SeatMap(intSubClassId)
            subfrmSeatMap.ShowDialog()

            If frmMain.GetOkState Then
                Dim seat2 As String = frmMain.GetSeatNum
                objCsol.ChangeSeatNum(id, intSubClassId, seat2, seat, strStuId)
                frmMain.SetSeatNum("")
                dtClassReg = objCsol.GetClassRegByStuId(strStuId)
                dgvClass.DataSource = dtClassReg
            End If

        End If
    End Sub

    
    

End Class