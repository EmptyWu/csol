﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SelectClassAuthority
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chklstClass = New System.Windows.Forms.CheckedListBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutSelected = New System.Windows.Forms.RadioButton
        Me.radbutAll = New System.Windows.Forms.RadioButton
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chklstClass)
        Me.GroupBox1.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 25)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(289, 309)
        Me.GroupBox1.TabIndex = 52
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "班級"
        '
        'chklstClass
        '
        Me.chklstClass.CheckOnClick = True
        Me.chklstClass.FormattingEnabled = True
        Me.chklstClass.Location = New System.Drawing.Point(6, 25)
        Me.chklstClass.Name = "chklstClass"
        Me.chklstClass.Size = New System.Drawing.Size(276, 276)
        Me.chklstClass.TabIndex = 18
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(137, 0)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPast.TabIndex = 16
        Me.chkboxShowPast.Text = "顯示過時"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutSelected)
        Me.GroupBox3.Controls.Add(Me.radbutAll)
        Me.GroupBox3.Controls.Add(Me.chklstSubClass)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 340)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(289, 285)
        Me.GroupBox3.TabIndex = 53
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "班別"
        '
        'radbutSelected
        '
        Me.radbutSelected.AutoSize = True
        Me.radbutSelected.Checked = True
        Me.radbutSelected.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutSelected.Location = New System.Drawing.Point(20, 40)
        Me.radbutSelected.Name = "radbutSelected"
        Me.radbutSelected.Size = New System.Drawing.Size(167, 16)
        Me.radbutSelected.TabIndex = 19
        Me.radbutSelected.TabStop = True
        Me.radbutSelected.Text = "只設定給下面有勾選的班別"
        Me.radbutSelected.UseVisualStyleBackColor = True
        '
        'radbutAll
        '
        Me.radbutAll.AutoSize = True
        Me.radbutAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutAll.Location = New System.Drawing.Point(20, 18)
        Me.radbutAll.Name = "radbutAll"
        Me.radbutAll.Size = New System.Drawing.Size(119, 16)
        Me.radbutAll.TabIndex = 18
        Me.radbutAll.Text = "設定給全部的班別"
        Me.radbutAll.UseVisualStyleBackColor = True
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        Me.chklstSubClass.Location = New System.Drawing.Point(7, 70)
        Me.chklstSubClass.Name = "chklstSubClass"
        Me.chklstSubClass.Size = New System.Drawing.Size(276, 208)
        Me.chklstSubClass.TabIndex = 17
        '
        'butConfirm
        '
        Me.butConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butConfirm.Location = New System.Drawing.Point(305, 25)
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.Size = New System.Drawing.Size(82, 25)
        Me.butConfirm.TabIndex = 54
        Me.butConfirm.Text = "確定"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(305, 70)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 55
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2SelectClassAuthority
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(396, 629)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frm2SelectClassAuthority"
        Me.Text = "選擇班級權限"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutSelected As System.Windows.Forms.RadioButton
    Friend WithEvents radbutAll As System.Windows.Forms.RadioButton
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents chklstClass As System.Windows.Forms.CheckedListBox
End Class
