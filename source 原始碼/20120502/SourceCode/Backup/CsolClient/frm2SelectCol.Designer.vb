﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SelectCol
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SelectCol))
        Me.chklst = New System.Windows.Forms.CheckedListBox
        Me.radbutPrimary = New System.Windows.Forms.RadioButton
        Me.radbutJuniory = New System.Windows.Forms.RadioButton
        Me.radbutHigh = New System.Windows.Forms.RadioButton
        Me.radbutUniversity = New System.Windows.Forms.RadioButton
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.linklblNoPaper = New System.Windows.Forms.LinkLabel
        Me.linklblAllPaper = New System.Windows.Forms.LinkLabel
        Me.SuspendLayout()
        '
        'chklst
        '
        Me.chklst.CheckOnClick = True
        Me.chklst.FormattingEnabled = True
        resources.ApplyResources(Me.chklst, "chklst")
        Me.chklst.Name = "chklst"
        '
        'radbutPrimary
        '
        resources.ApplyResources(Me.radbutPrimary, "radbutPrimary")
        Me.radbutPrimary.Name = "radbutPrimary"
        Me.radbutPrimary.TabStop = True
        Me.radbutPrimary.UseVisualStyleBackColor = True
        '
        'radbutJuniory
        '
        resources.ApplyResources(Me.radbutJuniory, "radbutJuniory")
        Me.radbutJuniory.Name = "radbutJuniory"
        Me.radbutJuniory.TabStop = True
        Me.radbutJuniory.UseVisualStyleBackColor = True
        '
        'radbutHigh
        '
        resources.ApplyResources(Me.radbutHigh, "radbutHigh")
        Me.radbutHigh.Name = "radbutHigh"
        Me.radbutHigh.TabStop = True
        Me.radbutHigh.UseVisualStyleBackColor = True
        '
        'radbutUniversity
        '
        resources.ApplyResources(Me.radbutUniversity, "radbutUniversity")
        Me.radbutUniversity.Name = "radbutUniversity"
        Me.radbutUniversity.TabStop = True
        Me.radbutUniversity.UseVisualStyleBackColor = True
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'linklblNoPaper
        '
        resources.ApplyResources(Me.linklblNoPaper, "linklblNoPaper")
        Me.linklblNoPaper.Name = "linklblNoPaper"
        Me.linklblNoPaper.TabStop = True
        '
        'linklblAllPaper
        '
        resources.ApplyResources(Me.linklblAllPaper, "linklblAllPaper")
        Me.linklblAllPaper.Name = "linklblAllPaper"
        Me.linklblAllPaper.TabStop = True
        '
        'frm2SelectCol
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.linklblNoPaper)
        Me.Controls.Add(Me.linklblAllPaper)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.radbutUniversity)
        Me.Controls.Add(Me.radbutHigh)
        Me.Controls.Add(Me.radbutJuniory)
        Me.Controls.Add(Me.radbutPrimary)
        Me.Controls.Add(Me.chklst)
        Me.Name = "frm2SelectCol"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chklst As System.Windows.Forms.CheckedListBox
    Friend WithEvents radbutPrimary As System.Windows.Forms.RadioButton
    Friend WithEvents radbutJuniory As System.Windows.Forms.RadioButton
    Friend WithEvents radbutHigh As System.Windows.Forms.RadioButton
    Friend WithEvents radbutUniversity As System.Windows.Forms.RadioButton
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents linklblNoPaper As System.Windows.Forms.LinkLabel
    Friend WithEvents linklblAllPaper As System.Windows.Forms.LinkLabel
End Class
