﻿Public Class frm2SelectCol
    Dim lstName As New ArrayList
    Dim lstShow As New ArrayList
    Dim lst As New ArrayList
    Dim intSchType As Integer

    Public Sub New(ByVal name As ArrayList, ByVal show As ArrayList, ByVal schType As Integer)
        InitializeComponent()
        lstName = name
        lstShow = show
        intSchType = schType
        frmMain.SetOkState(False)
        InitSelection()
    End Sub

    Private Sub InitSelection()
        For index As Integer = 0 To lstName.Count - 1
            lst.Add(GetColName(lstName(index)))
            chklst.Items.Add(GetColName(lstName(index)), CBool(lstShow(index)))
        Next
        Select Case intSchType
            Case 0
                radbutPrimary.Checked = True
            Case 1
                radbutJuniory.Checked = True
            Case 2
                radbutHigh.Checked = True
            Case 3
                radbutUniversity.Checked = True
        End Select
    End Sub

    Private Function GetColName(ByVal name As String) As String
        Dim s As String = ""
        If name = c_IDColumnName Then
            s = My.Resources.stuID
        ElseIf name = c_NameColumnName Then
            s = My.Resources.stuName
        ElseIf name = c_EngNameColumnName Then
            s = My.Resources.engName
        ElseIf name = c_SexColumnName Then
            s = My.Resources.sex
        ElseIf name = c_BirthdayColumnName Then
            s = My.Resources.birthday
        ElseIf name = c_StuBirthdayColumnName Then
            s = My.Resources.stuBirthday
        ElseIf name = c_AddressColumnName Then
            s = My.Resources.address
        ElseIf name = c_PostalCodeColumnName Then
            s = My.Resources.postalCode
        ElseIf name = c_Address2ColumnName Then
            s = My.Resources.address2
        ElseIf name = c_PostalCode2ColumnName Then
            s = My.Resources.postalCode2
        ElseIf name = c_Tel1ColumnName Then
            s = My.Resources.tel1
        ElseIf name = c_Tel2ColumnName Then
            s = My.Resources.tel2
        ElseIf name = c_OfficeTelColumnName Then
            s = My.Resources.officeTel
        ElseIf name = c_MobileColumnName Then
            s = My.Resources.mobile
        ElseIf name = c_EmailColumnName Then
            s = My.Resources.email
        ElseIf name = c_SchoolColumnName Then
            s = My.Resources.school
        ElseIf name = c_StuSchoolColumnName Then
            s = My.Resources.stuSchool
        ElseIf name = c_SchoolClassColumnName Then
            s = My.Resources.schoolClass
        ElseIf name = c_SchoolGradeColumnName Then
            s = My.Resources.schoolGrade
        ElseIf name = c_CurrentSchColumnName Then
            s = My.Resources.currentSch
        ElseIf name = c_SchGroupColumnName Then
            s = My.Resources.schGroup
        ElseIf name = c_GraduateFromColumnName Then
            s = My.Resources.graduateFrom
        ElseIf name = c_DadNameColumnName Then
            s = My.Resources.dadName
        ElseIf name = c_MumNameColumnName Then
            s = My.Resources.mumName
        ElseIf name = c_DadTitleColumnName Then
            s = My.Resources.dadJob
        ElseIf name = c_MumTitleColumnName Then
            s = My.Resources.mumJob
        ElseIf name = c_DadMobileColumnName Then
            s = My.Resources.dadMobile
        ElseIf name = c_MumMobileColumnName Then
            s = My.Resources.mumMobile
        ElseIf name = c_IntroIDColumnName Then
            s = My.Resources.introId
        ElseIf name = c_IntroNameColumnName Then
            s = My.Resources.introName
        ElseIf name = c_CreateDateColumnName Then
            s = My.Resources.createDate
        ElseIf name = c_RemarksColumnName Then
            s = My.Resources.remarks
        ElseIf name = c_StuRemarksColumnName Then
            s = My.Resources.stuRemarks
        ElseIf name = c_Cust1ColumnName Then
            s = My.Resources.regInfo
        ElseIf name = c_Cust2ColumnName Then
            s = My.Resources.classNum
        ElseIf name = c_Cust3ColumnName Then
            s = My.Resources.hostel
        ElseIf name = c_EnrollSchColumnName Then
            s = My.Resources.enrollSch
        ElseIf name = c_PunchCardNumColumnName Then
            s = My.Resources.punchCardNum
        ElseIf name = c_HseeMarkColumnName Then
            s = My.Resources.hseeMark
        ElseIf name = c_HasCardColumnName Then
            s = My.Resources.hasCard
        ElseIf name = c_RegDateColumnName Then
            s = My.Resources.regDate
        ElseIf name = c_SalesPersonColumnName Then
            s = My.Resources.sales
        ElseIf name = c_PaperDateColumnName Then
            s = My.Resources.paperDate
        ElseIf name = c_ContentColumnName Then
            s = My.Resources.classContent
        ElseIf name = c_GroupColumnName Then
            s = My.Resources.schGroup
        ElseIf name = c_SubClassNameColumnName Then
            s = My.Resources.subClassName
        ElseIf name = c_SeatNumColumnName Then
            s = My.Resources.seatNum
        ElseIf name = c_ICColumnName Then
            s = My.Resources.ic
        ElseIf name = c_CardNumColumnName Then
            s = My.Resources.IDCardHas

        ElseIf name = "ComeInfo" Then
            s = "來班資訊"
        ElseIf name = "ClassNum" Then
            s = "班級編號"
        ElseIf name = "Accommodation" Then
            s = "住宿"
        ElseIf name = "SalesName" Then
            s = "業務"
        Else
            s = name
        End If
        Return s
    End Function

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        frmMain.SetOkState(True)
        For index As Integer = 0 To chklst.Items.Count - 1
            If chklst.GetItemChecked(index) Then
                lstShow(index) = 1
            Else
                lstShow(index) = 0
            End If
        Next
        If radbutPrimary.Checked Then
            intSchType = 0
        ElseIf radbutJuniory.Checked Then
            intSchType = 1
        ElseIf radbutHigh.Checked Then
            intSchType = 2
        ElseIf radbutUniversity.Checked Then
            intSchType = 3
        End If
        frmMain.SetCurrentList(lstShow)
        frmMain.SetCurrentList2(lst)
        frmMain.SetCurrentValue(intSchType)
        Me.Close()
    End Sub

    Private Sub linklblAllPaper_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblAllPaper.LinkClicked
        For index As Integer = 0 To chklst.Items.Count - 1
            chklst.SetItemChecked(index, True)
        Next
    End Sub


    Private Sub linklblNoPaper_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblNoPaper.LinkClicked
        For index As Integer = 0 To chklst.Items.Count - 1
            chklst.SetItemChecked(index, False)
        Next
    End Sub
End Class