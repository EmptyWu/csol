﻿Public Class frm2InputReason
    Dim reason As String = ""

    Private Sub frm2InputReason_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        reason = tboxQ.Text.Trim
        If reason = "" Then
            MsgBox("請輸入刪除收據原因 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Exit Sub
        End If
        frmMain.SetCurrentString(reason)
        frmMain.SetOkState(True)
        Me.Close()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        frmMain.SetOkState(False)
        Me.Close()
    End Sub

End Class