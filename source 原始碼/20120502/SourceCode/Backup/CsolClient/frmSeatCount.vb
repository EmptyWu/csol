﻿Public Class frmSeatCount
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private dtSchoolType As New DataTable
    Private dtClassRoomInfo As New DataTable
    Private dtSeatNaList As New DataTable
    Private dtSeatTakenList As New DataTable
    Private dsSeatRegInfo As New DataSet
    Private dtClassSeatMap As New DataTable
    Private dtSchool As New DataTable
    Private lstSubClassh As New ArrayList
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private dtBook As New DataTable
    Private lstBook As New ArrayList
    Private lstBookShown As New ArrayList
    Private lstRec As New ArrayList
    Private dtStu As New DataTable
    Private dtSession As New DataTable
    Private lstSession As New ArrayList
    Private multiClass As New ArrayList


    Private intRowCnt As Integer = 0
    Private intColCnt As Integer = 0
    Private intPwCnt As Integer = 0
    Private intState As Integer = -1
    Private intDisStyle As Integer = -1

    Private lstColumn As New ArrayList
    Private lstRow As New ArrayList
    Private lstPw As New ArrayList
    Private lstConvex As New ArrayList
    Private lstNaCol As New ArrayList
    Private lstNaRow As New ArrayList
    Private lstTk As New ArrayList
    Private lstName As New ArrayList
    Private blSaved As Boolean = False
    Private lstAbc() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ".Split(",")
    Private dtTime As Date

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSeatCount
        tboxSeatNa.Text = c_SeatNaText
        RefreshClassLst()
        '20100512 sherry 11
        dtBook = frmMain.GetBooks
        RefreshData()
        InitTable()
        LoadColumnText()
    End Sub

    Private Sub RefreshClassLst()

        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        dtContent = frmMain.GetContents
        dtSession = frmMain.GetSessions
        cboxClass.Items.Clear()
        lstClass = New ArrayList

        If chkboxShowAll.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) > Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                   
                End If
            Next
        End If
        If lstClass.Count > 0 Then
            cboxClass.SelectedIndex = -1
        End If

    End Sub

    'Private Sub frmSeatCount_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    '    If Not blSaved Then
    '        If lstRec.Count > 0 Then
    '            If radbutPCTime.Checked Then
    '                dtTime = Now
    '            ElseIf radbutThisTime.Checked Then
    '                dtTime = dtpickDate.Value
    '                If Not tboxTime.Text.Trim = "" Then
    '                    Dim lst As Array = tboxTime.Text.Split(":")
    '                    If lst.Length = 3 Then
    '                        dtTime = New Date(dtTime.Year, dtTime.Month, dtTime.Day, CInt(lst(0)), _
    '                                     CInt(lst(1)), CInt(lst(2)))
    '                    End If
    '                End If
    '            End If
    '            Dim strSeat As String = ""
    '            Dim idx As Integer = -1
    '            Dim s As String = ""
    '            For index As Integer = 0 To dgvSeat.Columns.Count - 1
    '                For j As Integer = 0 To dgvSeat.Rows.Count - 1
    '                    If Not dgvSeat.Columns(index).HeaderText = "" And _
    '                        Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
    '                        Select Case intDisStyle
    '                            Case 1, 3
    '                                strSeat = dgvSeat.Columns(index).HeaderText & _
    '                                        dgvSeat.Rows(j).HeaderCell.Value
    '                            Case 2
    '                                strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
    '                                        dgvSeat.Columns(index).HeaderText
    '                        End Select
    '                        idx = lstTk.IndexOf(strSeat)
    '                        If idx > -1 Then 'taken
    '                            If dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxHere.BackColor Then
    '                                lstRec.Add(GetID(strSeat))
    '                            End If
    '                        End If
    '                    End If
    '                Next
    '            Next
    '            Dim c1 As Integer = 0
    '            Dim c2 As Integer = 0
    '            Dim c3 As Integer = 0
    '            If cboxClass.SelectedIndex > -1 Then
    '                idx = lstClass(cboxClass.SelectedIndex)
    '            Else
    '                idx = -1
    '            End If

    '            If cboxSubClass.SelectedIndex > -1 Then
    '                c1 = lstSubClass(cboxSubClass.SelectedIndex)
    '            Else
    '                c1 = -1
    '            End If

    '            If cboxContent.SelectedIndex > -1 Then
    '                c2 = lstContent(cboxContent.SelectedIndex)
    '            Else
    '                c2 = -1
    '            End If

    '            If cboxSession.SelectedIndex > -1 Then
    '                c3 = lstSession(cboxSession.SelectedIndex)
    '            Else
    '                c3 = -1
    '            End If

    '            objCsol.AddPunchRec(lstRec, c1, c2, -1, c3, dtTime)
    '            objCsol.AddBookIssueRec(lstRec, idx, lstBook, dtTime)
    '        End If
    '    End If
    '    frmMain.CloseTab(Me.Tag)
    'End Sub

    Private Sub frmSeatCount_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim strCompute1 As String = "COUNT(" + c_ColumnNumColumnName + ")"
        Dim strCompute2 As String = c_StateColumnName & "=0"
        If cboxSubClass.SelectedIndex > -1 Then


            Try
                dsSeatRegInfo = objCsol.GetSeatRegInfo(lstSubClass(cboxSubClass.SelectedIndex))
                dtClassRoomInfo = dsSeatRegInfo.Tables(c_ClassRoomInfoTableName)
                dtSeatNaList = dsSeatRegInfo.Tables(c_SeatNaListTableName)
                dtSeatTakenList = dsSeatRegInfo.Tables(c_SeatTakenListTableName)
                dtClassSeatMap = objCsol.GetSeatMap(lstSubClass(cboxSubClass.SelectedIndex))

                If dtClassRoomInfo.Rows.Count > 0 Then
                    intRowCnt = dtClassRoomInfo.Rows(0).Item(c_LineCntColumnName)
                    intColCnt = dtClassRoomInfo.Rows(0).Item(c_ColumnCntColumnName)
                    intDisStyle = dtClassRoomInfo.Rows(0).Item(c_DisStyleColumnName)
                    intPwCnt = dtSeatNaList.Compute(strCompute1, strCompute2)
                    lstPw = New ArrayList
                    lstConvex = New ArrayList
                    lstNaCol = New ArrayList
                    lstNaRow = New ArrayList
                    lstTk = New ArrayList
                    lstName = New ArrayList

                    Dim intS = 0
                    For index As Integer = 0 To dtSeatNaList.Rows.Count - 1
                        intS = dtSeatNaList.Rows(index).Item(c_StateColumnName)
                        If intS = 0 Then
                            lstPw.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                        ElseIf intS = 2 Then
                            lstConvex.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                        ElseIf intS = 1 Then
                            lstNaCol.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                            lstNaRow.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                        End If
                    Next

                    For index As Integer = 0 To dtSeatTakenList.Rows.Count - 1
                        lstTk.Add(dtSeatTakenList.Rows(index).Item(c_SeatNumColumnName).ToString.Trim)
                        lstName.Add(dtSeatTakenList.Rows(index).Item(c_StuNameColumnName))

                    Next

                    If intRowCnt > 0 And intColCnt + intPwCnt > 0 Then
                        InitTable()
                        RefreshTable()
                    End If
                End If

            Catch ex As ApplicationException
                MessageBox.Show(ex.Message)
            End Try
        End If
    End Sub

    Private Sub InitTable()
        dgvSeat.ColumnCount = intColCnt + intPwCnt
        dgvSeat.RowCount = intRowCnt

        FillArrayList(1, intColCnt + intPwCnt, lstColumn)
        FillArrayList(1, intRowCnt, lstRow)

        Dim i As Integer = 0

        Select Case intDisStyle
            Case 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        lstColumn(index) = lstAbc(i).ToString
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next

            Case 2
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i + 1 < 10 Then
                            lstColumn(index) = "0" & (i + 1).ToString
                        Else
                            lstColumn(index) = (i + 1).ToString
                        End If
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    lstRow(index) = lstAbc(index).ToString
                Next

            Case 3
                i = lstAbc.Count - 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i > 0 Then
                            lstColumn(index) = lstAbc(i).ToString
                            i = i - 1
                        End If
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next
        End Select

        For index As Integer = 0 To dgvSeat.ColumnCount - 1
            If index < lstColumn.Count Then
                dgvSeat.Columns(index).HeaderText = lstColumn(index)
                If lstColumn(index) = "" Then
                    dgvSeat.Columns(index).DefaultCellStyle.BackColor = Color.Silver
                End If
                dgvSeat.Columns(index).Width = 40
                dgvSeat.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            End If
        Next

        For index As Integer = 0 To dgvSeat.RowCount - 1
            If index < lstRow.Count Then
                dgvSeat.Rows(index).HeaderCell.Value = lstRow(index)
            End If
            If lstConvex.IndexOf(index) > -1 Then
                dgvSeat.Rows(index).HeaderCell.Style.BackColor = Color.Aqua
            End If
        Next

        If lstNaCol.Count > 0 Then
            For index As Integer = 0 To lstNaCol.Count - 1
                dgvSeat.Rows(lstNaRow(index)).Cells(lstNaCol(index)).value = c_SeatNaText
            Next
        End If

    End Sub

    Private Sub RefreshTable()
        Dim strSeat As String = ""
        Dim idx As Integer = -1
        Dim s As String = ""
        Dim sc As Integer = lstSubClass(cboxSubClass.SelectedIndex)                                                         '100209
        dtStu = objCsol.GetSubClassPunchRecNoCont(sc, GetDateStart(Today), GetDateEnd(Today))         '100209

        For index As Integer = 0 To dgvSeat.Columns.Count - 1
            For j As Integer = 0 To dgvSeat.Rows.Count - 1
                If Not dgvSeat.Columns(index).HeaderText = "" And _
                    Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
                    Select Case intDisStyle
                        Case 1, 3
                            strSeat = dgvSeat.Columns(index).HeaderText & _
                                    dgvSeat.Rows(j).HeaderCell.Value
                        Case 2
                            strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
                                    dgvSeat.Columns(index).HeaderText
                    End Select
                    idx = lstTk.IndexOf(strSeat)
                    If idx > -1 Then 'taken
                        dgvSeat.Rows(j).Cells(index).Style.BackColor = Color.White
                        s = strSeat & " " & lstName(idx)
                        's = lstName(idx)

                        If dtStu.Rows.Count > 0 Then
                            For Each row As DataRow In dtStu.Rows                                               '100209
                                With row
                                    Dim stuName As String = .Item("stuname").ToString.Trim
                                    If lstName(idx) = stuName Then
                                        dgvSeat.Rows(j).Cells(index).Value = s
                                        dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxHere.BackColor
                                    Else
                                        dgvSeat.Rows(j).Cells(index).Value = s
                                    End If
                                End With
                            Next                                                                                '100209
                        Else
                            dgvSeat.Rows(j).Cells(index).Style.BackColor = Color.White
                            dgvSeat.Rows(j).Cells(index).Value = s
                        End If

                    Else 'available
                        dgvSeat.Rows(j).Cells(index).Style.BackColor = Color.White
                        dgvSeat.Rows(j).Cells(index).Value = strSeat
                    End If
                End If
            Next
        Next
    End Sub


    Private Function GetID(ByVal s As String) As String
        Dim r As String = ""
        If dtClassSeatMap.Rows.Count > 0 Then
            For index As Integer = 0 To dtClassSeatMap.Rows.Count - 1
                If dtClassSeatMap.Rows(index).Item(c_SeatNumColumnName).ToString.Trim = s Then
                    r = dtClassSeatMap.Rows(index).Item(c_StuIDColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Sub LoadColumnText()
        'To be added
    End Sub

    Private Sub frmSeatCount_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowAll.CheckedChanged
        RefreshClassLst()
        RefreshData()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        If cboxClass.SelectedIndex > -1 Then
            cboxSubClass.Items.Clear()
            lstSubClass = New ArrayList

            If lstClass.Count > 0 Then
                Dim c As Integer = lstClass(cboxClass.SelectedIndex)
                For index As Integer = 0 To dtSubClass.Rows.Count - 1
                    If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = c Then
                        cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                        lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            End If
            If cboxSubClass.Items.Count > 0 Then
                cboxSubClass.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        cboxContent.Items.Clear()
        lstContent.Clear()
        cboxSession.Items.Clear()
        lstSession.Clear()
        If cboxSubClass.SelectedIndex > -1 Then
            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            If dtContent.Rows.Count > 0 Then
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If GetIntValue(dtContent.Rows(index).Item(c_SubClassIDColumnName)) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxContent.Items.Count > 0 Then
                    cboxContent.SelectedIndex = 0
                End If
            End If
            If dtSession.Rows.Count > 0 Then
                For index As Integer = 0 To dtSession.Rows.Count - 1
                    If dtSession.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxSession.Items.Add(GetWeekDayName(dtSession.Rows(index).Item(c_DayOfWeekColumnName), 0) & _
                                              "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_StartColumnName))) & _
                                              "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_EndColumnName))))
                        lstSession.Add(dtSession.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxSession.Items.Count > 0 Then
                    cboxSession.SelectedIndex = 0
                End If
            End If


            dtStu = objCsol.ListStuBySubClass(i)
            RefreshData()
        End If
    End Sub

    Private Sub chkboxBook_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxBook.CheckedChanged
        If chkboxBook.Checked Then
            If cboxClass.SelectedIndex > -1 Then
                Dim i As Integer = lstClass(cboxClass.SelectedIndex)
                Dim j As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                lstBook.Clear()
                lstBookShown.Clear()
                For index As Integer = 0 To dtBook.Rows.Count - 1
                    If dtBook.Rows(index).Item(c_SubClassIDColumnName) = j Then
                        lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                        chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName))
                    End If
                Next
                chklstBook.Visible = True
            End If
        Else
            lstBook.Clear()
            lstBookShown.Clear()
            chklstBook.Items.Clear()
            chklstBook.Visible = False
        End If
    End Sub

    Private Sub butSetBook_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSetBook.Click
        If lstBookShown.Count > 0 Then
            For index As Integer = 0 To chklstBook.Items.Count - 1
                If chklstBook.GetItemChecked(index) Then
                    lstBook.Add(lstBookShown(index))

                End If
            Next
        End If
        chklstBook.Visible = False
    End Sub

    Private Sub dgvSeat_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvSeat.CellClick
        If Not dgvSeat.CurrentCell Is Nothing Then
            If Not dgvSeat.CurrentCell.Value Is Nothing Then
                If dgvSeat.CurrentCell.Value.ToString.Length > 3 Then
                    If dgvSeat.CurrentCell.Style.BackColor = tboxHere.BackColor Then
                        dgvSeat.CurrentCell.Style.BackColor = tboxAbsent.BackColor
                    ElseIf dgvSeat.CurrentCell.Style.BackColor = tboxAbsent.BackColor Then
                        dgvSeat.CurrentCell.Style.BackColor = tboxHere.BackColor
                    End If
                End If
            End If
        End If
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim sc As Integer = lstSubClass(cboxSubClass.SelectedIndex)                                                         '100210
        Dim lstRec1 As New ArrayList
        Dim lstRec2 As New ArrayList
        dtStu = objCsol.GetSubClassPunchRecNoCont(sc, GetDateStart(Today), GetDateEnd(Today))                                 '100210

        If radbutPCTime.Checked Then
            dtTime = Now
        ElseIf radbutThisTime.Checked Then
            dtTime = dtpickDate.Value
            If Not tboxTime.Text.Trim = "" Then
                Dim lst As Array = tboxTime.Text.Split(":")
                If lst.Length = 3 Then
                    dtTime = New Date(dtTime.Year, dtTime.Month, dtTime.Day, CInt(lst(0)), _
                                 CInt(lst(1)), CInt(lst(2)))
                End If
            End If
        End If
        Dim strSeat As String = ""
        Dim idx As Integer = -1
        Dim s As String = ""
        For index As Integer = 0 To dgvSeat.Columns.Count - 1
            For j As Integer = 0 To dgvSeat.Rows.Count - 1
                If Not dgvSeat.Columns(index).HeaderText = "" And _
                    Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
                    Select Case intDisStyle
                        Case 1, 3
                            strSeat = dgvSeat.Columns(index).HeaderText & _
                                    dgvSeat.Rows(j).HeaderCell.Value
                        Case 2
                            strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
                                    dgvSeat.Columns(index).HeaderText
                    End Select
                    idx = lstTk.IndexOf(strSeat)
                    If idx > -1 Then 'taken
                        If dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxHere.BackColor Then
                            lstRec1.Add(GetID(strSeat))
                        ElseIf dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxAbsent.BackColor Then          '100210
                            For Each row As DataRow In dtStu.Rows
                                With row
                                    If GetID(strSeat) = .Item("stuid").ToString.Trim Then
                                        lstRec2.Add(.Item("ID"))
                                    End If
                                End With
                            Next
                        End If                                                                                   '100210
                    End If
                End If
            Next
        Next
        Dim c1 As Integer = 0
        Dim c2 As Integer = 0
        Dim c3 As Integer = 0
        If cboxClass.SelectedIndex > -1 Then
            idx = lstClass(cboxClass.SelectedIndex)
        Else
            idx = -1
        End If

        If cboxSubClass.SelectedIndex > -1 Then
            c1 = lstSubClass(cboxSubClass.SelectedIndex)
        Else
            c1 = -1
        End If

        If cboxContent.SelectedIndex > -1 Then
            c2 = lstContent(cboxContent.SelectedIndex)
        Else
            c2 = -1
        End If

        If cboxSession.SelectedIndex > -1 Then
            c3 = lstSession(cboxSession.SelectedIndex)
        Else
            c3 = -1
        End If
        If lstRec1.Count > 0 Then                                                            '100210
            For i As Integer = 0 To lstRec1.Count - 1
                Dim dtPunchRec As New DataTable
                Dim dtPunchTime As New Date
                Dim subClassName As String = ""
                Dim flag As Integer = 0
                dtPunchRec = objCsol.GetPunchRecById(lstRec1(i))
                For Each row As DataRow In dtPunchRec.Rows
                    With row
                        dtPunchTime = .Item("DateTime1")
                        subClassName = .Item("subClassName").ToString.Trim
                        If dtPunchTime.Date = Now.Date And subClassName = cboxSubClass.Text.Trim Then
                            flag = 1
                            Exit For
                        End If
                    End With
                Next
                If flag = 0 Then
                    objCsol.AddPunchRec(lstRec1(i), dtTime, c1, c2, 0, c3, 0)

                    objCsol.AddBookIssueRec(lstRec1(i), idx, lstBook, dtTime)

                End If
            Next
        End If
        If lstRec2.Count > 0 Then
            For i As Integer = 0 To lstRec2.Count - 1
                objCsol.DeletePunchRec(lstRec2(i))
            Next
        End If                                                                              '100210


        blSaved = True
        MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub


End Class