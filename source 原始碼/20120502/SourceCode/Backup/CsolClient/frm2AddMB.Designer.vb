﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddMB
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.tboxReason = New System.Windows.Forms.TextBox
        Me.tboxHandler = New System.Windows.Forms.TextBox
        Me.tboxSubclass = New System.Windows.Forms.TextBox
        Me.tboxClass = New System.Windows.Forms.TextBox
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxAmount = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.tboxStuId = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(10, 185)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 12)
        Me.Label8.TabIndex = 151
        Me.Label8.Text = "退費日期: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(2, 127)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(71, 12)
        Me.Label7.TabIndex = 150
        Me.Label7.Text = "可領回金額: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(79, 180)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(229, 22)
        Me.dtpickDate.TabIndex = 149
        '
        'tboxReason
        '
        Me.tboxReason.Location = New System.Drawing.Point(79, 152)
        Me.tboxReason.Name = "tboxReason"
        Me.tboxReason.Size = New System.Drawing.Size(229, 22)
        Me.tboxReason.TabIndex = 148
        '
        'tboxHandler
        '
        Me.tboxHandler.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxHandler.Location = New System.Drawing.Point(79, 208)
        Me.tboxHandler.Name = "tboxHandler"
        Me.tboxHandler.ReadOnly = True
        Me.tboxHandler.Size = New System.Drawing.Size(229, 22)
        Me.tboxHandler.TabIndex = 147
        '
        'tboxSubclass
        '
        Me.tboxSubclass.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxSubclass.Location = New System.Drawing.Point(79, 96)
        Me.tboxSubclass.Name = "tboxSubclass"
        Me.tboxSubclass.ReadOnly = True
        Me.tboxSubclass.Size = New System.Drawing.Size(229, 22)
        Me.tboxSubclass.TabIndex = 146
        '
        'tboxClass
        '
        Me.tboxClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxClass.Location = New System.Drawing.Point(79, 68)
        Me.tboxClass.Name = "tboxClass"
        Me.tboxClass.ReadOnly = True
        Me.tboxClass.Size = New System.Drawing.Size(229, 22)
        Me.tboxClass.TabIndex = 145
        '
        'tboxName
        '
        Me.tboxName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxName.Location = New System.Drawing.Point(79, 40)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.ReadOnly = True
        Me.tboxName.Size = New System.Drawing.Size(229, 22)
        Me.tboxName.TabIndex = 144
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(10, 155)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 12)
        Me.Label6.TabIndex = 143
        Me.Label6.Text = "退費原因: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxAmount
        '
        Me.tboxAmount.Location = New System.Drawing.Point(79, 124)
        Me.tboxAmount.Name = "tboxAmount"
        Me.tboxAmount.Size = New System.Drawing.Size(229, 22)
        Me.tboxAmount.TabIndex = 142
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(10, 211)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(59, 12)
        Me.Label5.TabIndex = 141
        Me.Label5.Text = "承辦人員: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(10, 99)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 140
        Me.Label4.Text = "退費班別: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(10, 71)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 139
        Me.Label3.Text = "退費班級: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(34, 43)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 138
        Me.Label1.Text = "姓名: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(226, 238)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 137
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(133, 238)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(82, 25)
        Me.butSave.TabIndex = 136
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'tboxStuId
        '
        Me.tboxStuId.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxStuId.Location = New System.Drawing.Point(79, 12)
        Me.tboxStuId.Name = "tboxStuId"
        Me.tboxStuId.ReadOnly = True
        Me.tboxStuId.Size = New System.Drawing.Size(229, 22)
        Me.tboxStuId.TabIndex = 135
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(34, 15)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 134
        Me.Label2.Text = "學號: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frm2AddMB
        '
        Me.AcceptButton = Me.butSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(318, 275)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxReason)
        Me.Controls.Add(Me.tboxHandler)
        Me.Controls.Add(Me.tboxSubclass)
        Me.Controls.Add(Me.tboxClass)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxAmount)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.tboxStuId)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frm2AddMB"
        Me.Text = "輸入退費"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxReason As System.Windows.Forms.TextBox
    Friend WithEvents tboxHandler As System.Windows.Forms.TextBox
    Friend WithEvents tboxSubclass As System.Windows.Forms.TextBox
    Friend WithEvents tboxClass As System.Windows.Forms.TextBox
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents tboxStuId As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
