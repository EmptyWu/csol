﻿Imports System.IO
Imports System.Drawing.Printing
Imports System.Drawing
Imports System.Drawing.Drawing2D

Public Class frmStuInfo
    Private strStuId As String
    Private dtStuInfo As New DataTable
    Private dtClassReg As New DataTable
    Private dtClassReg2 As New DataTable
    Private dtPayRec As New DataTable
    Private dtSib As New DataTable
    Private dsStuInfo As New DataSet
    Private dtKBRec As New DataTable
    Private dtKBPay As New DataTable
    Private dtKBAtt As New DataTable
    Private dtAttClass As New DataTable
    Private dtAttRec As New DataTable
    Private dtAssign As New DataTable
    Private dtPaper As New DataTable
    Private dtDisc As New DataTable
    Private dtTeleInterview As New DataTable
    Private dtTeleInterviewType As New DataTable
    Private lstTeleInterviewType As New ArrayList
    Private intTeleInterviewType As Integer = -1
    Dim subfrmReg As frm2ClassReg

    Dim dtAssignRecHas As New DataTable
    Dim dtAssignRecNo As New DataTable

    Dim dtPaperRecHas As New DataTable
    Dim dtPaperRecNo As New DataTable

    Dim dtSch As New DataTable
    Dim dtSchGrp As New DataTable
    Dim dtSibType As New DataTable
    Dim lstPrimary As New ArrayList
    Dim lstJunior As New ArrayList
    Dim lstHigh As New ArrayList
    Dim lstUniversity As New ArrayList
    Dim lstSchGrp As New ArrayList
    Dim lstSibType As New ArrayList

    Dim lstSibRank As New ArrayList
    Dim lstSibTypeID As New ArrayList
    Dim lstSibName As New ArrayList
    Dim lstSibBirth As New ArrayList
    Dim lstSibSchool As New ArrayList
    Dim lstSibRemarks As New ArrayList
    Dim blNewClassReg As Boolean = False
    Dim dtSc As New DataTable
    Dim lstid As New ArrayList
    Dim lstname As New ArrayList
    Dim strCardNum As String = ""

    Dim lstbookClass As New ArrayList
    Dim dtBookRecHas As New DataTable
    Dim dtBookRecNo As New DataTable
    Dim dtClassChange As New DataTable

    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuInfo
    Private sUserName As String = frmMain.GetUsrName

    Private PageNumber As Integer
    Private intAct As Integer = 0
    Private intOldDisc As Integer = -999999
    Private strOldDiscRemarks As String = ""
    Private intOldDiscId As Integer = -1

    Public Sub New(ByVal strId As String, ByVal intAction As Integer)
        InitializeComponent()

        Me.Text = My.Resources.frmStuInfo
        intAct = intAction
        strStuId = strId
        RefreshData(strStuId)

    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvAttClass.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        'If nPageNo = 1 Then

        For Each oColumn As DataGridViewColumn In dgvAttClass.Columns
            If oColumn.Visible = True Then

                nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                oColumnLefts.Add(nLeft)
                oColumnWidths.Add(nWidth)
                oColumnTypes.Add(oColumn.GetType)
                nLeft += nWidth
            End If
        Next

        'End If

        Do While nRowPos < dgvAttClass.Rows.Count

            Dim oRow As DataGridViewRow = dgvAttClass.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvAttClass.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvAttClass.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvAttClass.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvAttClass.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvAttClass.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvAttClass.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvAttClass.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvAttClass.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvAttClass.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuInfo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        RefreshKBData()
        RefreshAttendanceData()
        RefreshBookData()
        RefreshClassChangeData()
        RefreshTeleInterviewData()
        RefreshAssignData()
        RefreshPaperData()

        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        'Action: 0-No, 1-Prompt Pay
        If intAct = c_DisplayStuInfoActionPay Then
            tabCtrl.SelectedIndex = 1
            blNewClassReg = True
        ElseIf intAct = c_DisplayStuInfoActionReg Then
            If strStuId.Length = 8 Then
                subfrmReg = New frm2ClassReg(strStuId)
                subfrmReg.ShowDialog()
                RefreshClassRegData()
            Else
                MsgBox(My.Resources.msgNoStuId, _
                                MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
    End Sub

    Private Sub RefreshClassChangeData()
        dtClassChange = objCsol.ListChangeClassRec(strStuId)
        Dim dt As DataTable = dtClassChange.DefaultView.ToTable(True, c_ClassNameColumnName, c_EndColumnName)
        cboxChangeClass.Items.Clear()
        cboxChangeClass.Items.Add(My.Resources.all)
        If chkboxChangeShowPast.Checked Then
            For index As Integer = 0 To dt.Rows.Count - 1
                cboxChangeClass.Items.Add(dt.Rows(index).Item(c_ClassNameColumnName).trim)
            Next
        Else
            For index As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(index).Item(c_EndColumnName) >= Now Then
                    cboxChangeClass.Items.Add(dt.Rows(index).Item(c_ClassNameColumnName).trim)
                End If
            Next
        End If
        cboxChangeClass.SelectedIndex = 0
        dgvChange.DataSource = dtClassChange
        LoadColumnTextClassChange()
    End Sub

    Public Function GetDataFromSql(ByRef AskAction As String, ByRef GetAction As String) As DataTable
        Dim dt As DataTable '----2011/1/21
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        RequestParams.Add("StuId", strStuId)
        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", AskAction, RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        dt = CSOL.Convert.XmlStringToDataTable(ResponseParams(GetAction))
        Return dt '----2011/1/21



        'Dim dt As DataTable '----2011/1/21
        'Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        'RequestParams.Add("StuId", strStuId)
        'Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", "GetStuAssign", RequestParams, System.Text.Encoding.UTF8)
        'Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        'dt = CSOL.Convert.XmlStringToDataTable(ResponseParams("StuAssign"))
        'Return dt '----2011/1/21
    End Function



    Public Sub RefreshAssignData()

        cboxAssignClass.Items.Clear()
        dtAssignRecHas.Rows.Clear()
        dtAssignRecNo.Rows.Clear()
        Try
            Dim AskAction As String = "GetStuAssign" '----2011/1/21
            Dim GetAction As String = "StuAssign"
            Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21
            cboxAssignClass.Items.Add(My.Resources.all)
            dtAssignRecHas.Reset()
            dtAssignRecNo.Reset()
            dtAssignRecHas = dt.Clone
            dtAssignRecNo = dt.Clone
            If chkboxAssignShowPast.Checked Then
                SelectAssPassOrNot(dt, dtAssignRecHas, dtAssignRecNo) '----2011/1/25
            Else
                SelectAssPassOrNot_time(dt, dtAssignRecHas, dtAssignRecNo) '----2011/1/25
            End If
            dgvAssignPass.DataSource = dtAssignRecHas
            dgvAssignNoPass.DataSource = dtAssignRecNo
            'cboxAssignClass.SelectedIndex = 0
            LoadColumnAssign()
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try
    End Sub



    Private Sub chkboxAssignShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxAssignShowPast.CheckedChanged
        dtAssignRecHas.Clear()
        dtAssignRecNo.Clear()

        Dim AskAction As String = "GetStuAssign" '----2011/1/21
        Dim GetAction As String = "StuAssign"
        Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21

        If chkboxAssignShowPast.Checked Then
            SelectAssPassOrNot(dt, dtAssignRecHas, dtAssignRecNo) '----2011/1/25
        Else
            SelectAssPassOrNot_time(dt, dtAssignRecHas, dtAssignRecNo) '----2011/1/25
        End If
        dgvAssignPass.DataSource = dtAssignRecHas
        dgvAssignNoPass.DataSource = dtAssignRecNo
        cboxAssignClass.SelectedIndex = 0
        LoadColumnAssign()
    End Sub

    Public Function SelectAssPassOrNot(ByVal dt As DataTable, ByRef dtAssignRecHas As DataTable, ByRef dtAssignRecNo As DataTable) As DataTable

        For index As Integer = 0 To dt.Rows.Count - 1 '----2011/1/25
            If Not dt.Rows(index).Item("Mark") = "0" Then
                dtAssignRecHas.Rows.Add(dt.Rows(index).ItemArray)
                Dim ClassName As String = dt.Rows(index).Item("SubClassName").trim
                If Not cboxAssignClass.Items.Contains(ClassName) Then
                    cboxAssignClass.Items.Add(ClassName)
                End If
            ElseIf dt.Rows(index).Item("Mark") = "0" Then
                dtAssignRecNo.Rows.Add(dt.Rows(index).ItemArray)
                Dim ClassName As String = dt.Rows(index).Item("SubClassName").trim
                If Not cboxAssignClass.Items.Contains(ClassName) Then
                    cboxAssignClass.Items.Add(ClassName)
                End If
            End If
        Next
        Return dt '----2011/1/25
    End Function

    Public Function SelectAssPassOrNot_time(ByVal dt As DataTable, ByRef dtAssignRecHas As DataTable, ByRef dtAssignRecNo As DataTable) As DataTable

        For index As Integer = 0 To dt.Rows.Count - 1 '----2011/1/25
            If Not dt.Rows(index).Item("Mark") = "0" Then
                If dt.Rows(index).Item("End") >= Now Then
                    Dim ClassName As String = dt.Rows(index).Item("SubClassName").trim
                    dtAssignRecHas.Rows.Add(dt.Rows(index).ItemArray)
                    If Not cboxAssignClass.Items.Contains(ClassName) Then
                        cboxAssignClass.Items.Add(ClassName)
                    End If
                End If
            ElseIf dt.Rows(index).Item("Mark") = "0" Then
                If dt.Rows(index).Item("End") >= Now Then
                    dtAssignRecNo.Rows.Add(dt.Rows(index).ItemArray)
                    Dim ClassName As String = dt.Rows(index).Item("SubClassName").trim
                    If Not cboxAssignClass.Items.Contains(ClassName) Then
                        cboxAssignClass.Items.Add(ClassName)
                    End If
                End If
            End If
        Next
        Return dt '----2011/1/25
    End Function

    Private Sub cboxAssignClass_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxAssignClass.SelectedValueChanged


        Dim AskAction As String = "GetStuAssign" '----2011/1/21
        Dim GetAction As String = "StuAssign"
        Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21

        If dt.Rows.Count > 0 Then
            Dim strFilter1 As String = ""
            Dim strFilter2 As String = ""
            chackAssShowPast(strFilter1, strFilter2) '----2011/1/25
            'dtAssignRecHas.Clear()
            'dtAssignRecNo.Clear()
            dtAssignRecHas.Reset() '----2011/1/25
            dtAssignRecNo.Reset()
            dtAssignRecHas = dt.Clone
            dtAssignRecNo = dt.Clone '----2011/1/25

            For index As Integer = 0 To dt.Rows.Count - 1
                If Not dt.Rows(index).Item("Mark") = "0" Then
                    dtAssignRecHas.Rows.Add(dt.Rows(index).ItemArray)
                ElseIf dt.Rows(index).Item("Mark") = "0" Then
                    dtAssignRecNo.Rows.Add(dt.Rows(index).ItemArray)
                End If
            Next
            Dim AssignPassView As New DataView(dtAssignRecHas, strFilter1, "", DataViewRowState.CurrentRows)
            Dim AssignNoPassView As New DataView(dtAssignRecNo, strFilter1, "", DataViewRowState.CurrentRows)

            dgvAssignPass.DataSource = AssignPassView
            dgvAssignNoPass.DataSource = AssignNoPassView
            LoadColumnAssign()
            'cboxAssignClass.SelectedIndex = 0
        End If
    End Sub
    Public Sub chackAssShowPast(ByRef strFilter1 As String, ByRef strFilter2 As String)
        If chkboxAssignShowPast.Checked Then '----2011/1/25
            If cboxAssignClass.SelectedIndex > 0 Then
                strFilter1 = c_SubClassNameColumnName & "='" & cboxAssignClass.SelectedItem & "'"
            End If
        Else
            strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
            If cboxAssignClass.SelectedIndex > 0 Then
                strFilter2 = c_SubClassNameColumnName & "='" & cboxAssignClass.SelectedItem & "'"
                strFilter1 = strFilter1 & " AND " & strFilter2
            End If
        End If '----2011/1/25
    End Sub


    Public Sub RefreshPaperData()
        cboxPaperClass.Items.Clear()
        dtPaperRecHas.Rows.Clear()
        dtPaperRecNo.Rows.Clear()
        Try
            Dim AskAction As String = "GetStuPaper" '----2011/1/21
            Dim GetAction As String = "StuPaper"
            Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21

            cboxPaperClass.Items.Add(My.Resources.all)

            dtPaperRecHas.Reset()
            dtPaperRecNo.Reset()

            dtPaperRecHas = dt.Clone
            dtPaperRecNo = dt.Clone
            If chkboxPaperShowPast.Checked Then

                SelectPaperPassOrNot(dt, dtPaperRecHas, dtPaperRecNo) '----2011/1/25
            Else

                SelectPaperPassOrNot_time(dt, dtPaperRecHas, dtPaperRecNo) '----2011/1/25
            End If
            dgvPaperPass.DataSource = dtPaperRecHas
            dgvPaperNoPass.DataSource = dtPaperRecNo
            'cboxAssignClass.SelectedIndex = 0
            LoadColumnPaper()
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try
    End Sub

    Private Sub chkboxPaperShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxPaperShowPast.CheckedChanged
        dtPaperRecHas.Clear()
        dtPaperRecNo.Clear()

        Dim AskAction As String = "GetStuPaper" '----2011/1/21
        Dim GetAction As String = "StuPaper"
        Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21

        If chkboxPaperShowPast.Checked Then
            SelectPaperPassOrNot(dt, dtPaperRecHas, dtPaperRecNo) '----2011/1/25
        Else
            SelectPaperPassOrNot_time(dt, dtPaperRecHas, dtPaperRecNo) '----2011/1/25
        End If
        dgvPaperPass.DataSource = dtPaperRecHas
        dgvPaperNoPass.DataSource = dtPaperRecNo
        cboxPaperClass.SelectedIndex = 0
        LoadColumnPaper()
    End Sub

    Private Sub cboxPaperClass_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxPaperClass.SelectedValueChanged


        Dim AskAction As String = "GetStuPaper" '----2011/1/21
        Dim GetAction As String = "StuPaper"
        Dim dt As DataTable = GetDataFromSql(AskAction, GetAction) '----2011/1/21
        If dt.Rows.Count > 0 Then
            Dim strFilter1 As String = ""
            Dim strFilter2 As String = ""
            'If chkboxPaperShowPast.Checked Then
            '    If cboxPaperClass.SelectedIndex > 0 Then
            '        strFilter1 = c_SubClassNameColumnName & "='" & cboxPaperClass.SelectedItem & "'"
            '    End If
            'Else
            '    strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
            '    If cboxPaperClass.SelectedIndex > 0 Then
            '        strFilter2 = c_SubClassNameColumnName & "='" & cboxPaperClass.SelectedItem & "'"
            '        strFilter1 = strFilter1 & " AND " & strFilter2
            '    End If
            'End If
            chackPaperShowPast(strFilter1, strFilter2) '----2011/1/25
            'dtPaperRecHas.Clear()
            'dtPaperRecNo.Clear()
            dtPaperRecHas.Reset() '----2011/1/25
            dtPaperRecNo.Reset()
            dtPaperRecHas = dt.Clone
            dtPaperRecNo = dt.Clone '----2011/1/25
            For index As Integer = 0 To dt.Rows.Count - 1
                If Not dt.Rows(index).Item("Mark") = "-1" Then
                    dtPaperRecHas.Rows.Add(dt.Rows(index).ItemArray)
                ElseIf dt.Rows(index).Item("Mark") = "-1" Then
                    dtPaperRecNo.Rows.Add(dt.Rows(index).ItemArray)
                End If
            Next
            Dim PaperPassView As New DataView(dtPaperRecHas, strFilter1, "", DataViewRowState.CurrentRows)
            Dim PaperNoPassView As New DataView(dtPaperRecNo, strFilter1, "", DataViewRowState.CurrentRows)

            dgvPaperPass.DataSource = PaperPassView
            dgvPaperNoPass.DataSource = PaperNoPassView
            LoadColumnPaper()
            'cboxAssignClass.SelectedIndex = 0


        End If
    End Sub

    Public Sub chackPaperShowPast(ByRef strFilter1 As String, ByRef strFilter2 As String)
        If chkboxPaperShowPast.Checked Then '----2011/1/25
            If cboxPaperClass.SelectedIndex > 0 Then
                strFilter1 = c_SubClassNameColumnName & "='" & cboxPaperClass.SelectedItem & "'"
            End If
        Else
            strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
            If cboxPaperClass.SelectedIndex > 0 Then
                strFilter2 = c_SubClassNameColumnName & "='" & cboxPaperClass.SelectedItem & "'"
                strFilter1 = strFilter1 & " AND " & strFilter2
            End If
        End If '----2011/1/25
    End Sub
    Public Function SelectPaperPassOrNot(ByVal dt As DataTable, ByRef dtAssignRecHas As DataTable, ByRef dtAssignRecNo As DataTable) As DataTable

        For index As Integer = 0 To dt.Rows.Count - 1 '----2011/1/25
            If Not dt.Rows(index).Item("Mark") = "-1" Then
                dtPaperRecHas.Rows.Add(dt.Rows(index).ItemArray)
                Dim ClassName As String = dt.Rows(index).Item(c_SubClassNameColumnName).trim
                If Not cboxPaperClass.Items.Contains(ClassName) Then
                    cboxPaperClass.Items.Add(ClassName)
                End If
            ElseIf dt.Rows(index).Item("Mark") = "-1" Then
                dtPaperRecNo.Rows.Add(dt.Rows(index).ItemArray)
                Dim ClassName As String = dt.Rows(index).Item(c_SubClassNameColumnName).trim
                If Not cboxPaperClass.Items.Contains(ClassName) Then
                    cboxPaperClass.Items.Add(ClassName)
                End If
            End If
        Next
        Return dt '----2011/1/25
    End Function

    Public Function SelectPaperPassOrNot_time(ByVal dt As DataTable, ByRef dtAssignRecHas As DataTable, ByRef dtAssignRecNo As DataTable) As DataTable

        For index As Integer = 0 To dt.Rows.Count - 1 '----2011/1/25
            If Not dt.Rows(index).Item("Mark") = "-1" Then
                If dt.Rows(index).Item("End") >= Now Then
                    Dim ClassName As String = dt.Rows(index).Item(c_SubClassNameColumnName).trim
                    dtPaperRecHas.Rows.Add(dt.Rows(index).ItemArray)
                    If Not cboxPaperClass.Items.Contains(ClassName) Then
                        cboxPaperClass.Items.Add(ClassName)
                    End If
                End If
            ElseIf dt.Rows(index).Item("Mark") = "-1" Then
                If dt.Rows(index).Item("End") >= Now Then
                    dtPaperRecNo.Rows.Add(dt.Rows(index).ItemArray)
                    Dim ClassName As String = dt.Rows(index).Item(c_SubClassNameColumnName).trim
                    If Not cboxPaperClass.Items.Contains(ClassName) Then
                        cboxPaperClass.Items.Add(ClassName)
                    End If
                End If
            End If
        Next
        Return dt '----2011/1/25
    End Function

    Private Sub RefreshTeleInterviewData()

        dtTeleInterviewType = frmMain.GetTeleInterviewType
        cboxTeleType.Items.Clear()
        lstTeleInterviewType.Clear()
        cboxTeleType.Items.Add(My.Resources.all)
        lstTeleInterviewType.Add(-1)
        For index As Integer = 0 To dtTeleInterviewType.Rows.Count - 1
            cboxTeleType.Items.Add(dtTeleInterviewType.Rows(index).Item(c_TypeColumnName))
            lstTeleInterviewType.Add(dtTeleInterviewType.Rows(index).Item(c_IDColumnName))
        Next
        cboxTeleType.SelectedIndex = 0
        chkboxTeleDate.Checked = False
        dtTeleInterview = objCsol.GetTeleInterviewStu(strStuId)
        dgvTeleRec.DataSource = dtTeleInterview
        LoadColumnTextTeleInterview()
    End Sub

    Private Sub LoadColumnTextTeleInterview()
        For Each col In dgvTeleRec.Columns
            col.visible = False
        Next
        If dgvTeleRec.Rows.Count > 0 Then
            dgvTeleRec.Columns.Item(c_DateTimeColumnName).DisplayIndex = 0
            dgvTeleRec.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvTeleRec.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.teleInterviewDateTime
            dgvTeleRec.Columns.Item(c_TypeColumnName).DisplayIndex = 1
            dgvTeleRec.Columns.Item(c_TypeColumnName).Visible = True
            dgvTeleRec.Columns.Item(c_TypeColumnName).HeaderText = My.Resources.teleinterviewType
            dgvTeleRec.Columns.Item(c_ContentColumnName).DisplayIndex = 2
            dgvTeleRec.Columns.Item(c_ContentColumnName).Visible = True
            dgvTeleRec.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.teleInterviewContent
            dgvTeleRec.Columns.Item(c_HandlerColumnName).DisplayIndex = 3
            dgvTeleRec.Columns.Item(c_HandlerColumnName).Visible = True
            dgvTeleRec.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.teleinterviewHandler
        End If
    End Sub
    Private Sub LoadColumnAssign()
        For Each col In dgvAssignPass.Columns
            col.visible = False
        Next
        If dgvAssignPass.Rows.Count > 0 Then
            dgvAssignPass.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgvAssignPass.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvAssignPass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvAssignPass.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvAssignPass.Columns.Item(c_NameColumnName).Visible = True
            dgvAssignPass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.assignmentName
            dgvAssignPass.Columns.Item(c_EndColumnName).DisplayIndex = 2
            dgvAssignPass.Columns.Item(c_EndColumnName).Visible = True
            dgvAssignPass.Columns.Item(c_EndColumnName).HeaderText = My.Resources.assignmentDate
            dgvAssignPass.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
            dgvAssignPass.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvAssignPass.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.assignmentDue
        End If
        For Each col In dgvAssignNoPass.Columns
            col.visible = False
        Next
        If dgvAssignNoPass.Rows.Count > 0 Then
            dgvAssignNoPass.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgvAssignNoPass.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvAssignNoPass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvAssignNoPass.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvAssignNoPass.Columns.Item(c_NameColumnName).Visible = True
            dgvAssignNoPass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.assignmentName
            dgvAssignNoPass.Columns.Item(c_EndColumnName).DisplayIndex = 2
            dgvAssignNoPass.Columns.Item(c_EndColumnName).Visible = True
            dgvAssignNoPass.Columns.Item(c_EndColumnName).HeaderText = My.Resources.assignmentDate
        End If
    End Sub

    Private Sub LoadColumnPaper()
        For Each col In dgvPaperPass.Columns
            col.visible = False
        Next
        If dgvPaperPass.Rows.Count > 0 Then
            dgvPaperPass.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgvPaperPass.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvPaperPass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvPaperPass.Columns.Item(c_PaperNameColumnName).DisplayIndex = 1
            dgvPaperPass.Columns.Item(c_PaperNameColumnName).Visible = True
            dgvPaperPass.Columns.Item(c_PaperNameColumnName).HeaderText = My.Resources.paperName
            dgvPaperPass.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvPaperPass.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvPaperPass.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.paperDate
            dgvPaperPass.Columns.Item(c_MarkColumnName).DisplayIndex = 3
            dgvPaperPass.Columns.Item(c_MarkColumnName).Visible = True
            dgvPaperPass.Columns.Item(c_MarkColumnName).HeaderText = My.Resources.mark
        End If
        For Each col In dgvPaperNoPass.Columns
            col.visible = False
        Next
        If dgvPaperNoPass.Rows.Count > 0 Then
            dgvPaperNoPass.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgvPaperNoPass.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvPaperNoPass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvPaperNoPass.Columns.Item(c_PaperNameColumnName).DisplayIndex = 1
            dgvPaperNoPass.Columns.Item(c_PaperNameColumnName).Visible = True
            dgvPaperNoPass.Columns.Item(c_PaperNameColumnName).HeaderText = My.Resources.paperName
            dgvPaperNoPass.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvPaperNoPass.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvPaperNoPass.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.paperDate
        End If
    End Sub


    Private Sub RefreshBookData()
        Dim dtBookRecNo1 As DataTable = objCsol.GetStuNoBookRec(strStuId)
        Dim bookRec As New ArrayList

        dtBookRecHas = objCsol.ListBookIssueRec2(strStuId)                                                              '100224
        For i As Integer = 0 To dtBookRecHas.Rows.Count - 1
            bookRec.Add(dtBookRecHas.Rows(i).Item("Name").ToString.Trim)
        Next
        dtBookRecNo = dtBookRecNo1.Clone
        For i As Integer = 0 To dtBookRecNo1.Rows.Count - 1
            If Not bookRec.Contains(dtBookRecNo1.Rows(i).Item("Name").ToString.Trim) Then
                dtBookRecNo.Rows.Add(dtBookRecNo1.Rows(i).ItemArray)
            End If
        Next

        RefreshBookClass()

        Dim strFilter1 As String = c_IDColumnName & " is not Null"
        Dim strFilter2 As String = c_BeforeDateColumnName & ">='" & Now.ToString & "'"
        Dim strFilter3 As String = c_IDColumnName & " is Null"

        If dtBookRecHas.Rows.Count > 0 Then
            RefreshBookReceive()
            RefreshBookNotReceive()

            dgvKBBook.DataSource = dtBookRecHas.DefaultView
        Else
            dgvBookReceive.DataSource = Nothing
            dgvKBBook.DataSource = Nothing
            dgvBookNotReceive.DataSource = Nothing
        End If
        LoadColumnTextBook()
    End Sub

    Private Sub RefreshAttendanceData()
        Try

            Dim ds As DataSet
            Dim dtAttClassName As DataTable
            ds = objCsol.ListPunchRec(strStuId)
            dtAttClass = ds.Tables(c_AttClassTableName)
            dtAttRec = ds.Tables(c_AttRecTableName)
            If Not dtAttRec.Columns.Contains("MakeUpClassID2") Then
                dtAttRec.Columns.Add("MakeUpClassID2")
            End If
            If Not dtAttRec.Columns.Contains("PunchDate") Then
                dtAttRec.Columns.Add("PunchDate")
            End If
            If Not dtAttRec.Columns.Contains("StratTime") Then
                dtAttRec.Columns.Add("StartTime")
            End If
            If Not dtAttRec.Columns.Contains("EndTime") Then
                dtAttRec.Columns.Add("EndTime")
            End If

            Dim dtAttRec2 As New DataTable
            dtAttRec2 = dtAttRec.Clone()

            Dim rows() As DataRow = dtAttRec.Select("", "start")

            For Each row As DataRow In rows
                Dim punchDate As String = CDate(row.Item("start")).ToString("yyyy-MM-dd")
                Dim punchTime As String = CDate(row.Item("start")).ToString("HH:mm:ss")
                Dim classIn As String = row.Item("in").ToString.Trim
                Dim punchType As String = row.Item("MakeUpClassID").ToString.Trim
                If punchType = "-1" Then
                    row.Item("MakeUpClassID2") = "補課刷卡"
                End If
                If classIn = "0" Then
                    Dim PunchOutrow() As DataRow = dtAttRec.Select(String.Format("Start >= '{0}' ANd start <='{1}' AND subclassId = {2} And [in] = 1 ", punchDate & " 00:00:00", punchDate & " 23:59:59", row.Item("SubClassID")))
                    If PunchOutrow.Count = 1 Then
                        row.Item("EndTime") = CDate(PunchOutrow(0).Item("start")).ToString("HH:mm:ss")
                        PunchOutrow(0).Item("In") = -1
                    End If
                    row.Item("PunchDate") = punchDate
                    row.Item("StartTime") = punchTime
                ElseIf classIn = "1" Then
                    row.Item("PunchDate") = punchDate
                    row.Item("StartTime") = punchTime
                End If
            Next

            For Each row As DataRow In dtAttRec.Select("[In] >= 0", "Start")
                dtAttRec2.Rows.Add(row.ItemArray)
            Next
            dtAttRec = dtAttRec2



            'For i As Integer = 0 To dtAttRec.Rows.Count - 1
            '    Dim punchDate As String = CDate(dtAttRec.Rows(i).Item("start")).ToString("yyyy-MM-dd")
            '    Dim punchTime As String = CDate(dtAttRec.Rows(i).Item("start")).ToString("HH:mm:ss")
            '    Dim punchType As String = dtAttRec.Rows(i).Item("MakeUpClassID").ToString.Trim
            '    Dim classIn As String = dtAttRec.Rows(i).Item("in").ToString.Trim
            '    If punchType = "-1" Then
            '        dtAttRec.Rows(i).Item("MakeUpClassID2") = "補課刷卡"
            '    End If
            '    If classIn = "0" Then
            '        dtAttRec.Rows(i).Item("PunchDate") = punchDate
            '        dtAttRec.Rows(i).Item("StartTime") = punchTime
            '    ElseIf classIn = "1" Then
            '        dtAttRec.Rows(i).Item("PunchDate") = punchDate
            '        dtAttRec.Rows(i).Item("EndTime") = punchTime
            '    End If
            'Next

            dtAttClassName = dtAttClass.DefaultView.ToTable(True, c_ClassNameColumnName, c_EndColumnName)
            dtKBAtt = dtAttRec.Clone
            dtKBAtt.Merge(dtAttRec)
            cboxAttClass.Items.Clear()
            dtAttClass.Columns.Add(c_DayOfWeekNameColumnName, GetType(System.String))
            Dim wd As Integer = 0
            For Each row As DataRow In dtAttClass.Rows
                wd = GetIntValue(row.Item(c_DayOfWeekColumnName))
                row.Item(c_DayOfWeekNameColumnName) = GetWeekDayName(wd, 0)
            Next
            If dtAttClass.Rows.Count > 0 Then
                cboxAttClass.Items.Add(My.Resources.all)
                Dim strFilter1 As String = ""
                If chkboxAttShowPast.Checked Then
                    For index As Integer = 0 To dtAttClassName.Rows.Count - 1
                        cboxAttClass.Items.Add(dtAttClassName.Rows(index).Item(c_ClassNameColumnName).trim)
                    Next
                Else
                    For index As Integer = 0 To dtAttClassName.Rows.Count - 1
                        If dtAttClassName.Rows(index).Item(c_EndColumnName) >= Now Then
                            cboxAttClass.Items.Add(dtAttClassName.Rows(index).Item(c_ClassNameColumnName).trim)
                        End If
                    Next
                    strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
                End If
                cboxAttClass.SelectedIndex = 0
                dtAttClass.DefaultView.RowFilter = strFilter1

                Dim dt As DataTable = dtAttRec2.Clone                                        '100321

                For i As Integer = 0 To dtAttRec.Rows.Count - 1
                    If CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = 0 Or CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = -1 Then
                        dt.Rows.Add(dtAttRec.Rows(i).ItemArray)
                    End If
                Next
                dgvAttClass.DataSource = dt.DefaultView                                     '100321
                dgvKBAttendance.DataSource = dtKBAtt
            End If
            LoadColumnTextAtt()
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try
    End Sub

    Private Sub RefreshClassTable()

        Dim strCompute2 As String
        Dim strCompute3 As String
        Dim strCompute4 As String
        Dim strCompute5 As String
        If dtClassReg.Rows.Count = 0 Then
            dgvClass2.DataSource = dtClassReg2
            dgvClass.DataSource = dtClassReg
            dgvPayDetails.DataSource = dtPayRec.DefaultView
            Exit Sub
        End If

        Try
            strCompute2 = c_EndColumnName & ">='" & Now.ToString & "'"
            If chkboxShowPast2.Checked Then
                dtClassReg2.DefaultView.RowFilter = ""
            Else
                dtClassReg2.DefaultView.RowFilter = strCompute2
            End If

            strCompute3 = c_StartColumnName & ">'" & Now.ToString & "'"
            strCompute4 = c_StartColumnName & "<='" & Now.ToString & "' AND " & _
                            c_EndColumnName & ">='" & Now.ToString & "'"
            strCompute5 = c_EndColumnName & "<'" & Now.ToString & "'"

            If chkboxShowPast.Checked And chkboxShowOpen.Checked And _
                chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = ""
            ElseIf chkboxShowPast.Checked And Not chkboxShowOpen.Checked And _
                Not chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute5
            ElseIf chkboxShowPast.Checked And chkboxShowOpen.Checked And _
                Not chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute5 & " OR (" & _
                    strCompute4 & ")"
            ElseIf Not chkboxShowPast.Checked And chkboxShowOpen.Checked And _
                Not chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute4
            ElseIf Not chkboxShowPast.Checked And Not chkboxShowOpen.Checked And _
                Not chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = c_SubClassIDColumnName & "=-1"
            ElseIf Not chkboxShowPast.Checked And Not chkboxShowOpen.Checked And _
                            chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute3
            ElseIf Not chkboxShowPast.Checked And chkboxShowOpen.Checked And _
                                        chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute4 & " OR (" & _
                    strCompute3 & ")"
            ElseIf chkboxShowPast.Checked And Not chkboxShowOpen.Checked And _
                                    chkboxShowNotOpen.Checked Then
                dtClassReg.DefaultView.RowFilter = strCompute3 & " OR (" & _
                    strCompute5 & ")"
            End If

            For i As Integer = 0 To dtClassReg.Rows.Count - 1
                If dtClassReg.Rows(i).Item(c_CancelColumnName).ToString.Trim = "1" Then
                    dtClassReg.Rows(i).Item("SeatNum") = "不上了"
                End If
            Next

            dgvClass2.DataSource = dtClassReg2
            dgvClass.DataSource = dtClassReg
            If dgvClass2.RowCount > 0 Then
                dgvClass2.CurrentCell = dgvClass2.Rows.Item(0).Cells(c_SubClassNameColumnName)
            End If

            dtPayRec.DefaultView.RowFilter = c_SubClassIDColumnName & "=" & _
                dtClassReg2.Rows(0).Item(c_SubClassIDColumnName).ToString
            Dim t As Integer = dtPayRec.Rows.Count
            dgvPayDetails.DataSource = dtPayRec.DefaultView


            RefreshPayDetails()


        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassRegData()
        Dim intSubClass As Integer
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"
        Dim intDisc As Integer = 0

        Try
            dsStuInfo.Clear()
            dtClassReg.Clear()
            dtClassReg2.Clear()
            dtPayRec.Clear()
            dtDisc.Clear()

            dsStuInfo = objCsol.DisplayStudentInfo(strStuId)
            dtClassReg = dsStuInfo.Tables(c_StuClassTableName)
            dtClassReg2 = dtClassReg.Clone
            dtClassReg2.Merge(dtClassReg)
            dtPayRec = dsStuInfo.Tables(c_StuPayRecTableName)
            dtPayRec.Columns.Add(c_PayMethodNameColumnName, GetType(System.String))
            For Each row As DataRow In dtPayRec.Rows
                Select Case row.Item(c_PayMethodColumnName)
                    Case c_PayMethodCash
                        row.Item(c_PayMethodNameColumnName) = My.Resources.cash
                    Case c_PayMethodCheque
                        row.Item(c_PayMethodNameColumnName) = My.Resources.cheque
                    Case c_PayMethodTransfer
                        row.Item(c_PayMethodNameColumnName) = My.Resources.transfer
                    Case c_PayMethodCreditCard
                        row.Item(c_PayMethodNameColumnName) = "其他"
                    Case c_PayMethodKeep
                        row.Item(c_PayMethodNameColumnName) = My.Resources.keepReceipt
                End Select
            Next
            dtDisc = dsStuInfo.Tables(c_StuDiscTableName)

            InitTable()

            For i = 0 To dtClassReg2.Rows.Count - 1
                intSubClass = dtClassReg2.Rows(i).Item(c_SubClassIDColumnName)
                intTotal = frmMain.GetClassFee(intSubClass)
                strCompute2 = c_SubClassIDColumnName & "=" & intSubClass.ToString
                If dtPayRec.Compute(strCompute3, strCompute2) > 0 Then
                    intTotal2 = dtPayRec.Compute(strCompute5, strCompute2) 'discount
                    intTotal3 = dtPayRec.Compute(strCompute1, strCompute2) 'pay
                    intTotal4 = dtPayRec.Compute(strCompute4, strCompute2) 'back
                Else
                    intTotal2 = 0
                    intTotal3 = 0
                    intTotal4 = 0
                End If

                If dtDisc.Compute(strCompute6, strCompute2) > 0 Then
                    intTotal2 = intTotal2 + dtDisc.Compute(strCompute5, strCompute2) 'discount
                End If

                dtClassReg2.Rows(i).Item(c_AmountColumnName) = intTotal - intTotal2
                dtClassReg2.Rows(i).Item(c_PayAmountColumnName) = intTotal3 - intTotal4
                dtClassReg2.Rows(i).Item(c_FeeOweCalculatedColumnName) = intTotal - intTotal2 - _
                                intTotal3 + intTotal4
                dtClassReg2.Rows(i).Item(c_DiscountColumnName) = intTotal2
            Next

            RefreshClassTable()
            dgvClass2.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvClass2.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvClass2.Columns(c_FeeOweCalculatedColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvClass2.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"

            dgvPayDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            blNewClassReg = False

            RefreshStuSubClass()

            LoadColumnTextClass()
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub RefreshAllData(ByVal strId As String)
        RefreshData(strId)
        'RefreshClassRegData()
        RefreshKBData()
        RefreshAttendanceData()
        RefreshBookData()
        RefreshClassChangeData()
        RefreshTeleInterviewData()
        RefreshStuInfo()
        RefreshAssignData()
        RefreshPaperData()
    End Sub


    Friend Sub RefreshData(ByVal strId As String)
        strStuId = strId
        dtSc = frmMain.GetSubClassList

        Try
            dsStuInfo = objCsol.DisplayStudentInfo(strStuId)
            dtStuInfo = dsStuInfo.Tables(c_StuInfoTableName)
            dtPayRec = dsStuInfo.Tables(c_StuPayRecTableName)

            dtPayRec.Columns.Add(c_PayMethodNameColumnName, GetType(System.String))
            For Each row As DataRow In dtPayRec.Rows
                Select Case row.Item(c_PayMethodColumnName)
                    Case c_PayMethodCash
                        row.Item(c_PayMethodNameColumnName) = My.Resources.cash
                    Case c_PayMethodCheque
                        row.Item(c_PayMethodNameColumnName) = My.Resources.cheque
                    Case c_PayMethodTransfer
                        row.Item(c_PayMethodNameColumnName) = My.Resources.transfer
                    Case c_PayMethodCreditCard
                        row.Item(c_PayMethodNameColumnName) = My.Resources.creditCard
                    Case c_PayMethodKeep
                        row.Item(c_PayMethodNameColumnName) = My.Resources.keepReceipt
                End Select
            Next
            dtSib = dsStuInfo.Tables(c_StuSibTableName)

            dtSch = frmMain.GetSchList
            dtSchGrp = frmMain.GetSchGrpList
            dtSibType = frmMain.GetSibTypeList
            For index As Integer = 0 To dtSch.Rows.Count - 1
                Select Case dtSch.Rows(index).Item(c_TypeIdColumnName)
                    Case 1
                        cboxPrimarySch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                        lstPrimary.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    Case 2
                        cboxJuniorSch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                        lstJunior.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    Case 3
                        cboxHighSch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                        lstHigh.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    Case 4
                        cboxUniversity.Items.Add(dtSch.Rows(index).Item(c_NameColumnName).trim)
                        lstUniversity.Add(dtSch.Rows(index).Item(c_IDColumnName))
                End Select
            Next
            cboxGroup.Items.Clear()
            lstSchGrp.Clear()
            For index As Integer = 0 To dtSchGrp.Rows.Count - 1
                cboxGroup.Items.Add(dtSchGrp.Rows(index).Item(c_GroupColumnName))
                lstSchGrp.Add(dtSchGrp.Rows(index).Item(c_IDColumnName))
            Next
            If cboxFamily1.Items.Count = 0 Then
                For index As Integer = 0 To dtSibType.Rows.Count - 1
                    cboxFamily1.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
                    cboxFamily2.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
                    cboxFamily3.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
                    cboxFamily4.Items.Add(dtSibType.Rows(index).Item(c_AppellationColumnName))
                    lstSibType.Add(dtSibType.Rows(index).Item(c_IDColumnName))
                Next
            End If

            RefreshStuInfo()
            RefreshClassRegData()

            butSavePic.Enabled = False

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub RefreshKBData()
        Dim dtKBPayRec As New DataTable
        dtPayRec = dsStuInfo.Tables(c_StuPayRecTableName)       '100206
        dtKBRec = objCsol.ListKBByStuId(strStuId)
        dtKBPayRec = dsStuInfo.Tables(c_StuPayRecTableName)
        dgvKB.DataSource = dtKBRec

        dtKBPay = New DataTable
        dtKBPay = dtKBPayRec.Clone
        dtKBPay.Merge(dtKBPayRec)
        Dim intClass As Integer
        If dgvKB.Rows.Count > 0 Then
            intClass = dgvKB.Rows(0).Cells(c_ClassIDColumnName).Value
            'dtKBPay.DefaultView.RowFilter = c_ClassIDColumnName & "=" & intClass.ToString
        End If

        dgvKBPayRec.DataSource = dtKBPay.DefaultView           '100206
        LoadColumnTextKB()
    End Sub

    Private Sub LoadColumnTextKB()
        For Each col In dgvKB.Columns
            col.visible = False
        Next
        If dgvKB.Rows.Count > 0 Then

            dgvKB.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvKB.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvKB.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvKB.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 1
            dgvKB.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvKB.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvKB.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvKB.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvKB.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvKB.Columns.Item(c_BackAmountColumnName).DisplayIndex = 3
            dgvKB.Columns.Item(c_BackAmountColumnName).Visible = True
            dgvKB.Columns.Item(c_BackAmountColumnName).HeaderText = "退費金額"
            dgvKB.Columns.Item(c_ReasonColumnName).DisplayIndex = 6
            dgvKB.Columns.Item(c_ReasonColumnName).Visible = True
            dgvKB.Columns.Item(c_ReasonColumnName).HeaderText = "退費原因"
            dgvKB.Columns.Item(c_HandlerColumnName).DisplayIndex = 7
            dgvKB.Columns.Item(c_HandlerColumnName).Visible = True
            dgvKB.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
            dgvKB.Columns(c_BackAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvKB.Columns(c_KeepAmountColumnName).DefaultCellStyle.Format = "$#,##0"
        End If
        For Each col In dgvKBPayRec.Columns
            col.visible = False
        Next
        If dgvKBPayRec.RowCount > 0 Then
            dgvKBPayRec.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgvKBPayRec.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName

            dgvKBPayRec.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 1
            dgvKBPayRec.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum

            dgvKBPayRec.Columns.Item(c_AmountColumnName).DisplayIndex = 2
            dgvKBPayRec.Columns.Item(c_AmountColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.payAmount

            dgvKBPayRec.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
            dgvKBPayRec.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.payDate

            dgvKBPayRec.Columns.Item(c_HandlerColumnName).DisplayIndex = 4
            dgvKBPayRec.Columns.Item(c_HandlerColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.handler

            dgvKBPayRec.Columns.Item(c_PayMethodNameColumnName).DisplayIndex = 5
            dgvKBPayRec.Columns.Item(c_PayMethodNameColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_PayMethodNameColumnName).HeaderText = My.Resources.payMethod

            dgvKBPayRec.Columns.Item(c_ExtraColumnName).DisplayIndex = 6
            dgvKBPayRec.Columns.Item(c_ExtraColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra                              '100206

            dgvKBPayRec.Columns.Item(c_BackAmountColumnName).DisplayIndex = 7
            dgvKBPayRec.Columns.Item(c_BackAmountColumnName).Visible = True
            dgvKBPayRec.Columns.Item(c_BackAmountColumnName).HeaderText = "退費"

            dgvKBPayRec.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
        End If
    End Sub

    Private Sub LoadColumnTextAtt()
        For Each col In dgvAttClass.Columns
            col.visible = False
        Next
        If dgvAttClass.Rows.Count > 0 Then
            dgvAttClass.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvAttClass.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvAttClass.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvAttClass.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvAttClass.Columns.Item(c_NameColumnName).Visible = True
            dgvAttClass.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
            dgvAttClass.Columns.Item("Content").HeaderText = "上課內容"
            dgvAttClass.Columns.Item("Content").DisplayIndex = 2
            dgvAttClass.Columns.Item("Content").Visible = True
            dgvAttClass.Columns.Item("PunchDate").HeaderText = "刷卡日期"
            dgvAttClass.Columns.Item("PunchDate").DisplayIndex = 3
            dgvAttClass.Columns.Item("PunchDate").Visible = True
            dgvAttClass.Columns.Item("StartTime").HeaderText = "上課時間"
            dgvAttClass.Columns.Item("StartTime").DisplayIndex = 4
            dgvAttClass.Columns.Item("StartTime").Visible = True
            dgvAttClass.Columns.Item("EndTime").HeaderText = "下課時間"
            dgvAttClass.Columns.Item("EndTime").DisplayIndex = 5
            dgvAttClass.Columns.Item("EndTime").Visible = True
            dgvAttClass.Columns.Item("MakeUpClassID2").HeaderText = "補課於"
            dgvAttClass.Columns.Item("MakeUpClassID2").DisplayIndex = 6
            dgvAttClass.Columns.Item("MakeUpClassID2").Visible = True
        End If

        For Each col In dgvAttRec.Columns
            col.visible = False
        Next
        If dgvAttRec.Rows.Count > 0 Then
            dgvAttRec.Columns.Item("PunchDate").HeaderText = "刷卡日期"
            dgvAttRec.Columns.Item("PunchDate").DisplayIndex = 0
            dgvAttRec.Columns.Item("PunchDate").Visible = True
            dgvAttRec.Columns.Item("StartTime").HeaderText = "上課時間"
            dgvAttRec.Columns.Item("StartTime").DisplayIndex = 1
            dgvAttRec.Columns.Item("StartTime").Visible = True
            dgvAttRec.Columns.Item("EndTime").HeaderText = "下課時間"
            dgvAttRec.Columns.Item("EndTime").DisplayIndex = 2
            dgvAttRec.Columns.Item("EndTime").Visible = True
        End If
        For Each col In dgvKBAttendance.Columns
            col.visible = False
        Next
        If dgvKBAttendance.Rows.Count > 0 Then
            dgvKBAttendance.Columns.Item(c_NameColumnName).DisplayIndex = 0
            dgvKBAttendance.Columns.Item(c_NameColumnName).Visible = True
            dgvKBAttendance.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
            dgvKBAttendance.Columns.Item(c_StartColumnName).DisplayIndex = 1
            dgvKBAttendance.Columns.Item(c_StartColumnName).Visible = True
            dgvKBAttendance.Columns.Item(c_StartColumnName).HeaderText = My.Resources.datetime
            dgvKBAttendance.Columns.Item(c_InColumnName).DisplayIndex = 2
            dgvKBAttendance.Columns.Item(c_InColumnName).Visible = True
            dgvKBAttendance.Columns.Item(c_InColumnName).HeaderText = My.Resources.punchType
        End If
    End Sub

    Private Sub LoadColumnTextBook()
        For Each col In dgvKBBook.Columns
            col.visible = False
        Next
        If dgvKBBook.Rows.Count > 0 Then

            dgvKBBook.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvKBBook.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvKBBook.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvKBBook.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvKBBook.Columns.Item(c_NameColumnName).Visible = True
            dgvKBBook.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
            dgvKBBook.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvKBBook.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvKBBook.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.bookDate
        End If
        For Each col In dgvBookNotReceive.Columns
            col.visible = False
        Next
        If dgvBookNotReceive.Rows.Count > 0 Then

            dgvBookNotReceive.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvBookNotReceive.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvBookNotReceive.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvBookNotReceive.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvBookNotReceive.Columns.Item(c_NameColumnName).Visible = True
            dgvBookNotReceive.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
        End If

        For Each col In dgvBookReceive.Columns
            col.visible = False
        Next
        If dgvBookReceive.Rows.Count > 0 Then

            dgvBookReceive.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvBookReceive.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvBookReceive.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvBookReceive.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvBookReceive.Columns.Item(c_NameColumnName).Visible = True
            dgvBookReceive.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
            dgvBookReceive.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvBookReceive.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvBookReceive.Columns.Item(c_DateTimeColumnName).HeaderText = "領取時間"

        End If
    End Sub

    Private Sub InitSelection(ByRef cbox As ComboBox)
        If cbox.Items.Count > 0 Then
            cbox.SelectedIndex = 0
        End If
    End Sub

    Private Sub InitTable()
        dtClassReg2.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtClassReg2.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
        dtClassReg2.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
        dtClassReg2.Columns.Add(c_FeeOweCalculatedColumnName, GetType(System.Int32))
    End Sub

    Private Sub RefreshStuInfo()
        If dtStuInfo.Rows.Count > 0 Then
            lblStuId.Text = strStuId
            strCardNum = dtStuInfo.Rows(0).Item(c_CardNumColumnName).trim
            tboxCreateDate.Text = dtStuInfo.Rows(0).Item(c_CreateDateColumnName).ToString.Trim
            If strCardNum = Nothing Then
                butIssueCard.Text = My.Resources.issueCard
                strCardNum = ""
            Else
                If strCardNum.Length < 6 Then
                    butIssueCard.Text = My.Resources.issueCard
                    strCardNum = ""
                Else
                    butIssueCard.Text = My.Resources.issueCardAgain
                End If
            End If
            tboxStuName.Text = dtStuInfo.Rows(0).Item(c_NameColumnName).trim
            Header = tboxStuName.Text & My.Resources.frmStuInfo
            Me.Text = tboxStuName.Text.Trim & My.Resources.frmStuInfo
            Dim i As Integer = dtStuInfo.Rows(0).Item(c_StuTypeColumnName)
            If i = c_StuTypeFormal Then
                radbutFormalStu.Checked = True
            ElseIf i = c_StuTypePotential Then
                radbutPotStu.Checked = True
            End If

            Dim s As String = dtStuInfo.Rows(0).Item(c_SexColumnName).trim
            If s = My.Resources.male Then
                cboxSex.SelectedIndex = 0
            Else
                cboxSex.SelectedIndex = 1
            End If

            tboxEngName.Text = dtStuInfo.Rows(0).Item(c_EngNameColumnName).trim
            If DBNull.Value.Equals(dtStuInfo.Rows(0).Item(c_BirthdayColumnName)) Then
                tboxBirthY.Text = ""
                tboxBirthM.Text = ""
                tboxBirthD.Text = ""
            ElseIf CDate(dtStuInfo.Rows(0).Item(c_BirthdayColumnName)) = GetMinDate() Then
                tboxBirthY.Text = ""
                tboxBirthM.Text = ""
                tboxBirthD.Text = ""
            Else
                tboxBirthY.Text = GetROCYear(CDate(dtStuInfo.Rows(0).Item(c_BirthdayColumnName))).ToString
                tboxBirthM.Text = CDate(dtStuInfo.Rows(0).Item(c_BirthdayColumnName)).Month.ToString
                tboxBirthD.Text = CDate(dtStuInfo.Rows(0).Item(c_BirthdayColumnName)).Day.ToString
            End If

            i = lstPrimary.IndexOf(dtStuInfo.Rows(0).Item(c_PrimarySchColumnName))
            If i > -1 Then
                cboxPrimarySch.SelectedIndex = i
            Else
                cboxPrimarySch.SelectedIndex = -1
            End If

            i = lstJunior.IndexOf(dtStuInfo.Rows(0).Item(c_JuniorSchColumnName))
            If i > -1 Then
                cboxJuniorSch.SelectedIndex = i
            Else
                cboxJuniorSch.SelectedIndex = -1
            End If

            i = lstHigh.IndexOf(dtStuInfo.Rows(0).Item(c_HighSchColumnName))
            If i > -1 Then
                cboxHighSch.SelectedIndex = i
            Else
                cboxHighSch.SelectedIndex = -1
            End If

            i = lstUniversity.IndexOf(dtStuInfo.Rows(0).Item(c_UniversityColumnName))
            If i > -1 Then
                cboxUniversity.SelectedIndex = i
            Else
                cboxUniversity.SelectedIndex = -1
            End If

            i = dtStuInfo.Rows(0).Item(c_SchoolGradeColumnName)
            Dim currentSch As Integer
            If Not dtStuInfo.Rows(0).Item(c_CurrentSchColumnName) Then
                currentSch = CInt(dtStuInfo.Rows(0).Item(c_CurrentSchColumnName))
            End If

            If currentSch = 1 And i > 5 Then
                cboxGrade.Text = "已畢業"
            ElseIf currentSch = 2 And i > 2 Then
                cboxGrade.Text = "已畢業"
            ElseIf currentSch = 3 And i > 2 Then
                cboxGrade.Text = "已畢業"
            ElseIf currentSch = 4 And i > 3 Then
                cboxGrade.Text = "已畢業"
            Else
                cboxGrade.SelectedIndex = i
                '    If i > 0 Then
                '        If i > 12 Then
                '            i = 12
                '        End If
                '        cboxGrade.SelectedIndex = i - 1
                '    Else
                '        cboxGrade.SelectedIndex = -1
                '    End If
            End If





            'Dim strArr() As String                                                              
            'Dim createTime As String = dtStuInfo.Rows(0).Item(c_CreateDateColumnName)
            'strArr = createTime.Split("/")
            'Dim createYear As Integer = CInt(strArr(0))
            'Dim createMonth As Integer = CInt(strArr(1))






            'Dim createTime As Date = Date.Parse(dtStuInfo.Rows(0).Item(c_CreateDateColumnName))
            'Dim createYear As Integer = createTime.Year
            'Dim createMonth As Integer = createTime.Month

            'Dim nowYear As Integer = CInt(Now.Year)
            'Dim nowMonth As Integer = CInt(Now.Month)
            'Dim years As Integer
            'years = nowYear - createYear
            'If Not createMonth = 7 Then
            '    If nowMonth >= 7 Then
            '        If 7 - createMonth >= 0 Then
            '            i = i + years + 1
            '        End If
            '    ElseIf createMonth >= 7 And nowMonth <= 6 Then
            '        i = i + years - 1
            '    Else
            '        i = i + years
            '    End If
            'ElseIf createMonth = 7 Then
            '    i = i + nowYear - createYear
            'Else
            '    i = i + years
            'End If

            'If currentSch = 1 And i > 6 Then
            '    cboxGrade.Text = "已畢業"
            'ElseIf currentSch = 2 And i > 6 Then
            '    cboxGrade.Text = "已畢業"
            'ElseIf currentSch = 3 And i > 6 Then
            '    cboxGrade.Text = "已畢業"
            'ElseIf currentSch = 4 And i > 4 Then
            '    cboxGrade.Text = "已畢業"
            'Else
            '    If i > 0 Then
            '        If i > 12 Then
            '            i = 12
            '        End If
            '        cboxGrade.SelectedIndex = i - 1
            '    Else
            '        cboxGrade.SelectedIndex = -1
            '    End If
            'End If








            tboxClass.Text = dtStuInfo.Rows(0).Item(c_SchoolClassColumnName).trim

            Dim intG As Integer = dtStuInfo.Rows(0).Item(c_SchGroupColumnName)
            i = lstSchGrp.IndexOf(intG)
            If i > -1 Then
                cboxGroup.SelectedIndex = i
            Else
                cboxGroup.SelectedIndex = -1
            End If

            i = GetIntValue(dtStuInfo.Rows(0).Item(c_CurrentSchColumnName))
            Select Case i
                Case 1
                    radbutNowPri.Checked = True
                Case 2
                    radbutNowJun.Checked = True
                Case 3
                    radbutNowHig.Checked = True
                Case 4
                    radbutNowUni.Checked = True

            End Select

            tboxTel1.Text = dtStuInfo.Rows(0).Item(c_Tel1ColumnName).trim
            tboxTel2.Text = dtStuInfo.Rows(0).Item(c_Tel2ColumnName).trim
            tboxOfficeTel.Text = dtStuInfo.Rows(0).Item(c_OfficeTelColumnName).trim
            tboxMobile.Text = dtStuInfo.Rows(0).Item(c_MobileColumnName).trim
            tboxDadName.Text = dtStuInfo.Rows(0).Item(c_DadNameColumnName).trim
            tboxMumName.Text = dtStuInfo.Rows(0).Item(c_MumNameColumnName).trim
            tboxDadMobile.Text = dtStuInfo.Rows(0).Item(c_DadMobileColumnName).trim
            tboxMumMobile.Text = dtStuInfo.Rows(0).Item(c_MumMobileColumnName).trim
            tboxIntroId.Text = dtStuInfo.Rows(0).Item(c_IntroIDColumnName).trim
            tboxIntroName.Text = dtStuInfo.Rows(0).Item(c_IntroNameColumnName).trim
            tboxGradJunior.Text = dtStuInfo.Rows(0).Item(c_GraduateFromColumnName).trim
            tboxIc.Text = dtStuInfo.Rows(0).Item(c_ICColumnName).trim
            tboxDadTitle.Text = dtStuInfo.Rows(0).Item(c_DadTitleColumnName).trim
            tboxMumTitle.Text = dtStuInfo.Rows(0).Item(c_MumTitleColumnName).trim
            tboxRegInfo.Text = dtStuInfo.Rows(0).Item(c_Cust1ColumnName).trim
            tboxClassId.Text = dtStuInfo.Rows(0).Item(c_Cust2ColumnName).trim
            tboxStay.Text = dtStuInfo.Rows(0).Item(c_Cust3ColumnName).trim
            tboxAdd1a.Text = dtStuInfo.Rows(0).Item(c_PostalCodeColumnName).trim
            tboxAdd1b.Text = dtStuInfo.Rows(0).Item(c_AddressColumnName).trim
            tboxAdd2a.Text = dtStuInfo.Rows(0).Item(c_PostalCode2ColumnName).trim
            tboxAdd2b.Text = dtStuInfo.Rows(0).Item(c_Address2ColumnName).trim
            tboxEmail.Text = dtStuInfo.Rows(0).Item(c_EmailColumnName).trim
            rtboxRemarks.Text = dtStuInfo.Rows(0).Item(c_RemarksColumnName).trim
            tboxEnrollSch.Text = GetDbString(dtStuInfo.Rows(0).Item(c_EnrollSchColumnName)).Trim
            tboxHseeMark.Text = GetDbString(dtStuInfo.Rows(0).Item(c_HseeMarkColumnName)).Trim
            tboxPunchCardNum.Text = GetDbString(dtStuInfo.Rows(0).Item(c_PunchCardNumColumnName)).Trim

            Dim s2 As String
            Dim s3 As String
            Dim s4 As String
            Dim i2 As String
            Dim i3 As String
            Dim i4 As String
            Dim i5 As Integer

            'If dtSib.Rows.Count = 0 Then
            cboxFamily1.SelectedIndex = -1
            tboxName1.Clear()
            tboxBirth1y.Clear()
            tboxBirth1m.Clear()
            tboxBirth1d.Clear()
            tboxSchool1.Clear()
            tboxRank1.Clear()
            tboxRemark1.Clear()

            cboxFamily2.SelectedIndex = -1
            tboxName2.Clear()
            tboxBirth2y.Clear()
            tboxBirth2m.Clear()
            tboxBirth2d.Clear()
            tboxSchool2.Clear()
            tboxRank2.Clear()
            tboxRemark2.Clear()

            cboxFamily3.SelectedIndex = -1
            tboxName3.Clear()
            tboxBirth3y.Clear()
            tboxBirth3m.Clear()
            tboxBirth3d.Clear()
            tboxSchool3.Clear()
            tboxRank3.Clear()
            tboxRemark3.Clear()

            cboxFamily4.SelectedIndex = -1
            tboxName4.Clear()
            tboxBirth4y.Clear()
            tboxBirth4m.Clear()
            tboxBirth4d.Clear()
            tboxSchool4.Clear()
            tboxRank4.Clear()
            tboxRemark4.Clear()
            'End If

            For index As Integer = 0 To dtSib.Rows.Count - 1
                i = lstSibType.IndexOf(CInt(dtSib.Rows(index).Item(c_SibTypeColumnName)))
                s2 = dtSib.Rows(index).Item(c_NameColumnName).trim
                If Not s2 = "" Then                                                                                 '100226

                    If DBNull.Value.Equals(dtSib.Rows(index).Item(c_BirthdayColumnName)) Then
                        i2 = ""
                        i3 = ""
                        i4 = ""
                    ElseIf CDate(dtSib.Rows(index).Item(c_BirthdayColumnName)) = GetMinDate() Then
                        i2 = ""
                        i3 = ""
                        i4 = ""
                    Else
                        i2 = GetROCYear(CDate(dtSib.Rows(index).Item(c_BirthdayColumnName))).ToString
                        i3 = CDate(dtSib.Rows(index).Item(c_BirthdayColumnName)).Month.ToString
                        i4 = CDate(dtSib.Rows(index).Item(c_BirthdayColumnName)).Day.ToString
                    End If

                    s3 = dtSib.Rows(index).Item(c_SchoolColumnName).trim
                    i5 = dtSib.Rows(index).Item(c_RankColumnName)
                    s4 = dtSib.Rows(index).Item(c_RemarksColumnName).trim

                    If i > -1 Then
                        Select Case index
                            Case 0
                                cboxFamily1.SelectedIndex = i
                                tboxName1.Text = s2
                                tboxBirth1y.Text = i2
                                tboxBirth1m.Text = i3
                                tboxBirth1d.Text = i4
                                tboxSchool1.Text = s3
                                tboxRank1.Text = i5.ToString
                                tboxRemark1.Text = s4
                            Case 1
                                cboxFamily2.SelectedIndex = i
                                tboxName2.Text = s2
                                tboxBirth2y.Text = i2
                                tboxBirth2m.Text = i3
                                tboxBirth2d.Text = i4
                                tboxSchool2.Text = s3
                                tboxRank2.Text = i5.ToString
                                tboxRemark2.Text = s4
                            Case 2
                                cboxFamily3.SelectedIndex = i
                                tboxName3.Text = s2
                                tboxBirth3y.Text = i2
                                tboxBirth3m.Text = i3
                                tboxBirth3d.Text = i4
                                tboxSchool3.Text = s3
                                tboxRank3.Text = i5.ToString
                                tboxRemark3.Text = s4
                            Case 3
                                cboxFamily4.SelectedIndex = i
                                tboxName4.Text = s2
                                tboxBirth4y.Text = i2
                                tboxBirth4m.Text = i3
                                tboxBirth4d.Text = i4
                                tboxSchool4.Text = s3
                                tboxRank4.Text = i5.ToString
                                tboxRemark4.Text = s4
                        End Select
                    End If
                End If
            Next

            If strStuId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                    lblImgInfo.Text = ""
                Else
                    '20100424 imgdb by sherry start
                    Dim result As Byte() = objCsol.DownloadImg(strStuId)
                    If Not result Is Nothing Then
                        If result.Length > 0 Then
                            Dim ms As MemoryStream = New MemoryStream(result, 0, result.Length)
                            Dim im As Image = Image.FromStream(ms)
                            picbox.Image = im

                            Dim bm_source As New Bitmap(picbox.Image)
                            Dim bm_dest As New Bitmap( _
                            CInt(bm_source.Width * 0.25), _
                            CInt(bm_source.Height * 0.25))

                            ' Make a Graphics object for the result Bitmap.
                            Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

                            ' Copy the source image into the destination bitmap.
                            gr_dest.DrawImage(bm_source, 0, 0, _
                                bm_dest.Width + 1, _
                                bm_dest.Height + 1)

                            bm_dest.Save(f, Imaging.ImageFormat.Bmp)
                            lblImgInfo.Text = ""
                        Else
                            picbox.Image = Nothing
                            lblImgInfo.Text = My.Resources.noPhoto
                        End If
                    Else
                        picbox.Image = Nothing
                        lblImgInfo.Text = My.Resources.noPhoto
                    End If
                    '20100424 imgdb by sherry end
                End If
            End If
        End If
    End Sub

    Private Sub LoadColumnTextClass()
        For Each col In dgvClass.Columns
            col.visible = False
        Next
        If dgvClass.ColumnCount > 0 Then
            dgvClass.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvClass.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvClass.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvClass.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvClass.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 1
            dgvClass.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvClass.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgvClass.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
            dgvClass.Columns.Item(c_SeatNumColumnName).Visible = True
            dgvClass.Columns.Item(c_SalesPersonColumnName).HeaderText = My.Resources.sales
            dgvClass.Columns.Item(c_SalesPersonColumnName).DisplayIndex = 3
            dgvClass.Columns.Item(c_SalesPersonColumnName).Visible = True
            dgvClass.Columns.Item(c_RegDateColumnName).HeaderText = My.Resources.regDate
            dgvClass.Columns.Item(c_RegDateColumnName).DisplayIndex = 4
            dgvClass.Columns.Item(c_RegDateColumnName).Visible = True
            dgvClass.Columns.Item(c_RegDateColumnName).DefaultCellStyle.Format = "d"
            If dgvClass.Columns.Contains("cancel2") Then
                dgvClass.Columns.Item("cancel2").HeaderText = My.Resources.cancel
                dgvClass.Columns.Item("cancel2").Visible = True
                dgvClass.Columns.Item("cancel2").DisplayIndex = 5
            End If
        End If

        For Each col In dgvClass2.Columns
            col.visible = False
        Next
        If dgvClass2.ColumnCount > 0 Then
            dgvClass2.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvClass2.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvClass2.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgvClass2.Columns.Item(c_SalesPersonColumnName).HeaderText = My.Resources.sales
            dgvClass2.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.shouldPay
            dgvClass2.Columns.Item(c_PayAmountColumnName).HeaderText = My.Resources.paid
            dgvClass2.Columns.Item(c_FeeOweCalculatedColumnName).HeaderText = My.Resources.feeOwe
            dgvClass2.Columns.Item(c_CancelColumnName).HeaderText = My.Resources.cancel
            dgvClass2.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvClass2.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvClass2.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 1
            dgvClass2.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvClass2.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
            dgvClass2.Columns.Item(c_SeatNumColumnName).Visible = True
            dgvClass2.Columns.Item(c_SalesPersonColumnName).DisplayIndex = 3
            dgvClass2.Columns.Item(c_SalesPersonColumnName).Visible = True
            dgvClass2.Columns.Item(c_AmountColumnName).DisplayIndex = 4
            dgvClass2.Columns.Item(c_AmountColumnName).Visible = True
            dgvClass2.Columns.Item(c_PayAmountColumnName).DisplayIndex = 5
            dgvClass2.Columns.Item(c_PayAmountColumnName).Visible = True
            dgvClass2.Columns.Item(c_FeeOweCalculatedColumnName).DisplayIndex = 6
            dgvClass2.Columns.Item(c_FeeOweCalculatedColumnName).Visible = True
        End If

        For Each col In dgvPayDetails.Columns
            col.visible = False
        Next
        If dgvPayDetails.ColumnCount > 0 Then
            dgvPayDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
            dgvPayDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 1
            dgvPayDetails.Columns.Item(c_AmountColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgvPayDetails.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 3
            dgvPayDetails.Columns.Item(c_HandlerColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_PayMethodNameColumnName).DisplayIndex = 4
            dgvPayDetails.Columns.Item(c_PayMethodNameColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_ExtraColumnName).DisplayIndex = 5
            dgvPayDetails.Columns.Item(c_ExtraColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgvPayDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.payAmount
            dgvPayDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.payDate
            dgvPayDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.handler
            dgvPayDetails.Columns.Item(c_PayMethodNameColumnName).HeaderText = My.Resources.payMethod
            dgvPayDetails.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra

            dgvPayDetails.Columns.Item(c_BackAmountColumnName).DisplayIndex = 6                                  '100304
            dgvPayDetails.Columns.Item(c_BackAmountColumnName).Visible = True
            dgvPayDetails.Columns.Item(c_BackAmountColumnName).HeaderText = My.Resources.backAmount              '100304

        End If


    End Sub

    'Private Sub LoadColumnAssignData()
    '    For Each col In dgvAssignPass.Columns
    '        col.visible = False
    '    Next
    '    If dgvChange.Rows.Count > 0 Then
    '        dgvChange.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
    '        dgvChange.Columns.Item(c_ClassNameColumnName).Visible = True
    '        dgvChange.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
    '        dgvChange.Columns.Item(c_OldSubClassNameColumnName).DisplayIndex = 1
    '        dgvChange.Columns.Item(c_OldSubClassNameColumnName).Visible = True
    '        dgvChange.Columns.Item(c_OldSubClassNameColumnName).HeaderText = My.Resources.oldSubClassName
    '        dgvChange.Columns.Item(c_OldSeatNumColumnName).DisplayIndex = 2
    '        dgvChange.Columns.Item(c_OldSeatNumColumnName).Visible = True
    '        dgvChange.Columns.Item(c_OldSeatNumColumnName).HeaderText = My.Resources.oldSeatNum
    '        dgvChange.Columns.Item(c_NewSubClassNameColumnName).DisplayIndex = 3
    '        dgvChange.Columns.Item(c_NewSubClassNameColumnName).Visible = True
    '        dgvChange.Columns.Item(c_NewSubClassNameColumnName).HeaderText = My.Resources.newSubClassName
    '        dgvChange.Columns.Item(c_SeatNumColumnName).DisplayIndex = 4
    '        dgvChange.Columns.Item(c_SeatNumColumnName).Visible = True
    '        dgvChange.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.newSeatNum
    '        dgvChange.Columns.Item(c_ChgdOnDateColumnName).DisplayIndex = 5
    '        dgvChange.Columns.Item(c_ChgdOnDateColumnName).Visible = True
    '        dgvChange.Columns.Item(c_ChgdOnDateColumnName).HeaderText = My.Resources.classChangeDate

    '    End If
    'End Sub

    Private Sub LoadColumnTextClassChange()
        For Each col In dgvChange.Columns
            col.visible = False
        Next
        If dgvChange.Rows.Count > 0 Then
            dgvChange.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvChange.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvChange.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvChange.Columns.Item(c_OldSubClassNameColumnName).DisplayIndex = 1
            dgvChange.Columns.Item(c_OldSubClassNameColumnName).Visible = True
            dgvChange.Columns.Item(c_OldSubClassNameColumnName).HeaderText = My.Resources.oldSubClassName
            dgvChange.Columns.Item(c_OldSeatNumColumnName).DisplayIndex = 2
            dgvChange.Columns.Item(c_OldSeatNumColumnName).Visible = True
            dgvChange.Columns.Item(c_OldSeatNumColumnName).HeaderText = My.Resources.oldSeatNum
            dgvChange.Columns.Item(c_NewSubClassNameColumnName).DisplayIndex = 3
            dgvChange.Columns.Item(c_NewSubClassNameColumnName).Visible = True
            dgvChange.Columns.Item(c_NewSubClassNameColumnName).HeaderText = My.Resources.newSubClassName
            dgvChange.Columns.Item(c_SeatNumColumnName).DisplayIndex = 4
            dgvChange.Columns.Item(c_SeatNumColumnName).Visible = True
            dgvChange.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.newSeatNum
            dgvChange.Columns.Item(c_ChgdOnDateColumnName).DisplayIndex = 5
            dgvChange.Columns.Item(c_ChgdOnDateColumnName).Visible = True
            dgvChange.Columns.Item(c_ChgdOnDateColumnName).HeaderText = My.Resources.classChangeDate

        End If
    End Sub

    Private Sub InitSelections()

    End Sub

    Private Sub frmStuInfo_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(My.Resources.frmStuInfo)
    End Sub

    Private Sub dgvClass2_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvClass2.CellLeave
        tboxPayToday.Text = ""
    End Sub

    Private Sub dgvClass2_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClass2.SelectionChanged

        Dim intSubClassId As Integer
        If dgvClass2.SelectedRows.Count > 0 Then
            intSubClassId = dgvClass2.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            If intSubClassId > -1 Then
                dtPayRec.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intSubClassId.ToString
                dgvPayDetails.DataSource = dtPayRec.DefaultView
                RefreshOldDiscount(intSubClassId)
            Else
                dtPayRec.DefaultView.RowFilter = ""
                dgvPayDetails.DataSource = dtPayRec.DefaultView
            End If
        End If
        dgvPayDetails_SelectionChanged(Nothing, Nothing)
        'dgvClass2.Rows(0).Selected = True
    End Sub

    Private Sub RefreshPayDetails()
        Dim intSubClassId As Integer
        If dgvClass2.SelectedRows.Count > 0 Then
            intSubClassId = dgvClass2.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            If intSubClassId > -1 Then
                dtPayRec.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intSubClassId.ToString
                dgvPayDetails.DataSource = dtPayRec.DefaultView
                'RefreshOldDiscount(intSubClassId)
            Else
                dtPayRec.DefaultView.RowFilter = ""
                dgvPayDetails.DataSource = dtPayRec.DefaultView

            End If
        End If
        RefreshOldDiscount(intSubClassId)
    End Sub

    Private Sub RefreshOldDiscount(ByVal id As Integer)
        Dim d As Integer = 0
        Dim strR As String = ""
        Dim blFound As Boolean = False
        Dim intId As Integer = -1
        If dtDisc.Rows.Count > 0 Then
            For index As Integer = 0 To dtDisc.Rows.Count - 1
                If dtDisc.Rows(index).Item(c_SubClassIDColumnName) = id Then
                    d = dtDisc.Rows(index).Item(c_DiscountColumnName)
                    strR = dtDisc.Rows(index).Item(c_RemarksColumnName).ToString.Trim
                    intId = dtDisc.Rows(index).Item(c_IDColumnName)
                    blFound = True
                End If
            Next
            If blFound And Not d.ToString = "" And Not strR = "" Then                                   '100304
                tboxDiscount.Text = d.ToString
                tboxDiscountRemarks.Text = strR
                tboxDiscount.Enabled = False
                tboxDiscountRemarks.Enabled = False
                butModDisc.Enabled = True
                butModDisc.Text = My.Resources.modDisc
                intOldDisc = d
                strOldDiscRemarks = strR
                intOldDiscId = intId
            Else
                tboxDiscount.Text = ""
                tboxDiscountRemarks.Text = ""
                tboxDiscount.Enabled = True
                tboxDiscountRemarks.Enabled = True
                butModDisc.Enabled = False
                intOldDisc = -999999
                strOldDiscRemarks = ""
                intOldDiscId = -1
                objCsol.DeleteDisc(intId)                               '100304
            End If
        Else
            tboxDiscount.Text = ""
            tboxDiscountRemarks.Text = ""
            tboxReceiptRemarks.Text = ""
            tboxDiscount.Enabled = True
            tboxDiscountRemarks.Enabled = True
            butModDisc.Enabled = False
            intOldDisc = -999999
            strOldDiscRemarks = ""
            intOldDiscId = -1
        End If
    End Sub

    Private Sub radbutMethod1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutMethod1.CheckedChanged
        If radbutMethod1.Checked Then
            grpboxExtra.Visible = False
        End If
    End Sub

    Private Sub radbutMethod2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutMethod2.CheckedChanged
        If radbutMethod2.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = True
            lblCheque.Visible = True
            grpboxExtra.Text = My.Resources.chequeNum
        End If
    End Sub

    Private Sub radbutMethod3_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutMethod3.CheckedChanged
        If radbutMethod3.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = False
            lblCheque.Visible = False
            grpboxExtra.Text = My.Resources.transferAccnt
        End If
    End Sub

    Private Sub radbutMethod4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutMethod4.CheckedChanged
        If radbutMethod4.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = False
            lblCheque.Visible = False
            grpboxExtra.Text = "額外資訊"
        End If
    End Sub


    Private Sub radbutMethod5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutMethod5.CheckedChanged
        If radbutMethod5.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = False
            lblCheque.Visible = False
            grpboxExtra.Text = My.Resources.extraInfo

        End If
    End Sub

    Private Sub butPayAck_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayAck.Click
        frmMain.SetOkState(False)
        frmMain.SetCurrentString(tboxStuName.Text)
        frmMain.SetCurrentStu(strStuId)
        Dim frm As New frm2PayNotice
        frm.SetDt(dtClassReg2)
        frm.ShowDialog()
    End Sub

    Private Sub butPaySave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPaySave.Click
        Try

            Dim intScId As Integer
            Dim datePay As Date
            Dim dateGet As Date
            Dim intAmt As Integer = -1
            Dim intPayMethod As Integer
            Dim strExtra As String = ""
            Dim intDiscount As Integer = 0
            Dim strDiscountRemarks As String
            Dim strSales As String
            Dim dtChequeValid As Date = Now
            Dim intOwe As Integer = 0
            Dim blDiscFlag As Boolean = False



            If MessageBox.Show(My.Resources.msgChkAmount, My.Resources.msgRemindTitle, MessageBoxButtons.OKCancel) = MsgBoxResult.Ok Then
                If dgvClass2.Rows.Count > 0 Then
                    Dim idx As Integer = 0
                    If Not dgvClass2.CurrentCell Is Nothing Then
                        idx = dgvClass2.CurrentCell.RowIndex
                    End If
                    intScId = dgvClass2.Rows(idx).Cells(c_SubClassIDColumnName).Value
                    strSales = dgvClass2.Rows(idx).Cells(c_SalesIDColumnName).Value
                    intOwe = dgvClass2.Rows(idx).Cells(c_FeeOweCalculatedColumnName).Value
                    If radbutNormal.Checked Then
                        datePay = Now
                        dateGet = Now
                    ElseIf radbutMakeup.Checked Then
                        datePay = dtpickMakeupDate.Value.Date + Now.TimeOfDay
                        dateGet = dtpickMakeupDate.Value.Date + Now.TimeOfDay
                    End If

                    If tboxDiscount.Enabled Then
                        If IsNumeric(tboxDiscount.Text) Then
                            If Not frmMain.CheckAuth(30) Then
                                frmMain.ShowNoAuthMsg()
                                intDiscount = 0
                            Else
                                intDiscount = CInt(tboxDiscount.Text)
                            End If
                        Else
                            intDiscount = 0
                        End If
                        strDiscountRemarks = tboxDiscountRemarks.Text
                        If tboxDiscount.Text.Length > 0 And tboxDiscountRemarks.Text.Trim.Length < 5 Then
                            ShowMsg(My.Resources.msgDiscRemarksLength)
                            tboxDiscountRemarks.Select(0, tboxDiscountRemarks.Text.Length)
                            Exit Sub
                        End If
                    Else
                        intDiscount = 0
                        strDiscountRemarks = ""
                        intOldDisc = -999999
                        strOldDiscRemarks = ""
                    End If

                    If intOldDisc <> -999999 Then
                        If intOwe <= 0 And intDiscount > intOldDisc Then
                            ShowMsg(My.Resources.msgClassFeeClear)
                            Exit Sub

                        End If
                    Else
                        If intOwe < 0 And intDiscount >= 0 Then
                            ShowMsg(My.Resources.msgClassFeeClear)
                            Exit Sub

                        End If
                    End If

                    If IsNumeric(tboxPayToday.Text) Then
                        intAmt = CInt(tboxPayToday.Text)

                        If intAmt < 0 Then
                            If intDiscount = 0 And intOldDisc = -999999 Then
                                Exit Sub
                            Else
                                blDiscFlag = True
                            End If
                        End If
                    Else
                        If intDiscount = 0 And intOldDisc = -999999 Then
                            Exit Sub
                        Else
                            blDiscFlag = True
                        End If
                    End If
                    If radbutMethod1.Checked Then
                        intPayMethod = c_PayMethodCash
                    Else
                        strExtra = tboxPayExtraInfo.Text
                        If radbutMethod2.Checked Then
                            intPayMethod = c_PayMethodCheque
                            dtChequeValid = dtpickCheque.Value
                        ElseIf radbutMethod3.Checked Then
                            intPayMethod = c_PayMethodTransfer
                        ElseIf radbutMethod4.Checked Then
                            intPayMethod = c_PayMethodCreditCard
                        ElseIf radbutMethod5.Checked Then
                            intPayMethod = c_PayMethodKeep
                        End If
                    End If
                    If strStuId.Length = 8 And intScId > -1 Then
                        Dim dtDate As Date

                        If radbutNormal.Checked Then
                            dtDate = Now
                        Else
                            dtDate = dtpickMakeupDate.Value
                        End If
                        'Dim ss As String = dtDate.ToString("MM/dd/yyyy hh:mm:ss.ffff")
                        'tboxReceiptNum.Text = objCsol.GenReceiptNum(GetDateStart(dtDate), GetDateEnd(dtDate))
                        '100年錯誤修正

                       
                        tboxReceiptNum.Text = objCsol.GenReceiptNum(dtDate)


                        objCsol.AddPay(tboxReceiptNum.Text, strStuId, intScId, _
                            intAmt, datePay, frmMain.GetUsrId, strSales, intPayMethod, _
                            tboxPayExtraInfo.Text, tboxReceiptRemarks.Text, dtChequeValid, _
                            intDiscount, tboxDiscountRemarks.Text, intOldDisc, strOldDiscRemarks, intOldDiscId, dateGet.ToString("yyyy/MM/dd"))

                        ShowMsg(My.Resources.msgSaveComplete)
                        tboxPayExtraInfo.Text = ""
                        tboxReceiptRemarks.Text = ""
                        tboxDiscountRemarks.Text = ""
                        tboxPayToday.Text = ""
                        intDiscount = 0
                        radbutNormal.Checked = True
                        tboxDiscount.Text = ""
                        radbutMethod1.Checked = True
                        RefreshData(strStuId)
                        For Each row As DataGridViewRow In dgvClass2.Rows
                            If row.Cells(c_SubClassIDColumnName).Value = intScId Then
                                row.Selected = True
                                dgvClass2.CurrentCell = row.Cells(c_SubClassNameColumnName) ' to be modified
                            End If
                        Next
                        If chkboxNoAskPrintReceipt.Checked = False And blDiscFlag = False Then
                            Dim result As MsgBoxResult = _
                                MsgBox(My.Resources.msgAskPrintReceipt, _
                                        MsgBoxStyle.YesNo, My.Resources.msgTitle)


                            If result = MsgBoxResult.Yes Then
                                If frmMain.CheckAuth(6) Or frmMain.CheckAuth(19) Then
                                    If dgvClass2.SelectedRows.Count > 0 And _
                                    dgvPayDetails.SelectedRows.Count > 0 Then
                                        For Each row As DataGridViewRow In dgvPayDetails.Rows
                                            If row.Cells(c_ReceiptNumColumnName).Value.ToString.Trim = tboxReceiptNum.Text Then
                                                'If row.Cells(c_ReceiptNumColumnName).Value.ToString.Trim = tboxReceiptNum.Text Then
                                                row.Selected = True
                                                dgvPayDetails.CurrentCell = row.Cells(c_ReceiptNumColumnName) 'to be modified
                                            End If
                                        Next
                                        prtdocReceipt.Print()
                                    End If
                                Else
                                    frmMain.ShowNoAuthMsg()
                                End If
                            End If
                        End If
                    End If
                End If
            Else
                tboxPayToday.Text = ""
            End If


        Catch ex As Exception
            CSOL.Logger.LogError(ex.ToString())
        End Try

        RefreshClassTable()
    End Sub

    Private Sub tabCtrl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabCtrl.Click
        If tabCtrl.SelectedIndex = 10 Then
            Me.Close()
        End If
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If strStuId.Length = 8 Then
            If tboxStuName.Text.Trim = "" Then
                ShowMsg()
                Exit Sub
            End If

            FillSibList()
            Dim d As Date
            If tboxBirthY.Text.Length = 0 Or tboxBirthM.Text.Length = 0 Or tboxBirthD.Text.Length = 0 Then
                d = GetMinDate()
            Else
                d = GetBirthday(tboxBirthY.Text, tboxBirthM.Text, tboxBirthD.Text)
            End If

            objCsol.ModifyStudent(strStuId, tboxStuName.Text, tboxEngName.Text, _
                    GetSex, d, tboxIc.Text, tboxAdd1b.Text, tboxAdd1a.Text, tboxAdd2b.Text, _
                    tboxAdd2a.Text, tboxTel1.Text, tboxTel2.Text, tboxOfficeTel.Text, _
                    tboxMobile.Text, tboxEmail.Text, GetStuType, GetSchool, tboxClass.Text, _
                    GetSchoolGrade, GetPrimary, GetJunior, GetHigh, GetUniversity, _
                    GetCurrent, GetGroup, tboxGradJunior.Text.Trim(), tboxDadName.Text, _
                    tboxMumName.Text, tboxDadTitle.Text, tboxMumTitle.Text, _
                    tboxDadMobile.Text, tboxMumMobile.Text, tboxIntroId.Text, _
                    tboxIntroName.Text, rtboxRemarks.Text, tboxRegInfo.Text, _
                    tboxClassId.Text, tboxStay.Text, "", "", lstSibRank, _
                    lstSibTypeID, lstSibName, lstSibBirth, lstSibSchool, lstSibRemarks, tboxEnrollSch.Text.Trim, _
                    tboxPunchCardNum.Text.Trim, GetSngValue(tboxHseeMark.Text.Trim))
            MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Me.Text = tboxStuName.Text.Trim & My.Resources.frmStuInfo
            Me.Tag.text = Me.Text
        End If
    End Sub

    Private Sub FillSibList()
        lstSibTypeID = New ArrayList
        lstSibName = New ArrayList
        lstSibBirth = New ArrayList
        lstSibSchool = New ArrayList
        lstSibRemarks = New ArrayList
        lstSibRank = New ArrayList

        If Not cboxFamily1.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily1.SelectedIndex))
            lstSibName.Add(tboxName1.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth1y.Text, tboxBirth1m.Text, tboxBirth1d.Text))
            lstSibSchool.Add(tboxSchool1.Text)
            lstSibRemarks.Add(tboxRemark1.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily2.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily2.SelectedIndex))
            lstSibName.Add(tboxName2.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth2y.Text, tboxBirth2m.Text, tboxBirth2d.Text))
            lstSibSchool.Add(tboxSchool2.Text)
            lstSibRemarks.Add(tboxRemark2.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily3.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily3.SelectedIndex))
            lstSibName.Add(tboxName3.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth3y.Text, tboxBirth3m.Text, tboxBirth3d.Text))
            lstSibSchool.Add(tboxSchool3.Text)
            lstSibRemarks.Add(tboxRemark3.Text)
            lstSibRank.Add(0)
        End If
        If Not cboxFamily4.SelectedIndex = -1 Then
            lstSibTypeID.Add(GetSibType(cboxFamily4.SelectedIndex))
            lstSibName.Add(tboxName4.Text)
            lstSibBirth.Add(GetBirthday(tboxBirth4y.Text, tboxBirth4m.Text, tboxBirth4d.Text))
            lstSibSchool.Add(tboxSchool4.Text)
            lstSibRemarks.Add(tboxRemark4.Text)
            lstSibRank.Add(0)
        End If

        'If Not tboxName1.Text = "" Then
        '    lstSibName.Add(tboxName1.Text)
        '    lstSibBirth.Add(GetBirthday(tboxBirth1y.Text, tboxBirth1m.Text, tboxBirth1d.Text))
        'End If
        'If Not tboxName2.Text = "" Then
        '    lstSibName.Add(tboxName2.Text)
        '    lstSibBirth.Add(GetBirthday(tboxBirth2y.Text, tboxBirth2m.Text, tboxBirth2d.Text))
        'End If
        'If Not tboxName3.Text = "" Then
        '    lstSibName.Add(tboxName3.Text)
        '    lstSibBirth.Add(GetBirthday(tboxBirth3y.Text, tboxBirth3m.Text, tboxBirth3d.Text))
        'End If
        'If Not tboxName4.Text = "" Then
        '    lstSibName.Add(tboxName4.Text)
        '    lstSibBirth.Add(GetBirthday(tboxBirth4y.Text, tboxBirth4m.Text, tboxBirth4d.Text))
        'End If

        'If Not tboxSchool1.Text = "" Then
        '    lstSibSchool.Add(tboxSchool1.Text)
        'End If
        'If Not tboxSchool2.Text = "" Then
        '    lstSibSchool.Add(tboxSchool2.Text)
        'End If
        'If Not tboxSchool3.Text = "" Then
        '    lstSibSchool.Add(tboxSchool3.Text)
        'End If
        'If Not tboxSchool4.Text = "" Then
        '    lstSibSchool.Add(tboxSchool4.Text)
        'End If

        'If Not tboxRemark1.Text = "" Then
        '    lstSibRemarks.Add(tboxRemark1.Text)
        'End If
        'If Not tboxRemark2.Text = "" Then
        '    lstSibRemarks.Add(tboxRemark2.Text)
        'End If
        'If Not tboxRemark3.Text = "" Then
        '    lstSibRemarks.Add(tboxRemark3.Text)
        'End If
        'If Not tboxRemark4.Text = "" Then
        '    lstSibRemarks.Add(tboxRemark4.Text)
        'End If





        'If IsNumeric(tboxRank1.Text) Then
        '    lstSibRank.Add(CInt(tboxRank1.Text))
        'Else
        '    lstSibRank.Add(0)
        'End If

        'If IsNumeric(tboxRank2.Text) Then
        '    lstSibRank.Add(CInt(tboxRank2.Text))
        'Else
        '    lstSibRank.Add(0)
        'End If

        'If IsNumeric(tboxRank3.Text) Then
        '    lstSibRank.Add(CInt(tboxRank3.Text))
        'Else
        '    lstSibRank.Add(0)
        'End If

        'If IsNumeric(tboxRank4.Text) Then
        '    lstSibRank.Add(CInt(tboxRank4.Text))
        'Else
        '    lstSibRank.Add(0)
        'End If

    End Sub

    Private Function GetSibType(ByVal idx As Integer) As Integer
        If idx > -1 Then
            Return lstSibType(idx)
        Else
            Return 0
        End If
    End Function

    Private Function GetCurrent() As Integer
        If radbutNowPri.Checked Then
            GetCurrent = 1
        ElseIf radbutNowJun.Checked Then
            GetCurrent = 2
        ElseIf radbutNowHig.Checked Then
            GetCurrent = 3
        ElseIf radbutNowUni.Checked Then
            GetCurrent = 4
        Else
            If cboxPrimarySch.SelectedIndex > -1 Then
                GetCurrent = 1
            ElseIf cboxJuniorSch.SelectedIndex > -1 Then
                GetCurrent = 2
            ElseIf cboxHighSch.SelectedIndex > -1 Then
                GetCurrent = 3
            ElseIf cboxUniversity.SelectedIndex > -1 Then
                GetCurrent = 4
            Else
                GetCurrent = 0
            End If
        End If
    End Function

    Private Function GetGroup() As Integer
        If cboxGroup.SelectedIndex > -1 Then
            Return lstSchGrp(cboxGroup.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetPrimary() As Integer
        If cboxPrimarySch.SelectedIndex > -1 Then
            Return lstPrimary(cboxPrimarySch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetUniversity() As Integer
        If cboxUniversity.SelectedIndex > -1 Then
            Return lstUniversity(cboxUniversity.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetJunior() As Integer
        If cboxJuniorSch.SelectedIndex > -1 Then
            Return lstJunior(cboxJuniorSch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetHigh() As Integer
        If cboxHighSch.SelectedIndex > -1 Then
            Return lstHigh(cboxHighSch.SelectedIndex)
        Else
            Return 0
        End If
    End Function

    Private Function GetSchoolGrade() As Integer
        If cboxGrade.SelectedIndex > -1 Then
            GetSchoolGrade = cboxGrade.SelectedIndex

        Else
            GetSchoolGrade = -1
        End If
    End Function

    Private Function GetSchool() As String
        If radbutNowPri.Checked Then
            GetSchool = cboxPrimarySch.Text
        ElseIf radbutNowJun.Checked Then
            GetSchool = cboxJuniorSch.Text
        ElseIf radbutNowHig.Checked Then
            GetSchool = cboxHighSch.Text
        Else
            GetSchool = cboxUniversity.Text
        End If
    End Function

    Private Function GetStuType() As Byte
        If radbutFormalStu.Checked Then
            GetStuType = c_StuTypeFormal
        Else
            GetStuType = c_StuTypePotential
        End If
    End Function

    Private Function GetSex() As String
        If cboxSex.SelectedIndex = 0 Then
            GetSex = My.Resources.male
        ElseIf cboxSex.SelectedIndex = 1 Then
            GetSex = My.Resources.female
        Else
            GetSex = ""
        End If
    End Function

    Private Function GetBirthday(ByVal str1 As String, ByVal str2 As String, _
                                 ByVal str3 As String) As Date
        Dim year As Integer
        Dim month As Integer
        Dim day As Integer
        Dim d As Date = GetMinDate()

        If IsNumeric(str1) And IsNumeric(str2) And IsNumeric(str3) Then
            If CInt(str1) > 0 Then
                year = CInt(str1) + 1911
            Else
                year = d.Year
            End If

            If CInt(str2) > 0 And CInt(str2) < 13 Then
                month = CInt(str2)
            Else
                month = d.Month
            End If

            If CInt(str3) > 0 And CInt(str3) < 32 Then
                day = CInt(str3)
            Else
                day = d.Day
            End If
            GetBirthday = New Date(year, month, day)
        Else
            GetBirthday = d
        End If
    End Function

    Private Overloads Sub ShowMsg()
        MsgBox(My.Resources.msgNoName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Overloads Sub ShowMsg(ByVal s As String)
        MsgBox(s, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Sub butReg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReg.Click

        RefreshStuSubClass()

        If frmMain.CheckAuth(67) = False Then
            frmMain.ShowNoAuthMsg()
        Else
            If strStuId.Length = 8 Then
                subfrmReg = New frm2ClassReg(strStuId)
                subfrmReg.ShowDialog()
                RefreshClassRegData()
            Else
                MsgBox(My.Resources.msgNoStuId, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
        'If MessageBox.Show(My.Resources.PayNow, My.Resources.msgRemindTitle, MessageBoxButtons.OKCancel) = MsgBoxResult.Ok Then
        '    tabCtrl.SelectedIndex = 1
        'End If
    End Sub

    Private Sub chkboxShowNotOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowNotOpen.CheckedChanged
        RefreshClassTable()
    End Sub

    Private Sub chkboxShowOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowOpen.CheckedChanged
        RefreshClassTable()
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassTable()
    End Sub

    Private Sub chkboxShowPast2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast2.CheckedChanged
        RefreshClassTable()
    End Sub

    Private Sub butClassChange_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClassChange.Click
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            If MsgBox("注意: 轉班連同學生繳費狀況一起轉至新班別 !", MsgBoxStyle.OkCancel, My.Resources.msgRemindTitle) = MsgBoxResult.Ok Then
                Dim intsc As Integer = dgvClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
                Dim intc As Integer = dgvClass.SelectedRows(0).Cells(c_ClassIDColumnName).Value
                Dim strseat As String = dgvClass.SelectedRows(0).Cells(c_SeatNumColumnName).Value
                lstid = New ArrayList
                lstname = New ArrayList
                For index As Integer = 0 To dtSc.Rows.Count - 1
                    If dtSc.Rows(index).Item(c_ClassIDColumnName) = intc Then
                        lstid.Add(dtSc.Rows(index).Item(c_IDColumnName))
                        lstname.Add(dtSc.Rows(index).Item(c_NameColumnName).trim)
                    End If
                Next
                Dim frm As New frm2ChangeClass(strStuId, intsc, strseat, lstname, lstid)
                frm.ShowDialog()
                RefreshClassRegData()
            End If
        End If
    End Sub

    Private Sub butClassCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClassCancel.Click
        If Not frmMain.CheckAuth(60) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim intsc As Integer = dgvClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim strseat As String = dgvClass.SelectedRows(0).Cells(c_SeatNumColumnName).Value
            Dim intC As Integer = dgvClass.SelectedRows(0).Cells(c_CancelColumnName).Value
            If intC = c_StuClassCancel Then
                objCsol.CancelStuClass(intsc, strStuId, c_StuClassNotCancel)
                ShowMsg(My.Resources.msgStuClassCancelNot)
            Else
                objCsol.CancelStuClass(intsc, strStuId, c_StuClassCancel)
                ShowMsg(My.Resources.msgStuClassCancel)


                Dim id As Integer = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value                                '100301
                Dim intSubClassId As Integer = dgvClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
                Dim seat As String = dgvClass.SelectedRows(0).Cells(c_SeatNumColumnName).Value
                Dim seat2 As String = ""
                objCsol.ChangeSeatNum(id, intSubClassId, seat2, seat, strStuId)                                         '100301
            End If
            RefreshClassRegData()
        End If
    End Sub

    Private Sub butSales_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSales.Click
        If Not frmMain.CheckAuth(53) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim sales As String = dgvClass.SelectedRows(0).Cells(c_SalesIDColumnName).Value
            Dim frm As New frm2ChangeSales(id, sales)
            frm.ShowDialog()
            RefreshClassRegData()
        End If
    End Sub

    Private Sub butDelClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelClass.Click
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            For i As Integer = 0 To dtClassReg2.Rows.Count - 1
                If CInt(dtClassReg2.Rows(i).Item(c_IDColumnName)) = CInt(id) Then
                    If CInt(dtClassReg2.Rows(i).Item(c_PayAmountColumnName)) = 0 And CInt(dtClassReg2.Rows(i).Item(c_DiscountColumnName)) = 0 Then
                        If MsgBox(My.Resources.msgConfirmDeleteClass, MsgBoxStyle.OkCancel, My.Resources.msgRemindTitle) = MsgBoxResult.Ok Then
                            objCsol.DeleteStuClass(id)
                        End If
                    Else
                        MsgBox(My.Resources.msgClassDelFail, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                    End If
                End If
            Next
            RefreshClassRegData()
        End If
    End Sub

    Private Sub butChangeId_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butChangeId.Click
        If strStuId.Length = 8 Then
            frmMain.SetCurrentStu(strStuId)
            Dim frm As New frm2ChangeStuID(strStuId)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                strStuId = frmMain.GetCurrentStu
                lblStuId.Text = strStuId
            End If
        End If
    End Sub

    Private Sub butPayModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayModify.Click
        If Not frmMain.CheckAuth(22) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        If dgvPayDetails.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvPayDetails.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim receipt As String = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
            Dim subclass As Integer = dgvPayDetails.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim amount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
            Dim handler As String = dgvPayDetails.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
            Dim method As Integer = dgvPayDetails.SelectedRows(0).Cells(c_PayMethodColumnName).Value
            Dim extra As String = dgvPayDetails.SelectedRows(0).Cells(c_ExtraColumnName).Value
            Dim remarks As String = dgvPayDetails.SelectedRows(0).Cells(c_RemarksColumnName).Value
            Dim frm As New frm2UpdateReceipt(id, receipt, strStuId, subclass, amount, _
                                            handler, method, extra, remarks)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshClassRegData()
            End If
        End If
    End Sub

    Private Sub butPayDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayDelete.Click
        If Not frmMain.CheckAuth(23) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvPayDetails.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvPayDetails.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim scid As Integer = dgvPayDetails.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim amount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
            Dim backAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_BackAmountColumnName).Value
            Dim keepAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_KeepAmountColumnName).Value
            Dim strRemarks As String = My.Resources.payRecDel & ", " & My.Resources.amount & "=" & amount.ToString
            Dim receipt As String = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
            Dim handler As String = dgvPayDetails.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
            Dim frm As New frm2InputReason
            Dim reason As String = ""
            If Not backAmount = 0 Or Not keepAmount = 0 Then
                If MsgBox("此筆繳費記錄含有退保記錄確定刪除嗎?", MsgBoxStyle.OkCancel, My.Resources.msgRemindTitle) = MsgBoxResult.Ok Then
                    frm.ShowDialog()
                    If frmMain.GetOkState Then
                        reason = frmMain.GetCurrentString
                    End If
                    objCsol.DeletePayRec(id, strStuId, scid, amount, backAmount, keepAmount)
                    objCsol.AddPayModRec(receipt, strStuId, scid, Now, handler, frmMain.GetUsrName, strRemarks, reason)
                    frmMain.SetOkState(True)
                    RefreshClassRegData()
                End If
            Else
                frm.ShowDialog()
                If frmMain.GetOkState Then
                    reason = frmMain.GetCurrentString

                    objCsol.DeletePayRec(id, strStuId, scid, amount, backAmount, keepAmount)
                    objCsol.AddPayModRec(receipt, strStuId, scid, Now, handler, frmMain.GetUsrName, strRemarks, reason)
                    frmMain.SetOkState(True)
                End If
                RefreshClassRegData()
            End If
        End If
    End Sub

    Private Sub butPayBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayBack.Click
        If Not frmMain.CheckAuth(25) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvPayDetails.SelectedRows.Count = 0 Then
            Exit Sub
        End If

        Dim id As Integer = dgvPayDetails.SelectedRows(0).Cells(c_IDColumnName).Value
        Dim ReceiptNum As String = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
        Dim SubClassID As Integer = dgvPayDetails.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
        Dim PayAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
        Dim PayDateTime As Date = dgvPayDetails.SelectedRows(0).Cells(c_DateTimeColumnName).Value
        Dim HandlerID As String = dgvPayDetails.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
        Dim SalesID As String = dgvPayDetails.SelectedRows(0).Cells(c_SalesIDColumnName).Value
        Dim PayMethod As Integer = dgvPayDetails.SelectedRows(0).Cells(c_PayMethodColumnName).Value
        Dim Extra As String = dgvPayDetails.SelectedRows(0).Cells(c_ExtraColumnName).Value
        Dim Remarks As String = dgvPayDetails.SelectedRows(0).Cells(c_RemarksColumnName).Value
        Dim Discount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_DiscountColumnName).Value
        Dim DiscountRemarks As String = dgvPayDetails.SelectedRows(0).Cells(c_DiscountRemarksColumnName).Value
        Dim ClassName As String = dgvPayDetails.SelectedRows(0).Cells(c_ClassNameColumnName).Value
        Dim SubClassName As String = dgvPayDetails.SelectedRows(0).Cells(c_SubClassNameColumnName).Value
        Dim StuName As String = ""
        If dtStuInfo.Rows.Count > 0 Then
            StuName = dtStuInfo.Rows(0).Item(c_NameColumnName).trim
        Else
            StuName = tboxStuName.Text.Trim()
        End If
        Dim HandlerName As String
        If Not DBNull.Value.Equals(dgvPayDetails.SelectedRows(0).Cells(c_HandlerColumnName).Value) Then


            HandlerName = dgvPayDetails.SelectedRows(0).Cells(c_HandlerColumnName).Value
        Else
            HandlerName = ""
        End If
        Dim BackAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_BackAmountColumnName).Value
        Dim KeepAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_KeepAmountColumnName).Value

        If BackAmount = 0 And KeepAmount = 0 Then
            Dim frm As New frm2AddMB(id, ReceiptNum, strStuId, _
                     SubClassID, _
                     PayAmount, PayDateTime, _
                     HandlerID, _
                     SalesID, PayMethod, _
                     Extra, Remarks, _
                     Discount, _
                     DiscountRemarks, _
                     ClassName, _
                     SubClassName, StuName, _
                     HandlerName)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshClassRegData()
                RefreshKBData()
            End If
        Else
            ShowMsg(My.Resources.msgKBRepeatError)
        End If
    End Sub

    Private Sub butPayKeep_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayKeep.Click
        If Not frmMain.CheckAuth(25) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvPayDetails.SelectedRows.Count = 0 Then
            Exit Sub
        End If
        Dim id As Integer = dgvPayDetails.SelectedRows(0).Cells(c_IDColumnName).Value
        Dim ReceiptNum As String = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
        Dim SubClassID As Integer = dgvPayDetails.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
        Dim PayAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
        Dim PayDateTime As Date = dgvPayDetails.SelectedRows(0).Cells(c_DateTimeColumnName).Value
        Dim HandlerID As String = dgvPayDetails.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
        Dim SalesID As String = dgvPayDetails.SelectedRows(0).Cells(c_SalesIDColumnName).Value
        Dim PayMethod As Integer = dgvPayDetails.SelectedRows(0).Cells(c_PayMethodColumnName).Value
        Dim Extra As String = dgvPayDetails.SelectedRows(0).Cells(c_ExtraColumnName).Value
        Dim Remarks As String = dgvPayDetails.SelectedRows(0).Cells(c_RemarksColumnName).Value
        Dim Discount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_DiscountColumnName).Value
        Dim DiscountRemarks As String = dgvPayDetails.SelectedRows(0).Cells(c_DiscountRemarksColumnName).Value
        Dim ClassName As String = dgvPayDetails.SelectedRows(0).Cells(c_ClassNameColumnName).Value
        Dim SubClassName As String = dgvPayDetails.SelectedRows(0).Cells(c_SubClassNameColumnName).Value
        Dim StuName As String = ""
        If dtStuInfo.Rows.Count > 0 Then
            StuName = dtStuInfo.Rows(0).Item(c_NameColumnName).trim
        Else
            StuName = tboxStuName.Text.Trim()
        End If
        Dim HandlerName As String = dgvPayDetails.SelectedRows(0).Cells(c_HandlerColumnName).Value
        Dim BackAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_BackAmountColumnName).Value
        Dim KeepAmount As Integer = dgvPayDetails.SelectedRows(0).Cells(c_KeepAmountColumnName).Value

        If BackAmount = 0 And KeepAmount = 0 Then
            Dim frm As New frm2AddMK(id, ReceiptNum, strStuId, _
                     SubClassID, _
                     PayAmount, PayDateTime, _
                     HandlerID, _
                     SalesID, PayMethod, _
                     Extra, Remarks, _
                     Discount, _
                     DiscountRemarks, _
                     ClassName, _
                     SubClassName, StuName, _
                     HandlerName)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshClassRegData()
                RefreshKBData()
            End If
        Else
            ShowMsg(My.Resources.msgKBRepeatError)
        End If
    End Sub

    Private Sub butPayPrintOpt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayPrintOpt.Click
        Dim frm As New frm2SetPrint
        frm.ShowDialog()
        If frmMain.GetOkState Then
            frmMain.RefreshReceiptPrintOpt()
        End If
    End Sub

    Private Sub butDiscountRemarks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDiscountRemarks.Click
        If frmMain.CheckAuth(32) = False Then
            frmMain.ShowNoAuthMsg()
        Else
            Dim frm As New frm2DiscDict
            frm.ShowDialog()
            If frmMain.GetOkState Then
                tboxDiscount.Text = frmMain.GetCurrentValue.ToString
                tboxDiscountRemarks.Text = frmMain.GetCurrentString
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub butReceiptRemarks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butReceiptRemarks.Click
        Dim frm As New frm2ReceiptRkDict
        frm.ShowDialog()
        If frmMain.GetOkState Then
            tboxReceiptRemarks.Text = frmMain.GetCurrentString
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub dgvKB_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvKB.SelectionChanged
        'Dim intClassId As Integer
        'If dgvKB.SelectedRows.Count > 0 Then
        '    intClassId = dgvKB.SelectedRows(0).Cells(c_ClassIDColumnName).Value
        '    If intClassId > -1 Then
        '        dtKBPay.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString
        '        dgvKBPayRec.DataSource = dtPayRec.DefaultView               '100206
        '    Else
        '        dtKBPay.DefaultView.RowFilter = ""
        '        dgvKBPayRec.DataSource = dtPayRec.DefaultView               '100206
        '    End If
        'End If
    End Sub

    Private Sub butKBMod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butKBMod.Click
        If Not frmMain.CheckAuth(26) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvKB.SelectedRows.Count > 0 Then
            Dim intId As Integer = dgvKB.SelectedRows(0).Cells(c_BackIDColumnName).Value
            Dim intAmt As Integer = dgvKB.SelectedRows(0).Cells(c_BackAmountColumnName).Value
            Dim reason As String = dgvKB.SelectedRows(0).Cells(c_ReasonColumnName).Value
            Dim frm As New frm2UpdKB(intId, reason, intAmt)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshKBData()
                RefreshClassRegData()
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub butKBRemarks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butKBRemarks.Click
        If dgvKB.SelectedRows.Count > 0 Then
            Dim intId As Integer = dgvKB.SelectedRows(0).Cells(c_BackIDColumnName).Value
            Dim frm As New frm2AddMBRemarks(intId)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshKBData()
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub butKBDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butKBDelete.Click
        If Not frmMain.CheckAuth(62) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        If dgvKB.SelectedRows.Count > 0 Then
            Dim intId As Integer = dgvKB.SelectedRows(0).Cells(c_BackIDColumnName).Value

            objCsol.DeleteMB(intId)
            RefreshKBData()
            RefreshClassRegData()
        End If
    End Sub



    Private Sub cboxAttClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxAttClass.SelectedIndexChanged

        If dtAttClass.Rows.Count > 0 Then
            Dim strFilter1 As String = ""
            Dim strFilter2 As String = ""
            Dim dt As DataTable = dtAttRec.Clone                                        '100321
            For i As Integer = 0 To dtAttRec.Rows.Count - 1
                If CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = 0 Or CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = -1 Then
                    dt.Rows.Add(dtAttRec.Rows(i).ItemArray)
                End If
            Next                                                                        '100321

            'If chkboxAttShowPast.Checked Then
            '    If cboxAttClass.SelectedIndex > 0 Then
            '        strFilter2 = c_ClassNameColumnName & "='" & cboxAttClass.SelectedItem & "'"
            '        dtAttClass.DefaultView.RowFilter = strFilter2
            '    Else
            '        dtAttClass.DefaultView.RowFilter = ""
            '    End If
            'Else
            '    strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
            '    If cboxAttClass.SelectedIndex > 0 Then
            '        strFilter2 = c_ClassNameColumnName & "='" & cboxAttClass.SelectedItem & "'"
            '        dtAttClass.DefaultView.RowFilter = strFilter1 & " AND " & strFilter2
            '    Else
            '        dtAttClass.DefaultView.RowFilter = strFilter1
            '    End If
            'End If

            If chkboxAttShowPast.Checked Then
                If cboxAttClass.SelectedIndex > 0 Then
                    strFilter2 = c_ClassNameColumnName & "='" & cboxAttClass.SelectedItem & "'"
                    dt.DefaultView.RowFilter = strFilter2
                Else
                    dt.DefaultView.RowFilter = ""
                End If
            Else
                strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
                If cboxAttClass.SelectedIndex > 0 Then
                    strFilter2 = c_ClassNameColumnName & "='" & cboxAttClass.SelectedItem & "'"
                    dt.DefaultView.RowFilter = strFilter1 & " AND " & strFilter2
                Else
                    dt.DefaultView.RowFilter = strFilter1

                End If
            End If

            dgvAttClass.DataSource = dt.DefaultView                                                 '100321
            'If dgvAttClass.Rows.Count > 0 Then
            '    dgvAttClass.CurrentCell = dgvAttClass.Rows(0).Cells(c_SubClassNameColumnName)
            'End If
        End If
    End Sub

    Private Sub dgvAttClass_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAttClass.SelectionChanged
        ChangeAttRecRange()
    End Sub

    Private Sub ChangeAttRecRange()
        If dgvAttClass.SelectedRows.Count > 0 And dtAttRec.Rows.Count > 0 Then
            Dim strFilter1 As String = c_SubClassIDColumnName & "=" & _
                dgvAttClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value.ToString
            If radbutAttShowAll.Checked Then
                dtAttRec.DefaultView.RowFilter = strFilter1
            Else
                Dim dtS As String = GetDateStart(dtpickFrom.Value).ToString
                Dim dtE As String = GetDateEnd(dtpickTo.Value).ToString
                Dim strFilter2 As String = c_StartColumnName & ">='" & dtS & _
                    "' AND " & c_StartColumnName & "<='" & dtE & "'"
                dtAttRec.DefaultView.RowFilter = strFilter1 & " AND (" & strFilter2 & ")"
            End If
            dtAttRec.DefaultView.RowFilter = "MakeUpClassID=1"                      '100321
            dgvAttRec.DataSource = dtAttRec.DefaultView
            LoadColumnTextAtt()                                                     '100226
        End If
    End Sub

    Private Sub chkboxAttShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxAttShowPast.CheckedChanged




        cboxAttClass.Items.Clear()
        If dtAttClass.Rows.Count > 0 Then
            Dim dt As DataTable = dtAttRec.Clone
            If chkboxAttShowPast.Checked Then

                cboxAttClass.Items.Add(My.Resources.all)
                Dim strFilter1 As String = ""
                If chkboxAttShowPast.Checked Then
                    For index As Integer = 0 To dtAttClass.Rows.Count - 1
                        If Not cboxAttClass.Items.Contains(dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim) Then
                            cboxAttClass.Items.Add(dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim)
                        End If
                    Next
                Else
                    For index As Integer = 0 To dtAttClass.Rows.Count - 1
                        If dtAttClass.Rows(index).Item(c_EndColumnName) >= Now Then
                            If Not cboxAttClass.Items.Contains(dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim) Then
                                cboxAttClass.Items.Add(dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim)
                            End If
                        End If
                    Next
                    strFilter1 = c_EndColumnName & ">='" & Now.ToString & "'"
                End If
                cboxAttClass.SelectedIndex = 0
                dtAttClass.DefaultView.RowFilter = strFilter1


                '100321
                For i As Integer = 0 To dtAttRec.Rows.Count - 1
                    If CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = 0 Or CInt(dtAttRec.Rows(i).Item("MakeUpClassID")) = -1 Then
                        dt.Rows.Add(dtAttRec.Rows(i).ItemArray)
                    End If
                Next
            End If
            dgvAttClass.DataSource = dt.DefaultView                                            '100321

            'dgvAttClass.CurrentCell = dgvAttClass.Rows(0).Cells(c_SubClassNameColumnName)
        End If
    End Sub


    Private Sub cboxBookClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxBookClass.SelectedIndexChanged
        RefreshBookReceive()
        RefreshBookNotReceive()
    End Sub

    Private Sub chkboxBookShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbBookPastClass.CheckedChanged
        RefreshBookClass()
        RefreshBookReceive()
        RefreshBookNotReceive()
    End Sub

    Private Sub cbBookReceivePast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbBookReceivePast.CheckedChanged
        RefreshBookReceive()
    End Sub

    Private Sub cbBookNotReceivePast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbBookNotReceivePast.CheckedChanged
        RefreshBookNotReceive()
    End Sub

    Private Sub RefreshBookReceive()
        If dtBookRecHas.Rows.Count > 0 Then
            If cboxBookClass.SelectedItem = My.Resources.all Then
                If cbBookReceivePast.Checked Then
                    dtBookRecHas.DefaultView.RowFilter = String.Format("{0} IS NOT NULL", c_IDColumnName)
                Else
                    dtBookRecHas.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} >= '{2}'", c_IDColumnName, c_BeforeDateColumnName, Now.ToString("yyyy/MM/dd"))
                End If
            Else
                Dim className As String = cboxBookClass.SelectedItem
                If cbBookReceivePast.Checked Then
                    dtBookRecHas.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} = '{2}'", c_IDColumnName, c_ClassNameColumnName, className)
                Else
                    dtBookRecHas.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} = '{2}' AND {3} >= '{4}'", c_IDColumnName, c_ClassNameColumnName, className, c_BeforeDateColumnName, Now().ToString("yyyy/MM/dd"))
                End If
            End If
            dgvBookReceive.DataSource = dtBookRecHas.DefaultView
        End If
        LoadColumnTextBook()
    End Sub

    Private Sub RefreshBookNotReceive()
        If dtBookRecNo.Rows.Count > 0 Then
            If cboxBookClass.SelectedItem = My.Resources.all Then
                If cbBookNotReceivePast.Checked Then
                    dtBookRecNo.DefaultView.RowFilter = String.Format("{0} IS NOT NULL", c_IDColumnName)
                Else
                    dtBookRecNo.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} >= '{2}'", c_IDColumnName, c_BeforeDateColumnName, Now.ToString("yyyy/MM/dd"))
                End If
            Else
                Dim className As String = cboxBookClass.SelectedItem
                If cbBookNotReceivePast.Checked Then
                    dtBookRecNo.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} = '{2}'", c_IDColumnName, c_ClassNameColumnName, className)
                Else
                    dtBookRecNo.DefaultView.RowFilter = String.Format("{0} IS NOT NULL AND {1} = '{2}' AND {3} >= '{4}'", c_IDColumnName, c_ClassNameColumnName, className, c_BeforeDateColumnName, Now.ToString("yyyy/MM/dd"))
                End If
            End If
            dgvBookNotReceive.DataSource = dtBookRecNo.DefaultView
        End If
        LoadColumnTextBook()
    End Sub

    Private Sub RefreshBookClass()

        cboxBookClass.Items.Clear()
        lstbookClass.Clear()
        cboxBookClass.Items.Add(My.Resources.all)

        If dtAttClass.Rows.Count > 0 Then
            If cbBookPastClass.Checked Then
                For index As Integer = 0 To dtAttClass.Rows.Count - 1                                                   '100223
                    If Not cboxBookClass.Items.Contains(dtAttClass.Rows(index).Item(c_ClassNameColumnName)) Then
                        Dim classname As String = dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim()
                        lstbookClass.Add(classname)
                        cboxBookClass.Items.Add(classname)
                    End If
                Next
            Else
                For index As Integer = 0 To dtAttClass.Rows.Count - 1
                    If dtAttClass.Rows(index).Item(c_EndColumnName) >= Now Then
                        If Not cboxBookClass.Items.Contains(dtAttClass.Rows(index).Item(c_ClassNameColumnName)) Then
                            Dim classname As String = dtAttClass.Rows(index).Item(c_ClassNameColumnName).trim()
                            lstbookClass.Add(classname)
                            cboxBookClass.Items.Add(classname)
                        End If
                    End If
                Next                                                                                                    '100223
            End If
            cboxAttClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboxChangeClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxChangeClass.SelectedIndexChanged
        If dtClassChange.Rows.Count > 0 Then
            If cboxChangeClass.SelectedIndex > 0 Then
                Dim strFilter1 As String = c_ClassNameColumnName & "='" & _
                            cboxChangeClass.SelectedItem & "'"
                dtClassChange.DefaultView.RowFilter = strFilter1
            Else
                dtClassChange.DefaultView.RowFilter = ""
            End If
            dgvChange.DataSource = dtClassChange.DefaultView
        End If
    End Sub

    Private Sub chkboxChangeShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxChangeShowPast.CheckedChanged
        Dim dt As DataTable = dtClassChange.DefaultView.ToTable(True, c_ClassNameColumnName, c_EndColumnName)
        cboxChangeClass.Items.Clear()
        cboxChangeClass.Items.Add(My.Resources.all)
        If chkboxChangeShowPast.Checked Then
            For index As Integer = 0 To dt.Rows.Count - 1
                cboxChangeClass.Items.Add(dt.Rows(index).Item(c_ClassNameColumnName).trim)
            Next
        Else
            For index As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(index).Item(c_EndColumnName) >= Now Then
                    cboxChangeClass.Items.Add(dt.Rows(index).Item(c_ClassNameColumnName).trim)
                End If
            Next
        End If
        cboxChangeClass.SelectedIndex = 0
        dtClassChange.DefaultView.RowFilter = ""
        dgvChange.DataSource = dtClassChange.DefaultView
    End Sub

    Private Sub butPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrevious.Click
        Dim s As String = ""
        s = objCsol.GetPreviousStu(strStuId)
        If s.Length = 8 Then
            strStuId = s
            frmMain.SetCurrentStu(s)
            frmMain.DisplayNextStuInfo(s)
        End If
    End Sub

    Private Sub butNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butNext.Click
        Dim s As String = ""
        s = objCsol.GetNextStu(strStuId)
        If s.Length = 8 Then
            strStuId = s
            frmMain.SetCurrentStu(s)
            frmMain.DisplayNextStuInfo(s)
        End If
    End Sub

    Private Sub butIssueCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butIssueCard.Click
        Dim frm As New frm2IssueCard(0, strStuId)
        frm.ShowDialog()
        If strCardNum = "" Then
            If frmMain.GetOkState Then
                strCardNum = frmMain.GetCurrentString
                butIssueCard.Text = My.Resources.issueCardAgain
            End If
        Else
            If Not frmMain.CheckAuth(65) Then
                frmMain.ShowNoAuthMsg()
                Exit Sub
            End If
            If frmMain.GetOkState Then
                strCardNum = frmMain.GetCurrentString
                objCsol.AddBadgeReissueRec(strStuId, Now, frmMain.GetUsrId)
            End If
        End If
    End Sub

    Private Sub butDelCard_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelCard.Click
        If Not frmMain.CheckAuth(66) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        If strCardNum.Length > 6 Then
            Dim result As MsgBoxResult = _
                        MsgBox(My.Resources.msgCardDeleted, _
                                MsgBoxStyle.YesNo, My.Resources.msgTitle)
            If result = MsgBoxResult.Yes Then
                objCsol.ModifyStuCard2(strStuId, "")
                butIssueCard.Text = My.Resources.issueCard
                strCardNum = ""
            End If
        End If
    End Sub

    Private Sub butPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrint.Click
        If frmMain.CheckAuth(2) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim b As New Bitmap(tpgInfo.ClientRectangle.Width, tpgInfo.ClientRectangle.Height)
        tpgInfo.DrawToBitmap(b, tpgInfo.ClientRectangle)
        e.Graphics.DrawImage(b, 100, 100)
    End Sub

    Private Sub butWebcam_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butWebcam.Click
        If strStuId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                picbox.Image.Dispose()
                picbox.Image = Nothing
            End If
            Dim frm As New frm2Photo(strStuId)
            frm.ShowDialog()

            Dim f As String = GetPhotoPath() & strStuId & ".bmp"
            If CheckFileExist(f) Then
                Dim bm As New Bitmap(f)
                picbox.Image = bm
                picbox.SizeMode = PictureBoxSizeMode.AutoSize
                butSavePic.Enabled = False
            End If
        End If
    End Sub

    Private Sub butAddPic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddPic.Click
        Dim path As String = ""
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "image files (*.bmp)|*.bmp|All files (*.*)|*.*"
        openFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                path = openFileDialog1.FileName
                Dim bm As New Bitmap(path)
                picbox.Image = bm
                picbox.SizeMode = PictureBoxSizeMode.AutoSize
                butSavePic.Enabled = True
                lblImgInfo.Text = ""
                MessageBox.Show(My.Resources.msgImportPhotoSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

            Catch Ex As Exception

            End Try
        End If

    End Sub

    Private Sub butDelPic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelPic.Click
        If strStuId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                picbox.Image.Dispose()
                picbox.Image = Nothing

                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Try
                        My.Computer.FileSystem.DeleteFile(f)
                        '20100424 imgdb by sherry 
                        objCsol.DelImgByStuID(strStuId)
                        MessageBox.Show(My.Resources.msgDeleteSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

                    Catch ex As Exception
                        MessageBox.Show(My.Resources.msgDeleteFail, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub butSavePic_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSavePic.Click
        If strStuId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                Dim bm_source As New Bitmap(picbox.Image)
                Dim bm_dest As New Bitmap( _
                CInt(bm_source.Width * 0.25), _
                CInt(bm_source.Height * 0.25))

                ' Make a Graphics object for the result Bitmap.
                Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

                ' Copy the source image into the destination bitmap.
                gr_dest.DrawImage(bm_source, 0, 0, _
                    bm_dest.Width + 1, _
                    bm_dest.Height + 1)
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Try
                        My.Computer.FileSystem.DeleteFile(f)

                    Catch ex As Exception
                        MessageBox.Show(My.Resources.msgPhotoAlreadySaved, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    End Try
                End If

                Try
                    '20100424 imgdb by sherry start

                    bm_dest.Save(f, Imaging.ImageFormat.Bmp)
                    Dim fs As FileStream = Nothing
                    Try
                        '#Region "Reading file"
                        fs = New FileStream(f, FileMode.Open)
                        '
                        ' Finding out the size of the file to be uploaded
                        '
                        Dim fi As FileInfo = New FileInfo(f)
                        Dim temp As Long = fi.Length
                        Dim lung As Integer = Convert.ToInt32(temp)
                        ' ------------------------------------------
                        '
                        ' Reading the content of the file into an array of bytes.
                        '
                        Dim picture As Byte() = New Byte(lung - 1) {}
                        fs.Read(picture, 0, lung)
                        fs.Close()
                        objCsol.UploadImg(strStuId, picture)

                    Catch ec As Exception
                        AddErrorLog(ec.ToString)
                    End Try
                    '20100424 imgdb by sherry end


                    MessageBox.Show(My.Resources.msgSaveComplete, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

                    butSavePic.Enabled = False
                Catch ex As Exception
                    MessageBox.Show(My.Resources.msgPhotoAlreadySaved, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                End Try

            End If
        End If
    End Sub

    Private Sub butAttPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAttPrint.Click
        PrintDocument2.DefaultPageSettings.Landscape = True
        PrintDocument2.Print()
    End Sub

    Private Sub butAttPrint2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAttPrint2.Click
        PrintDocument3.DefaultPageSettings.Landscape = True
        PrintDocument3.Print()
    End Sub

    Private Sub PrintDocument3_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument3.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvAttRec.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If

        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument3_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument3.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvAttRec.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvAttRec.Rows.Count

            Dim oRow As DataGridViewRow = dgvAttRec.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvAttRec.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvAttRec.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvAttRec.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvAttRec.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvAttRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvAttRec.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvAttRec.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvAttRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvAttRec.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub butAttExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAttExport.Click
        ExportDgvToExcel(dgvAttClass)
    End Sub

    Private Sub butAttExport2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAttExport2.Click
        ExportDgvToExcel(dgvAttRec)
    End Sub

    Private Sub prtdocReceipt_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles prtdocReceipt.BeginPrint
        PageNumber = 0
    End Sub

    Private Sub prtdocReceipt_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles prtdocReceipt.PrintPage
        'PageNumber += 1
        'If (PageNumber >= 2) Then e.HasMorePages = False Else _
        'e.HasMorePages = True

        'Page height and width, A4: 210x297 B5:182x257
        Dim dtPrintOpt As New DataTable
        dtPrintOpt = frmMain.GetReceiptPrint
        Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)
        Dim nPageH As Integer = 0
        Dim nPageW As Integer = 0

        'Page Margin
        Dim nPageRL As Integer = 0
        Dim nPageTB As Integer = 0
        If bytPaperSize = c_PaperSizeA4 Then
            nPageH = 297
            nPageW = 210
            'nPageRL = 32
            'nPageTB = 30
            nPageRL = 16
            'nPageRL = 1
            nPageTB = 15
            'nPageTB = 1
        ElseIf bytPaperSize = c_PaperSizeB5 Then
            nPageH = 257
            nPageW = 182
            'nPageRL = 28
            'nPageTB = 26
            'nPageRL = 14
            'nPageTB = 13
            nPageRL = 20                                     '100208
            nPageTB = 8                                    '100208
        Else
            nPageH = 297
            nPageW = 210
            'nPageRL = 32
            'nPageTB = 30
            nPageRL = 16
            'nPageRL = 1
            nPageTB = 15
            'nPageTB = 1
        End If

        'Define the cusor's coordinate
        Dim cursor_X As Integer = nPageRL
        Dim cursor_Y As Integer = nPageTB

        Dim MyCopy As Boolean = False
        'Draw the first part
        DrawReceipt(MyCopy, cursor_X, cursor_Y, e)
        'Define the page unit
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        'Draw the center line
        Dim myPen As Pen = New Pen(Color.Black, 0.5)
        myPen.DashStyle = Drawing2D.DashStyle.Dash
        cursor_X = nPageRL
        cursor_Y = nPageH / 2
        e.Graphics.DrawLine(myPen, cursor_X, cursor_Y, nPageW - cursor_X, cursor_Y)
        'Draw the 2nd part
        MyCopy = True
        'cursor_Y = cursor_Y + nPageRL / 4
        cursor_Y = cursor_Y + nPageTB / 6
        If bytPaperSize = c_PaperSizeB5 Then
            DrawReceipt(MyCopy, cursor_X, cursor_Y + 4, e)
        Else
            DrawReceipt(MyCopy, cursor_X, cursor_Y, e)
        End If
    End Sub

    Private Sub DrawReceipt(ByVal IsCopy As Boolean, ByRef cursor_X As Integer, ByRef cursor_Y As Integer, ByVal e As Printing.PrintPageEventArgs)
        'Define the page unit
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        'Define the Title format
        Dim MyTitformat As New StringFormat
        MyTitformat.Alignment = StringAlignment.Near

        'Title fonts
        Dim MyTitFont1 As New Font("Arial", 12, FontStyle.Bold)
        Dim dtPrintOpt As New DataTable
        dtPrintOpt = frmMain.GetReceiptPrint
        If dtPrintOpt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1 Then
            If IsCopy Then
                Exit Sub
            End If
        End If
        'Page height and width, A4: 210x297
        Dim nPageH As Integer = 0
        Dim nPageW As Integer = 0

        'Page Margin
        Dim nPageRL As Integer = 0
        Dim nPageTB As Integer = 0
        Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)

        'If bytPaperSize = c_PaperSizeA4 Then
        '    nPageH = 297
        '    nPageW = 210
        '    nPageRL = 32
        '    nPageTB = 25
        'ElseIf bytPaperSize = c_PaperSizeB5 Then
        '    nPageH = 257
        '    nPageW = 182
        '    nPageRL = 28
        '    nPageTB = 22
        'Else
        '    nPageH = 297
        '    nPageW = 210
        '    nPageRL = 32
        '    nPageTB = 25
        'End If
        If bytPaperSize = c_PaperSizeA4 Then
            nPageH = 297
            nPageW = 210
            'nPageRL = 32
            'nPageTB = 30
            nPageRL = 16
            nPageTB = 15
        ElseIf bytPaperSize = c_PaperSizeB5 Then
            nPageH = 257
            nPageW = 182
            'nPageRL = 28
            'nPageTB = 26
            nPageRL = 12                                    '100208
            nPageTB = 12                                    '100208
        Else
            nPageH = 297
            nPageW = 210
            'nPageRL = 32
            'nPageTB = 30
            nPageRL = 16
            nPageTB = 15
        End If

        Dim intScId As Integer
        Dim strSales As String = ""
        Dim MyRecNum As String = ""
        Dim dtChequeValid As Date = Now
        Dim idx As Integer = dgvClass2.SelectedRows(0).Index
        dgvClass2.CurrentCell = dgvClass2.SelectedRows(0).Cells(c_SubClassNameColumnName)
        Dim lstReceiptNum As New ArrayList
        Dim lstDate As New ArrayList
        Dim lstPayType As New ArrayList
        Dim lstAmount As New ArrayList
        Dim intReceiveAmt As Integer
        Dim strRemarks As String = ""
        Dim strHandler As String = ""
        Dim datePay As Date = Now
        Dim intDiscount As Integer
        Dim strDiscRemarks As String = ""
        Dim strPayType As String = ""

        If dgvPayDetails.SelectedRows.Count > 0 Then
            Dim idx2 As Integer = dgvPayDetails.SelectedRows(0).Index
            dgvPayDetails.CurrentCell = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName)
            MyRecNum = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value.ToString.Trim
            intReceiveAmt = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
            strRemarks = dgvPayDetails.SelectedRows(0).Cells(c_RemarksColumnName).Value.ToString.Trim
            strHandler = dgvPayDetails.SelectedRows(0).Cells(c_HandlerColumnName).Value.ToString.Trim
            datePay = dgvPayDetails.SelectedRows(0).Cells(c_DateTimeColumnName).Value
            intDiscount = dgvPayDetails.SelectedRows(0).Cells(c_DiscountColumnName).Value
            strDiscRemarks = dgvPayDetails.SelectedRows(0).Cells(c_DiscountRemarksColumnName).Value.ToString.Trim
            strPayType = dgvPayDetails.SelectedRows(0).Cells(c_PayMethodNameColumnName).Value
        End If
        intScId = dgvClass2.Rows(idx).Cells(c_SubClassIDColumnName).Value
        If dtDisc.Rows.Count > 0 And intDiscount = 0 Then
            For i As Integer = 0 To dtDisc.Rows.Count - 1
                If dtDisc.Rows(i).Item(c_SubClassIDColumnName) = intScId Then
                    intDiscount = dtDisc.Rows(i).Item(c_DiscountColumnName)
                    strDiscRemarks = dtDisc.Rows(i).Item(c_RemarksColumnName)
                End If
            Next
        End If

        Dim intPrintToday As Byte = dtPrintOpt.Rows(0).Item(c_PrintPayTodayColumnName)
        If intPrintToday = 0 Then
            For Each row As DataGridViewRow In dgvPayDetails.Rows
                If row.Cells(c_SubClassIDColumnName).Value = intScId Then
                    lstReceiptNum.Add(row.Cells(c_ReceiptNumColumnName).Value.ToString.Trim)
                    lstDate.Add(GetROCDateNum(row.Cells(c_DateTimeColumnName).Value))
                    lstPayType.Add(strPayType)
                    lstAmount.Add(row.Cells(c_AmountColumnName).Value.ToString.Trim)
                End If
            Next
        Else
            lstReceiptNum.Add(MyRecNum)
            lstDate.Add(GetROCDateNum(datePay))
            lstPayType.Add(strPayType)
            lstAmount.Add(intReceiveAmt)
        End If

        strSales = dgvClass2.Rows(idx).Cells(c_SalesPersonColumnName).Value.ToString.Trim
        Dim dateStart As Date = dgvClass2.Rows(idx).Cells(c_StartColumnName).Value
        Dim dateEnd As Date = dgvClass2.Rows(idx).Cells(c_EndColumnName).Value
        Dim strStuName As String = tboxStuName.Text
        Dim strCName As String = dgvClass2.Rows(idx).Cells(c_ClassNameColumnName).Value.ToString.Trim
        Dim strScName As String = dgvClass2.Rows(idx).Cells(c_SubClassNameColumnName).Value.ToString.Trim
        Dim strSeat As String = dgvClass2.Rows(idx).Cells(c_SeatNumColumnName).Value.ToString.Trim
        Dim dtSession As New DataTable
        dtSession = frmMain.GetSessions
        Dim lstWd As New ArrayList
        Dim lstSessionStart As New ArrayList
        Dim lstSessionEnd As New ArrayList
        For index As Integer = 0 To dtSession.Rows.Count - 1
            If dtSession.Rows(index).Item(c_SubClassIDColumnName) = intScId Then
                lstWd.Add(dtSession.Rows(index).Item(c_DayOfWeekColumnName))
                lstSessionStart.Add(dtSession.Rows(index).Item(c_StartColumnName))
                lstSessionEnd.Add(dtSession.Rows(index).Item(c_EndColumnName))
            End If
        Next
        Dim intFeeOwe As Integer = GetIntValue(dgvClass2.Rows(idx).Cells(c_FeeOweCalculatedColumnName).Value)

        'Declare the title 
        Dim MyTitle As String = ""
        If dtPrintOpt.Rows(0).Item(c_DefaultTitleColumnName) = 1 Then
            MyTitle = GetClassTitle() & My.Resources.receipt
        Else
            MyTitle = GetClassTitle() & dtPrintOpt.Rows(0).Item(c_CustTitleColumnName).trim()
        End If

        If dtPrintOpt.Rows(0).Item(c_PrintReceiptNumColumnName) = 1 Then
            If (IsCopy = False) Then
                e.Graphics.DrawString(MyTitle & MyRecNum & "-" & My.Resources.receiptStu, _
                                      MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
            Else
                e.Graphics.DrawString(MyTitle & MyRecNum & "-" & My.Resources.receiptClass, _
                                      MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
            End If
        Else
            If (IsCopy = False) Then
                e.Graphics.DrawString(MyTitle & "-" & My.Resources.receiptStu, _
                                      MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
            Else
                e.Graphics.DrawString(MyTitle & "-" & My.Resources.receiptClass, _
                                      MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
            End If
        End If
        'The height of a string
        Dim dHLine As Integer = 0

        'height of Title string
        dHLine = e.Graphics.MeasureString(MyTitle, MyTitFont1).Height
        cursor_Y = cursor_Y + dHLine * 2
        Dim MyTitFont2 As New Font("Arial", 12, FontStyle.Regular)
        Dim MyTitle2 As String = My.Resources.classPeriod & " : "
        'The start Date and end date need to read from database
        Dim MyStartDate As String = GetROCDateFull(dateStart)
        Dim MyEndDate As String = GetROCDateFull(dateEnd)
        If dtPrintOpt.Rows(0).Item(c_PrintClassPeriodColumnName) = 1 Then
            e.Graphics.DrawString(MyTitle2 & MyStartDate & _
                                  My.Resources.startUntil & MyEndDate, _
                                  MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
        End If

        dHLine = e.Graphics.MeasureString(MyTitle2, MyTitFont2).Height
        'e.Graphics.MeasureString((MyTitle2, MyTitFont2,100).Height 
        cursor_Y = cursor_Y + dHLine
        Dim MyTitle3 As String = My.Resources.stuName & " : "
        'the Name should be read from database
        Dim MyName As String = strStuName
        e.Graphics.DrawString(MyTitle3 & MyName, MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

        'No need to recalculate the the character height since it should be same as the 2nd line
        cursor_Y = cursor_Y + dHLine
        Dim MyTitle4 As String = My.Resources.actualAmt & " : "
        'the Name should be read from database
        Dim MyFee As Integer = intReceiveAmt
        e.Graphics.DrawString(MyTitle4 & MyFee & My.Resources.NTComplete, _
                              MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

        cursor_Y = cursor_Y + dHLine
        'Define the Content format
        Dim MyCharMargin As Integer = 1
        Dim MyConformat1 As New StringFormat
        MyConformat1.Alignment = StringAlignment.Center
        MyConformat1.LineAlignment = StringAlignment.Center
        Dim MyConFont1 As New Font("Arial", 10, FontStyle.Regular)

        Dim MyConTit1 As String = My.Resources.payRec
        Dim MyConTit2 As String = My.Resources.regStatus
        dHLine = e.Graphics.MeasureString(MyConTit1, MyConFont1).Height
        Dim MyTableW As Integer = nPageW / 2 - nPageRL
        'Draw the table cell1
        cursor_Y = cursor_Y + dHLine
        Dim MyRect1 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit1, MyConFont1, Brushes.Black, MyRect1, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect1)

        'Draw the Table Cell2
        cursor_X = cursor_X + MyTableW
        Dim MyRect2 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit2, MyConFont1, Brushes.Black, MyRect2, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect2)

        'Draw the Table Cell3, there are 8 rows in each cell
        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        cursor_X = cursor_X - MyTableW
        Dim MyRect3 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 8)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect3)
        'There are 8 small cells in the Rect3, here can change to a loop and use array string 
        'Draw Cell 1
        Dim MyRect3Cell1 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW / 5 * 2, dHLine + MyCharMargin * 2)
        Dim MyCell1Con As String = My.Resources.receiptNum
        e.Graphics.DrawString(MyCell1Con, MyConFont1, Brushes.Black, MyRect3Cell1, MyConformat1)
        Dim MyRect3Cell3 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 2, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
        Dim MyCell3Con As String = My.Resources.strDate
        e.Graphics.DrawString(MyCell3Con, MyConFont1, Brushes.Black, MyRect3Cell3, MyConformat1)
        Dim MyRect3Cell5 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 3, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
        Dim MyCell5Con As String = My.Resources.payType
        e.Graphics.DrawString(MyCell5Con, MyConFont1, Brushes.Black, MyRect3Cell5, MyConformat1)
        Dim MyRect3Cell7 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 4, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
        Dim MyCell7Con As String = My.Resources.amount
        e.Graphics.DrawString(MyCell7Con, MyConFont1, Brushes.Black, MyRect3Cell7, MyConformat1)

        Dim MyRect3Cell2 As Rectangle
        Dim MyRect3Cell4 As Rectangle
        Dim MyRect3Cell6 As Rectangle
        Dim MyRect3Cell8 As Rectangle
        For index As Integer = 0 To lstReceiptNum.Count - 1
            If index > 3 Then Exit For
            MyRect3Cell2 = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5 * 2, dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstReceiptNum(index), MyConFont1, Brushes.Black, MyRect3Cell2, MyConformat1)

            MyRect3Cell4 = New Rectangle(cursor_X + MyTableW / 5 * 2, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstDate(index), MyConFont1, Brushes.Black, MyRect3Cell4, MyConformat1)

            MyRect3Cell6 = New Rectangle(cursor_X + MyTableW / 5 * 3, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstPayType(index), MyConFont1, Brushes.Black, MyRect3Cell6, MyConformat1)

            MyRect3Cell8 = New Rectangle(cursor_X + MyTableW / 5 * 4, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstAmount(index), MyConFont1, Brushes.Black, MyRect3Cell8, MyConformat1)
        Next
        'DrawCell 9 , this value should be read from the database
        Dim MyRect3Cell9 As Rectangle = New Rectangle(cursor_X, cursor_Y + (dHLine + MyCharMargin * 2) * 7, MyTableW, dHLine + MyCharMargin * 2)
        Dim MyCell9Con As String = ""
        If dtPrintOpt.Rows(0).Item(c_UseDefaultFeeOweColumnName) = 1 Then
            MyCell9Con = My.Resources.feeOwe & " : "
        Else
            MyCell9Con = dtPrintOpt.Rows(0).Item(c_CustFeeOweColumnName).trim & " : "
        End If

        Dim MyOweFee As Integer = intFeeOwe
        Dim MyConformat2 As New StringFormat
        MyConformat2.Alignment = StringAlignment.Near
        MyConformat2.LineAlignment = StringAlignment.Center
        If dtPrintOpt.Rows(0).Item(c_PrintFeeOweColumnName) = 1 Then
            e.Graphics.DrawString(MyCell9Con & MyOweFee, MyConFont1, Brushes.Black, MyRect3Cell9, MyConformat2)
        End If
        'Draw the Table Cell4
        cursor_X = cursor_X + MyTableW
        Dim MyRect4 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 8)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect4)

        Dim MyStNum As String = My.Resources.stu & "       " & My.Resources.num & " : "
        cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) / 2
        e.Graphics.DrawString(MyStNum & strStuId, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        Dim MyCla As String = My.Resources.registerClass & " : "
        'Get from database
        Dim MyClaVal As String = strCName
        e.Graphics.DrawString(MyCla & MyClaVal, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        Dim MyClaNum As String = My.Resources.registerSubClass & " : "
        'Get from database
        Dim MyClaNumVal As String = strScName
        e.Graphics.DrawString(MyClaNum & MyClaNumVal, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        Dim MySit As String = My.Resources.seat & "       " & My.Resources.num & " : "
        'Get from database
        Dim MySitNum As String = strSeat
        e.Graphics.DrawString(MySit & MySitNum, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        Dim MyTime As String = My.Resources.sessionTime & " : "
        'Get from database
        Dim MyTimeVal1 As String
        Dim MyTimeVal2 As String
        Dim wt As Byte = dtPrintOpt.Rows(0).Item(c_UseEnglishColumnName)
        If lstSessionStart.Count > 0 Then
            MyTimeVal1 = GetWeekDayName(lstWd(0), wt) & "." & _
                        GetTimeNum(lstSessionStart(0)) & "-" & _
                        GetTimeNum(lstSessionEnd(0))
            e.Graphics.DrawString(MyTime & MyTimeVal1, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)
        End If
        Dim cursor_Y2 As Integer = cursor_Y
        For index As Integer = 1 To lstSessionStart.Count - 1
            MyTimeVal2 = GetWeekDayName(lstWd(index), wt) & "." & _
                            GetTimeNum(lstSessionStart(index)) & "-" & _
                            GetTimeNum(lstSessionEnd(index))

            If index <= 3 Then
                'cursor_Y = cursor_Y + dHLine * index + MyCharMargin * 2
                cursor_Y2 = cursor_Y2 + dHLine + MyCharMargin * 2
                e.Graphics.DrawString(MyTimeVal2, MyConFont1, Brushes.Black, cursor_X + e.Graphics.MeasureString(MyTime, MyConFont1).Width, cursor_Y2, MyConformat2)
            ElseIf index > 3 And index < 8 Then
                If index = 4 Then
                    cursor_Y2 = cursor_Y2 - (dHLine + MyCharMargin * 2) * 3
                Else
                    cursor_Y2 = cursor_Y2 + dHLine + MyCharMargin * 2
                End If
                e.Graphics.DrawString(MyTimeVal2, MyConFont1, Brushes.Black, cursor_X + e.Graphics.MeasureString(MyTime & MyTimeVal2, MyConFont1).Width, cursor_Y2, MyConformat2)
            Else
                Exit For
            End If
        Next
        'cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        'Draw the bottom cell
        cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) * 7 / 2
        cursor_X = cursor_X - MyTableW
        Dim MyNote As String = My.Resources.remarks
        Dim MyNoteW As Integer = e.Graphics.MeasureString(MyNote, MyConFont1).Width * 2
        Dim MyRect5 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyNoteW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect5)
        e.Graphics.DrawString(MyNote, MyConFont1, Brushes.Black, MyRect5, MyConformat1)

        cursor_X = cursor_X + MyNoteW

        Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 2 - MyNoteW, dHLine + MyCharMargin * 2)
        'Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 2, dHLine + MyCharMargin * 2)
        Dim MyNoteVal As String = "  " & strRemarks
        If (IsCopy = False) Then
            'Display for the copy to the student
            e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)
            e.Graphics.DrawString(MyNoteVal, MyConFont1, Brushes.Black, MyRect6, MyConformat2)
        Else
            'Display for the copy to the school
            Dim MyDiscount As String = My.Resources.discount & " = "
            'Got from database
            Dim MyDiscountVal As Integer = intDiscount
            Dim MyDisReason As String = My.Resources.reason & " = "
            'Got from database
            Dim MyDisReVal As String = strDiscRemarks
            Dim MyNote2 As String = My.Resources.receiptRemarks & " = "
            e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)
            e.Graphics.DrawString(MyDiscount & MyDiscountVal & ", " & MyDisReason & MyDisReVal _
                                  & ", " & MyNote2 & MyNoteVal, MyConFont1, Brushes.Black, MyRect6, MyConformat2)
        End If

        cursor_X = cursor_X - MyNoteW
        cursor_Y = cursor_Y + dHLine + MyCharMargin * 4
        Dim MyRect7 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
        Dim MyAdmin As String = My.Resources.dealer & ": "
        Dim MyAdminVal As String = strHandler
        e.Graphics.DrawString(MyAdmin & MyAdminVal, MyConFont1, Brushes.Black, MyRect7, MyConformat2)

        cursor_X = cursor_X + MyTableW * 2 / 3
        Dim MyRect8 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
        Dim MyTeacher As String = My.Resources.teacherInCharge & ": "
        'Get from database
        Dim MyTeaVal As String = strSales
        If dtPrintOpt.Rows(0).Item(c_PrintNoSalesColumnName) = 0 Then
            e.Graphics.DrawString(MyTeacher & MyTeaVal, MyConFont1, Brushes.Black, MyRect8, MyConformat1)
        End If
        cursor_X = cursor_X + MyTableW * 2 / 3
        Dim MyRect9 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
        Dim MyConformat3 As New StringFormat
        MyConformat3.Alignment = StringAlignment.Far
        MyConformat3.LineAlignment = StringAlignment.Center
        Dim MyCell4Con As String = GetROCDateNum(datePay)
        e.Graphics.DrawString(MyCell3Con & ":" & MyCell4Con, MyConFont1, Brushes.Black, MyRect9, MyConformat3)

    End Sub
    'Private Sub DrawReceipt(ByVal IsCopy As Boolean, ByRef cursor_X As Integer, ByRef cursor_Y As Integer, ByVal e As Printing.PrintPageEventArgs)
    '    'Define the page unit
    '    e.Graphics.PageUnit = GraphicsUnit.Millimeter
    '    'Define the Title format
    '    Dim MyTitformat As New StringFormat
    '    MyTitformat.Alignment = StringAlignment.Near

    '    'Title fonts
    '    Dim MyTitFont1 As New Font("新細明體", 12, FontStyle.Bold)                              '100225
    '    Dim dtPrintOpt As New DataTable
    '    dtPrintOpt = frmMain.GetReceiptPrint
    '    If dtPrintOpt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1 Then
    '        If IsCopy Then
    '            Exit Sub
    '        End If
    '    End If
    '    'Page height and width, A4: 210x297
    '    Dim nPageH As Integer = 0
    '    Dim nPageW As Integer = 0

    '    'Page Margin
    '    Dim nPageRL As Integer = 0
    '    Dim nPageTB As Integer = 0
    '    Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)

    '    'If bytPaperSize = c_PaperSizeA4 Then
    '    '    nPageH = 297
    '    '    nPageW = 210
    '    '    nPageRL = 32
    '    '    nPageTB = 25
    '    'ElseIf bytPaperSize = c_PaperSizeB5 Then
    '    '    nPageH = 257
    '    '    nPageW = 182
    '    '    nPageRL = 28
    '    '    nPageTB = 22
    '    'Else
    '    '    nPageH = 297
    '    '    nPageW = 210
    '    '    nPageRL = 32
    '    '    nPageTB = 25
    '    'End If
    '    If bytPaperSize = c_PaperSizeA4 Then
    '        nPageH = 297
    '        nPageW = 210
    '        'nPageRL = 32
    '        'nPageTB = 30
    '        nPageRL = 16
    '        nPageTB = 15
    '    ElseIf bytPaperSize = c_PaperSizeB5 Then
    '        nPageH = 257
    '        nPageW = 182
    '        'nPageRL = 28
    '        'nPageTB = 26
    '        nPageRL = 12                                    '100208
    '        nPageTB = 12                                    '100208
    '    Else
    '        nPageH = 297
    '        nPageW = 210
    '        'nPageRL = 32
    '        'nPageTB = 30
    '        nPageRL = 16
    '        nPageTB = 15
    '    End If

    '    Dim intScId As Integer
    '    Dim strSales As String = ""
    '    Dim MyRecNum As String = ""
    '    Dim dtChequeValid As Date = Now
    '    Dim idx As Integer = dgvClass2.SelectedRows(0).Index
    '    dgvClass2.CurrentCell = dgvClass2.SelectedRows(0).Cells(c_SubClassNameColumnName)
    '    Dim lstReceiptNum As New ArrayList
    '    Dim lstDate As New ArrayList
    '    Dim lstPayType As New ArrayList
    '    Dim lstAmount As New ArrayList
    '    Dim intReceiveAmt As Integer
    '    Dim strRemarks As String = ""
    '    Dim strHandler As String = ""
    '    Dim datePay As Date = Now
    '    Dim intDiscount As Integer
    '    Dim strDiscRemarks As String = ""
    '    Dim strPayType As String = ""

    '    If dgvPayDetails.SelectedRows.Count > 0 Then
    '        Dim idx2 As Integer = dgvPayDetails.SelectedRows(0).Index
    '        dgvPayDetails.CurrentCell = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName)
    '        MyRecNum = dgvPayDetails.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value.ToString.Trim
    '        intReceiveAmt = dgvPayDetails.SelectedRows(0).Cells(c_AmountColumnName).Value
    '        strRemarks = dgvPayDetails.SelectedRows(0).Cells(c_RemarksColumnName).Value.ToString.Trim
    '        strHandler = dgvPayDetails.SelectedRows(0).Cells(c_HandlerColumnName).Value.ToString.Trim
    '        datePay = dgvPayDetails.SelectedRows(0).Cells(c_DateTimeColumnName).Value
    '        intDiscount = dgvPayDetails.SelectedRows(0).Cells(c_DiscountColumnName).Value
    '        strDiscRemarks = dgvPayDetails.SelectedRows(0).Cells(c_DiscountRemarksColumnName).Value.ToString.Trim
    '        strPayType = dgvPayDetails.SelectedRows(0).Cells(c_PayMethodNameColumnName).Value
    '    End If
    '    intScId = dgvClass2.Rows(idx).Cells(c_SubClassIDColumnName).Value
    '    If dtDisc.Rows.Count > 0 And intDiscount = 0 Then
    '        For i As Integer = 0 To dtDisc.Rows.Count - 1
    '            If dtDisc.Rows(i).Item(c_SubClassIDColumnName) = intScId Then
    '                intDiscount = dtDisc.Rows(i).Item(c_DiscountColumnName)
    '                strDiscRemarks = dtDisc.Rows(i).Item(c_RemarksColumnName)
    '            End If
    '        Next
    '    End If

    '    Dim intPrintToday As Byte = dtPrintOpt.Rows(0).Item(c_PrintPayTodayColumnName)
    '    If intPrintToday = 0 Then
    '        For Each row As DataGridViewRow In dgvPayDetails.Rows
    '            If row.Cells(c_SubClassIDColumnName).Value = intScId Then
    '                lstReceiptNum.Add(row.Cells(c_ReceiptNumColumnName).Value.ToString.Trim)
    '                lstDate.Add(GetROCDateNum(row.Cells(c_DateTimeColumnName).Value))
    '                lstPayType.Add(strPayType)
    '                lstAmount.Add(row.Cells(c_AmountColumnName).Value.ToString.Trim)
    '            End If
    '        Next
    '    Else
    '        lstReceiptNum.Add(MyRecNum)
    '        lstDate.Add(GetROCDateNum(datePay))
    '        lstPayType.Add(strPayType)
    '        lstAmount.Add(intReceiveAmt)
    '    End If

    '    strSales = dgvClass2.Rows(idx).Cells(c_SalesPersonColumnName).Value.ToString.Trim
    '    Dim dateStart As Date = dgvClass2.Rows(idx).Cells(c_StartColumnName).Value
    '    Dim dateEnd As Date = dgvClass2.Rows(idx).Cells(c_EndColumnName).Value
    '    Dim strStuName As String = tboxStuName.Text
    '    Dim strCName As String = dgvClass2.Rows(idx).Cells(c_ClassNameColumnName).Value.ToString.Trim
    '    Dim strScName As String = dgvClass2.Rows(idx).Cells(c_SubClassNameColumnName).Value.ToString.Trim
    '    Dim strSeat As String = dgvClass2.Rows(idx).Cells(c_SeatNumColumnName).Value.ToString.Trim
    '    Dim dtSession As New DataTable
    '    dtSession = frmMain.GetSessions
    '    Dim lstWd As New ArrayList
    '    Dim lstSessionStart As New ArrayList
    '    Dim lstSessionEnd As New ArrayList
    '    For index As Integer = 0 To dtSession.Rows.Count - 1
    '        If dtSession.Rows(index).Item(c_SubClassIDColumnName) = intScId Then
    '            lstWd.Add(dtSession.Rows(index).Item(c_DayOfWeekColumnName))
    '            lstSessionStart.Add(dtSession.Rows(index).Item(c_StartColumnName))
    '            lstSessionEnd.Add(dtSession.Rows(index).Item(c_EndColumnName))
    '        End If
    '    Next
    '    Dim intFeeOwe As Integer = GetIntValue(dgvClass2.Rows(idx).Cells(c_FeeOweCalculatedColumnName).Value)               '100304

    '    'Declare the title 
    '    Dim MyTitle As String = ""
    '    If dtPrintOpt.Rows(0).Item(c_DefaultTitleColumnName) = 1 Then
    '        MyTitle = GetClassTitle() & My.Resources.receipt
    '    Else
    '        MyTitle = GetClassTitle() & dtPrintOpt.Rows(0).Item(c_CustTitleColumnName).trim()
    '    End If

    '    If dtPrintOpt.Rows(0).Item(c_PrintReceiptNumColumnName) = 1 Then
    '        If (IsCopy = False) Then
    '            e.Graphics.DrawString(MyTitle & MyRecNum & "-" & My.Resources.receiptStu, _
    '                                  MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '        Else
    '            e.Graphics.DrawString(MyTitle & MyRecNum & "-" & My.Resources.receiptClass, _
    '                                  MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '        End If
    '    Else
    '        If (IsCopy = False) Then
    '            e.Graphics.DrawString(MyTitle & "-" & My.Resources.receiptStu, _
    '                                  MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '        Else
    '            e.Graphics.DrawString(MyTitle & "-" & My.Resources.receiptClass, _
    '                                  MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '        End If
    '    End If
    '    'The height of a string
    '    Dim dHLine As Integer = 0

    '    'height of Title string
    '    dHLine = e.Graphics.MeasureString(MyTitle, MyTitFont1).Height
    '    cursor_Y = cursor_Y + dHLine * 2
    '    Dim MyTitFont2 As New Font("新細明體", 12, FontStyle.Regular)                            '100225
    '    Dim MyTitle2 As String = My.Resources.classPeriod & " : "
    '    'The start Date and end date need to read from database
    '    Dim MyStartDate As String = GetROCDateFull(dateStart)
    '    Dim MyEndDate As String = GetROCDateFull(dateEnd)
    '    If dtPrintOpt.Rows(0).Item(c_PrintClassPeriodColumnName) = 1 Then
    '        e.Graphics.DrawString(MyTitle2 & MyStartDate & _
    '                              My.Resources.startUntil & MyEndDate, _
    '                              MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '    End If

    '    dHLine = e.Graphics.MeasureString(MyTitle2, MyTitFont2).Height
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyTitle3 As String = My.Resources.stuName & " : "
    '    'the Name should be read from database
    '    Dim MyName As String = strStuName
    '    e.Graphics.DrawString(MyTitle3 & MyName, MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

    '    'No need to recalculate the the character height since it should be same as the 2nd line
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyTitle4 As String = My.Resources.actualAmt & " : "
    '    'the Name should be read from database
    '    Dim MyFee As Integer = intReceiveAmt
    '    e.Graphics.DrawString(MyTitle4 & MyFee & My.Resources.NTComplete, _
    '                          MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

    '    cursor_Y = cursor_Y + dHLine
    '    'Define the Content format
    '    Dim MyCharMargin As Integer = 1
    '    Dim MyConformat1 As New StringFormat
    '    MyConformat1.Alignment = StringAlignment.Center
    '    MyConformat1.LineAlignment = StringAlignment.Center
    '    Dim MyConFont1 As New Font("新細明體", 10, FontStyle.Regular)                          '100225

    '    Dim MyConTit1 As String = My.Resources.payRec
    '    Dim MyConTit2 As String = My.Resources.regStatus
    '    dHLine = e.Graphics.MeasureString(MyConTit1, MyConFont1).Height
    '    Dim MyTableW As Integer = nPageW / 2 - nPageRL
    '    'Draw the table cell1
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyRect1 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit1, MyConFont1, Brushes.Black, MyRect1, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect1)

    '    'Draw the Table Cell2
    '    cursor_X = cursor_X + MyTableW
    '    Dim MyRect2 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit2, MyConFont1, Brushes.Black, MyRect2, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect2)

    '    'Draw the Table Cell3, there are 8 rows in each cell
    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    cursor_X = cursor_X - MyTableW
    '    Dim MyRect3 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 8)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect3)
    '    'There are 8 small cells in the Rect3, here can change to a loop and use array string 
    '    'Draw Cell 1
    '    Dim MyRect3Cell1 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW / 5 * 2, dHLine + MyCharMargin * 2)
    '    Dim MyCell1Con As String = My.Resources.receiptNum
    '    e.Graphics.DrawString(MyCell1Con, MyConFont1, Brushes.Black, MyRect3Cell1, MyConformat1)
    '    Dim MyRect3Cell3 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 2, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
    '    Dim MyCell3Con As String = My.Resources.strDate
    '    e.Graphics.DrawString(MyCell3Con, MyConFont1, Brushes.Black, MyRect3Cell3, MyConformat1)
    '    Dim MyRect3Cell5 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 3, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
    '    Dim MyCell5Con As String = My.Resources.payType
    '    e.Graphics.DrawString(MyCell5Con, MyConFont1, Brushes.Black, MyRect3Cell5, MyConformat1)
    '    Dim MyRect3Cell7 As Rectangle = New Rectangle(cursor_X + MyTableW / 5 * 4, cursor_Y, MyTableW / 5, dHLine + MyCharMargin * 2)
    '    Dim MyCell7Con As String = My.Resources.amount
    '    e.Graphics.DrawString(MyCell7Con, MyConFont1, Brushes.Black, MyRect3Cell7, MyConformat1)

    '    Dim MyRect3Cell2 As Rectangle
    '    Dim MyRect3Cell4 As Rectangle
    '    Dim MyRect3Cell6 As Rectangle
    '    Dim MyRect3Cell8 As Rectangle
    '    For index As Integer = 0 To lstReceiptNum.Count - 1
    '        If index > 3 Then Exit For
    '        MyRect3Cell2 = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5 * 2, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstReceiptNum(index), MyConFont1, Brushes.Black, MyRect3Cell2, MyConformat1)

    '        MyRect3Cell4 = New Rectangle(cursor_X + MyTableW / 5 * 2, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstDate(index), MyConFont1, Brushes.Black, MyRect3Cell4, MyConformat1)

    '        MyRect3Cell6 = New Rectangle(cursor_X + MyTableW / 5 * 3, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstPayType(index), MyConFont1, Brushes.Black, MyRect3Cell6, MyConformat1)

    '        MyRect3Cell8 = New Rectangle(cursor_X + MyTableW / 5 * 4, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW / 5, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstAmount(index), MyConFont1, Brushes.Black, MyRect3Cell8, MyConformat1)
    '    Next
    '    'DrawCell 9 , this value should be read from the database
    '    Dim MyRect3Cell9 As Rectangle = New Rectangle(cursor_X, cursor_Y + (dHLine + MyCharMargin * 2) * 7, MyTableW, dHLine + MyCharMargin * 2)
    '    Dim MyCell9Con As String = ""
    '    If dtPrintOpt.Rows(0).Item(c_UseDefaultFeeOweColumnName) = 1 Then
    '        MyCell9Con = My.Resources.feeOwe & " : "
    '    Else
    '        MyCell9Con = dtPrintOpt.Rows(0).Item(c_CustFeeOweColumnName).trim & " : "
    '    End If

    '    Dim MyOweFee As Integer = intFeeOwe
    '    Dim MyConformat2 As New StringFormat
    '    MyConformat2.Alignment = StringAlignment.Near
    '    MyConformat2.LineAlignment = StringAlignment.Center
    '    If dtPrintOpt.Rows(0).Item(c_PrintFeeOweColumnName) = 1 Then
    '        e.Graphics.DrawString(MyCell9Con & MyOweFee, MyConFont1, Brushes.Black, MyRect3Cell9, MyConformat2)
    '    End If
    '    'Draw the Table Cell4
    '    cursor_X = cursor_X + MyTableW
    '    Dim MyRect4 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 8)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect4)

    '    Dim MyStNum As String = My.Resources.stu & "       " & My.Resources.num & " : "
    '    cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) / 2
    '    e.Graphics.DrawString(MyStNum & strStuId, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    Dim MyCla As String = My.Resources.registerClass & " : "
    '    'Get from database
    '    Dim MyClaVal As String = strCName
    '    e.Graphics.DrawString(MyCla & MyClaVal, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    Dim MyClaNum As String = My.Resources.registerSubClass & " : "
    '    'Get from database
    '    Dim MyClaNumVal As String = strScName
    '    e.Graphics.DrawString(MyClaNum & MyClaNumVal, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    Dim MySit As String = My.Resources.seat & "       " & My.Resources.num & " : "
    '    'Get from database
    '    Dim MySitNum As String = strSeat
    '    e.Graphics.DrawString(MySit & MySitNum, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)

    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    Dim MyTime As String = My.Resources.sessionTime & " : "
    '    'Get from database
    '    Dim MyTimeVal1 As String
    '    Dim MyTimeVal2 As String
    '    Dim wt As Byte = dtPrintOpt.Rows(0).Item(c_UseEnglishColumnName)
    '    If lstSessionStart.Count > 0 Then
    '        MyTimeVal1 = GetWeekDayName(lstWd(0), wt) & "." & _
    '                    GetTimeNum(lstSessionStart(0)) & "-" & _
    '                    GetTimeNum(lstSessionEnd(0))
    '        e.Graphics.DrawString(MyTime & MyTimeVal1, MyConFont1, Brushes.Black, cursor_X, cursor_Y, MyConformat2)
    '    End If
    '    For index As Integer = 1 To lstSessionStart.Count - 1
    '        MyTimeVal2 = GetWeekDayName(lstWd(index), wt) & "." & _
    '                        GetTimeNum(lstSessionStart(index)) & "-" & _
    '                        GetTimeNum(lstSessionEnd(index))

    '        If index <= 3 Then
    '            'cursor_Y = cursor_Y + dHLine * index + MyCharMargin * 2
    '            cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '            e.Graphics.DrawString(MyTimeVal2, MyConFont1, Brushes.Black, cursor_X + e.Graphics.MeasureString(MyTime, MyConFont1).Width, cursor_Y, MyConformat2)
    '        ElseIf index > 3 And index < 8 Then
    '            If index = 4 Then
    '                cursor_Y = cursor_Y - (dHLine + MyCharMargin * 2) * 3
    '            Else
    '                cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '            End If
    '            e.Graphics.DrawString(MyTimeVal2, MyConFont1, Brushes.Black, cursor_X + e.Graphics.MeasureString(MyTime & MyTimeVal2, MyConFont1).Width, cursor_Y, MyConformat2)
    '        Else
    '            Exit For
    '        End If
    '    Next
    '    'Draw the bottom cell
    '    cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) * 7 / 2 - 14          '100226    減14因為新細明體比Arial 大1 height
    '    cursor_X = cursor_X - MyTableW
    '    Dim MyNote As String = My.Resources.remarks
    '    Dim MyNoteW As Integer = e.Graphics.MeasureString(MyNote, MyConFont1).Width * 2
    '    Dim MyRect5 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyNoteW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect5)
    '    e.Graphics.DrawString(MyNote, MyConFont1, Brushes.Black, MyRect5, MyConformat1)

    '    cursor_X = cursor_X + MyNoteW

    '    Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 2 - MyNoteW, dHLine + MyCharMargin * 2)
    '    'Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 2, dHLine + MyCharMargin * 2)
    '    Dim MyNoteVal As String = "  " & strRemarks
    '    If (IsCopy = False) Then
    '        'Display for the copy to the student
    '        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)
    '        e.Graphics.DrawString(MyNoteVal, MyConFont1, Brushes.Black, MyRect6, MyConformat2)
    '    Else
    '        'Display for the copy to the school
    '        Dim MyDiscount As String = My.Resources.discount & " = "
    '        'Got from database
    '        Dim MyDiscountVal As Integer = intDiscount
    '        Dim MyDisReason As String = My.Resources.reason & " = "
    '        'Got from database
    '        Dim MyDisReVal As String = strDiscRemarks
    '        Dim MyNote2 As String = My.Resources.receiptRemarks & " = "
    '        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)
    '        e.Graphics.DrawString(MyDiscount & MyDiscountVal & ", " & MyDisReason & MyDisReVal _
    '                              & ", " & MyNote2 & MyNoteVal, MyConFont1, Brushes.Black, MyRect6, MyConformat2)
    '    End If

    '    cursor_X = cursor_X - MyNoteW
    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 4
    '    Dim MyRect7 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyAdmin As String = My.Resources.dealer & ": "
    '    Dim MyAdminVal As String = strHandler
    '    e.Graphics.DrawString(MyAdmin & MyAdminVal, MyConFont1, Brushes.Black, MyRect7, MyConformat2)

    '    cursor_X = cursor_X + MyTableW * 2 / 3
    '    Dim MyRect8 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyTeacher As String = My.Resources.teacherInCharge & ": "
    '    'Get from database
    '    Dim MyTeaVal As String = strSales
    '    If dtPrintOpt.Rows(0).Item(c_PrintNoSalesColumnName) = 0 Then
    '        e.Graphics.DrawString(MyTeacher & MyTeaVal, MyConFont1, Brushes.Black, MyRect8, MyConformat1)
    '    End If
    '    cursor_X = cursor_X + MyTableW * 2 / 3
    '    Dim MyRect9 As New Rectangle(cursor_X, cursor_Y, MyTableW * 2 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyConformat3 As New StringFormat
    '    MyConformat3.Alignment = StringAlignment.Far
    '    MyConformat3.LineAlignment = StringAlignment.Center
    '    Dim MyCell4Con As String = GetROCDateNum(datePay)
    '    e.Graphics.DrawString(MyCell3Con & MyCell4Con, MyConFont1, Brushes.Black, MyRect9, MyConformat3)

    'End Sub

    Private Sub butPayPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayPrint.Click
        If frmMain.CheckAuth(6) Or frmMain.CheckAuth(19) Then
            If dgvClass2.SelectedRows.Count > 0 And dgvPayDetails.SelectedRows.Count > 0 Then
                prtdocReceipt.Print()
            End If
        Else
            frmMain.ShowNoAuthMsg()
        End If

    End Sub

    Private Sub butPri_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPri.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxPrimarySch.Items.Count - 1
                If cboxPrimarySch.Items(index).ToString.Contains(name) Then
                    cboxPrimarySch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butJun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butJun.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxJuniorSch.Items.Count - 1
                If cboxJuniorSch.Items(index).ToString.Contains(name) Then
                    cboxJuniorSch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butHig_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butHig.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxHighSch.Items.Count - 1
                If cboxHighSch.Items(index).ToString.Contains(name) Then
                    cboxHighSch.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butUni_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butUni.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To cboxUniversity.Items.Count - 1
                If cboxUniversity.Items(index).ToString.Contains(name) Then
                    cboxUniversity.SelectedIndex = index
                    Exit Sub
                End If
            Next
        End If
    End Sub

    Private Sub butPayTogether_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayTogether.Click
        frmMain.SetCurrentStu(strStuId)
        frmMain.SetOkState(False)
        Dim pass As Boolean = chkboxShowPast2.Checked
        Dim frm As New frm2PayTogether(dtClassReg2, dtDisc, pass)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshClassRegData()
            Dim result As MsgBoxResult = MsgBox(My.Resources.msgAskPrintReceipt, MsgBoxStyle.YesNo, My.Resources.msgTitle)
            If result = MsgBoxResult.Yes Then
                frmMain.SetCurrentString(tboxStuName.Text.Trim)
                frmMain.SetOkState(False)
                Dim frm2 As New frm2PrintTogether
                frm2.SetDt(dtClassReg2, dtPayRec, dtDisc)
                frm2.ShowDialog()
            End If
        End If
    End Sub

    Private Sub butPayPrintTogether_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayPrintTogether.Click
        frmMain.SetCurrentStu(strStuId)
        frmMain.SetCurrentString(tboxStuName.Text.Trim)
        frmMain.SetOkState(False)
        Dim frm As New frm2PrintTogether
        frm.SetDt(dtClassReg2, dtPayRec, dtDisc)
        frm.ShowDialog()
    End Sub

    Private Sub butSeatNum_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSeatNum.Click
        If dgvClass.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvClass.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim intSubClassId As Integer = dgvClass.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim seat As String = dgvClass.SelectedRows(0).Cells(c_SeatNumColumnName).Value.ToString.Trim
            frmMain.SetOkState(False)

            Dim subfrmSeatMap As New frm2SeatMap(intSubClassId, strStuId)
            subfrmSeatMap.ShowDialog()
            If frmMain.GetOkState Then
                Dim seat2 As String = frmMain.GetSeatNum
                objCsol.ChangeSeatNum(id, intSubClassId, seat2, seat, strStuId)
                frmMain.SetSeatNum("")
                RefreshClassRegData()
            End If


        End If
    End Sub

    Private Sub butPayOthers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPayOthers.Click

    End Sub

    Private Sub butModDisc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butModDisc.Click
        If Not frmMain.CheckAuth(83) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        If butModDisc.Text = My.Resources.modDisc Then
            tboxDiscount.Enabled = True
            tboxDiscountRemarks.Enabled = True
            butModDisc.Text = My.Resources.cancelModDisc
        Else
            tboxDiscount.Enabled = False
            tboxDiscountRemarks.Enabled = False
            butModDisc.Text = My.Resources.modDisc
        End If

    End Sub

    Private Sub butTeleListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTeleListSta.Click
        RefreshTeleTable()
    End Sub

    Private Sub RefreshTeleTable()
        Dim strCompute1 As String = ""
        Dim strCompute2 As String = ""
        Dim strF As String = ""

        If dtTeleInterview.Rows.Count = 0 Then
            dgvTeleRec.DataSource = dtTeleInterview
            LoadColumnTextTeleInterview()
            Exit Sub
        End If

        If chkboxTeleDate.Checked = True Then
            strCompute1 = c_DateTimeColumnName & ">='" & GetDateStart(dtpickTele.Value).ToString & "' AND " & _
                c_DateTimeColumnName & "<='" & GetDateEnd(dtpickTele.Value).ToString & "'"
        End If

        If cboxTeleType.SelectedIndex > 0 Then
            strCompute2 = c_TypeIdColumnName & "=" & lstTeleInterviewType(cboxTeleType.SelectedIndex)
        End If

        If strCompute1.Length = 0 Then
            If strCompute2.Length > 0 Then
                strF = strCompute2
            End If
        Else
            If strCompute2.Length > 0 Then
                strF = strCompute1 & " AND " & strCompute2
            Else
                strF = strCompute1
            End If
        End If

        dtTeleInterview.DefaultView.RowFilter = strF

        dgvTeleRec.DataSource = dtTeleInterview

        LoadColumnTextTeleInterview()
    End Sub

    Private Sub butTeleAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTeleAdd.Click
        If strStuId.Length = 8 Then
            frmMain.SetOkState(False)
            Dim frm As New frm2AddTeleInterviewStu(strStuId)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshTeleInterviewData()
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub butTeleMod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTeleMod.Click
        If dgvTeleRec.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvTeleRec.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim typeId As Integer = dgvTeleRec.SelectedRows(0).Cells(c_TypeIdColumnName).Value
            Dim handler As String = dgvTeleRec.SelectedRows(0).Cells(c_HandlerIDColumnName).Value.ToString.Trim
            Dim content As String = dgvTeleRec.SelectedRows(0).Cells(c_ContentColumnName).Value
            If handler.Equals(frmMain.GetUsrId.Trim) Then
                Dim frm As New frm2UpdTeleInterviewStu(id, typeId, content)
                frm.ShowDialog()
                If frmMain.GetOkState Then
                    RefreshTeleInterviewData()
                    frmMain.SetOkState(False)
                End If
            Else
                ShowMsg(My.Resources.msgModTeleError)
            End If
        End If
    End Sub

    Private Sub butTeleDel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTeleDel.Click
        If dgvTeleRec.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim id As Integer = dgvTeleRec.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim handler As String = dgvTeleRec.SelectedRows(0).Cells(c_HandlerIDColumnName).Value.ToString.Trim
            If handler.Equals(frmMain.GetUsrId.Trim) Then
                objCsol.DeleteTeleInterview(id)
                RefreshTeleInterviewData()
            Else
                ShowMsg(My.Resources.msgDelTeleError)
            End If
        End If
    End Sub

    Private Sub butBookPrint2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBookPrint2.Click
        PrintDocument5.DefaultPageSettings.Landscape = True
        PrintDocument5.Print()
    End Sub

    Private Sub butBookExport2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBookExport2.Click
        ExportDgvToExcel(dgvBookNotReceive)
    End Sub

    Private Sub butBookPrint1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBookPrint1.Click
        PrintDocument4.DefaultPageSettings.Landscape = True
        PrintDocument4.Print()
    End Sub

    Private Sub butAbsentPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAbsentPrint.Click

    End Sub

    Private Sub PrintDocument4_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument4.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvBookReceive.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument4_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument4.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvBookReceive.Columns
                If oColumn.Visible = True Then

                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvBookReceive.Rows.Count

            Dim oRow As DataGridViewRow = dgvBookReceive.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvBookReceive.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvBookReceive.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvBookReceive.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter4(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter4(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvBookReceive.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvBookReceive.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvBookReceive.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvBookReceive.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvBookReceive.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvBookReceive.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument5_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument5.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvBookNotReceive.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument5_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument5.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        'If nPageNo = 1 Then

        For Each oColumn As DataGridViewColumn In dgvBookNotReceive.Columns
            If oColumn.Visible = True Then

                nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                oColumnLefts.Add(nLeft)
                oColumnWidths.Add(nWidth)
                oColumnTypes.Add(oColumn.GetType)
                nLeft += nWidth
            End If
        Next

        'End If

        Do While nRowPos < dgvBookNotReceive.Rows.Count

            Dim oRow As DataGridViewRow = dgvBookNotReceive.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvBookNotReceive.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvBookNotReceive.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvBookNotReceive.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter5(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter5(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvBookNotReceive.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvBookNotReceive.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvBookNotReceive.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvBookNotReceive.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvBookNotReceive.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvBookNotReceive.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument6_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument6.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvTeleRec.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument6_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument6.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvTeleRec.Columns
                If oColumn.Visible = True Then

                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvTeleRec.Rows.Count

            Dim oRow As DataGridViewRow = dgvTeleRec.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvTeleRec.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvTeleRec.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvTeleRec.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter6(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter6(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvTeleRec.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvTeleRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvTeleRec.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvTeleRec.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvTeleRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvTeleRec.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub butBookExport1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butBookExport1.Click
        ExportDgvToExcel(dgvBookReceive)
    End Sub

    Private Sub butTelePrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTelePrint.Click
        PrintDocument6.DefaultPageSettings.Landscape = True
        PrintDocument6.Print()
    End Sub

    Private Sub butTeleExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butTeleExport.Click
        ExportDgvToExcel(dgvTeleRec)
    End Sub

    Private Sub radbutAttShowAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutAttShowAll.CheckedChanged
        ChangeAttRecRange()
    End Sub

    Private Sub radbutAttShowRange_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutAttShowRange.CheckedChanged
        ChangeAttRecRange()
    End Sub

    Private Sub dtpickFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpickFrom.ValueChanged
        If dtpickFrom.Value <= dtpickTo.Value Then
            ChangeAttRecRange()
        End If
    End Sub

    Private Sub dtpickTo_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpickTo.ValueChanged
        If dtpickFrom.Value <= dtpickTo.Value Then
            ChangeAttRecRange()
        End If
    End Sub

    Private Sub tboxPayToday_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxPayToday.TextChanged
        UpdatePayDisplay()


    End Sub
    Private Sub UpdatePayDisplay()
        tboxReceiptRemarks.Text = ""
        Dim idx As Integer = 0
        If Not dgvClass2.CurrentCell Is Nothing Then
            idx = dgvClass2.CurrentCell.RowIndex
        End If
        If dgvClass2.Rows.Count > 0 Then
            If tboxPayToday.Text = "" Then
                dgvClass2.Rows(idx).Cells(c_FeeOweCalculatedColumnName).Value = dgvClass2.Rows(idx).Cells(c_AmountColumnName).Value - _
                dgvClass2.Rows(idx).Cells(c_PayAmountColumnName).Value
                dgvPayDetails_SelectionChanged(Nothing, Nothing)
            Else
                Dim payToday As Integer = CInt(tboxPayToday.Text)
                dgvClass2.Rows(idx).Cells(c_FeeOweCalculatedColumnName).Value = dgvClass2.Rows(idx).Cells(c_AmountColumnName).Value - _
                dgvClass2.Rows(idx).Cells(c_PayAmountColumnName).Value - payToday
            End If
        End If
    End Sub

    Private Sub btnDiscCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDiscCancel.Click
        If Not frmMain.CheckAuth(83) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        tboxDiscount.Enabled = True
        tboxDiscountRemarks.Enabled = True
        tboxDiscount.Text = ""
        tboxDiscountRemarks.Text = ""
    End Sub

    Private Sub rtboxRemarks_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles rtboxRemarks.MouseUp
        If e.Button = Windows.Forms.MouseButtons.Right Then
            rtboxRemarks.ContextMenuStrip.Show()
        End If
    End Sub


    Private Sub ToolStripMenuItem1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem1.Click
        '剪下
        rtboxRemarks.Cut()
    End Sub

    Private Sub ToolStripMenuItem2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem2.Click
        '複製
        rtboxRemarks.Copy()
    End Sub

    Private Sub ToolStripMenuItem3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ToolStripMenuItem3.Click
        '貼上
        rtboxRemarks.Paste()
    End Sub

    Private Sub dgvPayDetails_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvPayDetails.SelectionChanged
        If dgvPayDetails.Rows.Count = 0 Then
            tboxReceiptRemarks.Text = ""
            Exit Sub
        End If
        Try
            tboxReceiptRemarks.Text = dgvPayDetails.CurrentRow.Cells("Remarks").Value.ToString
        Catch ex As Exception

        End Try
    End Sub

    Private Sub RefreshStuSubClass()
        Dim dgvcla As DataTable = dgvClass.DataSource
        Dim lstSubClass As New ArrayList
        For i As Integer = 0 To dgvcla.Rows.Count - 1
            lstSubClass.Add(dgvcla.Rows(i).Item(c_SubClassIDColumnName))
        Next
        frmMain.SetlstStuSubClass(lstSubClass)
    End Sub

    Private Sub RefreshAssgin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefreshAssgin.Click
        RefreshAssignData()
    End Sub



End Class