﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2UpdPunchRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tboxHour = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxMin = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.radioIn = New System.Windows.Forms.RadioButton
        Me.radioOut = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'tboxHour
        '
        Me.tboxHour.Location = New System.Drawing.Point(53, 36)
        Me.tboxHour.Name = "tboxHour"
        Me.tboxHour.Size = New System.Drawing.Size(34, 22)
        Me.tboxHour.TabIndex = 134
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(12, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 132
        Me.Label1.Text = "時間: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(201, 64)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(75, 29)
        Me.butCancel.TabIndex = 131
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(201, 5)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(75, 29)
        Me.butSave.TabIndex = 130
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(12, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 129
        Me.Label2.Text = "日期: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(53, 8)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(119, 22)
        Me.dtpickDate.TabIndex = 135
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(93, 41)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(8, 12)
        Me.Label3.TabIndex = 136
        Me.Label3.Text = ":"
        '
        'tboxMin
        '
        Me.tboxMin.Location = New System.Drawing.Point(107, 36)
        Me.tboxMin.Name = "tboxMin"
        Me.tboxMin.Size = New System.Drawing.Size(34, 22)
        Me.tboxMin.TabIndex = 137
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(12, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 138
        Me.Label4.Text = "類型: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'radioIn
        '
        Me.radioIn.AutoSize = True
        Me.radioIn.Location = New System.Drawing.Point(53, 72)
        Me.radioIn.Name = "radioIn"
        Me.radioIn.Size = New System.Drawing.Size(47, 16)
        Me.radioIn.TabIndex = 139
        Me.radioIn.TabStop = True
        Me.radioIn.Text = "上課"
        Me.radioIn.UseVisualStyleBackColor = True
        '
        'radioOut
        '
        Me.radioOut.AutoSize = True
        Me.radioOut.Location = New System.Drawing.Point(106, 72)
        Me.radioOut.Name = "radioOut"
        Me.radioOut.Size = New System.Drawing.Size(47, 16)
        Me.radioOut.TabIndex = 140
        Me.radioOut.TabStop = True
        Me.radioOut.Text = "下課"
        Me.radioOut.UseVisualStyleBackColor = True
        '
        'frm2UpdPunchRec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(291, 107)
        Me.Controls.Add(Me.radioOut)
        Me.Controls.Add(Me.radioIn)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tboxMin)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxHour)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2UpdPunchRec"
        Me.Text = "修改刷卡記錄"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tboxHour As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxMin As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents radioIn As System.Windows.Forms.RadioButton
    Friend WithEvents radioOut As System.Windows.Forms.RadioButton
End Class
