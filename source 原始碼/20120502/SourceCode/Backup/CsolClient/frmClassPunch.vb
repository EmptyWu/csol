﻿Public Class frmClassPunch
    
    Private dtResult As New DataTable
    Private LS As New ArrayList
    Private Stuinfo As New DataTable
    Private PunchResultType As Integer
    Private idx As Integer
    Private strStuName As String
    Private strClassName As String
    Private strSubclassName As String
    Private strSeat As String
    Private strContent As String
    Private strSession As String
    Private strPunchTime As String
    Private strBooks As String
    Private strBookResults As String
    Private strNote As String
    Private intClassMode As Integer
    Private strCardNum As String

    Private blIsID As Boolean = False
    Private intWaitUsb As Integer
    Private strStuId As String
    Private blBookIssue As Boolean

    Private strLblName As String
    Private strLblClass As String
    Private strLblBook As String
    Private strLblNote As String

    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private dtContent As New DataTable
    Private dtSession As New DataTable
    Private dtBook As New DataTable
    Private dtPay As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstContens As New ArrayList
    Private lstBook As New ArrayList
    Private lstBookShown As New ArrayList
    Private lstMulti As New ArrayList
    Private lstMultiShown As New ArrayList
    Private dtStu As New DataTable
    Private intSubClass As Integer
    Private intClass As Integer
    Private bookList As New ArrayList
    Private refreshFlag As Boolean


    Public Sub New()
        InitializeComponent()
        Me.Text = My.Resources.frmClassPunch
        ClearMsg()
        'RefreshData()
        'dtBook = frmMain.GetBooks
        Dim aa As DataTable = objCsol.GetClassPunchBook(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
        dtBook = frmMain.GetClassPunchBook
        dtContent = frmMain.GetClassPunchContent
        refreshFlag = True
    End Sub

    Private Sub frmClassPunch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmClassPunch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        PrepareStart()
    End Sub

    Private Sub PrepareStart()
        tboxID.Clear()
        tboxID.Focus()
    End Sub

    Private Sub ClearMsg()
        blIsID = False
        strLblName = ""
        strLblClass = ""
        strLblBook = ""
        strLblNote = ""
        lblMsg.Text = ""
        tboxScreen.Clear()
    End Sub

    Private Sub MakeScreen()
        tboxScreen.Text = strLblName & strLblClass & strLblBook & strLblNote
    End Sub

    Private Sub frmClassPunch_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub PunchWithCard()
        Try
            strCardNum = tboxID.Text.Trim.ToUpper
            strPunchTime = GetTimeString(Now)

            If radbutBeginClass.Checked Then
                intClassMode = c_ClassIn
            ElseIf radbutEndClass.Checked Then
                intClassMode = c_ClassOut
            End If

            If chkboxBook.Checked Then
                blBookIssue = True                             '100222
            Else
                blBookIssue = False
            End If

            dtResult = objCsol.StuPunchWithCard(strCardNum, Now, c_ClassSessionBuffer, intClassMode, blBookIssue, LS, Stuinfo)
            'dtResult = objCsol.StuPunchWithCard(strCardNum, Now, c_ClassSessionBuffer, intClassMode, blBookIssue)
            'If dtResult.Rows(0).Item(c_PunchResultColumnName) = 0 Or dtResult.Rows(0).Item(c_PunchResultColumnName) = 1 Or dtResult.Rows(0).Item(c_PunchResultColumnName) = 6 Then
            If dtResult.Rows(0).Item(c_PunchResultColumnName) = 0 Or dtResult.Rows(0).Item(c_PunchResultColumnName) = 1 Then
            Else
                dtResult = GetSelectClass()
                If dtResult.Rows(0).Item(c_PunchResultColumnName) = 5 Then
                Else
                    dtResult = objCsol.StuPunchWithCardstep2(Now, c_ClassSessionBuffer, intClassMode, blBookIssue, Stuinfo, dtResult, idx)
                End If
            End If

            strStuId = dtResult.Rows(0).Item("stuID").ToString.Trim

            'CSOL.Logger.LogError(String.Format("{0}:Card-{1},ID-{2};BookIssue:{3}", "PunchWithCard", strCardNum, strStuId, chkboxBook.Checked))



            Dim intresult As Integer = CInt(dtResult.Rows(0).Item(c_PunchResultColumnName))

            ClearMsg()
            Select Case intresult
                Case c_PunchResultNoStu
                    strLblName = My.Resources.msgNoStuRecFound
                    MakeScreen()
                Case c_PunchResultNoClass
                    strLblName = dtResult.Rows(0).Item(c_StuNameColumnName) & ":" & My.Resources.msgPunchNotInSession
                    MakeScreen()
                Case c_PunchResultRepeat
                    strLblName = dtResult.Rows(0).Item(c_StuNameColumnName) & ":" & My.Resources.msgStuPunched & vbNewLine
                    CheckBook()
                    MakeScreen()
                Case c_PunchResultSuccess
                    strStuName = dtResult.Rows(0).Item(c_StuNameColumnName)
                    strClassName = dtResult.Rows(0).Item(c_ClassNameColumnName)
                    strSubclassName = dtResult.Rows(0).Item(c_SubClassNameColumnName)
                    strSeat = dtResult.Rows(0).Item(c_SeatNumColumnName)
                    strLblName = My.Resources.stuName & ":" & strStuName & vbNewLine & My.Resources.className & ":" & strClassName & vbNewLine & _
                                My.Resources.subClassName & ":" & strSubclassName & vbNewLine & My.Resources.seatNum & ":" & strSeat

                    strContent = dtResult.Rows(0).Item(c_ContentColumnName)
                    strSession = GetTimeString(dtResult.Rows(0).Item(c_SessionStartColumnName)) & "-" & GetTimeString(dtResult.Rows(0).Item(c_SessionEndColumnName))
                    strLblClass = vbNewLine & My.Resources.content & ":" & strContent & vbNewLine & My.Resources.session & ":" & strSession & vbNewLine & _
                                    My.Resources.punchTime & ":" & strPunchTime & vbNewLine

                    'If blBookIssue Then
                    '    strBooks = dtResult.Rows(0).Item(c_BooksColumnName)
                    '    strBookResults = dtResult.Rows(0).Item(c_BookResultColumnName)
                    '    Dim lstBooks As Array = strBooks.Split("^")
                    '    Dim lstBookResults As Array = strBookResults.Split("^")
                    '    For i As Integer = 0 To lstBooks.Length - 1
                    '        If i = 0 Then
                    '            strBooks = lstBooks(i) & "(" & GetBookMsg(CInt(lstBookResults(i))) & ")"
                    '        Else
                    '            strBooks = strBooks & ";" & lstBooks(i) & "(" & GetBookMsg(CInt(lstBookResults(i))) & ")"
                    '        End If
                    '    Next

                    '    strLblBook = My.Resources.booksIssued & strBooks
                    'End If
                    '100223
                    CheckBook()

                    strNote = dtResult.Rows(0).Item(c_NoteColumnName)
                    If strNote.Length > 0 Then
                        strLblNote = My.Resources.stuNote & strNote
                    End If

                    MakeScreen()

                    strStuId = dtResult.Rows(0).Item(c_StuIDColumnName)
                    If strCardNum.Length = 10 Then
                        Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                        If CheckFileExist(f) Then
                            Dim bm As New Bitmap(f)
                            picbox.Image = bm
                        Else
                            picbox.Image = Nothing
                        End If
                    End If

                Case c_PunchResultNoCard
                    strLblName = My.Resources.msgNoCard
                    MakeScreen()

                Case 5
                    strLblName = "請重新刷卡!!"
                    MakeScreen()

                Case 6
                    strLblName = "請確實設定上課內容!!" & vbNewLine & "未填入上課內容班級：" & dtResult.Rows(0).Item(c_ClassNameColumnName) & vbNewLine & "未填入上課內容班別：" & dtResult.Rows(0).Item(c_SubClassNameColumnName)
                    MakeScreen()
            End Select
            'CSOL.Logger.LogError(String.Format("{0}:Card-{1},ID-{2},Result-{3}", "PunchWithCard.Success", strCardNum, strStuId, intresult))
            PrepareStart()

        Catch ex As Exception
            lblMsg.Text = strCardNum & ":" & My.Resources.msgPunchFailed
            CSOL.Logger.LogError(ex.ToString())
            PrepareStart()
        End Try

    End Sub

    Private Function GetSelectClass()

        If LS.Count > 1 Then
            Dim List As New ArrayList
            For i As Integer = 0 To LS.Count - 1
                List.AddRange(stuInfo.Rows(LS.Item(i)).Item("subclassname").ToString().Split(","))
            Next
            Dim dr As New ArrayList

            Dim frm As New frmSelectClass(List)
            If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
                dr = frm.lstClass
            Else
                dtResult.Rows(0).Item(c_PunchResultColumnName) = 5

                Return dtResult
            End If
            If dr.Count = 0 Then
                MsgBox("請選擇一個班別!!")

                dtResult.Rows(0).Item(c_PunchResultColumnName) = 5
                Return dtResult
            End If

            For i As Integer = 0 To List.Count - 1
                If dr.Item(0).text.ToString = List.Item(i).ToString Then
                    idx = LS.Item(i)
                    Exit For
                End If
            Next
        Else
            idx = LS.Item(0).ToString
        End If
        Return dtResult
    End Function

    Private Function GetBookMsg(ByVal i As Integer) As String
        Dim result As String = ""
        Select Case i
            Case 0
                result = My.Resources.bookIssueOk
            Case 1
                result = My.Resources.msgBookIssue1
            Case 2
                result = My.Resources.msgBookIssue2
            Case 3
                result = My.Resources.msgBookIssue3
            Case 4
                result = My.Resources.msgBookIssue4
            Case 5
                result = My.Resources.msgBookIssue5
        End Select

        Return result
    End Function

    Private Sub tboxID_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxID.KeyDown
        If e.KeyCode = Keys.Return Then
            If tboxID.Text.Trim.Length = 8 Then
                PunchWithID()
            ElseIf tboxID.Text.Trim.Length = 10 Then
                PunchWithCard()
                'ElseIf cboxContent.SelectedIndex = -1 Then
                '    MsgBox("請確實選取上課內容 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Else
                MsgBox("請輸入正確卡號或學號 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
            tboxID.Text = ""
            tboxID.Clear()
        End If

    End Sub



    Private Sub PunchWithID()
        Try

            strStuId = tboxID.Text.Trim

            'CSOL.Logger.LogError(String.Format("{0}:{1};BookIssue:{2}", "PunchWithID", strStuId, chkboxBook.Checked))
            strPunchTime = GetTimeString(Now)
            If radbutBeginClass.Checked Then
                intClassMode = c_ClassIn
            ElseIf radbutEndClass.Checked Then
                intClassMode = c_ClassOut
            End If

            If chkboxBook.Checked Then
                blBookIssue = True                                     '100222
            Else
                blBookIssue = False
            End If

            'dtResult = objCsol.StuPunchWithID(strStuId, Now, c_ClassSessionBuffer, intClassMode, blBookIssue)
            dtResult = objCsol.StuPunchWithID(strStuId, Now, c_ClassSessionBuffer, intClassMode, blBookIssue, LS, Stuinfo)


            'If dtResult.Rows(0).Item(c_PunchResultColumnName) = 0 Or dtResult.Rows(0).Item(c_PunchResultColu'mnName) = 1 Or dtResult.Rows(0).Item(c_PunchResultColumnName) = 6 Then

            If dtResult.Rows(0).Item(c_PunchResultColumnName) = 0 Or dtResult.Rows(0).Item(c_PunchResultColumnName) = 1 Then
            Else
                dtResult = GetSelectClass()
                If dtResult.Rows(0).Item(c_PunchResultColumnName) = 5 Then

                Else
                    dtResult = objCsol.StuPunchWithIDstep2(strStuId, Now, c_ClassSessionBuffer, intClassMode, blBookIssue, dtResult, Stuinfo, idx)
                End If
            End If

            Dim intresult As Integer = CInt(dtResult.Rows(0).Item(c_PunchResultColumnName))
            ClearMsg()


            Select Case intresult
                Case c_PunchResultNoStu
                    strLblName = My.Resources.msgNoStuRecFound
                    MakeScreen()
                Case c_PunchResultNoClass
                    strLblName = dtResult.Rows(0).Item(c_StuNameColumnName) & ":" & My.Resources.msgPunchNotInSession
                    MakeScreen()
                Case c_PunchResultRepeat
                    strLblName = dtResult.Rows(0).Item(c_StuNameColumnName) & ":" & My.Resources.msgStuPunched & vbNewLine
                    CheckBook()
                    MakeScreen()
                Case c_PunchResultSuccess
                    strStuName = dtResult.Rows(0).Item(c_StuNameColumnName)
                    strClassName = dtResult.Rows(0).Item(c_ClassNameColumnName)
                    strSubclassName = dtResult.Rows(0).Item(c_SubClassNameColumnName)
                    strSeat = dtResult.Rows(0).Item(c_SeatNumColumnName)
                    strLblName = My.Resources.stuName & ":" & strStuName & vbNewLine & My.Resources.className & ":" & strClassName & vbNewLine & _
                                My.Resources.subClassName & ":" & strSubclassName & vbNewLine & My.Resources.seatNum & ":" & strSeat

                    strContent = dtResult.Rows(0).Item(c_ContentColumnName)
                    strSession = GetTimeString(dtResult.Rows(0).Item(c_SessionStartColumnName)) & "-" & GetTimeString(dtResult.Rows(0).Item(c_SessionEndColumnName))
                    strLblClass = vbNewLine & My.Resources.content & ":" & strContent & vbNewLine & My.Resources.session & ":" & strSession & vbNewLine & _
                                    My.Resources.punchTime & ":" & strPunchTime & vbNewLine

                    'If blBookIssue Then
                    '    strBooks = dtResult.Rows(0).Item(c_BooksColumnName)
                    '    strBookResults = dtResult.Rows(0).Item(c_BookResultColumnName)
                    '    Dim lstBooks As Array = strBooks.Split("^")
                    '    Dim lstBookResults As Array = strBookResults.Split("^")
                    '    For i As Integer = 0 To lstBooks.Length - 1
                    '        If i = 0 Then
                    '            strBooks = lstBooks(i) & "(" & GetBookMsg(CInt(lstBookResults(i))) & ")"
                    '        Else
                    '            strBooks = strBooks & ";" & lstBooks(i) & "(" & GetBookMsg(CInt(lstBookResults(i))) & ")"
                    '        End If
                    '    Next

                    '    strLblBook = My.Resources.booksIssued & strBooks
                    'End If
                    CheckBook()
                    '100223

                    strNote = dtResult.Rows(0).Item(c_NoteColumnName)
                    If strNote.Length > 0 Then
                        strLblNote = My.Resources.stuNote & strNote
                    End If
                    MakeScreen()
                    If strStuId.Length = 8 Then
                        Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                        If CheckFileExist(f) Then
                            Dim bm As New Bitmap(f)
                            picbox.Image = bm
                        Else
                            picbox.Image = Nothing
                        End If
                    End If
                Case c_PunchResultNoCard
                    strLblName = My.Resources.msgNoCard
                    MakeScreen()
                Case 5
                    strLblName = "請重新刷卡!!"
                    MakeScreen()
                Case 6
                    strLblName = "請確實設定上課內容!!" & vbNewLine & "未填入上課內容班級：" & dtResult.Rows(0).Item(c_ClassNameColumnName) & vbNewLine & "未填入上課內容班別：" & dtResult.Rows(0).Item(c_SubClassNameColumnName)
                    MakeScreen()
            End Select
           
            'CSOL.Logger.LogError(String.Format("{0}:ID-{1},Result-{2}", "PunchWithID.Success", strStuId, intresult))
            PrepareStart()

        Catch ex As Exception
            lblMsg.Text = strStuId & ":" & My.Resources.msgPunchFailed
            CSOL.Logger.LogError(ex.ToString())
            PrepareStart()

        End Try

    End Sub

    Private Sub CheckBook()

        If chkboxBook.Checked = True Then                                                    '100223
            intClass = lstClass(cboxClass.SelectedIndex)
            '20100501 by sherry for speed
            dtPay = objCsol.ListPayRecByStuId(strStuId)
            Dim i As Integer = CheckIssue()
            Dim arr As New ArrayList
            If i = -1 Then
                For k As Integer = 0 To lstBook.Count - 1
                    i = objCsol.AddBookIssueRec2(strStuId, intClass, lstBook(k), Now, lstMulti)
                    arr.Add(i)
                Next
            Else
                arr.Add(i)
            End If
            'GetBookMsg(i)

            Dim strBookList As String = ""
            Dim punchbooklist As New List(Of String)
            For j As Integer = 0 To lstBook.Count - 1
                'strBookList = bookList(j) & "." & GetBookMsg(arr(j)) & "、" & strBookList
                punchbooklist.Add(bookList(j) & "." & GetBookMsg(arr(j)))
            Next
            If punchbooklist.Count > 0 Then
                strBookList = String.Join("、" & vbCrLf, punchbooklist.ToArray)
            End If


            If Not strBookList = "" Then
                'strBookList = strBookList.Remove(strBookList.Length - 1, 1)
                strLblBook = My.Resources.booksIssued & strBookList & vbNewLine
            End If

        End If
    End Sub

    Private Sub timerUsb_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerUsb.Tick
        If intWaitUsb < 1 Then
            intWaitUsb = intWaitUsb + 1
        Else
            timerUsb.Stop()
            intWaitUsb = 0
            If tboxID.Text.Length > 7 Then
                PunchWithCard()
            End If
        End If

    End Sub

    Private Sub radbutBeginClass_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutBeginClass.CheckedChanged
        chkboxBook.Checked = False
        tboxID.Text = ""
        tboxID.Focus()
    End Sub

    Private Sub radbutEndClass_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutEndClass.CheckedChanged
        chkboxBook.Checked = False
        tboxID.Text = ""
        tboxID.Focus()
    End Sub

    Private Sub chkboxBook_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxBook.CheckedChanged
        tboxID.Text = ""
        tboxID.Focus()
        If chkboxBook.Checked = True Then
            cboxClass.Visible = True
            cboxSubClass.Visible = True
            chklstBook.Visible = True
            'If refreshFlag Then
            RefreshData()
            refreshFlag = False
            'End If
        Else
            cboxClass.Visible = False
            cboxSubClass.Visible = False
            chklstBook.Visible = False
        End If
    End Sub


    Friend Sub RefreshData()
        dtClass = frmMain.GetClassPunchClass
        dtSubClass = frmMain.GetClassPunchSubClass
        dtSession = frmMain.GetClassPunchSession

        dtContent = frmMain.GetContents
        'dtBook = frmMain.GetBooks
        cboxClass.Items.Clear()
        lstClass.Clear()
        For index As Integer = 0 To dtClass.Rows.Count - 1
            If CDate(dtClass.Rows(index).Item(c_EndColumnName)) >= Now.Date And lstClass.IndexOf(dtClass.Rows(index).Item(c_IDColumnName)) = -1 Then
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            End If
        Next
        cboxClass.SelectedIndex = -1
    End Sub
    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()

    

        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1

        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        lstContens.Clear()
        If cboxSubClass.SelectedIndex > -1 Then

            'Dim j As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            'For index As Integer = 0 To dtContent.Rows.Count - 1
            '    If IsDBNull(dtContent.Rows(index).Item(c_SubClassIDColumnName)) Then
            '        Continue For
            '    ElseIf dtContent.Rows(index).Item(c_SubClassIDColumnName) = j Then
            '        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName).trim)
            '        lstContens.Add(dtContent.Rows(index).Item(c_IDColumnName))
            '    End If

            'Next




            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            lstBook.Clear()
            lstBookShown.Clear()
            lstMulti.Clear()
            lstMultiShown.Clear()
            chklstBook.Items.Clear()

            For index As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(index).Item(c_SubClassIDColumnName) = i Then
                    If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                        chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).trim)
                        lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                        lstMulti.Add(dtBook.Rows(index).Item(c_MultiClassColumnName))

                    End If
                End If
            Next

            dtStu = objCsol.ListStuBySubClass(i)
        End If

    End Sub

    'Private Sub cboxContens_SelectedIndexChange(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxContent.SelectedIndexChanged
    '    If cboxContent.SelectedIndex > -1 Then
    '        Dim i As Integer = lstContens(cboxContent.SelectedIndex)
    '        For index As Integer = 0 To dtSubClass.Rows.Count - 1
    '            If dtSubClass.Rows(index).Item(c_SubClassIDColumnName) = i Then
    '                cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName).trim)
    '                'lstContens.Add()
    '            End If

    '        Next

    '    ElseIf cboxContent.SelectedIndex = -1 Then
    '        MsgBox("請確實選取上課內容!!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    '        Exit Sub
    '    Else

    '    End If
    'End Sub


    Private Function CheckIssue() As Integer
        Dim r As Integer = -1
        Dim book As Integer
        Dim FeeClear As Byte
        Dim FeeClearDate As Date
        Dim MultiClass As Byte
        Dim CreateDate As Date
        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        Dim c2 As String = c_StuIDColumnName & "='"
        Dim c3 As String = c_SubClassIDColumnName & "="
        Dim c4 As String = c_DateTimeColumnName & "> '"
        If cboxSubClass.SelectedIndex > -1 And cboxClass.SelectedIndex > -1 Then                    '100222
            intSubClass = lstSubClass(cboxSubClass.SelectedIndex)
        End If

        If dtStu.Rows.Count = 0 Then
            r = 5
            Return r
        End If

        bookList.Clear()

        For index As Integer = 0 To lstBook.Count - 1
            book = lstBook(index)

            Dim idx As Integer = -1
            For j As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(j).Item(c_IDColumnName) = book Then
                    idx = j
                    Exit For
                End If
            Next
            For k As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(k).Item(c_IDColumnName) = book And bookList.IndexOf(dtBook.Rows(k).Item(c_NameColumnName)) = -1 Then
                    bookList.Add(dtBook.Rows(k).Item(c_NameColumnName))
                End If
            Next
            If idx > -1 Then
                FeeClear = dtBook.Rows(idx).Item(c_FeeClearColumnName)
                FeeClearDate = dtBook.Rows(idx).Item(c_FeeClearDateColumnName)
                MultiClass = dtBook.Rows(idx).Item(c_MultiClassColumnName)
                CreateDate = dtBook.Rows(idx).Item(c_CreateDateColumnName)
            Else
                r = 5
                Exit For
            End If
            If CreateDate.Year <> 1911 Then
                If dtStu.Rows(0).Item(c_RegDateColumnName) > CreateDate Then
                    r = 1
                    Exit For
                End If
            End If

            If FeeClear = 0 Then
                If FeeClearDate.Year <> 1911 Then
                    If dtPay.Compute(c1, c3 & intSubClass.ToString & " AND " & c4 & _
                                        FeeClearDate.ToString & "'") > 0 Then
                        r = 3
                        Exit For
                    End If
                End If
                Dim rows() As DataRow = dtStu.Select(String.Format("ID = {0}", strStuId))

                If rows(0).Item(c_FeeOweColumnName) > 0 Then
                    r = 2
                    Exit For
                ElseIf rows(0).Item(c_FeeOweColumnName) = 0 Then
                    r = 0
                    Exit For
                End If

                'If dtStu.Rows(0).Item(c_FeeOweColumnName) > 0 Then
                '    r = 2
                '    Exit For
                'End If
            End If
        Next
        Return r
    End Function

    Private Sub ShowMsg(ByVal i As Integer)
        Select Case i
            Case 0
                strLblBook = My.Resources.bookIssueOk
            Case 1
                strLblBook = My.Resources.msgBookIssue1
            Case 2
                strLblBook = My.Resources.msgBookIssue2
            Case 3
                strLblBook = My.Resources.msgBookIssue3
            Case 4
                strLblBook = My.Resources.msgBookIssue4
            Case 5
                strLblBook = My.Resources.msgBookIssue5
        End Select
    End Sub
    Private Sub chklstBook_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstBook.ItemCheck
        Dim intC As Integer
        'Dim intM As Integer
        Dim i As Integer
        intC = lstBookShown(e.Index)
        'intM = lstMultiShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstBook.Add(intC)
            'lstMulti.Add(intM)
        Else
            i = lstBook.IndexOf(intC)
            If i > -1 Then
                lstBook.RemoveAt(i)
                lstMulti.RemoveAt(i)
            End If
        End If
    End Sub

    Private Sub timerClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerClock.Tick
        lblTime.Text = GetTimeNumNow()
    End Sub


    Private Sub lblTime_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblTime.Click

    End Sub
    Private Sub picbox_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles picbox.Click

    End Sub
    Private Sub tboxScreen_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxScreen.TextChanged

    End Sub
    Private Sub ComboBox1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub
    Private Sub lblMsg_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblMsg.Click

    End Sub
    Private Sub Label9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label9.Click

    End Sub
    Private Sub tboxID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged

    End Sub
    Private Sub chklstBook_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chklstBook.SelectedIndexChanged

    End Sub
End Class