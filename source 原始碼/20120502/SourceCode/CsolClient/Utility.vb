﻿Namespace CSOL
    Public Class Utility
        Public Shared Sub DataTableToCSV(ByVal dt As DataTable, ByVal filename As String)
            DataTableToCSV(dt, filename, System.Text.Encoding.Default)
        End Sub

        Public Shared Sub DataTableToCSV(ByVal dt As DataTable, ByVal filename As String, ByVal encoding As System.Text.Encoding)
            Dim alrows As New ArrayList()
            Dim alcells As New ArrayList()
            For Each column As DataColumn In dt.Columns
                If column.ColumnName.IndexOf(",") = -1 Then
                    alcells.Add(column.ColumnName)
                Else
                    alcells.Add("""" & column.ColumnName & """")
                End If
            Next
            alrows.Add(String.Join(",", alcells.ToArray(GetType(String))))

            For Each row As DataRow In dt.Rows
                alcells.Clear()
                For i As Integer = 0 To dt.Columns.Count - 1

                    'For Each item As String In row.ItemArray
                    If row.Item(i) Is DBNull.Value Then
                        alcells.Add("")
                    Else
                        Dim item As String = row.Item(i)
                        If item.IndexOf(",") = -1 Then
                            alcells.Add(item)
                        Else
                            alcells.Add("""" & item & """")
                        End If
                    End If
                Next
                alrows.Add(String.Join(",", alcells.ToArray(GetType(String))))
            Next

                System.IO.File.WriteAllLines(filename, alrows.ToArray(GetType(String)), encoding)
        End Sub
    End Class
End Namespace
