﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddJournalRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2AddJournalRec))
        Me.cboxGrp = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.tboxItem = New System.Windows.Forms.TextBox
        Me.tboxAmount = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxNum = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxCheque = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxRemark3 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxRemark4 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxRemark5 = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboxGrp
        '
        Me.cboxGrp.FormattingEnabled = True
        resources.ApplyResources(Me.cboxGrp, "cboxGrp")
        Me.cboxGrp.Name = "cboxGrp"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'tboxItem
        '
        resources.ApplyResources(Me.tboxItem, "tboxItem")
        Me.tboxItem.Name = "tboxItem"
        '
        'tboxAmount
        '
        resources.ApplyResources(Me.tboxAmount, "tboxAmount")
        Me.tboxAmount.Name = "tboxAmount"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'tboxNum
        '
        resources.ApplyResources(Me.tboxNum, "tboxNum")
        Me.tboxNum.Name = "tboxNum"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxCheque
        '
        resources.ApplyResources(Me.tboxCheque, "tboxCheque")
        Me.tboxCheque.Name = "tboxCheque"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tboxRemark3
        '
        resources.ApplyResources(Me.tboxRemark3, "tboxRemark3")
        Me.tboxRemark3.Name = "tboxRemark3"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxRemark4
        '
        resources.ApplyResources(Me.tboxRemark4, "tboxRemark4")
        Me.tboxRemark4.Name = "tboxRemark4"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tboxRemark5
        '
        resources.ApplyResources(Me.tboxRemark5, "tboxRemark5")
        Me.tboxRemark5.Name = "tboxRemark5"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'frm2AddJournalRec
        '
        Me.AcceptButton = Me.butSave
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxRemark5)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tboxRemark4)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tboxRemark3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxCheque)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tboxNum)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tboxAmount)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxItem)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboxGrp)
        Me.Controls.Add(Me.Label11)
        Me.Name = "frm2AddJournalRec"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboxGrp As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tboxItem As System.Windows.Forms.TextBox
    Friend WithEvents tboxAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxNum As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxCheque As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxRemark4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxRemark5 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
End Class
