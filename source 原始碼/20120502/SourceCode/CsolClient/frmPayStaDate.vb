﻿Public Class frmPayStaDate
    Private dateNow As Date = Now
    Private dsPaySta As New DataSet
    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private dtPaySta As New DataTable
    Private dtKBSta As New DataTable
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPayStaDate
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPayStaDate
        SetDate()
        RefreshData()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Friend Sub SetDate()
        dtpickFrom.Value = dateNow
        dtpickTo.Value = dateNow
        lblThisMonth.Text = (Now.Year - 1911).ToString & My.Resources.year _
                            & Now.Month.ToString & My.Resources.month
    End Sub

    Private Sub frmPayStaDate_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPayStaDate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim intTotal5 As Integer
        Dim intTotal6 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_DateTimeColumnName + ")"
        Dim strCompute4 As String
        Dim strCompute5 As String
        Dim strCompute6 As String
        Dim strCompute7 As String
        Dim strCompute8 As String
        Dim strDate As String

        Try
            If radbutByDate.Checked = True Then
                dsPaySta = objCsol.ListDatePaySta(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            Else
                dsPaySta = objCsol.ListDatePaySta(New Date(Now.Year, Now.Month, 1), _
                    New Date(Now.Year, Now.Month, dhDaysInMonth(Now) - 1))
            End If

            dtPaySta = dsPaySta.Tables.Item(c_PayStaByDateTableName)
            dtKBSta = dsPaySta.Tables.Item(c_MKeepStaByDateTableName)

            dtMaster = New DataTable
            dtDetails = New DataTable
            dtMaster = dtPaySta.DefaultView.ToTable(True, c_DateTimeColumnName)
            dtDetails = dtKBSta.DefaultView.ToTable(True, c_DateTimeColumnName)
            dtMaster.Merge(dtDetails)
            dtMaster = dtMaster.DefaultView.ToTable(True, c_DateTimeColumnName)

            InitTable()

            For index As Integer = 0 To dtMaster.Rows.Count - 1
                strDate = dtMaster.Rows(index).Item(c_DateTimeColumnName)
                strCompute2 = c_DateTimeColumnName + "='" + strDate + "'"
                strCompute4 = strCompute2 & " And " & c_TypeColumnName + "=1"
                strCompute5 = strCompute2 & " And " & c_TypeColumnName + "=2"

                If dtPaySta.Rows.Count > 0 Then
                    intTotal = dtPaySta.Compute(strCompute3, strCompute2)
                    If intTotal > 0 Then
                        intTotal2 = dtPaySta.Compute(strCompute1, strCompute2)
                    Else
                        intTotal2 = 0
                    End If
                Else
                    intTotal = 0
                    intTotal2 = 0
                End If
                dtMaster.Rows(index).Item(c_PayCntColumnName) = intTotal
                dtMaster.Rows(index).Item(c_TotalPayColumnName) = intTotal2
                If dtKBSta.Rows.Count > 0 Then
                    intTotal3 = dtKBSta.Compute(strCompute3, strCompute2)
                    If dtKBSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal4 = dtKBSta.Compute(strCompute1, strCompute2)
                    Else
                        intTotal4 = 0
                    End If
                    If dtKBSta.Compute(strCompute3, strCompute4) > 0 Then
                        intTotal5 = dtKBSta.Compute(strCompute1, strCompute4)
                    Else
                        intTotal5 = 0
                    End If
                    If dtKBSta.Compute(strCompute3, strCompute5) > 0 Then
                        intTotal6 = dtKBSta.Compute(strCompute1, strCompute5)
                    Else
                        intTotal6 = 0
                    End If
                Else
                    intTotal3 = 0
                    intTotal4 = 0
                    intTotal5 = 0
                    intTotal6 = 0
                End If
                    dtMaster.Rows(index).Item(c_KBCntColumnName) = intTotal3
                    dtMaster.Rows(index).Item(c_KBAmountColumnName) = intTotal4
                    dtMaster.Rows(index).Item(c_TotalBackColumnName) = intTotal5
                    dtMaster.Rows(index).Item(c_TotalKeepColumnName) = intTotal6
                    dtMaster.Rows(index).Item(c_DataCntColumnName) = intTotal + intTotal3
                    dtMaster.Rows(index).Item(c_AmountColumnName) = intTotal2 - intTotal5
            Next

            tbox0.Text = "0"
            tbox1.Text = "0"
            tbox2.Text = "0"
            tbox3.Text = "0"
            tbox4.Text = "0"
            tbox5.Text = "0"

            strCompute1 = "SUM(" + c_PayCntColumnName + ")"
            strCompute2 = "SUM(" + c_TotalPayColumnName + ")"
            strCompute3 = "SUM(" + c_KBCntColumnName + ")"
            strCompute4 = "SUM(" + c_KBAmountColumnName + ")"
            strCompute5 = "SUM(" + c_TotalBackColumnName + ")"
            strCompute6 = "SUM(" + c_TotalKeepColumnName + ")"
            strCompute7 = "SUM(" + c_DataCntColumnName + ")"
            strCompute8 = "SUM(" + c_AmountColumnName + ")"

            If dtMaster.Rows.Count > 0 Then
                dtMaster.Rows.Add(My.Resources.totalAmt, dtMaster.Compute(strCompute1, ""), _
                                    dtMaster.Compute(strCompute2, ""), dtMaster.Compute(strCompute3, ""), _
                                    dtMaster.Compute(strCompute4, ""), dtMaster.Compute(strCompute5, ""), _
                                    dtMaster.Compute(strCompute6, ""), dtMaster.Compute(strCompute7, ""), _
                                    dtMaster.Compute(strCompute8, ""))
            Else
                dtMaster.Rows.Add(My.Resources.totalAmt, 0, 0, 0, 0, 0, 0, 0, 0)
            End If

            'dgvMaster.DataSource = dtMaster
            'If dgvMaster.RowCount > 0 Then
            '    dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells(c_DateTimeColumnName)
            'End If

            'RefreshPayMethodSta(dtPaySta, c_AmountColumnName)

            'tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_AmountColumnName).Value).ToString

            'tbox0.Text = Format(CInt(tbox0.Text), "$#,##0")
            'tbox1.Text = Format(CInt(tbox1.Text), "$#,##0")
            'tbox2.Text = Format(CInt(tbox2.Text), "$#,##0")
            'tbox3.Text = Format(CInt(tbox3.Text), "$#,##0")
            'tbox4.Text = Format(CInt(tbox4.Text), "$#,##0")
            'tbox5.Text = Format(CInt(tbox5.Text), "$#,##0")
            'tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            'tboxTotal.Text = tbox6.Text

            'dgvMaster.Columns(c_TotalPayColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_KBAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_TotalBackColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_TotalKeepColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"

            'LoadColumnText()

           
            LoadColumnText()

            If dgvMaster.RowCount > 0 Then
                dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells("日期時間")
            End If

            RefreshPayMethodSta(dtPaySta, c_AmountColumnName)

            tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells("全部總金額").Value).ToString

            tbox0.Text = Format(CInt(tbox0.Text), "$#,##0")
            tbox1.Text = Format(CInt(tbox1.Text), "$#,##0")
            tbox2.Text = Format(CInt(tbox2.Text), "$#,##0")
            tbox3.Text = Format(CInt(tbox3.Text), "$#,##0")
            tbox4.Text = Format(CInt(tbox4.Text), "$#,##0")
            tbox5.Text = Format(CInt(tbox5.Text), "$#,##0")
            tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            tboxTotal.Text = tbox6.Text

            dgvMaster.Columns("繳費總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("退保總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("退回總金額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("保留總金額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("全部總金額").DefaultCellStyle.Format = "$#,##0"


        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshPayMethodSta(ByRef dt As DataTable, ByVal colName As String)
        Dim strCompute1 = "SUM(" + colName + ")"
        Dim strCompute2 As String
        Dim strCompute3 = "COUNT(" + colName + ")"

        If dt.Rows.Count > 0 Then
            strCompute2 = c_PayMethodColumnName + "=1"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox0.Text = (CInt(tbox0.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=2"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox1.Text = (CInt(tbox1.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=3"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox2.Text = (CInt(tbox2.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=4"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox3.Text = (CInt(tbox3.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=5"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox4.Text = (CInt(tbox4.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=6"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox5.Text = (CInt(tbox5.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
        End If
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_PayCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_KBCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_KBAmountColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalBackColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalKeepColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))

    End Sub

    Private Sub LoadColumnText()
        'For Each col In dgvMaster.Columns
        '    col.visible = False
        'Next
        'If dgvMaster.Rows.Count > 0 Then

        '    dgvMaster.Columns.Item(c_DateTimeColumnName).DisplayIndex = 0
        '    dgvMaster.Columns.Item(c_DateTimeColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvMaster.Columns.Item(c_PayCntColumnName).DisplayIndex = 1
        '    dgvMaster.Columns.Item(c_PayCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_PayCntColumnName).HeaderText = My.Resources.payCnt
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).DisplayIndex = 2
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).HeaderText = My.Resources.totalPay
        '    dgvMaster.Columns.Item(c_KBCntColumnName).DisplayIndex = 3
        '    dgvMaster.Columns.Item(c_KBCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_KBCntColumnName).HeaderText = My.Resources.kbCnt
        '    dgvMaster.Columns.Item(c_KBAmountColumnName).DisplayIndex = 4
        '    dgvMaster.Columns.Item(c_KBAmountColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_KBAmountColumnName).HeaderText = My.Resources.kbTotal
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).DisplayIndex = 5
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).HeaderText = My.Resources.totalBack
        '    dgvMaster.Columns.Item(c_TotalKeepColumnName).DisplayIndex = 6
        '    dgvMaster.Columns.Item(c_TotalKeepColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_TotalKeepColumnName).HeaderText = My.Resources.totalKeep
        '    dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 7
        '    dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.allTotalCnt
        '    dgvMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 8
        '    dgvMaster.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.allTotalAmt
        'End If

        Dim dtM As DataTable = dtMaster.Clone
        dtM.Columns("DateTime").ColumnName = "日期時間"
        dtM.Columns("PayCnt").ColumnName = "繳費筆數"
        dtM.Columns("TotalPay").ColumnName = "繳費總額"
        dtM.Columns("KBCnt").ColumnName = "退保人數"
        dtM.Columns("KBAmount").ColumnName = "退保總額"
        dtM.Columns("TotalBack").ColumnName = "退回總金額"
        dtM.Columns("TotalKeep").ColumnName = "保留總金額"
        dtM.Columns("DataCnt").ColumnName = "全部筆數"
        dtM.Columns("Amount").ColumnName = "全部總金額"
        For Each row As DataRow In dtMaster.Rows
            dtM.Rows.Add(row.ItemArray)
        Next

        dgvMaster.DataSource = dtM

    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmPayStaDate_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvMaster)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub
End Class