﻿Imports System
Imports System.IO
Imports System.Net

Public Class frm2Login
    Dim lst As New ArrayList

    'Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
    '    If Not cboxAccount.Text.Trim = "" And Not tboxPwd.Text.Trim = "" Then
    '        Dim lst As ArrayList = objCsol.UsrLogin(cboxAccount.Text, tboxPwd.Text, _
    '                                           System.Net.Dns.GetHostName)

    '        Dim r As String = ""
    '        Dim status As Integer = 0
    '        If lst.Count > 0 Then
    '            r = lst(0)
    '            status = lst(2)
    '        End If

    '        If r = "OK" Then     'success
    '            If status = 1 Then
    '                frmMain.SetUsrId(cboxAccount.Text.Trim)
    '                If lst.Count > 1 Then
    '                    frmMain.SetUsrName(lst(1))
    '                Else
    '                    frmMain.SetUsrName("")
    '                End If
    '                frmMain.SetCurrentValue(0)
    '                frmMain.SetOkState(True)
    '                AddLoginAccount(cboxAccount.Text)
    '                Me.Close()
    '            Else
    '                MsgBox(My.Resources.msgLoginFail, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    '            End If
    '        ElseIf r.Length > 2 Then 'found previous login fail rec
    '            frmMain.SetUsrId(cboxAccount.Text.Trim)
    '            If lst.Count > 1 Then
    '                frmMain.SetUsrName(lst(1))
    '            Else
    '                frmMain.SetUsrName("")
    '            End If
    '            frmMain.SetCurrentString(r)
    '            frmMain.SetCurrentValue(1)
    '            frmMain.SetOkState(True)
    '            AddLoginAccount(cboxAccount.Text)
    '            Me.Close()
    '        ElseIf r = "" Then
    '            MsgBox(My.Resources.msgLoginFail, _
    '                        MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    '        End If

    '    End If
    'End Sub
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If Not cboxAccount.Text.Trim = "" And Not tboxPwd.Text.Trim = "" Then
            Dim lst As ArrayList = objCsol.UsrLogin(cboxAccount.Text, tboxPwd.Text, _
                                               System.Net.Dns.GetHostName)

            Dim r As String = ""
            'Dim status As Integer = 0
            If lst.Count > 0 Then
                r = lst(0)
                'status = lst(2)
            End If

            If r = "OK" Then     'success
                'If status = 1 Then
                frmMain.SetUsrId(cboxAccount.Text.Trim)
                If lst.Count > 1 Then
                    frmMain.SetUsrName(lst(1))
                Else
                    frmMain.SetUsrName("")
                End If
                frmMain.SetCurrentValue(0)
                frmMain.SetOkState(True)
                AddLoginAccount(cboxAccount.Text)
                Me.Close()
                'Else
                '    MsgBox(My.Resources.msgLoginStatusFail, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                'End If
            ElseIf r.Length > 2 Then 'found previous login fail rec
                frmMain.SetUsrId(cboxAccount.Text.Trim)
                If lst.Count > 1 Then
                    frmMain.SetUsrName(lst(1))
                Else
                    frmMain.SetUsrName("")
                End If
                frmMain.SetCurrentString(r)
                frmMain.SetCurrentValue(1)
                frmMain.SetOkState(True)
                AddLoginAccount(cboxAccount.Text)
                Me.Close()
            ElseIf r = "" Then
                MsgBox(My.Resources.msgLoginFail, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                'ElseIf r = "1" Then
                '    MsgBox(My.Resources.msgLoginStatusFail, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If

        End If
    End Sub

    Private Sub AddLoginAccount(ByVal accnt As String)
        If lst.IndexOf(cboxAccount.Text) > -1 Then
            lst.Remove(cboxAccount.Text)
        End If
        lst.Add(cboxAccount.Text)
        Using sw As StreamWriter = New StreamWriter(c_LoginLogFileName)
            For index As Integer = 0 To lst.Count - 1
                sw.WriteLine(lst(index))
            Next
            sw.Close()
        End Using

    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub frm2Login_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            frmMain.SetOkState(False)
            Dim line As String
            Using sr As StreamReader = New StreamReader(c_LoginLogFileName)
                Do
                    line = sr.ReadLine()
                    If Not line = Nothing Then
                        lst.Add(line.Trim)
                    End If
                Loop Until line Is Nothing
                sr.Close()
            End Using
            Dim index As Integer = lst.Count - 1
            Do While index > -1
                line = lst(index)
                cboxAccount.Items.Add(line)
                index = index - 1
            Loop
            If cboxAccount.Items.Count > 0 Then
                cboxAccount.SelectedIndex = 0
            End If
        Catch ex As Exception
            MsgBox(ex.ToString, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End Try

    End Sub

    Private Sub linklblClear_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblClear.LinkClicked
        lst.Clear()
        cboxAccount.Items.Clear()
        cboxAccount.Text = ""
        Using sw As StreamWriter = New StreamWriter(c_LoginLogFileName)
            sw.Write("")
            sw.Close()
        End Using
    End Sub

    Private Sub linklblChangePwd_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblChangePwd.LinkClicked
        Dim frm As New frm2ModifyPwd
        frm.ShowDialog()
    End Sub
End Class
