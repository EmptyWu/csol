﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsrInfo
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsrInfo))
        Me.tabCtrl = New System.Windows.Forms.TabControl
        Me.tpgInfo = New System.Windows.Forms.TabPage
        Me.picbox = New System.Windows.Forms.PictureBox
        Me.butNext = New System.Windows.Forms.Button
        Me.butPrevious = New System.Windows.Forms.Button
        Me.lnklabImport = New System.Windows.Forms.LinkLabel
        Me.lnklabSave = New System.Windows.Forms.LinkLabel
        Me.lnklabCam = New System.Windows.Forms.LinkLabel
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tboxRemarks = New System.Windows.Forms.TextBox
        Me.butSave = New System.Windows.Forms.Button
        Me.dtPickerStart = New System.Windows.Forms.GroupBox
        Me.butUni = New System.Windows.Forms.Button
        Me.butHighSch = New System.Windows.Forms.Button
        Me.butMidSch = New System.Windows.Forms.Button
        Me.cboxUniversity = New System.Windows.Forms.ComboBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.cboxHigh = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.cboxJuniory = New System.Windows.Forms.ComboBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.cboxBloodType = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.dtpickBirth = New System.Windows.Forms.DateTimePicker
        Me.Label21 = New System.Windows.Forms.Label
        Me.cboxSex = New System.Windows.Forms.ComboBox
        Me.dtpickOnBoard = New System.Windows.Forms.DateTimePicker
        Me.tboxAccount = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.tboxStar = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.tBoxEngName = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label20 = New System.Windows.Forms.Label
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkboxIsSales = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tboxEmail = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.tboxAddr = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tboxPostal = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tboxParentMobile = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxParent = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.tboxMobile = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxIC = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxTel2 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxTel1 = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lnklabDel = New System.Windows.Forms.LinkLabel
        Me.tpgAuth = New System.Windows.Forms.TabPage
        Me.lblAuthName1 = New System.Windows.Forms.Label
        Me.lblAuthID1 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.chkboxDiscMod = New System.Windows.Forms.CheckBox
        Me.chkboxUnitPay = New System.Windows.Forms.CheckBox
        Me.chkboxClassPay = New System.Windows.Forms.CheckBox
        Me.chkboxSetDisc = New System.Windows.Forms.CheckBox
        Me.chkboxPaybyDate = New System.Windows.Forms.CheckBox
        Me.chkoxPaybyClass = New System.Windows.Forms.CheckBox
        Me.chkboxModReturn = New System.Windows.Forms.CheckBox
        Me.chkboxKpReturn = New System.Windows.Forms.CheckBox
        Me.chkboxAddOwe = New System.Windows.Forms.CheckBox
        Me.chkboxDelRpt = New System.Windows.Forms.CheckBox
        Me.chkboxModRpt = New System.Windows.Forms.CheckBox
        Me.ckhboxAddFee = New System.Windows.Forms.CheckBox
        Me.chkboxChkRec = New System.Windows.Forms.CheckBox
        Me.chkboxMkRpt = New System.Windows.Forms.CheckBox
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.butSelectNone4 = New System.Windows.Forms.Button
        Me.butSelectAll4 = New System.Windows.Forms.Button
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.chkboxDelDisc = New System.Windows.Forms.CheckBox
        Me.chkboxModDisc = New System.Windows.Forms.CheckBox
        Me.chkboxAddDisc = New System.Windows.Forms.CheckBox
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.chkboxSendBook = New System.Windows.Forms.CheckBox
        Me.chkboxSendAnalysis = New System.Windows.Forms.CheckBox
        Me.chkboxSendSeat = New System.Windows.Forms.CheckBox
        Me.chkboxSendStuInfo = New System.Windows.Forms.CheckBox
        Me.chkBoxSendCome = New System.Windows.Forms.CheckBox
        Me.chkboxSendAbsent = New System.Windows.Forms.CheckBox
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.butSelectNone3 = New System.Windows.Forms.Button
        Me.butSelectAll3 = New System.Windows.Forms.Button
        Me.GroupBox15 = New System.Windows.Forms.GroupBox
        Me.chkboxMkList = New System.Windows.Forms.CheckBox
        Me.chkboxSendList = New System.Windows.Forms.CheckBox
        Me.chkboxPList = New System.Windows.Forms.CheckBox
        Me.chkboxReadList = New System.Windows.Forms.CheckBox
        Me.chkboxDelList = New System.Windows.Forms.CheckBox
        Me.chkboxModList = New System.Windows.Forms.CheckBox
        Me.chkboxAddList = New System.Windows.Forms.CheckBox
        Me.GroupBox18 = New System.Windows.Forms.GroupBox
        Me.chkboxSendRec = New System.Windows.Forms.CheckBox
        Me.chkboxPRec = New System.Windows.Forms.CheckBox
        Me.chkboxModRec = New System.Windows.Forms.CheckBox
        Me.chkboxDelRec = New System.Windows.Forms.CheckBox
        Me.chkboxReadRec = New System.Windows.Forms.CheckBox
        Me.GroupBox19 = New System.Windows.Forms.GroupBox
        Me.chkboxDelAble = New System.Windows.Forms.CheckBox
        Me.chkboxStopAcc = New System.Windows.Forms.CheckBox
        Me.chkboxModEmp = New System.Windows.Forms.CheckBox
        Me.chkboxReademp = New System.Windows.Forms.CheckBox
        Me.chkboxAddEmp = New System.Windows.Forms.CheckBox
        Me.GroupBox17 = New System.Windows.Forms.GroupBox
        Me.chkboxModWork = New System.Windows.Forms.CheckBox
        Me.chkboxAllPerf = New System.Windows.Forms.CheckBox
        Me.chkboxMyPerf = New System.Windows.Forms.CheckBox
        Me.GroupBox16 = New System.Windows.Forms.GroupBox
        Me.butSelectNone5 = New System.Windows.Forms.Button
        Me.butSelectAll5 = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkboxBook = New System.Windows.Forms.CheckBox
        Me.chkboxAnalysis = New System.Windows.Forms.CheckBox
        Me.chkboxSeat = New System.Windows.Forms.CheckBox
        Me.chkboxStuInfo = New System.Windows.Forms.CheckBox
        Me.chkBoxCome = New System.Windows.Forms.CheckBox
        Me.chkboxAbsent = New System.Windows.Forms.CheckBox
        Me.butNext2 = New System.Windows.Forms.Button
        Me.butPrevious2 = New System.Windows.Forms.Button
        Me.butSave2 = New System.Windows.Forms.Button
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.radbutManageAllClass = New System.Windows.Forms.RadioButton
        Me.chkBoxShowPast = New System.Windows.Forms.CheckBox
        Me.butModify = New System.Windows.Forms.Button
        Me.lstboxClass = New System.Windows.Forms.ListBox
        Me.butDel = New System.Windows.Forms.Button
        Me.radbutManageClasses = New System.Windows.Forms.RadioButton
        Me.butAdd = New System.Windows.Forms.Button
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.butSelectNone2 = New System.Windows.Forms.Button
        Me.butSelectAll2 = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.chkboxOwe = New System.Windows.Forms.CheckBox
        Me.chkboxPay = New System.Windows.Forms.CheckBox
        Me.chkboxPayRec = New System.Windows.Forms.CheckBox
        Me.chkboxRpt = New System.Windows.Forms.CheckBox
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.chkboxSendOwe = New System.Windows.Forms.CheckBox
        Me.chkboxSendPay = New System.Windows.Forms.CheckBox
        Me.chkboxSendPayRec = New System.Windows.Forms.CheckBox
        Me.GroupBox20 = New System.Windows.Forms.GroupBox
        Me.butSelectNone1 = New System.Windows.Forms.Button
        Me.butSelectAll1 = New System.Windows.Forms.Button
        Me.tpgAuth2 = New System.Windows.Forms.TabPage
        Me.butNext3 = New System.Windows.Forms.Button
        Me.butPrevious3 = New System.Windows.Forms.Button
        Me.butSave3 = New System.Windows.Forms.Button
        Me.GroupBox22 = New System.Windows.Forms.GroupBox
        Me.CheckBox2 = New System.Windows.Forms.CheckBox
        Me.Button8 = New System.Windows.Forms.Button
        Me.CheckBox20 = New System.Windows.Forms.CheckBox
        Me.Button9 = New System.Windows.Forms.Button
        Me.CheckBox21 = New System.Windows.Forms.CheckBox
        Me.CheckBox22 = New System.Windows.Forms.CheckBox
        Me.CheckBox23 = New System.Windows.Forms.CheckBox
        Me.CheckBox24 = New System.Windows.Forms.CheckBox
        Me.CheckBox25 = New System.Windows.Forms.CheckBox
        Me.CheckBox26 = New System.Windows.Forms.CheckBox
        Me.CheckBox27 = New System.Windows.Forms.CheckBox
        Me.CheckBox28 = New System.Windows.Forms.CheckBox
        Me.CheckBox29 = New System.Windows.Forms.CheckBox
        Me.CheckBox30 = New System.Windows.Forms.CheckBox
        Me.CheckBox31 = New System.Windows.Forms.CheckBox
        Me.CheckBox32 = New System.Windows.Forms.CheckBox
        Me.lblAuthName2 = New System.Windows.Forms.Label
        Me.lblAuthID2 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.Label29 = New System.Windows.Forms.Label
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.CheckBox7 = New System.Windows.Forms.CheckBox
        Me.CheckBox8 = New System.Windows.Forms.CheckBox
        Me.CheckBox9 = New System.Windows.Forms.CheckBox
        Me.CheckBox10 = New System.Windows.Forms.CheckBox
        Me.CheckBox11 = New System.Windows.Forms.CheckBox
        Me.CheckBox12 = New System.Windows.Forms.CheckBox
        Me.CheckBox13 = New System.Windows.Forms.CheckBox
        Me.GroupBox21 = New System.Windows.Forms.GroupBox
        Me.CheckBox33 = New System.Windows.Forms.CheckBox
        Me.CheckBox34 = New System.Windows.Forms.CheckBox
        Me.Button6 = New System.Windows.Forms.Button
        Me.CheckBox14 = New System.Windows.Forms.CheckBox
        Me.Button7 = New System.Windows.Forms.Button
        Me.CheckBox15 = New System.Windows.Forms.CheckBox
        Me.CheckBox16 = New System.Windows.Forms.CheckBox
        Me.CheckBox17 = New System.Windows.Forms.CheckBox
        Me.CheckBox18 = New System.Windows.Forms.CheckBox
        Me.CheckBox19 = New System.Windows.Forms.CheckBox
        Me.tpgClose = New System.Windows.Forms.TabPage
        Me.tabCtrl.SuspendLayout()
        Me.tpgInfo.SuspendLayout()
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.dtPickerStart.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.tpgAuth.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.GroupBox18.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        Me.GroupBox16.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox20.SuspendLayout()
        Me.tpgAuth2.SuspendLayout()
        Me.GroupBox22.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox21.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabCtrl
        '
        resources.ApplyResources(Me.tabCtrl, "tabCtrl")
        Me.tabCtrl.Controls.Add(Me.tpgInfo)
        Me.tabCtrl.Controls.Add(Me.tpgAuth)
        Me.tabCtrl.Controls.Add(Me.tpgAuth2)
        Me.tabCtrl.Controls.Add(Me.tpgClose)
        Me.tabCtrl.Name = "tabCtrl"
        Me.tabCtrl.SelectedIndex = 0
        '
        'tpgInfo
        '
        Me.tpgInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tpgInfo.Controls.Add(Me.picbox)
        Me.tpgInfo.Controls.Add(Me.butNext)
        Me.tpgInfo.Controls.Add(Me.butPrevious)
        Me.tpgInfo.Controls.Add(Me.lnklabImport)
        Me.tpgInfo.Controls.Add(Me.lnklabSave)
        Me.tpgInfo.Controls.Add(Me.lnklabCam)
        Me.tpgInfo.Controls.Add(Me.GroupBox2)
        Me.tpgInfo.Controls.Add(Me.butSave)
        Me.tpgInfo.Controls.Add(Me.dtPickerStart)
        Me.tpgInfo.Controls.Add(Me.tboxName)
        Me.tpgInfo.Controls.Add(Me.tboxID)
        Me.tpgInfo.Controls.Add(Me.Label2)
        Me.tpgInfo.Controls.Add(Me.Label1)
        Me.tpgInfo.Controls.Add(Me.chkboxIsSales)
        Me.tpgInfo.Controls.Add(Me.GroupBox1)
        Me.tpgInfo.Controls.Add(Me.lnklabDel)
        resources.ApplyResources(Me.tpgInfo, "tpgInfo")
        Me.tpgInfo.Name = "tpgInfo"
        Me.tpgInfo.UseVisualStyleBackColor = True
        '
        'picbox
        '
        resources.ApplyResources(Me.picbox, "picbox")
        Me.picbox.Name = "picbox"
        Me.picbox.TabStop = False
        '
        'butNext
        '
        resources.ApplyResources(Me.butNext, "butNext")
        Me.butNext.Name = "butNext"
        Me.butNext.UseVisualStyleBackColor = True
        '
        'butPrevious
        '
        resources.ApplyResources(Me.butPrevious, "butPrevious")
        Me.butPrevious.Name = "butPrevious"
        Me.butPrevious.UseVisualStyleBackColor = True
        '
        'lnklabImport
        '
        resources.ApplyResources(Me.lnklabImport, "lnklabImport")
        Me.lnklabImport.Name = "lnklabImport"
        Me.lnklabImport.TabStop = True
        '
        'lnklabSave
        '
        resources.ApplyResources(Me.lnklabSave, "lnklabSave")
        Me.lnklabSave.Name = "lnklabSave"
        Me.lnklabSave.TabStop = True
        '
        'lnklabCam
        '
        resources.ApplyResources(Me.lnklabCam, "lnklabCam")
        Me.lnklabCam.Name = "lnklabCam"
        Me.lnklabCam.TabStop = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tboxRemarks)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'tboxRemarks
        '
        resources.ApplyResources(Me.tboxRemarks, "tboxRemarks")
        Me.tboxRemarks.Name = "tboxRemarks"
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dtPickerStart
        '
        Me.dtPickerStart.Controls.Add(Me.butUni)
        Me.dtPickerStart.Controls.Add(Me.butHighSch)
        Me.dtPickerStart.Controls.Add(Me.butMidSch)
        Me.dtPickerStart.Controls.Add(Me.cboxUniversity)
        Me.dtPickerStart.Controls.Add(Me.Label17)
        Me.dtPickerStart.Controls.Add(Me.cboxHigh)
        Me.dtPickerStart.Controls.Add(Me.Label14)
        Me.dtPickerStart.Controls.Add(Me.cboxJuniory)
        Me.dtPickerStart.Controls.Add(Me.Label13)
        Me.dtPickerStart.Controls.Add(Me.cboxBloodType)
        Me.dtPickerStart.Controls.Add(Me.Label15)
        Me.dtPickerStart.Controls.Add(Me.dtpickBirth)
        Me.dtPickerStart.Controls.Add(Me.Label21)
        Me.dtPickerStart.Controls.Add(Me.cboxSex)
        Me.dtPickerStart.Controls.Add(Me.dtpickOnBoard)
        Me.dtPickerStart.Controls.Add(Me.tboxAccount)
        Me.dtPickerStart.Controls.Add(Me.Label12)
        Me.dtPickerStart.Controls.Add(Me.tboxStar)
        Me.dtPickerStart.Controls.Add(Me.Label16)
        Me.dtPickerStart.Controls.Add(Me.tBoxEngName)
        Me.dtPickerStart.Controls.Add(Me.Label18)
        Me.dtPickerStart.Controls.Add(Me.Label19)
        Me.dtPickerStart.Controls.Add(Me.Label20)
        resources.ApplyResources(Me.dtPickerStart, "dtPickerStart")
        Me.dtPickerStart.Name = "dtPickerStart"
        Me.dtPickerStart.TabStop = False
        '
        'butUni
        '
        resources.ApplyResources(Me.butUni, "butUni")
        Me.butUni.Name = "butUni"
        Me.butUni.UseVisualStyleBackColor = True
        '
        'butHighSch
        '
        resources.ApplyResources(Me.butHighSch, "butHighSch")
        Me.butHighSch.Name = "butHighSch"
        Me.butHighSch.UseVisualStyleBackColor = True
        '
        'butMidSch
        '
        resources.ApplyResources(Me.butMidSch, "butMidSch")
        Me.butMidSch.Name = "butMidSch"
        Me.butMidSch.UseVisualStyleBackColor = True
        '
        'cboxUniversity
        '
        Me.cboxUniversity.FormattingEnabled = True
        resources.ApplyResources(Me.cboxUniversity, "cboxUniversity")
        Me.cboxUniversity.Name = "cboxUniversity"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'cboxHigh
        '
        Me.cboxHigh.FormattingEnabled = True
        resources.ApplyResources(Me.cboxHigh, "cboxHigh")
        Me.cboxHigh.Name = "cboxHigh"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'cboxJuniory
        '
        Me.cboxJuniory.FormattingEnabled = True
        resources.ApplyResources(Me.cboxJuniory, "cboxJuniory")
        Me.cboxJuniory.Name = "cboxJuniory"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'cboxBloodType
        '
        Me.cboxBloodType.FormattingEnabled = True
        Me.cboxBloodType.Items.AddRange(New Object() {resources.GetString("cboxBloodType.Items"), resources.GetString("cboxBloodType.Items1"), resources.GetString("cboxBloodType.Items2"), resources.GetString("cboxBloodType.Items3")})
        resources.ApplyResources(Me.cboxBloodType, "cboxBloodType")
        Me.cboxBloodType.Name = "cboxBloodType"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'dtpickBirth
        '
        resources.ApplyResources(Me.dtpickBirth, "dtpickBirth")
        Me.dtpickBirth.Name = "dtpickBirth"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'cboxSex
        '
        Me.cboxSex.FormattingEnabled = True
        Me.cboxSex.Items.AddRange(New Object() {resources.GetString("cboxSex.Items"), resources.GetString("cboxSex.Items1")})
        resources.ApplyResources(Me.cboxSex, "cboxSex")
        Me.cboxSex.Name = "cboxSex"
        '
        'dtpickOnBoard
        '
        resources.ApplyResources(Me.dtpickOnBoard, "dtpickOnBoard")
        Me.dtpickOnBoard.Name = "dtpickOnBoard"
        '
        'tboxAccount
        '
        resources.ApplyResources(Me.tboxAccount, "tboxAccount")
        Me.tboxAccount.Name = "tboxAccount"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'tboxStar
        '
        resources.ApplyResources(Me.tboxStar, "tboxStar")
        Me.tboxStar.Name = "tboxStar"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'tBoxEngName
        '
        resources.ApplyResources(Me.tBoxEngName, "tBoxEngName")
        Me.tBoxEngName.Name = "tBoxEngName"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'tboxName
        '
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        '
        'tboxID
        '
        Me.tboxID.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        Me.tboxID.ReadOnly = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'chkboxIsSales
        '
        resources.ApplyResources(Me.chkboxIsSales, "chkboxIsSales")
        Me.chkboxIsSales.Checked = True
        Me.chkboxIsSales.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxIsSales.Name = "chkboxIsSales"
        Me.chkboxIsSales.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tboxEmail)
        Me.GroupBox1.Controls.Add(Me.Label11)
        Me.GroupBox1.Controls.Add(Me.tboxAddr)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.tboxPostal)
        Me.GroupBox1.Controls.Add(Me.Label10)
        Me.GroupBox1.Controls.Add(Me.tboxParentMobile)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.tboxParent)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.tboxMobile)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.tboxIC)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.tboxTel2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.tboxTel1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'tboxEmail
        '
        resources.ApplyResources(Me.tboxEmail, "tboxEmail")
        Me.tboxEmail.Name = "tboxEmail"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'tboxAddr
        '
        resources.ApplyResources(Me.tboxAddr, "tboxAddr")
        Me.tboxAddr.Name = "tboxAddr"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'tboxPostal
        '
        resources.ApplyResources(Me.tboxPostal, "tboxPostal")
        Me.tboxPostal.Name = "tboxPostal"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'tboxParentMobile
        '
        resources.ApplyResources(Me.tboxParentMobile, "tboxParentMobile")
        Me.tboxParentMobile.Name = "tboxParentMobile"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tboxParent
        '
        resources.ApplyResources(Me.tboxParent, "tboxParent")
        Me.tboxParent.Name = "tboxParent"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'tboxMobile
        '
        resources.ApplyResources(Me.tboxMobile, "tboxMobile")
        Me.tboxMobile.Name = "tboxMobile"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tboxIC
        '
        resources.ApplyResources(Me.tboxIC, "tboxIC")
        Me.tboxIC.Name = "tboxIC"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxTel2
        '
        resources.ApplyResources(Me.tboxTel2, "tboxTel2")
        Me.tboxTel2.Name = "tboxTel2"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxTel1
        '
        resources.ApplyResources(Me.tboxTel1, "tboxTel1")
        Me.tboxTel1.Name = "tboxTel1"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'lnklabDel
        '
        resources.ApplyResources(Me.lnklabDel, "lnklabDel")
        Me.lnklabDel.Name = "lnklabDel"
        Me.lnklabDel.TabStop = True
        '
        'tpgAuth
        '
        Me.tpgAuth.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tpgAuth.Controls.Add(Me.lblAuthName1)
        Me.tpgAuth.Controls.Add(Me.lblAuthID1)
        Me.tpgAuth.Controls.Add(Me.Label22)
        Me.tpgAuth.Controls.Add(Me.Label23)
        Me.tpgAuth.Controls.Add(Me.GroupBox13)
        Me.tpgAuth.Controls.Add(Me.GroupBox14)
        Me.tpgAuth.Controls.Add(Me.GroupBox12)
        Me.tpgAuth.Controls.Add(Me.GroupBox10)
        Me.tpgAuth.Controls.Add(Me.GroupBox11)
        Me.tpgAuth.Controls.Add(Me.GroupBox15)
        Me.tpgAuth.Controls.Add(Me.GroupBox18)
        Me.tpgAuth.Controls.Add(Me.GroupBox19)
        Me.tpgAuth.Controls.Add(Me.GroupBox17)
        Me.tpgAuth.Controls.Add(Me.GroupBox16)
        Me.tpgAuth.Controls.Add(Me.GroupBox3)
        Me.tpgAuth.Controls.Add(Me.butNext2)
        Me.tpgAuth.Controls.Add(Me.butPrevious2)
        Me.tpgAuth.Controls.Add(Me.butSave2)
        Me.tpgAuth.Controls.Add(Me.GroupBox4)
        Me.tpgAuth.Controls.Add(Me.GroupBox7)
        Me.tpgAuth.Controls.Add(Me.GroupBox6)
        Me.tpgAuth.Controls.Add(Me.GroupBox9)
        Me.tpgAuth.Controls.Add(Me.GroupBox20)
        resources.ApplyResources(Me.tpgAuth, "tpgAuth")
        Me.tpgAuth.Name = "tpgAuth"
        Me.tpgAuth.UseVisualStyleBackColor = True
        '
        'lblAuthName1
        '
        resources.ApplyResources(Me.lblAuthName1, "lblAuthName1")
        Me.lblAuthName1.Name = "lblAuthName1"
        '
        'lblAuthID1
        '
        resources.ApplyResources(Me.lblAuthID1, "lblAuthID1")
        Me.lblAuthID1.Name = "lblAuthID1"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.chkboxDiscMod)
        Me.GroupBox13.Controls.Add(Me.chkboxUnitPay)
        Me.GroupBox13.Controls.Add(Me.chkboxClassPay)
        Me.GroupBox13.Controls.Add(Me.chkboxSetDisc)
        Me.GroupBox13.Controls.Add(Me.chkboxPaybyDate)
        Me.GroupBox13.Controls.Add(Me.chkoxPaybyClass)
        Me.GroupBox13.Controls.Add(Me.chkboxModReturn)
        Me.GroupBox13.Controls.Add(Me.chkboxKpReturn)
        Me.GroupBox13.Controls.Add(Me.chkboxAddOwe)
        Me.GroupBox13.Controls.Add(Me.chkboxDelRpt)
        Me.GroupBox13.Controls.Add(Me.chkboxModRpt)
        Me.GroupBox13.Controls.Add(Me.ckhboxAddFee)
        Me.GroupBox13.Controls.Add(Me.chkboxChkRec)
        Me.GroupBox13.Controls.Add(Me.chkboxMkRpt)
        resources.ApplyResources(Me.GroupBox13, "GroupBox13")
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.TabStop = False
        '
        'chkboxDiscMod
        '
        resources.ApplyResources(Me.chkboxDiscMod, "chkboxDiscMod")
        Me.chkboxDiscMod.Name = "chkboxDiscMod"
        Me.chkboxDiscMod.UseVisualStyleBackColor = True
        '
        'chkboxUnitPay
        '
        resources.ApplyResources(Me.chkboxUnitPay, "chkboxUnitPay")
        Me.chkboxUnitPay.Checked = True
        Me.chkboxUnitPay.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxUnitPay.Name = "chkboxUnitPay"
        Me.chkboxUnitPay.UseVisualStyleBackColor = True
        '
        'chkboxClassPay
        '
        resources.ApplyResources(Me.chkboxClassPay, "chkboxClassPay")
        Me.chkboxClassPay.Name = "chkboxClassPay"
        Me.chkboxClassPay.UseVisualStyleBackColor = True
        '
        'chkboxSetDisc
        '
        resources.ApplyResources(Me.chkboxSetDisc, "chkboxSetDisc")
        Me.chkboxSetDisc.Checked = True
        Me.chkboxSetDisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSetDisc.Name = "chkboxSetDisc"
        Me.chkboxSetDisc.UseVisualStyleBackColor = True
        '
        'chkboxPaybyDate
        '
        resources.ApplyResources(Me.chkboxPaybyDate, "chkboxPaybyDate")
        Me.chkboxPaybyDate.Name = "chkboxPaybyDate"
        Me.chkboxPaybyDate.UseVisualStyleBackColor = True
        '
        'chkoxPaybyClass
        '
        resources.ApplyResources(Me.chkoxPaybyClass, "chkoxPaybyClass")
        Me.chkoxPaybyClass.Name = "chkoxPaybyClass"
        Me.chkoxPaybyClass.UseVisualStyleBackColor = True
        '
        'chkboxModReturn
        '
        resources.ApplyResources(Me.chkboxModReturn, "chkboxModReturn")
        Me.chkboxModReturn.Name = "chkboxModReturn"
        Me.chkboxModReturn.UseVisualStyleBackColor = True
        '
        'chkboxKpReturn
        '
        resources.ApplyResources(Me.chkboxKpReturn, "chkboxKpReturn")
        Me.chkboxKpReturn.Name = "chkboxKpReturn"
        Me.chkboxKpReturn.UseVisualStyleBackColor = True
        '
        'chkboxAddOwe
        '
        resources.ApplyResources(Me.chkboxAddOwe, "chkboxAddOwe")
        Me.chkboxAddOwe.Checked = True
        Me.chkboxAddOwe.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxAddOwe.Name = "chkboxAddOwe"
        Me.chkboxAddOwe.UseVisualStyleBackColor = True
        '
        'chkboxDelRpt
        '
        resources.ApplyResources(Me.chkboxDelRpt, "chkboxDelRpt")
        Me.chkboxDelRpt.Name = "chkboxDelRpt"
        Me.chkboxDelRpt.UseVisualStyleBackColor = True
        '
        'chkboxModRpt
        '
        resources.ApplyResources(Me.chkboxModRpt, "chkboxModRpt")
        Me.chkboxModRpt.Name = "chkboxModRpt"
        Me.chkboxModRpt.UseVisualStyleBackColor = True
        '
        'ckhboxAddFee
        '
        resources.ApplyResources(Me.ckhboxAddFee, "ckhboxAddFee")
        Me.ckhboxAddFee.Name = "ckhboxAddFee"
        Me.ckhboxAddFee.UseVisualStyleBackColor = True
        '
        'chkboxChkRec
        '
        resources.ApplyResources(Me.chkboxChkRec, "chkboxChkRec")
        Me.chkboxChkRec.Checked = True
        Me.chkboxChkRec.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxChkRec.Name = "chkboxChkRec"
        Me.chkboxChkRec.UseVisualStyleBackColor = True
        '
        'chkboxMkRpt
        '
        resources.ApplyResources(Me.chkboxMkRpt, "chkboxMkRpt")
        Me.chkboxMkRpt.Checked = True
        Me.chkboxMkRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxMkRpt.Name = "chkboxMkRpt"
        Me.chkboxMkRpt.UseVisualStyleBackColor = True
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.butSelectNone4)
        Me.GroupBox14.Controls.Add(Me.butSelectAll4)
        resources.ApplyResources(Me.GroupBox14, "GroupBox14")
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.TabStop = False
        '
        'butSelectNone4
        '
        resources.ApplyResources(Me.butSelectNone4, "butSelectNone4")
        Me.butSelectNone4.Name = "butSelectNone4"
        Me.butSelectNone4.UseVisualStyleBackColor = True
        '
        'butSelectAll4
        '
        resources.ApplyResources(Me.butSelectAll4, "butSelectAll4")
        Me.butSelectAll4.Name = "butSelectAll4"
        Me.butSelectAll4.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.chkboxDelDisc)
        Me.GroupBox12.Controls.Add(Me.chkboxModDisc)
        Me.GroupBox12.Controls.Add(Me.chkboxAddDisc)
        resources.ApplyResources(Me.GroupBox12, "GroupBox12")
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.TabStop = False
        '
        'chkboxDelDisc
        '
        resources.ApplyResources(Me.chkboxDelDisc, "chkboxDelDisc")
        Me.chkboxDelDisc.Checked = True
        Me.chkboxDelDisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxDelDisc.Name = "chkboxDelDisc"
        Me.chkboxDelDisc.UseVisualStyleBackColor = True
        '
        'chkboxModDisc
        '
        resources.ApplyResources(Me.chkboxModDisc, "chkboxModDisc")
        Me.chkboxModDisc.Checked = True
        Me.chkboxModDisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxModDisc.Name = "chkboxModDisc"
        Me.chkboxModDisc.UseVisualStyleBackColor = True
        '
        'chkboxAddDisc
        '
        resources.ApplyResources(Me.chkboxAddDisc, "chkboxAddDisc")
        Me.chkboxAddDisc.Checked = True
        Me.chkboxAddDisc.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxAddDisc.Name = "chkboxAddDisc"
        Me.chkboxAddDisc.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.chkboxSendBook)
        Me.GroupBox10.Controls.Add(Me.chkboxSendAnalysis)
        Me.GroupBox10.Controls.Add(Me.chkboxSendSeat)
        Me.GroupBox10.Controls.Add(Me.chkboxSendStuInfo)
        Me.GroupBox10.Controls.Add(Me.chkBoxSendCome)
        Me.GroupBox10.Controls.Add(Me.chkboxSendAbsent)
        resources.ApplyResources(Me.GroupBox10, "GroupBox10")
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.TabStop = False
        '
        'chkboxSendBook
        '
        resources.ApplyResources(Me.chkboxSendBook, "chkboxSendBook")
        Me.chkboxSendBook.Checked = True
        Me.chkboxSendBook.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSendBook.Name = "chkboxSendBook"
        Me.chkboxSendBook.UseVisualStyleBackColor = True
        '
        'chkboxSendAnalysis
        '
        resources.ApplyResources(Me.chkboxSendAnalysis, "chkboxSendAnalysis")
        Me.chkboxSendAnalysis.Name = "chkboxSendAnalysis"
        Me.chkboxSendAnalysis.UseVisualStyleBackColor = True
        '
        'chkboxSendSeat
        '
        resources.ApplyResources(Me.chkboxSendSeat, "chkboxSendSeat")
        Me.chkboxSendSeat.Checked = True
        Me.chkboxSendSeat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSendSeat.Name = "chkboxSendSeat"
        Me.chkboxSendSeat.UseVisualStyleBackColor = True
        '
        'chkboxSendStuInfo
        '
        resources.ApplyResources(Me.chkboxSendStuInfo, "chkboxSendStuInfo")
        Me.chkboxSendStuInfo.Checked = True
        Me.chkboxSendStuInfo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSendStuInfo.Name = "chkboxSendStuInfo"
        Me.chkboxSendStuInfo.UseVisualStyleBackColor = True
        '
        'chkBoxSendCome
        '
        resources.ApplyResources(Me.chkBoxSendCome, "chkBoxSendCome")
        Me.chkBoxSendCome.Checked = True
        Me.chkBoxSendCome.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBoxSendCome.Name = "chkBoxSendCome"
        Me.chkBoxSendCome.UseVisualStyleBackColor = True
        '
        'chkboxSendAbsent
        '
        resources.ApplyResources(Me.chkboxSendAbsent, "chkboxSendAbsent")
        Me.chkboxSendAbsent.Checked = True
        Me.chkboxSendAbsent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSendAbsent.Name = "chkboxSendAbsent"
        Me.chkboxSendAbsent.UseVisualStyleBackColor = True
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.butSelectNone3)
        Me.GroupBox11.Controls.Add(Me.butSelectAll3)
        resources.ApplyResources(Me.GroupBox11, "GroupBox11")
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.TabStop = False
        '
        'butSelectNone3
        '
        resources.ApplyResources(Me.butSelectNone3, "butSelectNone3")
        Me.butSelectNone3.Name = "butSelectNone3"
        Me.butSelectNone3.UseVisualStyleBackColor = True
        '
        'butSelectAll3
        '
        resources.ApplyResources(Me.butSelectAll3, "butSelectAll3")
        Me.butSelectAll3.Name = "butSelectAll3"
        Me.butSelectAll3.UseVisualStyleBackColor = True
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.chkboxMkList)
        Me.GroupBox15.Controls.Add(Me.chkboxSendList)
        Me.GroupBox15.Controls.Add(Me.chkboxPList)
        Me.GroupBox15.Controls.Add(Me.chkboxReadList)
        Me.GroupBox15.Controls.Add(Me.chkboxDelList)
        Me.GroupBox15.Controls.Add(Me.chkboxModList)
        Me.GroupBox15.Controls.Add(Me.chkboxAddList)
        resources.ApplyResources(Me.GroupBox15, "GroupBox15")
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.TabStop = False
        '
        'chkboxMkList
        '
        resources.ApplyResources(Me.chkboxMkList, "chkboxMkList")
        Me.chkboxMkList.Name = "chkboxMkList"
        Me.chkboxMkList.UseVisualStyleBackColor = True
        '
        'chkboxSendList
        '
        resources.ApplyResources(Me.chkboxSendList, "chkboxSendList")
        Me.chkboxSendList.Name = "chkboxSendList"
        Me.chkboxSendList.UseVisualStyleBackColor = True
        '
        'chkboxPList
        '
        resources.ApplyResources(Me.chkboxPList, "chkboxPList")
        Me.chkboxPList.Name = "chkboxPList"
        Me.chkboxPList.UseVisualStyleBackColor = True
        '
        'chkboxReadList
        '
        resources.ApplyResources(Me.chkboxReadList, "chkboxReadList")
        Me.chkboxReadList.Name = "chkboxReadList"
        Me.chkboxReadList.UseVisualStyleBackColor = True
        '
        'chkboxDelList
        '
        resources.ApplyResources(Me.chkboxDelList, "chkboxDelList")
        Me.chkboxDelList.Name = "chkboxDelList"
        Me.chkboxDelList.UseVisualStyleBackColor = True
        '
        'chkboxModList
        '
        resources.ApplyResources(Me.chkboxModList, "chkboxModList")
        Me.chkboxModList.Name = "chkboxModList"
        Me.chkboxModList.UseVisualStyleBackColor = True
        '
        'chkboxAddList
        '
        resources.ApplyResources(Me.chkboxAddList, "chkboxAddList")
        Me.chkboxAddList.Name = "chkboxAddList"
        Me.chkboxAddList.UseVisualStyleBackColor = True
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.chkboxSendRec)
        Me.GroupBox18.Controls.Add(Me.chkboxPRec)
        Me.GroupBox18.Controls.Add(Me.chkboxModRec)
        Me.GroupBox18.Controls.Add(Me.chkboxDelRec)
        Me.GroupBox18.Controls.Add(Me.chkboxReadRec)
        resources.ApplyResources(Me.GroupBox18, "GroupBox18")
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.TabStop = False
        '
        'chkboxSendRec
        '
        resources.ApplyResources(Me.chkboxSendRec, "chkboxSendRec")
        Me.chkboxSendRec.Name = "chkboxSendRec"
        Me.chkboxSendRec.UseVisualStyleBackColor = True
        '
        'chkboxPRec
        '
        resources.ApplyResources(Me.chkboxPRec, "chkboxPRec")
        Me.chkboxPRec.Name = "chkboxPRec"
        Me.chkboxPRec.UseVisualStyleBackColor = True
        '
        'chkboxModRec
        '
        resources.ApplyResources(Me.chkboxModRec, "chkboxModRec")
        Me.chkboxModRec.Name = "chkboxModRec"
        Me.chkboxModRec.UseVisualStyleBackColor = True
        '
        'chkboxDelRec
        '
        resources.ApplyResources(Me.chkboxDelRec, "chkboxDelRec")
        Me.chkboxDelRec.Name = "chkboxDelRec"
        Me.chkboxDelRec.UseVisualStyleBackColor = True
        '
        'chkboxReadRec
        '
        resources.ApplyResources(Me.chkboxReadRec, "chkboxReadRec")
        Me.chkboxReadRec.Name = "chkboxReadRec"
        Me.chkboxReadRec.UseVisualStyleBackColor = True
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.chkboxDelAble)
        Me.GroupBox19.Controls.Add(Me.chkboxStopAcc)
        Me.GroupBox19.Controls.Add(Me.chkboxModEmp)
        Me.GroupBox19.Controls.Add(Me.chkboxReademp)
        Me.GroupBox19.Controls.Add(Me.chkboxAddEmp)
        resources.ApplyResources(Me.GroupBox19, "GroupBox19")
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.TabStop = False
        '
        'chkboxDelAble
        '
        resources.ApplyResources(Me.chkboxDelAble, "chkboxDelAble")
        Me.chkboxDelAble.Name = "chkboxDelAble"
        Me.chkboxDelAble.UseVisualStyleBackColor = True
        '
        'chkboxStopAcc
        '
        resources.ApplyResources(Me.chkboxStopAcc, "chkboxStopAcc")
        Me.chkboxStopAcc.Name = "chkboxStopAcc"
        Me.chkboxStopAcc.UseVisualStyleBackColor = True
        '
        'chkboxModEmp
        '
        resources.ApplyResources(Me.chkboxModEmp, "chkboxModEmp")
        Me.chkboxModEmp.Name = "chkboxModEmp"
        Me.chkboxModEmp.UseVisualStyleBackColor = True
        '
        'chkboxReademp
        '
        resources.ApplyResources(Me.chkboxReademp, "chkboxReademp")
        Me.chkboxReademp.Name = "chkboxReademp"
        Me.chkboxReademp.UseVisualStyleBackColor = True
        '
        'chkboxAddEmp
        '
        resources.ApplyResources(Me.chkboxAddEmp, "chkboxAddEmp")
        Me.chkboxAddEmp.Name = "chkboxAddEmp"
        Me.chkboxAddEmp.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.chkboxModWork)
        Me.GroupBox17.Controls.Add(Me.chkboxAllPerf)
        Me.GroupBox17.Controls.Add(Me.chkboxMyPerf)
        resources.ApplyResources(Me.GroupBox17, "GroupBox17")
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.TabStop = False
        '
        'chkboxModWork
        '
        resources.ApplyResources(Me.chkboxModWork, "chkboxModWork")
        Me.chkboxModWork.Name = "chkboxModWork"
        Me.chkboxModWork.UseVisualStyleBackColor = True
        '
        'chkboxAllPerf
        '
        resources.ApplyResources(Me.chkboxAllPerf, "chkboxAllPerf")
        Me.chkboxAllPerf.Name = "chkboxAllPerf"
        Me.chkboxAllPerf.UseVisualStyleBackColor = True
        '
        'chkboxMyPerf
        '
        resources.ApplyResources(Me.chkboxMyPerf, "chkboxMyPerf")
        Me.chkboxMyPerf.Name = "chkboxMyPerf"
        Me.chkboxMyPerf.UseVisualStyleBackColor = True
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.butSelectNone5)
        Me.GroupBox16.Controls.Add(Me.butSelectAll5)
        resources.ApplyResources(Me.GroupBox16, "GroupBox16")
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.TabStop = False
        '
        'butSelectNone5
        '
        resources.ApplyResources(Me.butSelectNone5, "butSelectNone5")
        Me.butSelectNone5.Name = "butSelectNone5"
        Me.butSelectNone5.UseVisualStyleBackColor = True
        '
        'butSelectAll5
        '
        resources.ApplyResources(Me.butSelectAll5, "butSelectAll5")
        Me.butSelectAll5.Name = "butSelectAll5"
        Me.butSelectAll5.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkboxBook)
        Me.GroupBox3.Controls.Add(Me.chkboxAnalysis)
        Me.GroupBox3.Controls.Add(Me.chkboxSeat)
        Me.GroupBox3.Controls.Add(Me.chkboxStuInfo)
        Me.GroupBox3.Controls.Add(Me.chkBoxCome)
        Me.GroupBox3.Controls.Add(Me.chkboxAbsent)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'chkboxBook
        '
        resources.ApplyResources(Me.chkboxBook, "chkboxBook")
        Me.chkboxBook.Checked = True
        Me.chkboxBook.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxBook.Name = "chkboxBook"
        Me.chkboxBook.UseVisualStyleBackColor = True
        '
        'chkboxAnalysis
        '
        resources.ApplyResources(Me.chkboxAnalysis, "chkboxAnalysis")
        Me.chkboxAnalysis.Name = "chkboxAnalysis"
        Me.chkboxAnalysis.UseVisualStyleBackColor = True
        '
        'chkboxSeat
        '
        resources.ApplyResources(Me.chkboxSeat, "chkboxSeat")
        Me.chkboxSeat.Checked = True
        Me.chkboxSeat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSeat.Name = "chkboxSeat"
        Me.chkboxSeat.UseVisualStyleBackColor = True
        '
        'chkboxStuInfo
        '
        resources.ApplyResources(Me.chkboxStuInfo, "chkboxStuInfo")
        Me.chkboxStuInfo.Checked = True
        Me.chkboxStuInfo.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxStuInfo.Name = "chkboxStuInfo"
        Me.chkboxStuInfo.UseVisualStyleBackColor = True
        '
        'chkBoxCome
        '
        resources.ApplyResources(Me.chkBoxCome, "chkBoxCome")
        Me.chkBoxCome.Checked = True
        Me.chkBoxCome.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkBoxCome.Name = "chkBoxCome"
        Me.chkBoxCome.UseVisualStyleBackColor = True
        '
        'chkboxAbsent
        '
        resources.ApplyResources(Me.chkboxAbsent, "chkboxAbsent")
        Me.chkboxAbsent.Checked = True
        Me.chkboxAbsent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxAbsent.Name = "chkboxAbsent"
        Me.chkboxAbsent.UseVisualStyleBackColor = True
        '
        'butNext2
        '
        resources.ApplyResources(Me.butNext2, "butNext2")
        Me.butNext2.Name = "butNext2"
        Me.butNext2.UseVisualStyleBackColor = True
        '
        'butPrevious2
        '
        resources.ApplyResources(Me.butPrevious2, "butPrevious2")
        Me.butPrevious2.Name = "butPrevious2"
        Me.butPrevious2.UseVisualStyleBackColor = True
        '
        'butSave2
        '
        resources.ApplyResources(Me.butSave2, "butSave2")
        Me.butSave2.Name = "butSave2"
        Me.butSave2.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.radbutManageAllClass)
        Me.GroupBox4.Controls.Add(Me.chkBoxShowPast)
        Me.GroupBox4.Controls.Add(Me.butModify)
        Me.GroupBox4.Controls.Add(Me.lstboxClass)
        Me.GroupBox4.Controls.Add(Me.butDel)
        Me.GroupBox4.Controls.Add(Me.radbutManageClasses)
        Me.GroupBox4.Controls.Add(Me.butAdd)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'radbutManageAllClass
        '
        resources.ApplyResources(Me.radbutManageAllClass, "radbutManageAllClass")
        Me.radbutManageAllClass.Name = "radbutManageAllClass"
        Me.radbutManageAllClass.TabStop = True
        Me.radbutManageAllClass.UseVisualStyleBackColor = True
        '
        'chkBoxShowPast
        '
        resources.ApplyResources(Me.chkBoxShowPast, "chkBoxShowPast")
        Me.chkBoxShowPast.Name = "chkBoxShowPast"
        Me.chkBoxShowPast.UseVisualStyleBackColor = True
        '
        'butModify
        '
        resources.ApplyResources(Me.butModify, "butModify")
        Me.butModify.Name = "butModify"
        Me.butModify.UseVisualStyleBackColor = True
        '
        'lstboxClass
        '
        Me.lstboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.lstboxClass, "lstboxClass")
        Me.lstboxClass.Name = "lstboxClass"
        '
        'butDel
        '
        resources.ApplyResources(Me.butDel, "butDel")
        Me.butDel.Name = "butDel"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'radbutManageClasses
        '
        resources.ApplyResources(Me.radbutManageClasses, "radbutManageClasses")
        Me.radbutManageClasses.Checked = True
        Me.radbutManageClasses.Name = "radbutManageClasses"
        Me.radbutManageClasses.TabStop = True
        Me.radbutManageClasses.UseVisualStyleBackColor = True
        '
        'butAdd
        '
        resources.ApplyResources(Me.butAdd, "butAdd")
        Me.butAdd.Name = "butAdd"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.butSelectNone2)
        Me.GroupBox7.Controls.Add(Me.butSelectAll2)
        resources.ApplyResources(Me.GroupBox7, "GroupBox7")
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.TabStop = False
        '
        'butSelectNone2
        '
        resources.ApplyResources(Me.butSelectNone2, "butSelectNone2")
        Me.butSelectNone2.Name = "butSelectNone2"
        Me.butSelectNone2.UseVisualStyleBackColor = True
        '
        'butSelectAll2
        '
        resources.ApplyResources(Me.butSelectAll2, "butSelectAll2")
        Me.butSelectAll2.Name = "butSelectAll2"
        Me.butSelectAll2.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.chkboxOwe)
        Me.GroupBox6.Controls.Add(Me.chkboxPay)
        Me.GroupBox6.Controls.Add(Me.chkboxPayRec)
        Me.GroupBox6.Controls.Add(Me.chkboxRpt)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'chkboxOwe
        '
        resources.ApplyResources(Me.chkboxOwe, "chkboxOwe")
        Me.chkboxOwe.Checked = True
        Me.chkboxOwe.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxOwe.Name = "chkboxOwe"
        Me.chkboxOwe.UseVisualStyleBackColor = True
        '
        'chkboxPay
        '
        resources.ApplyResources(Me.chkboxPay, "chkboxPay")
        Me.chkboxPay.Name = "chkboxPay"
        Me.chkboxPay.UseVisualStyleBackColor = True
        '
        'chkboxPayRec
        '
        resources.ApplyResources(Me.chkboxPayRec, "chkboxPayRec")
        Me.chkboxPayRec.Name = "chkboxPayRec"
        Me.chkboxPayRec.UseVisualStyleBackColor = True
        '
        'chkboxRpt
        '
        resources.ApplyResources(Me.chkboxRpt, "chkboxRpt")
        Me.chkboxRpt.Checked = True
        Me.chkboxRpt.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxRpt.Name = "chkboxRpt"
        Me.chkboxRpt.UseVisualStyleBackColor = True
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.chkboxSendOwe)
        Me.GroupBox9.Controls.Add(Me.chkboxSendPay)
        Me.GroupBox9.Controls.Add(Me.chkboxSendPayRec)
        resources.ApplyResources(Me.GroupBox9, "GroupBox9")
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.TabStop = False
        '
        'chkboxSendOwe
        '
        resources.ApplyResources(Me.chkboxSendOwe, "chkboxSendOwe")
        Me.chkboxSendOwe.Checked = True
        Me.chkboxSendOwe.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxSendOwe.Name = "chkboxSendOwe"
        Me.chkboxSendOwe.UseVisualStyleBackColor = True
        '
        'chkboxSendPay
        '
        resources.ApplyResources(Me.chkboxSendPay, "chkboxSendPay")
        Me.chkboxSendPay.Name = "chkboxSendPay"
        Me.chkboxSendPay.UseVisualStyleBackColor = True
        '
        'chkboxSendPayRec
        '
        resources.ApplyResources(Me.chkboxSendPayRec, "chkboxSendPayRec")
        Me.chkboxSendPayRec.Name = "chkboxSendPayRec"
        Me.chkboxSendPayRec.UseVisualStyleBackColor = True
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.butSelectNone1)
        Me.GroupBox20.Controls.Add(Me.butSelectAll1)
        resources.ApplyResources(Me.GroupBox20, "GroupBox20")
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.TabStop = False
        '
        'butSelectNone1
        '
        resources.ApplyResources(Me.butSelectNone1, "butSelectNone1")
        Me.butSelectNone1.Name = "butSelectNone1"
        Me.butSelectNone1.UseVisualStyleBackColor = True
        '
        'butSelectAll1
        '
        resources.ApplyResources(Me.butSelectAll1, "butSelectAll1")
        Me.butSelectAll1.Name = "butSelectAll1"
        Me.butSelectAll1.UseVisualStyleBackColor = True
        '
        'tpgAuth2
        '
        Me.tpgAuth2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tpgAuth2.Controls.Add(Me.butNext3)
        Me.tpgAuth2.Controls.Add(Me.butPrevious3)
        Me.tpgAuth2.Controls.Add(Me.butSave3)
        Me.tpgAuth2.Controls.Add(Me.GroupBox22)
        Me.tpgAuth2.Controls.Add(Me.lblAuthName2)
        Me.tpgAuth2.Controls.Add(Me.lblAuthID2)
        Me.tpgAuth2.Controls.Add(Me.Label28)
        Me.tpgAuth2.Controls.Add(Me.Label29)
        Me.tpgAuth2.Controls.Add(Me.GroupBox8)
        Me.tpgAuth2.Controls.Add(Me.GroupBox21)
        resources.ApplyResources(Me.tpgAuth2, "tpgAuth2")
        Me.tpgAuth2.Name = "tpgAuth2"
        '
        'butNext3
        '
        resources.ApplyResources(Me.butNext3, "butNext3")
        Me.butNext3.Name = "butNext3"
        Me.butNext3.UseVisualStyleBackColor = True
        '
        'butPrevious3
        '
        resources.ApplyResources(Me.butPrevious3, "butPrevious3")
        Me.butPrevious3.Name = "butPrevious3"
        Me.butPrevious3.UseVisualStyleBackColor = True
        '
        'butSave3
        '
        resources.ApplyResources(Me.butSave3, "butSave3")
        Me.butSave3.Name = "butSave3"
        Me.butSave3.UseVisualStyleBackColor = True
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.CheckBox2)
        Me.GroupBox22.Controls.Add(Me.Button8)
        Me.GroupBox22.Controls.Add(Me.CheckBox20)
        Me.GroupBox22.Controls.Add(Me.Button9)
        Me.GroupBox22.Controls.Add(Me.CheckBox21)
        Me.GroupBox22.Controls.Add(Me.CheckBox22)
        Me.GroupBox22.Controls.Add(Me.CheckBox23)
        Me.GroupBox22.Controls.Add(Me.CheckBox24)
        Me.GroupBox22.Controls.Add(Me.CheckBox25)
        Me.GroupBox22.Controls.Add(Me.CheckBox26)
        Me.GroupBox22.Controls.Add(Me.CheckBox27)
        Me.GroupBox22.Controls.Add(Me.CheckBox28)
        Me.GroupBox22.Controls.Add(Me.CheckBox29)
        Me.GroupBox22.Controls.Add(Me.CheckBox30)
        Me.GroupBox22.Controls.Add(Me.CheckBox31)
        Me.GroupBox22.Controls.Add(Me.CheckBox32)
        resources.ApplyResources(Me.GroupBox22, "GroupBox22")
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.TabStop = False
        '
        'CheckBox2
        '
        resources.ApplyResources(Me.CheckBox2, "CheckBox2")
        Me.CheckBox2.Checked = True
        Me.CheckBox2.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox2.Name = "CheckBox2"
        Me.CheckBox2.UseVisualStyleBackColor = True
        '
        'Button8
        '
        resources.ApplyResources(Me.Button8, "Button8")
        Me.Button8.Name = "Button8"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'CheckBox20
        '
        resources.ApplyResources(Me.CheckBox20, "CheckBox20")
        Me.CheckBox20.Checked = True
        Me.CheckBox20.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox20.Name = "CheckBox20"
        Me.CheckBox20.UseVisualStyleBackColor = True
        '
        'Button9
        '
        resources.ApplyResources(Me.Button9, "Button9")
        Me.Button9.Name = "Button9"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'CheckBox21
        '
        resources.ApplyResources(Me.CheckBox21, "CheckBox21")
        Me.CheckBox21.Name = "CheckBox21"
        Me.CheckBox21.UseVisualStyleBackColor = True
        '
        'CheckBox22
        '
        resources.ApplyResources(Me.CheckBox22, "CheckBox22")
        Me.CheckBox22.Checked = True
        Me.CheckBox22.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox22.Name = "CheckBox22"
        Me.CheckBox22.UseVisualStyleBackColor = True
        '
        'CheckBox23
        '
        resources.ApplyResources(Me.CheckBox23, "CheckBox23")
        Me.CheckBox23.Name = "CheckBox23"
        Me.CheckBox23.UseVisualStyleBackColor = True
        '
        'CheckBox24
        '
        resources.ApplyResources(Me.CheckBox24, "CheckBox24")
        Me.CheckBox24.Name = "CheckBox24"
        Me.CheckBox24.UseVisualStyleBackColor = True
        '
        'CheckBox25
        '
        resources.ApplyResources(Me.CheckBox25, "CheckBox25")
        Me.CheckBox25.Name = "CheckBox25"
        Me.CheckBox25.UseVisualStyleBackColor = True
        '
        'CheckBox26
        '
        resources.ApplyResources(Me.CheckBox26, "CheckBox26")
        Me.CheckBox26.Name = "CheckBox26"
        Me.CheckBox26.UseVisualStyleBackColor = True
        '
        'CheckBox27
        '
        resources.ApplyResources(Me.CheckBox27, "CheckBox27")
        Me.CheckBox27.Checked = True
        Me.CheckBox27.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox27.Name = "CheckBox27"
        Me.CheckBox27.UseVisualStyleBackColor = True
        '
        'CheckBox28
        '
        resources.ApplyResources(Me.CheckBox28, "CheckBox28")
        Me.CheckBox28.Name = "CheckBox28"
        Me.CheckBox28.UseVisualStyleBackColor = True
        '
        'CheckBox29
        '
        resources.ApplyResources(Me.CheckBox29, "CheckBox29")
        Me.CheckBox29.Name = "CheckBox29"
        Me.CheckBox29.UseVisualStyleBackColor = True
        '
        'CheckBox30
        '
        resources.ApplyResources(Me.CheckBox30, "CheckBox30")
        Me.CheckBox30.Name = "CheckBox30"
        Me.CheckBox30.UseVisualStyleBackColor = True
        '
        'CheckBox31
        '
        resources.ApplyResources(Me.CheckBox31, "CheckBox31")
        Me.CheckBox31.Checked = True
        Me.CheckBox31.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox31.Name = "CheckBox31"
        Me.CheckBox31.UseVisualStyleBackColor = True
        '
        'CheckBox32
        '
        resources.ApplyResources(Me.CheckBox32, "CheckBox32")
        Me.CheckBox32.Checked = True
        Me.CheckBox32.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox32.Name = "CheckBox32"
        Me.CheckBox32.UseVisualStyleBackColor = True
        '
        'lblAuthName2
        '
        resources.ApplyResources(Me.lblAuthName2, "lblAuthName2")
        Me.lblAuthName2.Name = "lblAuthName2"
        '
        'lblAuthID2
        '
        resources.ApplyResources(Me.lblAuthID2, "lblAuthID2")
        Me.lblAuthID2.Name = "lblAuthID2"
        '
        'Label28
        '
        resources.ApplyResources(Me.Label28, "Label28")
        Me.Label28.Name = "Label28"
        '
        'Label29
        '
        resources.ApplyResources(Me.Label29, "Label29")
        Me.Label29.Name = "Label29"
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.Button2)
        Me.GroupBox8.Controls.Add(Me.Button3)
        Me.GroupBox8.Controls.Add(Me.CheckBox7)
        Me.GroupBox8.Controls.Add(Me.CheckBox8)
        Me.GroupBox8.Controls.Add(Me.CheckBox9)
        Me.GroupBox8.Controls.Add(Me.CheckBox10)
        Me.GroupBox8.Controls.Add(Me.CheckBox11)
        Me.GroupBox8.Controls.Add(Me.CheckBox12)
        Me.GroupBox8.Controls.Add(Me.CheckBox13)
        resources.ApplyResources(Me.GroupBox8, "GroupBox8")
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.TabStop = False
        '
        'Button2
        '
        resources.ApplyResources(Me.Button2, "Button2")
        Me.Button2.Name = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        resources.ApplyResources(Me.Button3, "Button3")
        Me.Button3.Name = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'CheckBox7
        '
        resources.ApplyResources(Me.CheckBox7, "CheckBox7")
        Me.CheckBox7.Name = "CheckBox7"
        Me.CheckBox7.UseVisualStyleBackColor = True
        '
        'CheckBox8
        '
        resources.ApplyResources(Me.CheckBox8, "CheckBox8")
        Me.CheckBox8.Checked = True
        Me.CheckBox8.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox8.Name = "CheckBox8"
        Me.CheckBox8.UseVisualStyleBackColor = True
        '
        'CheckBox9
        '
        resources.ApplyResources(Me.CheckBox9, "CheckBox9")
        Me.CheckBox9.Name = "CheckBox9"
        Me.CheckBox9.UseVisualStyleBackColor = True
        '
        'CheckBox10
        '
        resources.ApplyResources(Me.CheckBox10, "CheckBox10")
        Me.CheckBox10.Name = "CheckBox10"
        Me.CheckBox10.UseVisualStyleBackColor = True
        '
        'CheckBox11
        '
        resources.ApplyResources(Me.CheckBox11, "CheckBox11")
        Me.CheckBox11.Name = "CheckBox11"
        Me.CheckBox11.UseVisualStyleBackColor = True
        '
        'CheckBox12
        '
        resources.ApplyResources(Me.CheckBox12, "CheckBox12")
        Me.CheckBox12.Checked = True
        Me.CheckBox12.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox12.Name = "CheckBox12"
        Me.CheckBox12.UseVisualStyleBackColor = True
        '
        'CheckBox13
        '
        resources.ApplyResources(Me.CheckBox13, "CheckBox13")
        Me.CheckBox13.Checked = True
        Me.CheckBox13.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox13.Name = "CheckBox13"
        Me.CheckBox13.UseVisualStyleBackColor = True
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.CheckBox33)
        Me.GroupBox21.Controls.Add(Me.CheckBox34)
        Me.GroupBox21.Controls.Add(Me.Button6)
        Me.GroupBox21.Controls.Add(Me.CheckBox14)
        Me.GroupBox21.Controls.Add(Me.Button7)
        Me.GroupBox21.Controls.Add(Me.CheckBox15)
        Me.GroupBox21.Controls.Add(Me.CheckBox16)
        Me.GroupBox21.Controls.Add(Me.CheckBox17)
        Me.GroupBox21.Controls.Add(Me.CheckBox18)
        Me.GroupBox21.Controls.Add(Me.CheckBox19)
        resources.ApplyResources(Me.GroupBox21, "GroupBox21")
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.TabStop = False
        '
        'CheckBox33
        '
        resources.ApplyResources(Me.CheckBox33, "CheckBox33")
        Me.CheckBox33.Checked = True
        Me.CheckBox33.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox33.Name = "CheckBox33"
        Me.CheckBox33.UseVisualStyleBackColor = True
        '
        'CheckBox34
        '
        resources.ApplyResources(Me.CheckBox34, "CheckBox34")
        Me.CheckBox34.Name = "CheckBox34"
        Me.CheckBox34.UseVisualStyleBackColor = True
        '
        'Button6
        '
        resources.ApplyResources(Me.Button6, "Button6")
        Me.Button6.Name = "Button6"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'CheckBox14
        '
        resources.ApplyResources(Me.CheckBox14, "CheckBox14")
        Me.CheckBox14.Checked = True
        Me.CheckBox14.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox14.Name = "CheckBox14"
        Me.CheckBox14.UseVisualStyleBackColor = True
        '
        'Button7
        '
        resources.ApplyResources(Me.Button7, "Button7")
        Me.Button7.Name = "Button7"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'CheckBox15
        '
        resources.ApplyResources(Me.CheckBox15, "CheckBox15")
        Me.CheckBox15.Name = "CheckBox15"
        Me.CheckBox15.UseVisualStyleBackColor = True
        '
        'CheckBox16
        '
        resources.ApplyResources(Me.CheckBox16, "CheckBox16")
        Me.CheckBox16.Checked = True
        Me.CheckBox16.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox16.Name = "CheckBox16"
        Me.CheckBox16.UseVisualStyleBackColor = True
        '
        'CheckBox17
        '
        resources.ApplyResources(Me.CheckBox17, "CheckBox17")
        Me.CheckBox17.Checked = True
        Me.CheckBox17.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox17.Name = "CheckBox17"
        Me.CheckBox17.UseVisualStyleBackColor = True
        '
        'CheckBox18
        '
        resources.ApplyResources(Me.CheckBox18, "CheckBox18")
        Me.CheckBox18.Checked = True
        Me.CheckBox18.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox18.Name = "CheckBox18"
        Me.CheckBox18.UseVisualStyleBackColor = True
        '
        'CheckBox19
        '
        resources.ApplyResources(Me.CheckBox19, "CheckBox19")
        Me.CheckBox19.Checked = True
        Me.CheckBox19.CheckState = System.Windows.Forms.CheckState.Checked
        Me.CheckBox19.Name = "CheckBox19"
        Me.CheckBox19.UseVisualStyleBackColor = True
        '
        'tpgClose
        '
        resources.ApplyResources(Me.tpgClose, "tpgClose")
        Me.tpgClose.Name = "tpgClose"
        Me.tpgClose.UseVisualStyleBackColor = True
        '
        'frmUsrInfo
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.tabCtrl)
        Me.Name = "frmUsrInfo"
        Me.tabCtrl.ResumeLayout(False)
        Me.tpgInfo.ResumeLayout(False)
        Me.tpgInfo.PerformLayout()
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.dtPickerStart.ResumeLayout(False)
        Me.dtPickerStart.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.tpgAuth.ResumeLayout(False)
        Me.tpgAuth.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox15.ResumeLayout(False)
        Me.GroupBox15.PerformLayout()
        Me.GroupBox18.ResumeLayout(False)
        Me.GroupBox18.PerformLayout()
        Me.GroupBox19.ResumeLayout(False)
        Me.GroupBox19.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox20.ResumeLayout(False)
        Me.tpgAuth2.ResumeLayout(False)
        Me.tpgAuth2.PerformLayout()
        Me.GroupBox22.ResumeLayout(False)
        Me.GroupBox22.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox21.ResumeLayout(False)
        Me.GroupBox21.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabCtrl As System.Windows.Forms.TabControl
    Friend WithEvents tpgInfo As System.Windows.Forms.TabPage
    Friend WithEvents tpgAuth As System.Windows.Forms.TabPage
    Friend WithEvents tpgClose As System.Windows.Forms.TabPage
    Friend WithEvents lnklabImport As System.Windows.Forms.LinkLabel
    Friend WithEvents lnklabSave As System.Windows.Forms.LinkLabel
    Friend WithEvents lnklabCam As System.Windows.Forms.LinkLabel
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents dtPickerStart As System.Windows.Forms.GroupBox
    Friend WithEvents butUni As System.Windows.Forms.Button
    Friend WithEvents butHighSch As System.Windows.Forms.Button
    Friend WithEvents butMidSch As System.Windows.Forms.Button
    Friend WithEvents cboxUniversity As System.Windows.Forms.ComboBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cboxHigh As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents cboxJuniory As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents cboxBloodType As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dtpickBirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents cboxSex As System.Windows.Forms.ComboBox
    Friend WithEvents dtpickOnBoard As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxAccount As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tboxStar As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents tBoxEngName As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkboxIsSales As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents tboxAddr As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tboxPostal As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tboxParentMobile As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxParent As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tboxMobile As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxIC As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxTel2 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxTel1 As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lnklabDel As System.Windows.Forms.LinkLabel
    Friend WithEvents tboxRemarks As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxUnitPay As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxClassPay As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSetDisc As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPaybyDate As System.Windows.Forms.CheckBox
    Friend WithEvents chkoxPaybyClass As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModReturn As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxKpReturn As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAddOwe As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxDelRpt As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModRpt As System.Windows.Forms.CheckBox
    Friend WithEvents ckhboxAddFee As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxChkRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxMkRpt As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelectNone4 As System.Windows.Forms.Button
    Friend WithEvents butSelectAll4 As System.Windows.Forms.Button
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxDelDisc As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModDisc As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAddDisc As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxSendBook As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendAnalysis As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendSeat As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendStuInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkBoxSendCome As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendAbsent As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelectNone3 As System.Windows.Forms.Button
    Friend WithEvents butSelectAll3 As System.Windows.Forms.Button
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxMkList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxReadList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxDelList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModList As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAddList As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxSendRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxDelRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxReadRec As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox19 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxStopAcc As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxModEmp As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxReademp As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAddEmp As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxModWork As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAllPerf As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxMyPerf As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelectNone5 As System.Windows.Forms.Button
    Friend WithEvents butSelectAll5 As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxBook As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAnalysis As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSeat As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxStuInfo As System.Windows.Forms.CheckBox
    Friend WithEvents chkBoxCome As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAbsent As System.Windows.Forms.CheckBox
    Friend WithEvents butPrevious2 As System.Windows.Forms.Button
    Friend WithEvents butSave2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents butDel As System.Windows.Forms.Button
    Friend WithEvents radbutManageClasses As System.Windows.Forms.RadioButton
    Friend WithEvents butAdd As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelectNone2 As System.Windows.Forms.Button
    Friend WithEvents butSelectAll2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxOwe As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPay As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPayRec As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxRpt As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxSendOwe As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendPay As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxSendPayRec As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox20 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelectNone1 As System.Windows.Forms.Button
    Friend WithEvents butSelectAll1 As System.Windows.Forms.Button
    Friend WithEvents tpgAuth2 As System.Windows.Forms.TabPage
    Friend WithEvents butNext2 As System.Windows.Forms.Button
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents CheckBox7 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox8 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox9 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox10 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox11 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox12 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox13 As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox21 As System.Windows.Forms.GroupBox
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents CheckBox14 As System.Windows.Forms.CheckBox
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents CheckBox15 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox16 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox17 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox18 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox19 As System.Windows.Forms.CheckBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents lblAuthName1 As System.Windows.Forms.Label
    Friend WithEvents lblAuthID1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox22 As System.Windows.Forms.GroupBox
    Friend WithEvents Button8 As System.Windows.Forms.Button
    Friend WithEvents CheckBox20 As System.Windows.Forms.CheckBox
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents CheckBox21 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox22 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox23 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox24 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox25 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox26 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox27 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox28 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox29 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox30 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox31 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox32 As System.Windows.Forms.CheckBox
    Friend WithEvents lblAuthName2 As System.Windows.Forms.Label
    Friend WithEvents lblAuthID2 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents butNext3 As System.Windows.Forms.Button
    Friend WithEvents butPrevious3 As System.Windows.Forms.Button
    Friend WithEvents butSave3 As System.Windows.Forms.Button
    Friend WithEvents CheckBox33 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox34 As System.Windows.Forms.CheckBox
    Friend WithEvents CheckBox2 As System.Windows.Forms.CheckBox
    Friend WithEvents lstboxClass As System.Windows.Forms.ListBox
    Friend WithEvents butModify As System.Windows.Forms.Button
    Friend WithEvents butNext As System.Windows.Forms.Button
    Friend WithEvents butPrevious As System.Windows.Forms.Button
    Friend WithEvents radbutManageAllClass As System.Windows.Forms.RadioButton
    Friend WithEvents chkBoxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
    Friend WithEvents chkboxDiscMod As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxDelAble As System.Windows.Forms.CheckBox
End Class
