﻿Public Class frm2AddBook
    Dim intId As Integer = -1
    Dim strName As String
    Dim intStock As Integer
    Dim intPrice As Integer
    Dim dtBefore As Date
    Dim bytFeeClear As Byte
    Dim dtClear As Date
    Dim bytMulti As Byte
    Dim dtCreate As Date
    Dim lstSubClass As New ArrayList
    Dim dtSubClass As New DataTable
    Dim dtSubClassName As New DataTable

    Public Sub New(ByVal ID As Integer, ByVal Name As String, _
                   ByVal Stock As Integer, ByVal Price As Integer, _
                   ByVal BeforeDate As Date, ByVal FeeClear As Byte, _
                   ByVal FeeClearDate As Date, ByVal MultiClass As Byte, _
                   ByVal CreateDate As Date, ByVal BookClass As ArrayList)

        InitializeComponent()
        dtSubClass = frmMain.GetSubClassList
        dtSubClassName.Columns.Add(c_IDColumnName, GetType(System.Int32))
        dtSubClassName.Columns.Add(c_NameColumnName, GetType(System.String))
        frmMain.SetOkState(False)
        If ID > -1 Then 'modify book
            Me.Text = My.Resources.updateBook
            intId = ID
            strName = Name
            intStock = Stock
            intPrice = Price
            dtBefore = BeforeDate
            bytFeeClear = FeeClear
            dtClear = FeeClearDate
            bytMulti = MultiClass
            dtCreate = CreateDate
            lstSubClass = BookClass
            RefreshDisplay()
        End If


    End Sub

    Private Sub RefreshDisplay()
        tboxName.Text = strName
        tboxStock.Text = intStock.ToString
        tboxPrice.Text = intPrice.ToString
        dtpickBefore.Value = dtBefore
        If dtCreate.Year <> 1911 Then
            chkboxRegDate.Checked = True
            dtpickReg.Value = dtCreate
        Else
            chkboxRegDate.Checked = False
        End If

        If bytFeeClear = 0 Then
            chkboxClearFee.Checked = True
            If dtClear.Year <> 1911 Then
                radbutClear.Checked = True
                dtPickClear.Value = dtClear
            Else
                radbutNoClearDate.Checked = True
            End If
        Else
            chkboxClearFee.Checked = False
        End If

        If bytMulti = 0 Then
            radbutMulti.Checked = True
        Else
            radbutMultiNo.Checked = True
        End If

        Dim intScId As Integer
        For index As Integer = 0 To dtSubClass.Rows.Count - 1
            intScId = dtSubClass.Rows(index).Item(c_IDColumnName)
            If lstSubClass.IndexOf(intScId) > -1 Then
                dtSubClassName.Rows.Add(dtSubClass.Rows(index).Item(c_IDColumnName), _
                                     dtSubClass.Rows(index).Item(c_NameColumnName).trim)
            End If
        Next
        dgv.DataSource = dtSubClassName
        LoadColumnText()

    End Sub

    Private Sub LoadColumnText()
        If dgv.Columns.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).Visible = False
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName

        End If
    End Sub

    Private Sub butAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAdd.Click
        Dim frm As New frm2SelectClass
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim lstAddSc As ArrayList = frmMain.GetCurrentList
            Dim i As Integer = -1
            Dim s As String = ""
            For index As Integer = 0 To lstAddSc.Count - 1
                i = lstAddSc(index)
                If lstSubClass.IndexOf(i) = -1 Then
                    s = ""
                    For Each row In dtSubClass.Rows
                        If row.item(c_IDColumnName) = i Then
                            s = row.item(c_NameColumnName).trim
                            Exit For
                        End If
                    Next
                    If Not s = "" Then
                        dtSubClassName.Rows.Add(i, s)
                        dgv.DataSource = dtSubClassName
                        LoadColumnText()
                        lstSubClass.Add(i)
                    End If
                End If
            Next

            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub butDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDel.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim i As Integer = dgv.SelectedRows(0).Index
            lstSubClass.Remove(dtSubClassName.Rows(i).Item(c_IDColumnName))
            dtSubClassName.Rows.RemoveAt(i)
        End If
    End Sub

    Private Sub butDelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDelAll.Click
        dtSubClassName.Clear()
        lstSubClass.Clear()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click

        If Not frmMain.CheckAuth(61) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        If lstSubClass.Count = 0 Then
            MsgBox(My.Resources.msgBookNeedClass, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Exit Sub
        End If

        strName = tboxName.Text
        intStock = GetIntValue(tboxStock.Text)
        intPrice = GetIntValue(tboxPrice.Text)
        If intStock = 0 Or intPrice = 0 Then
            Dim result As MsgBoxResult = MsgBox(My.Resources.msgAddBookNoStockPrice, MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
            If result = MsgBoxResult.No Then
                Exit Sub
            End If
        End If

        dtBefore = GetDateStart(dtpickBefore.Value)
        If chkboxRegDate.Checked = True Then
            dtCreate = dtpickReg.Value
        Else
            dtCreate = New Date(1911, 11, 11)
        End If
        dtClear = New Date(1911, 11, 11)

        If chkboxClearFee.Checked = True Then
            bytFeeClear = 0
            If radbutClear.Checked = True Then
                dtClear = dtPickClear.Value
            End If
        Else
            bytFeeClear = 1
        End If
        If radbutMulti.Checked = True Then
            bytMulti = 0
        Else
            bytMulti = 1
        End If

        If intId = -1 Then
            objCsol.AddBook(strName, intStock, intPrice, dtBefore, bytFeeClear, _
                             dtClear, bytMulti, dtCreate, lstSubClass)
        Else
            objCsol.UpdateBook(intId, strName, intStock, intPrice, dtBefore, bytFeeClear, _
                             dtClear, bytMulti, dtCreate, lstSubClass)
        End If
        frmMain.BackgroundWorker8.RunWorkerAsync()
        frmMain.SetOkState(True)
        Me.Close()
    End Sub
End Class