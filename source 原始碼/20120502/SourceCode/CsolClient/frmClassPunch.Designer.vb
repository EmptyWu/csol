﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClassPunch
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClassPunch))
        Me.chkboxBook = New System.Windows.Forms.CheckBox
        Me.timerUsb = New System.Windows.Forms.Timer(Me.components)
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.radbutEndClass = New System.Windows.Forms.RadioButton
        Me.radbutBeginClass = New System.Windows.Forms.RadioButton
        Me.lblMsg = New System.Windows.Forms.Label
        Me.tboxScreen = New System.Windows.Forms.TextBox
        Me.picbox = New System.Windows.Forms.PictureBox
        Me.chklstBook = New System.Windows.Forms.CheckedListBox
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.timerClock = New System.Windows.Forms.Timer(Me.components)
        Me.lblTime = New System.Windows.Forms.Label
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'chkboxBook
        '
        resources.ApplyResources(Me.chkboxBook, "chkboxBook")
        Me.chkboxBook.ForeColor = System.Drawing.Color.White
        Me.chkboxBook.Name = "chkboxBook"
        Me.chkboxBook.UseVisualStyleBackColor = True
        '
        'timerUsb
        '
        Me.timerUsb.Enabled = True
        Me.timerUsb.Interval = 250
        '
        'tboxID
        '
        Me.tboxID.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxID.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Name = "Label9"
        '
        'radbutEndClass
        '
        resources.ApplyResources(Me.radbutEndClass, "radbutEndClass")
        Me.radbutEndClass.ForeColor = System.Drawing.Color.White
        Me.radbutEndClass.Name = "radbutEndClass"
        Me.radbutEndClass.TabStop = True
        Me.radbutEndClass.UseVisualStyleBackColor = True
        '
        'radbutBeginClass
        '
        resources.ApplyResources(Me.radbutBeginClass, "radbutBeginClass")
        Me.radbutBeginClass.Checked = True
        Me.radbutBeginClass.ForeColor = System.Drawing.Color.White
        Me.radbutBeginClass.Name = "radbutBeginClass"
        Me.radbutBeginClass.TabStop = True
        Me.radbutBeginClass.UseVisualStyleBackColor = True
        '
        'lblMsg
        '
        resources.ApplyResources(Me.lblMsg, "lblMsg")
        Me.lblMsg.ForeColor = System.Drawing.Color.White
        Me.lblMsg.Name = "lblMsg"
        '
        'tboxScreen
        '
        Me.tboxScreen.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxScreen.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.tboxScreen, "tboxScreen")
        Me.tboxScreen.ForeColor = System.Drawing.Color.White
        Me.tboxScreen.Name = "tboxScreen"
        Me.tboxScreen.ReadOnly = True
        '
        'picbox
        '
        resources.ApplyResources(Me.picbox, "picbox")
        Me.picbox.MaximumSize = New System.Drawing.Size(300, 300)
        Me.picbox.Name = "picbox"
        Me.picbox.TabStop = False
        '
        'chklstBook
        '
        Me.chklstBook.BackColor = System.Drawing.Color.White
        Me.chklstBook.CheckOnClick = True
        Me.chklstBook.FormattingEnabled = True
        resources.ApplyResources(Me.chklstBook, "chklstBook")
        Me.chklstBook.Name = "chklstBook"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'cboxClass
        '
        Me.cboxClass.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Name = "cboxClass"
        '
        'timerClock
        '
        Me.timerClock.Enabled = True
        Me.timerClock.Interval = 1000
        '
        'lblTime
        '
        resources.ApplyResources(Me.lblTime, "lblTime")
        Me.lblTime.ForeColor = System.Drawing.Color.White
        Me.lblTime.Name = "lblTime"
        '
        'frmClassPunch
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.chklstBook)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.picbox)
        Me.Controls.Add(Me.tboxScreen)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.chkboxBook)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.radbutEndClass)
        Me.Controls.Add(Me.radbutBeginClass)
        Me.Name = "frmClassPunch"
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkboxBook As System.Windows.Forms.CheckBox
    Friend WithEvents timerUsb As System.Windows.Forms.Timer
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents radbutEndClass As System.Windows.Forms.RadioButton
    Friend WithEvents radbutBeginClass As System.Windows.Forms.RadioButton
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents tboxScreen As System.Windows.Forms.TextBox
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
    Friend WithEvents chklstBook As System.Windows.Forms.CheckedListBox
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents timerClock As System.Windows.Forms.Timer
    Friend WithEvents lblTime As System.Windows.Forms.Label
End Class
