﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayRecSearch
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayRecSearch))
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.tboxTotal = New System.Windows.Forms.TextBox
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.Label9 = New System.Windows.Forms.Label
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.butListSta = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.radbutCustDate = New System.Windows.Forms.RadioButton
        Me.radbutNoDate = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboxFilter1 = New System.Windows.Forms.ComboBox
        Me.cboxFilter2 = New System.Windows.Forms.ComboBox
        Me.tboxFilter = New System.Windows.Forms.TextBox
        Me.dgvRec = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'tboxTotal
        '
        resources.ApplyResources(Me.tboxTotal, "tboxTotal")
        Me.tboxTotal.Name = "tboxTotal"
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuDelete, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'radbutCustDate
        '
        resources.ApplyResources(Me.radbutCustDate, "radbutCustDate")
        Me.radbutCustDate.Checked = True
        Me.radbutCustDate.Name = "radbutCustDate"
        Me.radbutCustDate.TabStop = True
        Me.radbutCustDate.UseVisualStyleBackColor = True
        '
        'radbutNoDate
        '
        resources.ApplyResources(Me.radbutNoDate, "radbutNoDate")
        Me.radbutNoDate.Name = "radbutNoDate"
        Me.radbutNoDate.UseVisualStyleBackColor = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'cboxFilter1
        '
        Me.cboxFilter1.FormattingEnabled = True
        Me.cboxFilter1.Items.AddRange(New Object() {resources.GetString("cboxFilter1.Items"), resources.GetString("cboxFilter1.Items1"), resources.GetString("cboxFilter1.Items2")})
        resources.ApplyResources(Me.cboxFilter1, "cboxFilter1")
        Me.cboxFilter1.Name = "cboxFilter1"
        '
        'cboxFilter2
        '
        Me.cboxFilter2.FormattingEnabled = True
        Me.cboxFilter2.Items.AddRange(New Object() {resources.GetString("cboxFilter2.Items"), resources.GetString("cboxFilter2.Items1"), resources.GetString("cboxFilter2.Items2")})
        resources.ApplyResources(Me.cboxFilter2, "cboxFilter2")
        Me.cboxFilter2.Name = "cboxFilter2"
        '
        'tboxFilter
        '
        resources.ApplyResources(Me.tboxFilter, "tboxFilter")
        Me.tboxFilter.Name = "tboxFilter"
        '
        'dgvRec
        '
        Me.dgvRec.AllowUserToAddRows = False
        Me.dgvRec.AllowUserToDeleteRows = False
        Me.dgvRec.AllowUserToResizeRows = False
        Me.dgvRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvRec, "dgvRec")
        Me.dgvRec.MultiSelect = False
        Me.dgvRec.Name = "dgvRec"
        Me.dgvRec.RowHeadersVisible = False
        Me.dgvRec.RowTemplate.Height = 15
        Me.dgvRec.RowTemplate.ReadOnly = True
        Me.dgvRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRec.ShowCellToolTips = False
        Me.dgvRec.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'frmPayRecSearch
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.dgvRec)
        Me.Controls.Add(Me.tboxFilter)
        Me.Controls.Add(Me.cboxFilter2)
        Me.Controls.Add(Me.cboxFilter1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tboxTotal)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.radbutCustDate)
        Me.Controls.Add(Me.radbutNoDate)
        Me.Name = "frmPayRecSearch"
        Me.ShowInTaskbar = False
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxTotal As System.Windows.Forms.TextBox
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents radbutCustDate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNoDate As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboxFilter1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboxFilter2 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxFilter As System.Windows.Forms.TextBox
    Friend WithEvents dgvRec As System.Windows.Forms.DataGridView
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
