﻿Namespace DataBaseAccess
    Public Class PayStaSubClass

        Public Shared Function GetListStuByClass(ByVal classID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode, ")
            sq.Append("SI.Address2, Cancel, SI.IC, ")
            sq.Append("PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email, SI.CardNum,StuType, School, ")
            sq.Append("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University, ")
            sq.Append("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sq.Append("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks,SI.Customize1 AS ComeInfo, ")
            sq.Append("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark,")
            sq.Append("SC.SubClassID, SU.ClassID, SU.Name AS SubClassName, SC.SeatNum, OU.Name AS SalesPerson, ")
            sq.Append("SC.FeeOwe,SC.RegisterDate,OU.Name AS SalesName ")
            sq.Append("FROM StuInfo AS SI LEFT OUTER JOIN ")
            sq.Append("StuClass AS SC ON SC.StuID = SI.ID INNER JOIN ")
            sq.Append("SubClass AS SU ON SC.SubClassID = SU.ID LEFT OUTER JOIN ")
            sq.Append("OrgUser AS OU ON SC.SalesID = OU.ID ")
            sq.AppendFormat("WHERE SU.ID in ({0}) ", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetListStuDiscByClass(ByVal classID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT distinct FD.Discount, FD.Remarks, FD.SubClassID, FD.StuID ")
            sq.Append("FROM FeeDiscount AS FD ")
            sq.Append("INNER JOIN SubClass AS SC on FD.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE SC.ID in ({0})", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetListStuOweByClass(ByVal classID As ArrayList) As DataTable

            Dim dt As New DataTable
            Dim arrclassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrclassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrclassid)

            Dim sq As New System.Text.StringBuilder()
            sq.Append("select pa.amount, pa.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("'0' as discount, sc.classid, pa.subclassid, tc.seatnum, '0' as backamount, ")
            sq.Append("'0' as keepamount, us2.name as salesperson, si.schoolgrade, pa.datetime, ")
            sq.Append("pa.paymethod, us1.name as handler, pa.extra, '' as discountremarks, ")
            sq.Append("pa.remarks, si.engname, pa.id, pa.receiptnum, pa.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from payrec as pa inner join ")
            sq.Append("subclass as sc on pa.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on pa.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on pa.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on pa.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (pa.stuid = tc.stuid and ")
            sq.Append("pa.subclassid = tc.subclassid) ")

            sq.AppendFormat("where (sc.id in ({0})) ", str)
            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("mb.amount as backamount, '0' as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from moneybackpayrec as mp inner join ")
            sq.Append("moneybackclassinfo as mb on mp.mbclassinfoid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.id in ({0})) ", str)

            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("'0' as backamount, mb.amount as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from keeppayrec as mp inner join ")
            sq.Append("keeppaysubclassrec as mb on mp.kpclassrecid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.id in ({0})) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetdtClassMBStaNoDate(ByVal subClassID As ArrayList) As DataTable
            Return GetdtClassMBSta(subClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetdtClassMBSta(ByVal subclass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            Dim arrSubClassid(subclass.Count - 1) As String
            For i As Integer = 0 To subclass.Count - 1
                arrSubClassid(i) = subclass(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)
            sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, ")
            sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount, BP.Extra, ")
            sq.Append("US.Name AS Handler, SC.Name AS SubClassName, MB.Amount AS BackAmount, ")
            sq.Append("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, ")
            sq.Append("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sq.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sq.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sq.Append("SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
            sq.Append("FROM MoneyBackClassInfo AS MB INNER JOIN ")
            sq.Append("MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID INNER JOIN ")
            sq.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sq.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer join ")
            sq.Append("OrgUser AS US ON BP.HandlerID = US.ID INNER JOIN ")
            sq.Append("StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")

            sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND SC.ID IN ({2}) ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"), str)

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt '000
        End Function


        Public Shared Function GetdtClassMKStaNoDate(ByVal subClassID As ArrayList) As DataTable
            Return GetdtClassMKSta(subClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetdtClassMKSta(ByVal subclass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            Dim arrSubClassid(subclass.Count - 1) As String
            For i As Integer = 0 To subclass.Count - 1
                arrSubClassid(i) = subclass(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)
            sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, ")
            sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount, BP.Extra, ")
            sq.Append("US.Name As Handler, SC.Name AS SubClassName, MB.Amount As BackAmount, ")
            sq.Append("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, ")
            sq.Append("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sq.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sq.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sq.Append("SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, ")
            sq.Append("SI.HighSch, SI.University, MB.DateTime ")
            sq.Append("FROM KeepPaySubClassRec AS MB INNER JOIN ")
            sq.Append("KeepPayRec AS BP ON MB.ID = BP.KPClassRecID INNER JOIN ")
            sq.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sq.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer join ")
            sq.Append("OrgUser As US ON BP.HandlerID = US.ID INNER JOIN ")
            sq.Append("StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")

            sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND (SC.ID = {2}) ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"), str)

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function



        Public Shared Function GetdtClassPayStaNoDate(ByVal subClassID As ArrayList) As DataTable
            Return GetdtClassPaySta(subClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetdtClassPaySta(ByVal subclass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            Dim arrSubClassid(subclass.Count - 1) As String
            For i As Integer = 0 To subclass.Count - 1
                arrSubClassid(i) = subclass(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)
            sq.Append("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sq.Append("SI.School, PA.DateTime, PA.PayMethod, PA.Amount, US.Name As Handler, ")
            sq.Append("PA.Extra, SC.ClassID, PA.SubClassID, TC.SeatNum, ")
            sq.Append("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sq.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sq.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sq.Append("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sq.Append("FROM PayRec AS PA INNER JOIN ")
            sq.Append("SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON PA.StuID = SI.ID left outer join ")
            sq.Append("OrgUser As US ON PA.HandlerID = US.ID INNER JOIN ")
            sq.Append("StuClass As TC ON (PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID) ")

            sq.AppendFormat("WHERE (PA.DateTime BETWEEN '{0}' AND '{1}') AND SC.ID in ({2}) ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"), str)

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function


        Public Shared Function GetListSubClassStaByNoDate(ByVal subClassID As ArrayList) As DataTable
            Return GetListSubClassSta(subClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetListSubClassSta(ByVal subclass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            Dim arrSubClassid(subclass.Count - 1) As String
            For i As Integer = 0 To subclass.Count - 1
                arrSubClassid(i) = subclass(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)
            sq.Append("SELECT CL.Name As ClassName, CL.[End], SU.ID, SU.Name, SU.ClassID, CL.Fee, ")
            sq.Append("(SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM StuClass ")
            sq.Append("WHERE (SubClassID = SU.ID) ")
            sq.AppendFormat("AND RegisterDate BETWEEN '{0}' AND '{1}') AS StuRegCnt, ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            sq.Append("(SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM StuClass ")
            sq.Append("WHERE (SubClassID = SU.ID) ")
            sq.AppendFormat("AND RegisterDate BETWEEN '{0}' AND '{1}' ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            sq.Append("AND	(FeeOwe = 0)) AS FeeClrCnt ")
            sq.Append("FROM SubClass AS SU INNER JOIN ")
            sq.Append("Class AS CL ON SU.ClassID = CL.ID ")
            sq.AppendFormat("WHERE su.ID in ({0}) ", str)
            sq.Append("ORDER BY ClassName ")

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function




        'Public Shared Function GetMKeepStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        '    Dim dt As New DataTable
        '    Dim sq As New System.Text.StringBuilder()
        '    sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
        '    sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name As Handler, ")
        '    sq.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount, ")
        '    sq.Append("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, SI.EngName, ")
        '    sq.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, ")
        '    sq.Append("SI.MumMobile, SI.SchoolGrade, SI.SchoolClass, SI.GraduateFrom, ")
        '    sq.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, TC.RegisterDate, ")
        '    sq.Append("SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
        '    sq.Append("FROM KeepPaySubClassRec AS MB ")
        '    sq.Append("INNER JOIN KeepPayRec AS BP ON MB.ID = BP.KPClassRecID ")
        '    sq.Append("INNER JOIN SubClass AS SC ON MB.SubClassID = SC.ID ")
        '    sq.Append("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
        '    sq.Append("INNER JOIN StuInfo AS SI ON MB.StuID = SI.ID ")
        '    sq.Append("left outer join OrgUser As US ON BP.HandlerID = US.ID ")
        '    sq.Append("INNER JOIN StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")

        '    sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') ", dateFrom.ToString("yyyy/MM/dd 00:00:00.000"), dateTo.ToString("yyyy/MM/dd 23:59:59.999"))

        '    Dim sql As String = sq.ToString()
        '    Try
        '        dt = CSOL.DataBase.LoadToDataTable(sql)
        '    Catch ex As Exception
        '        System.Diagnostics.Debug.Write(ex.ToString())
        '    End Try
        '    Return dt
        'End Function


        'Public Shared Function GetMBackStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        '    Dim dt As New DataTable
        '    Dim sq As New System.Text.StringBuilder()
        '    sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
        '    sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name AS Handler, ")
        '    sq.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount AS Amount, SC.ClassID, ")
        '    sq.Append("CL.Name AS ClassName, MB.SubClassID, SI.EngName, SI.Birthday, SI.DadName, ")
        '    sq.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
        '    sq.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
        '    sq.Append("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
        '    sq.Append("FROM MoneyBackClassInfo AS MB ")
        '    sq.Append("INNER JOIN MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID ")
        '    sq.Append("INNER JOIN SubClass AS SC ON MB.SubClassID = SC.ID ")
        '    sq.Append("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
        '    sq.Append("INNER JOIN StuInfo AS SI ON MB.StuID = SI.ID ")
        '    sq.Append("left outer join OrgUser AS US ON BP.HandlerID = US.ID ")
        '    sq.Append("INNER JOIN StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")

        '    sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') OR (BP.DateTime BETWEEN '{0}' AND '{1}') ", dateFrom.ToString("yyyy/MM/dd 00:00:00.000"), dateTo.ToString("yyyy/MM/dd 23:59:59.999"))

        '    Dim sql As String = sq.ToString()
        '    Try
        '        dt = CSOL.DataBase.LoadToDataTable(sql)
        '    Catch ex As Exception
        '        System.Diagnostics.Debug.Write(ex.ToString())
        '    End Try
        '    Return dt
        'End Function
    End Class
End Namespace
