﻿Namespace CSOL
    Public Class Convert
        Public Shared Function GetQuery(ByVal params As System.Collections.Specialized.NameValueCollection) As String
            Dim result As String = ""

            Dim pairs As New List(Of String)
            For Each key As String In params.AllKeys
                pairs.Add(String.Format("{0}={1}", System.Web.HttpUtility.UrlEncode(key), System.Web.HttpUtility.UrlEncode(params(key))))
            Next
            result = String.Join("&", pairs.ToArray())

            Return result
        End Function

        Public Shared Function ParseQuery(ByVal query As String) As System.Collections.Specialized.NameValueCollection
            Dim nv As New System.Collections.Specialized.NameValueCollection()

            If query.StartsWith("?") Then
                query = query.Remove(0, 1)
            End If

            If query.Trim() = "" Then
                Return nv
            End If

            Dim params() As String = query.Split("&")
            For Each param As String In params
                Dim pair() As String = param.Split("=")
                If pair.Length = 1 Then
                    nv.Add(System.Web.HttpUtility.UrlDecode(pair(0)), "")
                Else
                    nv.Add(System.Web.HttpUtility.UrlDecode(pair(0)), System.Web.HttpUtility.UrlDecode(String.Join("=", pair, 1, pair.Length - 1)))
                End If
            Next
            Return nv
        End Function

        Public Shared Function DataTableToXmlString(ByVal dt As DataTable) As String
            If dt.TableName = "" Then
                dt.TableName = "DataTable"
            End If

            Dim sw As New System.IO.StringWriter()
            dt.WriteXml(sw, System.Data.XmlWriteMode.WriteSchema)
            Return sw.ToString()
        End Function

        Public Shared Function XmlStringToDataTable(ByVal xmlString As String) As DataTable
            Dim sr As New System.IO.StringReader(xmlString)
            Dim dt As New DataTable()
            dt.ReadXml(sr)
            Return dt
        End Function

        Public Shared Function DataSetToXmlString(ByVal ds As DataSet) As String
            If ds.DataSetName = "" Then
                ds.DataSetName = "DataSet"
            End If
            For i As Integer = 0 To ds.Tables.Count - 1
                If ds.Tables(i).TableName = "" Then
                    ds.Tables(i).TableName = String.Format("DataTable{0}", i + 1)
                End If
            Next

            Dim sw As New System.IO.StringWriter()
            ds.WriteXml(sw, XmlWriteMode.WriteSchema)
            Return sw.ToString()
        End Function

        Public Shared Function XmlStringToDataSet(ByVal xmlString As String) As DataSet
            Dim sr As New System.IO.StringReader(xmlString)
            Dim ds As New DataSet()
            ds.ReadXml(sr)
            Return ds
        End Function

        Public Shared Function ImageToBase64String(ByVal image As System.Drawing.Image, ByVal imageformat As System.Drawing.Imaging.ImageFormat) As String
            Using ms As New System.IO.MemoryStream()
                image.Save(ms, imageformat)
                Dim bs() As Byte = ms.ToArray()
                Dim base64String As String = System.Convert.ToBase64String(bs)
                Return base64String
            End Using
        End Function

        Public Shared Function Base64StringToImage(ByVal base64String As String) As System.Drawing.Image
            Dim bs() As Byte = System.Convert.FromBase64String(base64String)
            Using ms As New System.IO.MemoryStream(bs, 0, bs.Length)
                ms.Write(bs, 0, bs.Length)
                Dim image As System.Drawing.Image = System.Drawing.Image.FromStream(ms, True)
                Return image
            End Using
        End Function

        Public Shared Function PointToString(ByVal pt As System.Drawing.Point) As String
            Return String.Format("{0},{1}", pt.X, pt.Y)
        End Function

        Public Shared Function PointsToString(ByVal pt() As System.Drawing.Point) As String
            Dim pts(pt.Length - 1) As String
            For i As Integer = 0 To pts.Length - 1
                pts(i) = PointToString(pt(i))
            Next

            Return String.Join(";", pts)
        End Function

        Public Shared Function StringToPoint(ByVal str As String) As System.Drawing.Point
            Dim pt As System.Drawing.Point = Nothing
            Dim pair() As String = str.Split(",")
            If pair.Length = 2 Then
                Dim x As Integer = 0
                Dim y As Integer = 0
                If Integer.TryParse(pair(0), x) And Integer.TryParse(pair(1), y) Then
                    pt = New System.Drawing.Point(x, y)
                End If
            End If

            Return pt
        End Function

        Public Shared Function StringToPoints(ByVal str As String) As System.Drawing.Point()
            Dim result As New List(Of System.Drawing.Point)

            Dim pts() As String = str.Split(";")
            For i As Integer = 0 To pts.Length - 1
                Dim pt As System.Drawing.Point = StringToPoint(pts(i))
                If pt = Nothing Then
                    Return New System.Drawing.Point() {}
                Else
                    result.Add(pt)
                End If
            Next

            Return result.ToArray()
        End Function

        Public Shared Function PowerSplit(ByVal content As String) As String()()
            Return PowerSplit(content, ";", ",")
        End Function

        Public Shared Function PowerSplit(ByVal content As String, ByVal listDelimeter As String, ByVal itemDelimeter As String) As String()()
            Dim list() As String = content.Split(listDelimeter)
            Dim result(list.Length - 1)() As String
            For i As Integer = 0 To list.Length - 1
                result(i) = list(i).Split(itemDelimeter)
            Next
            Return result
        End Function

        Public Shared Function PowerJoin(ByVal contents()() As String) As String
            Return PowerJoin(contents, ";", ",")
        End Function

        Public Shared Function PowerJoin(ByVal contents()() As String, ByVal listDelimeter As String, ByVal itemDelimeter As String) As String
            Dim list(contents.Length - 1) As String
            For i As Integer = 0 To contents.Length - 1
                list(i) = String.Join(itemDelimeter, contents(i))
            Next
            Return String.Join(listDelimeter, list)
        End Function
    End Class
End Namespace