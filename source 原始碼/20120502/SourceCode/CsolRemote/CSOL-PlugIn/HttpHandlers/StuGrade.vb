﻿Namespace HTTPHandlers
    Public Class StuGrade
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetClassPaperList"
                    GetClassPaperList()
                Case "GetStuGrades_sql"
                    GetStuGrades_sql()

                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/StuGrade"
            End Get
        End Property

        Private Sub GetClassPaperList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim GetClassPaperList As DataTable = DataBaseAccess.StuGrade.GetClassPaperList(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetClassPaperList", CSOL.Convert.DataTableToXmlString(GetClassPaperList))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetClassPaperList", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetStuGrades_sql()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim PaperID As Integer = RequestParams("PaperID")
            Dim sc As Integer = RequestParams("sc")

            Dim GetStuGrades_sql As DataTable = DataBaseAccess.StuGrade.GetStuGrades_sql(PaperID, sc)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetStuGrades_sql", CSOL.Convert.DataTableToXmlString(GetStuGrades_sql))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetStuGrades_sql", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

    End Class
End Namespace
