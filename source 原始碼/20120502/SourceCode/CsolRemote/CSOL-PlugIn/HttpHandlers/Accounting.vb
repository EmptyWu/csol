﻿Namespace HTTPHandlers
    Public Class Accounting
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetPayStaClassNoDAte"
                    GetPayStaClassNoDAte()
                Case "GetPayStaClass"
                    GetPayStaClass()
                Case "GetMKeepStaBySubClassNoDAte"
                    GetMKeepStaBySubClassNoDAte()
                Case "GetMKeepStaBySubClass"
                    GetMKeepStaBySubClass()
                Case "GetMBackStaBySubClassNoDAte"
                    GetMBackStaBySubClassNoDate()
                Case "GetMBackStaBySubClass"
                    GetMBackStaBySubClass()
                Case "GetListStuOweByClass"
                    GetListStuOweByClass()
                Case "GetListStuDiscByClass"
                    GetListStuDiscByClass()
                Case "GetListStuByClass"
                    GetListStuByClass()
                Case "GetListClassStaByDate"
                    GetListClassStaByDate()
                Case "GetListClassStaByNoDate"
                    GetListClassStaByNoDate()
                Case "GetSubClassID"
                    GetSubClassID()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/Accounting"
            End Get
        End Property

        Private Sub GetSubClassID()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim GetSubClassID As DataTable = DataBaseAccess.Accounting.GetSubClassID(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetSubClassID", CSOL.Convert.DataTableToXmlString(GetSubClassID))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetSubClassID", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetPayStaClassNoDAte()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim PayStaClass As DataTable = DataBaseAccess.Accounting.GetPayStaClassNoDAte(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("PayStaClass", CSOL.Convert.DataTableToXmlString(PayStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("PayStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub
        Private Sub GetPayStaClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim PayStaClass As DataTable = DataBaseAccess.Accounting.GetPayStaByClass(classID, DateFrom, DateTo)

            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("PayStaClass", CSOL.Convert.DataTableToXmlString(PayStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("PayStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetMKeepStaBySubClassNoDAte()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))

            Dim MKStaClass As DataTable = DataBaseAccess.Accounting.GetMKeepStaBySubClassNoDate(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("MKStaClass", CSOL.Convert.DataTableToXmlString(MKStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("MKStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub
        Private Sub GetMKeepStaBySubClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim MKStaClass As DataTable = DataBaseAccess.Accounting.GetMKeepStaBySubClass(classID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("MKStaClass", CSOL.Convert.DataTableToXmlString(MKStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("MKStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetMBackStaBySubClassNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            Dim MBStaClass As DataTable = DataBaseAccess.Accounting.GetMBackStaBySubClassNoDate(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("MBStaClass", CSOL.Convert.DataTableToXmlString(MBStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("MBStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub
        Private Sub GetMBackStaBySubClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim MBStaClass As DataTable = DataBaseAccess.Accounting.GetMBackStaBySubClass(classID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("MBStaClass", CSOL.Convert.DataTableToXmlString(MBStaClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("MKStaClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListStuOweByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuOweByClass As DataTable = DataBaseAccess.Accounting.ListStuOweByClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuOweByClass", CSOL.Convert.DataTableToXmlString(ListStuOweByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuOweByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListStuDiscByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuDiscByClass As DataTable = DataBaseAccess.Accounting.ListStuDiscByClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuDiscByClass", CSOL.Convert.DataTableToXmlString(ListStuDiscByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuDiscByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListStuByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim ListStuByClass As DataTable = DataBaseAccess.Accounting.ListStuByClass(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListStuByClass", CSOL.Convert.DataTableToXmlString(ListStuByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListStuByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListClassStaByDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim ListClassStaByDate As DataTable = DataBaseAccess.Accounting.ListClassStaByDate(classID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListClassStaByDate", CSOL.Convert.DataTableToXmlString(ListClassStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ListClassStaByDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListClassStaByNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList
            classID.AddRange(RequestParams("classID").Split(","))
            
            Dim ListClassStaByNoDate As DataTable = DataBaseAccess.Accounting.ListClassStaByNoDate(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("ListClassStaNoByDate", CSOL.Convert.DataTableToXmlString(ListClassStaByNoDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("ClassStaByNoDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

    End Class
End Namespace

