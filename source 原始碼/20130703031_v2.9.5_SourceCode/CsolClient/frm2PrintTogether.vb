﻿Public Class frm2PrintTogether
    Dim dtClassReg As New DataTable
    Private dtPay As New DataTable
    Dim strStuId As String = ""
    Private PageNumber As Integer
    Private dtDisc As New DataTable

    Public Sub SetDt(ByRef d As DataTable, ByRef d2 As DataTable, ByRef d3 As DataTable)
        dtClassReg = d.Clone
        dtClassReg.Merge(d)
        dtPay = d2.Clone
        dtPay.Merge(d2)
        dtDisc = d3.Clone
        dtDisc.Merge(d3)
        strStuId = frmMain.GetCurrentStu
        InitTable()
        RefreshClassTable()
        LoadColumnText()
    End Sub

    Private Sub InitTable()
        dtPay.Columns.Add(c_ShouldPayColumnName, GetType(System.Int32))
        dtPay.Columns.Add(c_FeeOweCalculatedColumnName, GetType(System.Int32))
        dtPay.Columns.Add(c_StartColumnName, GetType(System.DateTime))
        dtPay.Columns.Add(c_EndColumnName, GetType(System.DateTime))
        Dim intSc As Integer = -1
        For Each row As DataRow In dtPay.Rows
            intSc = row.Item(c_SubClassIDColumnName)
            For Each row2 As DataRow In dtClassReg.Rows
                If intSc = row2.Item(c_SubClassIDColumnName) Then
                    row.Item(c_ShouldPayColumnName) = row2.Item(c_AmountColumnName)
                    row.Item(c_FeeOweCalculatedColumnName) = row2.Item(c_FeeOweCalculatedColumnName)
                    row.Item(c_StartColumnName) = row2.Item(c_StartColumnName)
                    row.Item(c_EndColumnName) = row2.Item(c_EndColumnName)
                    Exit For
                End If
            Next
        Next
    End Sub

    Private Sub RefreshClassTable()

        Dim strCompute2 As String
        Dim strCompute3 As String
        Dim strF As String = ""
        If dtPay.Rows.Count = 0 Then
            dgv.DataSource = dtPay
            Dim column As New DataGridViewCheckBoxColumn(False)
            column.Name = c_CheckColumnName
            dgv.Columns.Add(column)
            Exit Sub
        End If

        Try
            strCompute2 = c_EndColumnName & ">='" & Now.ToString & "'"
            strCompute3 = c_DateTimeColumnName & ">='" & GetDateStart(Now).ToString & "'"
            If chkboxShowPast.Checked Then
                If radbutShowToday.Checked Then
                    strF = strCompute3
                End If
            Else
                If radbutShowToday.Checked Then
                    strF = strCompute2 & " AND " & strCompute3
                Else
                    strF = strCompute2
                End If
            End If

            dtPay.DefaultView.RowFilter = strF

            dgv.DataSource = dtPay
            Dim column As New DataGridViewCheckBoxColumn(False)
            column.Name = c_CheckColumnName
            dgv.Columns.Add(column)

            For index As Integer = 0 To dgv.Rows.Count - 1
                dgv.Rows(index).Cells(c_CheckColumnName).Value = False
            Next



        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_CheckColumnName).HeaderText = My.Resources.selectCheck
            dgv.Columns.Item(c_CheckColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_CheckColumnName).Visible = True
            dgv.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgv.Columns.Item(c_ClassNameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_ClassNameColumnName).Visible = True
            dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgv.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgv.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.payDateTime
            dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_DateTimeColumnName).Visible = True
            dgv.Columns.Item(c_ShouldPayColumnName).HeaderText = My.Resources.shouldPay
            dgv.Columns.Item(c_ShouldPayColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_ShouldPayColumnName).Visible = True
            dgv.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.thisPay
            dgv.Columns.Item(c_AmountColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_AmountColumnName).Visible = True
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).HeaderText = My.Resources.feeOwe
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).DisplayIndex = 7
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).Visible = True

        End If

    End Sub


    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrint.Click
        Dim flag As Boolean = False
        For index As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(index).Cells("Check").Value = True Then
                flag = True
                Exit For
            End If
        Next
        If flag Then
            prtdocReceipt.Print()
        End If
        Me.Close()
    End Sub

    Private Sub radbutShowToday_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutShowToday.CheckedChanged
        RefreshClassTable()
        LoadColumnText()
    End Sub

    Private Sub radbutShowAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutShowAll.CheckedChanged
        RefreshClassTable()
        LoadColumnText()
    End Sub

    Private Sub butSelectAll1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll1.Click
        For index As Integer = 0 To dgv.Rows.Count - 1
            dgv.Rows(index).Cells(c_CheckColumnName).Value = True
        Next
    End Sub

    Private Sub butSelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone.Click
        For index As Integer = 0 To dgv.Rows.Count - 1
            dgv.Rows(index).Cells(c_CheckColumnName).Value = False
        Next
    End Sub

    Private Sub chkboxShowPast_CheckedChanged_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassTable()
        LoadColumnText()
    End Sub

    Private Sub dgv_CellClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        If dgv.CurrentCell.ColumnIndex = dgv.Columns(c_CheckColumnName).Index Then
            If Not Object.Equals(dgv.Item(e.ColumnIndex, e.RowIndex).Value, True) Then
                dgv.Item(e.ColumnIndex, e.RowIndex).Value = True
            Else
                dgv.Item(e.ColumnIndex, e.RowIndex).Value = False
            End If
        End If
    End Sub

    Private Sub prtdocReceipt_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles prtdocReceipt.BeginPrint
        PageNumber = 0
    End Sub

    Private Sub prtdocReceipt_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles prtdocReceipt.PrintPage
        'PageNumber += 1
        'If (PageNumber >= 2) Then e.HasMorePages = False Else _
        'e.HasMorePages = True

        'Page height and width, A4: 210x297 B5:182x257
        Dim dtPrintOpt As New DataTable
        dtPrintOpt = frmMain.GetReceiptPrint
        Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)
        Dim nPageH As Integer = 0
        Dim nPageW As Integer = 0

        'Page Margin
        Dim nPageRL As Integer = 0
        Dim nPageTB As Integer = 0


        If bytPaperSize = c_PaperSizeA4 Then
            nPageH = 297
            nPageW = 210
            nPageRL = 8
            nPageTB = 12
        ElseIf bytPaperSize = c_PaperSizeB5 Then
            nPageH = 257
            nPageW = 182
            nPageRL = 25
            nPageTB = 8
        Else
            nPageH = 297
            nPageW = 210

            nPageRL = 8
            nPageTB = 12
        End If
       

        'Define the cusor's coordinate
        Dim cursor_X As Integer = nPageRL
        Dim cursor_Y As Integer = nPageTB

        Dim MyCopy As Boolean = False
        'Draw the first part
        DrawReceipt(MyCopy, cursor_X, cursor_Y, e)
        'Define the page unit
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        'Draw the center line
        Dim myPen As Pen = New Pen(Color.Black, 0.5)
        myPen.DashStyle = Drawing2D.DashStyle.Dash
        cursor_X = nPageRL
        cursor_Y = nPageH / 2
        e.Graphics.DrawLine(myPen, cursor_X, cursor_Y, nPageW - cursor_X, cursor_Y)
        'Draw the 2nd part
        MyCopy = True
        'cursor_Y = cursor_Y + nPageRL / 4
        cursor_Y = cursor_Y + nPageTB / 6
        If bytPaperSize = c_PaperSizeB5 Then
            DrawReceipt(MyCopy, cursor_X, cursor_Y + 4, e)
        Else
            DrawReceipt(MyCopy, cursor_X, cursor_Y, e)
        End If
    End Sub

    Private Sub DrawReceipt(ByVal IsCopy As Boolean, ByRef cursor_X As Integer, ByRef cursor_Y As Integer, ByVal e As Printing.PrintPageEventArgs)
        'Define the page unit
        e.Graphics.PageUnit = GraphicsUnit.Millimeter
        'Define the Title format
        Dim MyTitformat As New StringFormat
        MyTitformat.Alignment = StringAlignment.Near
        'Title fonts
        Dim MyTitFont1 As New Font("Arial", 12, FontStyle.Bold)
        Dim dtPrintOpt As New DataTable
        dtPrintOpt = frmMain.GetReceiptPrint
        If dtPrintOpt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1 Then
            If IsCopy Then
                Exit Sub
            End If
        End If
        Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)
        'Page height and width, A4: 210x297
        Dim nPageH As Integer = 0
        Dim nPageW As Integer = 0
        'Page Margin
        Dim nPageRL As Integer = 0
        Dim nPageTB As Integer = 0
        If bytPaperSize = c_PaperSizeA4 Then
            nPageH = 297
            nPageW = 210
            nPageRL = 8
            nPageTB = 12
        ElseIf bytPaperSize = c_PaperSizeB5 Then
            nPageH = 257
            nPageW = 182
            nPageRL = 25
            nPageTB = 8
        Else
            nPageH = 297
            nPageW = 210
            nPageRL = 8
            nPageTB = 12
        End If
        Dim lstScName As New ArrayList
        Dim lstPeriod As New ArrayList
        Dim lstSeat As New ArrayList
        Dim lstAmount As New ArrayList
        Dim lstReceiptNum As New ArrayList
        Dim lstFeeOwe As New ArrayList
        Dim intTotalAmt As Integer = 0
        Dim strRemarks As String = ""
        Dim strRemarks_Temporary As New List(Of String)
        Dim strHandler As String = ""
        Dim strSalesName As String = ""
        Dim strSalesName_Temporary As New List(Of String)
        Dim strDate As String = GetROCDateNum(Now)
        Dim strDiscRemarks As String = ""
        Dim strDiscRemarks_Temporary As New List(Of String)
        Dim date1 As Date
        Dim date2 As Date
        Dim strPeriod As String = ""
        Dim intscId As Integer
        For index As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(index).Cells("Check").Value = True Then
                lstScName.Add(dgv.Rows(index).Cells(c_SubClassNameColumnName).Value)
                lstSeat.Add(dgv.Rows(index).Cells(c_SeatNumColumnName).Value)
                lstAmount.Add(dgv.Rows(index).Cells(c_AmountColumnName).Value)
                lstReceiptNum.Add(dgv.Rows(index).Cells(c_ReceiptNumColumnName).Value)
                date1 = dgv.Rows(index).Cells(c_StartColumnName).Value
                date2 = dgv.Rows(index).Cells(c_EndColumnName).Value
                strPeriod = GetDateNum(date1) & " - " & GetDateNum(date2)
                lstPeriod.Add(strPeriod)
                If Not dgv.Rows(index).Cells("Remarks").Value.ToString.Trim = "" Then
                    strRemarks_Temporary.Add(dgv.Rows(index).Cells("Remarks").Value.ToString.Trim)
                End If
                If Not dgv.Rows(index).Cells("SalesPerson").Value.ToString.Trim = "" Then
                    If Not strSalesName_Temporary.Contains(dgv.Rows(index).Cells("SalesPerson").Value.ToString) Then
                        strSalesName_Temporary.Add(dgv.Rows(index).Cells("SalesPerson").Value.ToString)
                    End If
                    'strSalesName += dgv.Rows(index).Cells("SalesPerson").Value.ToString + ","
                Else
                    strSalesName_Temporary.Add("無")
                    'strSalesName += "無" + ","
                End If
                intscId = dgv.Rows(index).Cells(c_SubClassIDColumnName).Value
                For i As Integer = 0 To dtDisc.Rows.Count - 1
                    If dtDisc.Rows(i).Item(c_SubClassIDColumnName) = intscId Then
                        If Not strDiscRemarks_Temporary.Contains(dtDisc.Rows(i).Item(c_RemarksColumnName)) Then
                            strDiscRemarks_Temporary.Add(dtDisc.Rows(i).Item(c_RemarksColumnName))
                        End If
                        'strDiscRemarks += dtDisc.Rows(i).Item(c_RemarksColumnName) & ","
                    End If
                Next
            End If
        Next
        strDiscRemarks = Join(strDiscRemarks_Temporary.ToArray, ",")
        strSalesName = Join(strSalesName_Temporary.ToArray, ",")
        strRemarks = Join(strRemarks_Temporary.ToArray, ",")
        'strSalesName = strSalesName.ToString.Remove(strSalesName.Length - 1, 1)
        For index As Integer = 0 To lstAmount.Count - 1
            intTotalAmt = intTotalAmt + CInt(lstAmount(index))
        Next
        'strDiscRemarks = ""
        'strRemarks = ""
        strHandler = frmMain.GetUsrName
        'Declare the title 
        Dim MyTitle As String = GetClassTitle() & " " & My.Resources.receipt
        e.Graphics.DrawString(MyTitle, MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
        'The height of a string
        Dim dHLine As Integer = 0
        'height of Title string
        dHLine = e.Graphics.MeasureString(MyTitle, MyTitFont1).Height
        cursor_Y = cursor_Y + dHLine * 2
        Dim MyTitFont2 As New Font("Arial", 12, FontStyle.Regular)
        Dim MyTitle2 As String = My.Resources.stuID & " : " & frmMain.GetCurrentStu
        e.Graphics.DrawString(MyTitle2, MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

        dHLine = e.Graphics.MeasureString(MyTitle2, MyTitFont2).Height
        cursor_Y = cursor_Y + dHLine
        Dim MyTitle3 As String = My.Resources.stuName & " : "
        'the Name should be read from database
        Dim MyName As String = frmMain.GetCurrentString
        e.Graphics.DrawString(MyTitle3 & MyName, MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

        'No need to recalculate the the character height since it should be same as the 2nd line
        cursor_Y = cursor_Y + dHLine
        Dim MyTitle4 As String = My.Resources.actualAmt & " : "
        'the Name should be read from database
        Dim MyFee As Integer = intTotalAmt
        e.Graphics.DrawString(MyTitle4 & MyFee & My.Resources.NTComplete, _
                              MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

        cursor_Y = cursor_Y + dHLine
        'Define the Content format
        Dim MyCharMargin As Integer = 1
        Dim MyConformat1 As New StringFormat
        MyConformat1.Alignment = StringAlignment.Center
        MyConformat1.LineAlignment = StringAlignment.Center
        Dim MyConFont1 As New Font("Arial", 10, FontStyle.Regular)
        Dim MyConTit1 As String = My.Resources.receiptNum
        Dim MyConTit2 As String = "報名班別"
        Dim MyConTit3 As String = My.Resources.classPeriod
        Dim MyConTit4 As String = My.Resources.seatNum
        Dim MyConTit5 As String = My.Resources.amount
        Dim MyConTit6 As String = "餘額"

        dHLine = e.Graphics.MeasureString(MyConTit1, MyConFont1).Height
        Dim MyTableW As Integer = (nPageW - 16) / 10
        'Draw the table cell0
        cursor_Y = cursor_Y + dHLine
        Dim MyRect0 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1.5, 0), dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit1, MyConFont1, Brushes.Black, MyRect0, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect0)

        cursor_X = cursor_X + Math.Round(MyTableW * 1.5, 0)
        Dim MyRect1 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 3, 0), dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit2, MyConFont1, Brushes.Black, MyRect1, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect1)

        cursor_X = cursor_X + Math.Round(MyTableW * 3, 0)
        Dim MyRect2 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 2.5, 0), dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit3, MyConFont1, Brushes.Black, MyRect2, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect2)

        cursor_X = cursor_X + Math.Round(MyTableW * 2.5, 0)
        Dim MyRect3 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1, 0), dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit4, MyConFont1, Brushes.Black, MyRect3, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect3)

        cursor_X = cursor_X + Math.Round(MyTableW * 1, 0)
        Dim MyRect4 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1, 0), dHLine + MyCharMargin * 2)
        e.Graphics.DrawString(MyConTit5, MyConFont1, Brushes.Black, MyRect4, MyConformat1)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect4)

        cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
        cursor_X = nPageRL
        Dim MyRect5 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1.5, 0), (dHLine + MyCharMargin * 2) * 10)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect5)
        Dim MyRect3Cell As Rectangle
        For index As Integer = 0 To lstReceiptNum.Count - 1
            If index > 7 Then Exit Sub
            MyRect3Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, Math.Round(MyTableW * 1.5, 0), dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstReceiptNum(index), MyConFont1, Brushes.Black, MyRect3Cell, MyConformat1)
        Next

        Dim dtpay2 As DataTable = dtPay.DefaultView.ToTable(True, c_SubClassNameColumnName, c_FeeOweCalculatedColumnName)
        Dim feeOweAmount As Integer = dtpay2.Compute("SUM(FeeOweCalculated)", "")

        e.Graphics.DrawString("餘額:" + feeOweAmount.ToString, MyConFont1, Brushes.Black, cursor_X, cursor_Y + 55)


        cursor_X = cursor_X + Math.Round(MyTableW * 1.5, 0)
        Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 3, 0), (dHLine + MyCharMargin * 2) * 10)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)


        Dim MyRect4Cell As Rectangle
        For index As Integer = 0 To lstScName.Count - 1
            If index > 7 Then Exit Sub
            Dim leangths As Integer = System.Text.Encoding.Default.GetBytes(lstScName(index)).Length
            Dim sizes As Integer = 1
            If leangths < 25 Then
                sizes = 10
            ElseIf leangths < 27 Then
                sizes = 9
            ElseIf leangths < 31 Then
                sizes = 8
            Else
                sizes = 7
            End If
            Dim MyConFontx As New Font("Arial", sizes, FontStyle.Regular)

            'dHLine = e.Graphics.MeasureString(lstScName(index), MyConFont1, Math.Round(MyTableW * 3, 0)).Height
            MyRect4Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, Math.Round(MyTableW * 3, 0), dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstScName(index), MyConFontx, Brushes.Black, MyRect4Cell, MyConformat1)
        Next

        cursor_X = cursor_X + Math.Round(MyTableW * 3, 0)
        Dim MyRect7 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 2.5, 0), (dHLine + MyCharMargin * 2) * 10)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect7)
        Dim MyRect5Cell As Rectangle
        For index As Integer = 0 To lstPeriod.Count - 1
            If index > 7 Then Exit Sub
            MyRect5Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, Math.Round(MyTableW * 2.5, 0), dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstPeriod(index), MyConFont1, Brushes.Black, MyRect5Cell, MyConformat1)
        Next

        cursor_X = cursor_X + Math.Round(MyTableW * 2.5, 0)
        Dim MyRect8 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1, 0), (dHLine + MyCharMargin * 2) * 10)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect8)
        Dim MyRect6Cell As Rectangle
        For index As Integer = 0 To lstSeat.Count - 1
            If index > 7 Then Exit Sub
            MyRect6Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, Math.Round(MyTableW * 1, 0), dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstSeat(index), MyConFont1, Brushes.Black, MyRect6Cell, MyConformat1)
        Next

        cursor_X = cursor_X + Math.Round(MyTableW * 1, 0)
        Dim MyRect9 As Rectangle = New Rectangle(cursor_X, cursor_Y, Math.Round(MyTableW * 1, 0), (dHLine + MyCharMargin * 2) * 10)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect9)
        Dim MyRect7Cell As Rectangle
        For index As Integer = 0 To lstAmount.Count - 1
            If index > 7 Then Exit Sub
            MyRect7Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, Math.Round(MyTableW * 1, 0), dHLine + MyCharMargin * 2)
            e.Graphics.DrawString(lstAmount(index), MyConFont1, Brushes.Black, MyRect7Cell, MyConformat1)
        Next





        'Draw the bottom cell
        cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) * 10
        cursor_X = nPageRL
        Dim MyNote As String = My.Resources.remarks
        Dim MyNoteW As Integer = e.Graphics.MeasureString(MyNote, MyConFont1).Width * 2
        Dim MyRect10 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyNoteW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect10)
        e.Graphics.DrawString("業務", MyConFont1, Brushes.Black, MyRect10, MyConformat1)

        Dim MyRect12 As Rectangle = New Rectangle(cursor_X, cursor_Y + 6, MyNoteW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect12)                                             '業務
        e.Graphics.DrawString(MyNote, MyConFont1, Brushes.Black, MyRect12, MyConformat1)

        cursor_X = cursor_X + MyNoteW

        Dim MyRect11 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 9 - MyNoteW, dHLine + MyCharMargin * 2)
        Dim MyRect15 As Rectangle = New Rectangle(cursor_X, cursor_Y + 1, MyTableW * 9 - MyNoteW, dHLine + MyCharMargin * 2)
        Dim MyNoteVal As String = "  " & strRemarks
        Dim MyConformat2 As New StringFormat
        Dim MyRect13 As Rectangle = New Rectangle(cursor_X, cursor_Y + 6, MyTableW * 9 - MyNoteW, dHLine + MyCharMargin * 2)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect11)
        e.Graphics.DrawString(strSalesName, MyConFont1, Brushes.Black, MyRect15, MyConformat2)
        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect13)

        MyConformat2.Alignment = StringAlignment.Near
        MyConformat2.LineAlignment = StringAlignment.Center
        If (IsCopy = False) Then
            'Display for the copy to the student
            e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect13)
            e.Graphics.DrawString(MyNoteVal, MyConFont1, Brushes.Black, MyRect13, MyConformat2)
        Else
            'Display for the copy to the school
            Dim MyDiscount As String = My.Resources.discount
            'Got from database
            Dim MyDisReason As String = My.Resources.reason & " = "
            'Got from database
            Dim MyDisReVal As String = strDiscRemarks
            Dim MyNote2 As String = My.Resources.receiptRemarks & " = "
            e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect13)
            e.Graphics.DrawString(MyDiscount & MyDisReason & MyDisReVal _
                                  & ", " & MyNote2 & MyNoteVal, MyConFont1, Brushes.Black, MyRect13, MyConformat2)
        End If
      

        'cursor_X = cursor_X - MyNoteW
        'cursor_Y = cursor_Y + dHLine + MyCharMargin * 4
        'Dim MyRect9 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
        'Dim MyAdmin As String = My.Resources.dealer
        'Dim MyAdminVal As String = strHandler
        'e.Graphics.DrawString(MyAdmin & MyAdminVal, MyConFont1, Brushes.Black, MyRect9, MyConformat2)

        'cursor_X = cursor_X + MyTableW * 10 / 3
        'Dim MyRect10 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
        'Dim MyTeacher As String = My.Resources.audit
        ''Get from database
        'Dim MyTeaVal As String = ""
        'e.Graphics.DrawString(MyTeacher & MyTeaVal, MyConFont1, Brushes.Black, MyRect10, MyConformat1)

        'cursor_X = cursor_X + MyTableW * 10 / 3
        'Dim MyRect11 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
        'Dim MyConformat3 As New StringFormat
        'MyConformat3.Alignment = StringAlignment.Far
        'MyConformat3.LineAlignment = StringAlignment.Center
        'Dim MyCell4Con As String = My.Resources.datetime & ": " & strDate
        'e.Graphics.DrawString(MyCell4Con, MyConFont1, Brushes.Black, MyRect11, MyConformat3)

    End Sub
    'Private Sub DrawReceipt(ByVal IsCopy As Boolean, ByRef cursor_X As Integer, ByRef cursor_Y As Integer, ByVal e As Printing.PrintPageEventArgs)
    '    'Define the page unit
    '    e.Graphics.PageUnit = GraphicsUnit.Millimeter
    '    'Define the Title format
    '    Dim MyTitformat As New StringFormat
    '    MyTitformat.Alignment = StringAlignment.Near


    '    'Title fonts
    '    Dim MyTitFont1 As New Font("Arial", 12, FontStyle.Bold)
    '    Dim dtPrintOpt As New DataTable
    '    dtPrintOpt = frmMain.GetReceiptPrint
    '    If dtPrintOpt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1 Then
    '        If IsCopy Then
    '            Exit Sub
    '        End If
    '    End If

    '    Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)
    '    'Page height and width, A4: 210x297
    '    Dim nPageH As Integer = 0
    '    Dim nPageW As Integer = 0

    '    'Page Margin
    '    Dim nPageRL As Integer = 0
    '    Dim nPageTB As Integer = 0

    '    If bytPaperSize = c_PaperSizeA4 Then
    '        nPageH = 297
    '        nPageW = 210
    '        'nPageRL = 16
    '        'nPageTB = 25
    '        nPageRL = 8
    '        nPageTB = 12
    '    ElseIf bytPaperSize = c_PaperSizeB5 Then
    '        nPageH = 257
    '        nPageW = 182
    '        'nPageRL = 14
    '        'nPageTB = 21
    '        nPageRL = 16
    '        nPageTB = 8
    '    Else
    '        nPageH = 297
    '        nPageW = 210
    '        'nPageRL = 16
    '        'nPageTB = 25
    '        nPageRL = 8
    '        nPageTB = 12
    '    End If
    '    'If bytPaperSize = c_PaperSizeA4 Then
    '    '    nPageH = 297
    '    '    nPageW = 210
    '    '    'nPageRL = 32
    '    '    nPageRL = 20
    '    '    nPageTB = 30
    '    'ElseIf bytPaperSize = c_PaperSizeB5 Then
    '    '    nPageH = 257
    '    '    nPageW = 182
    '    '    'nPageRL = 28
    '    '    nPageRL = 17
    '    '    nPageTB = 26
    '    'Else
    '    '    nPageH = 297
    '    '    nPageW = 210
    '    '    'nPageRL = 32
    '    '    nPageRL = 20
    '    '    nPageTB = 30
    '    'End If

    '    Dim lstScName As New ArrayList
    '    Dim lstPeriod As New ArrayList
    '    Dim lstSeat As New ArrayList
    '    Dim lstAmount As New ArrayList
    '    Dim lstReceiptNum As New ArrayList
    '    Dim lstFeeOwe As New ArrayList


    '    Dim intTotalAmt As Integer = 0
    '    Dim strRemarks As String = ""
    '    Dim strHandler As String = ""
    '    Dim strDate As String = GetROCDateNum(Now)
    '    Dim strDiscRemarks As String = ""
    '    Dim date1 As Date
    '    Dim date2 As Date
    '    Dim strPeriod As String = ""

    '    For index As Integer = 0 To dgv.Rows.Count - 1
    '        If dgv.Rows(index).Cells("Check").Value = True Then

    '            lstScName.Add(dgv.Rows(index).Cells(c_SubClassNameColumnName).Value)
    '            lstSeat.Add(dgv.Rows(index).Cells(c_SeatNumColumnName).Value)
    '            lstAmount.Add(dgv.Rows(index).Cells(c_AmountColumnName).Value)
    '            lstReceiptNum.Add(dgv.Rows(index).Cells(c_ReceiptNumColumnName).Value)
    '            lstFeeOwe.Add(dgv.Rows(index).Cells(c_FeeOweCalculatedColumnName).Value)
    '            date1 = dgv.Rows(index).Cells(c_StartColumnName).Value
    '            date2 = dgv.Rows(index).Cells(c_EndColumnName).Value
    '            strPeriod = GetDateNum(date1) & " - " & GetDateNum(date2)
    '            lstPeriod.Add(strPeriod)
    '        End If
    '    Next

    '    For index As Integer = 0 To lstAmount.Count - 1
    '        intTotalAmt = intTotalAmt + CInt(lstAmount(index))
    '    Next
    '    strDiscRemarks = ""
    '    strRemarks = ""
    '    strHandler = frmMain.GetUsrName

    '    'Declare the title 
    '    Dim MyTitle As String = GetClassTitle() & " " & My.Resources.receipt
    '    e.Graphics.DrawString(MyTitle, MyTitFont1, Brushes.Black, cursor_X, cursor_Y, MyTitformat)
    '    'The height of a string
    '    Dim dHLine As Integer = 0

    '    'height of Title string
    '    dHLine = e.Graphics.MeasureString(MyTitle, MyTitFont1).Height
    '    cursor_Y = cursor_Y + dHLine * 2
    '    Dim MyTitFont2 As New Font("Arial", 12, FontStyle.Regular)
    '    Dim MyTitle2 As String = My.Resources.stuID & " : " & frmMain.GetCurrentStu
    '    e.Graphics.DrawString(MyTitle2, _
    '                          MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

    '    dHLine = e.Graphics.MeasureString(MyTitle2, MyTitFont2).Height
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyTitle3 As String = My.Resources.stuName & " : "
    '    'the Name should be read from database
    '    Dim MyName As String = frmMain.GetCurrentString
    '    e.Graphics.DrawString(MyTitle3 & MyName, MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

    '    'No need to recalculate the the character height since it should be same as the 2nd line
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyTitle4 As String = My.Resources.actualAmt & " : "
    '    'the Name should be read from database
    '    Dim MyFee As Integer = intTotalAmt
    '    e.Graphics.DrawString(MyTitle4 & MyFee & My.Resources.NTComplete, _
    '                          MyTitFont2, Brushes.Black, cursor_X, cursor_Y, MyTitformat)

    '    cursor_Y = cursor_Y + dHLine
    '    'Define the Content format
    '    Dim MyCharMargin As Integer = 1
    '    Dim MyConformat1 As New StringFormat
    '    MyConformat1.Alignment = StringAlignment.Center
    '    MyConformat1.LineAlignment = StringAlignment.Center
    '    Dim MyConFont1 As New Font("Arial", 10, FontStyle.Regular)

    '    Dim MyConTit1 As String = My.Resources.subClassName
    '    Dim MyConTit2 As String = My.Resources.classPeriod
    '    Dim MyConTit3 As String = My.Resources.seatNum
    '    Dim MyConTit4 As String = My.Resources.amount
    '    dHLine = e.Graphics.MeasureString(MyConTit1, MyConFont1).Height
    '    Dim MyTableW As Integer = (nPageW - nPageRL) / 10
    '    'Draw the table cell1
    '    cursor_Y = cursor_Y + dHLine
    '    Dim MyRect1 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 4, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit1, MyConFont1, Brushes.Black, MyRect1, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect1)

    '    'Draw the Table Cell2
    '    cursor_X = cursor_X + MyTableW * 4
    '    Dim MyRect2 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 4, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit2, MyConFont1, Brushes.Black, MyRect2, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect2)

    '    cursor_X = cursor_X + MyTableW * 4
    '    Dim MyRect21 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit3, MyConFont1, Brushes.Black, MyRect21, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect21)

    '    cursor_X = cursor_X + MyTableW
    '    Dim MyRect22 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawString(MyConTit4, MyConFont1, Brushes.Black, MyRect22, MyConformat1)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect22)

    '    'Draw the Table Cell3, there are 8 rows in each cell
    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 2
    '    cursor_X = nPageRL
    '    Dim MyRect3 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 4, (dHLine + MyCharMargin * 2) * 10)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect3)
    '    'There are 8 small cells in the Rect3
    '    Dim MyRect3Cell As Rectangle
    '    For index As Integer = 0 To lstScName.Count - 1
    '        If index > 7 Then Exit Sub
    '        MyRect3Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW * 4, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstScName(index), MyConFont1, Brushes.Black, MyRect3Cell, MyConformat1)
    '    Next
    '    'Draw the Table Cell4
    '    cursor_X = cursor_X + MyTableW * 4
    '    Dim MyRect4 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 4, (dHLine + MyCharMargin * 2) * 10)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect4)
    '    Dim MyRect4Cell As Rectangle
    '    For index As Integer = 0 To lstPeriod.Count - 1
    '        If index > 7 Then Exit Sub
    '        MyRect4Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW * 4, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstPeriod(index), MyConFont1, Brushes.Black, MyRect4Cell, MyConformat1)
    '    Next
    '    'Draw the Table Cell5
    '    cursor_X = cursor_X + MyTableW * 4
    '    Dim MyRect5 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 10)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect5)
    '    Dim MyRect5Cell As Rectangle
    '    For index As Integer = 0 To lstSeat.Count - 1
    '        If index > 7 Then Exit Sub
    '        MyRect5Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstSeat(index), MyConFont1, Brushes.Black, MyRect5Cell, MyConformat1)
    '    Next
    '    'Draw the Table Cell6
    '    cursor_X = cursor_X + MyTableW
    '    Dim MyRect6 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW, (dHLine + MyCharMargin * 2) * 10)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect6)
    '    Dim MyRect6Cell As Rectangle
    '    For index As Integer = 0 To lstAmount.Count - 1
    '        If index > 7 Then Exit Sub
    '        MyRect6Cell = New Rectangle(cursor_X, cursor_Y + dHLine * (index + 1) + MyCharMargin * 2, MyTableW, dHLine + MyCharMargin * 2)
    '        e.Graphics.DrawString(lstAmount(index), MyConFont1, Brushes.Black, MyRect6Cell, MyConformat1)
    '    Next

    '    'Draw the bottom cell
    '    cursor_Y = cursor_Y + (dHLine + MyCharMargin * 2) * 10
    '    cursor_X = nPageRL
    '    Dim MyNote As String = My.Resources.remarks
    '    Dim MyNoteW As Integer = e.Graphics.MeasureString(MyNote, MyConFont1).Width * 2
    '    Dim MyRect7 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyNoteW, dHLine + MyCharMargin * 2)
    '    e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect7)
    '    e.Graphics.DrawString(MyNote, MyConFont1, Brushes.Black, MyRect7, MyConformat1)

    '    cursor_X = cursor_X + MyNoteW

    '    Dim MyRect8 As Rectangle = New Rectangle(cursor_X, cursor_Y, MyTableW * 10 - MyNoteW, dHLine + MyCharMargin * 2)
    '    Dim MyNoteVal As String = "  " & strRemarks
    '    Dim MyConformat2 As New StringFormat
    '    MyConformat2.Alignment = StringAlignment.Near
    '    MyConformat2.LineAlignment = StringAlignment.Center
    '    If (IsCopy = False) Then
    '        'Display for the copy to the student
    '        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect8)
    '        e.Graphics.DrawString(MyNoteVal, MyConFont1, Brushes.Black, MyRect8, MyConformat2)
    '    Else
    '        'Display for the copy to the school
    '        Dim MyDiscount As String = My.Resources.discount
    '        'Got from database
    '        Dim MyDisReason As String = My.Resources.reason & " = "
    '        'Got from database
    '        Dim MyDisReVal As String = strDiscRemarks
    '        Dim MyNote2 As String = My.Resources.receiptRemarks & " = "
    '        e.Graphics.DrawRectangle(New Pen(Color.Black, 0.5), MyRect8)
    '        e.Graphics.DrawString(MyDiscount & MyDisReason & MyDisReVal _
    '                              & ", " & MyNote2 & MyNoteVal, MyConFont1, Brushes.Black, MyRect8, MyConformat2)
    '    End If

    '    cursor_X = cursor_X - MyNoteW
    '    cursor_Y = cursor_Y + dHLine + MyCharMargin * 4
    '    Dim MyRect9 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyAdmin As String = My.Resources.dealer
    '    Dim MyAdminVal As String = strHandler
    '    e.Graphics.DrawString(MyAdmin & MyAdminVal, MyConFont1, Brushes.Black, MyRect9, MyConformat2)

    '    cursor_X = cursor_X + MyTableW * 10 / 3
    '    Dim MyRect10 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyTeacher As String = My.Resources.audit
    '    'Get from database
    '    Dim MyTeaVal As String = ""
    '    e.Graphics.DrawString(MyTeacher & MyTeaVal, MyConFont1, Brushes.Black, MyRect10, MyConformat1)

    '    cursor_X = cursor_X + MyTableW * 10 / 3
    '    Dim MyRect11 As New Rectangle(cursor_X, cursor_Y, MyTableW * 10 / 3, dHLine + MyCharMargin * 2)
    '    Dim MyConformat3 As New StringFormat
    '    MyConformat3.Alignment = StringAlignment.Far
    '    MyConformat3.LineAlignment = StringAlignment.Center
    '    Dim MyCell4Con As String = My.Resources.datetime & ": " & strDate
    '    e.Graphics.DrawString(MyCell4Con, MyConFont1, Brushes.Black, MyRect11, MyConformat3)

    'End Sub

    

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。

    End Sub
End Class