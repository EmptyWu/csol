﻿Public Class frm2SelectClass
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private dtSubClassList As New DataTable
    Private lstSubClass As New ArrayList
    Private lstSubClassShown As New ArrayList

    Public Sub New()

        InitializeComponent()

        dtClass = frmMain.GetClassList
        dtSubClassList = frmMain.GetSubClassList
        For index As Integer = 0 To dtClass.Rows.Count - 1
            If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            End If
        Next

        lstboxClass.SelectedIndex = -1
        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            If chklstSubClass.GetItemChecked(index) = True Then
                lstSubClass.Add(lstSubClassShown(index))
            End If
        Next

        If lstSubClass.Count > 0 Then
            frmMain.SetCurrentList(lstSubClass)
            frmMain.SetOkState(True)
            Me.Close()
        End If
    End Sub

    Private Sub lstboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstboxClass.SelectedIndexChanged
        If lstboxClass.SelectedIndex > -1 Then
            Dim intC As Integer
            Dim intSc As Integer
            intC = lstClass(lstboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    If lstSubClassShown.Contains(intSc) = False Then
                        lstSubClassShown.Add(intSc)
                        chklstSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName))
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
    End Sub

    Private Sub butSelNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub
    Private Sub InitList()
        lstClass.Clear()
        lstboxClass.Items.Clear()
        chklstSubClass.Items.Clear()
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                    lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        lstboxClass.SelectedIndex = -1
    End Sub
End Class