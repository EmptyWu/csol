﻿Public Class frmPunchMakeup
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private dtBook As New DataTable
    Private dtPay As New DataTable
    Private lstBook As New ArrayList
    Private lstBookShown As New ArrayList
    Private dtSession As New DataTable
    Private lstSession As New ArrayList
    Private dtStu As New DataTable
    Private dtResult As New DataTable
    Private ds As New DataSet

    Private strCardNum As String
    Private intContent As Integer
    Private intSubClass As Integer
    Private intSession As Integer
    Private intClass As Integer
    Private strStuId As String
    Private intResult As Integer
    Private lstMulti As New ArrayList
    Private lstMultiShown As New ArrayList

    Private intWaitUsb As Integer = 0
    Private blManual As Boolean = False
    Private stuIdx As Integer = 0
    Private lstPunchedStu As New List(Of String)

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPunchMakeup
        RefreshData()

    End Sub

    Private Sub frmPunchMakeup_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPunchMakeup_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        '20100501 by sherry for speed
        dtClass = frmMain.GetClassList
        dtSubClass = frmMain.GetSubClassList

        'GetClassList
        'GetSubClassList
        If dtClass.Compute("Count (" & c_IDColumnName & ")", c_IDColumnName & "=" & c_IndividualClassID) = 0 Then
            dtClass.Rows.Add(c_IndividualClassID, My.Resources.idvClassName, Now, Now, 0, 0, 0)
        End If

        If dtSubClass.Compute("Count (" & c_IDColumnName & ")", c_IDColumnName & "=" & c_IndividualClassID) = 0 Then
            dtSubClass.Rows.Add(c_IndividualClassID, My.Resources.idvClassName, c_IndividualClassID, 0, 0, "", "", _
                                1, 0)
        End If

        dtContent = frmMain.GetContentsNoByUsr
        dtSession = frmMain.GetSessions
        dtBook = frmMain.GetBooks
        '20100501 by sherry for speed
        'dtPay = objCsol.ListPayRec
        cboxClass.Items.Clear()
        lstClass.Clear()
        For index As Integer = 0 To dtClass.Rows.Count - 1
            If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                If Not lstClass.Contains(dtClass.Rows(index).Item(c_IDColumnName)) Then                 '100211
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If                                                                                  '100211
            End If
        Next
        If lstClass.Contains(c_IndividualClassID) = False Then
            cboxClass.Items.Add(My.Resources.idvClassName)
            lstClass.Add(c_IndividualClassID)
        End If
        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged        
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            If cboxSubClass.Items.Count > 0 Then
                cboxSubClass.SelectedIndex = 0
            Else
                cboxSubClass.Text = ""
            End If
            cboxBook.SelectedIndex = 0
        End If
    End Sub

    Private Sub frmPunchMakeup_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        Try
            cboxContent.Items.Clear()
            lstContent.Clear()
            cboxSession.Items.Clear()
            lstSession.Clear()
            If cboxSubClass.SelectedIndex > -1 Then
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If GetIntValue(dtContent.Rows(index).Item(c_SubClassIDColumnName)) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName).trim)
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                For index As Integer = 0 To dtSession.Rows.Count - 1
                    If GetIntValue(dtSession.Rows(index).Item(c_SubClassIDColumnName)) = i Then
                        cboxSession.Items.Add(GetWeekDayName(GetIntValue(dtSession.Rows(index).Item(c_DayOfWeekColumnName)), 0) & _
                                              "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_StartColumnName))) & _
                                              "-" & GetTimeString(CDate(dtSession.Rows(index).Item(c_EndColumnName))))
                        lstSession.Add(dtSession.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxContent.Items.Count > 0 Then
                    cboxContent.SelectedIndex = 0
                End If
                If cboxSession.Items.Count > 0 Then
                    cboxSession.SelectedIndex = 0
                End If
                dtStu = objCsol.ListStuBySubClass(i)
            End If
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try
    End Sub

    Private Sub cboxContent_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxContent.SelectedIndexChanged
        updatePunchInfo()
    End Sub

    Private Sub cboxBook_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxBook.SelectedIndexChanged
        If cboxBook.SelectedIndex = 1 And cboxSubClass.SelectedIndex > -1 Then
            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            lstBook.Clear()
            lstBookShown.Clear()
            lstMulti.Clear()
            lstMultiShown.Clear()
            For index As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(index).Item(c_SubClassIDColumnName) = i Then
                    If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                        lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                        chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).trim)
                        lstMultiShown.Add(dtBook.Rows(index).Item(c_MultiClassColumnName))
                    End If
                End If
            Next
            chklstBook.Visible = True
        Else
            lstBook.Clear()
            lstBookShown.Clear()
            lstMultiShown.Clear()
            chklstBook.Items.Clear()
            chklstBook.Visible = False
        End If
    End Sub

    Private Sub timerClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerClock.Tick
        lblTime.Text = GetTimeNumNow()
    End Sub

    Private Sub butStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butStart.Click
        tboxID.Clear()
        tboxCardNum.Clear()
        timerClock.Start()

        'And cboxSession.SelectedIndex > -1 
        'cboxContent.SelectedIndex > -1 And _
        If cboxSubClass.SelectedIndex > -1 And _
            cboxClass.SelectedIndex > -1 Then

            If cboxSession.SelectedIndex > -1 Then
                intSession = lstSession(cboxSession.SelectedIndex)
            Else
                intSession = -1
            End If
            'lstPunchedStu = New ArrayList

            MsgBox(My.Resources.msgCheckInputLang, MsgBoxStyle.Information)
            If radbutBeginClass.Checked Then
                lblName.Text = My.Resources.classInPunchStart
            ElseIf radbutEndClass.Checked Then
                lblName.Text = My.Resources.classOutPunchStart
            End If
            tboxCardNum.ReadOnly = False
            tboxCardNum.Focus()

        Else
            MessageBox.Show(My.Resources.msgNoClassSelected, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End If
    End Sub

    Private Sub chklstBook_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstBook.ItemCheck
        Dim intC As Integer
        Dim intM As Integer
        Dim i As Integer
        intC = lstBookShown(e.Index)
        intM = lstMultiShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstBook.Add(intC)
            lstMulti.Add(intM)
        Else
            i = lstBook.IndexOf(intC)
            If i > -1 Then
                lstBook.RemoveAt(i)
                lstMulti.RemoveAt(i)
            End If
        End If
    End Sub

    Private Sub butEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEnd.Click
        If radbutBeginClass.Checked Then
            lblName.Text = My.Resources.classInPunchEnd
        ElseIf radbutEndClass.Checked Then
            lblName.Text = My.Resources.classOutPunchEnd
        End If
        tboxID.Text = ""
        lblSeatNum.Text = ""
        tboxSeatNum.Text = ""
        lblMsg.Text = ""
        tboxStatus.Text = ""
        lblMsg.Text = ""

        tboxCardNum.ReadOnly = True

    End Sub

    Private Sub UpdateInforUsb()
        Dim dtNote As New DataTable
        Dim ds As New DataSet
        Dim dis As Byte
        Dim time As Integer
        Dim content As String = ""
        Dim strTempId As String

        If cboxContent.SelectedIndex = -1 Then
            MsgBox("請確實選取上課內容!!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Exit Sub
        End If

        strCardNum = tboxCardNum.Text.Trim
        If lstClass(cboxClass.SelectedIndex) = c_IndividualClassID And strCardNum.Length > 6 Then
            Try 'for individual study class

                Dim intClassMode As Integer
                If radbutBeginClass.Checked Then
                    intClassMode = c_ClassIn
                ElseIf radbutEndClass.Checked Then
                    intClassMode = c_ClassOut
                End If
                dtResult = objCsol.StuPunchWithCardForIdvClass(strCardNum, Now, intClassMode)                                   '100315
                Dim intresult As Integer = CInt(dtResult.Rows(0).Item(c_PunchResultColumnName))
                Select Case intresult
                    Case c_PunchResultNoStu
                        lblMsg.Text = "請注意：" & vbNewLine & "並未找到該學生或該卡號在此班的報名資料！" & vbNewLine & "請確認他有報名此班級！"
                        tboxID.Clear()
                        tboxCardNum.Clear()

                    Case c_PunchResultSuccess
                        strStuId = dtResult.Rows(0).Item(c_StuIDColumnName)
                        tboxID.Text = strStuId
                        lblName.Text = dtResult.Rows(0).Item(c_StuNameColumnName)
                        lblSeatNum.Text = ""
                        tboxSeatNum.Text = ""
                        If strStuId.Length = 8 Then
                            Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                            If CheckFileExist(f) Then
                                Dim bm As New Bitmap(f)
                                picbox.Image = bm
                            End If
                        End If

                        tboxCardNum.Clear()

                    Case c_PunchResultNoCard
                        lblMsg.Text = My.Resources.msgNoCard
                        tboxID.Clear()
                        tboxCardNum.Clear()
                End Select

                
            Catch ex As Exception
                lblMsg.Text = strCardNum & ":" & My.Resources.msgPunchFailed
                tboxID.Clear()
                tboxCardNum.Clear()
                AddErrorLog(ex.ToString)
                Exit Sub
            End Try

            Exit Sub
        End If

        ds = objCsol.GetPunchData(strCardNum, intClass)
        dtStu = ds.Tables(c_StuInfoTableName)
        dtNote = ds.Tables(c_StuNoteDataTableName)
        If dtStu.Rows.Count > 0 Then
            stuIdx = -1
            For idx As Integer = 0 To dtStu.Rows.Count - 1
                If dtStu.Rows(idx).Item(c_CardNumColumnName).ToString.Trim.Equals(strCardNum) Then
                    stuIdx = idx
                End If
            Next
            If stuIdx = -1 Then
                tboxCardNum.Clear()
                Exit Sub
            End If
            strTempId = dtStu.Rows(stuIdx).Item(c_IDColumnName)
            If lstPunchedStu.Contains(strTempId.Trim) Then
                lblMsg.Text = My.Resources.msgStuPunched
                tboxID.Clear()
                tboxCardNum.Clear()
                Exit Sub
            Else
                lblMsg.Text = ""
                lstPunchedStu.Add(strTempId)
            End If
            strStuId = dtStu.Rows(stuIdx).Item(c_IDColumnName)
            tboxID.Text = strStuId
            lblName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName).trim
            lblSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
            tboxSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
            If strStuId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                End If
            End If

            tboxCardNum.Clear()

            If dtNote.Rows.Count > 0 Then
                dis = dtNote.Rows(0).Item(c_StyleColumnName)
                time = dtNote.Rows(0).Item(c_DisplayForColumnName)
                content = dtNote.Rows(0).Item(c_ContentColumnName).trim
                If content.Length > 0 Then
                    Dim frm As New frm2NoteDisplay(dis, time, content, _
                                    dtStu.Rows(stuIdx).Item(c_NameColumnName).trim)
                    frm.MdiParent = frmMain
                    frm.Show()
                End If
            End If

            If cboxBook.SelectedIndex = 1 Then
                '20100501 by sherry for speed
                dtPay = objCsol.ListPayRecByStuId(strStuId)
                Dim i As Integer = CheckIssue()
                If i = 0 Then
                    i = objCsol.AddBookIssueRec(strStuId, _
                     intClass, lstBook, Now, lstMulti)
                End If
                ShowMsg(i)
            End If
            If radbutBeginClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassIn)
            ElseIf radbutEndClass.Checked Then
                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                      -1, intSession, c_ClassOut)
            End If

        Else
            MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
            tboxID.Clear()
            tboxCardNum.Clear()
        End If
    End Sub

    Private Sub ShowMsg(ByVal i As Integer)
        Select Case i
            Case 0
                tboxStatus.Text = My.Resources.bookIssueOk
            Case 1
                tboxStatus.Text = My.Resources.msgBookIssue1
            Case 2
                tboxStatus.Text = My.Resources.msgBookIssue2
            Case 3
                tboxStatus.Text = My.Resources.msgBookIssue3
            Case 4
                tboxStatus.Text = My.Resources.msgBookIssue4
            Case 5
                tboxStatus.Text = My.Resources.msgBookIssue5
        End Select
    End Sub

    Private Function CheckIssue() As Integer
        Dim r As Integer = 0
        Dim book As Integer
        Dim FeeClear As Byte
        Dim FeeClearDate As Date
        Dim MultiClass As Byte
        Dim CreateDate As Date
        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        Dim c2 As String = c_StuIDColumnName & "='"
        Dim c3 As String = c_SubClassIDColumnName & "="
        Dim c4 As String = c_DateTimeColumnName & "> '"

        If dtStu.Rows.Count = 0 Then
            r = 5
            Return r
        End If
        For index As Integer = 0 To lstBook.Count - 1
            book = lstBook(index)
            Dim idx As Integer = -1
            For j As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(j).Item(c_IDColumnName) = book Then
                    idx = j
                    Exit For
                End If
            Next
            If idx > -1 Then
                FeeClear = dtBook.Rows(idx).Item(c_FeeClearColumnName)
                FeeClearDate = dtBook.Rows(idx).Item(c_FeeClearDateColumnName)
                MultiClass = dtBook.Rows(idx).Item(c_MultiClassColumnName)
                CreateDate = dtBook.Rows(idx).Item(c_CreateDateColumnName)
            Else
                r = 5
                Exit For
            End If
            If CreateDate.Year <> 1911 Then
                If dtStu.Rows(stuIdx).Item(c_RegDateColumnName) > CreateDate Then
                    r = 1
                    Exit For
                End If
            End If

            If FeeClear = 0 Then
                If FeeClearDate.Year <> 1911 Then
                    If dtPay.Compute(c1, c3 & intSubClass.ToString & " AND " & c4 & _
                                        FeeClearDate.ToString & "'") > 0 Then
                        r = 3
                        Exit For
                    End If
                End If
                If dtStu.Rows(stuIdx).Item(c_FeeOweColumnName) > 0 Then
                    r = 2
                    Exit For
                End If
            End If
        Next
        Return r
    End Function

    Private Sub tboxCardNum_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxCardNum.KeyDown
        If e.KeyCode = Keys.Return Then
            UpdateInforUsb()
        End If
    End Sub

    'Private Sub tboxCardNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxCardNum.TextChanged
    '    tboxCardNum.Text = tboxCardNum.Text.Trim.ToUpper
    '    If tboxCardNum.Text.Trim.Length > 5 Then
    '        If intWaitUsb = 0 Then
    '            timerUsb.Start()
    '        End If
    '    End If
    'End Sub

    Private Sub butManualPunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butManualPunch.Click
        blManual = True
        tboxID.ReadOnly = False
        tboxID.Clear()
        tboxCardNum.Clear()
        lblName.Text = ""
        lblSeatNum.Text = ""
        tboxSeatNum.Text = ""
        lblMsg.Text = ""
        picbox.Image = Nothing
        tboxID.Focus()
    End Sub

    Private Sub tboxID_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxID.KeyDown
        If e.KeyCode = Keys.Return Then
            If Not cboxClass.SelectedItem Is Nothing Then
                If cboxContent.SelectedIndex = -1 And cboxClass.SelectedItem.ToString = "有來補習班" Then

                ElseIf cboxContent.SelectedIndex = -1 Then
                    MsgBox("請確實選取上課內容!!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                    Exit Sub
                Else
                    'MsgBox("請確實選取上課內容!!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                    'Exit Sub
                End If
            Else
            End If
            If tboxID.Text.Trim.Length = 8 And blManual Then
                strStuId = tboxID.Text.Trim
                If lstClass(cboxClass.SelectedIndex) = c_IndividualClassID Then
                    Try 'for individual study class

                        Dim intClassMode As Integer
                        If radbutBeginClass.Checked Then
                            intClassMode = c_ClassIn
                        ElseIf radbutEndClass.Checked Then
                            intClassMode = c_ClassOut
                        End If

                        dtResult = objCsol.StuPunchWithIDForIdvClass(strStuId, Now, intClassMode)
                        Dim intresult As Integer = CInt(dtResult.Rows(0).Item(c_PunchResultColumnName))
                        Select Case intresult
                            Case c_PunchResultNoStu
                                'lblMsg.Text = My.Resources.msgNoStuRecFound
                                lblMsg.Text = "請注意：" & vbNewLine & "並未找到該學生或該卡號在此班的報名資料！" & vbNewLine & "請確認他有報名此班級！"
                                tboxID.Clear()
                                tboxCardNum.Clear()
                                picbox.Image = Nothing

                            Case c_PunchResultSuccess
                                lblMsg.Text = 2
                                lblName.Text = dtResult.Rows(0).Item(c_StuNameColumnName)
                                lblSeatNum.Text = ""
                                tboxSeatNum.Text = ""
                                If strStuId.Length = 8 Then
                                    Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                                    If CheckFileExist(f) Then
                                        Dim bm As New Bitmap(f)
                                        picbox.Image = bm
                                    Else
                                        picbox.Image = Nothing
                                    End If
                                End If
                                tboxCardNum.Clear()

                            Case c_PunchResultNoCard
                                lblMsg.Text = My.Resources.msgNoCard
                                tboxID.Clear()
                                tboxCardNum.Clear()
                        End Select

                    Catch ex As Exception
                        lblMsg.Text = strCardNum & ":" & My.Resources.msgPunchFailed
                        tboxID.Clear()
                        tboxCardNum.Clear()
                        AddErrorLog(ex.ToString)
                        Exit Sub
                    End Try
                    tboxID.Text = ""
                    Exit Sub
                End If

                Dim strTempId As String

                Dim stuInfo As DataTable = objCsol.DisplayStudentInfo(strStuId).Tables(c_StuInfoTableName)
                If stuInfo.Rows.Count = 0 Then
                    MessageBox.Show(My.Resources.msgNoSuchStuID, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    Exit Sub
                Else
                    strCardNum = stuInfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim
                End If

                Dim dtNote As New DataTable
                Dim ds As New DataSet
                Dim dis As Byte
                Dim time As Integer
                Dim content As String = ""
                'ds = objCsol.GetPunchData(strCardNum, intClass)                                    '100211
                ds = objCsol.GetPunchDataByID2(strStuId, intClass)
                dtStu = ds.Tables(c_StuInfoTableName)
                dtNote = ds.Tables(c_StuNoteDataTableName)

                If dtStu.Rows.Count > 0 Then
                    stuIdx = 0
                    strTempId = strStuId
                    If lstPunchedStu.Contains(strTempId.Trim) Then
                        lblMsg.Text = My.Resources.msgStuPunched
                        tboxID.Clear()
                        tboxCardNum.Clear()
                        Exit Sub
                    Else
                        lblMsg.Text = ""
                        lstPunchedStu.Add(strTempId)
                    End If

                    lblName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName).trim
                    lblSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
                    tboxSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
                    If strStuId.Length = 8 Then
                        Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                        If CheckFileExist(f) Then
                            Dim bm As New Bitmap(f)
                            picbox.Image = bm
                        End If
                    End If

                    tboxCardNum.Clear()

                    If dtNote.Rows.Count > 0 Then
                        dis = dtNote.Rows(0).Item(c_StyleColumnName)
                        time = dtNote.Rows(0).Item(c_DisplayForColumnName)
                        content = dtNote.Rows(0).Item(c_ContentColumnName).trim
                        If content.Length > 0 Then
                            Dim frm As New frm2NoteDisplay(dis, time, content, _
                                            dtStu.Rows(stuIdx).Item(c_NameColumnName).trim)
                            frm.MdiParent = frmMain
                            frm.Show()
                        End If
                    End If

                    If cboxBook.SelectedIndex = 1 Then
                        '20100501 by sherry for speed
                        dtPay = objCsol.ListPayRecByStuId(strStuId)
                        Dim i As Integer = CheckIssue()
                        If i = 0 Then
                            i = objCsol.AddBookIssueRec(strStuId, _
                             intClass, lstBook, Now, lstMulti)
                        End If
                        ShowMsg(i)
                    End If
                    If radbutBeginClass.Checked Then
                        objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                              -1, intSession, c_ClassIn)
                    ElseIf radbutEndClass.Checked Then
                        objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
                                              -1, intSession, c_ClassOut)
                    End If
                Else
                    MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    tboxID.Clear()
                    tboxCardNum.Clear()
                End If
                tboxID.Text = ""
                tboxID.Clear()
            Else
                MsgBox("請輸入正確學號以及確認是否有點選手動輸入學號 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                tboxID.Text = ""
                tboxID.Clear()
            End If
        End If
    End Sub

    'Private Sub tboxID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged
    '    If tboxID.Text.Trim.Length = 8 And blManual Then
    '        strStuId = tboxID.Text.Trim

    '        If lstClass(cboxClass.SelectedIndex) = c_IndividualClassID Then
    '            Try 'for individual study class

    '                Dim intClassMode As Integer
    '                If radbutBeginClass.Checked Then
    '                    intClassMode = c_ClassIn
    '                ElseIf radbutEndClass.Checked Then
    '                    intClassMode = c_ClassOut
    '                End If

    '                dtResult = objCsol.StuPunchWithIDForIdvClass(strStuId, Now, intClassMode)
    '                Dim intresult As Integer = CInt(dtResult.Rows(0).Item(c_PunchResultColumnName))
    '                Select Case intresult
    '                    Case c_PunchResultNoStu
    '                        'lblMsg.Text = My.Resources.msgNoStuRecFound
    '                        lblMsg.Text = "請注意：" & vbNewLine & "並未找到該學生或該卡號在此班的報名資料！" & vbNewLine & "請確認他有報名此班級！"
    '                        tboxID.Clear()
    '                        tboxCardNum.Clear()
    '                        picbox.Image = Nothing

    '                    Case c_PunchResultSuccess
    '                        lblMsg.Text = ""
    '                        lblName.Text = dtResult.Rows(0).Item(c_StuNameColumnName)
    '                        lblSeatNum.Text = ""
    '                        tboxSeatNum.Text = ""
    '                        If strStuId.Length = 8 Then
    '                            Dim f As String = GetPhotoPath() & strStuId & ".bmp"
    '                            If CheckFileExist(f) Then
    '                                Dim bm As New Bitmap(f)
    '                                picbox.Image = bm
    '                            Else
    '                                picbox.Image = Nothing
    '                            End If
    '                        End If
    '                        tboxCardNum.Clear()

    '                    Case c_PunchResultNoCard
    '                        lblMsg.Text = My.Resources.msgNoCard
    '                        tboxID.Clear()
    '                        tboxCardNum.Clear()
    '                End Select

    '            Catch ex As Exception
    '                lblMsg.Text = strCardNum & ":" & My.Resources.msgPunchFailed
    '                tboxID.Clear()
    '                tboxCardNum.Clear()
    '                AddErrorLog(ex.ToString)
    '                Exit Sub
    '            End Try
    '            tboxID.Text = ""
    '            Exit Sub
    '        End If

    '        Dim strTempId As String

    '        Dim stuInfo As DataTable = objCsol.DisplayStudentInfo(strStuId).Tables(c_StuInfoTableName)
    '        If stuInfo.Rows.Count = 0 Then
    '            MessageBox.Show(My.Resources.msgNoSuchStuID, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
    '            Exit Sub
    '        Else
    '            strCardNum = stuInfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim
    '        End If

    '        Dim dtNote As New DataTable
    '        Dim ds As New DataSet
    '        Dim dis As Byte
    '        Dim time As Integer
    '        Dim content As String = ""
    '        'ds = objCsol.GetPunchData(strCardNum, intClass)                                    '100211
    '        ds = objCsol.GetPunchDataByID2(strStuId, intClass)
    '        dtStu = ds.Tables(c_StuInfoTableName)
    '        dtNote = ds.Tables(c_StuNoteDataTableName)

    '        If dtStu.Rows.Count > 0 Then
    '            stuIdx = 0
    '            strTempId = strStuId
    '            If lstPunchedStu.Contains(strTempId.Trim) Then
    '                lblMsg.Text = My.Resources.msgStuPunched
    '                tboxID.Clear()
    '                tboxCardNum.Clear()
    '                Exit Sub
    '            Else
    '                lblMsg.Text = ""
    '                lstPunchedStu.Add(strTempId)
    '            End If

    '            lblName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName).trim
    '            lblSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
    '            tboxSeatNum.Text = dtStu.Rows(stuIdx).Item(c_SeatNumColumnName)
    '            If strStuId.Length = 8 Then
    '                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
    '                If CheckFileExist(f) Then
    '                    Dim bm As New Bitmap(f)
    '                    picbox.Image = bm
    '                End If
    '            End If

    '            tboxCardNum.Clear()

    '            If dtNote.Rows.Count > 0 Then
    '                dis = dtNote.Rows(0).Item(c_StyleColumnName)
    '                time = dtNote.Rows(0).Item(c_DisplayForColumnName)
    '                content = dtNote.Rows(0).Item(c_ContentColumnName).trim
    '                If content.Length > 0 Then
    '                    Dim frm As New frm2NoteDisplay(dis, time, content, _
    '                                    dtStu.Rows(stuIdx).Item(c_NameColumnName).trim)
    '                    frm.MdiParent = frmMain
    '                    frm.Show()
    '                End If
    '            End If

    '            If cboxBook.SelectedIndex = 1 Then
    '                Dim i As Integer = CheckIssue()
    '                If i = 0 Then
    '                    i = objCsol.AddBookIssueRec(strStuId, _
    '                     intClass, lstBook, Now, lstMulti)
    '                End If
    '                ShowMsg(i)
    '            End If
    '            If radbutBeginClass.Checked Then
    '                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
    '                                      -1, intSession, c_ClassIn)
    '            ElseIf radbutEndClass.Checked Then
    '                objCsol.AddPunchRec(strStuId, Now, intSubClass, intContent, _
    '                                      -1, intSession, c_ClassOut)
    '            End If
    '        Else
    '            MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
    '            tboxID.Clear()
    '            tboxCardNum.Clear()
    '        End If
    '        'blManual = False

    '        tboxID.ReadOnly = True
    '    End If

    'End Sub

    Private Sub timerUsb_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerUsb.Tick
        If intWaitUsb < 1 Then
            intWaitUsb = intWaitUsb + 1
        Else
            timerUsb.Stop()
            intWaitUsb = 0
            If tboxCardNum.Text.Length > 5 Then
                UpdateInforUsb()
            End If
        End If

    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        cboxClass.Items.Clear()
        lstClass = New ArrayList
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If Not lstClass.Contains(dtClass.Rows(index).Item(c_IDColumnName)) Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) > Now Then
                    If Not lstClass.Contains(dtClass.Rows(index).Item(c_IDColumnName)) Then
                        cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                End If
            Next
        End If
        If lstClass.Contains(c_IndividualClassID) = False Then
            cboxClass.Items.Add(My.Resources.idvClassName)
            lstClass.Add(c_IndividualClassID)
        End If
    End Sub

    Private Sub updatePunchInfo()
        intSubClass = lstSubClass(cboxSubClass.SelectedIndex)
        intClass = lstClass(cboxClass.SelectedIndex)
        If cboxContent.SelectedIndex > -1 Then
            intContent = lstContent(cboxContent.SelectedIndex)
        Else
            intContent = -1
        End If
        lstPunchedStu = getPunchedStu()
    End Sub

    Private Function getPunchedStu() As List(Of String)
        Dim al As New List(Of String)
        al = objCsol.GetPunchRecByContent(intSubClass, intContent)
        'objCsol.GetPunchData()
        Return al
    End Function

End Class