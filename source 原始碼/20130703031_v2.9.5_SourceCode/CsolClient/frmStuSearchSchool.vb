﻿Public Class frmStuSearchSchool
    Private dtSch As New DataTable
    Private lstSch As New ArrayList
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Dim intType As Integer = 0
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuSearchSchool
    Private sUserName As String = frmMain.GetUsrName
    Private rowNum As Integer

    Public Sub New()
        InitializeComponent()

        RefreshData()
        InitSchList()
        InitSelections()
        Me.Text = My.Resources.frmStuSearchSchool
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuSearchSchool_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuSearchSchool_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub InitSchList()
        cboxSch.Text = ""
        If radbutPri.Checked Then
            intType = 1
        ElseIf radbutJun.Checked Then
            intType = 2
        ElseIf radbutHig.Checked Then
            intType = 3
        ElseIf radbutUni.Checked Then
            intType = 4
        End If
        cboxSch.Items.Clear()
        lstSch.Clear()

        For index As Integer = 0 To dtSch.Rows.Count - 1
            If dtSch.Rows(index).Item(c_TypeIdColumnName) = intType Then
                cboxSch.Items.Add(dtSch.Rows(index).Item(c_NameColumnName))
                lstSch.Add(dtSch.Rows(index).Item(c_IDColumnName))
            End If
        Next
    End Sub

    Friend Sub RefreshData()
        dtSch = frmMain.GetSchList
        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                    c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)
        
        cboxClass.Items.Add(My.Resources.all)
        lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        cboxClass.SelectedIndex = -1
    End Sub

    Friend Sub RefreshClassList()
        If dtClass.Rows.Count = 0 Then
            Exit Sub
        End If
        cboxClass.Items.Clear()
        lstClass.Clear()
        cboxClass.Items.Add(My.Resources.all)
        lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub RefreshTable()
        Dim strFilter1 As String = ""
        Dim strFilter2 As String = ""
        Dim strFilter3 As String = ""
        Dim strFilter4 As String = ""
        If cboxGrade.SelectedIndex > 0 Then
            strFilter1 = c_SchoolGradeColumnName & "=" & CInt(cboxGrade.Text).ToString
        End If
        If Not tboxClass.Text.Trim = "" Then
            strFilter2 = c_SchoolClassColumnName & "='" & tboxClass.Text.Trim & "'"
        End If

        If radbutClass.Checked Then
            If cboxClass.SelectedIndex > 0 Then
                If cboxSubClass.SelectedIndex > 0 Then
                    strFilter3 = c_SubClassIDColumnName & "=" & lstSubClass(cboxSubClass.SelectedIndex).ToString
                Else
                    strFilter3 = c_ClassIDColumnName & "=" & lstClass(cboxClass.SelectedIndex).ToString
                End If
            End If
            If Not chkboxShowCancel.Checked Then
                strFilter4 = c_CancelColumnName & "=0"
            End If
        ElseIf radbutType.Checked Then
            If chkboxInClass.Checked And Not chkboxNotInClass.Checked Then
                strFilter3 = c_SubClassIDColumnName & " is not Null"
            ElseIf Not chkboxInClass.Checked And chkboxNotInClass.Checked Then
                strFilter3 = c_SubClassIDColumnName & " is Null"
            End If
        End If

        Dim s As String = ""
        If Not strFilter1 = "" Then
            s = strFilter1
        End If

        If Not s = "" And Not strFilter2 = "" Then
            s = s & " AND " & strFilter2
        ElseIf s = "" Then
            s = strFilter2
        End If

        If Not s = "" And Not strFilter3 = "" Then
            s = s & " AND " & strFilter3
        ElseIf s = "" Then
            s = strFilter3
        End If

        If Not s = "" And Not strFilter4 = "" Then
            s = s & " AND " & strFilter4
        ElseIf s = "" Then
            s = strFilter4
        End If

        dtStu.DefaultView.RowFilter = s
        dgv.DataSource = dtStu.DefaultView
        rowNum = dtStu.DefaultView.Count
        tboxCount.Text = "1/" + rowNum.ToString
        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_Tel1ColumnName).Visible = True
            dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgv.Columns.Item(c_SchoolGradeColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_SchoolGradeColumnName).Visible = True
            dgv.Columns.Item(c_SchoolGradeColumnName).HeaderText = My.Resources.schoolGrade
            dgv.Columns.Item(c_SchoolClassColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_SchoolClassColumnName).Visible = True
            dgv.Columns.Item(c_SchoolClassColumnName).HeaderText = My.Resources.schoolClass
            dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgv.Columns.Item(c_ClassNameColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_ClassNameColumnName).Visible = True
            dgv.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className

            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgv.Columns.Item(lstColName(index)).Visible = True
                    dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next

            For j As Integer = 0 To dgv.Rows.Count - 1                                          '100225 
                Dim str As String = dgv.Rows(j).Cells("CardNum").Value.ToString.Trim

                If str = "" Then
                    dgv.Rows(j).Cells("CardNum").Value = "否"
                ElseIf str = "否" Then
                    dgv.Rows(j).Cells("CardNum").Value = "否"
                Else
                    dgv.Rows(j).Cells("CardNum").Value = "是"
                End If
            Next                                                                                '100225

            If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
        End If
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CardNumColumnName)
        'lstColName.Add(c_SchoolColumnName)
        'lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        lstColName.Add("ComeInfo")
        lstColName.Add("ClassNum")
        lstColName.Add("Accommodation")
        lstColName.Add("EnrollSch")
        lstColName.Add("PunchCardNum")
        lstColName.Add("HseeMark")
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub frmStuSearchSchool_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub radbutPri_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutPri.CheckedChanged
        If radbutPri.Checked Then
            InitSchList()
        End If
    End Sub

    Private Sub radbutJun_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutJun.CheckedChanged
        If radbutJun.Checked Then
            InitSchList()
        End If
    End Sub

    Private Sub radbutHig_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutHig.CheckedChanged
        If radbutHig.Checked Then
            InitSchList()
        End If
    End Sub

    Private Sub radbutUni_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutUni.CheckedChanged
        If radbutUni.Checked Then
            InitSchList()
        End If
    End Sub

    Private Sub butFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butFilter.Click
        If cboxSch.SelectedIndex > -1 Then
            Dim i As Integer = lstSch(cboxSch.SelectedIndex)
            If intType > 0 And intType < 5 Then
                dtStu = objCsol.ListStuBySch(i, intType)
                For Each row As DataRow In dtStu.Rows
                    'With row
                    '    If .Item("SchGroup").ToString = 0 Then
                    '        .Item("SchGroup") = 1
                    '    ElseIf .Item("SchGroup").ToString = 1 Then
                    '        .Item("SchGroup") = 2
                    '    ElseIf .Item("SchGroup").ToString = 2 Then
                    '        .Item("SchGroup") = 3
                    '    ElseIf .Item("SchGroup").ToString = 3 Then
                    '        .Item("SchGroup") = 4
                    '    End If
                    'End With

                    Dim key_Group As String = row.Item("Schgroup").ToString.Trim()
                    Dim key_Current As String = row.Item("CurrentSch").ToString.Trim()
                    Dim key_Grade As String = row.Item("SchoolGrade").ToString.Trim()
                    Select Case key_Group
                        Case "-1"
                            row.Item("Schgroup") = ""
                        Case "0"
                            row.Item("Schgroup") = 1
                        Case "1"
                            row.Item("Schgroup") = 2
                        Case "2"
                            row.Item("Schgroup") = 3
                        Case "3"
                            row.Item("Schgroup") = 4
                        Case Else
                    End Select

                    Select Case key_Current
                        Case "0"
                            'row.Item("SchoolGrade") = 1
                            Continue For
                        Case "1"
                            Select Case key_Grade
                                Case "0"
                                    row.Item("SchoolGrade") = 1
                                    Continue For
                                Case "1"
                                    row.Item("SchoolGrade") = 2
                                    Continue For
                                Case "2"
                                    row.Item("SchoolGrade") = 3
                                    Continue For
                                Case "3"
                                    row.Item("SchoolGrade") = 4
                                    Continue For
                                Case "4"
                                    row.Item("SchoolGrade") = 5
                                    Continue For
                                Case "5"
                                    row.Item("SchoolGrade") = 6
                                    Continue For
                                Case Else
                                    row.Item("SchoolGrade") = -1
                                    Continue For
                            End Select
                        Case "2"
                            Select Case key_Grade
                                Case "0"
                                    row.Item("SchoolGrade") = 1
                                    Continue For
                                Case "1"
                                    row.Item("SchoolGrade") = 2
                                    Continue For
                                Case "2"
                                    row.Item("SchoolGrade") = 3
                                    Continue For
                                Case Else
                                    row.Item("SchoolGrade") = -1
                                    Continue For
                            End Select
                        Case "3"
                            Select Case key_Grade
                                Case "0"
                                    row.Item("SchoolGrade") = 1
                                    Continue For
                                Case "1"
                                    row.Item("SchoolGrade") = 2
                                    Continue For
                                Case "2"
                                    row.Item("SchoolGrade") = 3
                                    Continue For
                                Case Else
                                    row.Item("SchoolGrade") = -1
                                    Continue For
                            End Select
                        Case "4"
                            Select Case key_Grade
                                Case "0"
                                    row.Item("SchoolGrade") = 1
                                    Continue For
                                Case "1"
                                    row.Item("SchoolGrade") = 2
                                    Continue For
                                Case "2"
                                    row.Item("SchoolGrade") = 3
                                    Continue For
                                Case "3"
                                    row.Item("SchoolGrade") = 4
                                    Continue For
                                Case Else
                                    row.Item("SchoolGrade") = -1
                                    Continue For
                            End Select
                        Case Else
                            Continue For
                    End Select
                Next
                RefreshTable()
            Else
                MessageBox.Show(My.Resources.msgStuSearchSchError1, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
            End If
        Else
            MessageBox.Show(My.Resources.msgStuSearchSchError1, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End If
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > 0 Then
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = 0
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(1)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(64) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim Answer As MsgBoxResult = MsgBox("請問真的確定要刪除該學生資料??", MsgBoxStyle.YesNo, "警告!!")
        Select Case Answer
            Case MsgBoxResult.Yes
                If dgv.SelectedRows.Count > 0 Then
                    Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                    If id.Length = 8 Then
                        objCsol.DeleteStudent(id)
                        DeleteStu(id)
                    End If
                End If
            Case MsgBoxResult.No
                Exit Sub
            Case Else
                Exit Sub
        End Select
    End Sub

    Private Sub DeleteStu(ByVal id As String)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(index).Item(c_IDColumnName) = id Then
                dtStu.Rows.RemoveAt(index)
            End If
        Next
        RefreshTable()
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 4 Then
            Dim lst As New ArrayList
            For index As Integer = 5 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub mnuExportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        If frmMain.CheckAuth(12) Then
            ExportDgvToExcel(dgv)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassList()
    End Sub

    Private Sub chkboxShowCancel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowCancel.CheckedChanged
        'dgv.DataSource = Nothing
        'dgv.Rows.Clear()
        'dgv.Columns.Clear()
    End Sub

    Private Sub mnuNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNote.Click
        Dim frm As New frm2AddNote
        frm.ShowDialog()
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick

        Dim currentRow As Integer = dgv.CurrentRow.Index + 1
        tboxCount.Text = currentRow.ToString + "/" + rowNum.ToString

    End Sub

End Class