﻿Public Class frm2UpdJournalRec
    Private dtGrp As New DataTable
    Private dtRec As New DataTable
    Private lstId As New ArrayList
    Private intId As Integer

    Public Sub New(ByVal dt As DataTable)

        InitializeComponent()
        dtRec = dt

        RefreshData()

    End Sub

    Private Sub RefreshData()
        dtGrp = frmMain.GetJournalGrp
        For index As Integer = 0 To dtGrp.Rows.Count - 1
            cboxGrp.Items.Add(dtGrp.Rows(index).Item(c_NameColumnName).trim)
            lstId.Add(dtGrp.Rows(index).Item(c_IDColumnName))
        Next

        If dtRec.Rows.Count > 0 Then
            intId = dtRec.Rows(0).Item(c_IDColumnName)
            If cboxGrp.Items.Count > 0 Then
                cboxGrp.SelectedIndex = lstId.IndexOf(dtRec.Rows(0).Item(c_GroupIDColumnName))
            End If

            tboxItem.Text = dtRec.Rows(0).Item("項目").trim
            tboxAmount.Text = dtRec.Rows(0).Item("金額").ToString
            dtpickDate.Value = dtRec.Rows(0).Item("日期時間")
            tboxNum.Text = dtRec.Rows(0).Item("序號").trim
            tboxCheque.Text = dtRec.Rows(0).Item("支票號碼").trim
            tboxRemark3.Text = dtRec.Rows(0).Item("備註三").trim
            tboxRemark4.Text = dtRec.Rows(0).Item("備註四").trim
            tboxRemark5.Text = dtRec.Rows(0).Item("備註五").ToString.Trim
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        frmMain.SetOkState(False)
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If cboxGrp.SelectedIndex > -1 Then
            If IsNumeric(tboxAmount.Text) Then
                If CInt(tboxAmount.Text) > 0 Then
                    If intId > -1 Then
                        objCsol.UpdJournalRec(intId, lstId(cboxGrp.SelectedIndex), _
                                              tboxItem.Text, CInt(tboxAmount.Text), _
                                              dtpickDate.Value, frmMain.GetUsrId, _
                                              tboxNum.Text, tboxCheque.Text, _
                                          tboxRemark3.Text, tboxRemark4.Text, _
                                          tboxRemark5.Text)
                        frmMain.SetOkState(True)
                        Me.Close()
                    End If
                Else
                    ShowMsg()
                End If
            Else
                ShowMsg()
            End If
        Else
            ShowMsg()
        End If

    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgWrongJournalFormat, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class