﻿Public Class frm2ChangeClass
    Private lstScId As New ArrayList
    Private lstScName As New ArrayList
    Private strStuId As String
    Private strSeat As String = ""
    Private intSubClassId As Integer
    Private intOldSubClassId As Integer
    Private strOldSeat As String = ""
    Private lstStuSubClass As New ArrayList

    Public Sub New(ByVal id As String, ByVal oldSc As Integer, _
                   ByVal oldseat As String, ByVal lstName As ArrayList, _
                   ByVal lstId As ArrayList)
        InitializeComponent()

        strStuId = id
        intOldSubClassId = oldSc
        strOldSeat = oldseat
        lstScId = lstId
        lstScName = lstName
        lstStuSubClass = frmMain.GetlstStuSubClass

        ShowClassList()
    End Sub

    Private Sub ShowClassList()
        For index As Integer = 0 To lstScName.Count - 1
            lstboxSubClass.Items.Add(lstScName(index))
        Next
        Dim i As Integer = lstScId.IndexOf(intOldSubClassId)
        If i > -1 Then
            lstboxSubClass.SelectedIndex = i
        Else
            If lstScName.Count > 0 Then
                lstboxSubClass.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click

        If lstboxSubClass.Items.Count > 0 And strStuId.Length = 8 Then


            If lstboxSubClass.SelectedIndex > -1 Then
                intSubClassId = lstScId(lstboxSubClass.SelectedIndex)

                For j As Integer = 0 To lstStuSubClass.Count - 1
                    If intSubClassId = lstStuSubClass(j) Then
                        MessageBox.Show("不可轉入已註冊的班級，請重新選擇。", "警告", MessageBoxButtons.OK)
                        Exit Sub
                    End If
                Next


                Dim subfrmSeatMap As New frm2SeatMap(intSubClassId)
                subfrmSeatMap.ShowDialog()
                strSeat = frmMain.GetSeatNum

                Dim ds As DataSet = objCsol.getTransSubClassInfo(intOldSubClassId, strStuId)

                Dim dtTransPayRec As DataTable = ds.Tables("TransPayRec")
                Dim dtTransFeeDiscount As DataTable = ds.Tables("TransFeeDiscount")
                Dim dtTransKeepPay As DataTable = ds.Tables("TransKeepPay")
                Dim dtTransMoneyBack As DataTable = ds.Tables("TransMoneyBack")
                If dtTransPayRec.Rows.Count > 0 Then
                    For i As Integer = 0 To dtTransPayRec.Rows.Count - 1
                        objCsol.UpdateTransPayRec(CInt(dtTransPayRec.Rows(i).Item("ID")), _
                                                  dtTransPayRec.Rows(i).Item("ReceiptNum").ToString, _
                                                   dtTransPayRec.Rows(i).Item("StuID").ToString, _
                                                   intSubClassId)
                    Next
                End If
                If dtTransFeeDiscount.Rows.Count > 0 Then
                    'For i As Integer = 0 To dtTransFeeDiscount.Rows.Count - 1
                    objCsol.UpdateTransFeeDiscount(CInt(dtTransFeeDiscount.Rows(0).Item("ID")), _
                                                   dtTransFeeDiscount.Rows(0).Item("StuID").ToString, _
                                                    intSubClassId)
                    ' Next
                End If
                If dtTransKeepPay.Rows.Count > 0 Then
                    For i As Integer = 0 To dtTransKeepPay.Rows.Count - 1
                        objCsol.UpdateTransKeepPay(CInt(dtTransKeepPay.Rows(i).Item("KPClassRecID")), _
                                                    dtTransKeepPay.Rows(i).Item("StuID").ToString, _
                                                   intSubClassId)

                    Next
                End If
                If dtTransMoneyBack.Rows.Count > 0 Then
                    For i As Integer = 0 To dtTransMoneyBack.Rows.Count - 1
                        objCsol.UpdateTransMoneyBack(CInt(dtTransMoneyBack.Rows(i).Item("MBClassInfoID")), _
                                                     dtTransMoneyBack.Rows(i).Item("StuID").ToString, _
                                                     intSubClassId)
                    Next
                End If


                objCsol.ChangeStuClass(intSubClassId, intOldSubClassId, strSeat, strOldSeat, strStuId)



                Me.Close()
            End If
        End If

    End Sub
End Class