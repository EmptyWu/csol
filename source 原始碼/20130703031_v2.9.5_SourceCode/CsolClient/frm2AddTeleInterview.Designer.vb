﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddTeleInterview
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.tboxContent = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboxType = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.butAddType = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(337, 304)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 8
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(230, 304)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(82, 25)
        Me.butSave.TabIndex = 7
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(56, 100)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(165, 22)
        Me.dtpickDate.TabIndex = 5
        '
        'tboxContent
        '
        Me.tboxContent.Location = New System.Drawing.Point(56, 129)
        Me.tboxContent.MaxLength = 255
        Me.tboxContent.Multiline = True
        Me.tboxContent.Name = "tboxContent"
        Me.tboxContent.Size = New System.Drawing.Size(363, 149)
        Me.tboxContent.TabIndex = 6
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(12, 129)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 120
        Me.Label4.Text = "內容: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(12, 105)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 119
        Me.Label3.Text = "日期: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxName
        '
        Me.tboxName.Location = New System.Drawing.Point(56, 45)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.Size = New System.Drawing.Size(165, 22)
        Me.tboxName.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(11, 74)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 117
        Me.Label2.Text = "類別: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxID
        '
        Me.tboxID.Location = New System.Drawing.Point(56, 12)
        Me.tboxID.Name = "tboxID"
        Me.tboxID.Size = New System.Drawing.Size(165, 22)
        Me.tboxID.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(12, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 115
        Me.Label1.Text = "姓名: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxType
        '
        Me.cboxType.FormattingEnabled = True
        Me.cboxType.Location = New System.Drawing.Point(56, 74)
        Me.cboxType.Name = "cboxType"
        Me.cboxType.Size = New System.Drawing.Size(165, 20)
        Me.cboxType.TabIndex = 3
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(12, 18)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 12)
        Me.Label11.TabIndex = 113
        Me.Label11.Text = "學號: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butAddType
        '
        Me.butAddType.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAddType.Location = New System.Drawing.Point(230, 71)
        Me.butAddType.Name = "butAddType"
        Me.butAddType.Size = New System.Drawing.Size(82, 25)
        Me.butAddType.TabIndex = 4
        Me.butAddType.Text = "新增類別"
        Me.butAddType.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(54, 281)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(192, 12)
        Me.Label5.TabIndex = 134
        Me.Label5.Text = "最長255個中文字，Enter算兩個字。"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frm2AddTeleInterview
        '
        Me.AcceptButton = Me.butSave
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(431, 339)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.butAddType)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxContent)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboxType)
        Me.Controls.Add(Me.Label11)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddTeleInterview"
        Me.Text = "輸入電訪"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxContent As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboxType As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents butAddType As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
End Class
