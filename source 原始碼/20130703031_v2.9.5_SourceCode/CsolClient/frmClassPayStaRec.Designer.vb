﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClassPayStaRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClassPayStaRec))
        Me.dgvKeep = New System.Windows.Forms.DataGridView
        Me.Label10 = New System.Windows.Forms.Label
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvBack = New System.Windows.Forms.DataGridView
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.butListSta = New System.Windows.Forms.Button
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvPay = New System.Windows.Forms.DataGridView
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.tboxKeepPayAmount = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.tboxBackAmount = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxBackPayAmount = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxPayAmount = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.radbutOne = New System.Windows.Forms.RadioButton
        Me.radbutMulti = New System.Windows.Forms.RadioButton
        Me.tboxKeepAmount = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tboxTotal = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutNoDate = New System.Windows.Forms.RadioButton
        Me.radbutCustDate = New System.Windows.Forms.RadioButton
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvKeep, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvBack, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvKeep
        '
        Me.dgvKeep.AllowUserToAddRows = False
        Me.dgvKeep.AllowUserToDeleteRows = False
        Me.dgvKeep.AllowUserToResizeRows = False
        Me.dgvKeep.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKeep.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvKeep, "dgvKeep")
        Me.dgvKeep.MultiSelect = False
        Me.dgvKeep.Name = "dgvKeep"
        Me.dgvKeep.ReadOnly = True
        Me.dgvKeep.RowHeadersVisible = False
        Me.dgvKeep.RowTemplate.Height = 15
        Me.dgvKeep.RowTemplate.ReadOnly = True
        Me.dgvKeep.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvKeep.ShowCellToolTips = False
        Me.dgvKeep.ShowEditingIcon = False
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvBack)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'dgvBack
        '
        Me.dgvBack.AllowUserToAddRows = False
        Me.dgvBack.AllowUserToDeleteRows = False
        Me.dgvBack.AllowUserToResizeRows = False
        Me.dgvBack.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvBack.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvBack, "dgvBack")
        Me.dgvBack.MultiSelect = False
        Me.dgvBack.Name = "dgvBack"
        Me.dgvBack.ReadOnly = True
        Me.dgvBack.RowHeadersVisible = False
        Me.dgvBack.RowTemplate.Height = 15
        Me.dgvBack.RowTemplate.ReadOnly = True
        Me.dgvBack.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBack.ShowCellToolTips = False
        Me.dgvBack.ShowEditingIcon = False
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvKeep)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvPay)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'dgvPay
        '
        Me.dgvPay.AllowUserToAddRows = False
        Me.dgvPay.AllowUserToDeleteRows = False
        Me.dgvPay.AllowUserToResizeRows = False
        Me.dgvPay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvPay, "dgvPay")
        Me.dgvPay.MultiSelect = False
        Me.dgvPay.Name = "dgvPay"
        Me.dgvPay.ReadOnly = True
        Me.dgvPay.RowHeadersVisible = False
        Me.dgvPay.RowTemplate.Height = 24
        Me.dgvPay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSelectCol, Me.mnuDelete, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'tboxKeepPayAmount
        '
        resources.ApplyResources(Me.tboxKeepPayAmount, "tboxKeepPayAmount")
        Me.tboxKeepPayAmount.Name = "tboxKeepPayAmount"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'tboxBackAmount
        '
        resources.ApplyResources(Me.tboxBackAmount, "tboxBackAmount")
        Me.tboxBackAmount.Name = "tboxBackAmount"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tboxBackPayAmount
        '
        resources.ApplyResources(Me.tboxBackPayAmount, "tboxBackPayAmount")
        Me.tboxBackPayAmount.Name = "tboxBackPayAmount"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxPayAmount
        '
        resources.ApplyResources(Me.tboxPayAmount, "tboxPayAmount")
        Me.tboxPayAmount.Name = "tboxPayAmount"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'radbutOne
        '
        resources.ApplyResources(Me.radbutOne, "radbutOne")
        Me.radbutOne.Checked = True
        Me.radbutOne.Name = "radbutOne"
        Me.radbutOne.TabStop = True
        Me.radbutOne.UseVisualStyleBackColor = True
        '
        'radbutMulti
        '
        resources.ApplyResources(Me.radbutMulti, "radbutMulti")
        Me.radbutMulti.Name = "radbutMulti"
        Me.radbutMulti.UseVisualStyleBackColor = True
        '
        'tboxKeepAmount
        '
        resources.ApplyResources(Me.tboxKeepAmount, "tboxKeepAmount")
        Me.tboxKeepAmount.Name = "tboxKeepAmount"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'tboxTotal
        '
        resources.ApplyResources(Me.tboxTotal, "tboxTotal")
        Me.tboxTotal.Name = "tboxTotal"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutNoDate)
        Me.GroupBox3.Controls.Add(Me.radbutCustDate)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'radbutNoDate
        '
        resources.ApplyResources(Me.radbutNoDate, "radbutNoDate")
        Me.radbutNoDate.Checked = True
        Me.radbutNoDate.Name = "radbutNoDate"
        Me.radbutNoDate.TabStop = True
        Me.radbutNoDate.UseVisualStyleBackColor = True
        '
        'radbutCustDate
        '
        resources.ApplyResources(Me.radbutCustDate, "radbutCustDate")
        Me.radbutCustDate.Name = "radbutCustDate"
        Me.radbutCustDate.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'frmClassPayStaRec
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.tboxTotal)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxKeepAmount)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.radbutMulti)
        Me.Controls.Add(Me.radbutOne)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.tboxKeepPayAmount)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tboxBackAmount)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tboxBackPayAmount)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tboxPayAmount)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.Label11)
        Me.Name = "frmClassPayStaRec"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvKeep, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvBack, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvPay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvKeep As System.Windows.Forms.DataGridView
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvBack As System.Windows.Forms.DataGridView
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents tboxKeepPayAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tboxBackAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxBackPayAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxPayAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents radbutOne As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMulti As System.Windows.Forms.RadioButton
    Friend WithEvents dgvPay As System.Windows.Forms.DataGridView
    Friend WithEvents tboxKeepAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tboxTotal As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutNoDate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCustDate As System.Windows.Forms.RadioButton
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
