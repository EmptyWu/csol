﻿Public Class frm2ChangeStuID
    Dim strNewId As String = ""
    Dim strOldId As String = ""
    Dim blSaved As Boolean = False

    Public Sub New(ByVal id As String)

        InitializeComponent()
        strOldId = id
        frmMain.SetOkState(False)
    End Sub

    Private Sub tboxYear_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxYear.TextChanged
        If radbutNewYear.Checked Then
            If IsNumeric(tboxYear.Text) Then
                Dim intY As Integer = CInt(tboxYear.Text)
                If intY < GetROCYear(Now) + c_StuYearBoundary And _
                    intY > GetROCYear(Now) - c_StuYearBoundary Then
                    If blSaved = False Then
                        blSaved = True
                    Else
                        objCsol.DeleteStudent(strNewId)
                    End If
                    strNewId = objCsol.GenStuId(tboxYear.Text)
                    tboxID.Text = strNewId
                End If
            End If
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        If blSaved And strNewId.Length = 8 Then
            objCsol.DeleteStudent(strNewId)
        End If
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim result As Integer = 0
        If strOldId.Length = 8 Then
            If radbutNewYear.Checked Then
                If blSaved Then
                    objCsol.DeleteStudent(strNewId)
                    blSaved = False
                    result = objCsol.ChangeStuID(strOldId, strNewId, _
                                                 frmMain.GetUsrId)
                Else
                    ShowMsg(My.Resources.msgWrongNewID)
                End If
            ElseIf radbutID.Checked Then
                If tboxID.Text.Trim.Length = 8 Then
                    If blSaved Then
                        objCsol.DeleteStudent(strNewId)
                        blSaved = False
                        result = objCsol.ChangeStuID(strOldId, tboxID.Text.Trim, _
                                                 frmMain.GetUsrId)
                    End If
                Else
                    ShowMsg(My.Resources.msgWrongNewID)
                End If
            End If
            If result = -1 Then
                ShowMsg(My.Resources.msgIDTaken)
            ElseIf result = 0 Then
                ShowMsg(My.Resources.msgChangeStuIDNoOldID)
            ElseIf result = 1 Then
                If radbutID.Checked Then
                    strNewId = tboxID.Text.Trim
                End If
                frmMain.SetCurrentStu(strNewId)
                frmMain.SetOkState(True)
                ShowMsg(My.Resources.msgModSuccess)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        MsgBox(msg, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Sub frm2ChangeStuID_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blSaved Then
            If strNewId.Length = 8 Then
                objCsol.DeleteStudent(strNewId)
            End If
        End If
    End Sub
End Class