﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2UpdateReceipt
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2UpdateReceipt))
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.tboxReceipt = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxExtra = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxAmount = New System.Windows.Forms.TextBox
        Me.tboxReason = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboxPayMethod = New System.Windows.Forms.ComboBox
        Me.cboxHandler = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'tboxReceipt
        '
        resources.ApplyResources(Me.tboxReceipt, "tboxReceipt")
        Me.tboxReceipt.Name = "tboxReceipt"
        Me.tboxReceipt.ReadOnly = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxExtra
        '
        resources.ApplyResources(Me.tboxExtra, "tboxExtra")
        Me.tboxExtra.Name = "tboxExtra"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxAmount
        '
        resources.ApplyResources(Me.tboxAmount, "tboxAmount")
        Me.tboxAmount.Name = "tboxAmount"
        '
        'tboxReason
        '
        resources.ApplyResources(Me.tboxReason, "tboxReason")
        Me.tboxReason.Name = "tboxReason"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'cboxPayMethod
        '
        Me.cboxPayMethod.FormattingEnabled = True
        resources.ApplyResources(Me.cboxPayMethod, "cboxPayMethod")
        Me.cboxPayMethod.Name = "cboxPayMethod"
        '
        'cboxHandler
        '
        Me.cboxHandler.FormattingEnabled = True
        resources.ApplyResources(Me.cboxHandler, "cboxHandler")
        Me.cboxHandler.Name = "cboxHandler"
        '
        'frm2UpdateReceipt
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.cboxHandler)
        Me.Controls.Add(Me.cboxPayMethod)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tboxReason)
        Me.Controls.Add(Me.tboxAmount)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxExtra)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.tboxReceipt)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frm2UpdateReceipt"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents tboxReceipt As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxExtra As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxAmount As System.Windows.Forms.TextBox
    Friend WithEvents tboxReason As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboxPayMethod As System.Windows.Forms.ComboBox
    Friend WithEvents cboxHandler As System.Windows.Forms.ComboBox
End Class
