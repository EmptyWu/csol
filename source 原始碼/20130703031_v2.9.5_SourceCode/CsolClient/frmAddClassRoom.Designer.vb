﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddClassRoom
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddClassRoom))
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tboxColCnt = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuInsertPw = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRemovePw = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetToNoSeat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCancelNoSeat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetConvex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuCancelConvex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetDisStyle = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.tboxRowCnt = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.dgvSeat = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'tboxName
        '
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'tboxColCnt
        '
        resources.ApplyResources(Me.tboxColCnt, "tboxColCnt")
        Me.tboxColCnt.Name = "tboxColCnt"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.mnuInsertPw, Me.mnuRemovePw, Me.mnuSetToNoSeat, Me.mnuCancelNoSeat, Me.mnuSetConvex, Me.mnuCancelConvex, Me.mnuSetDisStyle, Me.mnuPrint, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        resources.ApplyResources(Me.mnuSave, "mnuSave")
        '
        'mnuInsertPw
        '
        Me.mnuInsertPw.Name = "mnuInsertPw"
        resources.ApplyResources(Me.mnuInsertPw, "mnuInsertPw")
        '
        'mnuRemovePw
        '
        Me.mnuRemovePw.Name = "mnuRemovePw"
        resources.ApplyResources(Me.mnuRemovePw, "mnuRemovePw")
        '
        'mnuSetToNoSeat
        '
        Me.mnuSetToNoSeat.Name = "mnuSetToNoSeat"
        resources.ApplyResources(Me.mnuSetToNoSeat, "mnuSetToNoSeat")
        '
        'mnuCancelNoSeat
        '
        Me.mnuCancelNoSeat.Name = "mnuCancelNoSeat"
        resources.ApplyResources(Me.mnuCancelNoSeat, "mnuCancelNoSeat")
        '
        'mnuSetConvex
        '
        Me.mnuSetConvex.Name = "mnuSetConvex"
        resources.ApplyResources(Me.mnuSetConvex, "mnuSetConvex")
        '
        'mnuCancelConvex
        '
        Me.mnuCancelConvex.Name = "mnuCancelConvex"
        resources.ApplyResources(Me.mnuCancelConvex, "mnuCancelConvex")
        '
        'mnuSetDisStyle
        '
        Me.mnuSetDisStyle.Name = "mnuSetDisStyle"
        resources.ApplyResources(Me.mnuSetDisStyle, "mnuSetDisStyle")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'tboxRowCnt
        '
        resources.ApplyResources(Me.tboxRowCnt, "tboxRowCnt")
        Me.tboxRowCnt.Name = "tboxRowCnt"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'dgvSeat
        '
        Me.dgvSeat.AllowUserToAddRows = False
        Me.dgvSeat.AllowUserToDeleteRows = False
        Me.dgvSeat.AllowUserToResizeColumns = False
        Me.dgvSeat.AllowUserToResizeRows = False
        Me.dgvSeat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSeat.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvSeat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSeat.DefaultCellStyle = DataGridViewCellStyle14
        resources.ApplyResources(Me.dgvSeat, "dgvSeat")
        Me.dgvSeat.MultiSelect = False
        Me.dgvSeat.Name = "dgvSeat"
        Me.dgvSeat.ReadOnly = True
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle15.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSeat.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvSeat.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvSeat.RowTemplate.Height = 24
        Me.dgvSeat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSeat.ShowCellErrors = False
        Me.dgvSeat.ShowCellToolTips = False
        Me.dgvSeat.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'frmAddClassRoom
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.dgvSeat)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxRowCnt)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tboxColCnt)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Name = "frmAddClassRoom"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tboxColCnt As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuInsertPw As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRemovePw As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetToNoSeat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents mnuCancelNoSeat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetConvex As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCancelConvex As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetDisStyle As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxRowCnt As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents dgvSeat As System.Windows.Forms.DataGridView
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
