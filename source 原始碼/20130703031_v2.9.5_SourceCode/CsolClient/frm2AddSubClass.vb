﻿Public Class frm2AddSubClass
    Private intId As Integer
    Private intCrId As Integer = 0
    Private intSubjectId As Integer = 0
    Private intClassId As Integer
    Private dtCr As New DataTable
    Private dtSubjects As New DataTable
    Private lstCr As New ArrayList
    Private lstSubjects As New ArrayList
    Private intSequence As Integer

    Public Sub New(ByVal ID As Integer, ByVal Name As String, _
                   ByVal ClassID As Integer, _
                         ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                         ByVal ClassRoomID As Integer)

        InitializeComponent()

        intSequence = Sequence
        intClassId = ClassID
        intId = ID
        If intId = -1 Then 'add new
            Me.Text = My.Resources.addSubClass

        Else 'update
            intCrId = ClassRoomID
            intSubjectId = SubjectID
            tboxName.Text = Name

            Me.Text = My.Resources.updSubClass
        End If

        RefreshData()
        ShowData()
    End Sub

    Private Sub RefreshData()
        dtCr = frmMain.GetClassRooms
        dtSubjects = frmMain.GetSubjects

        lstCr = New ArrayList
        lstSubjects = New ArrayList
        cboxCr.Items.Clear()
        cboxSubject.Items.Clear()

        For index As Integer = 0 To dtCr.Rows.Count - 1
            cboxCr.Items.Add(dtCr.Rows(index).Item(c_NameColumnName).trim)
            lstCr.Add(dtCr.Rows(index).Item(c_IDColumnName))
        Next

        For index As Integer = 0 To dtSubjects.Rows.Count - 1
            cboxSubject.Items.Add(dtSubjects.Rows(index).Item(c_SubjectColumnName).trim)
            lstSubjects.Add(dtSubjects.Rows(index).Item(c_IDColumnName))
        Next
    End Sub

    Private Sub ShowData()
        If intId = -1 Then
            If cboxCr.Items.Count > 0 Then
                cboxCr.SelectedIndex = 0
            End If

            If cboxSubject.Items.Count > 0 Then
                cboxSubject.SelectedIndex = 0
            End If
        Else
            Dim idx As Integer
            idx = lstCr.IndexOf(intCrId)
            If idx > -1 Then
                cboxCr.SelectedIndex = idx
            End If
            idx = lstSubjects.IndexOf(intSubjectId)
            If idx > -1 Then
                cboxSubject.SelectedIndex = idx
            End If

        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not tboxName.Text.Trim = "" Then
            If cboxCr.SelectedIndex = -1 Then
                intCrId = 0
            Else
                intCrId = lstCr(cboxCr.SelectedIndex)
            End If
            If cboxSubject.SelectedIndex = -1 Then
                intSubjectId = 0
            Else
                intSubjectId = lstSubjects(cboxSubject.SelectedIndex)
            End If

            If intId = -1 Then
                objCsol.AddSubClass(tboxName.Text, intClassId, intSequence, _
                                intSubjectId, intCrId, frmMain.GetUsrId)
            Else
                objCsol.ModifySubClass(intId, tboxName.Text, intClassId, intSequence, _
                                intSubjectId, intCrId)
            End If
            frmMain.SetOkState(True)
            frmMain.RefreshClassRegInfo()
        Else
            ShowMsg()
        End If
        frmMain.BackgroundWorker7.RunWorkerAsync()
        Me.Close()
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

End Class