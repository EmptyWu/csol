﻿Public Class frm2IssueCard
    Dim intType As Integer
    Dim strId As String
    Dim strCardNum As String
    Public Sub New(ByVal type As Integer, ByVal id As String)
        InitializeComponent()
        intType = type
        strId = id
        tboxMsg.Text = My.Resources.msgComportReady
    End Sub
    Private Sub frm2IssueCard_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        tboxCard.Focus()
        tboxCard.ReadOnly = False
    End Sub
    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not tboxCard.Text.Trim = "" Then
            strCardNum = tboxCard.Text.Trim
            If intType = 0 Then 'studeng
                Dim result As Integer = objCsol.ModifyStuCard(strId, strCardNum)
                If result = 0 Then
                    MessageBox.Show(My.Resources.msgSaveComplete, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                ElseIf result = 1 Then
                    MessageBox.Show(My.Resources.msgCardNumExist, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    Exit Sub
                End If
            Else 'employee
                objCsol.ModifyUsrCard(strId, strCardNum)
            End If
            frmMain.SetOkState(True)
            frmMain.SetCurrentString(strCardNum)
            Me.Close()
        End If
    End Sub
    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub tboxCard_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxCard.KeyDown
        If e.KeyCode = Keys.Enter Then
            If Not tboxCard.Text.Trim = "" Then

                strCardNum = tboxCard.Text.Trim
                If intType = 0 Then 'studeng
                    Dim result As Integer = objCsol.ModifyStuCard(strId, strCardNum)
                    If result = 0 Then
                        MessageBox.Show(My.Resources.msgSaveComplete, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                        tboxCard.Clear()
                    ElseIf result = 1 Then
                        MessageBox.Show(My.Resources.msgCardNumExist, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                        tboxCard.Clear()
                        Exit Sub
                    End If
                Else 'employee
                    objCsol.ModifyUsrCard(strId, strCardNum)
                End If
                frmMain.SetOkState(True)
                frmMain.SetCurrentString(strCardNum)
                Me.Close()
            End If
        End If
    End Sub
End Class