﻿Imports System.IO

Public Class frmStuBookIssue
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private dtSubClass As New DataTable
    Private lstSubClass As New ArrayList
    Private dtBook As New DataTable
    Private lstBook As New ArrayList
    Private lstBookShown As New ArrayList
    Private dtStu As New DataTable
    Dim WithEvents myComPort As New System.IO.Ports.SerialPort
    Private strCardNum As String
    Private intClass As Integer
    Private intSubClass As Integer
    Private strStuId As String
    Private intResult As Integer
    Private lstMulti As New ArrayList
    Private dtPay As New DataTable
    Private intWaitUsb As Integer = 0
    Private blManual As Boolean = False
    Private stuIdx As Integer = 0
    Private lstBookResults As ArrayList = New ArrayList
    Private lstBookNames As ArrayList = New ArrayList

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmStuBookIssue
        RefreshData()
        timerClock.Start()
    End Sub

    Private Sub frmStuBookIssue_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuBookIssue_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()

        dtClass = frmMain.GetClassList
        dtSubClass = frmMain.GetSubClassList
        'dtBook = frmMain.GetRefreshBooks()

        cboxClass.Items.Clear()
        lstClass.Clear()
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        lstSubClass.Clear()
        cboxSubClass.Items.Clear()
        'dtBook = frmMain.GetRefreshBooks()

        If cboxClass.SelectedIndex > -1 Then
            Dim ClassId As Integer = lstClass(cboxClass.SelectedIndex)
            lstSubClass.Add(-1)
            cboxSubClass.Items.Add(My.Resources.all)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = ClassId Then
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).ToString.Trim)
                End If
            Next
            If cboxSubClass.Items.Count > 0 Then
                If cboxClass.SelectedIndex > -1 Then
                    dtBook = frmMain.GetRefreshBooks(ClassId)
                End If
                cboxSubClass.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged


        chklstBook.Items.Clear()
        lstBookShown.Clear()
        lstBook.Clear()
        lstMulti.Clear()
        chklstBook.Visible = False
        lblBook.Visible = False
        If cboxSubClass.SelectedIndex = 0 Then
            If cboxClass.SelectedIndex > -1 Then
                Dim i As Integer = lstClass(cboxClass.SelectedIndex)
                Dim t As Integer
                For index As Integer = 0 To dtBook.Rows.Count - 1
                    t = GetIntValue(dtBook.Rows(index).Item(c_ClassIDColumnName))
                    If GetIntValue(dtBook.Rows(index).Item(c_ClassIDColumnName)) = i Then
                        If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                            If lstBookShown.Contains(dtBook.Rows(index).Item(c_IDColumnName)) = False Then
                                lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                                lstMulti.Add(dtBook.Rows(index).Item(c_MultiClassColumnName))
                                chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).ToString.Trim)
                            End If
                        End If
                    End If
                Next
                chklstBook.Visible = True
                lblBook.Visible = True
            End If
        ElseIf cboxSubClass.SelectedIndex > 0 Then
            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            For index As Integer = 0 To dtBook.Rows.Count - 1
                If GetIntValue(dtBook.Rows(index).Item(c_SubClassIDColumnName)) = i Then
                    If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                        If lstBookShown.Contains(dtBook.Rows(index).Item(c_IDColumnName)) = False Then
                            lstBookShown.Add(dtBook.Rows(index).Item(c_IDColumnName))
                            lstMulti.Add(dtBook.Rows(index).Item(c_MultiClassColumnName))
                            chklstBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).ToString.Trim)
                        End If
                    End If
                End If
            Next
            chklstBook.Visible = True
            lblBook.Visible = True
        End If
    End Sub

    Private Sub frmStuBookIssue_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub timerClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerClock.Tick
        lblTime.Text = GetTimeNumNow()
    End Sub

    Private Sub chklstBook_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstBook.ItemCheck
        Dim intC As Integer
        intC = lstBookShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstBook.Add(intC)
        Else
            lstBook.Remove(intC)
        End If
    End Sub

    Private Sub butEnd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEnd.Click

        tboxCardNum.ReadOnly = True

        lblName.Text = My.Resources.bookIssueEnd
    End Sub

    Private Sub butStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butStart.Click
        tboxID.Clear()
        tboxCardNum.Clear()
        tboxName.Clear()
        If lstBook.Count > 0 And cboxClass.SelectedIndex > -1 And cboxSubClass.SelectedIndex > -1 Then
            intClass = lstClass(cboxClass.SelectedIndex)
            intSubClass = lstSubClass(cboxSubClass.SelectedIndex)

            lblName.Text = My.Resources.bookIssueStart
            MsgBox(My.Resources.msgCheckInputLang, MsgBoxStyle.Information)

            tboxCardNum.ReadOnly = False
            tboxCardNum.Focus()

        Else
            MessageBox.Show(My.Resources.msgNoBookSelected, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End If
    End Sub

    Public Sub UpdateInfoUsb()
        strCardNum = tboxCardNum.Text.Trim.ToUpper
        dtStu = objCsol.GetStuRecByCardNumAbsoluteEqual(strCardNum, intClass)
        If dtStu.Rows.Count > 0 Then
            stuIdx = -1
            For idx As Integer = 0 To dtStu.Rows.Count - 1
                If dtStu.Rows(idx).Item(c_CardNumColumnName).ToString.Trim.ToUpper.Equals(strCardNum) Then
                    stuIdx = idx
                End If
            Next
            If stuIdx = -1 Then
                tboxCardNum.Clear()
                MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                Exit Sub
            End If
            strStuId = dtStu.Rows(stuIdx).Item(c_IDColumnName)
            tboxID.Text = strStuId
            tboxName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName)
            lblName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName)
            dtPay = objCsol.ListPayRecByStuId(strStuId)
            CheckIssue()

            If strStuId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                    lblImgInfo.Text = ""
                Else
                    '20100424 imgdb by sherry start
                    Dim result As Byte() = objCsol.DownloadImg(strStuId)
                    If Not result Is Nothing Then
                        If result.Length > 0 Then
                            Dim ms As MemoryStream = New MemoryStream(result, 0, result.Length)
                            Dim im As Image = Image.FromStream(ms)
                            picbox.Image = im

                            Dim bm_source As New Bitmap(picbox.Image)
                            Dim bm_dest As New Bitmap( _
                            CInt(bm_source.Width * 0.25), _
                            CInt(bm_source.Height * 0.25))

                            ' Make a Graphics object for the result Bitmap.
                            Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

                            ' Copy the source image into the destination bitmap.
                            gr_dest.DrawImage(bm_source, 0, 0, _
                                bm_dest.Width + 1, _
                                bm_dest.Height + 1)

                            bm_dest.Save(f, Imaging.ImageFormat.Bmp)
                            lblImgInfo.Text = ""
                        Else
                            picbox.Image = Nothing
                            lblImgInfo.Text = My.Resources.noPhoto
                        End If
                    Else
                        picbox.Image = Nothing
                        lblImgInfo.Text = My.Resources.noPhoto
                    End If
                    '20100424 imgdb by sherry end
                End If
            End If

            ShowMsg()
            tboxDate.Text = Now.ToString
            tboxCardNum.Clear()
            tboxCardNum.Text = ""
            tboxID.Clear()
        Else
            MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
            tboxID.Clear()
            tboxCardNum.Clear()
            tboxCardNum.Text = ""
        End If

    End Sub

    Private Sub ShowMsg()
        Dim strMsg As String = ""
        tboxStatus.Text = ""
        Dim j As Integer = 0

        For i As Integer = 0 To lstBookNames.Count - 1
            j = lstBookResults(i)
            Select Case j
                Case 0
                    strMsg = My.Resources.bookIssueOk
                Case 1
                    strMsg = "失敗:" + My.Resources.msgBookIssue1
                Case 2
                    strMsg = "失敗:" + My.Resources.msgBookIssue2
                Case 3
                    strMsg = "失敗:" + My.Resources.msgBookIssue3
                Case 4
                    strMsg = "失敗:" + My.Resources.msgBookIssue4
                Case 5
                    strMsg = "失敗:" + My.Resources.msgBookIssue5
                Case 6
                    strMsg = My.Resources.bookIssueFail
            End Select

            If i > 0 Then
                tboxStatus.Text = tboxStatus.Text & lstBookNames(i) & "-" & strMsg & vbNewLine
            Else
                tboxStatus.Text = lstBookNames(i) & "-" & strMsg & vbNewLine
                'tboxStatus.ForeColor = System.Drawing.Color.Red
            End If
        Next

    End Sub

    Private Function CheckIssue() As Integer
        Dim r As Integer = 0
        Dim book As Integer
        Dim FeeClear As Byte
        Dim FeeClearDate As Date
        Dim MultiClass As Byte
        Dim CreateDate As Date
        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        Dim c3 As String = c_SubClassIDColumnName & "="
        Dim c4 As String = c_DateTimeColumnName & "> '"
        Dim c5 As String = c_ClassIDColumnName & "="

        'Dim bookList As DataTable = objCsol.GetBookList(lstBook)

        If dtStu.Rows.Count = 0 Then
            r = 5
            Return r
        End If

        Dim stuBookIssue As Boolean = False
        Dim sc As Integer = dtStu.Rows(0).Item(c_SubClassIDColumnName)
        Dim scBook As Integer = -1
        lstBookResults = New ArrayList
        lstBookNames = New ArrayList

        Dim lstTemp As ArrayList

        For index As Integer = 0 To lstBook.Count - 1
            book = lstBook(index)
            Dim idx As Integer = -1
            Dim bookName As String = ""
            For j As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(j).Item(c_IDColumnName) = book Then
                    bookName = dtBook(j).Item(c_NameColumnName)
                    If dtBook.Rows(j).Item(c_SubClassIDColumnName) = sc Then
                        idx = j
                        scBook = dtBook.Rows(j).Item(c_SubClassIDColumnName)
                        Exit For
                    End If

                End If
            Next
            If idx > -1 Then
                FeeClear = dtBook.Rows(idx).Item(c_FeeClearColumnName)
                FeeClearDate = dtBook.Rows(idx).Item(c_FeeClearDateColumnName)
                MultiClass = dtBook.Rows(idx).Item(c_MultiClassColumnName)
                CreateDate = dtBook.Rows(idx).Item(c_CreateDateColumnName)
            Else
                'MsgBox("請確認此學生領取書籍所屬的班級班別 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                lstBookNames.Add(bookName)
                lstBookResults.Add(6)
                Continue For
            End If

            If sc <> scBook Then
                Continue For
            End If


            Select Case FeeClear
                Case 1 '無須繳清可領書
                    'If FeeClearDate.Year <> 1911 Then
                    If intSubClass = -1 Then
                        'If dtPay.Compute(c1, c5 & intClass.ToString & " AND " & c4 & FeeClearDate.ToString & "'") > 0 Then
                        'If dtPay.Compute(String.Format("Count({0})", c_IDColumnName), _
                        '                 String.Format("{0} = {1} And {2} > '{3}'", c_ClassIDColumnName, intClass.ToString(), c_DateTimeColumnName, FeeClearDate.ToString)) = 0 Then
                        '    lstBookNames.Add(bookName)
                        '    lstBookResults.Add(3)
                        '    Continue For
                        'End If

                    ElseIf intSubClass > 0 Then
                        'If dtPay.Compute(c1, c3 & intSubClass.ToString & " AND " & c4 & FeeClearDate.ToString & "'") > 0 Then
                        'If dtPay.Compute(String.Format("Count({0})", c_IDColumnName), _
                        '                 String.Format("{0} = {1} And {2} > '{3}'", c_SubClassIDColumnName, intSubClass.ToString(), c_DateTimeColumnName, FeeClearDate.ToString())) = 0 Then
                        '    lstBookNames.Add(bookName)
                        '    lstBookResults.Add(3)
                        '    Continue For
                        'End If

                    End If
                Case 0 '須繳清可領書
                    If FeeClear = 0 Then
                        If dtStu.Rows(stuIdx).Item(c_FeeOweColumnName) > 0 Then
                            lstBookNames.Add(bookName)
                            lstBookResults.Add(3)
                            Continue For
                        End If
                    End If
                Case Else
            End Select

            lstTemp = New ArrayList
            lstTemp.Add(book)

            r = objCsol.AddBookIssueRec(strStuId, intClass, lstTemp, Now, lstMulti)
            'End If
            lstBookNames.Add(bookName)
            lstBookResults.Add(r)
        Next
        Return r

    End Function

    Private Sub butManualPunch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butManualPunch.Click
        blManual = True
        tboxID.ReadOnly = False
        tboxID.Clear()
        tboxID.Focus()
    End Sub

    Private Sub tboxCardNum_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxCardNum.KeyDown
        If e.KeyCode = Keys.Return Then
            UpdateInfoUsb()
        End If
    End Sub

    Private Sub tboxID_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxID.KeyDown
        If e.KeyCode = Keys.Return Then
            If tboxID.Text.Trim.Length = 8 And blManual Then
                strStuId = tboxID.Text.Trim
                intClass = lstClass(cboxClass.SelectedIndex)
                intSubClass = lstSubClass(cboxSubClass.SelectedIndex)
                Dim stuInfo As DataTable = objCsol.DisplayStudentInfo(strStuId).Tables(c_StuInfoTableName)
                If stuInfo.Rows.Count = 0 Then
                    MessageBox.Show(My.Resources.msgNoSuchStuID, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    Exit Sub
                Else
                    strCardNum = stuInfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim
                End If

                dtStu = objCsol.GetStuRecByID(strStuId, intClass)

                If dtStu.Rows.Count > 0 Then

                    stuIdx = 0
                    tboxName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName)
                    lblName.Text = dtStu.Rows(stuIdx).Item(c_NameColumnName)
                    dtPay = objCsol.ListPayRecByStuId(strStuId)
                    CheckIssue()

                    If strStuId.Length = 8 Then
                        Dim f As String = GetPhotoPath() & strStuId & ".bmp"
                        If CheckFileExist(f) Then
                            Dim bm As New Bitmap(f)
                            picbox.Image = bm
                            lblImgInfo.Text = ""
                        Else
                            '20100424 imgdb by sherry start
                            Dim result As Byte() = objCsol.DownloadImg(strStuId)
                            If Not result Is Nothing Then
                                If result.Length > 0 Then
                                    Dim ms As MemoryStream = New MemoryStream(result, 0, result.Length)
                                    Dim im As Image = Image.FromStream(ms)
                                    picbox.Image = im

                                    Dim bm_source As New Bitmap(picbox.Image)
                                    Dim bm_dest As New Bitmap( _
                                    CInt(bm_source.Width * 0.25), _
                                    CInt(bm_source.Height * 0.25))

                                    ' Make a Graphics object for the result Bitmap.
                                    Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

                                    ' Copy the source image into the destination bitmap.
                                    gr_dest.DrawImage(bm_source, 0, 0, _
                                        bm_dest.Width + 1, _
                                        bm_dest.Height + 1)

                                    bm_dest.Save(f, Imaging.ImageFormat.Bmp)
                                    lblImgInfo.Text = ""
                                Else
                                    picbox.Image = Nothing
                                    lblImgInfo.Text = My.Resources.noPhoto
                                End If
                            Else
                                picbox.Image = Nothing
                                lblImgInfo.Text = My.Resources.noPhoto
                            End If
                            '20100424 imgdb by sherry end
                        End If
                    End If
                    ShowMsg()
                    tboxDate.Text = Now.ToString
                    tboxCardNum.Clear()

                Else
                    MessageBox.Show(My.Resources.msgNoStuRecFound, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    tboxID.Clear()
                    tboxID.Text = ""
                    tboxCardNum.Clear()
                    tboxCardNum.Text = ""
                End If
                tboxID.Text = ""
            Else
                MsgBox("請輸入正確學號以及確認是否有點選手動輸入學號 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                tboxID.Text = ""
                tboxID.Clear()
            End If
        End If
    End Sub

    Private Sub timerUsb_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles timerUsb.Tick
        If intWaitUsb < 1 Then
            intWaitUsb = intWaitUsb + 1
        Else
            timerUsb.Stop()
            intWaitUsb = 0
            If tboxCardNum.Text.Length > 5 Then
                UpdateInfoUsb()
            End If
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        lstClass.Clear()
        cboxClass.Items.Clear()
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) > Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        cboxClass.Text = ""
        cboxSubClass.Text = ""
    End Sub

End Class