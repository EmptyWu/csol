﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClassContRate
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClassContRate))
        Me.chkboxShowCancel = New System.Windows.Forms.CheckBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tboxGrowRate = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxContRate = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.dgvContNo = New System.Windows.Forms.DataGridView
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cboxSubClass2 = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.cboxClass2 = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.radbutShowValidInHalfYear = New System.Windows.Forms.RadioButton
        Me.radbutShowAll = New System.Windows.Forms.RadioButton
        Me.radbutShowValidInOneYear = New System.Windows.Forms.RadioButton
        Me.radbutShowValid = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.dgvCont = New System.Windows.Forms.DataGridView
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.dgvNew = New System.Windows.Forms.DataGridView
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuAnalyze = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument3 = New System.Drawing.Printing.PrintDocument
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvContNo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvCont, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.dgvNew, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkboxShowCancel
        '
        resources.ApplyResources(Me.chkboxShowCancel, "chkboxShowCancel")
        Me.chkboxShowCancel.Name = "chkboxShowCancel"
        Me.chkboxShowCancel.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tboxGrowRate)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.tboxContRate)
        Me.GroupBox2.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'tboxGrowRate
        '
        resources.ApplyResources(Me.tboxGrowRate, "tboxGrowRate")
        Me.tboxGrowRate.Name = "tboxGrowRate"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxContRate
        '
        resources.ApplyResources(Me.tboxContRate, "tboxContRate")
        Me.tboxContRate.Name = "tboxContRate"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'dgvContNo
        '
        Me.dgvContNo.AllowUserToAddRows = False
        Me.dgvContNo.AllowUserToDeleteRows = False
        Me.dgvContNo.AllowUserToResizeRows = False
        Me.dgvContNo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvContNo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvContNo, "dgvContNo")
        Me.dgvContNo.MultiSelect = False
        Me.dgvContNo.Name = "dgvContNo"
        Me.dgvContNo.RowHeadersVisible = False
        Me.dgvContNo.RowTemplate.Height = 24
        Me.dgvContNo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvContNo.ShowCellToolTips = False
        Me.dgvContNo.ShowEditingIcon = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboxSubClass)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Controls.Add(Me.cboxClass)
        Me.GroupBox3.Controls.Add(Me.Label2)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cboxSubClass2)
        Me.GroupBox4.Controls.Add(Me.Label6)
        Me.GroupBox4.Controls.Add(Me.cboxClass2)
        Me.GroupBox4.Controls.Add(Me.Label7)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'cboxSubClass2
        '
        Me.cboxSubClass2.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass2, "cboxSubClass2")
        Me.cboxSubClass2.Name = "cboxSubClass2"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'cboxClass2
        '
        Me.cboxClass2.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass2, "cboxClass2")
        Me.cboxClass2.Name = "cboxClass2"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'radbutShowValidInHalfYear
        '
        resources.ApplyResources(Me.radbutShowValidInHalfYear, "radbutShowValidInHalfYear")
        Me.radbutShowValidInHalfYear.Name = "radbutShowValidInHalfYear"
        Me.radbutShowValidInHalfYear.UseVisualStyleBackColor = True
        '
        'radbutShowAll
        '
        resources.ApplyResources(Me.radbutShowAll, "radbutShowAll")
        Me.radbutShowAll.Name = "radbutShowAll"
        Me.radbutShowAll.UseVisualStyleBackColor = True
        '
        'radbutShowValidInOneYear
        '
        resources.ApplyResources(Me.radbutShowValidInOneYear, "radbutShowValidInOneYear")
        Me.radbutShowValidInOneYear.Name = "radbutShowValidInOneYear"
        Me.radbutShowValidInOneYear.UseVisualStyleBackColor = True
        '
        'radbutShowValid
        '
        resources.ApplyResources(Me.radbutShowValid, "radbutShowValid")
        Me.radbutShowValid.Checked = True
        Me.radbutShowValid.Name = "radbutShowValid"
        Me.radbutShowValid.TabStop = True
        Me.radbutShowValid.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvContNo)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dgvCont)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'dgvCont
        '
        Me.dgvCont.AllowUserToAddRows = False
        Me.dgvCont.AllowUserToDeleteRows = False
        Me.dgvCont.AllowUserToResizeRows = False
        Me.dgvCont.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvCont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvCont, "dgvCont")
        Me.dgvCont.MultiSelect = False
        Me.dgvCont.Name = "dgvCont"
        Me.dgvCont.RowHeadersVisible = False
        Me.dgvCont.RowTemplate.Height = 24
        Me.dgvCont.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCont.ShowCellToolTips = False
        Me.dgvCont.ShowEditingIcon = False
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.dgvNew)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'dgvNew
        '
        Me.dgvNew.AllowUserToAddRows = False
        Me.dgvNew.AllowUserToDeleteRows = False
        Me.dgvNew.AllowUserToResizeRows = False
        Me.dgvNew.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvNew.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvNew, "dgvNew")
        Me.dgvNew.MultiSelect = False
        Me.dgvNew.Name = "dgvNew"
        Me.dgvNew.RowHeadersVisible = False
        Me.dgvNew.RowTemplate.Height = 24
        Me.dgvNew.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvNew.ShowCellToolTips = False
        Me.dgvNew.ShowEditingIcon = False
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAnalyze, Me.mnuPrint, Me.mnuExportData, Me.mnuSelectCol, Me.mnuNote, Me.mnuClose})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuAnalyze
        '
        Me.mnuAnalyze.Name = "mnuAnalyze"
        resources.ApplyResources(Me.mnuAnalyze, "mnuAnalyze")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        resources.ApplyResources(Me.mnuExportData, "mnuExportData")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        resources.ApplyResources(Me.mnuNote, "mnuNote")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'PrintDocument3
        '
        '
        'frmClassContRate
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.radbutShowValidInHalfYear)
        Me.Controls.Add(Me.radbutShowAll)
        Me.Controls.Add(Me.radbutShowValidInOneYear)
        Me.Controls.Add(Me.chkboxShowCancel)
        Me.Controls.Add(Me.radbutShowValid)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmClassContRate"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvContNo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvCont, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.dgvNew, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkboxShowCancel As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvContNo As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxSubClass2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboxClass2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxGrowRate As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxContRate As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents radbutShowValidInHalfYear As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShowValidInOneYear As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShowValid As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCont As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvNew As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuAnalyze As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument3 As System.Drawing.Printing.PrintDocument
End Class
