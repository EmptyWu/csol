﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2UpdKB
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxAmount = New System.Windows.Forms.TextBox
        Me.tboxReason = New System.Windows.Forms.TextBox
        Me.tboxReasonMod = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(16, 111)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 12)
        Me.Label3.TabIndex = 124
        Me.Label3.Text = "修改原因: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(16, 37)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(59, 12)
        Me.Label1.TabIndex = 123
        Me.Label1.Text = "退費原因: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(235, 202)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(75, 29)
        Me.butCancel.TabIndex = 122
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(130, 202)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(75, 29)
        Me.butSave.TabIndex = 121
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(4, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(71, 12)
        Me.Label2.TabIndex = 120
        Me.Label2.Text = "可領回金額: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxAmount
        '
        Me.tboxAmount.Location = New System.Drawing.Point(81, 6)
        Me.tboxAmount.Name = "tboxAmount"
        Me.tboxAmount.Size = New System.Drawing.Size(229, 22)
        Me.tboxAmount.TabIndex = 127
        '
        'tboxReason
        '
        Me.tboxReason.Location = New System.Drawing.Point(81, 34)
        Me.tboxReason.Multiline = True
        Me.tboxReason.Name = "tboxReason"
        Me.tboxReason.Size = New System.Drawing.Size(229, 64)
        Me.tboxReason.TabIndex = 128
        '
        'tboxReasonMod
        '
        Me.tboxReasonMod.Location = New System.Drawing.Point(81, 104)
        Me.tboxReasonMod.Multiline = True
        Me.tboxReasonMod.Name = "tboxReasonMod"
        Me.tboxReasonMod.Size = New System.Drawing.Size(229, 92)
        Me.tboxReasonMod.TabIndex = 129
        '
        'frm2UpdKB
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(326, 242)
        Me.Controls.Add(Me.tboxReasonMod)
        Me.Controls.Add(Me.tboxReason)
        Me.Controls.Add(Me.tboxAmount)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.Label2)
        Me.Name = "frm2UpdKB"
        Me.Text = "修改退費紀錄"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxAmount As System.Windows.Forms.TextBox
    Friend WithEvents tboxReason As System.Windows.Forms.TextBox
    Friend WithEvents tboxReasonMod As System.Windows.Forms.TextBox
End Class
