﻿Public Class frm2SetBookQ
    Dim c As Integer = 0

    Private Sub frm2SetBookQ_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        frmMain.SetCurrentValue(0)
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If IsNumeric(tboxQ.Text.Trim) Then
            c = CInt(tboxQ.Text.Trim)
            frmMain.SetCurrentValue(c)
            frmMain.SetOkState(True)
            Me.Close()
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Public Sub New(ByVal q As Integer)
        InitializeComponent()

        c = q
        tboxQ.Text = c.ToString
    End Sub
End Class