﻿Public Interface ICsol
    'File

    Function UsrLogin(ByVal UsrId As String, ByVal UsrPwd As String, _
                             ByVal pcname As String) As ArrayList

    Function ChgUsrPwd(ByVal UsrId As String, ByVal OldPwd As String, _
                       ByVal NewPwd As String) As Integer

    'Basic Settings
    Function ListClassRooms() As DataSet

    Function AddClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                 ByVal Name As String, ByVal DisStyle As Integer, _
                                 ByVal lstPw As ArrayList, ByVal lstNaCol As ArrayList, _
                                 ByVal lstNaRow As ArrayList, _
                                 ByVal lstConvex As ArrayList) As Integer

    Sub ModifyClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                 ByVal Name As String, ByVal DisStyle As Integer, _
                                 ByVal lstPw As ArrayList, ByVal lstNaCol As ArrayList, _
                                 ByVal lstNaRow As ArrayList, _
                                 ByVal lstConvex As ArrayList, _
                                 ByVal id As Integer)

    Function GetIdvClassPunchRec(ByVal scId As Integer, _
                                      ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable

    Sub DeleteClassRoom(ByVal rmId As Integer)

    Function ListSeatNA(ByVal rmId As Integer) As DataTable

    Function ListUserNameList() As DataTable

    Sub AddSeatNA(ByVal seatList As ArrayList)

    Sub AddStuNote(ByVal StuID As String _
           , ByVal DisplayDate As Date _
           , ByVal RepeatBfDate As Byte _
           , ByVal DisplayFor As Integer _
           , ByVal Frequency As Byte _
           , ByVal Content As String _
           , ByVal Style As Integer _
           , ByVal CreateBy As String)

    Sub UpdateSeatNA(ByVal seatDt As DataTable)

    Sub ChangeSeatNum(ByVal ID As Integer, ByVal subClassID As Integer, _
                       ByVal newSeat As String, _
                       ByVal oldSeat As String, ByVal stuID As String)

    Function ListClassSta(ByVal sc As ArrayList) As DataTable
    Function ListClassStaByDate(ByVal sc As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable

    Function GetStuNote(ByVal StuId As String, ByVal StuName As String) As DataSet
    Sub DeleteStuNote(ByVal id As Integer)
    Sub DeletePunchRec(ByVal id As Integer)
    Sub DeleteBookIssueRec(ByVal id As Integer)

    Function ListSubClassSta(ByVal scIdList As ArrayList) As DataTable
    Function ListSubClassStaHelper(ByVal scIdList As ArrayList) As DataTable

    Function GetUsrList() As DataTable
    Function GetSubClassPunchRec(ByVal scId As Integer, ByVal cId As Integer, _
                                       ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
    Function GetSubClassPunchRecNoCont(ByVal scId As Integer, _
                                       ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
    Function GetPunchRecById(ByVal strId As String) As DataTable

    Function GetPunchDataByID2(ByVal strCard As String, _
                                       ByVal ClassID As Integer) As DataSet

    Function CheckStuPunch(ByVal stuId As String, ByVal scId As Integer, ByVal sStart As Date, ByVal sEnd As Date, _
                                   ByVal sessionId As Integer) As Boolean

    Function GetPunchRecByName(ByVal strName As String) As DataTable

    Function GetPunchRecByContent(ByVal subclass As String, ByVal content As String) As List(Of String)

    Function ListSubjects() As DataTable
    Function ListClasses(ByVal UsrId As String) As DataSet

    Function AddClass(ByVal className As String, ByVal srtDate As Date, _
                      ByVal endDate As Date, ByVal fee As Integer, ByVal usrId As String) As Integer

    Sub AddContVideo(ByVal cId As Integer, ByVal lstId As ArrayList, _
                                 ByVal lstPath As ArrayList)

    Sub ModifyClass(ByVal ID As Integer, ByVal Name As String, _
                           ByVal dateStart As Date, _
                         ByVal dateEnd As Date, ByVal Fee As Integer)

    Sub DeleteClass(ByVal classId As Integer)

    Function ListClassContents(ByVal UsrId As String) As DataTable
    Function ListClassContents() As DataTable

    Function ListClassInfoSet(ByVal UsrId As String) As DataSet
    Function ListClassInfoSetHelper(ByVal UsrId As String) As DataSet

    Function ListClassBooks() As DataTable
    Function ListBooks(ByVal UsrId As String) As DataTable
    Function ListBooksWhithoutUsrID() As DataTable
    Function ListBookksByClassId(ByVal userId As String, ByVal ClassId As Integer) As DataTable


    Function GetClassPaper(ByVal classId As ArrayList) As System.Data.DataTable

    Function GetClassPaperList(ByVal classId As Integer) As System.Data.DataTable

    Function GetRepeatContent(ByVal subClassID As ArrayList, ByVal strDate As Date, ByVal endDate As Date) As Boolean
    Sub AddClassContent(ByVal classId As Integer, ByVal content As String, _
                             ByVal srtDate As Date, ByVal endDate As Date, _
                             ByVal subClassIDs As ArrayList)

    Sub AddPunchRec(ByVal stuid As String, ByVal ptime As DateTime, ByVal SubClassID As Integer, ByVal SubClassContID As Integer, ByVal MakeUpClassID As Integer, ByVal SessionID As Integer, ByVal bIn As Byte)

    Sub AddBookIssueRec(ByVal dtRec As DataTable, ByVal ClassID As Integer, ByVal BookID As ArrayList)

    Sub AddPunchRec(ByVal dtRec As ArrayList, ByVal SubClassID As Integer, ByVal SubClassContID As Integer, ByVal MakeUpClassID As Integer, ByVal SessionID As Integer, ByVal DateTime As Date)

    Sub AddBookIssueRec(ByVal stuid As String, ByVal ClassID As Integer, ByVal BookID As ArrayList, ByVal DateTime As Date)

    Function AddBookIssueRec(ByVal stuID As String, ByVal ClassID As Integer, ByVal BookID As ArrayList, ByVal DateTime As Date, ByVal MultiClass As ArrayList) As Integer

    Function AddBookIssueRec2(ByVal stuID As String, ByVal ClassID As Integer, ByVal BookID As Integer, ByVal DateTime As Date, ByVal MultiClass As ArrayList) As Integer

    Function GetClassListByUsr(ByVal usrId As String) As DataTable

    Function GetSubClassListByUsr(ByVal usrId As String) As DataTable

    Sub ModifyClassContent(ByVal Id As Integer, _
                                  ByVal content As String, _
                               ByVal srtDate As Date, ByVal endDate As Date, _
                               ByVal subClassIDs As ArrayList)

    Sub DeleteClassContent(ByVal contId As Integer)

    Function ListContentVideo(ByVal contId As Integer) As DataTable

    Sub ModifyContentVideo(ByVal cId As Integer, ByVal lstId As ArrayList, _
                                 ByVal lstPath As ArrayList)

    Sub UpdateSubClasses(ByVal subClassDt As DataTable)

    Function AddSubClass(ByVal Name As String, ByVal ClassID As Integer, _
                         ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                         ByVal ClassRoomID As Integer, ByVal usrId As String) As Integer

    Sub ModifySubClass(ByVal id As Integer, ByVal Name As String, _
                                ByVal ClassID As Integer, _
                     ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                     ByVal ClassRoomID As Integer)

    Sub ModifyScSequences(ByVal id As ArrayList, ByVal Sequence As ArrayList)

    Sub DeleteSubClass(ByVal subClassId As Integer)

    Sub DeleteStuClass(ByVal Id As Integer)

    Function ListClasseSbjs() As DataTable

    Function AddClassSbj(ByVal subject As String) As Integer

    Sub DeleteClassSbj(ByVal classSbjId As Integer)

    Sub ModifyClassSbj(ByVal classSbjId As Integer, ByVal subject As String)

    Function ListSampleTimes() As DataTable

    Sub UpdateSampleTimes(ByVal dt As ArrayList)

    Function ListClassSessions(ByVal usrId As String) As DataTable

    Function GetClassToday(ByVal d As Date) As DataTable

    Function AddClassSession(ByVal SubClassID As Integer, _
                      ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                      ByVal dtEnd As Date) As Integer

    Sub ModifyClassSession(ByVal id As Integer, ByVal SubClassID As Integer, _
                  ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                  ByVal dtEnd As Date)

    Sub DeleteClassSession(ByVal classSessionId As Integer)

    Sub UpdateSubjects(ByVal ChangedDt As DataTable)

    Function ListSchools() As DataTable

    Function AddSchool(ByVal Name As String, ByVal TypeID As Integer, _
                              ByVal Phone As String, ByVal Address As String, _
                              ByVal PersonContact As String) As Integer

    Sub ModifySchool(ByVal schoolId As Integer, ByVal name As String, _
                     ByVal typeId As Integer, ByVal phone As String, _
                     ByVal address As String, ByVal personContact As String)

    Sub DeleteSchool(ByVal schoolId As Integer)

    Sub ImportSchools(ByVal schoolDt As DataTable)

    Function GetClassRegInfo() As DataSet

    Function GetSeatRegInfo(ByVal scId As Integer) As DataSet
    Function GetSeatTakenList(ByVal scid As Integer) As DataTable
    Function GetSeatNaList(ByVal crId As Integer) As DataTable

    Function GetClassRegByStuId(ByVal stuId As String) As DataTable
    Function GetSubClassList() As DataTable
    Function GetClassList() As DataTable
    Function GetSubClassID(ByVal classID As Integer) As DataTable

    '會計系統
    Function GetPayStaByHelper(ByVal DateFrom As Date, ByVal DateTo As Date) As Byte()
    Function GetListClassStaByDateHelper(ByVal DateFrom As Date, ByVal DateTo As Date, ByVal classid As String) As Byte()
    Function GetListClassStaByNoDateHelper(ByVal classid As String) As Byte()
    Function GetListStuByClassHelper(ByVal subclassID As String) As Byte()
    Function GetListStuDiscByClassHelper(ByVal subclassID As String) As Byte()
    Function GetListStuBySubClassHelper(ByVal classID As String) As Byte()
    Function GetListStuOweByClassHelper(ByVal classID As String) As Byte()
    Function ListStuBySubClassHelper(ByVal classID As String) As Byte()
    Function ListStuDiscByClassHelperStuOwe(ByVal classID As String) As Byte()
    'Function ListStuDiscByClassHelperArray(ByVal classId As ArrayList) As DataTable

    'Student Information
    Function StuPunchWithCard(ByVal cardNum As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByRef LS As ArrayList, ByRef stuInfo As DataTable) As DataTable

    'Function StuPunchWithCard(ByVal cardNum As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean) As DataTable

    Function StuPunchWithCardstep2Helper(ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal StuInfo As DataTable, ByVal dtResult As DataTable, ByVal idx As Integer) As DataTable

    Function StuPunchWithCardstep2(ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal StuInfo As DataTable, ByVal dtResult As DataTable, ByVal idx As Integer) As DataTable


    Function StuPunchWithID(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByRef LS As ArrayList, ByRef stuinfo As DataTable) As DataTable

    'Function StuPunchWithID(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean) As DataTable

    Function StuPunchWithIDstep2Helper(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal dtResult As DataTable, ByVal idx As Integer) As DataTable

    Function StuPunchWithIDstep2(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal dtResult As DataTable, ByRef stuinfo As DataTable, ByVal idx As Integer) As DataTable

    Function StuPunchWithIDForIdvClass(ByVal stuID As String, ByVal punch As Date, ByVal classMode As Integer) As DataTable

    Function StuPunchWithCardForIdvClass(ByVal cardNum As String, ByVal punch As Date, ByVal classMode As Integer) As DataTable

    Function GetStuRegInfo() As DataSet

    Function GenStuId(ByVal strYear As String) As String

    Function CountStu(ByVal strYear As String) As Integer

    Function ImportStu(ByVal dtStu As DataTable, ByVal stuType As Byte, _
                       ByVal replace As Boolean) As ArrayList

    Function GetIntValue(ByVal s As Object) As Integer

    Function GetstringValue(ByVal s As Object) As String

    Function GetByteValue(ByVal s As Object) As Byte

    Function GetDateValue(ByVal s As Object) As Date

    Sub DeleteTeleInterview(ByVal id As Integer)

    Function GenUsrId() As String

    Function GetStuRecByName(ByVal strName As String) As DataTable

    Function GetStuRecByCardNum(ByVal strCard As String, ByVal ClassID As Integer) As DataTable
    Function GetStuRecByCardNumAbsoluteEqual(ByVal strCard As String, ByVal ClassID As Integer) As DataTable

    Function GetStuRecByID(ByVal strID As String) As DataTable
    Function GetStuRecByID(ByVal strID As String, ByVal intClass As Integer) As DataTable
    Function GetBookList(ByVal lstBook As ArrayList) As DataTable

    Function GetClassPunchBook(ByVal dayOfWeek As Integer) As DataTable
    Function GetClassPunchClass(ByVal dayOfWeek As Integer) As DataTable
    Function GetClassPunchSubClass(ByVal dayOfWeek As Integer) As DataTable
    Function GetClassPunchContent(ByVal dayOfWeek As Integer) As DataTable
    Function GetClassPunchSession(ByVal dayOfWeek As Integer) As DataTable

    Function GetStuNameListByClass(ByVal id As Integer) As DataTable

    Function GetStuNameListBySc(ByVal id As Integer) As DataTable

    Function GetStuNameByID(ByVal id As String) As String

    Function GetTeleInterview(ByVal cid As Integer, ByVal action As Byte) As DataTable

    Function GetTeleInterviewStu(ByVal StuID As String) As DataTable

    Function GetTeleInterviewDate(ByVal cid As Integer, ByVal datef As Date, ByVal datet As Date, ByVal action As Byte) As DataTable

    Function DisplayStudentInfo(ByVal ID As String) As DataSet

    Sub UpdateStuBasic(ByVal strID As String, ByVal strName As String, _
                            ByVal strTel1 As String)

    Sub AddTeleInterviewType(ByVal t As String)

    Sub AddBadgeReissueRec(ByVal StuID As String, _
           ByVal DateTime As Date, _
           ByVal HandlerID As String)

    Sub AddStudent(ByVal ID As String, ByVal Name As String, ByVal EngName As String, ByVal Sex As String, _
                  ByVal Birthday As Date, ByVal IC As String, ByVal PWD As String, ByVal Address As String, _
                  ByVal PostalCode As String, ByVal Address2 As String, ByVal PostalCode2 As String, _
                  ByVal Tel1 As String, ByVal Tel2 As String, ByVal OfficeTel As String, ByVal Mobile As String, _
                  ByVal Email As String, ByVal CardNum As String, ByVal StuType As Byte, ByVal School As String, _
                  ByVal SchoolClass As String, ByVal SchoolGrade As Int32, ByVal PrimarySch As Int32, _
                  ByVal JuniorSch As Int32, ByVal HighSch As Int32, ByVal University As Int32, _
                  ByVal CurrentSch As Int32, ByVal SchGroup As Int32, ByVal GraduateFrom As String, _
                  ByVal DadName As String, ByVal MumName As String, ByVal DadJob As String, ByVal MumJob As String, _
                  ByVal DadMobile As String, ByVal MumMobile As String, ByVal IntroID As String, _
                  ByVal IntroName As String, ByVal CreateDate As Date, ByVal FeeOwe As Int32, _
                  ByVal Remarks As String, ByVal Customize1 As String, ByVal Customize2 As String, _
                  ByVal Customize3 As String, ByVal Customize4 As String, ByVal Customize5 As String, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String)


    Sub ModifyStudent(ByVal ID As String, ByVal strName As String, _
                     ByVal EngName As String, ByVal Sex As String, _
                     ByVal Birthday As Date, ByVal IC As String, _
                     ByVal Address As String, _
                     ByVal PostalCode As String, ByVal Address2 As String, _
                     ByVal PostalCode2 As String, ByVal Tel1 As String, _
                     ByVal Tel2 As String, ByVal OfficeTel As String, _
                     ByVal Mobile As String, ByVal Email As String, _
                     ByVal StuType As Byte, ByVal School As String, _
                     ByVal SchoolClass As String, ByVal SchoolGrade As Integer, _
                     ByVal PrimarySch As Integer, ByVal JuniorSch As Integer, _
                     ByVal HighSch As Integer, ByVal University As Integer, _
                     ByVal CurrentSch As Integer, ByVal SchGroup As Integer, _
                     ByVal GraduateFrom As String, ByVal DadName As String, _
                     ByVal MumName As String, ByVal DadJob As String, _
                     ByVal MumJob As String, ByVal DadMobile As String, _
                     ByVal MumMobile As String, ByVal IntroID As String, _
                     ByVal IntroName As String, _
                     ByVal Remarks As String, _
                     ByVal Customize1 As String, ByVal Customize2 As String, _
                     ByVal Customize3 As String, ByVal Customize4 As String, _
                     ByVal Customize5 As String, ByVal lstSibRank As ArrayList, _
                    ByVal lstSibTypeID As ArrayList, _
                    ByVal lstSibName As ArrayList, _
                    ByVal lstSibBirth As ArrayList, _
                    ByVal lstSibSchool As ArrayList, _
                    ByVal lstSibRemarks As ArrayList, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String)

    Sub DeleteStudent(ByVal ID As String)

    Sub UpdateStuSibling(ByVal StuSibling As DataTable)

    Sub UpdateStuClass(ByVal StuClass As DataTable)
    Function AddClassReg(ByVal StuID As String, ByVal SubClassID As Integer, _
                                ByVal SeatNum As String, ByVal Cancel As Byte, _
                                ByVal FeeOwe As Integer, ByVal ReceiptRemarks As String, _
                                ByVal SalesID As String) As Integer

    Function AddClassReg(ByVal StuID As String, ByVal SubClassID As ArrayList, _
                                ByVal SeatNum As String, ByVal Cancel As Byte, _
                                ByVal FeeOwe As ArrayList, ByVal ReceiptRemarks As String, _
                                ByVal SalesID As String) As Integer

    Sub ChangeStuClass(ByVal newClassID As Integer, _
                       ByVal oldClassID As Integer, ByVal newSeat As String, _
                       ByVal oldSeat As String, ByVal stuID As String)


    Function getTransSubClassInfo(ByVal intOldSubClassId As Integer, ByVal strStuId As String) As DataSet
    Sub UpdateTransPayRec(ByVal ID As Integer, ByVal ReceiptNum As String, ByVal StuID As String, ByVal IntSubClassId As Integer)
    Sub UpdateTransMoneyBack(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)
    Sub UpdateTransFeeDiscount(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)
    Sub UpdateTransKeepPay(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)


    Sub ChangeStuSales(ByVal ID As Integer, ByVal sales As String)

    'action: 0-cancel, 1-resume
    Sub CancelStuClass(ByVal subClassID As Integer, ByVal stuID As String, _
                              ByVal cancel As Byte)

    Function ChangeStuID(ByVal oldID As String, ByVal newID As String, _
                    ByVal user As String) As Integer

    'Function GenReceiptNum(ByVal dtDateFrom As Date, ByVal dtDateTo As Date) As String

    Function GenReceiptNum(ByVal RectiptDate As Date) As String

    Function ListPayRec() As DataTable

    Sub UpdatePayRec(ByVal id As Integer, _
                              ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal UsrID As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String, _
                     ByVal Reason As String, ByVal OldAmount As Integer)

    Sub UpdateDisc(ByVal id As Integer, _
                             ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Discount As Integer, _
                     ByVal Remarks As String, ByVal OldDiscount As Integer)

    Sub UpdatePayOther(ByVal payOtherDt As DataTable)

    Sub UpdateKeepPay(ByVal keepPay As DataTable)

    Sub AddPayModRec(ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal UsrID As String, _
                     ByVal Remarks As String, _
                     ByVal Reason As String)

    Function AddPay(ByVal ReceiptNum As String, ByVal StuID As String, _
                         ByVal SubClassID As Integer, ByVal Amount As Integer, _
                         ByVal DateTime As Date, ByVal HandlerID As String, _
                         ByVal SalesID As String, ByVal PayMethod As Integer, _
                         ByVal Extra As String, ByVal Remarks As String, _
                         ByVal ChequeValid As Date, ByVal Discount As Integer, _
                         ByVal DiscountRemarks As String, ByVal oldDiscount As Integer, _
                         ByVal oldDiscountRemarks As String, ByVal oldDiscountId As Integer, ByVal DateGet As DateTime) As Int32

    Function AddMB(ByVal pid As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal PayAmount As Integer, ByVal PayDateTime As Date, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal SalesID As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String, _
                     ByVal Discount As Integer, _
                     ByVal DiscountRemarks As String, _
                     ByVal Reason As String) As Int32

    Function AddMK(ByVal pid As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal PayAmount As Integer, ByVal PayDateTime As Date, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal SalesID As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String, _
                     ByVal Discount As Integer, _
                     ByVal DiscountRemarks As String, _
                     ByVal Reason As String, ByVal KeepUntil As DateTime) As Int32

    Function ListDiscountDict() As DataTable
    Function ListDiscountDictType() As DataTable

    Sub UpdateDiscDictType(ByVal ChangedDt As DataTable)

    Function ListTeleInterviewType() As DataTable

    Sub UpdateTeleInterviewType(ByVal ChangedDt As DataTable)

    Sub AddTeleInterview(ByVal StuID As String, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String)

    Sub UpdTeleInterview(ByVal ID As Integer, _
                                ByVal TypeID As Integer, ByVal Content As String)

    Sub AddTeleInterview(ByVal StuID As ArrayList, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String)

    Sub UpdateDiscDict(ByVal ChangedDt As DataTable)

    Sub UpdateRemarksDict(ByVal ChangedDt As DataTable)

    Function AddDiscountDict(ByVal amount As Integer, ByVal reason As String, _
                             ByVal typeId As Integer) As Integer

    Sub ModifyDiscountDict(ByVal dictId As Integer, ByVal amount As Integer, _
                           ByVal reason As String, ByVal typeId As Integer)

    Sub DeleteDiscountDict(ByVal dictId As Integer)

    Function ListReceiptNoteDict() As DataTable

    Function AddListReceiptNoteDict(ByVal reason As String) As Integer

    Sub ModifyListReceiptNoteDict(ByVal dictId As Integer, ByVal reason As String)

    Sub DeleteListReceiptNoteDict(ByVal dictId As Integer)

    Function ListPayBackRec(ByVal stuId As String) As DataTable

    Sub UpdateMB(ByVal ID As Integer, _
                  ByVal OldAmount As Integer, ByVal Amount As Integer, _
                  ByVal UsrID As String, _
                  ByVal OldReason As String, ByVal Reason As String, _
                  ByVal ReasonMod As String)

    Sub UpdPunchRec(ByVal ID As Integer, _
                 ByVal DateTime1 As DateTime, ByVal InType As Integer)


    Sub AddMBRemarks(ByVal ID As Integer, ByVal Remarks As String)

    Function ListPunchRec(ByVal stuId As String) As DataSet

    Function ListBookIssueRec(ByVal stuId As String) As DataTable
    Function ListBookIssueRec2(ByVal stuId As String) As DataTable                                          '100224
    Function GetStuNoBookRec(ByVal StuId As String) As DataTable
    Function SearchStu(ByVal method As Integer, ByVal kw As String, ByVal type As Byte) As DataTable
    Function SearchStu(ByVal method As Integer, ByVal kw As String, ByVal type As Byte, ByVal int As Integer) As DataTable

    Function SearchStu_Short(ByVal method As Integer, ByVal kw As String, _
                                 ByVal type As Byte) As DataTable

    Function GetSeatMap(ByVal scId As Integer) As DataTable

    Function GetNextStu(ByVal id As String) As String

    Function GetPreviousStu(ByVal id As String) As String

    Function ListChangeClassRec(ByVal stuId As String) As DataTable

    Function GetStuChangeClassList() As DataTable

    Function ListStudents(ByVal classId As Integer) As ArrayList
    Function ListStuBySch(ByVal schId As Integer, ByVal schType As Integer) As DataTable
    Function ListStuByClass(ByVal id As Integer) As DataTable
    Function ListStuByClass(ByVal id As ArrayList) As DataTable
    Function ListStuBySubClass(ByVal id As Integer) As DataTable
    Function ListStuBySubClass(ByVal id As ArrayList) As DataTable
    Function ListStudentSiblings() As DataTable

    Function ListStudentSiblings(ByVal lstfilter As List(Of String)) As DataTable


    Function ListStuBadgeReIssue() As DataTable

    Function GetNameRepeatStu() As DataTable

    Sub ImportStuInfo(ByVal stus As ArrayList)

    Function ListNameRepeat() As DataTable
    Sub UpdateReceiptPrint(ByVal ChangedDt As DataTable)
    Function ListSchGrp() As DataTable
    Function ListSchType() As DataTable
    Function ListSibType() As DataTable

    'Book
    Function AddBook(ByVal Name As String, ByVal Stock As Integer, _
                          ByVal Price As Integer, ByVal BeforeDate As Date, _
                          ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                          ByVal MultiClass As Byte, ByVal CreateDate As Date, _
                          ByVal lstClass As ArrayList) As Integer
    Sub UpdateBook(ByVal ID As Integer, _
                   ByVal Name As String, ByVal Stock As Integer, _
                          ByVal Price As Integer, ByVal BeforeDate As Date, _
                          ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                          ByVal MultiClass As Byte, ByVal CreateDate As Date, _
                          ByVal lstClass As ArrayList)
    Sub UpdateBookQ(ByVal ID As Integer, ByVal Stock As Integer)
    Sub DeleteBook(ByVal id As Integer)

    '20100205 updated by sherry start
    Function GetBookRecByUsr(ByVal ClassId As Integer, ByVal BookId As Integer) As DataTable
    '20100205 updated by sherry end

    'Financial
    Function ListPayStatistics(ByVal startDate As Date, ByVal endDate As Date) As DataSet

    Function ListPayStaByClass(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataSet
    Function ListPayStaByClassHelper(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataSet
    Function ListPayStaByClass(ByVal classId As ArrayList) As DataSet

    Function ListPayStaByClassHelper(ByVal classId As ArrayList) As DataSet


    Function ListPayStaBySubClass(ByVal classID As ArrayList, ByVal subClassID As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataSet
    Function ListPayStaBySubClass(ByVal classID As ArrayList, ByVal subClassID As ArrayList) As DataSet

    Function ListPayStaForDates(ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListDatePaySta(ByVal startDate As Date, ByVal endDate As Date) As DataSet

    Function ListPersonalSales(ByVal usrId As String, ByVal classId As ArrayList, _
                               ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListPersonalSales(ByVal usrId As String, ByVal classId As ArrayList) As DataTable
    Function ListPersonalDisc(ByVal usrId As String, ByVal classId As ArrayList, _
                               ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListPersonalDisc(ByVal usrId As String, ByVal classId As ArrayList) As DataTable

    Function ListPersonalHandle(ByVal usrId As String, ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListStuOweByClass(ByVal classId As ArrayList) As DataTable
    Function ListStuOweByClass(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable

    Function ListStuDiscByClassHelper(ByVal classId As ArrayList) As DataTable

    Function ListStuDiscByClass(ByVal classId As ArrayList) As DataTable
    Function ListStuDiscByClass(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListPayRecByStuId(ByVal stuId As String) As DataTable
    Function ListKBByStuId(ByVal stuId As String) As DataTable
    Function ListStuMKByClass(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListStuMBByClass(ByVal classId As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable

    Function ListJournalGrp() As DataTable
    Sub UpdateJournalGrp(ByVal ChangedDt As DataTable)

    Sub UpdJournalRec(ByVal ID As Int32, ByVal GroupID As Int32, _
       ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
       ByVal UserID As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
       ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String)

    Function ListJournalSta(ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListJournalSta() As DataTable
    Function AddJournalRec(ByVal GroupID As Int32, _
        ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
        ByVal User As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
        ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String) As Int32
    Sub DeleteJournalRec(ByVal ID As Integer)
    Sub DeletePayRec(ByVal ID As Integer)
    Sub DeletePayRec(ByVal ID As Integer, ByVal stuid As String, ByVal scid As Integer, ByVal amount As Integer, _
                     ByVal backAmount As Integer, ByVal keepAmount As Integer)
    Sub DeletePayRec(ByVal ID As Integer, ByVal stuid As String, ByVal scid As Integer, ByVal amount As Integer)

    Sub DeleteDisc(ByVal ID As Integer, ByVal stuid As String, _
                     ByVal scid As Integer, ByVal Discount As Integer)

    Sub DeleteDisc(ByVal ID As Integer)                                             '100304

    Sub DeleteMB(ByVal ID As Integer)

    Function ListPayStaByKeyword(ByVal method As Integer, _
                                                  ByVal keyword As String, _
                                                  ByVal dateFrom As Date, _
                                                  ByVal dateTo As Date) As DataTable

    Function ListPayStaByKeyword(ByVal method As Integer, _
                                                  ByVal keyword As String) As DataTable


    Function ListDiscountStaByClass(ByVal classId As ArrayList) As DataTable
    Function ListChequeValidSta(ByVal startDate As Date, ByVal endDate As Date) As DataTable
    Function ListPayModifyRec() As DataTable
    Function ListDiscModifyRec() As DataTable

    'Admin
    Function ListEmployee() As DataTable
    Sub ModifyUsrStatus(ByVal UsrID As String, ByVal Status As Int32)
    Sub DeleteUsrByID(ByVal UsrID As String)
    Sub ModifyUsrCard(ByVal UsrID As String, ByVal CardNum As String)
    Function ModifyStuCard(ByVal ID As String, ByVal CardNum As String) As Integer
    Sub ModifyStuCard2(ByVal ID As String, ByVal CardNum As String)
    Sub ModifyUsrPwd(ByVal UsrID As String, ByVal Pwd As String)
    Sub AddUser(ByVal ID As String, ByVal Name As String, ByVal PWD As String, _
         ByVal Modify As Int32, ByVal Status As Int32, ByVal GroupID As Int32, _
         ByVal CardNum As String, ByVal Phone1 As String, ByVal Phone2 As String, _
         ByVal IC As String, ByVal Mobile As String, ByVal Guardian As String, _
         ByVal GuardMobile As String, ByVal PostalCode As String, _
         ByVal Address As String, ByVal Email As String, ByVal Remarks As String, _
         ByVal Sex As String, ByVal EnglishName As String, ByVal Birthday As Date, _
         ByVal Horoscope As String, ByVal AccntName As String, _
         ByVal OnBoardDate As Date, ByVal BloodType As Int32, _
         ByVal JuniorSchID As Int32, ByVal HighSchID As Int32, _
         ByVal UniversityID As Int32, ByVal IsSales As Int32)
    Function GetUsrByID(ByVal UsrID As String) As DataTable
    Sub UpdateUsrInfo(ByVal ChangedUser As DataTable)
    Function ListUsrAllAuthority(ByVal UsrId As String) As DataSet
    Function ListUsrAuthority(ByVal UsrId As String) As DataTable
    Sub UpdateUsrAuthority(ByVal id As String, ByVal items As ArrayList)
    Sub UpdateUsrAuthority(ByVal id As String, ByVal items As ArrayList, _
                           ByVal ids As ArrayList, ByVal types As ArrayList)
    Function GetReceiptPrint() As DataTable
    Function GetInitData(ByVal id As String) As DataSet
    Function GetPunchData(ByVal strCard As String, _
                                       ByVal ClassID As Integer) As DataSet
    Function GetClassRate(ByVal SubClassID As Integer, ByVal ContentID As Integer, _
                          ByVal dateStart As Date, ByVal dateEnd As Date) As Array

    Function AddPaper(ByVal Name As String, ByVal Abbr As String, _
                       ByVal PaperDate As Date, ByVal content As Integer, _
                         ByVal lstSc As ArrayList) As Int32

    Sub UpdatePaper(ByVal ID As Integer, ByVal Name As String, ByVal Abbr As String, _
                      ByVal PaperDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList)

    Sub DeletePaper(ByVal id As Integer)

    Function GetStuGrades(ByVal paperId As Integer, ByVal sc As ArrayList) As DataTable

    Function GetClassAssignment(ByVal classId As ArrayList) As System.Data.DataTable

    Function AddAssignment(ByVal Name As String, ByVal StartDate As Date, _
                       ByVal EndDate As Date, ByVal content As Integer, _
                         ByVal lstSc As ArrayList) As Int32

    Sub UpdateAssignment(ByVal ID As Integer, ByVal Name As String, ByVal StartDate As Date, _
                      ByVal EndDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList)

    Sub DeleteAssignment(ByVal id As Integer)

    Sub UpdateStuGrade(ByVal stuId As String, ByVal paperId As Integer, ByVal mark As Single, _
                              ByVal dt As Date, ByVal remarks As String, ByVal inputby As String)

    Sub DeleteStuGrade(ByVal stuId As String, ByVal paperId As Integer)

    Function GetStuGradesMul(ByVal paperId As ArrayList, ByVal sc As ArrayList, ByVal c As Integer) As DataTable

    Function GetStuGradesMulByMulClass(ByVal paperId As ArrayList, ByVal sc As ArrayList, ByVal c As ArrayList) As DataTable

    Function GetClassAssignmentList(ByVal classId As Integer) As System.Data.DataTable

    Sub UpdateStuAssignment(ByVal stuId As String, ByVal AssignmentId As Integer, ByVal dt As Date, ByVal mark As Integer)

    Function GetStuAssignments(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable

    Function GetStuAssignmentsMul(ByVal assignmentId As ArrayList, ByVal sc As ArrayList, ByVal c As Integer) As DataTable

    Function GetStuDoneAssignmentList(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable

    Function GetStuNoAssignmentList(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable

    Sub ImportStuGrades(ByVal dt As DataTable, ByVal nameType As Integer, ByVal replace As Integer, _
                        ByVal datetime As Date, ByVal paper As Integer, ByVal sc As ArrayList, ByVal handler As String)
    Function GetStuGradeFromTable(ByRef dt As DataTable, ByVal id As String) As Integer
    Function GetStuIDFromGradeTable(ByRef dt As DataTable, ByVal name As String, ByVal type As Integer) As String

    Function GetSubClassPunchRecList(ByVal sc As ArrayList, _
                                   ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable

    Function GetUpdateList(ByVal last As Date) As String()
    Function GetUpdateFile(ByVal filename As String) As Byte()
    '20100424 imgdb by sherry start
    Sub UploadImg(ByVal StuID As String, ByVal img As Byte())
    Sub DelImg(ByVal id As Integer)
    Sub DelImgByStuID(ByVal id As String)
    Function DownloadImg(ByVal StuID As String) As Byte()
    Function GetAutoUpdatePath() As String
    Function GetStuIdList() As DataTable
    '20100424 imgdb by sherry end
End Interface
