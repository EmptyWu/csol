﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SelectClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SelectClass))
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.butSelNone = New System.Windows.Forms.Button
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.butSelAll = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.lstboxClass = New System.Windows.Forms.ListBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.butSelNone)
        Me.GroupBox3.Controls.Add(Me.chklstSubClass)
        Me.GroupBox3.Controls.Add(Me.butSelAll)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'butSelNone
        '
        resources.ApplyResources(Me.butSelNone, "butSelNone")
        Me.butSelNone.Name = "butSelNone"
        Me.butSelNone.UseVisualStyleBackColor = True
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstSubClass, "chklstSubClass")
        Me.chklstSubClass.Name = "chklstSubClass"
        '
        'butSelAll
        '
        resources.ApplyResources(Me.butSelAll, "butSelAll")
        Me.butSelAll.Name = "butSelAll"
        Me.butSelAll.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.lstboxClass)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'lstboxClass
        '
        Me.lstboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.lstboxClass, "lstboxClass")
        Me.lstboxClass.Name = "lstboxClass"
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'frm2SelectClass
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.butCancel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SelectClass"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelNone As System.Windows.Forms.Button
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents butSelAll As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lstboxClass As System.Windows.Forms.ListBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
End Class
