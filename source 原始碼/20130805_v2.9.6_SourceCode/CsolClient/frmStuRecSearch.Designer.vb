﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuRecSearch
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuRecSearch))
        Me.mnuExportPhoto = New System.Windows.Forms.ToolStripMenuItem
        Me.cboxFilter = New System.Windows.Forms.ComboBox
        Me.mnuPostalTag = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuUpgrade = New System.Windows.Forms.ToolStripMenuItem
        Me.butFilter = New System.Windows.Forms.Button
        Me.tboxFilter = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.cboxItem = New System.Windows.Forms.ComboBox
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.chkboxNotInClass = New System.Windows.Forms.CheckBox
        Me.chkboxInClass = New System.Windows.Forms.CheckBox
        Me.tboxCount = New System.Windows.Forms.TextBox
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuExportPhoto
        '
        Me.mnuExportPhoto.Name = "mnuExportPhoto"
        resources.ApplyResources(Me.mnuExportPhoto, "mnuExportPhoto")
        '
        'cboxFilter
        '
        Me.cboxFilter.FormattingEnabled = True
        Me.cboxFilter.Items.AddRange(New Object() {resources.GetString("cboxFilter.Items"), resources.GetString("cboxFilter.Items1"), resources.GetString("cboxFilter.Items2")})
        resources.ApplyResources(Me.cboxFilter, "cboxFilter")
        Me.cboxFilter.Name = "cboxFilter"
        '
        'mnuPostalTag
        '
        Me.mnuPostalTag.Name = "mnuPostalTag"
        resources.ApplyResources(Me.mnuPostalTag, "mnuPostalTag")
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        resources.ApplyResources(Me.mnuExportData, "mnuExportData")
        '
        'mnuUpgrade
        '
        Me.mnuUpgrade.Name = "mnuUpgrade"
        resources.ApplyResources(Me.mnuUpgrade, "mnuUpgrade")
        '
        'butFilter
        '
        resources.ApplyResources(Me.butFilter, "butFilter")
        Me.butFilter.Name = "butFilter"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'tboxFilter
        '
        resources.ApplyResources(Me.tboxFilter, "tboxFilter")
        Me.tboxFilter.Name = "tboxFilter"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'cboxItem
        '
        Me.cboxItem.FormattingEnabled = True
        Me.cboxItem.Items.AddRange(New Object() {resources.GetString("cboxItem.Items"), resources.GetString("cboxItem.Items1"), resources.GetString("cboxItem.Items2"), resources.GetString("cboxItem.Items3")})
        resources.ApplyResources(Me.cboxItem, "cboxItem")
        Me.cboxItem.Name = "cboxItem"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuSelectCol, Me.mnuDelete, Me.mnuPrint, Me.mnuExportData, Me.mnuExportPhoto, Me.mnuPostalTag, Me.mnuUpgrade, Me.mnuClose})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'PrintDocument1
        '
        '
        'chkboxNotInClass
        '
        resources.ApplyResources(Me.chkboxNotInClass, "chkboxNotInClass")
        Me.chkboxNotInClass.Name = "chkboxNotInClass"
        Me.chkboxNotInClass.UseVisualStyleBackColor = True
        '
        'chkboxInClass
        '
        resources.ApplyResources(Me.chkboxInClass, "chkboxInClass")
        Me.chkboxInClass.Checked = True
        Me.chkboxInClass.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxInClass.Name = "chkboxInClass"
        Me.chkboxInClass.UseVisualStyleBackColor = True
        '
        'tboxCount
        '
        resources.ApplyResources(Me.tboxCount, "tboxCount")
        Me.tboxCount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tboxCount.Name = "tboxCount"
        Me.tboxCount.ReadOnly = True
        '
        'frmStuRecSearch
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.tboxCount)
        Me.Controls.Add(Me.chkboxNotInClass)
        Me.Controls.Add(Me.chkboxInClass)
        Me.Controls.Add(Me.cboxFilter)
        Me.Controls.Add(Me.butFilter)
        Me.Controls.Add(Me.tboxFilter)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboxItem)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Name = "frmStuRecSearch"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuExportPhoto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboxFilter As System.Windows.Forms.ComboBox
    Friend WithEvents mnuPostalTag As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUpgrade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents tboxFilter As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboxItem As System.Windows.Forms.ComboBox
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents chkboxNotInClass As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxInClass As System.Windows.Forms.CheckBox
    Friend WithEvents tboxCount As System.Windows.Forms.TextBox
End Class
