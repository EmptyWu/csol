﻿Imports Microsoft.Office.Interop
Imports System.Data.OleDb


Public Class frm2ImportGrades
    Dim path As String = ""
    Dim fileExt As String = ""
    Dim myExcel As Excel.Application
    Dim myWorkBookCollection As Excel.Workbooks ' Workbook-collection (note the 's' at the end)        
    Dim myWorkBook As Excel.Workbook ' Single Workbook (spreadsheet-collection)        
    Dim myWorkSheet As Excel.Worksheet ' Single spreadsheet  
    Dim dt As New DataTable
    Private dtPaper As New DataTable
    Private dtPaper2 As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstPaper As New ArrayList
    Dim ds As DataSet
    Dim intClass As Integer
    Dim intPaper As Integer

    Private Sub frm2ImportGrades_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not myExcel Is Nothing Then
            myExcel.Quit()
        End If
    End Sub

    Private Sub frm2ImportGrades_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        cboxName.SelectedIndex = -1
        cboxID.SelectedIndex = -1
        cboxMark.SelectedIndex = -1

        ds = frmMain.GetClassInfoSet
        dtClass = ds.Tables(c_ClassListDataTableName).Copy()
        dtSubClass = ds.Tables(c_SubClassListDataTableName).Copy()

        InitList()

    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()


        Dim rows() As DataRow = dtClass.Select("", String.Format("{0} DESC ", c_StartColumnName))
        'dtClass.Clear()
        Dim dtClass2 As New DataTable
        dtClass2 = dtClass.Clone()

        For Each row As DataRow In rows
            dtClass2.Rows.Add(row.ItemArray)
        Next

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass2.Rows.Count - 1
                Dim classid As String = dtClass2.Rows(index).Item(c_NameColumnName)
                If cboxClass.Items.IndexOf(classid) = -1 Then
                    cboxClass.Items.Add(dtClass2.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass2.Rows(index).Item(c_IDColumnName))
                End If
            Next
        Else
            For index As Integer = 0 To dtClass2.Rows.Count - 1
                If dtClass2.Rows(index).Item(c_EndColumnName) >= Now Then
                    Dim classid As String = dtClass2.Rows(index).Item(c_NameColumnName)
                    If cboxClass.Items.IndexOf(classid) = -1 Then
                        cboxClass.Items.Add(dtClass2.Rows(index).Item(c_NameColumnName))
                        lstClass.Add(dtClass2.Rows(index).Item(c_IDColumnName))
                    End If
                End If
            Next
        End If

        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = 0
        End If

    End Sub

    Private Sub butFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFile.Click
        Dim fileName As String = ""
        Dim openFileDialog1 As New OpenFileDialog()
        Dim xcFileInfo As IO.FileInfo

        openFileDialog1.InitialDirectory = "c:\"

        openFileDialog1.Filter = "Excel files (*.xls)|*.xls|All files (*.*)|*.*"

        openFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                path = openFileDialog1.FileName
                tboxFile.Text = path
                fileName = path.Substring(path.LastIndexOf("\\") + 1)
                fileExt = fileName.Substring(fileName.LastIndexOf(".") + 1)
                If fileExt.ToLower() = "xls" Or fileExt.ToLower = "xlsx" Then
                    xcFileInfo = New IO.FileInfo(path)

                    ' Initialize the interface to Excel.exe        
                    myExcel = New Excel.Application
                    If myExcel Is Nothing Then
                        cboxTable.Items.Clear()
                        Exit Sub
                    End If
                    ' initialise access to Excel's workbook collection        
                    myWorkBookCollection = myExcel.Workbooks
                    'open spreadsheet from disk        
                    myWorkBook = myWorkBookCollection.Open(xcFileInfo.FullName, , False)
                    'get 1st sheet from workbook
                    For i As Integer = 1 To myWorkBook.Sheets.Count
                        myWorkSheet = myWorkBook.Sheets.Item(i)
                        cboxTable.Items.Add(myWorkSheet.Name)
                    Next
                    If cboxTable.Items.Count > 0 Then
                        cboxTable.SelectedIndex = 0
                    End If


                End If

            Catch Ex As Exception
                AddErrorLog(Ex.ToString)

            End Try
        End If
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub butImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butImport.Click
        If cboxID.Text = "" And cboxName.Text = "" Then
            MsgBox("請選擇對應欄位中學號(姓名)的欄位位置")
            Return
        End If

        If cboxMark.Text = "" Then
            MsgBox("請選擇對應欄位中分數的欄位位置")
            Return
        End If
        If cboxPaper.SelectedIndex = -1 Then
            MsgBox("請選擇考試試卷")
            Return
        End If


        Try

            Dim nameType As Integer = 0 '0-ID 1-Name

            Dim i1 As Integer = cboxID.SelectedIndex + 1
            Dim i2 As Integer = cboxName.SelectedIndex + 1
            Dim i3 As Integer = cboxMark.SelectedIndex + 1
            If i1 = 0 Then
                i1 = i2
                nameType = 1
            End If
            intPaper = lstPaper(cboxPaper.SelectedIndex)
            If i1 = 0 Or i3 = 0 Or intPaper = 0 Or lstSubClass.Count = 0 Then
                ShowInfo(My.Resources.msgNoSelection)
                Exit Sub
            End If

            Dim replace As Integer = 0 '0-keep, 1-replace
            If radbutKeepOld.Checked = False Then
                replace = 1
            End If

            Dim datetime As Date
            If radbutNow.Checked Then
                datetime = Now
            Else
                datetime = dtPickInput.Value
            End If

            dt.Columns.Clear()
            dt.Columns.Add(c_IDColumnName, GetType(System.String))
            dt.Columns.Add(c_NameColumnName, GetType(System.String))
            dt.Columns.Add(c_MarkColumnName, GetType(System.Single))

            Dim r As Integer = myWorkSheet.UsedRange.Rows.Count

            If nameType = 0 Then
                For i As Integer = 1 To r
                    dt.Rows.Add(myWorkSheet.UsedRange.Cells(i, i1).Value.ToString.Trim, "", GetDbSingle(myWorkSheet.UsedRange.Cells(i, i3).Value.ToString.Trim))
                Next
            ElseIf nameType = 1 Then
                For i As Integer = 1 To r
                    dt.Rows.Add("", myWorkSheet.UsedRange.Cells(i, i1).Value.ToString.Trim, GetDbSingle(myWorkSheet.UsedRange.Cells(i, i3).Value.ToString.Trim))
                Next
            End If

            If dt.Rows.Count > 0 Then

                frmMain.SetOkState(True)
                objCsol.ImportStuGrades(dt, nameType, replace, datetime, intPaper, lstSubClass, frmMain.GetUsrId)
                ShowInfo(My.Resources.msgImportDone)
                Me.Close()
            Else
                ShowInfo(My.Resources.msgInvalidData)
            End If

        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try
    End Sub



    Private Sub cboxTable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxTable.SelectedIndexChanged
        Try
            If cboxTable.SelectedIndex > -1 Then
                myWorkSheet = myWorkBook.Sheets.Item(cboxTable.SelectedIndex + 1)
                cboxID.Items.Clear()
                cboxName.Items.Clear()
                cboxMark.Items.Clear()

                MakeList(cboxName)
                If cboxName.Items.Count > 0 Then
                    cboxName.SelectedIndex = 0
                End If

                MakeList(cboxID)
                If cboxID.Items.Count > 0 Then
                    cboxID.SelectedIndex = 0
                End If

                MakeList(cboxMark)
                If cboxMark.Items.Count > 0 Then
                    cboxMark.SelectedIndex = 0
                End If

            End If
        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub MakeList(ByRef cbox As System.Windows.Forms.ComboBox)
        cbox.Items.Clear()
        Dim UsedColumns As Integer = myWorkSheet.UsedRange.Columns.Count
        Dim s As String
        For j As Integer = 1 To UsedColumns
            s = myWorkSheet.Cells(1, j).value.ToString.Trim
            cbox.Items.Add(s)
        Next

    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        Try
            lstSubClass.Clear()
            intClass = lstClass(cboxClass.SelectedIndex)
            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClass.Add(row.Item(c_IDColumnName))
                    End If
                Next

            End If

            If intClass > 0 Then
                dtPaper2 = objCsol.GetClassPaperList(intClass)
                dtPaper = dtPaper2.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName)
            End If

            RefreshPaperList()

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshPaperList()
        cboxPaper.Items.Clear()
        lstPaper.Clear()
        cboxPaper.Text = ""
        For Each row As DataRow In dtPaper.Rows
            cboxPaper.Items.Add(row.Item(c_NameColumnName))
            lstPaper.Add(row.Item(c_IDColumnName))
        Next

        If cboxPaper.Items.Count > 0 Then
            cboxPaper.SelectedIndex = 0
        End If

    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub cboxID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxID.SelectedIndexChanged
        If cboxID.SelectedIndex > -1 Then
            cboxName.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboxName_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxName.SelectedIndexChanged
        If cboxName.SelectedIndex > -1 Then
            cboxID.SelectedIndex = -1
        End If
    End Sub
End Class