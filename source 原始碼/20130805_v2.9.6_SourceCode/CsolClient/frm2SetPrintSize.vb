﻿Public Class frm2SetPrintSize
    Private intSize As Integer = 0

    Private Sub frm2SetPrintSize_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        intSize = GetPrintSize()
        Select Case intSize
            Case 9
                radbut9.Checked = True
            Case 12
                radbut12.Checked = True
            Case 14
                radbut14.Checked = True
            Case 16
                radbut16.Checked = True
            Case Else
                radbut9.Checked = True
                SetPrintSize(9)
        End Select
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If radbut12.Checked Then
            intSize = 12
        ElseIf radbut14.Checked Then
            intSize = 14
        ElseIf radbut16.Checked Then
            intSize = 16
        Else
            intSize = 9
        End If
        SetPrintSize(intSize)
        frmMain.SetOkState(True)
        Me.Close()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class