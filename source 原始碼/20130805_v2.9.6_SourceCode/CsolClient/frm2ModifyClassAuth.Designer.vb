﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ModifyClassAuth
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2ModifyClassAuth))
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutSelected = New System.Windows.Forms.RadioButton
        Me.radbutAll = New System.Windows.Forms.RadioButton
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutSelected)
        Me.GroupBox3.Controls.Add(Me.radbutAll)
        Me.GroupBox3.Controls.Add(Me.chklstSubClass)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'radbutSelected
        '
        resources.ApplyResources(Me.radbutSelected, "radbutSelected")
        Me.radbutSelected.Name = "radbutSelected"
        Me.radbutSelected.TabStop = True
        Me.radbutSelected.UseVisualStyleBackColor = True
        '
        'radbutAll
        '
        resources.ApplyResources(Me.radbutAll, "radbutAll")
        Me.radbutAll.Name = "radbutAll"
        Me.radbutAll.TabStop = True
        Me.radbutAll.UseVisualStyleBackColor = True
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstSubClass, "chklstSubClass")
        Me.chklstSubClass.Name = "chklstSubClass"
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2ModifyClassAuth
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.GroupBox3)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ModifyClassAuth"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutSelected As System.Windows.Forms.RadioButton
    Friend WithEvents radbutAll As System.Windows.Forms.RadioButton
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
