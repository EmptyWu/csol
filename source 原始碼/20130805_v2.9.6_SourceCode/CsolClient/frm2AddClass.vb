﻿Public Class frm2AddClass

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not tboxName.Text.Trim = "" Then
            'Me.tboxName.Text = Strings.StrConv(Me.tboxName.Text, VbStrConv.Narrow)
            Dim intFee As Integer
            If IsNumeric(tboxFee.Text) Then
                intFee = CInt(tboxFee.Text)
                If intFee < 0 Then
                    intFee = 0
                End If
            Else
                intFee = 0
            End If

            If intFee = 0 Then
                Dim result As MsgBoxResult = _
                            MsgBox(My.Resources.msgClassFeeEmpty, _
                                    MsgBoxStyle.YesNo, My.Resources.msgTitle)
                If result = MsgBoxResult.Yes Then

                    objCsol.AddClass(tboxName.Text, GetDateStart(dtpickFrom.Value), _
                                     GetDateEnd(dtpickTo.Value), intFee, frmMain.GetUsrId)
                    frmMain.SetOkState(True)
                    Me.Close()
                End If
            Else
                objCsol.AddClass(tboxName.Text, GetDateStart(dtpickFrom.Value), _
                                     GetDateEnd(dtpickTo.Value), intFee, frmMain.GetUsrId)
                frmMain.SetOkState(True)
                Me.Close()
            End If
            frmMain.BackgroundWorker7.RunWorkerAsync()
        Else
            ShowMsg()
        End If
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoClassName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class