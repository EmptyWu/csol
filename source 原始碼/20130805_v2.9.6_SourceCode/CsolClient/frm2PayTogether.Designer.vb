﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2PayTogether
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2PayTogether))
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtpickMakeupDate = New System.Windows.Forms.DateTimePicker
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.radbutCard = New System.Windows.Forms.RadioButton
        Me.radbutChk = New System.Windows.Forms.RadioButton
        Me.radbutCash = New System.Windows.Forms.RadioButton
        Me.radbutTrans = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.radbutNormal = New System.Windows.Forms.RadioButton
        Me.radbutMakeup = New System.Windows.Forms.RadioButton
        Me.Label8 = New System.Windows.Forms.Label
        Me.tboxDisc = New System.Windows.Forms.TextBox
        Me.butRemarkDict = New System.Windows.Forms.Button
        Me.tboxReceiptRemarks = New System.Windows.Forms.TextBox
        Me.tboxOwe = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxTodayPay = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxDiscRemarks = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.butDisDict = New System.Windows.Forms.Button
        Me.butMkPay = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.grpboxExtra = New System.Windows.Forms.GroupBox
        Me.lblCheque = New System.Windows.Forms.Label
        Me.dtpickCheque = New System.Windows.Forms.DateTimePicker
        Me.tboxPayExtraInfo = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.grpboxExtra.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'dtpickMakeupDate
        '
        resources.ApplyResources(Me.dtpickMakeupDate, "dtpickMakeupDate")
        Me.dtpickMakeupDate.Name = "dtpickMakeupDate"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.radbutCard)
        Me.GroupBox2.Controls.Add(Me.radbutChk)
        Me.GroupBox2.Controls.Add(Me.radbutCash)
        Me.GroupBox2.Controls.Add(Me.radbutTrans)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'radbutCard
        '
        resources.ApplyResources(Me.radbutCard, "radbutCard")
        Me.radbutCard.Name = "radbutCard"
        Me.radbutCard.UseVisualStyleBackColor = True
        '
        'radbutChk
        '
        resources.ApplyResources(Me.radbutChk, "radbutChk")
        Me.radbutChk.Name = "radbutChk"
        Me.radbutChk.UseVisualStyleBackColor = True
        '
        'radbutCash
        '
        resources.ApplyResources(Me.radbutCash, "radbutCash")
        Me.radbutCash.Checked = True
        Me.radbutCash.Name = "radbutCash"
        Me.radbutCash.TabStop = True
        Me.radbutCash.UseVisualStyleBackColor = True
        '
        'radbutTrans
        '
        resources.ApplyResources(Me.radbutTrans, "radbutTrans")
        Me.radbutTrans.Name = "radbutTrans"
        Me.radbutTrans.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radbutNormal)
        Me.GroupBox1.Controls.Add(Me.radbutMakeup)
        Me.GroupBox1.Controls.Add(Me.dtpickMakeupDate)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'radbutNormal
        '
        resources.ApplyResources(Me.radbutNormal, "radbutNormal")
        Me.radbutNormal.Checked = True
        Me.radbutNormal.Name = "radbutNormal"
        Me.radbutNormal.TabStop = True
        Me.radbutNormal.UseVisualStyleBackColor = True
        '
        'radbutMakeup
        '
        resources.ApplyResources(Me.radbutMakeup, "radbutMakeup")
        Me.radbutMakeup.Name = "radbutMakeup"
        Me.radbutMakeup.UseVisualStyleBackColor = True
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.ForeColor = System.Drawing.Color.Black
        Me.Label8.Name = "Label8"
        '
        'tboxDisc
        '
        resources.ApplyResources(Me.tboxDisc, "tboxDisc")
        Me.tboxDisc.Name = "tboxDisc"
        Me.tboxDisc.ReadOnly = True
        '
        'butRemarkDict
        '
        resources.ApplyResources(Me.butRemarkDict, "butRemarkDict")
        Me.butRemarkDict.Name = "butRemarkDict"
        Me.butRemarkDict.UseVisualStyleBackColor = True
        '
        'tboxReceiptRemarks
        '
        resources.ApplyResources(Me.tboxReceiptRemarks, "tboxReceiptRemarks")
        Me.tboxReceiptRemarks.Name = "tboxReceiptRemarks"
        '
        'tboxOwe
        '
        Me.tboxOwe.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxOwe, "tboxOwe")
        Me.tboxOwe.Name = "tboxOwe"
        Me.tboxOwe.ReadOnly = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Name = "Label7"
        '
        'tboxTodayPay
        '
        Me.tboxTodayPay.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxTodayPay, "tboxTodayPay")
        Me.tboxTodayPay.Name = "tboxTodayPay"
        Me.tboxTodayPay.ReadOnly = True
        '
        'Label6
        '
        Me.Label6.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxDiscRemarks
        '
        resources.ApplyResources(Me.tboxDiscRemarks, "tboxDiscRemarks")
        Me.tboxDiscRemarks.Name = "tboxDiscRemarks"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'butDisDict
        '
        resources.ApplyResources(Me.butDisDict, "butDisDict")
        Me.butDisDict.Name = "butDisDict"
        Me.butDisDict.UseVisualStyleBackColor = True
        '
        'butMkPay
        '
        resources.ApplyResources(Me.butMkPay, "butMkPay")
        Me.butMkPay.Name = "butMkPay"
        Me.butMkPay.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        '
        'grpboxExtra
        '
        Me.grpboxExtra.Controls.Add(Me.lblCheque)
        Me.grpboxExtra.Controls.Add(Me.dtpickCheque)
        Me.grpboxExtra.Controls.Add(Me.tboxPayExtraInfo)
        Me.grpboxExtra.ForeColor = System.Drawing.Color.Blue
        resources.ApplyResources(Me.grpboxExtra, "grpboxExtra")
        Me.grpboxExtra.Name = "grpboxExtra"
        Me.grpboxExtra.TabStop = False
        '
        'lblCheque
        '
        resources.ApplyResources(Me.lblCheque, "lblCheque")
        Me.lblCheque.Name = "lblCheque"
        '
        'dtpickCheque
        '
        resources.ApplyResources(Me.dtpickCheque, "dtpickCheque")
        Me.dtpickCheque.Name = "dtpickCheque"
        '
        'tboxPayExtraInfo
        '
        resources.ApplyResources(Me.tboxPayExtraInfo, "tboxPayExtraInfo")
        Me.tboxPayExtraInfo.Name = "tboxPayExtraInfo"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'frm2PayTogether
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.grpboxExtra)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.tboxDisc)
        Me.Controls.Add(Me.butRemarkDict)
        Me.Controls.Add(Me.tboxReceiptRemarks)
        Me.Controls.Add(Me.tboxOwe)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tboxTodayPay)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxDiscRemarks)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.butDisDict)
        Me.Controls.Add(Me.butMkPay)
        Me.Name = "frm2PayTogether"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.grpboxExtra.ResumeLayout(False)
        Me.grpboxExtra.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpickMakeupDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutCard As System.Windows.Forms.RadioButton
    Friend WithEvents radbutChk As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCash As System.Windows.Forms.RadioButton
    Friend WithEvents radbutTrans As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutNormal As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMakeup As System.Windows.Forms.RadioButton
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tboxDisc As System.Windows.Forms.TextBox
    Friend WithEvents butRemarkDict As System.Windows.Forms.Button
    Friend WithEvents tboxReceiptRemarks As System.Windows.Forms.TextBox
    Friend WithEvents tboxOwe As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxTodayPay As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxDiscRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents butDisDict As System.Windows.Forms.Button
    Friend WithEvents butMkPay As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents grpboxExtra As System.Windows.Forms.GroupBox
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents dtpickCheque As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxPayExtraInfo As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
