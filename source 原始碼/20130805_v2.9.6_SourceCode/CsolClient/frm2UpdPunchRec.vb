﻿Public Class frm2UpdPunchRec
    Dim intId As Integer
    Dim datTime As DateTime
    Dim intIn As Integer

    Public Sub New(ByVal id As Integer, ByVal dt As DateTime, ByVal intype As Integer)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。

        intId = id
        datTime = dt
        intIn = intype

    End Sub

    Private Sub frm2UpdPunchRec_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        dtpickDate.Value = datTime
        tboxHour.Text = GetHourNum(datTime)
        tboxMin.Text = GetMinuteNum(datTime)
        If intIn = 0 Then
            radioIn.Checked = True
        Else
            radioOut.Checked = True
        End If

    End Sub


    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        Try
            datTime = New Date(dtpickDate.Value.Year, dtpickDate.Value.Month, dtpickDate.Value.Day, _
                               CInt(tboxHour.Text.Trim), CInt(tboxMin.Text.Trim), 1)
            If radioIn.Checked Then
                intIn = 0
            Else
                intIn = 1
            End If
            objCsol.UpdPunchRec(intId, datTime, intIn)
            frmMain.SetOkState(True)
            Me.Close()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class