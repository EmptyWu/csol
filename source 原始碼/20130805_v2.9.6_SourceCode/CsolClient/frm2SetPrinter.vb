﻿Public Class frm2SetPrinter

    Private Sub frm2SetPrinter_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        For Each prt In Printing.PrinterSettings.InstalledPrinters
            lstbox.Items.Add(prt)
        Next
        For index As Integer = 0 To lstbox.Items.Count - 1
            If lstbox.Items(index).trim = GetConfigPrinter() Then
                lstbox.SelectedIndex = index
                Exit For
            End If
        Next
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If lstbox.SelectedIndex > -1 Then
            SetConfigPrinter(lstbox.SelectedItem.ToString.Trim)
        End If
        Me.Close()
    End Sub

    Private Sub butContent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butContent.Click
        If lstbox.SelectedIndex > -1 Then
            Dim s As String = lstbox.SelectedItem.ToString.Trim
            If Not s = "" Then
                Dim dlg As New PrintDialog
                dlg.PrinterSettings.PrinterName = lstbox.SelectedItem.ToString.Trim
                dlg.ShowDialog()
            End If
        End If
    End Sub
End Class