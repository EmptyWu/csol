﻿Public Class frm2AddTeleInterviewClass
    Dim dtType As New DataTable
    Dim lstTypeId As New ArrayList
    Dim lstStuIdSelected As New ArrayList
    Dim lstStuId As New ArrayList
    Dim dtStu As New DataTable
    Dim dtClass As New DataTable
    Dim dtSubClass As New DataTable
    Dim lstClass As New ArrayList
    Dim lstSc As New ArrayList
    Dim intClass As Integer = -1
    Dim intSc As Integer = -1

    Private Sub butAddType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddType.Click
        Dim frm As New frm2AddTeleInterviewType
        frm.ShowDialog()
        If frmMain.GetOkState Then
            frmMain.SetOkState(False)
            frmMain.RefreshTeleInterviewType()
            RefreshTypeData()
        End If
    End Sub

    Private Sub frm2AddTeleInterviewClass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        RefreshTypeData()
        refreshClassData()
    End Sub

    Private Sub RefreshClassData()
        dtClass = frmMain.GetClassList
        lstClass.Clear()
        cboxClass.Items.Clear()
        dtSubClass = frmMain.GetSubClassList
        lstSc.Clear()
        cboxSubClass.Items.Clear()
        For index As Integer = 0 To dtClass.Rows.Count - 1
            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).ToString.Trim)
            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        Next
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = 0
        End If

    End Sub

    Private Sub RefreshTypeData()
        dtType = frmMain.GetTeleInterviewType
        lstTypeId.Clear()
        cboxType.Items.Clear()
        For index As Integer = 0 To dtType.Rows.Count - 1
            cboxType.Items.Add(dtType.Rows(index).Item(c_TypeColumnName).ToString.Trim)
            lstTypeId.Add(dtType.Rows(index).Item(c_IDColumnName))
        Next

    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        If cboxClass.SelectedIndex > -1 Then
            intClass = lstClass(cboxClass.SelectedIndex)
            lstSc.Clear()
            cboxSubClass.Items.Clear()
            lstSc.Add(-1)
            cboxSubClass.Items.Add(My.Resources.all)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intClass Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                    lstSc.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.Items.Count > 1 And cboxSubClass.SelectedIndex > -1 Then
            intSc = lstSc(cboxSubClass.SelectedIndex)
            RefreshStuList()
        End If
    End Sub

    Private Sub RefreshStuList()
        If intSc = -1 Then
            If intClass > -1 Then
                dtStu = objCsol.GetStuNameListByClass(intClass)
            End If
        Else
            dtStu = objCsol.GetStuNameListBySc(intSc)
        End If

        chklstStu.Items.Clear()
        lstStuId.Clear()
        lstStuIdSelected.Clear()

        For index As Integer = 0 To dtStu.Rows.Count - 1
            lstStuId.Add(dtStu.Rows(index).Item(c_IDColumnName))
            chklstStu.Items.Add(dtStu.Rows(index).Item(c_IDColumnName) & " " & dtStu.Rows(index).Item(c_NameColumnName).ToString.Trim, _
                                 False)
        Next


    End Sub

    Private Sub butSelAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstStu.Items.Count - 1
            chklstStu.SetItemChecked(index, True)
        Next
    End Sub


    Private Sub butSelNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstStu.Items.Count - 1
            chklstStu.SetItemChecked(index, False)
        Next
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        For index As Integer = 0 To chklstStu.Items.Count - 1
            If chklstStu.GetItemChecked(index) Then
                lstStuIdSelected.Add(lstStuId(index))
            End If
        Next

        If lstStuIdSelected.Count > 0 And _
            cboxType.SelectedIndex > -1 And tboxContent.Text.Trim.Length > 0 And lstTypeId.Count > 0 Then
            objCsol.AddTeleInterview(lstStuIdSelected, dtpickDate.Value, lstTypeId(cboxType.SelectedIndex), _
                                    tboxContent.Text.Trim, frmMain.GetUsrId)
            Me.Close()
        Else
            MsgBox(My.Resources.msgInvalidTeleInterviewInput, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub
End Class