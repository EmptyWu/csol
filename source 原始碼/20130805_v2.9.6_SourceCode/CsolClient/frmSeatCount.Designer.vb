﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeatCount
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSeatCount))
        Me.Label6 = New System.Windows.Forms.Label()
        Me.tboxTime = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker()
        Me.radbutThisTime = New System.Windows.Forms.RadioButton()
        Me.radbutPCTime = New System.Windows.Forms.RadioButton()
        Me.butSetBook = New System.Windows.Forms.Button()
        Me.chkboxBook = New System.Windows.Forms.CheckBox()
        Me.chkboxShowAll = New System.Windows.Forms.CheckBox()
        Me.dgvSeat = New System.Windows.Forms.DataGridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.cboxSubClass = New System.Windows.Forms.ComboBox()
        Me.cboxContent = New System.Windows.Forms.ComboBox()
        Me.cboxClass = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.tboxSeatNa = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.tboxAbsent = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tboxHere = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.chklstBook = New System.Windows.Forms.CheckedListBox()
        Me.cboxSession = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.butSave = New System.Windows.Forms.Button()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.ForeColor = System.Drawing.Color.Red
        Me.Label6.Name = "Label6"
        '
        'tboxTime
        '
        resources.ApplyResources(Me.tboxTime, "tboxTime")
        Me.tboxTime.Name = "tboxTime"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'radbutThisTime
        '
        resources.ApplyResources(Me.radbutThisTime, "radbutThisTime")
        Me.radbutThisTime.Name = "radbutThisTime"
        Me.radbutThisTime.UseVisualStyleBackColor = True
        '
        'radbutPCTime
        '
        resources.ApplyResources(Me.radbutPCTime, "radbutPCTime")
        Me.radbutPCTime.Checked = True
        Me.radbutPCTime.Name = "radbutPCTime"
        Me.radbutPCTime.TabStop = True
        Me.radbutPCTime.UseVisualStyleBackColor = True
        '
        'butSetBook
        '
        resources.ApplyResources(Me.butSetBook, "butSetBook")
        Me.butSetBook.Name = "butSetBook"
        Me.butSetBook.UseVisualStyleBackColor = True
        '
        'chkboxBook
        '
        resources.ApplyResources(Me.chkboxBook, "chkboxBook")
        Me.chkboxBook.Name = "chkboxBook"
        Me.chkboxBook.UseVisualStyleBackColor = True
        '
        'chkboxShowAll
        '
        resources.ApplyResources(Me.chkboxShowAll, "chkboxShowAll")
        Me.chkboxShowAll.Name = "chkboxShowAll"
        Me.chkboxShowAll.UseVisualStyleBackColor = True
        '
        'dgvSeat
        '
        Me.dgvSeat.AllowUserToAddRows = False
        Me.dgvSeat.AllowUserToDeleteRows = False
        Me.dgvSeat.AllowUserToResizeRows = False
        resources.ApplyResources(Me.dgvSeat, "dgvSeat")
        Me.dgvSeat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvSeat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSeat.Name = "dgvSeat"
        Me.dgvSeat.RowHeadersVisible = False
        Me.dgvSeat.RowTemplate.Height = 15
        Me.dgvSeat.RowTemplate.ReadOnly = True
        Me.dgvSeat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSeat.ShowCellToolTips = False
        Me.dgvSeat.ShowEditingIcon = False
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'cboxContent
        '
        Me.cboxContent.FormattingEnabled = True
        resources.ApplyResources(Me.cboxContent, "cboxContent")
        Me.cboxContent.Name = "cboxContent"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'GroupBox3
        '
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Controls.Add(Me.tboxSeatNa)
        Me.GroupBox3.Controls.Add(Me.Label7)
        Me.GroupBox3.Controls.Add(Me.TextBox4)
        Me.GroupBox3.Controls.Add(Me.Label8)
        Me.GroupBox3.Controls.Add(Me.TextBox3)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.tboxAbsent)
        Me.GroupBox3.Controls.Add(Me.Label10)
        Me.GroupBox3.Controls.Add(Me.tboxHere)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'tboxSeatNa
        '
        Me.tboxSeatNa.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxSeatNa, "tboxSeatNa")
        Me.tboxSeatNa.Name = "tboxSeatNa"
        Me.tboxSeatNa.ReadOnly = True
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'TextBox4
        '
        Me.TextBox4.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.TextBox4, "TextBox4")
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.ReadOnly = True
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'TextBox3
        '
        Me.TextBox3.BackColor = System.Drawing.Color.Silver
        resources.ApplyResources(Me.TextBox3, "TextBox3")
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'tboxAbsent
        '
        Me.tboxAbsent.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxAbsent, "tboxAbsent")
        Me.tboxAbsent.Name = "tboxAbsent"
        Me.tboxAbsent.ReadOnly = True
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'tboxHere
        '
        Me.tboxHere.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.tboxHere, "tboxHere")
        Me.tboxHere.Name = "tboxHere"
        Me.tboxHere.ReadOnly = True
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'chklstBook
        '
        Me.chklstBook.CheckOnClick = True
        Me.chklstBook.FormattingEnabled = True
        resources.ApplyResources(Me.chklstBook, "chklstBook")
        Me.chklstBook.Name = "chklstBook"
        '
        'cboxSession
        '
        Me.cboxSession.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSession, "cboxSession")
        Me.cboxSession.Name = "cboxSession"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.ForeColor = System.Drawing.Color.Black
        Me.Label12.Name = "Label12"
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'frmSeatCount
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.cboxSession)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.chklstBook)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxTime)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.radbutThisTime)
        Me.Controls.Add(Me.radbutPCTime)
        Me.Controls.Add(Me.butSetBook)
        Me.Controls.Add(Me.chkboxBook)
        Me.Controls.Add(Me.chkboxShowAll)
        Me.Controls.Add(Me.dgvSeat)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxContent)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frmSeatCount"
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxTime As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutThisTime As System.Windows.Forms.RadioButton
    Friend WithEvents radbutPCTime As System.Windows.Forms.RadioButton
    Friend WithEvents butSetBook As System.Windows.Forms.Button
    Friend WithEvents chkboxBook As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowAll As System.Windows.Forms.CheckBox
    Friend WithEvents dgvSeat As System.Windows.Forms.DataGridView
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxContent As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxSeatNa As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents TextBox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents TextBox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tboxAbsent As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tboxHere As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents chklstBook As System.Windows.Forms.CheckedListBox
    Friend WithEvents cboxSession As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents butSave As System.Windows.Forms.Button
End Class
