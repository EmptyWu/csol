﻿Public Class frm2AddNote

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If tboxID.Text.Trim.Length = 8 And IsNumeric(tboxID.Text.Trim) Then
            If tboxNote.Text.Trim = "" Then
                Exit Sub
            Else
                Dim d1 As Date = dtpickDate.Value
                Dim repeat As Byte = 0
                If radbutRepeat.Checked Then
                    repeat = 1
                End If
                Dim s As Integer = 0
                If IsNumeric(tboxSeconds.Text) Then
                    If CInt(tboxSeconds.Text) > 0 Then
                        s = CInt(tboxSeconds.Text)
                    End If
                End If
                Dim f As Integer = 0
                If radbutEverytime.Checked Then
                    f = 1
                End If
                Dim c As String = tboxNote.Text
                Dim y As Byte = 0
                If radbutInfoBox.Checked Then
                    y = 1
                End If
                objCsol.AddStuNote(tboxID.Text.Trim, d1, repeat, s, f, c, y, _
                                     frmMain.GetUsrId)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        Else
            MsgBox(My.Resources.msgWrongStuID, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

End Class