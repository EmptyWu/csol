﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2Photo
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2Photo))
        Me.picbox = New System.Windows.Forms.PictureBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboxDevices = New System.Windows.Forms.ComboBox
        Me.butSave = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'picbox
        '
        resources.ApplyResources(Me.picbox, "picbox")
        Me.picbox.Name = "picbox"
        Me.picbox.TabStop = False
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'cboxDevices
        '
        Me.cboxDevices.FormattingEnabled = True
        resources.ApplyResources(Me.cboxDevices, "cboxDevices")
        Me.cboxDevices.Name = "cboxDevices"
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2Photo
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.cboxDevices)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.picbox)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2Photo"
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboxDevices As System.Windows.Forms.ComboBox
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
