﻿Namespace DataBaseAccess
    Friend Class WorkStation
        Friend Shared Function IsWorkStationExists(ByVal ID As String) As Boolean
            Return GetWorkStation(ID).Rows.Count = 1
        End Function

        Friend Shared Function CreateWorkStation(ByVal Name As String, ByVal Department As String, ByVal UserID As String) As String
            Dim uuid As String = Guid.NewGuid().ToString("N")
            While IsWorkStationExists(uuid)
                uuid = Guid.NewGuid().ToString("N")
            End While

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID VARCHAR(50), @Name NVARCHAR(50), @Department NVARCHAR(50), @Now datetime, @UserID NVARCHAR(50)")
            sb.AppendLine(String.Format("SELECT @ID = N'{0}', @Name = N'{1}', @Department = N'{2}', @Now = GETDATE(), @UserID = N'{3}'", _
                                        uuid, Name, Department, UserID))
            sb.AppendLine("INSERT INTO WorkStation(ID, Name, Department, CreateDate, CreateBy, ModifyDate, ModifyBy)")
            sb.AppendLine("VALUES(@ID, @Name, @Department, @Now, @UserID, @Now, @UserID)")

            Dim sql As String = sb.ToString()
            'System.Diagnostics.Debug.WriteLine(sql)

            CSOL.DataBase.ExecSql(sql)
            Return uuid
        End Function

        Friend Shared Sub ModifyWorkStation(ByVal ID As String, ByVal Name As String, ByVal Department As String, ByVal UserID As String)
            If IsWorkStationExists(ID) Then
                Dim sb As New System.Text.StringBuilder()
                sb.AppendLine("DECLARE @ID VARCHAR(50), @Name NVARCHAR(50), @Department NVARCHAR(50), @Now datetime, @UserID NVARCHAR(50)")
                sb.AppendLine(String.Format("SELECT @ID = N'{0}', @Name = N'{1}', @Department = N'{2}', @Now = GETDATE(), @UserID = N'{3}'", ID, Name, Department, UserID))
                sb.AppendLine("UPDATE WorkStation SET Name = @Name, Department = @Department, ModifyDate = @Now, ModifyBy = @UserID WHERE ID = @ID")

                Dim sql As String = sb.ToString()
                'System.Diagnostics.Debug.WriteLine(sql)

                CSOL.DataBase.ExecSql(sql)
            End If
        End Sub

        Friend Shared Function GetWorkStation(ByVal ID As String) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID VARCHAR(50)")
            sb.AppendLine(String.Format("SELECT @ID = N'{0}'", ID))
            sb.AppendLine("SELECT Name, Department FROM WorkStation WHERE ID = @ID")

            Dim sql As String = sb.ToString()
            'System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
            Return dt
        End Function

        Friend Shared Function GetDepartment(ByVal ID As String) As String
            Dim dt As DataTable = GetWorkStation(ID)
            If dt.Rows.Count > 0 Then
                Return GetWorkStation(ID).Rows(0)("Department")
            Else
                Return ""
            End If
        End Function

        Friend Shared Function GetDepartments() As String()
            Dim sql As String = "SELECT Department FROM WorkStation GROUP BY Department"
            Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)

            Dim result As New List(Of String)
            For Each row As DataRow In dt.Rows
                result.Add(row("Department"))
            Next
            Return result.ToArray()
        End Function
    End Class
End Namespace