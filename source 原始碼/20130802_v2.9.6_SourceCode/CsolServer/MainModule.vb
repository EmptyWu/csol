﻿Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.Tcp
Imports System.Configuration

Module MainModule
    Friend frmM As frmMain

    Public Sub Main()
        My.User.CurrentPrincipal = New System.Security.Principal.WindowsPrincipal(System.Security.Principal.WindowsIdentity.GetCurrent())

        Dim LogPath As String = String.Format("{0}\Log", My.Application.Info.DirectoryPath)
        If Not System.IO.Directory.Exists(LogPath) Then
            System.IO.Directory.CreateDirectory(LogPath)
        End If
        Csol.CSOL.Logger.Path = LogPath

        Dim prefix As String = My.Settings.Server
        If prefix Is Nothing Then
            prefix = "http://*:8801/"
        End If
        Dim httpserver As New Csol.HttpServer(prefix.Split(";"))
        httpserver.RegisterHandler(New Csol.TestHandler())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.Data())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.StuInfo())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.ClassInfo())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.Accounting())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.AssignDone())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.StuGrade())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.PayStaToday())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.PayStaSubClass())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.StuOweSta())
        httpserver.RegisterHandler(New Csol.HTTPHandlers.JounalRec())


        Dim channel As New TcpServerChannel(GetPort)

        RemotingConfiguration.ApplicationName = "CsolServer"
        ChannelServices.RegisterChannel(channel, False)
        RemotingConfiguration.RegisterWellKnownServiceType( _
            GetType(Csol.CsolBL), "CsolBL", _
            WellKnownObjectMode.SingleCall)

        frmM = New frmMain
        frmM.Show()
        If Not frmM Is Nothing Then
            If frmM.Created = True Then
                System.Windows.Forms.Application.Run(frmM)
            End If
        End If

    End Sub

    Function GetPort() As Integer
        Return CType(My.Settings.Port, Integer)

    End Function

End Module