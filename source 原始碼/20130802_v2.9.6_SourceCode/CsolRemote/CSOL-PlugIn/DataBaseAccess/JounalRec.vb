﻿Namespace DataBaseAccess
    Public Class JounalRec
        Public Shared Function GetJournalStaNoDate() As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT jg.Name As [Group], jr.*, ou.Name AS Handler ")
            sb.AppendLine("FROM JournalRec AS jr ")
            sb.AppendLine("Left outer Join OrgUser AS ou ")
            sb.AppendLine("ON jr.UserID = ou.ID ")
            sb.AppendLine("INNER JOIN JournalGroup AS jg ")
            sb.AppendLine("ON jr.GroupID = jg.ID ")

            Dim sql As String = sb.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function
        'Public Shared Function ListJournalSta() As DataTable
        '    Dim dt As New DataTable
        '    Dim sb As New System.Text.StringBuilder()
        '    sb.AppendLine("SELECT jr.*, ou.Name AS Handler, jg.Name As [Group] ")
        '    sb.AppendLine("FROM JournalRec AS jr ")
        '    sb.AppendLine("Left outer Join OrgUser AS ou ")
        '    sb.AppendLine("ON jr.UserID = ou.ID ")
        '    sb.AppendLine("INNER JOIN JournalGroup AS jg ")
        '    sb.AppendLine("ON jr.GroupID = jg.ID ")

        '    Dim sql As String = sb.ToString()

        '    Try
        '        dt = CSOL.DataBase.LoadToDataTable(sql)
        '    Catch ex As Exception
        '        System.Diagnostics.Debug.Write(ex.ToString())
        '    End Try

        '    Return dt
        'End Function
    End Class
End Namespace