﻿Namespace HTTPHandlers
    Public Class PayStaSubClass
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetListSubClassStaByNoDate"
                    GetListSubClassStaByNoDate()
                Case "GetListSubClassSta"
                    GetListSubClassSta()
                Case "GetdtClassPaySta"
                    GetdtClassPaySta()
                Case "GetdtClassPayStaNoDate"
                    GetdtClassPayStaNoDate()
                Case "GetdtClassMKSta"
                    GetdtClassMKSta()
                Case "GetdtClassMKStaNoDate"
                    GetdtClassMKStaNoDate()
                Case "GetdtClassMBSta"
                    GetdtClassMBSta()
                Case "GetdtClassMBStaNoDate"
                    GetdtClassMBStaNoDate()
                Case "GetListStuOweByClass"
                    GetListStuOweByClass()
                Case "GetListStuDiscByClass"
                    GetListStuDiscByClass()
                Case "GetListStuByClass"
                    GetListStuByClass()


                    

                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/PayStaSubClass"
            End Get
        End Property


        'GetListStuByClass
        Private Sub GetListStuByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetListStuByClass As DataTable = DataBaseAccess.PayStaSubClass.GetListStuByClass(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetListStuByClass", CSOL.Convert.DataTableToXmlString(GetListStuByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetListStuByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub


        Private Sub GetListStuDiscByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetListStuDiscByClass As DataTable = DataBaseAccess.PayStaSubClass.GetListStuDiscByClass(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetListStuDiscByClass", CSOL.Convert.DataTableToXmlString(GetListStuDiscByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetListStuDiscByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListStuOweByClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetListStuOweByClass As DataTable = DataBaseAccess.PayStaSubClass.GetListStuOweByClass(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetListStuOweByClass", CSOL.Convert.DataTableToXmlString(GetListStuOweByClass))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetListStuOweByClass", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub


        Private Sub GetdtClassMBStaNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetdtClassMBStaNoDate As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassMBStaNoDate(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassMBStaNoDate", CSOL.Convert.DataTableToXmlString(GetdtClassMBStaNoDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassMBStaNoDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetdtClassMBSta()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim GetdtClassMBSta As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassMBSta(subclassID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassMBSta", CSOL.Convert.DataTableToXmlString(GetdtClassMBSta))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassMBSta", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetdtClassMKStaNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetdtClassMKStaNoDate As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassMKStaNoDate(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassMKStaNoDate", CSOL.Convert.DataTableToXmlString(GetdtClassMKStaNoDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassMKStaNoDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetdtClassMKSta()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim GetdtClassMKSta As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassMKSta(subclassID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassMKSta", CSOL.Convert.DataTableToXmlString(GetdtClassMKSta))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassMKSta", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub


        Private Sub GetdtClassPayStaNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetdtClassPayStaNoDate As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassPayStaNoDate(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassPayStaNoDate", CSOL.Convert.DataTableToXmlString(GetdtClassPayStaNoDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassPayStaNoDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetdtClassPaySta()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim GetdtClassPaySta As DataTable = DataBaseAccess.PayStaSubClass.GetdtClassPaySta(subclassID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetdtClassPaySta", CSOL.Convert.DataTableToXmlString(GetdtClassPaySta))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetdtClassPaySta", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListSubClassStaByNoDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList

            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim GetListSubClassStaByNoDate As DataTable = DataBaseAccess.PayStaSubClass.GetListSubClassStaByNoDate(subclassID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetListSubClassStaByNoDate", CSOL.Convert.DataTableToXmlString(GetListSubClassStaByNoDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetListSubClassStaByNoDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetListSubClassSta()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim subclassID As New ArrayList
            subclassID.AddRange(RequestParams("subclassID").Split(","))
            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))
            Dim GetListSubClassSta As DataTable = DataBaseAccess.PayStaSubClass.GetListSubClassSta(subclassID, DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetListSubClassSta", CSOL.Convert.DataTableToXmlString(GetListSubClassSta))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetListSubClassSta", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub
       
    End Class
End Namespace
