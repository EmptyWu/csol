﻿Public Class frm2SearchStuSmall
    Private intFrmId As Integer = 0

    Public Sub SetFrmId(ByVal id As Integer)
        intFrmId = id
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch.Click
        If Not tboxID.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 0, tboxID.Text.Trim, 1)
        ElseIf Not tboxName.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 0, tboxName.Text.Trim, 2)
        ElseIf Not tboxTel.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 0, tboxTel.Text.Trim, 3)
        End If
    End Sub


    Private Sub butSearch2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSearch2.Click
        If Not tboxID.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 1, tboxID.Text.Trim, 1)
        ElseIf Not tboxName.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 1, tboxName.Text.Trim, 2)
        ElseIf Not tboxTel.Text.Trim = "" Then
            frmMain.CallSearchFrm(intFrmId, 1, tboxTel.Text.Trim, 3)
        End If
    End Sub

    Private Sub frm2SearchStuSmall_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Me.WindowState = FormWindowState.Normal
    End Sub
End Class