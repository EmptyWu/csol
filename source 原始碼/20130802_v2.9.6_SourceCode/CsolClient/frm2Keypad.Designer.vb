﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2Keypad
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2Keypad))
        Me.butEnter = New System.Windows.Forms.Button
        Me.but0 = New System.Windows.Forms.Button
        Me.but9 = New System.Windows.Forms.Button
        Me.but8 = New System.Windows.Forms.Button
        Me.but7 = New System.Windows.Forms.Button
        Me.but6 = New System.Windows.Forms.Button
        Me.but5 = New System.Windows.Forms.Button
        Me.but4 = New System.Windows.Forms.Button
        Me.but3 = New System.Windows.Forms.Button
        Me.but2 = New System.Windows.Forms.Button
        Me.but1 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'butEnter
        '
        resources.ApplyResources(Me.butEnter, "butEnter")
        Me.butEnter.Name = "butEnter"
        Me.butEnter.UseVisualStyleBackColor = True
        '
        'but0
        '
        resources.ApplyResources(Me.but0, "but0")
        Me.but0.Name = "but0"
        Me.but0.UseVisualStyleBackColor = True
        '
        'but9
        '
        resources.ApplyResources(Me.but9, "but9")
        Me.but9.Name = "but9"
        Me.but9.UseVisualStyleBackColor = True
        '
        'but8
        '
        resources.ApplyResources(Me.but8, "but8")
        Me.but8.Name = "but8"
        Me.but8.UseVisualStyleBackColor = True
        '
        'but7
        '
        resources.ApplyResources(Me.but7, "but7")
        Me.but7.Name = "but7"
        Me.but7.UseVisualStyleBackColor = True
        '
        'but6
        '
        resources.ApplyResources(Me.but6, "but6")
        Me.but6.Name = "but6"
        Me.but6.UseVisualStyleBackColor = True
        '
        'but5
        '
        resources.ApplyResources(Me.but5, "but5")
        Me.but5.Name = "but5"
        Me.but5.UseVisualStyleBackColor = True
        '
        'but4
        '
        resources.ApplyResources(Me.but4, "but4")
        Me.but4.Name = "but4"
        Me.but4.UseVisualStyleBackColor = True
        '
        'but3
        '
        resources.ApplyResources(Me.but3, "but3")
        Me.but3.Name = "but3"
        Me.but3.UseVisualStyleBackColor = True
        '
        'but2
        '
        resources.ApplyResources(Me.but2, "but2")
        Me.but2.Name = "but2"
        Me.but2.UseVisualStyleBackColor = True
        '
        'but1
        '
        resources.ApplyResources(Me.but1, "but1")
        Me.but1.Name = "but1"
        Me.but1.UseVisualStyleBackColor = True
        '
        'frm2Keypad
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.butEnter)
        Me.Controls.Add(Me.but0)
        Me.Controls.Add(Me.but9)
        Me.Controls.Add(Me.but8)
        Me.Controls.Add(Me.but7)
        Me.Controls.Add(Me.but6)
        Me.Controls.Add(Me.but5)
        Me.Controls.Add(Me.but4)
        Me.Controls.Add(Me.but3)
        Me.Controls.Add(Me.but2)
        Me.Controls.Add(Me.but1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2Keypad"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents butEnter As System.Windows.Forms.Button
    Friend WithEvents but0 As System.Windows.Forms.Button
    Friend WithEvents but9 As System.Windows.Forms.Button
    Friend WithEvents but8 As System.Windows.Forms.Button
    Friend WithEvents but7 As System.Windows.Forms.Button
    Friend WithEvents but6 As System.Windows.Forms.Button
    Friend WithEvents but5 As System.Windows.Forms.Button
    Friend WithEvents but4 As System.Windows.Forms.Button
    Friend WithEvents but3 As System.Windows.Forms.Button
    Friend WithEvents but2 As System.Windows.Forms.Button
    Friend WithEvents but1 As System.Windows.Forms.Button
End Class
