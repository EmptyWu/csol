﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ImportSch
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2ImportSch))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.radbutAccess = New System.Windows.Forms.RadioButton
        Me.radbutExcel = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.butFile = New System.Windows.Forms.Button
        Me.tboxFile = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.cboxTable = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.radbutUni = New System.Windows.Forms.RadioButton
        Me.radbutHig = New System.Windows.Forms.RadioButton
        Me.radbutJun = New System.Windows.Forms.RadioButton
        Me.radbutPri = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.cboxContact = New System.Windows.Forms.ComboBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.cboxAddr = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboxTel = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboxName = New System.Windows.Forms.ComboBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.butImport = New System.Windows.Forms.Button
        Me.butClose = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radbutAccess)
        Me.GroupBox1.Controls.Add(Me.radbutExcel)
        Me.GroupBox1.Controls.Add(Me.Label1)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'radbutAccess
        '
        resources.ApplyResources(Me.radbutAccess, "radbutAccess")
        Me.radbutAccess.Name = "radbutAccess"
        Me.radbutAccess.UseVisualStyleBackColor = True
        '
        'radbutExcel
        '
        resources.ApplyResources(Me.radbutExcel, "radbutExcel")
        Me.radbutExcel.Checked = True
        Me.radbutExcel.Name = "radbutExcel"
        Me.radbutExcel.TabStop = True
        Me.radbutExcel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.butFile)
        Me.GroupBox2.Controls.Add(Me.tboxFile)
        Me.GroupBox2.Controls.Add(Me.Label2)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'butFile
        '
        resources.ApplyResources(Me.butFile, "butFile")
        Me.butFile.Name = "butFile"
        Me.butFile.UseVisualStyleBackColor = True
        '
        'tboxFile
        '
        resources.ApplyResources(Me.tboxFile, "tboxFile")
        Me.tboxFile.Name = "tboxFile"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.cboxTable)
        Me.GroupBox3.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'cboxTable
        '
        Me.cboxTable.FormattingEnabled = True
        resources.ApplyResources(Me.cboxTable, "cboxTable")
        Me.cboxTable.Name = "cboxTable"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgv)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Tag = ""
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column3, Me.Column4, Me.Column2})
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        '
        'Column1
        '
        resources.ApplyResources(Me.Column1, "Column1")
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column3
        '
        resources.ApplyResources(Me.Column3, "Column3")
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        '
        'Column4
        '
        resources.ApplyResources(Me.Column4, "Column4")
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        '
        'Column2
        '
        resources.ApplyResources(Me.Column2, "Column2")
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.radbutUni)
        Me.GroupBox5.Controls.Add(Me.radbutHig)
        Me.GroupBox5.Controls.Add(Me.radbutJun)
        Me.GroupBox5.Controls.Add(Me.radbutPri)
        Me.GroupBox5.Controls.Add(Me.Label5)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'radbutUni
        '
        resources.ApplyResources(Me.radbutUni, "radbutUni")
        Me.radbutUni.Name = "radbutUni"
        Me.radbutUni.TabStop = True
        Me.radbutUni.UseVisualStyleBackColor = True
        '
        'radbutHig
        '
        resources.ApplyResources(Me.radbutHig, "radbutHig")
        Me.radbutHig.Name = "radbutHig"
        Me.radbutHig.TabStop = True
        Me.radbutHig.UseVisualStyleBackColor = True
        '
        'radbutJun
        '
        resources.ApplyResources(Me.radbutJun, "radbutJun")
        Me.radbutJun.Name = "radbutJun"
        Me.radbutJun.TabStop = True
        Me.radbutJun.UseVisualStyleBackColor = True
        '
        'radbutPri
        '
        resources.ApplyResources(Me.radbutPri, "radbutPri")
        Me.radbutPri.Checked = True
        Me.radbutPri.Name = "radbutPri"
        Me.radbutPri.TabStop = True
        Me.radbutPri.UseVisualStyleBackColor = True
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.cboxContact)
        Me.GroupBox6.Controls.Add(Me.Label8)
        Me.GroupBox6.Controls.Add(Me.cboxAddr)
        Me.GroupBox6.Controls.Add(Me.Label7)
        Me.GroupBox6.Controls.Add(Me.cboxTel)
        Me.GroupBox6.Controls.Add(Me.Label4)
        Me.GroupBox6.Controls.Add(Me.cboxName)
        Me.GroupBox6.Controls.Add(Me.Label6)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'cboxContact
        '
        Me.cboxContact.FormattingEnabled = True
        Me.cboxContact.Items.AddRange(New Object() {resources.GetString("cboxContact.Items"), resources.GetString("cboxContact.Items1"), resources.GetString("cboxContact.Items2"), resources.GetString("cboxContact.Items3")})
        resources.ApplyResources(Me.cboxContact, "cboxContact")
        Me.cboxContact.Name = "cboxContact"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'cboxAddr
        '
        Me.cboxAddr.FormattingEnabled = True
        Me.cboxAddr.Items.AddRange(New Object() {resources.GetString("cboxAddr.Items"), resources.GetString("cboxAddr.Items1"), resources.GetString("cboxAddr.Items2"), resources.GetString("cboxAddr.Items3")})
        resources.ApplyResources(Me.cboxAddr, "cboxAddr")
        Me.cboxAddr.Name = "cboxAddr"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'cboxTel
        '
        Me.cboxTel.FormattingEnabled = True
        Me.cboxTel.Items.AddRange(New Object() {resources.GetString("cboxTel.Items"), resources.GetString("cboxTel.Items1"), resources.GetString("cboxTel.Items2"), resources.GetString("cboxTel.Items3")})
        resources.ApplyResources(Me.cboxTel, "cboxTel")
        Me.cboxTel.Name = "cboxTel"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'cboxName
        '
        Me.cboxName.FormattingEnabled = True
        Me.cboxName.Items.AddRange(New Object() {resources.GetString("cboxName.Items"), resources.GetString("cboxName.Items1"), resources.GetString("cboxName.Items2"), resources.GetString("cboxName.Items3")})
        resources.ApplyResources(Me.cboxName, "cboxName")
        Me.cboxName.Name = "cboxName"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'butImport
        '
        resources.ApplyResources(Me.butImport, "butImport")
        Me.butImport.Name = "butImport"
        Me.butImport.UseVisualStyleBackColor = True
        '
        'butClose
        '
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'frm2ImportSch
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.butImport)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ImportSch"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutAccess As System.Windows.Forms.RadioButton
    Friend WithEvents radbutExcel As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutJun As System.Windows.Forms.RadioButton
    Friend WithEvents radbutPri As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents butImport As System.Windows.Forms.Button
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents butFile As System.Windows.Forms.Button
    Friend WithEvents tboxFile As System.Windows.Forms.TextBox
    Friend WithEvents cboxTable As System.Windows.Forms.ComboBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents radbutUni As System.Windows.Forms.RadioButton
    Friend WithEvents radbutHig As System.Windows.Forms.RadioButton
    Friend WithEvents cboxContact As System.Windows.Forms.ComboBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents cboxAddr As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboxTel As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboxName As System.Windows.Forms.ComboBox
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
