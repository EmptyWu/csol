﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2Subjects
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butDelete = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.dgvS = New System.Windows.Forms.DataGridView
        CType(Me.dgvS, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'butDelete
        '
        Me.butDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelete.Location = New System.Drawing.Point(300, 32)
        Me.butDelete.Name = "butDelete"
        Me.butDelete.Size = New System.Drawing.Size(75, 29)
        Me.butDelete.TabIndex = 7
        Me.butDelete.Text = "刪除"
        Me.butDelete.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(297, 335)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(75, 29)
        Me.butCancel.TabIndex = 6
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(297, 286)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(75, 29)
        Me.butSave.TabIndex = 5
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dgvS
        '
        Me.dgvS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvS.Location = New System.Drawing.Point(12, 12)
        Me.dgvS.MultiSelect = False
        Me.dgvS.Name = "dgvS"
        Me.dgvS.RowTemplate.Height = 24
        Me.dgvS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvS.Size = New System.Drawing.Size(262, 352)
        Me.dgvS.TabIndex = 4
        '
        'frm2Subjects
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(389, 381)
        Me.Controls.Add(Me.butDelete)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.dgvS)
        Me.Name = "frm2Subjects"
        Me.Text = "科別"
        CType(Me.dgvS, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents butDelete As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents dgvS As System.Windows.Forms.DataGridView
End Class
