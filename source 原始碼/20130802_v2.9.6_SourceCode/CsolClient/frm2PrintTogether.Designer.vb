﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2PrintTogether
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.radbutShowToday = New System.Windows.Forms.RadioButton
        Me.radbutShowAll = New System.Windows.Forms.RadioButton
        Me.Label1 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.butPrint = New System.Windows.Forms.Button
        Me.butSelectNone = New System.Windows.Forms.Button
        Me.butSelectAll1 = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.prtdocReceipt = New System.Drawing.Printing.PrintDocument
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'radbutShowToday
        '
        Me.radbutShowToday.AutoSize = True
        Me.radbutShowToday.Checked = True
        Me.radbutShowToday.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutShowToday.Location = New System.Drawing.Point(12, 8)
        Me.radbutShowToday.Name = "radbutShowToday"
        Me.radbutShowToday.Size = New System.Drawing.Size(155, 16)
        Me.radbutShowToday.TabIndex = 71
        Me.radbutShowToday.TabStop = True
        Me.radbutShowToday.Text = "只顯示今日有繳費的清單"
        Me.radbutShowToday.UseVisualStyleBackColor = True
        '
        'radbutShowAll
        '
        Me.radbutShowAll.AutoSize = True
        Me.radbutShowAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutShowAll.Location = New System.Drawing.Point(182, 8)
        Me.radbutShowAll.Name = "radbutShowAll"
        Me.radbutShowAll.Size = New System.Drawing.Size(131, 16)
        Me.radbutShowAll.TabIndex = 72
        Me.radbutShowAll.Text = "顯示全部的繳費清單"
        Me.radbutShowAll.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.Color.Red
        Me.Label1.Location = New System.Drawing.Point(13, 524)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(287, 12)
        Me.Label1.TabIndex = 74
        Me.Label1.Text = "一次收據最多只能印8列，超過的部分會印到第二張。"
        '
        'butCancel
        '
        Me.butCancel.AutoSize = True
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(113, 542)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(80, 32)
        Me.butCancel.TabIndex = 88
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butPrint
        '
        Me.butPrint.AutoSize = True
        Me.butPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPrint.Location = New System.Drawing.Point(12, 542)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(80, 32)
        Me.butPrint.TabIndex = 87
        Me.butPrint.Text = "合併列印"
        Me.butPrint.UseVisualStyleBackColor = True
        '
        'butSelectNone
        '
        Me.butSelectNone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSelectNone.Location = New System.Drawing.Point(78, 30)
        Me.butSelectNone.Name = "butSelectNone"
        Me.butSelectNone.Size = New System.Drawing.Size(60, 25)
        Me.butSelectNone.TabIndex = 144
        Me.butSelectNone.Text = "全不選"
        Me.butSelectNone.UseVisualStyleBackColor = True
        '
        'butSelectAll1
        '
        Me.butSelectAll1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSelectAll1.Location = New System.Drawing.Point(12, 30)
        Me.butSelectAll1.Name = "butSelectAll1"
        Me.butSelectAll1.Size = New System.Drawing.Size(60, 25)
        Me.butSelectAll1.TabIndex = 143
        Me.butSelectAll1.Text = "全選"
        Me.butSelectAll1.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(15, 65)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgv.Size = New System.Drawing.Size(670, 456)
        Me.dgv.TabIndex = 145
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.Location = New System.Drawing.Point(348, 8)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 146
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'frm2PrintTogether
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(694, 586)
        Me.ControlBox = False
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.butSelectNone)
        Me.Controls.Add(Me.butSelectAll1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butPrint)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.radbutShowToday)
        Me.Controls.Add(Me.radbutShowAll)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2PrintTogether"
        Me.ShowIcon = False
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "合併列印"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents radbutShowToday As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butPrint As System.Windows.Forms.Button
    Friend WithEvents butSelectNone As System.Windows.Forms.Button
    Friend WithEvents butSelectAll1 As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents prtdocReceipt As System.Drawing.Printing.PrintDocument
End Class
