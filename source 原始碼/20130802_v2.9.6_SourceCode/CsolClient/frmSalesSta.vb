﻿Public Class frmSalesSta
    Private dtEmployeeList As New DataTable
    Private dtSubClassSta As New DataTable
    Private strUsrId As String
    Private lstEmployee As New ArrayList
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dateNow As Date = Now
    Private dtPaySta As New DataTable
    Private dtDisc As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmSalesSta
    Private sUserName As String = frmMain.GetUsrName
    Private getReturn As Boolean = False

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSalesSta
        'InitEmployeeList()
        'InitClassList()
        'ShowClassList()
        'RefreshData()
        dtClassList = Nothing
        dtEmployeeList = Nothing
        dtSubClassList = Nothing
        getReturn = False

        Dim t As New System.Threading.Thread(AddressOf SalesStaList)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Sub SalesStaList()
        While Not getReturn
            If Me.InvokeRequired Then
                InitClassList()
                Me.Invoke(New SalesStaListDelegate(AddressOf SalesStaList))
            Else
                If dtEmployeeList Is Nothing Then
                Else
                    InitEmployeeList()
                    ShowClassList()
                    getReturn = True
                End If
            End If
        End While
    End Sub

    Private Delegate Sub SalesStaListDelegate()

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        PrintDocument2.DefaultPageSettings.Landscape = True
        PrintDocument2.Print()
    End Sub

    Private Sub frmSalesSta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)

        If Not IsDisposed Then
            Me.Dispose()
        End If
    End Sub

    Private Sub frmSalesSta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intSubClass As Integer
        Dim strStuId As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"

        Try
            'CSOL.Logger.LogMessage("frmSalesSta1: " & Now.ToString())
            If radbutByDate.Checked = True Then
                dtPaySta = objCsol.ListPersonalSales(strUsrId, lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                'CSOL.Logger.LogMessage("frmSalesSta2: " & Now.ToString())
                dtDisc = objCsol.ListPersonalDisc(strUsrId, lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                'CSOL.Logger.LogMessage("frmSalesSta3: " & Now.ToString())
            ElseIf radbutByDateNo.Checked = True Then
                dtPaySta = objCsol.ListPersonalSales(strUsrId, lstClass)
                'CSOL.Logger.LogMessage("frmSalesSta4: " & Now.ToString())
                dtDisc = objCsol.ListPersonalDisc(strUsrId, lstClass)
                'CSOL.Logger.LogMessage("frmSalesSta5: " & Now.ToString())
            End If

            dtMaster = New DataTable()
            dtDetails = New DataTable()
            dtDetails = dtPaySta.DefaultView.ToTable(True, c_SubClassIDColumnName, _
                                c_StuIDColumnName, c_StuNameColumnName, c_SubClassNameColumnName, _
                                c_SeatNumColumnName)
            'CSOL.Logger.LogMessage("frmSalesSta6: " & Now.ToString())
            InitTable()
            'CSOL.Logger.LogMessage("frmSalesSta7: " & Now.ToString())

            'dtSubClassSta = objCsol.ListSubClassSta(lstSubClass)
            dtSubClassSta = objCsol.ListSubClassSta(lstSubClass)

            'CSOL.Logger.LogMessage("frmSalesSta8: " & Now.ToString())
            For index As Integer = 0 To dtSubClassSta.Rows.Count - 1
                intSubClass = dtSubClassSta.Rows(index).Item(c_IDColumnName)
                If lstSubClass.IndexOf(intSubClass) > -1 Then
                    intTotal = dtSubClassSta.Rows(index).Item(c_StuRegCntColumnName)
                    intTotal2 = dtSubClassSta.Rows(index).Item(c_FeeClrCntColumnName)
                    dtMaster.Rows.Add(dtSubClassSta.Rows(index).Item(c_IDColumnName), _
                                      dtSubClassSta.Rows(index).Item(c_ClassNameColumnName), _
                                      dtSubClassSta.Rows(index).Item(c_NameColumnName), _
                                      intTotal, intTotal2, intTotal - intTotal2)
                End If
            Next
            'CSOL.Logger.LogMessage("frmSalesSta9: " & Now.ToString())

            For i = 0 To dtDetails.Rows.Count - 1
                intSubClass = dtDetails.Rows(i).Item(c_SubClassIDColumnName)
                strStuId = dtDetails.Rows(i).Item(c_StuIDColumnName)
                intTotal = frmMain.GetClassFee(intSubClass)
                strCompute2 = c_SubClassIDColumnName & "=" & intSubClass.ToString & _
                                " AND " & c_StuIDColumnName & "=" & strStuId
                If dtPaySta.Compute(strCompute3, strCompute2) > 0 Then
                    intTotal2 = dtPaySta.Compute(strCompute5, strCompute2) 'discount
                    intTotal3 = dtPaySta.Compute(strCompute1, strCompute2) 'pay
                    intTotal4 = dtPaySta.Compute(strCompute4, strCompute2) 'back
                Else
                    intTotal2 = 0
                    intTotal3 = 0
                    intTotal4 = 0
                End If

                If dtDisc.Compute(strCompute6, strCompute2) > 0 Then
                    intTotal2 = intTotal2 + dtDisc.Compute(strCompute5, strCompute2) 'discount
                End If

                dtDetails.Rows(i).Item(c_AmountColumnName) = intTotal - intTotal2
                dtDetails.Rows(i).Item(c_PayAmountColumnName) = intTotal3 - intTotal4
                dtDetails.Rows(i).Item(c_FeeOweColumnName) = intTotal - intTotal2 - _
                                intTotal3 + intTotal4
                dtDetails.Rows(i).Item(c_DiscountColumnName) = intTotal2
            Next
            'CSOL.Logger.LogMessage("frmSalesSta10: " & Now.ToString())

            dgvMaster.DataSource = dtMaster
            If dgvMaster.RowCount > 0 Then
                'dgvMaster.CurrentCell = dgvMaster.Rows.Item(0).Cells(c_NameColumnName)
                intSubClass = dtMaster.Rows(0).Item(c_IDColumnName)
                dtDetails.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intSubClass.ToString
            Else
                dtDetails.DefaultView.RowFilter = ""
            End If
            'CSOL.Logger.LogMessage("frmSalesSta11: " & Now.ToString())

            dgvDetails.DataSource = dtDetails.DefaultView
            dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_FeeOweColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()
            'CSOL.Logger.LogMessage("frmSalesSta12: " & Now.ToString())

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitClassList()
        Try
            If dtClassList Is Nothing Then
                dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                 c_NameColumnName, c_EndColumnName)
            End If

            If dtSubClassList Is Nothing Then
                dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)
            End If

            If dtEmployeeList Is Nothing Then
                dtEmployeeList = objCsol.ListEmployee()
            End If
            'dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
            ' c_NameColumnName, c_EndColumnName)
            'dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitEmployeeList()
        Try
            Dim strId As String
            Dim strName As String
            'dtEmployeeList = objCsol.ListEmployee()

            'For index As Integer = 0 To dtEmployeeList.Rows.Count - 1
            '    strId = dtEmployeeList.Rows(index).Item(c_IDColumnName)
            '    strName = dtEmployeeList.Rows(index).Item(c_NameColumnName)
            '    cboxUser.Items.Add(strId & " " & strName)
            '    lstEmployee.Add(strId)
            'Next

            '1213 updated by sherry start
            If frmMain.GetUsrId.ToUpper.Equals("ADMIN") Then
                For index As Integer = 0 To dtEmployeeList.Rows.Count - 1
                    strId = dtEmployeeList.Rows(index).Item(c_IDColumnName)
                    strName = dtEmployeeList.Rows(index).Item(c_NameColumnName)
                    cboxUser.Items.Add(strId & " " & strName)
                    lstEmployee.Add(strId)
                Next
            Else
                'For index As Integer = 0 To dtEmployeeList.Rows.Count - 1
                strId = frmMain.GetUsrId
                strName = frmMain.GetUsrName
                cboxUser.Items.Add(strId & " " & strName)
                lstEmployee.Add(strId)
                'Next
                'Label3.Visible = False
                'cboxUser.Visible = False
            End If
            '1213 updated by sherry end


            If cboxUser.Items.Count > 0 Then
                cboxUser.SelectedIndex = 0
                strUsrId = lstEmployee(cboxUser.SelectedIndex)
            Else
                strUsrId = ""
            End If

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        chklstSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()
            RefreshClassList()
            RefreshSubClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_IDColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_ClassNameColumnName, GetType(System.String))
        dtMaster.Columns.Add(c_NameColumnName, GetType(System.String))
        dtMaster.Columns.Add(c_StuRegCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeClrCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeOweCntColumnName, GetType(System.Int32))

        dtDetails.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvMaster.Columns
            col.visible = False
        Next
        If dgvMaster.ColumnCount > 0 Then

            dgvMaster.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvMaster.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvMaster.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvMaster.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvMaster.Columns.Item(c_NameColumnName).Visible = True
            dgvMaster.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
            dgvMaster.Columns.Item(c_StuRegCntColumnName).DisplayIndex = 2
            dgvMaster.Columns.Item(c_StuRegCntColumnName).Visible = True
            dgvMaster.Columns.Item(c_StuRegCntColumnName).HeaderText = My.Resources.regCnt
            dgvMaster.Columns.Item(c_FeeClrCntColumnName).DisplayIndex = 3
            dgvMaster.Columns.Item(c_FeeClrCntColumnName).Visible = True
            dgvMaster.Columns.Item(c_FeeClrCntColumnName).HeaderText = My.Resources.clearCnt
            dgvMaster.Columns.Item(c_FeeOweCntColumnName).DisplayIndex = 4
            dgvMaster.Columns.Item(c_FeeOweCntColumnName).Visible = True
            dgvMaster.Columns.Item(c_FeeOweCntColumnName).HeaderText = My.Resources.oweCnt
        End If
        For Each col In dgvDetails.Columns
            col.visible = False
        Next
        If dgvDetails.ColumnCount > 0 Then

            dgvDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvDetails.Columns.Item(c_StuIDColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvDetails.Columns.Item(c_StuNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgvDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvDetails.Columns.Item(c_SeatNumColumnName).DisplayIndex = 3
            dgvDetails.Columns.Item(c_SeatNumColumnName).Visible = True
            dgvDetails.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 4
            dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.shouldPay
            dgvDetails.Columns.Item(c_PayAmountColumnName).DisplayIndex = 5
            dgvDetails.Columns.Item(c_PayAmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_PayAmountColumnName).HeaderText = My.Resources.paid
            dgvDetails.Columns.Item(c_FeeOweColumnName).DisplayIndex = 6
            dgvDetails.Columns.Item(c_FeeOweColumnName).Visible = True
            dgvDetails.Columns.Item(c_FeeOweColumnName).HeaderText = My.Resources.feeOwe
            dgvDetails.Columns.Item(c_DiscountColumnName).DisplayIndex = 7
            dgvDetails.Columns.Item(c_DiscountColumnName).Visible = True
            dgvDetails.Columns.Item(c_DiscountColumnName).HeaderText = My.Resources.discount
        End If
    End Sub


    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            chklstClass.SetItemChecked(0, True)
            chklstClass.SelectedItem = chklstClass.Items(0)

            If chklstSubClass.Items.Count > 0 Then
                chklstSubClass.SetItemChecked(0, True)
                chklstSubClass.SelectedItem = chklstSubClass.Items(0)
            End If
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmSalesSta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click
        If frmMain.CheckAuth(52) = False Then
            If strUsrId <> frmMain.GetUsrId Then
                MsgBox(My.Resources.msgNoAuth, _
                           MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                Exit Sub
            End If
        End If
        RefreshData()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intClassId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intClassId = dgvMaster.SelectedRows(0).Cells(c_IDColumnName).Value
            If intClassId > -1 Then
                dtDetails.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intClassId.ToString
                dgvDetails.DataSource = dtDetails.DefaultView
            Else
                dtDetails.DefaultView.RowFilter = ""
                dgvDetails.DataSource = dtDetails.DefaultView
            End If
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        Else
            lstClass.Remove(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    lstSubClass.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        End If
    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        Dim intC As Integer
        intC = lstSubClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstSubClass.Add(intC)
        Else
            lstSubClass.Remove(intC)
        End If
    End Sub

    Private Sub cboxUser_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxUser.SelectedIndexChanged
        If cboxUser.SelectedIndex > -1 And cboxUser.SelectedIndex < lstEmployee.Count Then
            strUsrId = lstEmployee(cboxUser.SelectedIndex)
            InitClassList()
            ShowClassList()
            RefreshData()
            LoadColumnText()
        End If
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        ExportDgvToExcel(dgvMaster)
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        ExportDgvToExcel(dgvDetails)
    End Sub
End Class