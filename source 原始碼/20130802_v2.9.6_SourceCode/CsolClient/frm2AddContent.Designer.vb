﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddContent
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2AddContent))
        Me.Label2 = New System.Windows.Forms.Label
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxCont = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.radbutAll = New System.Windows.Forms.RadioButton
        Me.radbutSelectedOnly = New System.Windows.Forms.RadioButton
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'tboxCont
        '
        resources.ApplyResources(Me.tboxCont, "tboxCont")
        Me.tboxCont.Name = "tboxCont"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'radbutAll
        '
        resources.ApplyResources(Me.radbutAll, "radbutAll")
        Me.radbutAll.Name = "radbutAll"
        Me.radbutAll.UseVisualStyleBackColor = True
        '
        'radbutSelectedOnly
        '
        resources.ApplyResources(Me.radbutSelectedOnly, "radbutSelectedOnly")
        Me.radbutSelectedOnly.Checked = True
        Me.radbutSelectedOnly.Name = "radbutSelectedOnly"
        Me.radbutSelectedOnly.TabStop = True
        Me.radbutSelectedOnly.UseVisualStyleBackColor = True
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstSubClass, "chklstSubClass")
        Me.chklstSubClass.Name = "chklstSubClass"
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'frm2AddContent
        '
        Me.AcceptButton = Me.butSave
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.chklstSubClass)
        Me.Controls.Add(Me.radbutSelectedOnly)
        Me.Controls.Add(Me.radbutAll)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tboxCont)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddContent"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxCont As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents radbutAll As System.Windows.Forms.RadioButton
    Friend WithEvents radbutSelectedOnly As System.Windows.Forms.RadioButton
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
End Class
