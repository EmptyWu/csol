﻿Public Class frmStuDoneAssign
    Private dtAssignment As New DataTable
    Private dtAssignment2 As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstAssignment As New ArrayList

    Private dtData As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Private intSearchIndex As Integer = 0
    Private strKw As String = ""

    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuDoneAssign
    Private sUserName As String = frmMain.GetUsrName

    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmStuDoneAssign
            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)

            InitSelections()
            InitList()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If GetDateEnd(dtClass.Rows(index).Item(c_EndColumnName)) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        If lstClass.Count > 0 Then
            cboxClass.SelectedIndex = 0
        Else
            cboxClass.SelectedIndex = -1
        End If
        RefreshScAndAssignList()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshScAndAssignList()
    End Sub

    Private Sub RefreshScAndAssignList()
        Try
            cboxSubClass.Items.Clear()
            lstSubClass.Clear()
            cboxAssignment.Items.Clear()
            lstAssignment.Clear()
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            If lstClass.Count = 0 Then
                Exit Sub
            End If
            intClass = lstClass(cboxClass.SelectedIndex)
            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClass.Add(row.Item(c_IDColumnName))
                        cboxSubClass.Items.Add(row.Item(c_NameColumnName))
                    End If
                Next

                dtAssignment = objCsol.GetClassAssignmentList(intClass)
                dtAssignment2 = objCsol.GetClassAssignmentList(intClass).DefaultView.ToTable(True, c_IDColumnName, _
                                                                    c_NameColumnName, c_EndDateColumnName)
                If dtAssignment.Rows.Count = 0 Then
                    dgv.DataSource = Nothing
                End If
            End If

            cboxSubClass.SelectedIndex = 0

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub


    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        RefreshAssignmentList()
    End Sub

    Private Sub RefreshAssignmentList()
        Try
            cboxAssignment.Items.Clear()
            lstAssignment.Clear()
            cboxAssignment.Text = ""
            dgv.DataSource = Nothing

            If cboxSubClass.SelectedIndex > -1 Then
                If cboxSubClass.SelectedIndex = 0 Then
                    If chkboxShowPastAssign.Checked Then
                        For Each row As DataRow In dtAssignment2.Rows
                            cboxAssignment.Items.Add(row.Item(c_NameColumnName) & "~" & GetDateNum(GetDbDate(row.Item(c_EndDateColumnName))))
                            lstAssignment.Add(row.Item(c_IDColumnName))
                        Next
                    Else
                        For Each row As DataRow In dtAssignment2.Rows
                            If GetDateEnd(row.Item(c_EndDateColumnName)) >= Now Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName) & "~" & GetDateNum(GetDbDate(row.Item(c_EndDateColumnName))))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    End If
                Else
                    Dim id As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                    If chkboxShowPastAssign.Checked Then
                        For Each row As DataRow In dtAssignment.Rows
                            If row.Item(c_SubClassIDColumnName) = id Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName) & "~" & GetDateNum(GetDbDate(row.Item(c_EndDateColumnName))))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    Else
                        For Each row As DataRow In dtAssignment.Rows
                            If GetDateEnd(row.Item(c_EndDateColumnName)) >= Now And row.Item(c_SubClassIDColumnName) = id Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName) & "~" & GetDateNum(GetDbDate(row.Item(c_EndDateColumnName))))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    End If
                End If
                If lstAssignment.Count > 0 Then
                    cboxAssignment.SelectedIndex = 0
                End If
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub cboxAssignment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxAssignment.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub RefreshData()
        Try
            If cboxAssignment.SelectedIndex > -1 Then
                Dim Assignment As Integer = lstAssignment(cboxAssignment.SelectedIndex)
                Dim lst As New ArrayList
                Dim sc As Integer = 0
                If cboxSubClass.SelectedIndex = 0 Then
                    For i As Integer = 1 To lstSubClass.Count - 1
                        sc = lstSubClass(i)
                        If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                            lst.Add(sc)
                        End If
                    Next
                ElseIf cboxSubClass.SelectedIndex > 0 Then
                    sc = lstSubClass(cboxSubClass.SelectedIndex)
                    If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                       c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                        lst.Add(sc)
                    End If
                End If
                dtData = objCsol.GetStuDoneAssignmentList(Assignment, lst)

                If cboxSubClass.SelectedIndex > 0 Then
                    dtData.DefaultView.RowFilter = c_SubClassIDColumnName & "=" & lstSubClass(cboxSubClass.SelectedIndex).ToString
                End If
                dgv.DataSource = dtData.DefaultView
                LoadColumnText()

            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub LoadColumnText()
        Try
            For Each col In dgv.Columns
                col.visible = False
            Next
            Dim strSch As String
            If dgv.Rows.Count > 0 Then
                dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_IDColumnName).Visible = True
                dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID

                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name

                dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_Tel1ColumnName).Visible = True
                dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1

                dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
                dgv.Columns.Item(c_DateTimeColumnName).Visible = True
                dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime

                If lstSubClass.Count > 0 And cboxSubClass.SelectedIndex = 0 Then
                    dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 4
                    dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                    dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
                End If

                Dim i As Integer = 5
                For index As Integer = 0 To lstColName.Count - 1
                    If lstColShow(index) = 1 Then
                        dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                        dgv.Columns.Item(lstColName(index)).Visible = True
                        dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                        i = i + 1
                    End If
                Next
                If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                    Dim id As Integer
                    Select Case intColSchType
                        Case 0
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                                strSch = frmMain.GetSchName(id)
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = strSch
                            Next
                        Case 1
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                        Case 2
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                        Case 3
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                    End Select
                End If

                If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                            If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                                dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                            End If
                        End If
                    Next
                End If

            End If
            RefreshIndexDisplay()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        lstColName.Add(c_ICColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmStuDoneAssign_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmStuDoneAssign_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuDoneAssign_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub chkboxShowPastAssign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPastAssign.CheckedChanged
        RefreshAssignmentList()
    End Sub


    Private Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        RefreshIndexDisplay()
    End Sub


    Private Sub RefreshIndexDisplay()
        Dim total As String = dgv.Rows.Count.ToString
        Dim current As String = ""
        If dgv.Rows.Count > 0 Then
            If Not dgv.CurrentRow Is Nothing Then
                current = (dgv.CurrentRow.Index + 1).ToString()
                tboxIndex.Text = current & "/" & total
            Else
                tboxIndex.Clear()
            End If
        Else
            tboxIndex.Clear()
        End If
    End Sub

    Private Sub mnuStuInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStuInfo.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(13)
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub mnuDetele_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDetele.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim idx As Integer = cboxAssignment.SelectedIndex
            Dim assignment As Integer = -1
            If idx > -1 Then
                assignment = lstAssignment(idx)
            End If
            If id.Length = 8 And idx > -1 Then
                objCsol.UpdateStuAssignment(id, assignment, Now, c_Fail)
                RefreshData()
            End If
        End If
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 3 Then

            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

End Class