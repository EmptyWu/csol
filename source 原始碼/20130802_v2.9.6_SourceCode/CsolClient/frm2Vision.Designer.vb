﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2Vision
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tboxContent = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'tboxContent
        '
        Me.tboxContent.BackColor = System.Drawing.SystemColors.Control
        Me.tboxContent.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tboxContent.Cursor = System.Windows.Forms.Cursors.Default
        Me.tboxContent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tboxContent.Font = New System.Drawing.Font("標楷體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.tboxContent.Location = New System.Drawing.Point(0, 0)
        Me.tboxContent.Multiline = True
        Me.tboxContent.Name = "tboxContent"
        Me.tboxContent.ReadOnly = True
        Me.tboxContent.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tboxContent.Size = New System.Drawing.Size(924, 559)
        Me.tboxContent.TabIndex = 0
        '
        'frm2Vision
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(924, 559)
        Me.Controls.Add(Me.tboxContent)
        Me.Name = "frm2Vision"
        Me.Text = "版本資訊"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tboxContent As System.Windows.Forms.TextBox
End Class
