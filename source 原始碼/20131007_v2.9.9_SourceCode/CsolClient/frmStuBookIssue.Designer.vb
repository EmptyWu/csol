﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuBookIssue
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuBookIssue))
        Me.Label9 = New System.Windows.Forms.Label
        Me.tboxDate = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.lblBook = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.lblTime = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.picbox = New System.Windows.Forms.PictureBox
        Me.chklstBook = New System.Windows.Forms.CheckedListBox
        Me.butEnd = New System.Windows.Forms.Button
        Me.butStart = New System.Windows.Forms.Button
        Me.timerClock = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.tboxCardNum = New System.Windows.Forms.TextBox
        Me.butManualPunch = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.timerUsb = New System.Windows.Forms.Timer(Me.components)
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.lblImgInfo = New System.Windows.Forms.Label
        Me.tboxStatus = New System.Windows.Forms.Label
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Name = "Label9"
        '
        'tboxDate
        '
        resources.ApplyResources(Me.tboxDate, "tboxDate")
        Me.tboxDate.Name = "tboxDate"
        Me.tboxDate.ReadOnly = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Name = "Label6"
        '
        'tboxID
        '
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Name = "Label3"
        '
        'tboxName
        '
        Me.tboxName.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        Me.tboxName.ReadOnly = True
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Name = "Label10"
        '
        'lblBook
        '
        resources.ApplyResources(Me.lblBook, "lblBook")
        Me.lblBook.ForeColor = System.Drawing.Color.White
        Me.lblBook.Name = "lblBook"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Name = "Label5"
        '
        'lblTime
        '
        resources.ApplyResources(Me.lblTime, "lblTime")
        Me.lblTime.ForeColor = System.Drawing.Color.White
        Me.lblTime.Name = "lblTime"
        '
        'lblName
        '
        resources.ApplyResources(Me.lblName, "lblName")
        Me.lblName.ForeColor = System.Drawing.Color.White
        Me.lblName.Name = "lblName"
        '
        'picbox
        '
        resources.ApplyResources(Me.picbox, "picbox")
        Me.picbox.MaximumSize = New System.Drawing.Size(300, 300)
        Me.picbox.Name = "picbox"
        Me.picbox.TabStop = False
        '
        'chklstBook
        '
        Me.chklstBook.CheckOnClick = True
        Me.chklstBook.FormattingEnabled = True
        resources.ApplyResources(Me.chklstBook, "chklstBook")
        Me.chklstBook.Name = "chklstBook"
        '
        'butEnd
        '
        Me.butEnd.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.butEnd.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.butEnd, "butEnd")
        Me.butEnd.Name = "butEnd"
        Me.butEnd.UseVisualStyleBackColor = False
        '
        'butStart
        '
        Me.butStart.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.butStart.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.butStart, "butStart")
        Me.butStart.Name = "butStart"
        Me.butStart.UseVisualStyleBackColor = False
        '
        'timerClock
        '
        Me.timerClock.Enabled = True
        Me.timerClock.Interval = 1000
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'tboxCardNum
        '
        Me.tboxCardNum.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tboxCardNum.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tboxCardNum.ForeColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        resources.ApplyResources(Me.tboxCardNum, "tboxCardNum")
        Me.tboxCardNum.Name = "tboxCardNum"
        Me.tboxCardNum.ReadOnly = True
        '
        'butManualPunch
        '
        Me.butManualPunch.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.butManualPunch.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.butManualPunch, "butManualPunch")
        Me.butManualPunch.Name = "butManualPunch"
        Me.butManualPunch.UseVisualStyleBackColor = False
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'timerUsb
        '
        Me.timerUsb.Enabled = True
        Me.timerUsb.Interval = 250
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.ForeColor = System.Drawing.Color.White
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'lblImgInfo
        '
        resources.ApplyResources(Me.lblImgInfo, "lblImgInfo")
        Me.lblImgInfo.Name = "lblImgInfo"
        '
        'tboxStatus
        '
        resources.ApplyResources(Me.tboxStatus, "tboxStatus")
        Me.tboxStatus.Name = "tboxStatus"
        '
        'frmStuBookIssue
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.tboxStatus)
        Me.Controls.Add(Me.lblImgInfo)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.butManualPunch)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxCardNum)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.butStart)
        Me.Controls.Add(Me.butEnd)
        Me.Controls.Add(Me.chklstBook)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.tboxDate)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.lblBook)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.picbox)
        Me.Name = "frmStuBookIssue"
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tboxDate As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents lblBook As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
    Friend WithEvents chklstBook As System.Windows.Forms.CheckedListBox
    Friend WithEvents butEnd As System.Windows.Forms.Button
    Friend WithEvents butStart As System.Windows.Forms.Button
    Friend WithEvents timerClock As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents tboxCardNum As System.Windows.Forms.TextBox
    Friend WithEvents butManualPunch As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents timerUsb As System.Windows.Forms.Timer
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents lblImgInfo As System.Windows.Forms.Label
    Friend WithEvents tboxStatus As System.Windows.Forms.Label
End Class
