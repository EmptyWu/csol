﻿Public Class frmSetClassPaper
    Private dtPaper As New DataTable
    Private dt As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private ds As New DataSet

    Public Sub New()
        InitializeComponent()

        'CSOL.Logger.LogMessage("frmSetClassPaper New1")
        Me.Text = My.Resources.frmSetClassPaper
        ds = frmMain.GetClassInfoSet
        'CSOL.Logger.LogMessage("frmSetClassPaper New2")
        dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
        InitList()
        'CSOL.Logger.LogMessage("frmSetClassPaper New3")
    End Sub

    Private Sub frmSetClassPaper_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetClassPaper_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()
        cboxClass.Items.Add(My.Resources.all)
        lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub RefreshData()
        Try
            If lstClass.Count > 0 And cboxClass.SelectedIndex > -1 Then
                Dim lst As New ArrayList
                If cboxClass.SelectedIndex > 0 Then
                    lst.Add(lstClass(cboxClass.SelectedIndex))
                Else
                    lst = lstClass
                End If
                'CSOL.Logger.LogMessage("frmSetClassPaper RefreshData1")
                dt = objCsol.GetClassPaper(lst)
                'CSOL.Logger.LogMessage("frmSetClassPaper RefreshData2")
                dtPaper = dt.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_AbbrColumnName, _
                                                  c_DateTimeColumnName, c_ContentColumnName, c_StuCntColumnName, _
                                                  c_MaxDataColumnName, c_MinDataColumnName, c_AvgDataColumnName, _
                                                  c_ContentIDColumnName)

                dgv.DataSource = dtPaper

                LoadColumnText()
                'CSOL.Logger.LogMessage("frmSetClassPaper RefreshData3")
            Else
                dgv.DataSource = Nothing
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.paperName
            dgv.Columns.Item(c_AbbrColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_AbbrColumnName).Visible = True
            dgv.Columns.Item(c_AbbrColumnName).HeaderText = My.Resources.paperAbbr
            dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_DateTimeColumnName).Visible = True
            dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.paperDate
            dgv.Columns.Item(c_DateTimeColumnName).DefaultCellStyle.Format = "d"
            dgv.Columns.Item(c_ContentColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_ContentColumnName).Visible = True
            dgv.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.paperLinkContent
            dgv.Columns.Item(c_StuCntColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_StuCntColumnName).Visible = True
            dgv.Columns.Item(c_StuCntColumnName).HeaderText = My.Resources.paperStuCnt
            dgv.Columns.Item(c_MaxDataColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_MaxDataColumnName).Visible = True
            dgv.Columns.Item(c_MaxDataColumnName).HeaderText = My.Resources.topMark
            dgv.Columns.Item(c_MinDataColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_MinDataColumnName).Visible = True
            dgv.Columns.Item(c_MinDataColumnName).HeaderText = My.Resources.bottomMark
            dgv.Columns.Item(c_AvgDataColumnName).DisplayIndex = 7
            dgv.Columns.Item(c_AvgDataColumnName).Visible = True
            dgv.Columns.Item(c_AvgDataColumnName).HeaderText = My.Resources.avgMark
        End If
        RefreshIndexDisplay()
    End Sub

    Private Sub frmSetClassPaper_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2ExamPaper(-1, True, "", "", Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim stuCount As Integer = dgv.SelectedRows(0).Cells(c_StuCntColumnName).Value
            If stuCount = 0 Then
                If MsgBox("確定刪除 ?", MsgBoxStyle.OkCancel, My.Resources.msgRemindTitle) = MsgBoxResult.Ok Then
                    objCsol.DeletePaper(id)
                    RefreshData()
                End If
            Else
                MsgBox("已有學生成績資料，無法刪除 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub mnuMod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMod.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim lst As New ArrayList
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value

            For index As Integer = 0 To dt.Rows.Count - 1
                If dt.Rows(index).Item(c_IDColumnName) = id Then
                    lst.Add(dt.Rows(index).Item(c_SubClassIDColumnName))
                End If
            Next
            frmMain.SetOkState(False)
            Dim frm As New frm2ExamPaper(id, False, dgv.SelectedRows(0).Cells(c_NameColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_AbbrColumnName).Value, _
                                     dgv.SelectedRows(0).Cells(c_DateTimeColumnName).Value, _
                                     dgv.SelectedRows(0).Cells(c_ContentIDColumnName).Value, _
                                    lst)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        RefreshIndexDisplay()
    End Sub


    Private Sub RefreshIndexDisplay()
        Dim total As String = dgv.Rows.Count.ToString
        Dim current As String = ""
        If dgv.Rows.Count > 0 Then
            If Not dgv.CurrentRow Is Nothing Then
                current = (dgv.CurrentRow.Index + 1).ToString()
                tboxIndex.Text = current & "/" & total
            Else
                tboxIndex.Clear()
            End If
        Else
            tboxIndex.Clear()
        End If
    End Sub
End Class