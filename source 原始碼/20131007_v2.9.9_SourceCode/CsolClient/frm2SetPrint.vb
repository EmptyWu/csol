﻿Public Class frm2SetPrint
    Dim dt As New DataTable

    Public Sub New()

        InitializeComponent()
        frmMain.SetOkState(False)
        RefreshData()

    End Sub

    Private Sub RefreshData()
        dt = objCsol.GetReceiptPrint

        If dt.Rows.Count > 0 Then
            If dt.Rows(0).Item(c_PrintPayTodayColumnName) = 1 Then
                radbutTodayFee.Checked = True
            Else
                radbutTodayFeeNo.Checked = True
            End If
            If dt.Rows(0).Item(c_DefaultTitleColumnName) = 1 Then
                radbutReceipt.Checked = True
            Else
                radbutReceiptCust.Checked = True
                tboxCallReceipt.Text = dt.Rows(0).Item(c_CustTitleColumnName).trim
            End If

            If dt.Rows(0).Item(c_PrintFeeOweColumnName) = 1 Then
                radbutOwe.Checked = True
            ElseIf dt.Rows(0).Item(c_PrintFeeOweColumnName) = 0 Then
                radbutOweNo.Checked = True
            End If
            If dt.Rows(0).Item(c_UseDefaultFeeOweColumnName) = 1 Then
                radbutCallOwe.Checked = True
            Else
                radbutCallOweNo.Checked = True
                tboxOwe.Text = dt.Rows(0).Item(c_CustFeeOweColumnName).trim
            End If

            If dt.Rows(0).Item(c_PrintClassDetailsColumnName) = 1 Then
                radbutTotalNo.Checked = True
            Else
                radbutTotal.Checked = True
            End If
            If dt.Rows(0).Item(c_PrintReceiptNumColumnName) = 1 Then
                radbutNum.Checked = True
            Else
                radbutNumNo.Checked = True
            End If
            If dt.Rows(0).Item(c_PrintClassPeriodColumnName) = 1 Then
                radbutTime.Checked = True
            Else
                radbutTimeNo.Checked = True
            End If
            If dt.Rows(0).Item(c_PrintCertNumColumnName) = 1 Then
                radbutCert.Checked = True
                tboxCert.Text = dt.Rows(0).Item(c_CertNumColumnName).trim
            Else
                radbutCertNo.Checked = True
            End If
            
            If dt.Rows(0).Item(c_LeftAllignColumnName) = 1 Then
                radbutLeft.Checked = True
            Else
                radbutRight.Checked = True
            End If
            If dt.Rows(0).Item(c_PaperSizeColumnName) = c_PaperSizeA4 Then
                radbutA4.Checked = True
            Else
                radbutB5.Checked = True
            End If
            If dt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1 Then
                radbutBothNo.Checked = True
            Else
                radbutBoth.Checked = True
            End If
            If dt.Rows(0).Item(c_UseEnglishColumnName) = 1 Then
                radbutEnglish.Checked = True
            Else
                radbutChinese.Checked = True
            End If
            If dt.Rows(0).Item(c_PrintNoSalesColumnName) = 1 Then
                radbutSalesNo.Checked = True
            Else
                radbutSales.Checked = True
            End If

        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If dt.Rows.Count > 0 Then
            If radbutTodayFee.Checked Then
                dt.Rows(0).Item(c_PrintPayTodayColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintPayTodayColumnName) = 0
            End If
            If radbutReceipt.Checked Then
                dt.Rows(0).Item(c_DefaultTitleColumnName) = 1
            Else
                dt.Rows(0).Item(c_DefaultTitleColumnName) = 0
                dt.Rows(0).Item(c_CustTitleColumnName) = tboxCallReceipt.Text
            End If
            If radbutOwe.Checked Then
                dt.Rows(0).Item(c_PrintFeeOweColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintFeeOweColumnName) = 0
            End If
            If radbutCallOwe.Checked Then
                dt.Rows(0).Item(c_UseDefaultFeeOweColumnName) = 1
            Else
                dt.Rows(0).Item(c_UseDefaultFeeOweColumnName) = 0
                dt.Rows(0).Item(c_CustFeeOweColumnName) = tboxOwe.Text
            End If

            If radbutTotalNo.Checked Then
                dt.Rows(0).Item(c_PrintClassDetailsColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintClassDetailsColumnName) = 0
            End If
            If radbutNum.Checked Then
                dt.Rows(0).Item(c_PrintReceiptNumColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintReceiptNumColumnName) = 0
            End If
            If radbutTime.Checked Then
                dt.Rows(0).Item(c_PrintClassPeriodColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintClassPeriodColumnName) = 0
            End If
            If radbutCert.Checked Then
                dt.Rows(0).Item(c_PrintCertNumColumnName) = 1
                dt.Rows(0).Item(c_CertNumColumnName) = tboxCert.Text
            Else
                dt.Rows(0).Item(c_PrintCertNumColumnName) = 0
            End If

            If radbutLeft.Checked Then
                dt.Rows(0).Item(c_LeftAllignColumnName) = 1
            Else
                dt.Rows(0).Item(c_LeftAllignColumnName) = 0
            End If
            If radbutA4.Checked Then
                dt.Rows(0).Item(c_PaperSizeColumnName) = c_PaperSizeA4
            Else
                dt.Rows(0).Item(c_PaperSizeColumnName) = c_PaperSizeB5
            End If
            If radbutBothNo.Checked Then
                dt.Rows(0).Item(c_FirstPageOnlyColumnName) = 1
            Else
                dt.Rows(0).Item(c_FirstPageOnlyColumnName) = 0
            End If
            If radbutEnglish.Checked Then
                dt.Rows(0).Item(c_UseEnglishColumnName) = 1
            Else
                dt.Rows(0).Item(c_UseEnglishColumnName) = 0
            End If
            If radbutSalesNo.Checked Then
                dt.Rows(0).Item(c_PrintNoSalesColumnName) = 1
            Else
                dt.Rows(0).Item(c_PrintNoSalesColumnName) = 0
            End If
            frmMain.SetOkState(True)
            objCsol.UpdateReceiptPrint(dt.GetChanges)
            Me.Close()
        End If
    End Sub
End Class