﻿Public Class frmJournalRec
    Private dtGrp As New DataTable
    Private lstGrp As New ArrayList
    Private dtJournalRec As New DataTable
    Private dtJR As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmJournalRec
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmJournalRec

        lstGrp = New ArrayList
        cboxGrp.Items.Clear()
        cboxGrp.Items.Add(My.Resources.all)
        lstGrp.Add(-1)
        dtGrp = frmMain.GetJournalGrp
        For index As Integer = 0 To dtGrp.Rows.Count - 1
            cboxGrp.Items.Add(dtGrp.Rows(index).Item(c_NameColumnName))
            lstGrp.Add(dtGrp.Rows(index).Item(c_IDColumnName))
        Next
        cboxGrp.SelectedIndex = 0

        RefreshData()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(39) = False Then
            frmMain.ShowNoAuthMsg()
        Else
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvRec.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvRec.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvRec.Rows.Count

            Dim oRow As DataGridViewRow = dgvRec.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvRec.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvRec.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvRec.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvRec.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvRec.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvRec.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvRec.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmJournalRec_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmJournalRec_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        If radbutNoDate.Checked Then
            Dim jobtime As DateTime = Now()
            dtJournalRec = objCsol.ListJournalSta()
            System.Diagnostics.Debug.WriteLine((Now - jobtime).TotalMilliseconds)
        Else
            dtJournalRec = objCsol.ListJournalSta(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
        End If
        LoadColumnText()
        dgvRec.DataSource = dtJR
        dgvRec.Columns("GroupID").Visible = False
        dgvRec.Columns("ID").Visible = False
        dgvRec.Columns("UserID").Visible = False

        dgvRec.Columns("金額").DefaultCellStyle.Format = "$#,##0"

        'If radbutNoDate.Checked Then
        '    Dim jobtime As DateTime = Now()
        '    dtJournalRec = objCsol.ListJournalSta()
        '    System.Diagnostics.Debug.WriteLine((Now - jobtime).TotalMilliseconds)
        'Else
        '    dtJournalRec = objCsol.ListJournalSta(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
        'End If
        'If cboxGrp.SelectedIndex <= 0 Then
        '    dtJournalRec.DefaultView.RowFilter = ""
        '    dgvRec.DataSource = dtJournalRec
        'ElseIf cboxGrp.SelectedIndex > 0 Then
        '    dtJournalRec.DefaultView.RowFilter = c_GroupIDColumnName & "=" & lstGrp(cboxGrp.SelectedIndex).ToString
        '    dgvRec.DataSource = dtJournalRec
        'End If
        'dgvRec.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
        'LoadColumnText()
    End Sub


    'Private Shared Function ListJournalSta(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
    '    Dim dt As New DataTable
    '    Dim RequestParams As New System.Collections.Specialized.NameValueCollection


    '    RequestParams.Add("dateFrom", dateFrom)
    '    RequestParams.Add("dateTo", dateTo)

    '    Dim ResponseData() As Byte = CSOL.HTTPClient.Post("JounalRec", "ListJournalSta", RequestParams)
    '    Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
    '    Dim xmlstring As String = ""
    '    Try
    '        Using ms As New System.IO.MemoryStream
    '            zip("ListJournalSta").Extract(ms)
    '            ms.Flush()
    '            ms.Position = 0
    '            Dim sr As New System.IO.StreamReader(ms)
    '            xmlstring = sr.ReadToEnd()
    '        End Using
    '        dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListJournalSta"))
    '    Catch ex As Exception
    '        System.Diagnostics.Debug.WriteLine(ex)
    '    Finally
    '    End Try
    '    Return dt
    'End Function

    Private Sub LoadColumnText()
        dtJR = dtJournalRec.Clone
        dtJR.Columns("Item").ColumnName = "項目"
        dtJR.Columns("Amount").ColumnName = "金額"
        dtJR.Columns("DateTime").ColumnName = "日期時間"
        dtJR.Columns("SerialNum").ColumnName = "序號"
        dtJR.Columns("ChequeNum").ColumnName = "支票號碼"
        dtJR.Columns("Remarks3").ColumnName = "備註三"
        dtJR.Columns("Remarks4").ColumnName = "備註四"
        dtJR.Columns("Remarks5").ColumnName = "備註五"
        dtJR.Columns("Handler").ColumnName = "承辦人員"
        dtJR.Columns("Group").ColumnName = "群組"


        For Each row As DataRow In dtJournalRec.Rows
            dtJR.Rows.Add(row.ItemArray)
        Next
        'dtJR.Columns.RemoveAt(dtJR.Columns.IndexOf("ID"))
        'dtJR.Columns.RemoveAt(dtJR.Columns.IndexOf("UserID"))
        If cboxGrp.SelectedIndex <= 0 Then
            ' dtJR.DefaultView.RowFilter = ""
        ElseIf cboxGrp.SelectedIndex > 0 Then
            dtJR = dtJR.Select(String.Format("{0} = {1}", c_GroupIDColumnName, lstGrp(cboxGrp.SelectedIndex))).CopyToDataTable
            ' dtJR.DefaultView.RowFilter = c_GroupIDColumnName & "=" & lstGrp(cboxGrp.SelectedIndex).ToString
        End If


        'For Each col In dgvRec.Columns
        '    col.visible = False
        'Next
        'If dgvRec.Rows.Count > 0 Then

        '    dgvRec.Columns.Item(c_GroupColumnName).DisplayIndex = 0
        '    dgvRec.Columns.Item(c_GroupColumnName).Visible = True
        '    dgvRec.Columns.Item(c_GroupColumnName).HeaderText = My.Resources.group
        '    dgvRec.Columns.Item(c_ItemColumnName).DisplayIndex = 1
        '    dgvRec.Columns.Item(c_ItemColumnName).Visible = True
        '    dgvRec.Columns.Item(c_ItemColumnName).HeaderText = My.Resources.item
        '    dgvRec.Columns.Item(c_AmountColumnName).DisplayIndex = 2
        '    dgvRec.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvRec.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        '    dgvRec.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
        '    dgvRec.Columns.Item(c_DateTimeColumnName).Visible = True
        '    dgvRec.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvRec.Columns.Item(c_SerialNumColumnName).DisplayIndex = 4
        '    dgvRec.Columns.Item(c_SerialNumColumnName).Visible = True
        '    dgvRec.Columns.Item(c_SerialNumColumnName).HeaderText = My.Resources.serialNum
        '    dgvRec.Columns.Item(c_ChequeNumColumnName).DisplayIndex = 5
        '    dgvRec.Columns.Item(c_ChequeNumColumnName).Visible = True
        '    dgvRec.Columns.Item(c_ChequeNumColumnName).HeaderText = My.Resources.chequeNum
        '    dgvRec.Columns.Item(c_Remarks3ColumnName).DisplayIndex = 6
        '    dgvRec.Columns.Item(c_Remarks3ColumnName).Visible = True
        '    dgvRec.Columns.Item(c_Remarks3ColumnName).HeaderText = My.Resources.remarks3
        '    dgvRec.Columns.Item(c_Remarks4ColumnName).DisplayIndex = 7
        '    dgvRec.Columns.Item(c_Remarks4ColumnName).Visible = True
        '    dgvRec.Columns.Item(c_Remarks4ColumnName).HeaderText = My.Resources.remarks4
        '    dgvRec.Columns.Item(c_Remarks5ColumnName).DisplayIndex = 8
        '    dgvRec.Columns.Item(c_Remarks5ColumnName).Visible = True
        '    dgvRec.Columns.Item(c_Remarks5ColumnName).HeaderText = My.Resources.remarks5
        '    dgvRec.Columns.Item(c_HandlerColumnName).DisplayIndex = 9
        '    dgvRec.Columns.Item(c_HandlerColumnName).Visible = True
        '    dgvRec.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        'End If

    End Sub

    Private Sub frmJournalRec_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub butFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFilter.Click
        'Dim t1 As Date = Now
        RefreshData()
    End Sub

    Private Sub cboxGrp_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxGrp.SelectedIndexChanged

        Dim dt As New DataTable
        If cboxGrp.SelectedIndex <= 0 Then
            'dtJR.DefaultView.RowFilter = c_GroupIDColumnName & "=" & lstGrp(cboxGrp.SelectedIndex).ToString
            dtJR.DefaultView.RowFilter = ""
        ElseIf cboxGrp.SelectedIndex > 0 Then
            'dtJR.DefaultView.RowFilter = c_GroupIDColumnName & "=" & lstGrp(cboxGrp.SelectedIndex).ToString
            dtJR.DefaultView.RowFilter = String.Format("{0} = {1}", c_GroupIDColumnName, lstGrp(cboxGrp.SelectedIndex))
        End If
        dgvRec.DataSource = dtJR

        'If cboxGrp.SelectedIndex <= 0 Then
        '    dtJournalRec.DefaultView.RowFilter = ""
        '    dgvRec.DataSource = dtJournalRec
        'ElseIf cboxGrp.SelectedIndex > 0 Then
        '    dtJournalRec.DefaultView.RowFilter = c_GroupIDColumnName & _
        '        "=" & lstGrp(cboxGrp.SelectedIndex).ToString
        '    dgvRec.DataSource = dtJournalRec
        'End If
    End Sub

    Private Sub mnuAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAdd.Click
        If Not frmMain.CheckAuth(35) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim subfrmAddJournalRec = New frm2AddJournalRec()

        subfrmAddJournalRec.ShowDialog()

        RefreshData()

    End Sub

    Private Sub mnuModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuModify.Click
        If Not frmMain.CheckAuth(36) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If

        Dim dt As New DataTable
        dt = dtJournalRec.Clone
        If lstGrp(cboxGrp.SelectedIndex).ToString = "-1" Then
            For i As Integer = 0 To dtJournalRec.Rows.Count - 1
                dt.Rows.Add(dtJournalRec.Rows(i).ItemArray)
            Next
        Else
            For i As Integer = 0 To dtJournalRec.Rows.Count - 1
                If lstGrp(cboxGrp.SelectedIndex).ToString = dtJournalRec.Rows(i).Item(c_GroupIDColumnName).ToString Then
                    dt.Rows.Add(dtJournalRec.Rows(i).ItemArray)
                End If
            Next
        End If

        If dtJournalRec.Rows.Count > 0 And dgvRec.SelectedRows.Count > 0 Then

            Dim dtRec As DataTable = dtJR.Clone
            Try
                dtRec.Rows.Add(dgvRec.CurrentRow.Cells("群組").Value, _
                               dgvRec.CurrentRow.Cells("ID").Value, _
                               dgvRec.CurrentRow.Cells("GroupID").Value, _
                               dgvRec.CurrentRow.Cells("項目").Value, _
                               dgvRec.CurrentRow.Cells("金額").Value, _
                               dgvRec.CurrentRow.Cells("日期時間").Value, _
                               dgvRec.CurrentRow.Cells("UserID").Value, _
                               dgvRec.CurrentRow.Cells("序號").Value, _
                               dgvRec.CurrentRow.Cells("支票號碼").Value, _
                               dgvRec.CurrentRow.Cells("備註三").Value, _
                               dgvRec.CurrentRow.Cells("備註四").Value, _
                               dgvRec.CurrentRow.Cells("備註五").Value)

            Catch
                dtRec.Rows.Add(dgvRec.Rows(0).Cells("群組").Value, _
                               dgvRec.Rows(0).Cells("ID").Value, _
                               dgvRec.Rows(0).Cells("GroupID").Value, _
                               dgvRec.Rows(0).Cells("項目").Value, _
                               dgvRec.Rows(0).Cells("金額").Value, _
                               dgvRec.Rows(0).Cells("日期時間").Value, _
                               dgvRec.Rows(0).Cells("UserID").Value, _
                               dgvRec.Rows(0).Cells("序號").Value, _
                               dgvRec.Rows(0).Cells("支票號碼").Value, _
                               dgvRec.Rows(0).Cells("備註三").Value, _
                               dgvRec.Rows(0).Cells("備註四").Value, _
                               dgvRec.Rows(0).Cells("備註五").Value)
            End Try





            'Dim dtRec As DataTable = dtJournalRec.Clone
            'Try
            '    dtRec.Rows.Add(dgvRec.CurrentRow.Cells("ID").Value, _
            '                  dgvRec.CurrentRow.Cells("GroupID").Value, _
            '                  dgvRec.CurrentRow.Cells("Item").Value, _
            '                  dgvRec.CurrentRow.Cells("Amount").Value, _
            '                  dgvRec.CurrentRow.Cells("DateTime").Value, _
            '                  dgvRec.CurrentRow.Cells("UserID").Value, _
            '                  dgvRec.CurrentRow.Cells("SerialNum").Value, _
            '                  dgvRec.CurrentRow.Cells("ChequeNum").Value, _
            '                  dgvRec.CurrentRow.Cells("Remarks3").Value, _
            '                  dgvRec.CurrentRow.Cells("Remarks4").Value, _
            '                  dgvRec.CurrentRow.Cells("Remarks5").Value)


            'Catch
            '    dtRec.Rows.Add(dgvRec.Rows(0).Cells("ID").Value, _
            '                  dgvRec.Rows(0).Cells("GroupID").Value, _
            '                  dgvRec.Rows(0).Cells("Item").Value, _
            '                  dgvRec.Rows(0).Cells("Amount").Value, _
            '                  dgvRec.Rows(0).Cells("DateTime").Value, _
            '                  dgvRec.Rows(0).Cells("UserID").Value, _
            '                  dgvRec.Rows(0).Cells("SerialNum").Value, _
            '                  dgvRec.Rows(0).Cells("ChequeNum").Value, _
            '                  dgvRec.Rows(0).Cells("Remarks3").Value, _
            '                  dgvRec.Rows(0).Cells("Remarks4").Value, _
            '                  dgvRec.Rows(0).Cells("Remarks5").Value)
            'End Try
            Dim subfrmUpdJournalRec = New frm2UpdJournalRec(dtRec)

            subfrmUpdJournalRec.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
                frmMain.SetOkState(False)
            End If


        End If

    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(37) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dtJournalRec.Rows.Count > 0 And dgvRec.SelectedRows.Count > 0 Then
            objCsol.DeleteJournalRec(dgvRec.Rows(dgvRec.SelectedRows(0).Index).Cells(c_IDColumnName).Value)
            RefreshData()
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If Not frmMain.CheckAuth(40) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        ExportDgvToExcel(dgvRec)
    End Sub
End Class