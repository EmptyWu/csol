﻿Public Class frmPostGrade
    Private dtPaper As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstPaper As New ArrayList

    Private dtData As New DataTable
    Private dtData2 As New DataTable
    Private dtData3 As New DataTable
    Private dtDataOrigin As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Private dtSchoolType As New DataTable
    Private dtSchool As New DataTable
    Private dtSchoolGroup As New DataTable
    Private lstSch As New ArrayList
    Private lstPaperShown As New ArrayList

    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private dtPunch As New DataTable

    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private intColFrom As Integer = 0
    Private intBasicCol As Integer = 0

    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String
    Private sUserName As String = frmMain.GetUsrName

    Private prtFont As Font
    Private blShowPaperDate As Boolean = False

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmPostGrade
            'CSOL.Logger.LogMessage("frmPostGrade New1")
            ds = frmMain.GetClassInfoSet
            'CSOL.Logger.LogMessage("frmPostGrade New2")
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)
            'CSOL.Logger.LogMessage("frmPostGrade New3")
            dtSchoolGroup = frmMain.GetSchGrpList
            'CSOL.Logger.LogMessage("frmPostGrade New4")
            InitSelections()
            'CSOL.Logger.LogMessage("frmPostGrade New5")
            InitList()
            'CSOL.Logger.LogMessage("frmPostGrade New6")
            MakeFontList()
            'CSOL.Logger.LogMessage("frmPostGrade New7")
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub MakeFontList()
        cboxFont.Items.Clear()
        cboxFont.Items.Add(My.Resources.font1)
        cboxFont.Items.Add(My.Resources.font2)
        cboxFont.Items.Add(My.Resources.font3)
        If cboxFont.Items.Count > 0 Then
            cboxFont.SelectedIndex = 0
        End If
        If cboxFontSize.Items.Count > 0 Then
            cboxFontSize.SelectedIndex = 0
        End If
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        'PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()

    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0
        Try
            prtFont = New Font(cboxFont.Text.Trim, CInt(cboxFontSize.Text.Trim))
        Catch ex As Exception
            prtFont = dgv.Font
        End Try



    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        Dim dtPrintOpt As New DataTable
        dtPrintOpt = frmMain.GetReceiptPrint
        Dim bytPaperSize As Byte = dtPrintOpt.Rows(0).Item(c_PaperSizeColumnName)
        Dim nPageH As Integer = 0

        'Page Margin
        Dim nPageRL As Integer = 0
        Dim nPageTB As Integer = 0
        If bytPaperSize = c_PaperSizeA4 Then
            nPageH = 297
            nPageRL = 16
            nPageTB = 15
        ElseIf bytPaperSize = c_PaperSizeB5 Then
            nPageH = 257
            nPageRL = 7
            nPageTB = 6
        Else
            nPageH = 297
            nPageRL = 16
            nPageTB = 15
        End If

        If chkboxAutoSize.Checked Then

        End If

        If nPageNo = 1 Then
            Dim i3 As Integer = 0
            For Each oColumn As DataGridViewColumn In dgv.Columns

                If oColumn.Visible = True Then
                    i3 = i3 + 1
                    If chkboxAutoSize.Checked Then
                        nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)
                    Else
                        nWidth = CType(Math.Floor(oColumn.Width), Int16)

                    End If
                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If



        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    Header = DrawHeader(nRowsPerPage)
                    e.Graphics.DrawString(Header, New Font(prtFont, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(prtFont, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    Dim i1 As Integer
                    Dim i2 As Integer
                    Dim c As Integer
                    c = oColumnLefts.Count
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            i1 = oColumnLefts(i)
                            i2 = oColumnWidths(i)

                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop



        DrawFooter(e)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs)
        Dim strInd
        strInd = My.Resources.topIndi & ": " & tboxTop.Text.Trim & vbTab & _
                My.Resources.highIndi & ": " & tboxHigh.Text.Trim & vbTab & _
                My.Resources.midIndi & ": " & tboxMid.Text.Trim & vbTab & _
                My.Resources.lowIndi & ": " & tboxLow.Text.Trim & vbTab & _
                My.Resources.bottomIndi & ": " & tboxBottom.Text.Trim

        ' Left Align - Date/Time
        e.Graphics.DrawString(strInd, prtFont, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)
    End Sub

    Private Function DrawHeader(ByVal RowsPerPage As Int32) As String
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = My.Resources.the & " " & nPageNo.ToString & My.Resources.page & _
                    ", " & My.Resources.totalOf & " 1" & My.Resources.page
        Else
            sPageNo = My.Resources.the & " " & nPageNo.ToString & My.Resources.page & _
                    ", " & My.Resources.totalOf & " " & Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString & _
                    My.Resources.page
        End If

        ' Right Align - User Name
        Dim strClass As String = My.Resources.transcript & ": " & GetClassNamesWithPaper() & GetSubClassNamesWithPaper()
        strClass = strClass & "     " & sPageNo

        Return strClass

    End Function

    Private Function GetClassNamesWithPaper() As String
        Dim str As String = ""
        If lstClassShown.Count = 0 Or dtPaper.Rows.Count = 0 Then
            Return str
        End If
        Dim s As String = ""
        For i As Integer = 0 To chklstClass.Items.Count - 1
            If chklstClass.GetItemChecked(i) Then
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", _
                                   c_ClassIDColumnName & "=" & lstClassShown(i).ToString) > 0 Then
                    s = chklstClass.Items(i).ToString.Trim
                    If str.Length > 0 Then
                        str = str & ", " & s
                    Else
                        str = s
                    End If
                End If
            End If
        Next
        If str.Length > 0 Then
            str = str & ". "
        End If
        Return str

    End Function

    Private Function GetSubClassNamesWithPaper() As String
        Dim str As String = ""
        If lstSubClass.Count = 0 Or dtPaper.Rows.Count = 0 Then
            Return str
        End If
        Dim s As String = ""
        For i As Integer = 0 To chklstSubClass.Items.Count - 1
            If chklstSubClass.GetItemChecked(i) Then
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", _
                                   c_SubClassIDColumnName & "=" & lstSubClass(i).ToString) > 0 Then
                    s = chklstSubClass.Items(i).ToString.Trim
                    If str.Length > 0 Then
                        str = str & ", " & s
                    Else
                        str = s
                    End If
                End If
            End If
        Next

        Return str

    End Function

    Private Sub InitList()
        chklstClass.Items.Clear()
        lstClassShown.Clear()

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName), False)
                lstClassShown.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If GetDateEnd(dtClass.Rows(index).Item(c_EndColumnName)) >= Now Then
                    chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        InitClassSelection()

    End Sub

    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            'chklstClass.SetItemChecked(0, True)                                 '100317
            chklstClass.SelectedItem = chklstClass.Items(0)

            If chklstSubClass.Items.Count > 0 Then
                'chklstSubClass.SetItemChecked(0, True)                          '100317
                chklstSubClass.SelectedItem = chklstSubClass.Items(0)
            End If
        Else
            dgv.DataSource = Nothing
            chklstPaper.Items.Clear()
            lstPaper.Clear()
            lstPaperShown.Clear()
            chklstSubClass.Items.Clear()
            lstSubClass.Clear()
            lstSubClassShown.Clear()
        End If
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck1")
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck2")
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClass.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                End If
            Next
            'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck3")
        Else
            'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck4")
            lstClass.Remove(intC)

            Dim i As Integer = -1                                                   '100317
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    i += 1
                    chklstSubClass.SetItemChecked(i, False)
                End If
            Next                                                                    '100317
            'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck5")

            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClass.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    lstSubClass.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClass.Rows(index).Item(c_NameColumnName))
                End If
            Next
            'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck6")
        End If

        If lstClass.Count > 0 Then
            'dtPaper = objCsol.GetClassPaper(lstClass)
            dtPaper = objCsol.GetClassPaperHelper(lstClass)
            'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck7")
            If dtPaper.Rows.Count = 0 Then
                chklstPaper.Items.Clear()
                lstPaper.Clear()
                lstPaperShown.Clear()
                dgv.DataSource = Nothing
            End If
        Else
            dgv.DataSource = Nothing
        End If
        'CSOL.Logger.LogMessage("frmPostGrade chklstClass_ItemCheck8")

    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck1")
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstSubClassShown(e.Index)

        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstSubClass.Add(intC)
            If lstSubClass.Count > 0 Then
                'dtPunch = objCsol.GetSubClassPunchRecList(lstSubClass, GetDateStart(dtpickDate.Value), GetDateEnd(dtpickDate.Value))
                dtPunch = objCsol.GetSubClassPunchRecListHelper(lstSubClass, GetDateStart(dtpickDate.Value), GetDateEnd(dtpickDate.Value))
                'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck2")
            Else
                dgv.DataSource = Nothing
                dtPunch = New DataTable
                chklstPaper.Items.Clear()
                lstPaper.Clear()
                lstPaperShown.Clear()
                Exit Sub
            End If
            'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck3")

            For index As Integer = 0 To dtPaper.Rows.Count - 1
                If dtPaper.Rows(index).Item(c_SubClassIDColumnName) = intC Then
                    intSc = dtPaper.Rows(index).Item(c_IDColumnName)
                    If lstPaperShown.Contains(intSc) = False Then
                        lstPaperShown.Add(intSc)
                        chklstPaper.Items.Add(dtPaper.Rows(index).Item(c_NameColumnName), False)
                    End If
                End If
            Next
            'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck4")
        Else
            lstSubClass.Remove(intC)
            If lstSubClass.Count > 0 Then
                If chkboxShowAttend.Checked Then
                    'dtPunch = objCsol.GetSubClassPunchRecList(lstSubClass, GetDateStart(dtpickDate.Value), GetDateEnd(dtpickDate.Value))
                    dtPunch = objCsol.GetSubClassPunchRecListHelper(lstSubClass, GetDateStart(dtpickDate.Value), GetDateEnd(dtpickDate.Value))
                    'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck5")
                Else
                    dtPunch = New DataTable
                End If
            Else
                dgv.DataSource = Nothing
                dtPunch = New DataTable
                chklstPaper.Items.Clear()
                lstPaper.Clear()
                lstPaperShown.Clear()
                Exit Sub
            End If

            'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck6")
            For index As Integer = 0 To dtPaper.Rows.Count - 1
                If dtPaper.Rows(index).Item(c_SubClassIDColumnName) = intC Then
                    intSc = dtPaper.Rows(index).Item(c_IDColumnName)

                    If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_IDColumnName & "=" & intSc.ToString & _
                                            " AND " & c_SubClassIDColumnName & "<>" & intC.ToString) = 0 Then
                        lstPaperShown.Remove(intSc)
                        chklstPaper.Items.Remove(dtPaper.Rows(index).Item(c_NameColumnName))
                    End If
                End If
            Next
            'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck7")
        End If

        RefreshPaperData()
        'CSOL.Logger.LogMessage("frmPostGrade chklstSubClass_ItemCheck8")
    End Sub

    Private Sub RefreshPaperData()
        Try
            If lstPaper.Count = 0 Or lstSubClass.Count = 0 Then
                dgv.DataSource = Nothing
                Exit Sub
            End If

            dtData3 = objCsol.GetStuGradesMulByMulClass(lstPaper, lstSubClass, lstClass)
            dtDataOrigin = New DataTable
            dtDataOrigin.Merge(dtData3)

            MakeTable()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub MakeTable()
        Try
            dtData3 = New DataTable
            dtData3.Merge(dtDataOrigin)
            dtData2 = New DataTable
            dtData = New DataTable
            If dtData3.Rows.Count > 0 Then
                If chkboxShowAttend.Checked Then
                    If dtPunch.Rows.Count > 0 Then
                        Dim id As String = ""
                        dtData2 = dtData3.Clone
                        For Each row As DataRow In dtData3.Rows
                            id = row.Item(c_IDColumnName)
                            If dtPunch.Compute("COUNT (" & c_StuIDColumnName & ")", c_StuIDColumnName & "=" & id) > 0 Then
                                dtData2.ImportRow(row)
                            End If
                        Next
                    End If
                Else
                    dtData2.Merge(dtData3)
                End If
            End If

            dtData3 = New DataTable
            If dtData2.Rows.Count > 0 Then
                dtData3 = dtData2.Clone
                If chkboxShowCancel.Checked = False Then
                    Dim c As Integer = 0
                    For Each row As DataRow In dtData2.Rows
                        c = row.Item(c_CancelColumnName)
                        If c = c_StuClassNotCancel Then
                            dtData3.ImportRow(row)
                        End If
                    Next
                Else
                    dtData3.Merge(dtData2)
                End If
            Else
                dgv.DataSource = Nothing
                Exit Sub
            End If

            If dtData3.Rows.Count > 0 Then
                dtData = dtData3.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_EngNameColumnName, c_BirthdayColumnName, c_SexColumnName, _
                                                     c_SchoolColumnName, c_SchoolGradeColumnName, c_SchoolClassColumnName, c_SchGroupColumnName, _
                                                     c_GraduateFromColumnName, c_SeatNumColumnName, c_DadNameColumnName, c_DadTitleColumnName, c_MumTitleColumnName, _
                                                     c_MumNameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_OfficeTelColumnName, _
                                                     c_DadMobileColumnName, c_MumMobileColumnName, c_MobileColumnName, c_PostalCodeColumnName, _
                                                     c_AddressColumnName, c_PostalCode2ColumnName, c_Address2ColumnName, c_IntroIDColumnName, _
                                                     c_IntroNameColumnName, c_ICColumnName, c_EmailColumnName, c_CreateDateColumnName, c_SubClassIDColumnName, _
                                                     c_CancelColumnName, c_CardNumColumnName, c_RegDateColumnName, c_SalesPersonColumnName, c_Cust3ColumnName, _
                                                     c_Cust1ColumnName, c_Cust2ColumnName, c_EnrollSchColumnName, c_HseeMarkColumnName, c_PunchCardNumColumnName, _
                                                     c_CurrentSchColumnName, c_JuniorSchColumnName, c_HighSchColumnName, c_PrimarySchColumnName, _
                                                     c_UniversityColumnName)

                Dim idxLast As Integer = dtData.Columns.Count - 1

                dtData.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
                dtData.Columns.Add(c_GroupColumnName, GetType(System.String)) 'school group
                dtData.Columns.Add(c_HasCardColumnName, GetType(System.String))
                dtData.Columns.Add(c_RankColumnName, GetType(System.Int32))


                dtData.Columns.Add(c_AverageColumnName, GetType(System.String))
                dtData.Columns.Add(c_TotalColumnName, GetType(System.String))
                dtData.Columns.Add(c_PaperCntColumnName, GetType(System.Int32))
                dtData.Columns.Add(c_AvgDataColumnName, GetType(System.Single)) 'for sorting purpose

                For i As Integer = 0 To lstPaper.Count - 1
                    dtData.Columns.Add("Paper" & i.ToString, GetType(System.String))
                    dtData.Columns.Add("Remarks" & i.ToString, GetType(System.String))
                Next

                dtData.Columns.Item(idxLast + 4).SetOrdinal(0)


                Dim mark As Single = 0
                Dim remarks As String = ""
                Dim s As String = ""
                Dim total As Single = 0
                Dim paperCount As Single = 0
                Dim paperCount2 As Single = 0
                Dim average As Single = 0
                For i As Integer = 0 To dtData.Rows.Count - 1
                    dtData.Rows(i).Item(c_SubClassNameColumnName) = GetSubClassName(dtData.Rows(i).Item(c_SubClassIDColumnName))
                    dtData.Rows(i).Item(c_GroupColumnName) = GetSchoolGroup(dtData.Rows(i).Item(c_SchGroupColumnName))
                    s = GetDbString(dtData.Rows(i).Item(c_CardNumColumnName)).Trim
                    If s.Length > 4 Then
                        dtData.Rows(i).Item(c_HasCardColumnName) = My.Resources.gotCard
                    Else
                        dtData.Rows(i).Item(c_HasCardColumnName) = My.Resources.noCard
                    End If
                    For j As Integer = 0 To lstPaper.Count - 1S
                        GetMark(mark, remarks, dtData.Rows(i).Item(c_IDColumnName), lstPaper(j))
                        If mark <> c_NullMark Then
                            If chkboxNoDigits.Checked Then
                                If CSng(CInt(mark)) = mark Then
                                    dtData.Rows(i).Item("Paper" & j.ToString) = CInt(mark).ToString
                                Else
                                    dtData.Rows(i).Item("Paper" & j.ToString) = mark.ToString("##.00")
                                End If
                            Else
                                dtData.Rows(i).Item("Paper" & j.ToString) = mark.ToString("##.00")
                            End If
                            dtData.Rows(i).Item("Remarks" & j.ToString) = remarks
                            total = total + mark
                            paperCount2 = paperCount2 + 1
                        Else
                            If chkboxShowRemarks.Checked Then
                                dtData.Rows(i).Item("Paper" & j.ToString) = remarks
                            End If
                        End If
                        If chkboxNoAbsent.Checked Then
                            If mark <> c_NullMark Then
                                paperCount = paperCount + 1
                            End If
                        Else
                            paperCount = paperCount + 1
                        End If
                    Next
                    If paperCount > 0 Then
                        average = total / paperCount
                    Else
                        average = 0
                    End If
                    If chkboxNoDigits.Checked Then
                        If CInt(total) = total Then
                            dtData.Rows(i).Item(c_TotalColumnName) = CInt(total).ToString
                        Else
                            dtData.Rows(i).Item(c_TotalColumnName) = total.ToString("##.00")
                        End If
                        If CInt(average) = average Then
                            dtData.Rows(i).Item(c_AverageColumnName) = CInt(average).ToString
                        Else
                            dtData.Rows(i).Item(c_AverageColumnName) = average.ToString("##.00")
                        End If
                    Else
                        dtData.Rows(i).Item(c_AverageColumnName) = average.ToString("##.00")
                        dtData.Rows(i).Item(c_TotalColumnName) = total.ToString("##.00")
                    End If
                    dtData.Rows(i).Item(c_AvgDataColumnName) = average

                    dtData.Rows(i).Item(c_PaperCntColumnName) = paperCount2
                    paperCount2 = 0
                    total = 0
                    paperCount = 0
                Next
            Else
                dgv.DataSource = Nothing
                Exit Sub
            End If

            If chkboxShowHasGrade.Checked And dtData.Rows.Count > 0 Then
                dtData3 = New DataTable
                dtData3 = dtData.Clone
                For Each row As DataRow In dtData.Rows
                    If row.Item(c_PaperCntColumnName) > 0 Then
                        dtData3.ImportRow(row)
                    End If
                Next
                dtData = New DataTable
                dtData.Merge(dtData3)
            End If

            If chkboxShowO.Checked Then
                Dim name As String = ""
                Dim l As Integer = 0
                For Each row As DataRow In dtData.Rows
                    name = row.Item(c_NameColumnName)
                    l = name.Length
                    If l > 2 Then
                        l = l - 2
                        name = name.Substring(0, 1) & "O" & name.Substring(2, l)
                        row.Item(c_NameColumnName) = name
                    Else
                        name = name.Substring(0, 1) & "O"
                        row.Item(c_NameColumnName) = name
                    End If
                Next
            End If

        

            If dtData.Rows.Count > 0 Then

                Dim dtsoft As DataTable = dtData.Clone()

                dtData.DefaultView.Sort = c_AvgDataColumnName & " DESC"
                dtsoft = dtData.DefaultView.ToTable
                dtData.Clear()
                dtData = dtsoft.Copy()


                For j As Integer = 0 To dtData.Rows.Count - 1
                    dtData.Rows(j).Item(c_RankColumnName) = j + 1
                Next

                

                Dim dtDataNoRepeat As New DataTable
                Dim arr As New ArrayList
                dtDataNoRepeat = dtData.Clone
                For i As Integer = 0 To dtData.Rows.Count - 1

                    If Not arr.Contains(dtData.Rows(i).Item("ID")) Then
                        arr.Add(dtData.Rows(i).Item("ID"))
                        dtDataNoRepeat.Rows.Add(dtData.Rows(i).ItemArray)
                    End If
                Next
                dgv.DataSource = dtDataNoRepeat.DefaultView
                LoadColumnText()
            Else
                dgv.DataSource = Nothing
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Function GetSubClassName(ByVal i As Integer) As String
        Dim n As String = ""
        For Each row As DataRow In dtSubClass.Rows
            If row.Item(c_IDColumnName) = i Then
                n = row.Item(c_NameColumnName)
            End If
        Next
        Return n
    End Function

    Private Function GetSchoolGroup(ByVal i As Integer) As String
        Dim n As String = ""
        For Each row As DataRow In dtSchoolGroup.Rows
            If row.Item(c_IDColumnName) = i Then
                n = row.Item(c_GroupColumnName)
            End If
        Next
        Return n
    End Function

    Private Sub GetMark(ByRef mark As Single, ByRef remarks As String, ByVal stuid As String, ByVal paper As Integer)

        Dim c As Array = dtDataOrigin.Select(c_IDColumnName & "=" & stuid & " AND " & _
                                                c_PaperIDColumnName & "=" & paper.ToString)
        If c.Length > 0 Then
            For Each row As DataRow In c
                mark = GetDbSingle(row.Item(c_MarkColumnName))
                remarks = GetDbString(row.Item(c_RemarksColumnName))
                Exit For
            Next
        Else
            mark = c_NullMark
            remarks = ""
        End If
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        Dim idx As Integer
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_RankColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_RankColumnName).Visible = True
            dgv.Columns.Item(c_RankColumnName).HeaderText = My.Resources.rank
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID

            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = "姓名"

            idx = 3
            For j As Integer = 0 To lstPaper.Count - 1
                dgv.Columns.Item("Paper" & j.ToString).DisplayIndex = idx
                dgv.Columns.Item("Paper" & j.ToString).Visible = True
                dgv.Columns.Item("Paper" & j.ToString).HeaderText = GetPaperName(lstPaper(j))
                idx = idx + 1
            Next

            If lstPaper.Count > 1 Then
                dgv.Columns.Item(c_AverageColumnName).DisplayIndex = idx
                dgv.Columns.Item(c_AverageColumnName).Visible = True
                dgv.Columns.Item(c_AverageColumnName).HeaderText = My.Resources.avgMark
                idx = idx + 1

                dgv.Columns.Item(c_TotalColumnName).DisplayIndex = idx
                If chkboxComputeTotal.Checked Then
                    dgv.Columns.Item(c_TotalColumnName).Visible = True
                Else
                    dgv.Columns.Item(c_TotalColumnName).Visible = False
                End If
                dgv.Columns.Item(c_TotalColumnName).HeaderText = My.Resources.totalMark
                idx = idx + 1
            End If
            intColFrom = idx
            Dim i As Integer = idx
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 And Not lstColName(index) = c_PaperDateColumnName Then
                    dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgv.Columns.Item(lstColName(index)).Visible = True
                    dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next

            Dim strSch As String
            If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                            strSch = frmMain.GetSchName(id)
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = strSch
                        Next
                    Case 1
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If

            If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
                For index As Integer = 0 To dgv.Rows.Count - 1
                    If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                        If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                            dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                        End If
                    End If

                Next
            End If

        End If
        RefreshSta()


    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GroupColumnName)
        lstColName.Add(c_SubClassNameColumnName)
        lstColName.Add(c_SeatNumColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_ICColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_HasCardColumnName)
        lstColName.Add(c_RegDateColumnName)
        lstColName.Add(c_SalesPersonColumnName)
        lstColName.Add(c_Cust1ColumnName)
        lstColName.Add(c_Cust2ColumnName)
        lstColName.Add(c_Cust3ColumnName)

        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        'lstColName.Add(c_ContentColumnName)
        lstColName.Add(c_PaperDateColumnName)
        lstColName.Add(c_EnrollSchColumnName)
        lstColName.Add(c_PunchCardNumColumnName)
        lstColName.Add(c_HseeMarkColumnName)

        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub RefreshSta()
        If dgv.Rows.Count > 0 Then
            Dim stuCnt As Integer = dgv.Rows.Count
            Dim top As Integer = stuCnt * (1 - 0.88)
            GetPositiveIndex(top)
            Dim high As Integer = stuCnt * (1 - 0.75)
            GetPositiveIndex(high)
            Dim mid As Integer = stuCnt * (1 - 0.5)
            GetPositiveIndex(mid)
            Dim low As Integer = stuCnt * (1 - 0.25)
            GetPositiveIndex(low)
            Dim bottom As Integer = stuCnt * (1 - 0.12)
            GetPositiveIndex(bottom)

            tboxBottom.Text = dgv.Rows(bottom).Cells(c_AverageColumnName).Value.ToString
            tboxHigh.Text = dgv.Rows(high).Cells(c_AverageColumnName).Value.ToString
            tboxLow.Text = dgv.Rows(low).Cells(c_AverageColumnName).Value.ToString
            tboxMid.Text = dgv.Rows(mid).Cells(c_AverageColumnName).Value.ToString
            tboxTop.Text = dgv.Rows(top).Cells(c_AverageColumnName).Value.ToString
        Else
            tboxBottom.Text = ""
            tboxHigh.Text = ""
            tboxLow.Text = ""
            tboxMid.Text = ""
            tboxTop.Text = ""
        End If
    End Sub

    Private Function GetPaperName(ByVal i As Integer) As String
        Dim idx As Integer = lstPaperShown.IndexOf(i)
        If idx > -1 Then
            If blShowPaperDate Then
                Return chklstPaper.Items(idx) & "(" & GetPaperDate(i) & ")"
            Else
                Return chklstPaper.Items(idx)
            End If

        Else
            Return ""
        End If
    End Function

    Private Function GetPaperDate(ByVal i As Integer) As String
        Dim s As String = ""
        Dim c As Array = dtPaper.Select(c_IDColumnName & "=" & i.ToString)
        If c.Length > 0 Then
            s = GetDateNum(GetDbDate(c(0).Item(c_DateTimeColumnName)))
        End If
        Return s
    End Function

    Private Function GetSchoolName(ByVal i As Integer) As String
        Dim r As String = ""
        If dtSchool.Rows.Count > 0 Then
            For index As Integer = 0 To dtSchool.Rows.Count - 1
                If dtSchool.Rows(index).Item(c_IDColumnName) = i Then
                    r = dtSchool.Rows(index).Item(c_NameColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmPostGrade_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmPostGrade_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPostGrade_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub chklstPaper_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstPaper.ItemCheck
        'CSOL.Logger.LogMessage("frmPostGrade chklstPaper_ItemCheck1")
        Dim intC As Integer
        intC = lstPaperShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstPaper.Add(intC)
        Else
            lstPaper.Remove(intC)
        End If

        'CSOL.Logger.LogMessage("frmPostGrade chklstPaper_ItemCheck2")
        RefreshPaperData()
        'CSOL.Logger.LogMessage("frmPostGrade chklstPaper_ItemCheck3")
    End Sub

    Private Sub chkboxShowRemarks_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowRemarks.CheckedChanged
        MakeTable()
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click

        Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
        Dim flag As Boolean = False
        frm.ShowDialog()
        If frmMain.GetOkState Then
            lstColShow = frmMain.GetCurrentList
            lstColTxt = frmMain.GetCurrentList2
            intColSchType = frmMain.GetCurrentValue
            Dim idx As Integer = lstColName.IndexOf(c_PaperDateColumnName)
            If idx > -1 And idx < lstColShow.Count Then
                If lstColShow(idx) = 1 Then
                    flag = True
                End If
            End If
            If Not flag = blShowPaperDate Then
                blShowPaperDate = flag
            End If

            LoadColumnText()
        End If

    End Sub

    Private Sub mnuStuInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuStuInfo.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub chkboxShowAttend_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowAttend.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxShowO_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowO.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxShowHasGrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowHasGrade.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxShowCancel_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowCancel.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxComputeTotal_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxComputeTotal.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxNoDigits_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxNoDigits.CheckedChanged
        MakeTable()
    End Sub

    Private Sub chkboxNoAbsent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxNoAbsent.CheckedChanged
        MakeTable()
    End Sub

    Private Sub linklblAllSc_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblAllSc.LinkClicked
        'CSOL.Logger.LogMessage("frmPostGrade linklblAllSc_LinkClicked1")
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
        'CSOL.Logger.LogMessage("frmPostGrade linklblAllSc_LinkClicked2")

    End Sub


    Private Sub linklblNoSc_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblNoSc.LinkClicked
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
    End Sub

    Private Sub linklblAllPaper_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblAllPaper.LinkClicked
        'CSOL.Logger.LogMessage("frmPostGrade linklblAllPaper_LinkClicked1")
        For index As Integer = 0 To chklstPaper.Items.Count - 1
            chklstPaper.SetItemChecked(index, True)
        Next
        'CSOL.Logger.LogMessage("frmPostGrade linklblAllPaper_LinkClicked2")
    End Sub


    Private Sub linklblNoPaper_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblNoPaper.LinkClicked
        For index As Integer = 0 To chklstPaper.Items.Count - 1
            chklstPaper.SetItemChecked(index, False)
        Next
    End Sub
End Class