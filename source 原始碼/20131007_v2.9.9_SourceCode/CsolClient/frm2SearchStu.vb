﻿Public Class frm2SearchStu
    Private dt As New DataTable
    Dim blFlag As Boolean = True

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgv.Columns.Item(c_Tel1ColumnName).Visible = True
            dgv.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            dgv.Columns.Item(c_Tel2ColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_Tel2ColumnName).Visible = True
            dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_SchoolColumnName).Visible = True
        End If
    End Sub

    Private Sub tboxID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged
        If blFlag Then
            If IsNumeric(tboxID.Text) Then
                If radbut5.Checked Then
                    If tboxID.Text.Trim.Length >= 5 Then
                        Dim s As String = tboxID.Text.Trim
                        blFlag = False
                        dt = objCsol.SearchStu(6, s, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                        'dt = objCsol.SearchStu(6, s, c_StuTypeFormal)
                        dgv.DataSource = dt
                        LoadColumnText()
                        blFlag = True
                    End If
                ElseIf radbut4.Checked Then
                    If tboxID.Text.Length >= 4 Then
                        blFlag = False
                        Dim s As String = tboxID.Text.Trim
                        dt = objCsol.SearchStu(6, s, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                        dgv.DataSource = dt
                        LoadColumnText()
                        blFlag = True
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub tboxCardNum_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxCardNum.TextChanged         '100226
        If blFlag Then
            If tboxCardNum.Text.Trim.Length = 10 Then
                butSearch.Focus()
                Dim str As String = tboxCardNum.Text.Trim
                dt = objCsol.SearchStu(9, str, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                             c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                dgv.DataSource = dt
                LoadColumnText()
            End If
        End If
    End Sub                                                                                                                                 '100226

    Private Sub tboxName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxName.TextChanged
        If blFlag Then
            If tboxName.Text.Trim.Length > 1 Then
                dt = objCsol.SearchStu(2, tboxName.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                dgv.DataSource = dt
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub tboxTel_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxTel.TextChanged
        If blFlag Then

            If tboxTel.Text.Trim.Length < 6 Then
                dt.Clear()
            ElseIf tboxTel.Text.Trim.Length > 5 Then
                If dt.Rows.Count > 0 Then
                    Dim dt_filter As New DataTable
                    dt_filter = dt.Clone
                    Dim rows() As DataRow = dt.Select(String.Format("{1} like '%{0}%' Or {2} like '%{0}%' Or {3} like '%{0}%' Or {4} like '%{0}%' Or {5} like '%{0}%'", _
                                                                                    tboxTel.Text.Trim(), c_Tel1ColumnName, c_Tel2ColumnName, c_MobileColumnName, c_DadMobileColumnName, c_MumMobileColumnName))
                    For Each row As DataRow In rows
                        dt_filter.Rows.Add(row.ItemArray)
                    Next
                    dgv.DataSource = dt_filter
                    LoadColumnText()
                Else
                    dt = objCsol.SearchStu_Short(3, tboxTel.Text.Trim, c_StuTypeFormal)
                    dgv.DataSource = dt
                    LoadColumnText()
                End If
            End If
            'If tboxTel.Text.Trim.Length = 6 Then
            '    'dt = objCsol.SearchStu(3, tboxTel.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
            '    '               c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName)
            '    dt = objCsol.SearchStu_Short(3, tboxTel.Text.Trim, c_StuTypeFormal)
            '    dgv.DataSource = dt
            '    LoadColumnText()
            'ElseIf tboxTel.Text.Trim.Length > 6 Then
            '    Dim dt_filter As New DataTable
            '    If dt.Rows.Count = 0 Then
            '        dt = objCsol.SearchStu_Short(3, tboxTel.Text.Trim, c_StuTypeFormal)
            '        dgv.DataSource = dt
            '    ElseIf dt.Rows.Count > 0 Then
            '        dt_filter = dt.Clone
            '        Dim rows() As DataRow = dt.Select(String.Format("{1} like '%{0}%' Or {2} like '%{0}%' Or {3} like '%{0}%' Or {4} like '%{0}%' Or {5} like '%{0}%'", _
            '                                                                         tboxTel.Text.Trim(), c_Tel1ColumnName, c_Tel2ColumnName, c_MobileColumnName, c_DadMobileColumnName, c_MumMobileColumnName))
            '        For Each row As DataRow In rows
            '            dt_filter.Rows.Add(row.ItemArray)
            '        Next
            '        dgv.DataSource = dt_filter
            '        LoadColumnText()
            '    End If
            'End If
        End If
    End Sub

    Private Sub tboxParent_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxParent.TextChanged
        If blFlag Then
            If tboxParent.Text.Trim.Length > 1 Then
                dt = objCsol.SearchStu(4, tboxParent.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                dgv.DataSource = dt
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub dgv_CellDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellDoubleClick
        If blFlag Then
            If dgv.SelectedRows.Count > 0 Then
                Dim s As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                frmMain.SetCurrentStu(s)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSearch.Click
        If blFlag Then
            lbMes.Text = ""
            If IsNumeric(tboxID.Text) Then
                blFlag = False
                Dim s As String = tboxID.Text.Trim
                dt = objCsol.SearchStu(6, s, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                        c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                RefreashData()
                blFlag = True
            ElseIf tboxName.Text.Trim.Length > 0 Then
                dt = objCsol.SearchStu(2, tboxName.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                RefreashData()
            ElseIf tboxTel.Text.Trim.Length > 1 Then
                dt = objCsol.SearchStu(3, tboxTel.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                RefreashData()
            ElseIf tboxParent.Text.Trim.Length > 0 Then
                dt = objCsol.SearchStu(4, tboxParent.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                RefreashData()
            End If
        End If
    End Sub

    Public Sub New()
        InitializeComponent()

        frmMain.SetOkState(False)

    End Sub

    Private Sub butDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDetails.Click
        If blFlag Then
            If dgv.SelectedRows.Count > 0 Then
                Dim s As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                frmMain.SetCurrentStu(s)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub dgv_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgv.KeyDown
        If e.KeyCode = Keys.Enter Then
            If blFlag Then
                If dgv.SelectedRows.Count > 0 Then
                    Dim s As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                    frmMain.SetCurrentStu(s)
                    frmMain.SetOkState(True)
                    Me.Close()
                End If
            End If
        End If
    End Sub


    Private Sub tboxSch_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxSch.TextChanged
        If blFlag Then
            If tboxSch.Text.Trim.Length >= 2 Then
                dt = objCsol.SearchStu(10, tboxSch.Text.Trim, c_StuTypeFormal).DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, c_SchoolColumnName)
                dgv.DataSource = dt
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub RefreashData()
        If dt.Rows.Count > 0 Then
            lbMes.Text = ""
            dgv.DataSource = dt
            LoadColumnText()
        Else
            lbMes.Text = "無搜尋結果"

        End If
        
    End Sub

End Class