﻿Public Class frmUsrInfo
    Dim dt As New DataTable
    Dim blNew As Boolean = True
    Dim blNewAuth As Boolean = True
    Dim dsAuth As New DataSet
    Dim strId As String = ""
    Dim blSaved As Boolean = False
    Dim dtSch As New DataTable
    Dim lstJun As New ArrayList
    Dim lstHih As New ArrayList
    Dim lstUni As New ArrayList
    Dim dtClassAuth As New DataTable
    Dim dtSubClassAuth As New DataTable
    Dim dtUsrAuth As New DataTable
    Dim dtAllClassAuth As New DataTable
    Dim lstClassID As New ArrayList
    Dim lstSubClassID As New ArrayList
    Dim lstScClassID As New ArrayList
    Dim lstClassShown As New ArrayList
    Dim lstUsr As New ArrayList

    Public Sub New(ByRef d As DataTable)
        InitializeComponent()

        Me.Text = My.Resources.frmUsrInfo
        RefreshData()
        If d.Rows.Count > 0 Then 'modify
            dt = d
            lstUsr = frmMain.GetCurrentList
            strId = dt.Rows(0).Item(c_IDColumnName)
            blNew = False
            RefreshInfo()
        ElseIf d.Rows.Count = 0 Then 'new
            strId = objCsol.GenUsrId
            d.Rows.Add(strId, "", "12345", 0, _
               1, 0, "", "", _
              "", "", "", _
              "", "", "", "", _
              "", "", "", "", _
              Now, "", "", _
              Now, 0, 0, _
              0, 0, 0)
            dt = d
            blNew = True
            tboxID.Text = strId
            butPrevious.Visible = False
            butNext.Visible = False
            butPrevious2.Visible = False
            butNext2.Visible = False
            butPrevious3.Visible = False
            butNext3.Visible = False
        End If

        LoadColumnText()
        frmMain.SetOkState(False)
    End Sub

    Public Sub RefreshData(ByRef d As DataTable)
        If d.Rows.Count > 0 Then 'modify
            dt = d
            strId = dt.Rows(0).Item(c_IDColumnName)
            blNew = False
            RefreshInfo()
        End If
    End Sub

    Private Sub frmUsrInfo_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blSaved = False And blNew Then
            objCsol.DeleteUsrByID(strId)
        End If
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmUsrInfo_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        dtSch = frmMain.GetSchList
        For index As Integer = 0 To dtSch.Rows.Count - 1
            Select Case dtSch.Rows(index).Item(c_TypeIdColumnName)
                Case 1
                    lstJun.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    cboxJuniory.Items.Add(dtSch.Rows(index).Item(c_NameColumnName))
                Case 2
                    lstHih.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    cboxHigh.Items.Add(dtSch.Rows(index).Item(c_NameColumnName))
                Case 3
                    lstUni.Add(dtSch.Rows(index).Item(c_IDColumnName))
                    cboxUniversity.Items.Add(dtSch.Rows(index).Item(c_NameColumnName))
            End Select
        Next
    End Sub

    Private Sub LoadColumnText()
        'To be added
    End Sub


    Private Sub InitSelections()

    End Sub

    Private Sub frmUsrInfo_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub


    Private Sub tabCtrl_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabCtrl.Click
        If tabCtrl.SelectedIndex = 3 Then
            Me.Close()
        ElseIf tabCtrl.SelectedIndex = 1 Or tabCtrl.SelectedIndex = 2 Then
            If blNewAuth Then
                dsAuth = objCsol.ListUsrAllAuthority(strId)
                RefreshAuth()
                blNewAuth = False
            End If

        End If
    End Sub

    Private Sub RefreshClassList()
        dtClassAuth = dsAuth.Tables(c_ClassAuthorityTableName)
        dtSubClassAuth = dsAuth.Tables(c_SubClassAuthorityTableName)
        dtAllClassAuth = dsAuth.Tables(c_AllClassAuthorityTableName)
        lstClassID.Clear()
        lstScClassID.Clear()
        lstSubClassID.Clear()
        lstboxClass.Items.Clear()
        lstClassShown.Clear()
        If dtAllClassAuth.Rows.Count > 0 Then
            radbutManageAllClass.Checked = True
        Else
            Dim i As Integer
            If chkBoxShowPast.Checked Then
                For index As Integer = 0 To dtClassAuth.Rows.Count - 1
                    lstboxClass.Items.Add(dtClassAuth.Rows(index).Item(c_NameColumnName))
                    lstClassID.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    lstClassShown.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                Next

                For index As Integer = 0 To dtSubClassAuth.Rows.Count - 1
                    i = dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName)
                    If lstClassShown.IndexOf(i) = -1 Then
                        lstboxClass.Items.Add(dtSubClassAuth.Rows(index).Item(c_ClassNameColumnName))
                        lstClassShown.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                    If lstClassID.IndexOf(i) = -1 Then
                        lstSubClassID.Add(dtSubClassAuth.Rows(index).Item(c_SubClassIDColumnName))
                        lstScClassID.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClassAuth.Rows.Count - 1
                    If dtClassAuth.Rows(index).Item(c_EndColumnName) >= Now Then
                        lstboxClass.Items.Add(dtClassAuth.Rows(index).Item(c_NameColumnName))
                        lstClassID.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        lstClassShown.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                Next

                For index As Integer = 0 To dtSubClassAuth.Rows.Count - 1
                    If dtSubClassAuth.Rows(index).Item(c_EndColumnName) >= Now Then
                        i = dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName)
                        If lstClassShown.IndexOf(i) = -1 Then
                            lstboxClass.Items.Add(dtSubClassAuth.Rows(index).Item(c_ClassNameColumnName))
                            lstClassShown.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        End If
                        If lstClassID.IndexOf(i) = -1 Then
                            lstSubClassID.Add(dtSubClassAuth.Rows(index).Item(c_SubClassIDColumnName))
                            lstScClassID.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        End If
                    End If
                Next
            End If
            radbutManageClasses.Checked = True
        End If
    End Sub

    Private Sub RefreshAuth()
        dtUsrAuth = dsAuth.Tables(c_UsrAuthorityTableName)
        dtClassAuth = dsAuth.Tables(c_ClassAuthorityTableName)
        dtSubClassAuth = dsAuth.Tables(c_SubClassAuthorityTableName)
        dtAllClassAuth = dsAuth.Tables(c_AllClassAuthorityTableName)
        lstClassID.Clear()
        lstScClassID.Clear()
        lstSubClassID.Clear()
        lstboxClass.Items.Clear()
        lstClassShown.Clear()
        If dtAllClassAuth.Rows.Count > 0 Then
            radbutManageAllClass.Checked = True
        Else
            Dim i As Integer
            If chkBoxShowPast.Checked Then
                For index As Integer = 0 To dtClassAuth.Rows.Count - 1
                    lstboxClass.Items.Add(dtClassAuth.Rows(index).Item(c_NameColumnName))
                    lstClassID.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    lstClassShown.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                Next

                For index As Integer = 0 To dtSubClassAuth.Rows.Count - 1
                    i = dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName)
                    If lstClassShown.IndexOf(i) = -1 Then
                        lstboxClass.Items.Add(dtSubClassAuth.Rows(index).Item(c_ClassNameColumnName))
                        lstClassShown.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                    If lstClassID.IndexOf(i) = -1 Then
                        lstSubClassID.Add(dtSubClassAuth.Rows(index).Item(c_SubClassIDColumnName))
                        lstScClassID.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClassAuth.Rows.Count - 1
                    If dtClassAuth.Rows(index).Item(c_EndColumnName) >= Now Then
                        lstboxClass.Items.Add(dtClassAuth.Rows(index).Item(c_NameColumnName))
                        lstClassID.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        lstClassShown.Add(dtClassAuth.Rows(index).Item(c_ClassIDColumnName))
                    End If
                Next

                For index As Integer = 0 To dtSubClassAuth.Rows.Count - 1
                    If dtSubClassAuth.Rows(index).Item(c_EndColumnName) >= Now Then
                        i = dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName)
                        If lstClassShown.IndexOf(i) = -1 Then
                            lstboxClass.Items.Add(dtSubClassAuth.Rows(index).Item(c_ClassNameColumnName))
                            lstClassShown.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        End If
                        If lstClassID.IndexOf(i) = -1 Then
                            lstSubClassID.Add(dtSubClassAuth.Rows(index).Item(c_SubClassIDColumnName))
                            lstScClassID.Add(dtSubClassAuth.Rows(index).Item(c_ClassIDColumnName))
                        End If
                    End If
                Next
            End If
        End If

        Dim c1 As String = "COUNT (" & c_IDColumnName & ")"
        Dim c2 As String = c_ItemIDColumnName & "="

        'print
        If dtUsrAuth.Compute(c1, c2 & "0") > 0 Then
            chkboxAbsent.Checked = True
        Else
            chkboxAbsent.Checked = False
        End If

        If dtUsrAuth.Compute(c1, c2 & "1") > 0 Then
            chkBoxCome.Checked = True
        Else
            chkBoxCome.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "2") > 0 Then
            chkboxStuInfo.Checked = True
        Else
            chkboxStuInfo.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "3") > 0 Then
            chkboxSeat.Checked = True
        Else
            chkboxSeat.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "4") > 0 Then
            chkboxAnalysis.Checked = True
        Else
            chkboxAnalysis.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "5") > 0 Then
            chkboxBook.Checked = True
        Else
            chkboxBook.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "6") > 0 Then
            chkboxRpt.Checked = True
        Else
            chkboxRpt.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "7") > 0 Then
            chkboxPay.Checked = True
        Else
            chkboxPay.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "8") > 0 Then
            chkboxPayRec.Checked = True
        Else
            chkboxPayRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "9") > 0 Then
            chkboxOwe.Checked = True
        Else
            chkboxOwe.Checked = False
        End If

        'export
        If dtUsrAuth.Compute(c1, c2 & "10") > 0 Then
            chkboxSendAbsent.Checked = True
        Else
            chkboxSendAbsent.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "11") > 0 Then
            chkBoxSendCome.Checked = True
        Else
            chkBoxSendCome.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "12") > 0 Then
            chkboxSendStuInfo.Checked = True
        Else
            chkboxSendStuInfo.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "13") > 0 Then
            chkboxSendSeat.Checked = True
        Else
            chkboxSendSeat.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "14") > 0 Then
            chkboxSendAnalysis.Checked = True
        Else
            chkboxSendAnalysis.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "15") > 0 Then
            chkboxSendBook.Checked = True
        Else
            chkboxSendBook.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "16") > 0 Then
            chkboxSendPay.Checked = True
        Else
            chkboxSendPay.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "17") > 0 Then
            chkboxSendPayRec.Checked = True
        Else
            chkboxSendPayRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "18") > 0 Then
            chkboxSendOwe.Checked = True
        Else
            chkboxSendOwe.Checked = False
        End If

        'accounting
        If dtUsrAuth.Compute(c1, c2 & "19") > 0 Then
            chkboxMkRpt.Checked = True
        Else
            chkboxMkRpt.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "20") > 0 Then
            chkboxChkRec.Checked = True
        Else
            chkboxChkRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "21") > 0 Then
            ckhboxAddFee.Checked = True
        Else
            ckhboxAddFee.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "22") > 0 Then
            chkboxModRpt.Checked = True
        Else
            chkboxModRpt.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "83") > 0 Then
            chkboxDiscMod.Checked = True
        Else
            chkboxDiscMod.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "23") > 0 Then
            chkboxDelRpt.Checked = True
        Else
            chkboxDelRpt.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "24") > 0 Then
            chkboxAddOwe.Checked = True
        Else
            chkboxAddOwe.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "25") > 0 Then
            chkboxKpReturn.Checked = True
        Else
            chkboxKpReturn.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "26") > 0 Then
            chkboxModReturn.Checked = True
        Else
            chkboxModReturn.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "27") > 0 Then
            chkoxPaybyClass.Checked = True
        Else
            chkoxPaybyClass.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "28") > 0 Then
            chkboxPaybyDate.Checked = True
        Else
            chkboxPaybyDate.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "29") > 0 Then
            chkboxClassPay.Checked = True
        Else
            chkboxClassPay.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "30") > 0 Then
            chkboxSetDisc.Checked = True
        Else
            chkboxSetDisc.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "31") > 0 Then
            chkboxUnitPay.Checked = True
        Else
            chkboxUnitPay.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "32") > 0 Then
            chkboxAddDisc.Checked = True
        Else
            chkboxAddDisc.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "33") > 0 Then
            chkboxModDisc.Checked = True
        Else
            chkboxModDisc.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "34") > 0 Then
            chkboxDelDisc.Checked = True
        Else
            chkboxDelDisc.Checked = False
        End If

        'expense
        If dtUsrAuth.Compute(c1, c2 & "35") > 0 Then
            chkboxAddList.Checked = True
        Else
            chkboxAddList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "36") > 0 Then
            chkboxModList.Checked = True
        Else
            chkboxModList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "37") > 0 Then
            chkboxDelList.Checked = True
        Else
            chkboxDelList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "38") > 0 Then
            chkboxReadList.Checked = True
        Else
            chkboxReadList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "39") > 0 Then
            chkboxPList.Checked = True
        Else
            chkboxPList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "40") > 0 Then
            chkboxSendList.Checked = True
        Else
            chkboxSendList.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "41") > 0 Then
            chkboxMkList.Checked = True
        Else
            chkboxMkList.Checked = False
        End If

        'employee
        If dtUsrAuth.Compute(c1, c2 & "42") > 0 Then
            chkboxAddEmp.Checked = True
        Else
            chkboxAddEmp.Checked = False
        End If

        If dtUsrAuth.Compute(c1, c2 & "84") > 0 Then
            chkboxDelAble.Checked = True
        Else
            chkboxDelAble.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "43") > 0 Then
            chkboxReademp.Checked = True
        Else
            chkboxReademp.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "44") > 0 Then
            chkboxModEmp.Checked = True
        Else
            chkboxModEmp.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "45") > 0 Then
            chkboxStopAcc.Checked = True
        Else
            chkboxStopAcc.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "46") > 0 Then
            chkboxReadRec.Checked = True
        Else
            chkboxReadRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "47") > 0 Then
            chkboxModRec.Checked = True
        Else
            chkboxModRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "48") > 0 Then
            chkboxDelRec.Checked = True
        Else
            chkboxDelRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "49") > 0 Then
            chkboxPRec.Checked = True
        Else
            chkboxPRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "50") > 0 Then
            chkboxSendRec.Checked = True
        Else
            chkboxSendRec.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "51") > 0 Then
            chkboxMyPerf.Checked = True
        Else
            chkboxMyPerf.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "52") > 0 Then
            chkboxAllPerf.Checked = True
        Else
            chkboxAllPerf.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "53") > 0 Then
            chkboxModWork.Checked = True
        Else
            chkboxModWork.Checked = False
        End If

        'others
        If dtUsrAuth.Compute(c1, c2 & "54") > 0 Then
            CheckBox19.Checked = True
        Else
            CheckBox19.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "55") > 0 Then
            CheckBox18.Checked = True
        Else
            CheckBox18.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "56") > 0 Then
            CheckBox17.Checked = True
        Else
            CheckBox17.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "57") > 0 Then
            CheckBox16.Checked = True
        Else
            CheckBox16.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "58") > 0 Then
            CheckBox15.Checked = True
        Else
            CheckBox15.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "59") > 0 Then
            CheckBox14.Checked = True
        Else
            CheckBox14.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "60") > 0 Then
            CheckBox34.Checked = True
        Else
            CheckBox34.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "61") > 0 Then
            CheckBox33.Checked = True
        Else
            CheckBox33.Checked = False
        End If

        If dtUsrAuth.Compute(c1, c2 & "62") > 0 Then
            CheckBox13.Checked = True
        Else
            CheckBox13.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "63") > 0 Then
            CheckBox12.Checked = True
        Else
            CheckBox12.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "64") > 0 Then
            CheckBox11.Checked = True
        Else
            CheckBox11.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "65") > 0 Then
            CheckBox10.Checked = True
        Else
            CheckBox10.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "66") > 0 Then
            CheckBox9.Checked = True
        Else
            CheckBox9.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "67") > 0 Then
            CheckBox8.Checked = True
        Else
            CheckBox8.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "68") > 0 Then
            CheckBox7.Checked = True
        Else
            CheckBox7.Checked = False
        End If

        'basic
        If dtUsrAuth.Compute(c1, c2 & "69") > 0 Then
            CheckBox32.Checked = True
        Else
            CheckBox32.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "70") > 0 Then
            CheckBox31.Checked = True
        Else
            CheckBox31.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "71") > 0 Then
            CheckBox30.Checked = True
        Else
            CheckBox30.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "72") > 0 Then
            CheckBox29.Checked = True
        Else
            CheckBox29.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "73") > 0 Then
            CheckBox28.Checked = True
        Else
            CheckBox28.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "74") > 0 Then
            CheckBox27.Checked = True
        Else
            CheckBox27.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "75") > 0 Then
            CheckBox26.Checked = True
        Else
            CheckBox26.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "76") > 0 Then
            CheckBox25.Checked = True
        Else
            CheckBox25.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "77") > 0 Then
            CheckBox24.Checked = True
        Else
            CheckBox24.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "78") > 0 Then
            CheckBox23.Checked = True
        Else
            CheckBox23.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "79") > 0 Then
            CheckBox21.Checked = True
        Else
            CheckBox21.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "80") > 0 Then
            CheckBox22.Checked = True
        Else
            CheckBox22.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "81") > 0 Then
            CheckBox20.Checked = True
        Else
            CheckBox20.Checked = False
        End If
        If dtUsrAuth.Compute(c1, c2 & "82") > 0 Then
            CheckBox2.Checked = True
        Else
            CheckBox2.Checked = False
        End If

    End Sub

    Private Sub RefreshInfo()
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        lnklabSave.Enabled = False
        With dt.Rows(0)
            tboxID.Text = .Item(c_IDColumnName)
            lblAuthID1.Text = .Item(c_IDColumnName)
            lblAuthID2.Text = .Item(c_IDColumnName)
            lblAuthName1.Text = .Item(c_NameColumnName)
            lblAuthName2.Text = .Item(c_NameColumnName)
            tboxName.Text = .Item(c_NameColumnName)
            If .Item(c_IsSalesColumnName) = c_IsSales Then
                chkboxIsSales.Checked = True
            Else
                chkboxIsSales.Checked = False
            End If
            tboxTel1.Text = .Item(c_Phone1ColumnName)
            tboxTel2.Text = .Item(c_Phone2ColumnName)
            tboxIC.Text = .Item(c_ICColumnName)
            tboxAddr.Text = .Item(c_AddressColumnName)
            tboxAccount.Text = .Item(c_AccntNameColumnName)
            tboxEmail.Text = .Item(c_EmailColumnName)
            tBoxEngName.Text = .Item(c_EnglishNameColumnName)
            tboxMobile.Text = .Item(c_MobileColumnName)
            tboxParent.Text = .Item(c_GuardianColumnName)
            tboxParentMobile.Text = .Item(c_GuardMobileColumnName)
            tboxPostal.Text = .Item(c_PostalCodeColumnName)
            tboxRemarks.Text = .Item(c_RemarksColumnName)
            tboxStar.Text = .Item(c_HoroscopeColumnName)
            If Not DBNull.Value.Equals(.Item(c_BirthdayColumnName)) Then
                dtpickBirth.Value = .Item(c_BirthdayColumnName)
            End If

            If Not DBNull.Value.Equals(.Item(c_OnBoardDateColumnName)) Then
                dtpickOnBoard.Value = .Item(c_OnBoardDateColumnName)
            End If

            cboxBloodType.SelectedIndex = .Item(c_BloodTypeColumnName)
            cboxSex.SelectedItem = .Item(c_SexColumnName).ToString.Trim

            Dim i As Integer = lstJun.IndexOf(.Item(c_JuniorSchIDColumnName))
            If i > -1 Then
                cboxJuniory.SelectedIndex = i
            End If

            i = lstHih.IndexOf(.Item(c_HighSchIDColumnName))
            If i > -1 Then
                cboxHigh.SelectedIndex = i
            End If

            i = lstUni.IndexOf(.Item(c_UniversityIDColumnName))
            If i > -1 Then
                cboxUniversity.SelectedIndex = i
            End If

            If strId.Length = 8 Then
                Dim f As String = GetPhotoPath() & strId & ".bmp"
                If CheckFileExist(f) Then
                    Dim bm As New Bitmap(f)
                    picbox.Image = bm
                End If
            End If
        End With
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        SaveAuth()
        SaveInfo()
        MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Me.Close()
    End Sub

    Private Sub SaveInfo()
        With dt.Rows(0)
            .Item(c_NameColumnName) = tboxName.Text
            If chkboxIsSales.Checked Then
                .Item(c_IsSalesColumnName) = c_IsSales
            Else
                .Item(c_IsSalesColumnName) = c_IsSalesNo
            End If
            .Item(c_Phone1ColumnName) = tboxTel1.Text
            .Item(c_Phone2ColumnName) = tboxTel2.Text
            .Item(c_ICColumnName) = tboxIC.Text
            .Item(c_AddressColumnName) = tboxAddr.Text
            .Item(c_AccntNameColumnName) = tboxAccount.Text
            .Item(c_EmailColumnName) = tboxEmail.Text
            .Item(c_EnglishNameColumnName) = tBoxEngName.Text
            .Item(c_MobileColumnName) = tboxMobile.Text
            .Item(c_GuardianColumnName) = tboxParent.Text
            .Item(c_GuardMobileColumnName) = tboxParentMobile.Text
            .Item(c_PostalCodeColumnName) = tboxPostal.Text
            .Item(c_RemarksColumnName) = tboxRemarks.Text
            .Item(c_HoroscopeColumnName) = tboxStar.Text

            .Item(c_BirthdayColumnName) = dtpickBirth.Value
            .Item(c_OnBoardDateColumnName) = dtpickOnBoard.Value

            .Item(c_BloodTypeColumnName) = cboxBloodType.SelectedIndex
            .Item(c_SexColumnName) = cboxSex.SelectedItem

            Dim i As Integer = cboxJuniory.SelectedIndex
            If i > -1 Then
                .Item(c_JuniorSchIDColumnName) = lstJun(i)
            End If

            i = cboxHigh.SelectedIndex
            If i > -1 Then
                .Item(c_HighSchIDColumnName) = lstHih(i)
            End If

            i = cboxUniversity.SelectedIndex
            If i > -1 Then
                .Item(c_UniversityIDColumnName) = lstUni(i)
            End If

        End With
        objCsol.UpdateUsrInfo(dt)
        blSaved = True
        frmMain.SetOkState(True)
        frmMain.RefreshUsrListForm()
        MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Me.Close()
    End Sub

    Private Sub butAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAdd.Click
        Dim frm As New frm2SelectClassAuthority
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim classID As ArrayList = frmMain.GetCurrentValueArr
            Dim className As ArrayList = frmMain.GetCurrentStringArr
            Dim dtSubClass As DataTable = frmMain.GetSubClassList
            Dim classIDInput As New ArrayList
            Dim j As Integer
            For i As Integer = 0 To classID.Count - 1
                If lstClassShown.IndexOf(classID.Item(i)) = -1 Then
                    lstClassShown.Add(classID.Item(i))
                    lstboxClass.Items.Add(className.Item(i))
                End If
            Next
            Dim lst As ArrayList = frmMain.GetCurrentList
            For i As Integer = 0 To lst.Count - 1
                j = lstSubClassID.IndexOf(lst(i))
                If j = -1 Then
                    lstSubClassID.Add(lst(i))
                End If
            Next
            Dim flag As Boolean = False
            For i As Integer = 0 To lstSubClassID.Count - 1
                dtSubClass.DefaultView.RowFilter = "ID=" + lstSubClassID.Item(i).ToString.Trim
                Dim a As Integer = dtSubClass.DefaultView.Item(0).Row.Item("classID")
                If lstScClassID.IndexOf(a) = -1 Then
                    lstScClassID.Add(a)
                    flag = True
                ElseIf flag Then
                    lstScClassID.Add(a)
                End If
            Next
        End If
    End Sub

    Private Sub butDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDel.Click
        If lstboxClass.SelectedIndex > -1 Then
            Dim j As Integer = lstboxClass.SelectedIndex
            Dim i As Integer = lstClassShown(j)
            lstboxClass.Items.RemoveAt(j)
            lstClassShown.RemoveAt(j)
            j = lstClassID.IndexOf(i)
            If j > -1 Then
                lstClassID.RemoveAt(j)
            End If
            Dim lst1 As New ArrayList
            Dim lst2 As New ArrayList
            For index As Integer = 0 To lstScClassID.Count - 1
                If lstScClassID(index) <> i Then
                    lst1.Add(lstScClassID(index))
                    lst2.Add(lstSubClassID(index))
                End If
            Next

            lstScClassID = lst1
            lstSubClassID = lst2
        End If
    End Sub

    Private Sub butSave2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave2.Click
        SaveAuth()
        SaveInfo()
        MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Me.Close()
    End Sub


    Private Sub butSave3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave3.Click
        SaveAuth()
        SaveInfo()
        MsgBox(My.Resources.showMsgSaved, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Me.Close()
    End Sub

    Private Sub SaveAuth()
        If Not frmMain.CheckAuth(44) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim lstItems As New ArrayList
        If chkboxAbsent.Checked Then
            lstItems.Add(0)
        End If
        If chkBoxCome.Checked Then
            lstItems.Add(1)
        End If
        If chkboxStuInfo.Checked Then
            lstItems.Add(2)
        End If
        If chkboxSeat.Checked Then
            lstItems.Add(3)
        End If
        If chkboxAnalysis.Checked Then
            lstItems.Add(4)
        End If
        If chkboxBook.Checked Then
            lstItems.Add(5)
        End If
        If chkboxRpt.Checked Then
            lstItems.Add(6)
        End If
        If chkboxPay.Checked Then
            lstItems.Add(7)
        End If
        If chkboxPayRec.Checked Then
            lstItems.Add(8)
        End If
        If chkboxOwe.Checked Then
            lstItems.Add(9)
        End If

        'export
        If chkboxSendAbsent.Checked Then
            lstItems.Add(10)
        End If
        If chkBoxSendCome.Checked Then
            lstItems.Add(11)
        End If
        If chkboxSendStuInfo.Checked Then
            lstItems.Add(12)
        End If
        If chkboxSendSeat.Checked Then
            lstItems.Add(13)
        End If
        If chkboxSendAnalysis.Checked Then
            lstItems.Add(14)
        End If
        If chkboxSendBook.Checked Then
            lstItems.Add(15)
        End If
        If chkboxSendPay.Checked Then
            lstItems.Add(16)
        End If
        If chkboxSendPayRec.Checked Then
            lstItems.Add(17)
        End If
        If chkboxSendOwe.Checked Then
            lstItems.Add(18)
        End If

        'accounting
        If chkboxMkRpt.Checked Then
            lstItems.Add(19)
        End If
        If chkboxChkRec.Checked Then
            lstItems.Add(20)
        End If
        If ckhboxAddFee.Checked Then
            lstItems.Add(21)
        End If
        If chkboxModRpt.Checked Then
            lstItems.Add(22)
        End If
        If chkboxDiscMod.Checked Then
            lstItems.Add(83)
        End If
        If chkboxDelRpt.Checked Then
            lstItems.Add(23)
        End If
        If chkboxAddOwe.Checked Then
            lstItems.Add(24)
        End If
        If chkboxKpReturn.Checked Then
            lstItems.Add(25)
        End If
        If chkboxModReturn.Checked Then
            lstItems.Add(26)
        End If
        If chkoxPaybyClass.Checked Then
            lstItems.Add(27)
        End If
        If chkboxPaybyDate.Checked Then
            lstItems.Add(28)
        End If
        If chkboxClassPay.Checked Then
            lstItems.Add(29)
        End If
        If chkboxSetDisc.Checked Then
            lstItems.Add(30)
        End If
        If chkboxUnitPay.Checked Then
            lstItems.Add(31)
        End If
        If chkboxAddDisc.Checked Then
            lstItems.Add(32)
        End If
        If chkboxModDisc.Checked Then
            lstItems.Add(33)
        End If
        If chkboxDelDisc.Checked Then
            lstItems.Add(34)
        End If

        'expense
        If chkboxAddList.Checked Then
            lstItems.Add(35)
        End If
        If chkboxModList.Checked Then
            lstItems.Add(36)
        End If
        If chkboxDelList.Checked Then
            lstItems.Add(37)
        End If
        If chkboxReadList.Checked Then
            lstItems.Add(38)
        End If
        If chkboxPList.Checked Then
            lstItems.Add(39)
        End If
        If chkboxSendList.Checked Then
            lstItems.Add(40)
        End If
        If chkboxMkList.Checked Then
            lstItems.Add(41)
        End If

        'employee
        If chkboxAddEmp.Checked Then
            lstItems.Add(42)
        End If
        If chkboxReademp.Checked Then
            lstItems.Add(43)
        End If
        If chkboxModEmp.Checked Then
            lstItems.Add(44)
        End If
        If chkboxStopAcc.Checked Then
            lstItems.Add(45)
        End If
        If chkboxReadRec.Checked Then
            lstItems.Add(46)
        End If
        If chkboxModRec.Checked Then
            lstItems.Add(47)
        End If
        If chkboxDelRec.Checked Then
            lstItems.Add(48)
        End If
        If chkboxPRec.Checked Then
            lstItems.Add(49)
        End If
        If chkboxSendRec.Checked Then
            lstItems.Add(50)
        End If
        If chkboxMyPerf.Checked Then
            lstItems.Add(51)
        End If
        If chkboxAllPerf.Checked Then
            lstItems.Add(52)
        End If
        If chkboxModWork.Checked Then
            lstItems.Add(53)
        End If
        If chkboxDelAble.Checked Then
            lstItems.Add(84)
        End If


        'others
        If CheckBox19.Checked Then
            lstItems.Add(54)
        End If
        If CheckBox18.Checked Then
            lstItems.Add(55)
        End If
        If CheckBox17.Checked Then
            lstItems.Add(56)
        End If
        If CheckBox16.Checked Then
            lstItems.Add(57)
        End If
        If CheckBox15.Checked Then
            lstItems.Add(58)
        End If
        If CheckBox14.Checked Then
            lstItems.Add(59)
        End If
        If CheckBox34.Checked Then
            lstItems.Add(60)
        End If
        If CheckBox33.Checked Then
            lstItems.Add(61)
        End If

        If CheckBox13.Checked Then
            lstItems.Add(62)
        End If
        If CheckBox12.Checked Then
            lstItems.Add(63)
        End If
        If CheckBox11.Checked Then
            lstItems.Add(64)
        End If
        If CheckBox10.Checked Then
            lstItems.Add(65)
        End If
        If CheckBox9.Checked Then
            lstItems.Add(66)
        End If
        If CheckBox8.Checked Then
            lstItems.Add(67)
        End If
        If CheckBox7.Checked Then
            lstItems.Add(68)
        End If


        'basic
        If CheckBox32.Checked Then
            lstItems.Add(69)
        End If
        If CheckBox31.Checked Then
            lstItems.Add(70)
        End If
        If CheckBox30.Checked Then
            lstItems.Add(71)
        End If
        If CheckBox29.Checked Then
            lstItems.Add(72)
        End If
        If CheckBox28.Checked Then
            lstItems.Add(73)
        End If
        If CheckBox27.Checked Then
            lstItems.Add(74)
        End If
        If CheckBox26.Checked Then
            lstItems.Add(75)
        End If
        If CheckBox25.Checked Then
            lstItems.Add(76)
        End If
        If CheckBox24.Checked Then
            lstItems.Add(77)
        End If
        If CheckBox23.Checked Then
            lstItems.Add(78)
        End If
        If CheckBox21.Checked Then
            lstItems.Add(79)
        End If
        If CheckBox22.Checked Then
            lstItems.Add(80)
        End If
        If CheckBox20.Checked Then
            lstItems.Add(81)
        End If
        If CheckBox2.Checked Then
            lstItems.Add(82)
        End If

        If radbutManageAllClass.Checked Then
            objCsol.UpdateUsrAuthority(tboxID.Text, lstItems)
        Else
            objCsol.UpdateUsrAuthority(tboxID.Text, lstItems, lstClassID, lstSubClassID)
        End If

    End Sub

    Private Sub butModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butModify.Click
        If lstboxClass.SelectedIndex > -1 Then

            Dim j As Integer = lstboxClass.SelectedIndex
            Dim k As Integer = lstClassShown(lstboxClass.SelectedIndex)
            Dim i As Integer = lstClassShown(j)
            Dim lst As New ArrayList
            If lstScClassID.IndexOf(i) > -1 Then
                For index As Integer = 0 To lstScClassID.Count - 1
                    If lstScClassID(index) = i Then
                        lst.Add(lstSubClassID(index))
                    End If
                Next
            End If
            Dim frm As New frm2ModifyClassAuth(i, lst)
            frm.ShowDialog()

            If frmMain.GetOkState Then
                j = frmMain.GetCurrentValue
                i = lstClassID.IndexOf(j)
                If j = -1 Then 'all sub
                    If i = -1 Then
                        lstClassID.Add(j)
                        Dim lst1 As New ArrayList
                        Dim lst2 As New ArrayList
                        For index As Integer = 0 To lstScClassID.Count - 1
                            If lstScClassID(index) <> j Then
                                lst1.Add(lstScClassID(index))
                                lst2.Add(lstSubClassID(index))
                            End If
                        Next

                        lstScClassID = lst1
                        lstSubClassID = lst2
                    End If
                Else 'partial subs
                    lst = frmMain.GetCurrentList
                    Dim lst1 As New ArrayList
                    Dim lst2 As New ArrayList
                    For index As Integer = 0 To lstScClassID.Count - 1
                        If lstScClassID(index) <> j Then
                            lst1.Add(lstScClassID(index))
                            lst2.Add(lstSubClassID(index))
                        End If
                    Next
                    lstScClassID = lst1
                    lstSubClassID = lst2
                    For index As Integer = 0 To lst.Count - 1
                        lstSubClassID.Add(lst(index))
                        lstScClassID.Add(k)
                    Next
                    If i > -1 Then
                        lstClassID.RemoveAt(i)
                    End If
                End If
                frmMain.SetOkState(False)
            End If
        End If
    End Sub

    Private Sub butPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrevious.Click
        GoPrevious()
    End Sub

    Private Sub butPrevious2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPrevious2.Click
        GoPrevious()
    End Sub

    Private Sub GoPrevious()
        Dim i As Integer = -1
        Dim s As String = ""
        If lstUsr.Count > 0 Then
            For index As Integer = 0 To lstUsr.Count - 1
                If strId = lstUsr(index).ToString.Trim Then
                    i = index
                    Exit For
                End If
            Next
        End If

        If i = 0 Then
            ShowMsg(My.Resources.msgNoPrevious)
        Else
            i = i - 1
            s = lstUsr(i).ToString.Trim
            strId = s
            blNew = False
            dt = objCsol.GetUsrByID(strId)
            dsAuth = objCsol.ListUsrAllAuthority(strId)
            blNewAuth = False
            RefreshInfo()
            RefreshAuth()
            RefreshClassList()
        End If
    End Sub

    Private Sub GoNext()
        Dim i As Integer = -1
        Dim s As String = ""
        If lstUsr.Count > 0 Then
            For index As Integer = 0 To lstUsr.Count - 1
                If strId = lstUsr(index).ToString.Trim Then
                    i = index
                    Exit For
                End If
            Next
        End If

        If i = lstUsr.Count - 1 Then
            ShowMsg(My.Resources.msgNoNext)
        Else
            i = i + 1
            s = lstUsr(i).ToString.Trim
            strId = s
            blNew = False
            dt = objCsol.GetUsrByID(strId)
            dsAuth = objCsol.ListUsrAllAuthority(strId)
            blNewAuth = False
            RefreshInfo()
            RefreshAuth()
            RefreshClassList()
        End If
    End Sub

    Private Sub butPrevious3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butPrevious3.Click
        GoPrevious()
    End Sub

    Private Sub butNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butNext.Click
        GoNext()
    End Sub

    Private Sub butNext2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butNext2.Click
        GoNext()
    End Sub

    Private Sub butNext3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butNext3.Click
        GoNext()
    End Sub

    Private Sub butSelectAll1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll1.Click
        chkboxAbsent.Checked = True
        chkBoxCome.Checked = True
        chkboxStuInfo.Checked = True
        chkboxSeat.Checked = True
        chkboxAnalysis.Checked = True
        chkboxBook.Checked = True
        chkboxRpt.Checked = True
        chkboxPay.Checked = True
        chkboxPayRec.Checked = True
        chkboxOwe.Checked = True
    End Sub

    Private Sub butSelectAll2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll2.Click
        chkboxSendAbsent.Checked = True
        chkBoxSendCome.Checked = True
        chkboxSendStuInfo.Checked = True
        chkboxSendSeat.Checked = True
        chkboxSendAnalysis.Checked = True
        chkboxSendBook.Checked = True
        chkboxSendPay.Checked = True
        chkboxSendPayRec.Checked = True
        chkboxSendOwe.Checked = True
    End Sub

    Private Sub butSelectAll3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll3.Click
        chkboxMkRpt.Checked = True
        chkboxChkRec.Checked = True
        ckhboxAddFee.Checked = True
        chkboxModRpt.Checked = True
        chkboxDiscMod.Checked = True
        chkboxDelRpt.Checked = True
        chkboxAddOwe.Checked = True
        chkboxKpReturn.Checked = True
        chkboxModReturn.Checked = True
        chkoxPaybyClass.Checked = True
        chkboxPaybyDate.Checked = True
        chkboxClassPay.Checked = True
        chkboxSetDisc.Checked = True
        chkboxUnitPay.Checked = True
        chkboxAddDisc.Checked = True
        chkboxModDisc.Checked = True
        chkboxDelDisc.Checked = True
    End Sub

    Private Sub butSelectAll4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll4.Click
        chkboxAddList.Checked = True
        chkboxModList.Checked = True
        chkboxDelList.Checked = True
        chkboxReadList.Checked = True
        chkboxPList.Checked = True
        chkboxSendList.Checked = True
        chkboxMkList.Checked = True
    End Sub

    Private Sub butSelectAll5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectAll5.Click
        chkboxAddEmp.Checked = True
        chkboxReademp.Checked = True
        chkboxModEmp.Checked = True
        chkboxStopAcc.Checked = True
        chkboxReadRec.Checked = True
        chkboxModRec.Checked = True
        chkboxDelRec.Checked = True
        chkboxPRec.Checked = True
        chkboxSendRec.Checked = True
        chkboxMyPerf.Checked = True
        chkboxAllPerf.Checked = True
        chkboxModWork.Checked = True
    End Sub

    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click
        CheckBox19.Checked = True
        CheckBox18.Checked = True
        CheckBox17.Checked = True
        CheckBox16.Checked = True
        CheckBox15.Checked = True
        CheckBox14.Checked = True
        CheckBox34.Checked = True
        CheckBox33.Checked = True
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        CheckBox13.Checked = True
        CheckBox12.Checked = True
        CheckBox11.Checked = True
        CheckBox10.Checked = True
        CheckBox9.Checked = True
        CheckBox8.Checked = True
        CheckBox7.Checked = True
    End Sub

    Private Sub Button9_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button9.Click
        CheckBox32.Checked = True
        CheckBox31.Checked = True
        CheckBox30.Checked = True
        CheckBox29.Checked = True
        CheckBox28.Checked = True
        CheckBox27.Checked = True
        CheckBox26.Checked = True
        CheckBox25.Checked = True
        CheckBox24.Checked = True
        CheckBox23.Checked = True
        CheckBox21.Checked = True
        CheckBox22.Checked = True
        CheckBox20.Checked = True
        CheckBox2.Checked = True
    End Sub

    Private Sub Button8_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button8.Click
        CheckBox32.Checked = False
        CheckBox31.Checked = False
        CheckBox30.Checked = False
        CheckBox29.Checked = False
        CheckBox28.Checked = False
        CheckBox27.Checked = False
        CheckBox26.Checked = False
        CheckBox25.Checked = False
        CheckBox24.Checked = False
        CheckBox23.Checked = False
        CheckBox21.Checked = False
        CheckBox22.Checked = False
        CheckBox20.Checked = False
        CheckBox2.Checked = False
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click

        CheckBox13.Checked = False
        CheckBox12.Checked = False
        CheckBox11.Checked = False
        CheckBox10.Checked = False
        CheckBox9.Checked = False
        CheckBox8.Checked = False
        CheckBox7.Checked = False
    End Sub

    Private Sub Button6_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button6.Click
        CheckBox19.Checked = False
        CheckBox18.Checked = False
        CheckBox17.Checked = False
        CheckBox16.Checked = False
        CheckBox15.Checked = False
        CheckBox14.Checked = False
        CheckBox34.Checked = False
        CheckBox33.Checked = False
    End Sub

    Private Sub butSelectNone5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone5.Click
        chkboxAddEmp.Checked = False
        chkboxReademp.Checked = False
        chkboxModEmp.Checked = False
        chkboxStopAcc.Checked = False
        chkboxReadRec.Checked = False
        chkboxModRec.Checked = False
        chkboxDelRec.Checked = False
        chkboxPRec.Checked = False
        chkboxSendRec.Checked = False
        chkboxMyPerf.Checked = False
        chkboxAllPerf.Checked = False
        chkboxModWork.Checked = False
    End Sub

    Private Sub butSelectNone4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone4.Click
        chkboxAddList.Checked = False
        chkboxModList.Checked = False
        chkboxDelList.Checked = False
        chkboxReadList.Checked = False
        chkboxPList.Checked = False
        chkboxSendList.Checked = False
        chkboxMkList.Checked = False
    End Sub

    Private Sub butSelectNone3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone3.Click
        chkboxMkRpt.Checked = False
        chkboxChkRec.Checked = False
        ckhboxAddFee.Checked = False
        chkboxModRpt.Checked = False
        chkboxDiscMod.Checked = False
        chkboxDelRpt.Checked = False
        chkboxAddOwe.Checked = False
        chkboxKpReturn.Checked = False
        chkboxModReturn.Checked = False
        chkoxPaybyClass.Checked = False
        chkboxPaybyDate.Checked = False
        chkboxClassPay.Checked = False
        chkboxSetDisc.Checked = False
        chkboxUnitPay.Checked = False
        chkboxAddDisc.Checked = False
        chkboxModDisc.Checked = False
        chkboxDelDisc.Checked = False
    End Sub

    Private Sub butSelectNone2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone2.Click
        chkboxSendAbsent.Checked = False
        chkBoxSendCome.Checked = False
        chkboxSendStuInfo.Checked = False
        chkboxSendSeat.Checked = False
        chkboxSendAnalysis.Checked = False
        chkboxSendBook.Checked = False
        chkboxSendPay.Checked = False
        chkboxSendPayRec.Checked = False
        chkboxSendOwe.Checked = False
    End Sub

    Private Sub butSelectNone1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelectNone1.Click
        chkboxAbsent.Checked = False
        chkBoxCome.Checked = False
        chkboxStuInfo.Checked = False
        chkboxSeat.Checked = False
        chkboxAnalysis.Checked = False
        chkboxBook.Checked = False
        chkboxRpt.Checked = False
        chkboxPay.Checked = False
        chkboxPayRec.Checked = False
        chkboxOwe.Checked = False
    End Sub

    Private Sub lnklabCam_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnklabCam.LinkClicked
        If strId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                picbox.Image.Dispose()
                picbox.Image = Nothing
            End If
            Dim frm As New frm2Photo(strId)
            frm.ShowDialog()

            Dim f As String = GetPhotoPath() & strId & ".bmp"
            If CheckFileExist(f) Then
                Dim bm As New Bitmap(f)
                picbox.Image = bm
                picbox.SizeMode = PictureBoxSizeMode.AutoSize
                lnklabSave.Enabled = False
            End If
        End If
    End Sub


    Private Sub lnklabSave_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnklabSave.LinkClicked

        If strId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                Dim bm_source As New Bitmap(picbox.Image)
                Dim bm_dest As New Bitmap( _
                CInt(bm_source.Width * 0.25), _
                CInt(bm_source.Height * 0.25))

                ' Make a Graphics object for the result Bitmap.
                Dim gr_dest As Graphics = Graphics.FromImage(bm_dest)

                ' Copy the source image into the destination bitmap.
                gr_dest.DrawImage(bm_source, 0, 0, _
                    bm_dest.Width + 1, _
                    bm_dest.Height + 1)
                Dim f As String = GetPhotoPath() & strId & ".bmp"
                If CheckFileExist(f) Then
                    Try
                        My.Computer.FileSystem.DeleteFile(f)

                    Catch ex As Exception
                        MessageBox.Show(My.Resources.msgPhotoAlreadySaved, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    End Try
                End If

                Try
                    bm_dest.Save(GetPhotoPath() & strId & ".bmp", Imaging.ImageFormat.Bmp)

                    MessageBox.Show(My.Resources.msgSaveComplete, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

                    lnklabSave.Enabled = False
                Catch ex As Exception
                    MessageBox.Show(My.Resources.msgPhotoAlreadySaved, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                End Try

            End If
        End If
    End Sub

    Private Sub lnklabImport_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnklabImport.LinkClicked
        Dim path As String = ""
        Dim openFileDialog1 As New OpenFileDialog()

        openFileDialog1.InitialDirectory = "c:\"
        openFileDialog1.Filter = "image files (*.bmp)|*.bmp|All files (*.*)|*.*"
        openFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                path = openFileDialog1.FileName
                Dim bm As New Bitmap(path)
                picbox.Image = bm
                picbox.SizeMode = PictureBoxSizeMode.AutoSize
                lnklabSave.Enabled = True
                MessageBox.Show(My.Resources.msgImportPhotoSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

            Catch Ex As Exception
            End Try
        End If

    End Sub

    Private Sub lnklabDel_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles lnklabDel.LinkClicked
        If strId.Length = 8 Then
            If Not picbox.Image Is Nothing Then
                picbox.Image.Dispose()
                picbox.Image = Nothing

                Dim f As String = GetPhotoPath() & strId & ".bmp"
                If CheckFileExist(f) Then
                    Try
                        My.Computer.FileSystem.DeleteFile(f)
                        MessageBox.Show(My.Resources.msgDeleteSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

                    Catch ex As Exception
                        MessageBox.Show(My.Resources.msgDeleteFail, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
                    End Try
                End If
            End If
        End If
    End Sub

    Private Sub ShowMsg(ByVal m As String)
        MsgBox(m, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Sub chkBoxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkBoxShowPast.CheckedChanged
        RefreshClassList()
    End Sub


End Class