﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuGrade
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxPaper = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.butNewPaper = New System.Windows.Forms.Button
        Me.radbutChineseName = New System.Windows.Forms.RadioButton
        Me.radbutEngName = New System.Windows.Forms.RadioButton
        Me.cboxSchool = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tboxIDRemarks = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.tboxIDMark = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.tboxIDName = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tboxSeatRemarks = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tboxSeatMark = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tboxSeatNum = New System.Windows.Forms.TextBox
        Me.tboxSeatName = New System.Windows.Forms.TextBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label12 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.dtPickInput = New System.Windows.Forms.DateTimePicker
        Me.radbutSelect = New System.Windows.Forms.RadioButton
        Me.radbutNow = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.tboxNameRemarks = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.tboxNameMark = New System.Windows.Forms.TextBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.tboxNameID = New System.Windows.Forms.TextBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.Size = New System.Drawing.Size(68, 20)
        Me.mnuNew.Text = "新增試卷"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuImport, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 108
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'mnuImport
        '
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.Size = New System.Drawing.Size(68, 20)
        Me.mnuImport.Text = "匯入成績"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(14, 40)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 109
        Me.Label1.Text = "班級: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(311, 40)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 111
        Me.Label2.Text = "班別: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(636, 40)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 113
        Me.Label3.Text = "試卷: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Location = New System.Drawing.Point(347, 36)
        Me.cboxSubClass.Name = "cboxSubClass"
        Me.cboxSubClass.Size = New System.Drawing.Size(283, 20)
        Me.cboxSubClass.TabIndex = 112
        '
        'cboxPaper
        '
        Me.cboxPaper.FormattingEnabled = True
        Me.cboxPaper.Location = New System.Drawing.Point(673, 36)
        Me.cboxPaper.Name = "cboxPaper"
        Me.cboxPaper.Size = New System.Drawing.Size(206, 20)
        Me.cboxPaper.TabIndex = 114
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(51, 36)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(254, 20)
        Me.cboxClass.TabIndex = 110
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(51, 64)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 121
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'butNewPaper
        '
        Me.butNewPaper.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butNewPaper.Location = New System.Drawing.Point(901, 34)
        Me.butNewPaper.Name = "butNewPaper"
        Me.butNewPaper.Size = New System.Drawing.Size(107, 25)
        Me.butNewPaper.TabIndex = 122
        Me.butNewPaper.Text = "新增試卷"
        Me.butNewPaper.UseVisualStyleBackColor = True
        '
        'radbutChineseName
        '
        Me.radbutChineseName.AutoSize = True
        Me.radbutChineseName.Checked = True
        Me.radbutChineseName.Location = New System.Drawing.Point(159, 64)
        Me.radbutChineseName.Name = "radbutChineseName"
        Me.radbutChineseName.Size = New System.Drawing.Size(95, 16)
        Me.radbutChineseName.TabIndex = 123
        Me.radbutChineseName.TabStop = True
        Me.radbutChineseName.Text = "顯示中文姓名"
        Me.radbutChineseName.UseVisualStyleBackColor = True
        '
        'radbutEngName
        '
        Me.radbutEngName.AutoSize = True
        Me.radbutEngName.Location = New System.Drawing.Point(266, 64)
        Me.radbutEngName.Name = "radbutEngName"
        Me.radbutEngName.Size = New System.Drawing.Size(95, 16)
        Me.radbutEngName.TabIndex = 124
        Me.radbutEngName.Text = "顯示英文姓名"
        Me.radbutEngName.UseVisualStyleBackColor = True
        '
        'cboxSchool
        '
        Me.cboxSchool.FormattingEnabled = True
        Me.cboxSchool.Location = New System.Drawing.Point(673, 62)
        Me.cboxSchool.Name = "cboxSchool"
        Me.cboxSchool.Size = New System.Drawing.Size(206, 20)
        Me.cboxSchool.TabIndex = 126
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(638, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(35, 12)
        Me.Label4.TabIndex = 125
        Me.Label4.Text = "學校: "
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tboxIDRemarks)
        Me.GroupBox1.Controls.Add(Me.Label8)
        Me.GroupBox1.Controls.Add(Me.tboxIDMark)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.tboxID)
        Me.GroupBox1.Controls.Add(Me.tboxIDName)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Location = New System.Drawing.Point(730, 88)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(278, 108)
        Me.GroupBox1.TabIndex = 127
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "使用學號"
        '
        'tboxIDRemarks
        '
        Me.tboxIDRemarks.Location = New System.Drawing.Point(51, 74)
        Me.tboxIDRemarks.Name = "tboxIDRemarks"
        Me.tboxIDRemarks.Size = New System.Drawing.Size(213, 22)
        Me.tboxIDRemarks.TabIndex = 10
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(10, 82)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(35, 12)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "備註: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxIDMark
        '
        Me.tboxIDMark.Location = New System.Drawing.Point(51, 46)
        Me.tboxIDMark.Name = "tboxIDMark"
        Me.tboxIDMark.Size = New System.Drawing.Size(80, 22)
        Me.tboxIDMark.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(10, 54)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 12)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "分數: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxID
        '
        Me.tboxID.Location = New System.Drawing.Point(50, 18)
        Me.tboxID.Name = "tboxID"
        Me.tboxID.Size = New System.Drawing.Size(80, 22)
        Me.tboxID.TabIndex = 6
        '
        'tboxIDName
        '
        Me.tboxIDName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxIDName.Location = New System.Drawing.Point(184, 19)
        Me.tboxIDName.Name = "tboxIDName"
        Me.tboxIDName.ReadOnly = True
        Me.tboxIDName.Size = New System.Drawing.Size(80, 22)
        Me.tboxIDName.TabIndex = 4
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(143, 22)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 12)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "姓名: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(9, 26)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(35, 12)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "學號: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tboxSeatRemarks)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.tboxSeatMark)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.tboxSeatNum)
        Me.GroupBox2.Controls.Add(Me.tboxSeatName)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Location = New System.Drawing.Point(730, 213)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(278, 108)
        Me.GroupBox2.TabIndex = 128
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "使用座號"
        '
        'tboxSeatRemarks
        '
        Me.tboxSeatRemarks.Location = New System.Drawing.Point(51, 74)
        Me.tboxSeatRemarks.Name = "tboxSeatRemarks"
        Me.tboxSeatRemarks.Size = New System.Drawing.Size(213, 22)
        Me.tboxSeatRemarks.TabIndex = 10
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(10, 82)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 12)
        Me.Label9.TabIndex = 9
        Me.Label9.Text = "備註: "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxSeatMark
        '
        Me.tboxSeatMark.Location = New System.Drawing.Point(51, 46)
        Me.tboxSeatMark.Name = "tboxSeatMark"
        Me.tboxSeatMark.Size = New System.Drawing.Size(80, 22)
        Me.tboxSeatMark.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(10, 54)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 12)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "分數: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxSeatNum
        '
        Me.tboxSeatNum.Location = New System.Drawing.Point(50, 18)
        Me.tboxSeatNum.Name = "tboxSeatNum"
        Me.tboxSeatNum.Size = New System.Drawing.Size(80, 22)
        Me.tboxSeatNum.TabIndex = 6
        '
        'tboxSeatName
        '
        Me.tboxSeatName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxSeatName.Location = New System.Drawing.Point(184, 19)
        Me.tboxSeatName.Name = "tboxSeatName"
        Me.tboxSeatName.ReadOnly = True
        Me.tboxSeatName.Size = New System.Drawing.Size(80, 22)
        Me.tboxSeatName.TabIndex = 4
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(143, 22)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 12)
        Me.Label11.TabIndex = 5
        Me.Label11.Text = "姓名: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(9, 26)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 12)
        Me.Label12.TabIndex = 3
        Me.Label12.Text = "座號: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dtPickInput)
        Me.GroupBox3.Controls.Add(Me.radbutSelect)
        Me.GroupBox3.Controls.Add(Me.radbutNow)
        Me.GroupBox3.Location = New System.Drawing.Point(730, 463)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(278, 108)
        Me.GroupBox3.TabIndex = 129
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "輸入日期"
        '
        'dtPickInput
        '
        Me.dtPickInput.Location = New System.Drawing.Point(29, 69)
        Me.dtPickInput.Name = "dtPickInput"
        Me.dtPickInput.Size = New System.Drawing.Size(149, 22)
        Me.dtPickInput.TabIndex = 2
        '
        'radbutSelect
        '
        Me.radbutSelect.AutoSize = True
        Me.radbutSelect.Location = New System.Drawing.Point(12, 45)
        Me.radbutSelect.Name = "radbutSelect"
        Me.radbutSelect.Size = New System.Drawing.Size(95, 16)
        Me.radbutSelect.TabIndex = 1
        Me.radbutSelect.Text = "使用這個日期"
        Me.radbutSelect.UseVisualStyleBackColor = True
        '
        'radbutNow
        '
        Me.radbutNow.AutoSize = True
        Me.radbutNow.Checked = True
        Me.radbutNow.Location = New System.Drawing.Point(11, 21)
        Me.radbutNow.Name = "radbutNow"
        Me.radbutNow.Size = New System.Drawing.Size(155, 16)
        Me.radbutNow.TabIndex = 0
        Me.radbutNow.TabStop = True
        Me.radbutNow.Text = "使用這台電腦現在的日期"
        Me.radbutNow.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.tboxNameRemarks)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.tboxNameMark)
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.tboxName)
        Me.GroupBox4.Controls.Add(Me.tboxNameID)
        Me.GroupBox4.Controls.Add(Me.Label15)
        Me.GroupBox4.Controls.Add(Me.Label16)
        Me.GroupBox4.Location = New System.Drawing.Point(730, 338)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(278, 108)
        Me.GroupBox4.TabIndex = 130
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "使用姓名"
        '
        'tboxNameRemarks
        '
        Me.tboxNameRemarks.Location = New System.Drawing.Point(51, 74)
        Me.tboxNameRemarks.Name = "tboxNameRemarks"
        Me.tboxNameRemarks.Size = New System.Drawing.Size(213, 22)
        Me.tboxNameRemarks.TabIndex = 10
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(10, 82)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 12)
        Me.Label13.TabIndex = 9
        Me.Label13.Text = "備註: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxNameMark
        '
        Me.tboxNameMark.Location = New System.Drawing.Point(51, 46)
        Me.tboxNameMark.Name = "tboxNameMark"
        Me.tboxNameMark.Size = New System.Drawing.Size(80, 22)
        Me.tboxNameMark.TabIndex = 8
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(10, 54)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 12)
        Me.Label14.TabIndex = 7
        Me.Label14.Text = "分數: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxName
        '
        Me.tboxName.Location = New System.Drawing.Point(50, 18)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.Size = New System.Drawing.Size(80, 22)
        Me.tboxName.TabIndex = 6
        '
        'tboxNameID
        '
        Me.tboxNameID.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxNameID.Location = New System.Drawing.Point(184, 19)
        Me.tboxNameID.Name = "tboxNameID"
        Me.tboxNameID.ReadOnly = True
        Me.tboxNameID.Size = New System.Drawing.Size(80, 22)
        Me.tboxNameID.TabIndex = 4
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(143, 22)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 12)
        Me.Label15.TabIndex = 5
        Me.Label15.Text = "學號: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(9, 26)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 12)
        Me.Label16.TabIndex = 3
        Me.Label16.Text = "姓名: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(16, 88)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.Size = New System.Drawing.Size(692, 510)
        Me.dgv.TabIndex = 131
        '
        'frmStuGrade
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.cboxSchool)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.radbutEngName)
        Me.Controls.Add(Me.radbutChineseName)
        Me.Controls.Add(Me.butNewPaper)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxPaper)
        Me.Controls.Add(Me.cboxClass)
        Me.Name = "frmStuGrade"
        Me.Text = "輸入學生成績"
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxPaper As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents butNewPaper As System.Windows.Forms.Button
    Friend WithEvents radbutChineseName As System.Windows.Forms.RadioButton
    Friend WithEvents radbutEngName As System.Windows.Forms.RadioButton
    Friend WithEvents cboxSchool As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents tboxIDName As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxIDRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tboxIDMark As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxSeatRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tboxSeatMark As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tboxSeatNum As System.Windows.Forms.TextBox
    Friend WithEvents tboxSeatName As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dtPickInput As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutSelect As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNow As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxNameRemarks As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tboxNameMark As System.Windows.Forms.TextBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents tboxNameID As System.Windows.Forms.TextBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
End Class
