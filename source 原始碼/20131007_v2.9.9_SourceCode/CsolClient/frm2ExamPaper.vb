﻿Public Class frm2ExamPaper
    Dim blNew As Boolean = True
    Dim strName As String
    Dim strAbbr As String
    Dim datePaper As Date
    Dim lstPaperSc As New ArrayList
    Dim intClass As Integer = -1
    Dim intContent As Integer = -1
    Private ds As New DataSet
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private lstClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstSubClass As New ArrayList
    Private intId As Integer = 0

    Public Sub New(ByVal id As Integer, ByVal isNew As Boolean, ByVal name As String, ByVal abbr As String, ByVal datetime As Date, ByVal content As Integer, _
                   ByVal lst As ArrayList)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            'CSOL.Logger.LogMessage("frm2ExamPaper New1")
            ds = frmMain.GetClassInfoSet
            'CSOL.Logger.LogMessage("frm2ExamPaper New2")
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)
            dtContent = frmMain.GetContents.DefaultView.ToTable(True, c_IDColumnName, c_ContentColumnName, _
                                                    c_ClassIDColumnName)
            blNew = isNew

            'CSOL.Logger.LogMessage("frm2ExamPaper New3")
            InitList()
            'CSOL.Logger.LogMessage("frm2ExamPaper New4")

            If isNew Then
                Me.Text = My.Resources.addExamPaper
            Else
                Me.Text = My.Resources.modExamPaper
                cboxClass.Enabled = False
                chkboxShowPast.Visible = False
                lstPaperSc = lst
                intClass = 0
                If lstPaperSc.Count > 0 Then
                    For Each row As DataRow In dtSubClass.Rows
                        If row.Item(c_IDColumnName) = lstPaperSc(0) Then
                            intClass = row.Item(c_ClassIDColumnName)
                            Exit For
                        End If
                    Next
                End If

                Dim idx As Integer = 0

                If intClass > 0 Then

                    idx = lstClass.IndexOf(intClass)
                    If idx > -1 Then
                        cboxClass.SelectedIndex = idx
                    End If
                End If

                Dim blFlag As Boolean = True
                For i As Integer = 0 To lstSubClassShown.Count - 1
                    If lstPaperSc.Contains(lstSubClassShown(i)) = False Then
                        blFlag = False
                        Exit For
                    End If
                Next

                If blFlag Then
                    radbutAll.Checked = True
                Else
                    radbutTick.Checked = True
                    For i As Integer = 0 To lstSubClassShown.Count - 1
                        If lstPaperSc.Contains(lstSubClassShown(i)) Then
                            chklstSubClass.SetItemChecked(i, True)
                        End If
                    Next
                End If

                strName = name
                tboxName.Text = name
                strAbbr = abbr
                tboxAbbr.Text = abbr
                datePaper = datetime
                dtpickDate.Value = datetime
                intId = id
                idx = -1
                intContent = content
                idx = lstContent.IndexOf(intContent)
                If idx > -1 Then
                    cboxContent.SelectedIndex = idx
                End If

            End If
            'CSOL.Logger.LogMessage("frm2ExamPaper New5")
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub InitList()
        Try
            cboxClass.Items.Clear()
            lstClass.Clear()
            If chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                        cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            End If

            If cboxClass.Items.Count > 0 Then
                If blNew Then
                    cboxClass.SelectedIndex = 0
                Else
                    cboxClass.SelectedIndex = -1
                End If

            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        Try
            chklstSubClass.Items.Clear()
            lstSubClassShown.Clear()
            cboxContent.Items.Clear()
            lstContent.Clear()

            intClass = lstClass(cboxClass.SelectedIndex)
            For Each row As DataRow In dtSubClass.Rows
                If row.Item(c_ClassIDColumnName) = intClass Then
                    lstSubClassShown.Add(row.Item(c_IDColumnName))
                    chklstSubClass.Items.Add(row.Item(c_NameColumnName), False)
                End If
            Next
            For Each row As DataRow In dtContent.Rows
                If row.Item(c_ClassIDColumnName) = intClass Then
                    lstContent.Add(row.Item(c_IDColumnName))
                    cboxContent.Items.Add(row.Item(c_ContentColumnName))
                End If
            Next

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        Try
            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click1")
            lstPaperSc.Clear()
            If blNew Then
                If cboxClass.SelectedIndex = -1 Then
                    ShowInfo(My.Resources.msgNoClassSelected)
                    Exit Sub
                End If
            End If

            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click2")
            If radbutAll.Checked Then
                lstPaperSc = lstSubClassShown
            Else
                For i As Integer = 0 To chklstSubClass.Items.Count - 1
                    If chklstSubClass.GetItemChecked(i) Then
                        lstPaperSc.Add(lstSubClassShown(i))
                    End If
                Next
            End If

            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click3")
            If lstPaperSc.Count < 0 Then
                ShowInfo(My.Resources.msgNoClassSelected)
                Exit Sub
            End If

            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click4")
            strName = tboxName.Text.Trim
            If strName.Length = 0 Then
                ShowInfo(My.Resources.msgNoName)
                Exit Sub
            End If
            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click5")
            strAbbr = tboxAbbr.Text.Trim
            datePaper = dtpickDate.Value
            If cboxContent.SelectedIndex > -1 Then
                intContent = lstContent(cboxContent.SelectedIndex)
            Else
                intContent = 0
            End If
            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click6")

            If blNew Then
                objCsol.AddPaper(strName, strAbbr, datePaper, intContent, lstPaperSc)
                'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click7")
            Else
                objCsol.UpdatePaper(intId, strName, strAbbr, datePaper, intContent, lstPaperSc)
                'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click8")
            End If

            frmMain.SetOkState(True)
            Me.Close()
            'CSOL.Logger.LogMessage("frm2ExamPaper butConfirm_Click9")
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        Catch ex As Exception
            AddErrorLog(ex.ToString())
            MessageBox.Show(ex.ToString(), "發生錯誤了...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class