﻿Public Class frmStuNoteRec
    Private dtStu As New DataTable
    Private ds As DataSet
    Private dtRec As New DataTable

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmStuNoteRec
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_StuIDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub frmStuNoteRec_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuNoteRec_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        ds = objCsol.GetStuNote(tboxID.Text, tboxName.Text)
        dtStu = ds.Tables(c_StuNoteDataTableName)
        dtRec = ds.Tables(c_StuNoteReceiveDataTableName)
        Dim s As String = ""
        If dtStu.Rows.Count > 0 Then
            If radbutToday.Checked Then
                Dim d As Date = GetDateStart(dtpickDate.Value)
                s = c_DisplayDateColumnName & ">'" & d.ToString & "'"
            End If
        End If
        dtStu.DefaultView.RowFilter = s
        dgv.DataSource = dtStu.DefaultView
        Dim f As String = ""
        If dgv.Rows.Count > 0 Then
            'dgv.CurrentCell = dgv.Rows(0).Cells(c_StuIDColumnName)
            f = c_NoteIDColumnName & "=" & dgv.Rows(0).Cells(c_IDColumnName).Value.ToString
        End If
        dtRec.DefaultView.RowFilter = f
        dgvRec.DataSource = dtRec.DefaultView
        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        'If dgv.Rows.Count > 0 Then
        dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
        dgv.Columns.Item(c_StuIDColumnName).Visible = True
        dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
        dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
        dgv.Columns.Item(c_NameColumnName).Visible = True
        dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.stuName
        dgv.Columns.Item(c_DisplayDateColumnName).DisplayIndex = 2
        dgv.Columns.Item(c_DisplayDateColumnName).Visible = True
        dgv.Columns.Item(c_DisplayDateColumnName).HeaderText = My.Resources.noteDisplayDate
        dgv.Columns.Item(c_ContentColumnName).DisplayIndex = 3
        dgv.Columns.Item(c_ContentColumnName).Visible = True
        dgv.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.content
        dgv.Columns.Item(c_RepeatBfDateColumnName).DisplayIndex = 4
        dgv.Columns.Item(c_RepeatBfDateColumnName).Visible = True
        dgv.Columns.Item(c_RepeatBfDateColumnName).HeaderText = My.Resources.noteDisplayMode
        dgv.Columns.Item(c_FrequencyColumnName).DisplayIndex = 4
        dgv.Columns.Item(c_FrequencyColumnName).Visible = True
        dgv.Columns.Item(c_FrequencyColumnName).HeaderText = My.Resources.noteFrequency

        'End If
        For Each col In dgvRec.Columns
            col.visible = False
        Next
        'If dgvRec.Rows.Count > 0 Then

        dgvRec.Columns.Item(c_DateTimeColumnName).DisplayIndex = 0
        dgvRec.Columns.Item(c_DateTimeColumnName).Visible = True
        dgvRec.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.noteReceiveTime
        dgvRec.Columns.Item(c_WayReceiveColumnName).DisplayIndex = 1
        dgvRec.Columns.Item(c_WayReceiveColumnName).Visible = True
        dgvRec.Columns.Item(c_WayReceiveColumnName).HeaderText = My.Resources.noteReceiveMethod

        'End If
    End Sub


    Private Sub InitSelections()

    End Sub

    Private Sub frmStuNoteRec_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub butFilter_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFilter.Click
        RefreshData()
    End Sub

    
    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        If Not dgv.CurrentCell Is Nothing Then
            If dtRec.Rows.Count > 0 Then
                Dim f As String = ""
                If dgv.Rows.Count > 0 Then
                    dgv.CurrentCell = dgv.Rows(0).Cells(c_IDColumnName)
                    f = c_NoteIDColumnName & "=" & dgv.Rows(0).Cells(c_IDColumnName).Value.ToString
                End If
                dtRec.DefaultView.RowFilter = f
                dgvRec.DataSource = dtRec
            End If
        End If
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        frmMain.SetOkState(False)
        Dim frm As New frm2AddNote
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()
            dgv.Refresh()
            dgvRec.Refresh()
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            objCsol.DeleteStuNote(id)
            RefreshData()
            dgv.Refresh()
            dgvRec.Refresh()
        End If
    End Sub
End Class