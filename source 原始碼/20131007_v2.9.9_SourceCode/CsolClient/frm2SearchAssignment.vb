﻿Public Class frm2SearchAssignment

    Public Sub New()

        InitializeComponent()

        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If Not tboxName.Text = "" Then
            frmMain.SetCurrentString(tboxName.Text)
            frmMain.SetOkState(True)
        End If
        Me.Close()
    End Sub
End Class