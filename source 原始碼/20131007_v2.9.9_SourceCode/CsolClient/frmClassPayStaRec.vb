﻿Public Class frmClassPayStaRec
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dtPaySta As New DataTable
    Private dtDisc As New DataTable
    Private intClass As Integer = 0
    Private intSubClass As Integer = 0
    Private lstClassShown As New ArrayList
    Private lstClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dtDetails As New DataTable
    Private dtBackSta As New DataTable
    Private dtKeepSta As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmClassPayStaRec
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmClassPayStaRec
        InitClassList()
        ShowClassList()
        InitColSelections()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvPay.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvPay.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvPay.Rows.Count

            Dim oRow As DataGridViewRow = dgvPay.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvPay.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvPay.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvPay.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvPay.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvPay.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvPay.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvPay.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvPay.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvPay.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmClassPayStaRec_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmClassPayStaRec_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub RefreshData()
        Try

            If cboxClass.SelectedIndex > -1 Then
                lstClass = New ArrayList
                lstClass.Add(lstClassShown(cboxClass.SelectedIndex))
                If radbutNoDate.Checked Then
                    dtPaySta = objCsol.ListStuOweByClass(lstClass)
                    dtDisc = objCsol.ListStuDiscByClass(lstClass)
                Else
                    dtPaySta = objCsol.ListStuOweByClass(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                    dtDisc = objCsol.ListStuDiscByClass(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                End If

                If dtPaySta.Rows.Count > 0 Then
                    RefreshTable()
                End If
            End If
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshTable()
        Dim intSubClass As Integer
        Dim strStuId As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim intTotal5 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String = ""
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"
        Dim strCompute7 As String = "COUNT(" + c_BackAmountColumnName + ")"
        Dim strCompute8 As String
        Dim strCompute9 As String

        'Try
        If dtPaySta Is Nothing Then
            Exit Sub
        End If
        If dtPaySta.Rows.Count = 0 Then
            Exit Sub
        End If
        dtDetails = New DataTable()
        dtBackSta = New DataTable()
        dtKeepSta = New DataTable()

        dtBackSta = dtPaySta.Clone
        dtBackSta.Merge(dtPaySta)
        dtKeepSta = dtPaySta.Clone
        dtKeepSta.Merge(dtPaySta)

        If radbutOne.Checked Then
            dtDetails = dtPaySta.DefaultView.ToTable(True, c_StuIDColumnName, _
            c_StuNameColumnName, c_SubClassIDColumnName, _
            c_ClassIDColumnName, _
                c_SubClassNameColumnName, c_EngNameColumnName, _
                 c_Tel1ColumnName, c_Tel2ColumnName, c_EmailColumnName, _
                c_SchoolColumnName, c_GraduateFromColumnName, _
            c_DadNameColumnName, c_MumNameColumnName, _
                 c_DadMobileColumnName, c_MumMobileColumnName, _
            c_IntroIDColumnName, c_IntroNameColumnName, _
            c_SchoolGradeColumnName, c_SchoolClassColumnName)      ' c_IDColumnName, c_ReceiptNumColumnName,   , _, c_HandlerIDColumnName

            InitTable()

            For i = 0 To dtDetails.Rows.Count - 1
                intSubClass = dtDetails.Rows(i).Item(c_SubClassIDColumnName)
                strStuId = dtDetails.Rows(i).Item(c_StuIDColumnName)
                intTotal = frmMain.GetClassFee(intSubClass)
                strCompute2 = c_SubClassIDColumnName & "=" & intSubClass.ToString & _
                                " AND " & c_StuIDColumnName & "='" & strStuId & "'"
                If dtPaySta.Compute(strCompute3, strCompute2) > 0 Then
                    If dtPaySta.Compute(strCompute6, strCompute2) Then
                        intTotal2 = dtPaySta.Compute(strCompute5, strCompute2) 'discount
                    Else
                        intTotal2 = 0
                    End If
                    intTotal3 = dtPaySta.Compute(strCompute1, strCompute2) 'pay
                    If dtPaySta.Compute(strCompute7, strCompute2) Then
                        intTotal4 = dtPaySta.Compute(strCompute4, strCompute2) 'back
                    Else
                        intTotal4 = 0
                    End If
                Else
                    intTotal2 = 0
                    intTotal3 = 0
                    intTotal4 = 0
                End If

                If dtDisc.Compute(strCompute6, strCompute2) Then
                    intTotal2 = intTotal2 + dtDisc.Compute(strCompute5, strCompute2) 'discount
                End If

                For j As Integer = 0 To dtPaySta.Rows.Count - 1
                    If dtPaySta.Rows(j).Item(c_StuIDColumnName) = strStuId And _
                        dtPaySta.Rows(j).Item(c_SubClassIDColumnName) = intSubClass And _
                        dtPaySta.Rows(j).Item(c_AmountColumnName) > 0 Then
                        dtDetails.Rows(i).Item(c_DateTimeColumnName) = dtPaySta.Rows(j).Item(c_DateTimeColumnName)
                        'dtDetails.Rows(i).Item(c_DiscountRemarksColumnName) = dtPaySta.Rows(j).Item(c_DiscountRemarksColumnName)
                    End If
                Next
                For k As Integer = 0 To dtDisc.Rows.Count - 1
                    If dtDisc.Rows(k).Item(c_StuIDColumnName) = strStuId And dtDisc.Rows(k).Item(c_SubClassIDColumnName) = intSubClass Then    'And _dtDisc.Rows(k).Item(c_AmountColumnName) > 0
                        dtDetails.Rows(i).Item(c_DiscountRemarksColumnName) = dtDisc.Rows(k).Item(c_RemarksColumnName)
                    End If
                Next
                dtDetails.Rows(i).Item(c_AmountColumnName) = intTotal3 - intTotal4
                dtDetails.Rows(i).Item(c_FeeOweColumnName) = intTotal - intTotal2 - _
                                intTotal3 + intTotal4
                dtDetails.Rows(i).Item(c_DiscountColumnName) = intTotal2
            Next

            dgvPay.DataSource = dtDetails.DefaultView
            dgvBack.DataSource = dtBackSta.DefaultView
            dgvKeep.DataSource = dtKeepSta.DefaultView

            dgvPay.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvPay.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvPay.Columns(c_FeeOweColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvBack.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvBack.Columns(c_BackAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvKeep.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvKeep.Columns(c_KeepAmountColumnName).DefaultCellStyle.Format = "$#,##0"
        Else
            dtDetails = dtPaySta.DefaultView.ToTable(True, c_StuIDColumnName, _
                c_StuNameColumnName, c_SubClassIDColumnName, _
                c_ClassIDColumnName, c_AmountColumnName, c_DateTimeColumnName, _
                c_DiscountColumnName, c_DiscountRemarksColumnName, c_SeatNumColumnName)
            For i As Integer = 0 To dtDetails.Rows.Count - 1
                intSubClass = dtDetails.Rows(i).Item(c_SubClassIDColumnName)
                strStuId = dtDetails.Rows(i).Item(c_StuIDColumnName)
                For j As Integer = 0 To dtDisc.Rows.Count - 1
                    If dtDisc.Rows(j).Item(c_StuIDColumnName) = strStuId And dtDisc.Rows(j).Item(c_SubClassIDColumnName) = intSubClass Then
                        dtDetails.Rows(i).Item(c_DiscountColumnName) = dtDisc.Rows(j).Item(c_DiscountColumnName)
                        dtDetails.Rows(i).Item(c_DiscountRemarksColumnName) = dtDisc.Rows(j).Item(c_RemarksColumnName)
                    End If
                Next
            Next
          

            dgvPay.DataSource = dtDetails.DefaultView
            dgvBack.DataSource = dtBackSta.DefaultView
            dgvKeep.DataSource = dtKeepSta.DefaultView

            dgvPay.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvPay.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvBack.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvBack.Columns(c_BackAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvKeep.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvKeep.Columns(c_KeepAmountColumnName).DefaultCellStyle.Format = "$#,##0"
        End If

        strCompute8 = " AND " & c_BackAmountColumnName & ">0"
        strCompute9 = " AND " & c_KeepAmountColumnName & ">0"
        strCompute5 = "SUM(" + c_KeepAmountColumnName + ")"
        strCompute6 = "COUNT(" + c_BackAmountColumnName + ")"

        If cboxSubClass.SelectedIndex = 0 And cboxClass.SelectedIndex > -1 Then
            strCompute2 = c_ClassIDColumnName & _
                "=" & lstClassShown(cboxClass.SelectedIndex).ToString
            dtDetails.DefaultView.RowFilter = strCompute2
            dtBackSta.DefaultView.RowFilter = strCompute2 & strCompute8
            dtKeepSta.DefaultView.RowFilter = strCompute2 & strCompute9
        Else
            If cboxSubClass.SelectedIndex > 0 Then
                strCompute2 = c_SubClassIDColumnName & _
                    "=" & lstSubClassShown(cboxSubClass.SelectedIndex).ToString
                dtDetails.DefaultView.RowFilter = strCompute2
                dtBackSta.DefaultView.RowFilter = strCompute2 & strCompute8
                dtKeepSta.DefaultView.RowFilter = strCompute2 & strCompute9
            End If
        End If

        If cboxSubClass.SelectedIndex > -1 Then
            If dtPaySta.Compute(strCompute3, strCompute2) > 0 Then
                If dtPaySta.Compute(strCompute6, strCompute2 & strCompute9) Then
                    intTotal2 = dtPaySta.Compute(strCompute5, strCompute2 & strCompute9) 'keep
                Else
                    intTotal2 = 0
                End If
                If dtPaySta.Compute(strCompute3, strCompute2 & strCompute9) Then
                    intTotal = dtPaySta.Compute(strCompute1, strCompute2 & strCompute9) 'keep pay
                Else
                    intTotal = 0
                End If
                intTotal3 = dtPaySta.Compute(strCompute3, strCompute2 & " AND " & _
                                             c_KeepAmountColumnName & "=0 AND " & _
                                             c_BackAmountColumnName & "=0") 'pay
                If intTotal3 > 0 Then
                    intTotal3 = dtPaySta.Compute(strCompute1, strCompute2 & " AND " & _
                                             c_KeepAmountColumnName & "=0 AND " & _
                                             c_BackAmountColumnName & "=0") 'pay
                End If
                If dtPaySta.Compute(strCompute7, strCompute2 & strCompute8) Then
                    intTotal4 = dtPaySta.Compute(strCompute4, strCompute2 & strCompute8) 'back
                Else
                    intTotal4 = 0
                End If

                If dtPaySta.Compute(strCompute3, strCompute2 & strCompute8) Then
                    intTotal5 = dtPaySta.Compute(strCompute1, strCompute2 & strCompute8) 'back pay
                Else
                    intTotal5 = 0
                End If
            Else
                intTotal = 0
                intTotal2 = 0
                intTotal3 = 0
                intTotal4 = 0
                intTotal5 = 0
            End If
        End If

        tboxPayAmount.Text = Format(intTotal3, "$#,##0")
        tboxBackPayAmount.Text = Format(intTotal5, "$#,##0")
        tboxBackAmount.Text = Format(intTotal4, "$#,##0")
        tboxKeepPayAmount.Text = Format(intTotal, "$#,##0")
        tboxKeepAmount.Text = Format(intTotal2, "$#,##0")
        tboxTotal.Text = Format((intTotal3 + intTotal5 + intTotal - intTotal4), "$#,##0")
        LoadColumnText()
        'Catch ex As ApplicationException
        'MessageBox.Show(ex.Message)
        'End Try
    End Sub

    'Private Function GetClassFee(ByVal intId As Integer) As Integer
    '    GetClassFee = 0
    '    For index As Integer = 0 To dtSubClassList.Rows.Count - 1
    '        If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intId Then
    '            GetClassFee = dtSubClassList.Rows(index).Item(c_FeeColumnName)
    '            Exit For
    '        End If
    '    Next

    'End Function

    Private Sub InitClassList()
        Try
            dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                    c_NameColumnName, c_EndColumnName)
            dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        cboxClass.Items.Clear()
        cboxSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtDetails.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DateTimeColumnName, GetType(System.DateTime))
        dtDetails.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DiscountRemarksColumnName, GetType(System.String))
        dtDetails.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvPay.Columns
            col.visible = False
        Next
        If dgvPay.Rows.Count > 0 Then
            dgvPay.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvPay.Columns.Item(c_StuIDColumnName).Visible = True
            dgvPay.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvPay.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvPay.Columns.Item(c_StuNameColumnName).Visible = True
            dgvPay.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvPay.Columns.Item(c_AmountColumnName).DisplayIndex = 2
            dgvPay.Columns.Item(c_AmountColumnName).Visible = True
            dgvPay.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.paidTotal
            dgvPay.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
            dgvPay.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvPay.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.firstPaidDate
            dgvPay.Columns.Item(c_DiscountColumnName).DisplayIndex = 4
            dgvPay.Columns.Item(c_DiscountColumnName).Visible = True
            dgvPay.Columns.Item(c_DiscountColumnName).HeaderText = My.Resources.discount
            dgvPay.Columns.Item(c_DiscountRemarksColumnName).DisplayIndex = 5
            dgvPay.Columns.Item(c_DiscountRemarksColumnName).Visible = True
            dgvPay.Columns.Item(c_DiscountRemarksColumnName).HeaderText = My.Resources.discRemarks
            Dim i As Integer = 7
            If radbutOne.Checked Then
                dgvPay.Columns.Item(c_FeeOweColumnName).DisplayIndex = 6
                dgvPay.Columns.Item(c_FeeOweColumnName).Visible = True
                dgvPay.Columns.Item(c_FeeOweColumnName).HeaderText = My.Resources.feeOwe
            Else
                i = 6
            End If
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgvPay.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgvPay.Columns.Item(lstColName(index)).Visible = True
                    dgvPay.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
        End If



        'If dgvPay.Columns.Item(c_SchoolColumnName).Visible = True Then
        '    Dim id As Integer
        '    Select Case intColSchType
        '        Case 0
        '            For index As Integer = 0 To dgvPay.Rows.Count - 1
        '                id = dgvPay.Rows(index).Cells(c_PrimarySchColumnName).Value
        '                dgvPay.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 1
        '            For index As Integer = 0 To dgvPay.Rows.Count - 1
        '                id = dgvPay.Rows(index).Cells(c_JuniorSchColumnName).Value
        '                dgvPay.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 2
        '            For index As Integer = 0 To dgvPay.Rows.Count - 1
        '                id = dgvPay.Rows(index).Cells(c_HighSchColumnName).Value
        '                dgvPay.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 3
        '            For index As Integer = 0 To dgvPay.Rows.Count - 1
        '                id = dgvPay.Rows(index).Cells(c_UniversityColumnName).Value
        '                dgvPay.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '    End Select
        'End If
        For Each col In dgvBack.Columns
            col.visible = False
        Next
        If dgvBack.Rows.Count > 0 Then

            dgvBack.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvBack.Columns.Item(c_StuIDColumnName).Visible = True
            dgvBack.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvBack.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvBack.Columns.Item(c_StuNameColumnName).Visible = True
            dgvBack.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvBack.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgvBack.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvBack.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvBack.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            dgvBack.Columns.Item(c_AmountColumnName).Visible = True
            dgvBack.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
            dgvBack.Columns.Item(c_BackAmountColumnName).DisplayIndex = 4
            dgvBack.Columns.Item(c_BackAmountColumnName).Visible = True
            dgvBack.Columns.Item(c_BackAmountColumnName).HeaderText = My.Resources.backAmt
            dgvBack.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
            dgvBack.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvBack.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvBack.Columns.Item(c_HandlerColumnName).DisplayIndex = 6
            dgvBack.Columns.Item(c_HandlerColumnName).Visible = True
            dgvBack.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        End If
        For Each col In dgvKeep.Columns
            col.visible = False
        Next
        If dgvKeep.Rows.Count > 0 Then

            dgvKeep.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvKeep.Columns.Item(c_StuIDColumnName).Visible = True
            dgvKeep.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvKeep.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvKeep.Columns.Item(c_StuNameColumnName).Visible = True
            dgvKeep.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvKeep.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgvKeep.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvKeep.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvKeep.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            dgvKeep.Columns.Item(c_AmountColumnName).Visible = True
            dgvKeep.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
            dgvKeep.Columns.Item(c_KeepAmountColumnName).DisplayIndex = 4
            dgvKeep.Columns.Item(c_KeepAmountColumnName).Visible = True
            dgvKeep.Columns.Item(c_KeepAmountColumnName).HeaderText = My.Resources.keepAmt
            dgvKeep.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
            dgvKeep.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvKeep.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvKeep.Columns.Item(c_HandlerColumnName).DisplayIndex = 6
            dgvKeep.Columns.Item(c_HandlerColumnName).Visible = True
            dgvKeep.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        End If
    End Sub

    Private Sub InitColSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click

        If radbutOne.Checked Then
            If dgvPay.Columns.Count > 6 Then
                Dim lst As New ArrayList
                For index As Integer = 7 To dgvPay.Columns.Count - 1
                    If dgvPay.Columns(index).Visible = True Then
                        lst.Add(1)
                    Else
                        lst.Add(0)
                    End If
                Next
                Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
                frm.ShowDialog()
                If frmMain.GetOkState Then
                    lstColShow = frmMain.GetCurrentList
                    lstColTxt = frmMain.GetCurrentList2
                    intColSchType = frmMain.GetCurrentValue
                    LoadColumnText()
                End If
            End If
        Else
            If dgvPay.Columns.Count > 6 Then
                Dim lst As New ArrayList
                For index As Integer = 6 To dgvPay.Columns.Count - 1
                    If dgvPay.Columns(index).Visible = True Then
                        lst.Add(1)
                    Else
                        lst.Add(0)
                    End If
                Next
                Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
                frm.ShowDialog()
                If frmMain.GetOkState Then
                    lstColShow = frmMain.GetCurrentList
                    lstColTxt = frmMain.GetCurrentList2
                    intColSchType = frmMain.GetCurrentValue
                    LoadColumnText()
                End If
            End If
        End If
        
    End Sub
    Private Sub InitClassSelection()
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmClassPayStaRec_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        Dim intC As Integer
        Dim intSc As Integer
        If cboxClass.SelectedIndex = -1 Then
            Exit Sub
        End If
        intC = lstClassShown(cboxClass.SelectedIndex)
        lstSubClassShown.Clear()
        cboxSubClass.Items.Clear()
        cboxSubClass.Text = ""
        lstSubClassShown.Add(-1)
        cboxSubClass.Items.Add(My.Resources.all)

        For index As Integer = 0 To dtSubClassList.Rows.Count - 1
            If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                lstSubClassShown.Add(intSc)
                cboxSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName).trim)
            End If
        Next

        cboxSubClass.SelectedIndex = -1

    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            RefreshTable()
        End If

    End Sub

    Private Sub radbutOne_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutOne.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgvPay)
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(23) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvPay.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim frm As New frm2InputReason
            frm.ShowDialog()
            Dim reason As String = ""
            If frmMain.GetOkState Then
                reason = frmMain.GetCurrentString
            End If
            Dim id As Integer = dgvPay.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim scid As Integer = dgvPay.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim amount As Integer = dgvPay.SelectedRows(0).Cells(c_AmountColumnName).Value
            Dim strStuId As String = dgvPay.SelectedRows(0).Cells(c_StuIDColumnName).Value

            objCsol.DeletePayRec(id, strStuId, scid, amount)
            Dim strRemarks As String = My.Resources.payRecDel & ", " & My.Resources.amount _
                    & "=" & amount.ToString
            Dim receipt As String = dgvPay.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
            Dim handler As String = dgvPay.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
            objCsol.AddPayModRec(receipt, strStuId, _
                                scid, _
                                Now, handler, frmMain.GetUsrName, _
                                strRemarks, reason)
            MessageBox.Show(My.Resources.msgDeleteSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
            dgvPay.DataSource = Nothing
            dgvBack.DataSource = Nothing
            dgvKeep.DataSource = Nothing
            RefreshData()
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgvPay.SelectedRows.Count > 0 Then
            Dim id As String = dgvPay.SelectedRows(0).Cells(c_StuIDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub
End Class