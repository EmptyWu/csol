﻿Public Class frmPayStaClass
    Private dtClassList As New DataTable
    Private dtClassSta As New DataTable
    Private dateNow As Date = Now
    Private dsPaySta As New DataSet
    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private dtClassPaySta As New DataTable
    Private dtClassMKSta As New DataTable
    Private dtClassMBSta As New DataTable
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private lstClassFee As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPayStaClass
    Private sUserName As String = frmMain.GetUsrName

    Private getReturn As Boolean = False

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPayStaClass
        SetDate()
        dtClassList = Nothing
        'InitClassList()
        'ShowClassList()
        ''RefreshData()
        'InitColSelections()
        getReturn = False
        Dim t As New System.Threading.Thread(AddressOf ClassList, 1)
        t.IsBackground = True
        t.Start()

    End Sub

    Private Delegate Sub ClassListDelegate()

    Private Sub ClassList()
        While Not getReturn
            If Me.InvokeRequired Then
                InitClassList()
                InitColSelections()
                Me.Invoke(New ClassListDelegate(AddressOf ClassList))
            Else
                If Not (dtClassList Is Nothing) Then
                    ShowClassList()
                    getReturn = True
                End If
            End If
        End While
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument2.DefaultPageSettings.Landscape = True
            PrintDocument2.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Friend Sub SetDate()
        dtpickFrom.Value = dateNow
        dtpickTo.Value = dateNow
    End Sub

    Private Sub frmPayStaClass_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
        While Not IsDisposed
            Me.Dispose()
        End While
    End Sub

    Private Sub frmPayStaClass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intClass As Integer
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_ReceiptNumColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String
        Dim strCompute6 As String
        Dim strCompute7 As String
        Dim stuOweCount As New ArrayList
        Dim jobtime As DateTime = Now
       

        Try
            dgvDetails.DataSource = Nothing
            dgvMaster.DataSource = Nothing

            If radbutByDate.Checked = True Then
                dsPaySta = objCsol.ListPayStaByClassHelper(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutByDateNo.Checked = True Then
                'dsPaySta = objCsol.ListPayStaByClass(lstClass)
                dsPaySta = objCsol.ListPayStaByClassHelper(lstClass)
            End If
            System.Diagnostics.Debug.WriteLine(String.Format("1{0}{1}", vbTab, (Now - jobtime).TotalMilliseconds))
            jobtime = Now
            dtClassPaySta.Clear()
            dtClassMKSta.Clear()
            dtClassMBSta.Clear()

            dtClassPaySta = dsPaySta.Tables.Item(c_PayStaByDateTableName)
            dtClassMKSta = dsPaySta.Tables.Item(c_MKeepStaByDateTableName)
            dtClassMBSta = dsPaySta.Tables.Item(c_MBackStaByDateTableName)

            If radbutByDate.Checked = True Then
                stuOweCount = getStuOweCount(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutByDateNo.Checked = True Then
                stuOweCount = getStuOweCount()
            End If
           
            Debug.Print("")

            dtMaster = New DataTable()
            dtDetails = New DataTable()
            dtDetails = dtClassMBSta.Clone()
            dtDetails.Merge(dtClassMBSta)
            dtDetails.Merge(dtClassMKSta)
            dtDetails.Merge(dtClassPaySta)
            tbox0.Text = "0"
            tbox1.Text = "0"
            tbox2.Text = "0"
            tbox3.Text = "0"
            tbox4.Text = "0"
            tbox5.Text = "0"
            InitTable()
            dtClassSta.Clear()
            If radbutByDate.Checked = True Then
                'dtClassSta = GetListClassStaByDate(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                dtClassSta = GetListClassStaByDateHelper(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutByDateNo.Checked = True Then
                'dtClassSta = GetListClassStaByNoDate(lstClass)
                dtClassSta = GetListClassStaByNoDateHelper(lstClass)
            End If
            For index As Integer = 0 To dtClassSta.Rows.Count - 1
                intClass = dtClassSta.Rows(index).Item(c_IDColumnName)
                If lstClass.IndexOf(intClass) > -1 Then
                    dtMaster.Rows.Add(dtClassSta.Rows(index).Item(c_IDColumnName), _
                                      dtClassSta.Rows(index).Item(c_NameColumnName), _
                                      dtClassSta.Rows(index).Item(c_StuRegCntColumnName), _
                                      dtClassSta.Rows(index).Item(c_FeeClrCntColumnName), _
                                      0, 0, 0, 0, 0)
                End If
            Next
    
            For i = 0 To dtMaster.Rows.Count - 1
                intClass = dtMaster.Rows(i).Item(c_IDColumnName)
                intTotal = dtMaster.Rows(i).Item(c_StuRegCntColumnName) - _
                    dtMaster.Rows(i).Item(c_FeeClrCntColumnName)
                dtMaster.Rows(i).Item(c_FeeOweCntColumnName) = intTotal
                strCompute2 = c_ClassIDColumnName + "=" + intClass.ToString
                If dtClassMKSta.Rows.Count > 0 Then
                    intTotal = dtClassMKSta.Compute(strCompute3, strCompute2)
                Else
                    intTotal = 0
                End If
                If dtClassMBSta.Rows.Count > 0 Then
                    intTotal = intTotal + dtClassMBSta.Compute(strCompute3, strCompute2)
                End If
                dtMaster.Rows(i).Item(c_DataCntColumnName) = intTotal
                If dtClassMKSta.Rows.Count > 0 Then
                    If dtClassMKSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = dtClassMKSta.Compute(strCompute1, strCompute2)
                    Else
                        intTotal = 0
                    End If
                Else
                    intTotal = 0
                End If
                If dtClassMBSta.Rows.Count > 0 Then
                    If dtClassMBSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = intTotal + dtClassMBSta.Compute(strCompute1, strCompute2)
                    End If
                End If
                If dtClassPaySta.Rows.Count > 0 Then
                    If dtClassPaySta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = intTotal + dtClassPaySta.Compute(strCompute1, strCompute2)
                    End If
                End If
                dtMaster.Rows(i).Item(c_TotalPayColumnName) = intTotal
                If dtClassMBSta.Rows.Count > 0 Then
                    If dtClassMBSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = dtClassMBSta.Compute(strCompute4, strCompute2)
                    Else
                        intTotal = 0
                    End If
                Else
                    intTotal = 0
                End If
                dtMaster.Rows(i).Item(c_TotalBackColumnName) = intTotal
                intTotal2 = dtMaster.Rows(i).Item(c_TotalPayColumnName)
                dtMaster.Rows(i).Item(c_AmountColumnName) = intTotal2 - intTotal
            Next

            strCompute1 = "SUM(" + c_StuRegCntColumnName + ")"
            strCompute2 = "SUM(" + c_FeeClrCntColumnName + ")"
            strCompute3 = "SUM(" + c_FeeOweCntColumnName + ")"
            strCompute4 = "SUM(" + c_DataCntColumnName + ")"
            strCompute5 = "SUM(" + c_TotalPayColumnName + ")"
            strCompute6 = "SUM(" + c_TotalBackColumnName + ")"
            strCompute7 = "SUM(" + c_AmountColumnName + ")"

            If dtMaster.Rows.Count > 0 Then
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, dtMaster.Compute(strCompute1, ""), _
                                    dtMaster.Compute(strCompute2, ""), dtMaster.Compute(strCompute3, ""), _
                                    dtMaster.Compute(strCompute4, ""), dtMaster.Compute(strCompute5, ""), _
                                    dtMaster.Compute(strCompute6, ""), dtMaster.Compute(strCompute7, ""))
            Else
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, 0, 0, 0, 0, 0, 0, 0)
            End If
           
            Dim dtM As DataTable = dtMaster.Clone
            dtM.Columns("name").ColumnName = "班級名稱"
            dtM.Columns("sturegcnt").ColumnName = "報名人數"
            dtM.Columns("feeclrcnt").ColumnName = "繳清人數"
            dtM.Columns("feeowecnt").ColumnName = "尚欠人數"
            dtM.Columns("datacnt").ColumnName = "退保人數"
            dtM.Columns("totalpay").ColumnName = "繳費總額"
            dtM.Columns("totalback").ColumnName = "退保總額"
            dtM.Columns("amount").ColumnName = "總計"
            For Each row As DataRow In dtMaster.Rows
                dtM.Rows.Add(row.ItemArray)
            Next

            dgvMaster.DataSource = dtM
            If dgvMaster.RowCount > 0 Then
                dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells("班級名稱")
            End If
            dgvMaster.Columns.Item(c_IDColumnName).Visible = False
            tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells("總計").Value).ToString

            dgvMaster.Columns("繳費總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("退保總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("總計").DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()

           
            RefreshPayMethodSta(dtClassPaySta, c_AmountColumnName)
            RefreshPayMethodSta(dtClassMKSta, c_AmountColumnName)
            RefreshPayMethodSta(dtClassMBSta, c_AmountColumnName)

            tbox0.Text = Format(CInt(tbox0.Text), "$#,##0")
            tbox1.Text = Format(CInt(tbox1.Text), "$#,##0")
            tbox2.Text = Format(CInt(tbox2.Text), "$#,##0")
            tbox3.Text = Format(CInt(tbox3.Text), "$#,##0")
            tbox4.Text = Format(CInt(tbox4.Text), "$#,##0")
            tbox5.Text = Format(CInt(tbox5.Text), "$#,##0")
            tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            tboxTotal.Text = tbox6.Text

            dgvDetails.Columns("繳費金額").DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns("退費金額").DefaultCellStyle.Format = "$#,##0"

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Shared Function GetPayStaClass(ByVal lstClass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        RequestParams.Add("DateFrom", DateFrom)
        RequestParams.Add("DateTo", DateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetPayStaClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("PayStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("PayStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetPayStaClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetPayStaClassNoDAte", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("PayStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("PayStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetMKStaClass(ByVal lstClass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        RequestParams.Add("DateFrom", DateFrom)
        RequestParams.Add("DateTo", DateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetMKeepStaBySubClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("MKStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("MKStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetMKStaClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetMKeepStaBySubClassNoDAte", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("MKStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("MKStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetMBStaClass(ByVal lstClass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        RequestParams.Add("DateFrom", DateFrom)
        RequestParams.Add("DateTo", DateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetMBackStaBySubClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("MBStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("MBStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetMBStaClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetMBackStaBySubClassNoDAte", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("MBStaClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("MBStaClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListClassStaByDateHelper(ByVal lstClass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        Dim classid As String = String.Join(",", list.ToArray)
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        RequestParams.Add("DateFrom", DateFrom)
        RequestParams.Add("DateTo", DateTo)
        'Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListClassStaByDate", RequestParams)
        Dim ResponseData() As Byte = objCsol.GetListClassStaByDateHelper(DateFrom, DateTo, classid)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListClassStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListClassStaByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function


    Private Shared Function GetListClassStaByDate(ByVal lstClass As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        RequestParams.Add("DateFrom", DateFrom)
        RequestParams.Add("DateTo", DateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListClassStaByDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListClassStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListClassStaByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListClassStaByNoDateHelper(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListClassStaByNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ClassStaByNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListClassStaNoByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListClassStaByNoDate(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListClassStaByNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ClassStaByNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListClassStaNoByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Sub RefreshPayMethodSta(ByRef dt As DataTable, ByVal colName As String)
        Dim strCompute1 = "SUM(" + colName + ")"
        Dim strCompute2 As String
        Dim strCompute3 = "COUNT(" + colName + ")"

        If dt.Rows.Count > 0 Then
            strCompute2 = c_PayMethodColumnName & "=" & c_PayMethodCash.ToString
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox0.Text = (CInt(tbox0.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName & "=" & c_PayMethodCheque.ToString
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox1.Text = (CInt(tbox1.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName & "=" & c_PayMethodTransfer.ToString
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox2.Text = (CInt(tbox2.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName & "=" & c_PayMethodCreditCard.ToString
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox3.Text = (CInt(tbox3.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName & "=" & c_PayMethodKeep.ToString
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox4.Text = (CInt(tbox4.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=6"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox5.Text = (CInt(tbox5.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
        End If
    End Sub

    Private Sub InitClassList()
        Try
            If dtClassList Is Nothing Then
                dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                         c_NameColumnName, c_EndColumnName)
            End If
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        lstClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            If chklstClass.Items.Count > 0 Then
                'chklstClass.SetItemChecked(0, True)
                chklstClass.SelectedItem = chklstClass.Items(0)
            End If
            RefreshClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_IDColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_NameColumnName, GetType(System.String))
        dtMaster.Columns.Add(c_StuRegCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeClrCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeOweCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalBackColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))

    End Sub

    Private Sub LoadColumnText()
        Dim dt As DataTable = dtDetails.Clone
        dt.Columns("Receiptnum").ColumnName = "收據編號"
        dt.Columns("stuid").ColumnName = "學號"
        dt.Columns("stuname").ColumnName = "學生姓名"
        dt.Columns("subclassname").ColumnName = "班別名稱"
        dt.Columns("datetime").ColumnName = "日期時間"
        dt.Columns("paymethod").ColumnName = "繳費方式"
        dt.Columns("amount").ColumnName = "繳費金額"
        dt.Columns("backamount").ColumnName = "退費金額"
        dt.Columns("handler").ColumnName = "承辦人員"
        dt.Columns("engname").ColumnName = "英文姓名"
        dt.Columns("tel1").ColumnName = "電話一"
        dt.Columns("tel2").ColumnName = "電話二"
        dt.Columns("email").ColumnName = "電郵"
        dt.Columns("extra").ColumnName = "額外資訊"
        dt.Columns("school").ColumnName = "學校"
        dt.Columns("dadname").ColumnName = "爸爸姓名"
        dt.Columns("mumname").ColumnName = "媽媽姓名"
        dt.Columns("dadmobile").ColumnName = "爸爸手機"
        dt.Columns("mummobile").ColumnName = "媽媽手機"
        dt.Columns("introid").ColumnName = "介紹人學號"
        dt.Columns("introname").ColumnName = "介紹人姓名"
        dt.Columns("schoolgrade").ColumnName = "學校年級"
        dt.Columns("schoolclass").ColumnName = "學校班級"
        For Each row As DataRow In dtDetails.Rows
            dt.Rows.Add(row.ItemArray)
        Next

        Dim id As Integer
        Select Case intColSchType
            Case 0
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_PrimarySchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 1
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_JuniorSchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 2
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_HighSchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 3
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_UniversityColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
        End Select
      
        dt.Columns.RemoveAt(dt.Columns.IndexOf("classid"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("classname"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("subclassid"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("mobile"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("birthday"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("graduatefrom"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("ic"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("registerdate"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("reason"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("primarysch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("juniorsch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("highsch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("university"))

        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow.Item(index).ToString = 1 Then

            Else
                dt.Columns.RemoveAt(dt.Columns.IndexOf(lstColName.Item(index).ToString))
            End If
        Next
        dgvDetails.DataSource = dt
    End Sub



    Private Sub InitColSelections()
        If lstColName.Count > 0 Then
            lstColName.Clear()
        End If
        lstColName.Add("英文姓名")
        lstColName.Add("電話一")
        lstColName.Add("電話二")
        lstColName.Add("電郵")
        lstColName.Add("學校")
        'lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add("爸爸姓名")
        lstColName.Add("媽媽姓名")
        lstColName.Add("爸爸手機")
        lstColName.Add("媽媽手機")
        lstColName.Add("介紹人學號")
        lstColName.Add("介紹人姓名")
        lstColName.Add("學校年級")
        lstColName.Add("學校班級")
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    'Private Sub InitColSelections()
    '    lstColName.Add(c_EngNameColumnName)
    '    lstColName.Add(c_Tel1ColumnName)
    '    lstColName.Add(c_Tel2ColumnName)
    '    lstColName.Add(c_EmailColumnName)
    '    lstColName.Add(c_SchoolColumnName)
    '    lstColName.Add(c_GraduateFromColumnName)
    '    lstColName.Add(c_DadNameColumnName)
    '    lstColName.Add(c_MumNameColumnName)
    '    lstColName.Add(c_DadMobileColumnName)
    '    lstColName.Add(c_MumMobileColumnName)
    '    lstColName.Add(c_IntroIDColumnName)
    '    lstColName.Add(c_IntroNameColumnName)
    '    lstColName.Add(c_SchoolGradeColumnName)
    '    lstColName.Add(c_SchoolClassColumnName)
    '    FillArrayList(2, lstColName.Count, lstColShow)
    '    FillArrayList(1, lstColName.Count, lstColTxt)
    'End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvDetails.Columns.Count > 9 Then
            Dim lst As New ArrayList
            For index As Integer = 10 To dgvDetails.Columns.Count - 1
                If dgvDetails.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmPayStaClass_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click


        RefreshClassList()
        RefreshData()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intClassId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intClassId = dgvMaster.SelectedRows(0).Cells(c_IDColumnName).Value
            If intClassId > -1 Then
                Dim rows() As DataRow = dtDetails.Select("ClassID =" & intClassId.ToString)
                Dim dt As DataTable = dtDetails.Clone
                dt.Columns("Receiptnum").ColumnName = "收據編號"
                dt.Columns("stuid").ColumnName = "學號"
                dt.Columns("stuname").ColumnName = "學生姓名"
                dt.Columns("subclassname").ColumnName = "班別名稱"
                dt.Columns("datetime").ColumnName = "日期時間"
                dt.Columns("paymethod").ColumnName = "繳費方式"
                dt.Columns("amount").ColumnName = "繳費金額"
                dt.Columns("backamount").ColumnName = "退費金額"
                dt.Columns("handler").ColumnName = "承辦人員"
                dt.Columns("engname").ColumnName = "英文姓名"
                dt.Columns("tel1").ColumnName = "電話一"
                dt.Columns("tel2").ColumnName = "電話二"
                dt.Columns("email").ColumnName = "電郵"
                dt.Columns("extra").ColumnName = "額外資訊"
                dt.Columns("school").ColumnName = "學校"
                dt.Columns("dadname").ColumnName = "爸爸姓名"
                dt.Columns("mumname").ColumnName = "媽媽姓名"
                dt.Columns("dadmobile").ColumnName = "爸爸手機"
                dt.Columns("mummobile").ColumnName = "媽媽手機"
                dt.Columns("introid").ColumnName = "介紹人學號"
                dt.Columns("introname").ColumnName = "介紹人姓名"
                dt.Columns("schoolgrade").ColumnName = "學校年級"

                For Each row As DataRow In rows
                    dt.Rows.Add(row.ItemArray)
                Next


                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_PrimarySchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_JuniorSchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_HighSchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_UniversityColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                End Select
                dt.Columns.RemoveAt(dt.Columns.IndexOf("classid"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("classname"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("subclassid"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("mobile"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("birthday"))
                'dt.Columns.RemoveAt(dt.Columns.IndexOf("schoolclass"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("graduatefrom"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("ic"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("registerdate"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("reason"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("primarysch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("juniorsch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("highsch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("university"))

                dgvDetails.DataSource = dt
                'dtDetails.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString

                'dgvDetails.DataSource = dtDetails.DefaultView
            Else

                Dim dt As DataTable = dtDetails.Clone
                dt.Columns("Receiptnum").ColumnName = "收據編號"
                dt.Columns("stuid").ColumnName = "學號"
                dt.Columns("stuname").ColumnName = "學生姓名"
                dt.Columns("subclassname").ColumnName = "班別名稱"
                dt.Columns("datetime").ColumnName = "日期時間"
                dt.Columns("paymethod").ColumnName = "繳費方式"
                dt.Columns("amount").ColumnName = "繳費金額"
                dt.Columns("backamount").ColumnName = "退費金額"
                dt.Columns("handler").ColumnName = "承辦人員"
                dt.Columns("engname").ColumnName = "英文姓名"
                dt.Columns("tel1").ColumnName = "電話一"
                dt.Columns("tel2").ColumnName = "電話二"
                dt.Columns("email").ColumnName = "電郵"
                dt.Columns("extra").ColumnName = "額外資訊"
                dt.Columns("school").ColumnName = "學校"
                dt.Columns("dadname").ColumnName = "爸爸姓名"
                dt.Columns("mumname").ColumnName = "媽媽姓名"
                dt.Columns("dadmobile").ColumnName = "爸爸手機"
                dt.Columns("mummobile").ColumnName = "媽媽手機"
                dt.Columns("introid").ColumnName = "介紹人學號"
                dt.Columns("introname").ColumnName = "介紹人姓名"
                dt.Columns("schoolgrade").ColumnName = "學校年級"

                For Each row As DataRow In dtDetails.Rows
                    dt.Rows.Add(row.ItemArray)
                Next


                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_PrimarySchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_JuniorSchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_HighSchColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dt.Rows.Count - 1
                            id = GetIntValue(dt.Rows(index).Item(c_UniversityColumnName))
                            dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                        Next
                End Select
                dt.Columns.RemoveAt(dt.Columns.IndexOf("classid"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("classname"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("subclassid"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("mobile"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("birthday"))
                'dt.Columns.RemoveAt(dt.Columns.IndexOf("schoolclass"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("graduatefrom"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("ic"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("registerdate"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("reason"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("primarysch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("juniorsch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("highsch"))
                dt.Columns.RemoveAt(dt.Columns.IndexOf("university"))

                dgvDetails.DataSource = dt
                'dgvDetails.DataSource = dtDetails.DefaultView
            End If
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstClass.Items.Count - 1
            chklstClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstClass.Items.Count - 1
            chklstClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvMaster)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvDetails)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Function getStuOweCount() As ArrayList
        Return getStuOweCount(Date.MinValue, Date.MaxValue)
    End Function

    Private Function GetStuOweCount(ByVal startDate As Date, ByVal endDate As Date) As ArrayList
        Dim intSubClass As Integer
        Dim intClass As Integer
        Dim strStuId As String
        Dim ClassFee As Integer
        Dim Discount As Integer
        Dim Pay As Integer
        Dim BackMoney As Integer
        Dim intTotalAll As Integer = 0
        'Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        'Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        'Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        'Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        'Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"
        Dim dtPaySta As New DataTable
        Dim dtDisc As New DataTable
        Dim dtStu As New DataTable
        Dim dtStuAll As New DataTable

        Dim lstSubClass As New List(Of String)
        Dim dtSubClass As New DataTable
        Dim stuOweArr As New ArrayList
        Dim countNum As New ArrayList
        Dim class_all As String = ""
        For k As Integer = 0 To lstClass.Count - 1
            class_all &= lstClass(k) & ","
        Next
        class_all = class_all.Trim()

        Dim inittime As DateTime = Now
        Dim jobtime As DateTime = Now

        dtPaySta = objCsol.ListStuOweByClass(lstClass)

        'dtDisc = objCsol.ListStuDiscByClass(lstClass)
        dtDisc = objCsol.ListStuDiscByClassHelper(lstClass)

        dtStuAll = objCsol.ListStuByClass(lstClass)

        'dtPaySta = GetListStuOweByClass(lstClass)
        'dtDisc = GetListStuDiscByClass(lstClass)
        'dtStuAll = GetListStuByClass(lstClass)

        Debug.Print("")

        For k As Integer = 0 To lstClass.Count - 1
            jobtime = Now
            inittime = Now
            Try
                dtSubClass = New DataTable

                dtSubClass = objCsol.GetSubClassID(lstClass(k))
                'dtSubClass = GetSubClassID(lstClass, k)
                Debug.Print("")

                lstSubClass.Clear()
                For j As Integer = 0 To dtSubClass.Rows.Count - 1
                    lstSubClass.Add(dtSubClass.Rows(j).Item("SubClassID"))
                Next




                Dim sturows() As DataRow = dtStuAll.Select(String.Format("'{0}' in ('{1}') ANd '{2}' >= '{3}' AND {2} <= '{4}'", c_SubClassIDColumnName, String.Join(",", lstSubClass.ToArray), c_RegDateColumnName, startDate, endDate))
                dtStu = dtStuAll.Clone()
                For Each row As DataRow In sturows
                    dtStu.Rows.Add(row.ItemArray)
                Next


                Dim dt As New DataTable
                dt = dtStu.DefaultView.ToTable(True, c_IDColumnName, _
                                    c_NameColumnName, c_SubClassNameColumnName, c_ClassIDColumnName, _
                                    c_SeatNumColumnName, c_SalesPersonColumnName, _
                                    c_SchoolGradeColumnName, c_SubClassIDColumnName, _
                                    c_EngNameColumnName, c_BirthdayColumnName, c_DadNameColumnName, _
                                    c_MumNameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, _
                                    c_DadMobileColumnName, c_MumMobileColumnName, c_MobileColumnName, _
                                    c_SchoolColumnName, c_SchoolClassColumnName, c_GraduateFromColumnName, _
                                    c_IntroIDColumnName, c_IntroNameColumnName, c_ICColumnName, _
                                    c_EmailColumnName, c_PrimarySchColumnName, c_JuniorSchColumnName, _
                                    c_HighSchColumnName, c_UniversityColumnName, c_CancelColumnName)
                dt.Columns.Add(c_AmountColumnName, GetType(System.Int32))
                dt.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
                dt.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
                dt.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
                dt.Columns.Add(c_Cancel2ColumnName, GetType(System.String))

                lstClassFee = New ArrayList
                For index As Integer = 0 To lstClass.Count - 1
                    lstClassFee.Add(frmMain.GetClassFee2(lstClass(index)))
                Next
                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i).Item(c_CancelColumnName).ToString = "0" Then
                        intSubClass = dt.Rows(i).Item(c_SubClassIDColumnName)
                        intClass = dt.Rows(i).Item(c_ClassIDColumnName)

                        If lstSubClass.IndexOf(intSubClass) > -1 Then
                            strStuId = dt.Rows(i).Item(c_IDColumnName)
                            ClassFee = GetClassFeeFromList(intClass)

                            Pay = 0
                            Discount = 0
                            BackMoney = 0

                            Dim rows() As DataRow = dtPaySta.Select(String.Format("{0} = '{1}' AND {2} = '{3}'", _
                                                                    c_SubClassIDColumnName, intSubClass.ToString(), c_StuIDColumnName, strStuId.ToString()))

                            For Each row As DataRow In rows
                                Pay += row.Item(c_AmountColumnName)
                                BackMoney += row.Item(c_BackAmountColumnName)
                            Next

                            rows = dtDisc.Select(String.Format("{0} = '{1}' AND {2} = '{3}'", _
                                                c_SubClassIDColumnName, intSubClass.ToString(), c_StuIDColumnName, strStuId.ToString()))

                            For Each row As DataRow In rows
                                Discount += row.Item(c_DiscountColumnName)
                            Next

                            dt.Rows(i).Item(c_AmountColumnName) = ClassFee - Discount
                            dt.Rows(i).Item(c_PayAmountColumnName) = Pay - BackMoney
                            dt.Rows(i).Item(c_DiscountColumnName) = Discount

                            dt.Rows(i).Item(c_FeeOweColumnName) = (ClassFee - Discount) - (Pay - BackMoney)

                            intTotalAll = intTotalAll + dt.Rows(i).Item(c_FeeOweColumnName)
                        End If
                    End If
                Next

                'Dim f As String = ""
                'If lstSubClass.Count > 0 Then
                '    f = c_SubClassIDColumnName & "=" & lstSubClass(0).ToString
                'End If
                'For index As Integer = 1 To lstSubClass.Count - 1
                '    f = f & " OR " & c_SubClassIDColumnName & "=" & lstSubClass(index).ToString
                'Next
                'If f = "" Then
                '    dt.DefaultView.RowFilter = "Cancel=0"
                'Else
                '    dt.DefaultView.RowFilter = f + " And Cancel=0"
                'End If

                'dt.DefaultView.RowFilter = "FeeOwe <=0"

                stuOweArr.Add(dt.Select(String.Format("{0} <= 0 ", c_FeeOweColumnName)).Count)

            Catch ex As ApplicationException
                AddErrorLog(ex.ToString)
                CSOL.Logger.LogError(ex.ToString())
            End Try

        Next
        Return stuOweArr
    End Function

    Private Shared Function GetSubClassID(ByVal lstClass As ArrayList, ByVal k As Integer) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)

        list.Add(lstClass(k))

        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetSubClassID", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetSubClassID").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetSubClassID"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListStuOweByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListStuOweByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuOweByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuOweByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListStuDiscByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListStuDiscByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuDiscByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuDiscByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListStuByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("classid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetListStuByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("ListStuByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("ListStuByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Function GetClassFeeFromList(ByVal intId As Integer) As Integer
        GetClassFeeFromList = 0
        Dim index As Integer = -1
        index = lstClass.IndexOf(intId)
        If index > -1 And index < lstClassFee.Count Then
            GetClassFeeFromList = lstClassFee(index)
        End If
    End Function

    Private Sub dtpickFrom_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dtpickFrom.ValueChanged
        dtpickTo.Value = dtpickFrom.Value
    End Sub


End Class