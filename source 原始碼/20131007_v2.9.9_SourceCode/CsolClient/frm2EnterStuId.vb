﻿Public Class frm2EnterStuId

    Private Sub frm2EnterStuId_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        Dim s As String = tboxId.Text.Trim
        If s.Length = 8 Then
            frmMain.SetCurrentStu(s)
            frmMain.SetOkState(True)
            Me.Close()
        End If
    End Sub
End Class