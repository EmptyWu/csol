﻿Public Class frmJournalSta
    Private dtMaster As New DataTable
    Private dtM As New DataTable
    Private dtDetails As New DataTable
    Private dtD As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmJournalSta
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmJournalSta
        RefreshData()
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        ExportDgvToExcel(dgvMaster)
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        ExportDgvToExcel(dgvDetails)
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        PrintDocument2.DefaultPageSettings.Landscape = True
        PrintDocument2.Print()
    End Sub

    Private Sub frmJournalSta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmJournalSta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Try
            Dim t1 As Date = Now
            If radbutDateCust.Checked Then
                dtDetails = objCsol.ListJournalSta(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutNoDate.Checked Then
                dtDetails = objCsol.ListJournalSta()
            End If
            If dtDetails.Rows.Count > 0 Then
                RefreshTable()
            End If
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshTable()
        Dim intGroupId As Integer
        Dim strItem As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_DataCntColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_AmountColumnName + ")"

        Try
            Dim t1 As Date = Now
            dtMaster.Clear()
            dtDetails.DefaultView.RowFilter = ""
            t1 = Now
            If radbutSta1.Checked Then
                dtMaster = dtDetails.DefaultView.ToTable(True, c_GroupIDColumnName, c_GroupColumnName)
                InitTable()
                For i = 0 To dtMaster.Rows.Count - 1
                    intGroupId = dtMaster.Rows(i).Item(c_GroupIDColumnName)
                    strCompute2 = c_GroupIDColumnName + "=" + intGroupId.ToString
                    If dtDetails.Rows.Count > 0 Then
                        intTotal = dtDetails.Compute(strCompute3, strCompute2)
                        If intTotal > 0 Then
                            intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                        Else
                            intTotal2 = 0
                        End If
                    Else
                        intTotal = 0
                    End If
                    dtMaster.Rows(i).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(i).Item(c_AmountColumnName) = intTotal2
                Next
                If dtMaster.Rows.Count > 0 Then
                    dtMaster.Rows.Add(-1, My.Resources.totalAmt, dtMaster.Compute(strCompute4, ""), _
                                        dtMaster.Compute(strCompute5, ""))
                Else
                    dtMaster.Rows.Add(-1, My.Resources.totalAmt, 0, 0)
                End If

            ElseIf radbutSta2.Checked Then
                dtMaster = dtDetails.DefaultView.ToTable(True, c_GroupIDColumnName, c_GroupColumnName, c_ItemColumnName)
                InitTable()
                For i = 0 To dtMaster.Rows.Count - 1
                    strItem = dtMaster.Rows(i).Item(c_ItemColumnName)
                    strCompute2 = String.Format("{0}='{1}'", c_ItemColumnName, strItem)
                    If dtDetails.Rows.Count > 0 Then
                        intTotal = dtDetails.Compute(strCompute3, strCompute2)
                        If intTotal > 0 Then
                            intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                        Else
                            intTotal2 = 0
                        End If
                    Else
                        intTotal = 0
                    End If
                    dtMaster.Rows(i).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(i).Item(c_AmountColumnName) = intTotal2
                Next
                If dtMaster.Rows.Count > 0 Then
                    dtMaster.Rows.Add(-1, My.Resources.totalAmt, "", dtMaster.Compute(strCompute4, ""), _
                                        dtMaster.Compute(strCompute5, ""))
                Else
                    dtMaster.Rows.Add(-1, My.Resources.totalAmt, "", 0, 0)
                End If
            End If
            t1 = Now
            LoadColumnText()
            t1 = Now
            dgvMaster.DataSource = dtM
            dgvMaster.Columns("GroupID").Visible = False


            'If dgvMaster.RowCount > 0 Then
            '    dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells("群組")
            'End If
            t1 = Now
            dgvDetails.DataSource = dtD
            dgvDetails.Columns("ID").Visible = False
            dgvDetails.Columns("GroupID").Visible = False
            dgvDetails.Columns("UserID").Visible = False
            tboxTotal.Text = Format((dgvMaster.Rows(dgvMaster.RowCount - 1).Cells("金額").Value), "$#,##0")

            dgvMaster.Columns("金額").DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns("金額").DefaultCellStyle.Format = "$#,##0"

            'dgvMaster.DataSource = dtMaster
            'If dgvMaster.RowCount > 0 Then
            '    dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells(c_GroupColumnName)
            'End If
            'dtDetails.DefaultView.RowFilter = ""
            'dgvDetails.DataSource = dtDetails

            'tboxTotal.Text = Format((dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_AmountColumnName).Value), "$#,##0")

            'dgvMaster.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"

            'LoadColumnText()
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))

    End Sub

    Private Sub LoadColumnText()
        dtM = dtMaster.Clone

        If radbutSta1.Checked Then
            dtM.Columns("Group").ColumnName = "群組"
            dtM.Columns("DataCnt").ColumnName = "總筆數"
            dtM.Columns("Amount").ColumnName = "金額"
        ElseIf radbutSta2.Checked Then
            dtM.Columns("Group").ColumnName = "群組"
            dtM.Columns("Item").ColumnName = "項目"
            dtM.Columns("DataCnt").ColumnName = "總筆數"
            dtM.Columns("Amount").ColumnName = "金額"
        End If
        For Each row As DataRow In dtMaster.Rows
            dtM.Rows.Add(row.ItemArray)
        Next

        dtD = dtDetails.Clone
        dtD.Columns("Item").ColumnName = "項目"
        dtD.Columns("Amount").ColumnName = "金額"
        dtD.Columns("DateTime").ColumnName = "日期時間"
        dtD.Columns("SerialNum").ColumnName = "序號"
        dtD.Columns("ChequeNum").ColumnName = "支票號碼"
        dtD.Columns("Remarks3").ColumnName = "備註三"
        dtD.Columns("Remarks4").ColumnName = "備註四"
        dtD.Columns("Remarks5").ColumnName = "備註五"
        dtD.Columns("Handler").ColumnName = "承辦人員"
        dtD.Columns("Group").ColumnName = "群組"

        For Each row As DataRow In dtDetails.Rows
            dtD.Rows.Add(row.ItemArray)
        Next

        'For Each col In dgvMaster.Columns
        '    col.visible = False
        'Next
        'If dgvMaster.Rows.Count > 0 Then

        '    If radbutSta1.Checked Then
        '        dgvMaster.Columns.Item(c_GroupColumnName).DisplayIndex = 0
        '        dgvMaster.Columns.Item(c_GroupColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_GroupColumnName).HeaderText = My.Resources.group
        '        dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 1
        '        dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
        '        dgvMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 2
        '        dgvMaster.Columns.Item(c_AmountColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        '    ElseIf radbutSta2.Checked Then
        '        dgvMaster.Columns.Item(c_GroupColumnName).DisplayIndex = 0
        '        dgvMaster.Columns.Item(c_GroupColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_GroupColumnName).HeaderText = My.Resources.group
        '        dgvMaster.Columns.Item(c_ItemColumnName).DisplayIndex = 1
        '        dgvMaster.Columns.Item(c_ItemColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_ItemColumnName).HeaderText = My.Resources.item
        '        dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 2
        '        dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
        '        dgvMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 3
        '        dgvMaster.Columns.Item(c_AmountColumnName).Visible = True
        '        dgvMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        '    End If
        'End If
        'For Each col In dgvDetails.Columns
        '    col.visible = False
        'Next
        'If dgvDetails.Rows.Count > 0 Then

        '    dgvDetails.Columns.Item(c_GroupColumnName).DisplayIndex = 0
        '    dgvDetails.Columns.Item(c_GroupColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_GroupColumnName).HeaderText = My.Resources.group
        '    dgvDetails.Columns.Item(c_ItemColumnName).DisplayIndex = 1
        '    dgvDetails.Columns.Item(c_ItemColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_ItemColumnName).HeaderText = My.Resources.item
        '    dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 2
        '    dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 3
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvDetails.Columns.Item(c_SerialNumColumnName).DisplayIndex = 4
        '    dgvDetails.Columns.Item(c_SerialNumColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_SerialNumColumnName).HeaderText = My.Resources.serialNum
        '    dgvDetails.Columns.Item(c_ChequeNumColumnName).DisplayIndex = 5
        '    dgvDetails.Columns.Item(c_ChequeNumColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_ChequeNumColumnName).HeaderText = My.Resources.chequeNum
        '    dgvDetails.Columns.Item(c_Remarks3ColumnName).DisplayIndex = 6
        '    dgvDetails.Columns.Item(c_Remarks3ColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_Remarks3ColumnName).HeaderText = My.Resources.remarks3
        '    dgvDetails.Columns.Item(c_Remarks4ColumnName).DisplayIndex = 7
        '    dgvDetails.Columns.Item(c_Remarks4ColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_Remarks4ColumnName).HeaderText = My.Resources.remarks4
        '    dgvDetails.Columns.Item(c_Remarks5ColumnName).DisplayIndex = 8
        '    dgvDetails.Columns.Item(c_Remarks5ColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_Remarks5ColumnName).HeaderText = My.Resources.remarks5
        '    dgvDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 9
        '    dgvDetails.Columns.Item(c_HandlerColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        'End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmJournalSta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click
        Dim t1 As Date = Now
        RefreshData()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        If radbutSta1.Checked Then
            Dim intGroupId As Integer
            If dgvMaster.SelectedRows.Count > 0 Then
                intGroupId = dgvMaster.SelectedRows(0).Cells(c_GroupIDColumnName).Value
                If intGroupId > -1 Then
                    dtD.DefaultView.RowFilter = String.Format("{0}='{1}'", c_GroupIDColumnName, intGroupId.ToString)
                Else
                    dtD.DefaultView.RowFilter = ""
                End If
                dgvDetails.DataSource = dtD.DefaultView
            End If
        ElseIf radbutSta2.Checked Then
            Dim strItem As String
            If dgvMaster.SelectedRows.Count > 0 Then
                strItem = dgvMaster.SelectedRows(0).Cells("項目").Value
                If Not strItem Is Nothing And Not strItem = "" Then
                    dtD.DefaultView.RowFilter = String.Format("項目 = '{0}'", strItem)
                Else
                    dtD.DefaultView.RowFilter = ""
                End If
                dgvDetails.DataSource = dtD.DefaultView
            End If
        End If

        'If radbutSta1.Checked Then
        '    Dim intGroupId As Integer
        '    If dgvMaster.SelectedRows.Count > 0 Then
        '        intGroupId = dgvMaster.SelectedRows(0).Cells(c_GroupIDColumnName).Value
        '        If intGroupId > -1 Then

        '            dtDetails.DefaultView.RowFilter = c_GroupIDColumnName + "=" + intGroupId.ToString

        '            dgvDetails.DataSource = dtDetails.DefaultView
        '        Else
        '            dtDetails.DefaultView.RowFilter = ""
        '            dgvDetails.DataSource = dtDetails.DefaultView
        '        End If
        '    End If
        'ElseIf radbutSta2.Checked Then
        '    Dim strItem As String
        '    If dgvMaster.SelectedRows.Count > 0 Then
        '        strItem = dgvMaster.SelectedRows(0).Cells(c_ItemColumnName).Value
        '        If Not strItem Is Nothing And Not strItem = "" Then
        '            dtDetails.DefaultView.RowFilter = c_ItemColumnName & "='" & strItem & "'"
        '            dgvDetails.DataSource = dtDetails.DefaultView
        '        Else
        '            dtDetails.DefaultView.RowFilter = ""
        '            dgvDetails.DataSource = dtDetails.DefaultView
        '        End If
        '    End If
        'End If

    End Sub


    Private Sub radbutSta1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutSta1.CheckedChanged
        If Not dtDetails Is Nothing Then
            If dtDetails.Rows.Count > 0 Then
                RefreshTable()
            End If
        End If
    End Sub

End Class