﻿Public Class frm2PayTogether
    Dim dt As New DataTable
    Dim dt2 As New DataTable
    Dim CheckPass As Boolean
    Dim strStuId As String = ""
    Dim lstOwe As ArrayList = New ArrayList
    Dim feeOweAmount As Integer

    Public Sub New(ByRef d As DataTable, ByRef d2 As DataTable, ByVal Pass As Boolean)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        dt = d.Clone 'class reg table
        dt.Merge(d)
        dt2 = d2.Clone 'discount table
        dt2.Merge(d2)
        CheckPass = Pass
        strStuId = frmMain.GetCurrentStu
        RefreshClassTable()
        tboxTodayPay.Text = "0"
        tboxDisc.Text = "0"
        RefreshData()
        LoadColumnText()
        For index As Integer = 0 To dgv.Rows.Count - 1
            If Not dgv.Rows(index).Cells(c_FeeOweCalculatedColumnName).Value Is Nothing Then
                Dim ix As Integer = dgv.Rows(index).Cells(c_FeeOweCalculatedColumnName).Value
                lstOwe.Add(ix)
            Else
                lstOwe.Add(0)
            End If
        Next

    End Sub

    Private Sub RefreshClassTable()
        Dim strCompute2 As String
        Dim i As Integer = 0

        If dt.Rows.Count = 0 Then
            dgv.DataSource = dt
            Dim column As New DataGridViewCheckBoxColumn(False)
            column.Name = "Check"
            dgv.Columns.Add(column)
            dgv.Columns.Add("TodayPay", "")
            dgv.Columns.Add("DiscountToday", "")
            Exit Sub
        End If

        If Not dt.Columns.Contains("DiscountRemarks") Then
            dt.Columns.Add("DiscountRemarks", GetType(System.String))
        End If
        For j As Integer = 0 To dt.Rows.Count - 1
            Dim subClassID As String = dt.Rows(j).Item("SubClassID").ToString.Trim
            Dim discount As String = dt.Rows(j).Item("Discount").ToString.Trim
            For k As Integer = 0 To dt2.Rows.Count - 1
                If dt2.Rows(k).Item("SubClassID").ToString.Trim = subClassID And dt2.Rows(k).Item("Discount").ToString.Trim = discount Then
                    dt.Rows(j).Item("DiscountRemarks") = dt2.Rows(k).Item("Remarks")
                End If
            Next
        Next
        Try
            strCompute2 = c_EndColumnName & ">='" & Now.ToString & "'"
            If CheckPass Then
                'dt.DefaultView.RowFilter = ""
            Else
                dt.DefaultView.RowFilter = strCompute2
            End If

            dgv.DataSource = dt
            Dim column As New DataGridViewCheckBoxColumn(False)
            column.Name = "Check"
            Dim column1 As New DataGridViewColumn(dgv.Rows(0).Cells(c_AmountColumnName))
            Dim column2 As New DataGridViewColumn(dgv.Rows(0).Cells(c_AmountColumnName))
            column1.Name = "TodayPay"
            column2.Name = "DiscountToday"
            dgv.Columns.Add(column)
            dgv.Columns.Add(column1)
            dgv.Columns.Add(column2)


            For index As Integer = 0 To dgv.Rows.Count - 1
                dgv.Rows(index).Cells("Check").Value = False
                dgv.Rows(index).Cells("TodayPay").Value = 0
                dgv.Rows(index).Cells("DiscountToday").Value = 0
            Next

            If dgv.RowCount > 0 Then
                dgv.CurrentCell = dgv.Rows.Item(0).Cells(c_SubClassNameColumnName)
            End If

            dgv.Refresh()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgv.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.shouldPay
            dgv.Columns.Item(c_PayAmountColumnName).HeaderText = My.Resources.paid
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).HeaderText = My.Resources.feeOwe
            dgv.Columns.Item("Check").HeaderText = My.Resources.selectCheck
            dgv.Columns.Item("TodayPay").HeaderText = My.Resources.todayPay
            dgv.Columns.Item("DiscountToday").HeaderText = My.Resources.discount
            dgv.Columns.Item("DiscountRemarks").HeaderText = "優待備註"
            dgv.Columns.Item("Check").DisplayIndex = 0
            dgv.Columns.Item("Check").Visible = True
            dgv.Columns.Item("Check").ReadOnly = True
            dgv.Columns.Item(c_ClassNameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_ClassNameColumnName).Visible = True
            dgv.Columns.Item(c_ClassNameColumnName).ReadOnly = True
            dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgv.Columns.Item(c_SubClassNameColumnName).ReadOnly = True
            dgv.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_AmountColumnName).Visible = True
            dgv.Columns.Item(c_AmountColumnName).ReadOnly = True
            dgv.Columns.Item(c_PayAmountColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_PayAmountColumnName).Visible = True
            dgv.Columns.Item(c_PayAmountColumnName).ReadOnly = True
            dgv.Columns.Item("DiscountToday").DisplayIndex = 5
            dgv.Columns.Item("DiscountToday").Visible = True
            dgv.Columns.Item("DiscountToday").ReadOnly = True
            dgv.Columns.Item("DiscountRemarks").DisplayIndex = 6
            dgv.Columns.Item("DiscountRemarks").Visible = True
            dgv.Columns.Item("DiscountRemarks").ReadOnly = True
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).DisplayIndex = 7
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).Visible = True
            dgv.Columns.Item(c_FeeOweCalculatedColumnName).ReadOnly = True
            dgv.Columns.Item("TodayPay").DisplayIndex = 8
            dgv.Columns.Item("TodayPay").Visible = True
        End If
    End Sub

    Private Sub RefreshData()
        Dim intTotalOwe As Integer = 0
        Dim intTodayPay As Integer = 0
        Dim intTotalDisc As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute3 As String
        Dim i As Integer = 0

        For index As Integer = 0 To dgv.Rows.Count - 1
            strCompute3 = c_SubClassIDColumnName & "=" & dgv.Rows(index).Cells(c_SubClassIDColumnName).Value.ToString
            Try
                i = dt2.Compute(strCompute1, strCompute3)
            Catch ex2 As Exception
                i = 0
            End Try
            dgv.Rows(index).Cells("DiscountToday").Value = i
        Next

        For index As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(index).Cells("Check").Value = True Then
                intTotalDisc = intTotalDisc + dgv.Rows(index).Cells("DiscountToday").Value
                dgv.Rows(index).Cells("TodayPay").Value = dgv.Rows(index).Cells(c_FeeOweCalculatedColumnName).Value
                intTodayPay = intTodayPay + CInt(dgv.Rows(index).Cells("TodayPay").Value)
            End If
            intTotalOwe = intTotalOwe + dgv.Rows(index).Cells(c_FeeOweCalculatedColumnName).Value
        Next

       

        tboxTodayPay.Text = intTodayPay.ToString
        tboxOwe.Text = intTotalOwe.ToString
        tboxDisc.Text = intTotalDisc.ToString
        feeOweAmount = intTotalOwe
    End Sub

    Private Sub butMkPay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butMkPay.Click
        Dim intScId As Integer
        Dim datePay As Date
        Dim dateGet As Date
        Dim intAmt As Integer
        Dim intPayMethod As Integer
        Dim strExtra As String = ""
        Dim intDiscount As Integer = 0
        Dim strDiscountRemarks As String = ""
        Dim strSales As String
        Dim dtChequeValid As Date = New Date(Now.Year + 1, Now.Month, Now.Day)
        Dim strReceipt As String
        Dim intHasPaid As Integer = 0
        For idx As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(idx).Cells("Check").Value = True Then
                intScId = dgv.Rows(idx).Cells(c_SubClassIDColumnName).Value
                strSales = dgv.Rows(idx).Cells(c_SalesIDColumnName).Value

                If radbutNormal.Checked Then
                    datePay = Now
                    dateGet = Now
                ElseIf radbutMakeup.Checked Then
                    datePay = dtpickMakeupDate.Value
                    dateGet = dtpickMakeupDate.Value
                End If
                intAmt = dgv.Rows(idx).Cells("TodayPay").Value
                If intAmt > 0 Then
                    If radbutCash.Checked Then
                        intPayMethod = c_PayMethodCash
                    Else
                        strExtra = tboxPayExtraInfo.Text.Trim
                        If radbutChk.Checked Then
                            intPayMethod = c_PayMethodCheque
                            dtChequeValid = dtpickCheque.Value
                        ElseIf radbutTrans.Checked Then
                            intPayMethod = c_PayMethodTransfer
                        ElseIf radbutCard.Checked Then
                            intPayMethod = c_PayMethodCreditCard
                        End If
                    End If
                    intDiscount = frmMain.GetCurrentValue()
                    strDiscountRemarks = tboxDiscRemarks.Text

                    If strStuId.Length = 8 And intScId > -1 Then
                        Dim dtDate As Date
                        If radbutNormal.Checked Then
                            dtDate = Now
                        Else
                            dtDate = dtpickMakeupDate.Value
                        End If

                        Dim d1 As Date = GetDateStart(dtDate)
                        Dim d2 As Date = GetDateEnd(dtDate)
                        Dim a1 As String = ""
                        Dim a2 As String = ""
                        Dim a3 As String = ""
                        strReceipt = objCsol.GenReceiptNum(dtDate)
                        objCsol.AddPay(strReceipt, strStuId, intScId, _
                            intAmt, datePay, frmMain.GetUsrId, strSales, intPayMethod, _
                            strExtra, tboxReceiptRemarks.Text, dtChequeValid, _
                               0, tboxDiscRemarks.Text, -999999, "", -1, dateGet.ToString("yyyy/MM/dd"))

                        intHasPaid = intHasPaid + 1
                    End If
                End If
            End If
        Next
        If intHasPaid > 0 Then
            frmMain.SetOkState(True)
            Me.Close()
        Else
            MsgBox(My.Resources.msgPayTogetherNoAmount, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butDisDict_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDisDict.Click
        Dim frm As New frm2DiscDict
        frmMain.SetOkState(False)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            tboxDiscRemarks.Text = frmMain.GetCurrentString
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub butRemarkDict_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butRemarkDict.Click
        Dim frm As New frm2ReceiptRkDict
        frmMain.SetOkState(False)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            tboxReceiptRemarks.Text = frmMain.GetCurrentString
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassTable()
        LoadColumnText()
        RefreshData()
    End Sub

    Private Sub dgv_CellClick1(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        If dgv.Columns(e.ColumnIndex).Name = "Check" Then
            If Not Object.Equals(dgv.Item(e.ColumnIndex, e.RowIndex).Value, True) Then
                dgv.Item(e.ColumnIndex, e.RowIndex).Value = True
            Else
                dgv.Item(e.ColumnIndex, e.RowIndex).Value = False
                dgv.Rows(e.RowIndex).Cells("TodayPay").Value = 0
            End If
        End If
    End Sub

    Private Sub radbutChk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutChk.CheckedChanged
        If radbutChk.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = True
            lblCheque.Visible = True
            grpboxExtra.Text = My.Resources.chequeNum
        End If
    End Sub

    Private Sub radbutCash_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutCash.CheckedChanged
        If radbutCash.Checked Then
            grpboxExtra.Visible = False
        End If
    End Sub

    Private Sub radbutTrans_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutTrans.CheckedChanged
        If radbutTrans.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = False
            lblCheque.Visible = False
            grpboxExtra.Text = My.Resources.transferAccnt
        End If
    End Sub

    Private Sub radbutCard_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutCard.CheckedChanged
        If radbutCard.Checked Then
            grpboxExtra.Visible = True
            dtpickCheque.Visible = False
            lblCheque.Visible = False
            grpboxExtra.Text = My.Resources.creditAuthNum
        End If
    End Sub

    Private Sub frm2PayTogether_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        RefreshData()
    End Sub

   

    Private Sub dgv_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellValueChanged

        If Not e.RowIndex = -1 Then
            If dgv.Rows(e.RowIndex).Cells("Check").Value = True Then
                dgv.Columns.Item("TodayPay").ReadOnly = False
                Dim amount As Integer = dgv.Rows(e.RowIndex).Cells(c_AmountColumnName).Value
                Dim payAmount As Integer = dgv.Rows(e.RowIndex).Cells(c_PayAmountColumnName).Value

                Dim discount As Integer
                Dim todayPay As Integer = dgv.Rows(e.RowIndex).Cells("TodayPay").Value

                dgv.Rows(e.RowIndex).Cells(c_FeeOweCalculatedColumnName).Value = amount - payAmount - todayPay
                Dim todayPayAmount As Integer

                For i As Integer = 0 To dgv.Rows.Count - 1
                    If dgv.Item(0, i).Value = True Then
                        todayPayAmount += dgv.Rows(i).Cells("TodayPay").Value
                        discount += dgv.Rows(i).Cells("DiscountToday").Value
                    End If
                Next
                tboxDisc.Text = discount
                tboxTodayPay.Text = todayPayAmount
            Else
                dgv.Columns.Item("TodayPay").ReadOnly = True
                Dim amount As Integer = dgv.Rows(e.RowIndex).Cells(c_AmountColumnName).Value
                Dim payAmount As Integer = dgv.Rows(e.RowIndex).Cells(c_PayAmountColumnName).Value

                Dim discount As Integer
                Dim todayPay As Integer = dgv.Rows(e.RowIndex).Cells("TodayPay").Value

                dgv.Rows(e.RowIndex).Cells(c_FeeOweCalculatedColumnName).Value = amount - payAmount - todayPay
                Dim todayPayAmount As Integer

                For i As Integer = 0 To dgv.Rows.Count - 1
                    If dgv.Item(0, i).Value = True Then
                        todayPayAmount += dgv.Rows(i).Cells("TodayPay").Value
                        discount += dgv.Rows(i).Cells("DiscountToday").Value
                    End If
                Next
                tboxDisc.Text = discount
                tboxTodayPay.Text = todayPayAmount
            End If

        End If

    End Sub

    Private Sub tboxTodayPay_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tboxTodayPay.TextChanged
        tboxOwe.Text = (feeOweAmount - CInt(tboxTodayPay.Text)).ToString
    End Sub

    Private Sub dgv_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.SelectionChanged
        Dim aas As Integer = 1
        Try
            tboxDiscRemarks.Text = dgv.CurrentRow.Cells("DiscountRemarks").Value.ToString.Trim
        Catch ex As Exception

        End Try

    End Sub
End Class