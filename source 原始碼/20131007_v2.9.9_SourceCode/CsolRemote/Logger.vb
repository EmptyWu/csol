﻿Namespace CSOL
    Public Class Logger
        Private Shared m_Path As String
        Public Shared Property Path() As String
            Get
                Return m_Path
            End Get
            Set(ByVal value As String)
                m_Path = value
            End Set
        End Property

        Public Shared Sub Log(ByVal Message As String)
            Log("", Message)
        End Sub

        Public Shared Sub Log(ByVal Prefix As String, ByVal Message As String)
            If System.IO.Directory.Exists(Path) Then
                Dim file As String = String.Format("{0}\{1}.log", Path, Now.ToString("yyyyMMdd"))
                Dim content As String = String.Format("{0}{1}{2}{3}", Now.ToString("HH:mm:ss.fff"), vbTab, Message, vbCrLf)
                If Prefix <> "" Then
                    String.Format("{0}\{1}-{2}.log", Path, Prefix, Now.ToString("yyyyMMdd"))
                End If

                Try
                    System.IO.File.AppendAllText(file, content)
                Catch ex As Exception
                    System.Windows.Forms.MessageBox.Show(ex.ToString)
                End Try
            End If
        End Sub

        Public Shared Sub LogError(ByVal Message As String)
            Log("Error", Message)
        End Sub

        Public Shared Sub LogMsg(ByVal Message As String)
            Log("Msg", Message)
        End Sub

    End Class
End Namespace
