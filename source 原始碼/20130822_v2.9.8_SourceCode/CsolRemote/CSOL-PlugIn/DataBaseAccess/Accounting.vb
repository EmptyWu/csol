﻿Namespace DataBaseAccess
    Public Class Accounting
        Public Shared Function ListStuOweByClass(ByVal classID As ArrayList) As DataTable

            Dim dt As New DataTable
            Dim arrclassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrclassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrclassid)

            Dim sq As New System.Text.StringBuilder()
            sq.Append("select pa.amount, pa.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("'0' as discount, sc.classid, pa.subclassid, tc.seatnum, '0' as backamount, ")
            sq.Append("'0' as keepamount, us2.name as salesperson, si.schoolgrade, pa.datetime, ")
            sq.Append("pa.paymethod, us1.name as handler, pa.extra, '' as discountremarks, ")
            sq.Append("pa.remarks, si.engname, pa.id, pa.receiptnum, pa.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from payrec as pa inner join ")
            sq.Append("subclass as sc on pa.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on pa.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on pa.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on pa.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (pa.stuid = tc.stuid and ")
            sq.Append("pa.subclassid = tc.subclassid) ")

            sq.AppendFormat("where (sc.classid in ({0})) ", str)
            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("mb.amount as backamount, '0' as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from moneybackpayrec as mp inner join ")
            sq.Append("moneybackclassinfo as mb on mp.mbclassinfoid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.classid in ({0})) ", str)

            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("'0' as backamount, mb.amount as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from keeppayrec as mp inner join ")
            sq.Append("keeppaysubclassrec as mb on mp.kpclassrecid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.classid in ({0})) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function
        Public Shared Function ListStuOweByClass(ByVal classID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)

            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT distinct PA.Amount, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sq.Append("'0' AS Discount, SC.ClassID, PA.SubClassID, TC.SeatNum, '0' AS BackAmount, ")
            sq.Append("'0' AS KeepAmount, US2.Name As SalesPerson, SI.SchoolGrade, PA.DateTime, ")
            sq.Append("PA.PayMethod, US1.Name As Handler, PA.Extra, '' AS DiscountRemarks, ")
            sq.Append("PA.Remarks, SI.EngName, PA.ID, PA.ReceiptNum, PA.HandlerID, ")
            sq.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, ")
            sq.Append("SI.MumMobile, SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, SI.CreateDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, SI.Sex, ")
            sq.Append("SI.CurrentSch, SI.SchGroup ")
            sq.Append("FROM PayRec AS PA INNER JOIN ")
            sq.Append("SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN ")
            sq.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON PA.StuID = SI.ID left outer JOIN ")
            sq.Append("OrgUser As US1 ON PA.HandlerID = US1.ID left outer JOIN ")
            sq.Append("OrgUser As US2 ON PA.SalesID = US2.ID INNER JOIN ")
            sq.Append("StuClass AS TC ON (PA.StuID = TC.StuID AND ")
            sq.Append("PA.SubClassID = TC.SubClassID) ")

            sq.AppendFormat("WHERE SC.ClassID in ({0}) And PA.DateTime BETWEEN '{1}' AND '{2}' ", str, DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            sq.Append("UNION ALL ")
            sq.Append("SELECT distinct MP.Amount, MB.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sq.Append("MB.Discount, SC.ClassID, MB.SubClassID, TC.SeatNum, ")
            sq.Append("MB.Amount As BackAmount, '0' AS KeepAmount, US2.Name As SalesPerson, ")
            sq.Append("SI.SchoolGrade, MP.DateTime, ")
            sq.Append("MP.PayMethod, US1.Name As Handler, MP.Extra, MB.DiscountRemarks, ")
            sq.Append("MP.Remarks, SI.EngName, MP.ID, MP.ReceiptNum, MP.HandlerID, ")
            sq.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, ")
            sq.Append("SI.MumMobile, SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, SI.CreateDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, SI.Sex, ")
            sq.Append("SI.CurrentSch, SI.SchGroup ")
            sq.Append("FROM MoneyBackPayRec AS MP INNER JOIN ")
            sq.Append("MoneyBackClassInfo AS MB ON MP.MBClassInfoID = MB.ID INNER JOIN ")
            sq.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sq.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer JOIN ")
            sq.Append("OrgUser As US1 ON MB.HandlerID = US1.ID left outer JOIN ")
            sq.Append("OrgUser As US2 ON MB.SalesID = US2.ID INNER JOIN ")
            sq.Append("StuClass AS TC ON (MB.StuID = TC.StuID AND ")
            sq.Append("MB.SubClassID = TC.SubClassID) ")
            sq.AppendFormat("WHERE SC.ClassID in ({0}) And PA.DateTime BETWEEN '{1}' AND '{2}' ", str, DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))

            sq.Append("UNION ALL ")
            sq.Append("SELECT distinct MP.Amount, MB.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sq.Append("MB.Discount, SC.ClassID, MB.SubClassID, TC.SeatNum, ")
            sq.Append("'0' As BackAmount, MB.Amount AS KeepAmount, US2.Name As SalesPerson, ")
            sq.Append("SI.SchoolGrade, MP.DateTime, ")
            sq.Append("MP.PayMethod, US1.Name As Handler, MP.Extra, MB.DiscountRemarks, ")
            sq.Append("MP.Remarks, SI.EngName, MP.ID, MP.ReceiptNum, MP.HandlerID, ")
            sq.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, ")
            sq.Append("SI.MumMobile, SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, SI.CreateDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, SI.Sex, ")
            sq.Append("SI.CurrentSch, SI.SchGroup ")
            sq.Append("FROM KeepPayRec AS MP INNER JOIN ")
            sq.Append("KeepPaySubClassRec AS MB ON MP.KPClassRecID = MB.ID INNER JOIN ")
            sq.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sq.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sq.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer JOIN ")
            sq.Append("OrgUser As US1 ON MB.HandlerID = US1.ID left outer JOIN ")
            sq.Append("OrgUser As US2 ON MB.SalesID = US2.ID INNER JOIN ")
            sq.Append("StuClass AS TC ON (MB.StuID = TC.StuID AND ")
            sq.Append("MB.SubClassID = TC.SubClassID) ")
            sq.AppendFormat("WHERE SC.ClassID in ({0}) And PA.DateTime BETWEEN '{1}' AND '{2}' ", str, DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function ListStuDiscByClass(ByVal ClassID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrClassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT distinct FD.Discount, FD.Remarks, FD.SubClassID, FD.StuID ")
            sq.Append("FROM FeeDiscount AS FD ")
            sq.Append("INNER JOIN PayRec AS PA on FD.SubClassID = PA.SubClassID AND FD.StuID = PA.StuID ")
            sq.Append("INNER JOIN SubClass AS SC on FD.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE SC.ClassID in ({0}) AND PA.DtaeTime BETWEEN '{1}' AND '{2}' ", str, DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function ListStuDiscByClass(ByVal classID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT distinct FD.Discount, FD.Remarks, FD.SubClassID, FD.StuID ")
            sq.Append("FROM FeeDiscount AS FD ")
            sq.Append("INNER JOIN SubClass AS SC on FD.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE SC.ClassID in ({0})", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function ListStuByClass(ByVal classID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode, ")
            sq.Append("SI.Address2, Cancel, SI.IC, ")
            sq.Append("PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email, SI.CardNum,StuType, School, ")
            sq.Append("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University, ")
            sq.Append("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sq.Append("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks,SI.Customize1 AS ComeInfo, ")
            sq.Append("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark,")
            sq.Append("SC.SubClassID, SU.ClassID, SU.Name AS SubClassName, SC.SeatNum, OU.Name AS SalesPerson, ")
            sq.Append("SC.FeeOwe,SC.RegisterDate,OU.Name AS SalesName ")
            sq.Append("FROM StuInfo AS SI LEFT OUTER JOIN ")
            sq.Append("StuClass AS SC ON SC.StuID = SI.ID INNER JOIN ")
            sq.Append("SubClass AS SU ON SC.SubClassID = SU.ID LEFT OUTER JOIN ")
            sq.Append("OrgUser AS OU ON SC.SalesID = OU.ID ")
            sq.AppendFormat("WHERE SU.SubClassID in ({0}) ", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function ListClassStaByNoDate(ByVal classID As ArrayList) As DataTable
            'Return ListClassStaByDate(classID, New Date(1900, 1, 1), Today)

            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT         CL.*, (SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM           StuClass ")
            sq.Append("WHERE          (SubClassID IN (SELECT ID FROM SubClass WHERE ClassID = CL.ID) )) AS StuRegCnt, ")
            sq.Append("(SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM           StuClass ")
            sq.Append("WHERE          (SubClassID IN (SELECT ID FROM SubClass WHERE ClassID = CL.ID) ) AND ")
            sq.Append("(FeeOwe = 0)) AS FeeClrCnt           ")
            sq.Append("FROM            Class AS CL  ")
            sq.AppendFormat("WHERE CL.ID in ({0}) ", str)
            sq.Append("ORDER BY Name ")
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt


        End Function

        Public Shared Function ListClassStaByDate(ByVal classID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next

            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT         CL.*, (SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM           StuClass ")
            sq.Append("WHERE          SubClassID IN (SELECT ID FROM SubClass WHERE ClassID = CL.ID)  ")
            sq.AppendFormat("AND RegisterDate BETWEEN '{0}' AND '{1}') AS StuRegCnt, ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            sq.Append("(SELECT COUNT(ID) AS SNID ")
            sq.Append("FROM           StuClass ")
            sq.Append("WHERE          (SubClassID IN (SELECT ID FROM SubClass WHERE ClassID = CL.ID) )   ")
            sq.AppendFormat("AND RegisterDate BETWEEN '{0}' AND '{1}' ", DateFrom.ToString("yyyy/MM/dd 00:00:00"), DateTo.ToString("yyyy/MM/dd 23:59:59"))
            sq.Append("AND	(FeeOwe = 0)) AS FeeClrCnt  ")
            sq.Append("FROM            Class AS CL  ")
            sq.AppendFormat("WHERE CL.ID in ({0}) ", str)
            sq.Append("ORDER BY Name ")

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetPayStaClassNoDAte(ByVal ClassID As ArrayList) As DataTable
            Return GetPayStaByClass(ClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetPayStaByClass(ByVal classID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable

            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            sb.AppendLine("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sb.AppendLine("SI.School, PA.DateTime, PA.PayMethod, PA.Amount, US.Name As Handler, ")
            sb.AppendLine("PA.Extra, SC.ClassID, PA.SubClassID, TC.SeatNum, ")
            sb.AppendLine("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sb.AppendLine("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sb.AppendLine("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sb.AppendLine("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sb.AppendLine("FROM PayRec AS PA INNER JOIN ")
            sb.AppendLine("SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN ")
            sb.AppendLine("StuInfo AS SI ON PA.StuID = SI.ID left outer join ")
            sb.AppendLine("OrgUser As US ON PA.HandlerID = US.ID INNER JOIN ")
            sb.AppendLine("StuClass As TC ON (PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID) ")
            'sb.AppendFormat("WHERE SC.ClassID = {0} And PA.DateTime BETWEEN {1} AND {2}", str, DateFrom, DateTo)
            sb.AppendFormat("WHERE (PA.DateTime BETWEEN '{0}' AND '{1}') AND SC.classID in ({2})", DateFrom.ToString("yyyy/MM/dd 00:00:00.000"), DateTo.ToString("yyyy/MM/dd 23:59:59.999"), str)
            '"WHERE (PA.DateTime BETWEEN '{0}' AND '{1}') AND SC.classID in ({2})", DateFrom.ToString("yyyy/MM/dd"), DateTo.ToString("yyyy/MM/dd"), str
            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function
        Public Shared Function GetPayStaBySubClassNoDAte(ByVal SubClassID As ArrayList) As DataTable
            Return GetPayStaBySubClass(SubClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetPayStaBySubClass(ByVal SubClassID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            Dim arrSubClassid(SubClassID.Count - 1) As String
            For i As Integer = 0 To SubClassID.Count - 1
                arrSubClassid(i) = SubClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)
            sb.AppendLine("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName,")
            sb.AppendLine("SI.School, PA.DateTime, PA.PayMethod, PA.Amount, US.Name As Handler, ")
            sb.AppendLine("PA.Extra, SC.ClassID, PA.SubClassID, TC.SeatNum, ")
            sb.AppendLine("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sb.AppendLine("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sb.AppendLine("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sb.AppendLine("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sb.AppendLine("FROM PayRec AS PA INNER JOIN ")
            sb.AppendLine("SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN ")
            sb.AppendLine("StuInfo AS SI ON PA.StuID = SI.ID LEFT OUTER JOIN ")
            sb.AppendLine("OrgUser As US ON PA.HandlerID = US.ID INNER JOIN ")
            sb.AppendLine("StuClass As TC ON (PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID) ")
            sb.AppendFormat("WHERE (PA.DateTime BETWEEN '{0}' AND '{1}') AND SC.ClassID in ({2})", DateFrom.ToString("yyyy/MM/dd 00:00:00.000"), DateTo.ToString("yyyy/MM/dd 23:59:59.999"), str)

            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetMBackStaBySubClassNoDate(ByVal SubClassID As ArrayList) As DataTable
            Return GetMBackStaBySubClass(SubClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetMBackStaBySubClass(ByVal SubClassID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            Dim arrSubClassid(SubClassID.Count - 1) As String
            For i As Integer = 0 To SubClassID.Count - 1
                arrSubClassid(i) = SubClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)

            sb.AppendLine("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, ")
            sb.AppendLine("BP.DateTime, BP.PayMethod, BP.Amount, BP.Extra, ")
            sb.AppendLine("US.Name AS Handler, SC.Name AS SubClassName, MB.Amount AS BackAmount, ")
            sb.AppendLine("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, ")
            sb.AppendLine("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sb.AppendLine("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sb.AppendLine("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sb.AppendLine("SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sb.AppendLine("FROM MoneyBackClassInfo AS MB INNER JOIN ")
            sb.AppendLine("MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID INNER JOIN ")
            sb.AppendLine("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sb.AppendLine("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sb.AppendLine("StuInfo AS SI ON MB.StuID = SI.ID LEFT OUTER JOIN ")
            sb.AppendLine("OrgUser AS US ON BP.HandlerID = US.ID INNER JOIN ")
            sb.AppendLine("StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")
            sb.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND SC.ClassID in ({2})", DateFrom.ToString("yyyy/MM/dd 00:00:00.000"), DateTo.ToString("yyyy/MM/dd 23:59:59.999"), str)

            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetSubClassID(ByVal classID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(classID.Count - 1) As String
            For i As Integer = 0 To classID.Count - 1
                arrClassid(i) = classID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT ID AS SubClassID,ClassID ")
            sq.Append("FROM SubClass ")
            sq.AppendFormat("WHERE ClassID in ({0}) ", str)

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetMKeepStaBySubClassNoDate(ByVal SubClassID As ArrayList) As DataTable
            Return GetMKeepStaBySubClass(SubClassID, New Date(1900, 1, 1), Today)
        End Function

        Public Shared Function GetMKeepStaBySubClass(ByVal SubClassID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            Dim arrSubClassid(SubClassID.Count - 1) As String
            For i As Integer = 0 To SubClassID.Count - 1
                arrSubClassid(i) = SubClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrSubClassid)

            sb.AppendLine("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, ")
            sb.AppendLine("BP.DateTime, BP.PayMethod, BP.Amount, BP.Extra, ")
            sb.AppendLine("US.Name As Handler, SC.Name AS SubClassName, MB.Amount As BackAmount, ")
            sb.AppendLine("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, ")
            sb.AppendLine("SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, ")
            sb.AppendLine("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sb.AppendLine("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sb.AppendLine("SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sb.AppendLine("FROM MoneyBackClassInfo AS MB ")
            sb.AppendLine("INNER JOIN MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID ")
            sb.AppendLine("INNER JOIN SubClass AS SC ON MB.SubClassID = SC.ID  ")
            sb.AppendLine("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
            sb.AppendLine("INNER JOIN StuInfo AS SI ON MB.StuID = SI.ID  ")
            sb.AppendLine("left outer join OrgUser As US ON BP.HandlerID = US.ID ")
            sb.AppendLine("INNER JOIN StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")
            sb.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND SC.ClassID in ({2})", DateFrom.ToString("yyyy/MM/dd 00:00:00.000"), DateTo.ToString("yyyy/MM/dd 23:59:59.999"), str)

            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetPayStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()

            sb.Append("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sb.Append("SI.SchoolGrade, PA.DateTime AS PayDateTime, PA.PayMethod, PA.Amount, ")
            sb.Append("US1.Name AS Handler, US2.Name AS SalesPerson, PA.Extra, ")
            sb.Append("PA.Remarks, SC.ClassID,  ")
            sb.Append("CL.Name AS ClassName, PA.SubClassID, SI.EngName, SI.Birthday,  ")
            sb.Append("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile,  ")
            sb.Append("SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName,  ")
            sb.Append("SI.IC, SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sb.Append("FROM PayRec AS PA INNER JOIN ")
            sb.Append("SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN ")
            sb.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sb.Append("StuInfo AS SI ON PA.StuID = SI.ID left outer JOIN ")
            sb.Append("OrgUser AS US1 ON PA.HandlerID = US1.ID left outer JOIN ")
            sb.Append("OrgUser AS US2 ON PA.SalesID = US2.ID INNER JOIN ")
            sb.Append("StuClass AS TC ON PA.StuID = TC.StuID AND  ")
            sb.Append("PA.SubClassID = TC.SubClassID ")
            sb.AppendFormat("WHERE (PA.DateTime BETWEEN '{0} 00:00:00.000' AND '{1} 23:59:59.999') ", dateFrom.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"))
            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt

        End Function

        Public Shared Function GetMBackStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()

            sb.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
            sb.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name AS Handler,  ")
            sb.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount AS Amount, SC.ClassID,  ")
            sb.Append("CL.Name AS ClassName, MB.SubClassID, SI.EngName, SI.Birthday, SI.DadName, ")
            sb.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade,  ")
            sb.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC,  ")
            sb.Append("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
            sb.Append("FROM MoneyBackClassInfo AS MB INNER JOIN ")
            sb.Append("MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID INNER JOIN ")
            sb.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sb.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sb.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer JOIN ")
            sb.Append("OrgUser AS US ON BP.HandlerID = US.ID INNER JOIN ")
            sb.Append("StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")
            'sb.AppendFormat("WHERE (MB.DateTime BETWEEN '{0} 00:00:00.000' AND '{1} 23:59:59.999') OR  ", dateFrom.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"))
            'sb.AppendFormat("(BP.DateTime BETWEEN '{0} 00:00:00.000' AND '{1} 23:59:59.999') ", dateFrom.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"))
            sb.AppendFormat("WHERE (BP.DateTime BETWEEN '{0} 00:00:00.000' AND '{1} 23:59:59.999') ", dateFrom.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"))



            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function GetMKeepStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()

            sb.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
            sb.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name As Handler,  ")
            sb.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount,  ")
            sb.Append("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, SI.EngName,  ")
            sb.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile,  ")
            sb.Append("SI.MumMobile, SI.SchoolGrade, SI.SchoolClass, SI.GraduateFrom,  ")
            sb.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, TC.RegisterDate,  ")
            sb.Append("SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
            sb.Append("FROM KeepPaySubClassRec AS MB INNER JOIN ")
            sb.Append("KeepPayRec AS BP ON MB.ID = BP.KPClassRecID INNER JOIN ")
            sb.Append("SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN ")
            sb.Append("Class AS CL ON SC.ClassID = CL.ID INNER JOIN ")
            sb.Append("StuInfo AS SI ON MB.StuID = SI.ID left outer join ")
            sb.Append("OrgUser As US ON BP.HandlerID = US.ID INNER JOIN ")
            sb.Append("StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")
            sb.AppendFormat("WHERE (MB.DateTime BETWEEN '{0} 00:00:00.000' AND '{1} 23:59:59.999') ", dateFrom.ToString("yyyy/MM/dd"), dateTo.ToString("yyyy/MM/dd"))

            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt

        End Function

        Public Shared Function GetPayRecModify(ByVal ReceDate As String) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()

            sb.Append("SELECT * FROM PayRecModify ")
            sb.AppendFormat("WHERE ReceiptNum LIKE '{0}%'", ReceDate)

            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try
            Return dt

        End Function
    End Class
End Namespace
