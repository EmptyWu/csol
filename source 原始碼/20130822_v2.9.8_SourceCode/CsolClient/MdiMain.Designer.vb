﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mdiMain
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(mdiMain))
        Me.mnuStripMain = New System.Windows.Forms.MenuStrip()
        Me.mnuItemFile = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuChangePwd = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFindPrinter = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReLogin = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCardReader = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExit = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemBasics = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetClassroom = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetSchool = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemStudent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNewStudent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchStudent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTakePhoto = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSeatMapping = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchStuBySchool = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchStuByClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchStuByMulClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuInfo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPotentialStu = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuSiblings = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClassTransferRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuCardReIssue = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuClassRetainAnalysis = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuImportStudent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuNameRepeat = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemAttendance = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchBulletin = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchMakeupClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCheckSeatMap = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetClassContent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStudentHere = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuAbsent = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuShowUpRate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAddNewNote = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuNoteRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemBook = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBookIssuePunch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuMulBookIssuePunch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetBookToIssue = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCheckBookRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuWithoutBook = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemTeleInterview = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAddTeleInterview = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAddTeleInterviewMul = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuTeleInterviewType = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuTeleInterviewSearch = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuGrade = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSetClassPaper = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuGrade = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuGradeMul = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPostGrade = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAssignment = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAssignDone = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAssignDoneMul = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuSetAssign = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuDoneAssign = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuNotDoneAssign = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemAccounting = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaToday = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaByMonth = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaByYear = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaByClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaBySubClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayStaByDate = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuIndvSales = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuIndvFeeReceive = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuFeeOweSta = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuMBSta = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuStuMKSta = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpense = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpenseGrp = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpenseNew = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpenseJournalRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpenseJournalSta = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuExpenseRemarks = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchPayRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPayKeepRecByClass = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchFeeDiscount = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSearchChequeExpire = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReceiptModifyRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDiscModRec = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemAdvanced = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuUsrPwd = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchCheckFeeOwe = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPunchShowPhoto = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuThisClassName = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuReceiptFormat = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCustFieldName = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSchoolFieldName = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuItemVision = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsLabelUsrName = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripLblTime = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tabctrlMain = New System.Windows.Forms.TabControl()
        Me.timerClock = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker3 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker4 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker5 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker6 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker7 = New System.ComponentModel.BackgroundWorker()
        Me.BackgroundWorker8 = New System.ComponentModel.BackgroundWorker()
        Me.toolstrFile = New System.Windows.Forms.ToolStrip()
        Me.toolstrbutClear = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutRelogin = New System.Windows.Forms.ToolStripButton()
        Me.toolstrAcc = New System.Windows.Forms.ToolStrip()
        Me.toolstrbutPayStaToday = New System.Windows.Forms.ToolStripButton()
        Me.toolstrAttend = New System.Windows.Forms.ToolStrip()
        Me.toolstrbutStuHere = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutStuAbsent = New System.Windows.Forms.ToolStripButton()
        Me.toolstrClass = New System.Windows.Forms.ToolStrip()
        Me.toolstrbutPunchClass = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutPunchMakeup = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutBook = New System.Windows.Forms.ToolStripButton()
        Me.toolstrStu = New System.Windows.Forms.ToolStrip()
        Me.toolstrbutNewStu = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutSearchStu = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutClassSeat = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutClassStu = New System.Windows.Forms.ToolStripButton()
        Me.toolstrbutStuPhoto = New System.Windows.Forms.ToolStripButton()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.mnuStripMain.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.toolstrFile.SuspendLayout()
        Me.toolstrAcc.SuspendLayout()
        Me.toolstrAttend.SuspendLayout()
        Me.toolstrClass.SuspendLayout()
        Me.toolstrStu.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuStripMain
        '
        Me.mnuStripMain.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuItemFile, Me.mnuItemBasics, Me.mnuItemStudent, Me.mnuItemAttendance, Me.mnuItemBook, Me.mnuItemTeleInterview, Me.mnuGrade, Me.mnuAssignment, Me.mnuItemAccounting, Me.mnuItemAdvanced, Me.mnuItemVision})
        resources.ApplyResources(Me.mnuStripMain, "mnuStripMain")
        Me.mnuStripMain.Name = "mnuStripMain"
        '
        'mnuItemFile
        '
        Me.mnuItemFile.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuChangePwd, Me.mnuFindPrinter, Me.mnuReLogin, Me.mnuCardReader, Me.mnuExit})
        Me.mnuItemFile.Name = "mnuItemFile"
        resources.ApplyResources(Me.mnuItemFile, "mnuItemFile")
        '
        'mnuChangePwd
        '
        Me.mnuChangePwd.Name = "mnuChangePwd"
        resources.ApplyResources(Me.mnuChangePwd, "mnuChangePwd")
        '
        'mnuFindPrinter
        '
        Me.mnuFindPrinter.Name = "mnuFindPrinter"
        resources.ApplyResources(Me.mnuFindPrinter, "mnuFindPrinter")
        '
        'mnuReLogin
        '
        Me.mnuReLogin.Name = "mnuReLogin"
        resources.ApplyResources(Me.mnuReLogin, "mnuReLogin")
        '
        'mnuCardReader
        '
        Me.mnuCardReader.Name = "mnuCardReader"
        resources.ApplyResources(Me.mnuCardReader, "mnuCardReader")
        '
        'mnuExit
        '
        Me.mnuExit.Name = "mnuExit"
        resources.ApplyResources(Me.mnuExit, "mnuExit")
        '
        'mnuItemBasics
        '
        Me.mnuItemBasics.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetClassroom, Me.mnuSetClass, Me.mnuSetSchool})
        Me.mnuItemBasics.Name = "mnuItemBasics"
        resources.ApplyResources(Me.mnuItemBasics, "mnuItemBasics")
        '
        'mnuSetClassroom
        '
        Me.mnuSetClassroom.Name = "mnuSetClassroom"
        resources.ApplyResources(Me.mnuSetClassroom, "mnuSetClassroom")
        '
        'mnuSetClass
        '
        Me.mnuSetClass.Name = "mnuSetClass"
        resources.ApplyResources(Me.mnuSetClass, "mnuSetClass")
        '
        'mnuSetSchool
        '
        Me.mnuSetSchool.Name = "mnuSetSchool"
        resources.ApplyResources(Me.mnuSetSchool, "mnuSetSchool")
        '
        'mnuItemStudent
        '
        Me.mnuItemStudent.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNewStudent, Me.mnuSearchStudent, Me.mnuTakePhoto, Me.mnuSeatMapping, Me.mnuSearchStuBySchool, Me.mnuSearchStuByClass, Me.mnuSearchStuByMulClass, Me.mnuStuInfo, Me.mnuPotentialStu, Me.mnuStuSiblings, Me.mnuClassTransferRec, Me.mnuStuCardReIssue, Me.mnuClassRetainAnalysis, Me.mnuImportStudent, Me.mnuStuNameRepeat})
        Me.mnuItemStudent.Name = "mnuItemStudent"
        resources.ApplyResources(Me.mnuItemStudent, "mnuItemStudent")
        '
        'mnuNewStudent
        '
        Me.mnuNewStudent.Name = "mnuNewStudent"
        resources.ApplyResources(Me.mnuNewStudent, "mnuNewStudent")
        '
        'mnuSearchStudent
        '
        Me.mnuSearchStudent.Name = "mnuSearchStudent"
        resources.ApplyResources(Me.mnuSearchStudent, "mnuSearchStudent")
        '
        'mnuTakePhoto
        '
        Me.mnuTakePhoto.Name = "mnuTakePhoto"
        resources.ApplyResources(Me.mnuTakePhoto, "mnuTakePhoto")
        '
        'mnuSeatMapping
        '
        Me.mnuSeatMapping.Name = "mnuSeatMapping"
        resources.ApplyResources(Me.mnuSeatMapping, "mnuSeatMapping")
        '
        'mnuSearchStuBySchool
        '
        Me.mnuSearchStuBySchool.Name = "mnuSearchStuBySchool"
        resources.ApplyResources(Me.mnuSearchStuBySchool, "mnuSearchStuBySchool")
        '
        'mnuSearchStuByClass
        '
        Me.mnuSearchStuByClass.Name = "mnuSearchStuByClass"
        resources.ApplyResources(Me.mnuSearchStuByClass, "mnuSearchStuByClass")
        '
        'mnuSearchStuByMulClass
        '
        Me.mnuSearchStuByMulClass.Name = "mnuSearchStuByMulClass"
        resources.ApplyResources(Me.mnuSearchStuByMulClass, "mnuSearchStuByMulClass")
        '
        'mnuStuInfo
        '
        Me.mnuStuInfo.Name = "mnuStuInfo"
        resources.ApplyResources(Me.mnuStuInfo, "mnuStuInfo")
        '
        'mnuPotentialStu
        '
        Me.mnuPotentialStu.Name = "mnuPotentialStu"
        resources.ApplyResources(Me.mnuPotentialStu, "mnuPotentialStu")
        '
        'mnuStuSiblings
        '
        Me.mnuStuSiblings.Name = "mnuStuSiblings"
        resources.ApplyResources(Me.mnuStuSiblings, "mnuStuSiblings")
        '
        'mnuClassTransferRec
        '
        Me.mnuClassTransferRec.Name = "mnuClassTransferRec"
        resources.ApplyResources(Me.mnuClassTransferRec, "mnuClassTransferRec")
        '
        'mnuStuCardReIssue
        '
        Me.mnuStuCardReIssue.Name = "mnuStuCardReIssue"
        resources.ApplyResources(Me.mnuStuCardReIssue, "mnuStuCardReIssue")
        '
        'mnuClassRetainAnalysis
        '
        Me.mnuClassRetainAnalysis.Name = "mnuClassRetainAnalysis"
        resources.ApplyResources(Me.mnuClassRetainAnalysis, "mnuClassRetainAnalysis")
        '
        'mnuImportStudent
        '
        Me.mnuImportStudent.Name = "mnuImportStudent"
        resources.ApplyResources(Me.mnuImportStudent, "mnuImportStudent")
        '
        'mnuStuNameRepeat
        '
        Me.mnuStuNameRepeat.Name = "mnuStuNameRepeat"
        resources.ApplyResources(Me.mnuStuNameRepeat, "mnuStuNameRepeat")
        '
        'mnuItemAttendance
        '
        Me.mnuItemAttendance.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPunchClass, Me.mnuPunchBulletin, Me.mnuPunchMakeupClass, Me.mnuCheckSeatMap, Me.mnuSetClassContent, Me.mnuStudentHere, Me.mnuStuAbsent, Me.mnuShowUpRate, Me.mnuAddNewNote, Me.mnuNoteRec, Me.mnuPunchRec})
        Me.mnuItemAttendance.Name = "mnuItemAttendance"
        resources.ApplyResources(Me.mnuItemAttendance, "mnuItemAttendance")
        '
        'mnuPunchClass
        '
        Me.mnuPunchClass.Name = "mnuPunchClass"
        resources.ApplyResources(Me.mnuPunchClass, "mnuPunchClass")
        '
        'mnuPunchBulletin
        '
        Me.mnuPunchBulletin.Name = "mnuPunchBulletin"
        resources.ApplyResources(Me.mnuPunchBulletin, "mnuPunchBulletin")
        '
        'mnuPunchMakeupClass
        '
        Me.mnuPunchMakeupClass.Name = "mnuPunchMakeupClass"
        resources.ApplyResources(Me.mnuPunchMakeupClass, "mnuPunchMakeupClass")
        '
        'mnuCheckSeatMap
        '
        Me.mnuCheckSeatMap.Name = "mnuCheckSeatMap"
        resources.ApplyResources(Me.mnuCheckSeatMap, "mnuCheckSeatMap")
        '
        'mnuSetClassContent
        '
        Me.mnuSetClassContent.Name = "mnuSetClassContent"
        resources.ApplyResources(Me.mnuSetClassContent, "mnuSetClassContent")
        '
        'mnuStudentHere
        '
        Me.mnuStudentHere.Name = "mnuStudentHere"
        resources.ApplyResources(Me.mnuStudentHere, "mnuStudentHere")
        '
        'mnuStuAbsent
        '
        Me.mnuStuAbsent.Name = "mnuStuAbsent"
        resources.ApplyResources(Me.mnuStuAbsent, "mnuStuAbsent")
        '
        'mnuShowUpRate
        '
        Me.mnuShowUpRate.Name = "mnuShowUpRate"
        resources.ApplyResources(Me.mnuShowUpRate, "mnuShowUpRate")
        '
        'mnuAddNewNote
        '
        Me.mnuAddNewNote.Name = "mnuAddNewNote"
        resources.ApplyResources(Me.mnuAddNewNote, "mnuAddNewNote")
        '
        'mnuNoteRec
        '
        Me.mnuNoteRec.Name = "mnuNoteRec"
        resources.ApplyResources(Me.mnuNoteRec, "mnuNoteRec")
        '
        'mnuPunchRec
        '
        Me.mnuPunchRec.Name = "mnuPunchRec"
        resources.ApplyResources(Me.mnuPunchRec, "mnuPunchRec")
        '
        'mnuItemBook
        '
        Me.mnuItemBook.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBookIssuePunch, Me.mnuMulBookIssuePunch, Me.mnuSetBookToIssue, Me.mnuCheckBookRec, Me.mnuStuWithoutBook})
        Me.mnuItemBook.Name = "mnuItemBook"
        resources.ApplyResources(Me.mnuItemBook, "mnuItemBook")
        '
        'mnuBookIssuePunch
        '
        Me.mnuBookIssuePunch.Name = "mnuBookIssuePunch"
        resources.ApplyResources(Me.mnuBookIssuePunch, "mnuBookIssuePunch")
        '
        'mnuMulBookIssuePunch
        '
        Me.mnuMulBookIssuePunch.Name = "mnuMulBookIssuePunch"
        resources.ApplyResources(Me.mnuMulBookIssuePunch, "mnuMulBookIssuePunch")
        '
        'mnuSetBookToIssue
        '
        Me.mnuSetBookToIssue.Name = "mnuSetBookToIssue"
        resources.ApplyResources(Me.mnuSetBookToIssue, "mnuSetBookToIssue")
        '
        'mnuCheckBookRec
        '
        Me.mnuCheckBookRec.Name = "mnuCheckBookRec"
        resources.ApplyResources(Me.mnuCheckBookRec, "mnuCheckBookRec")
        '
        'mnuStuWithoutBook
        '
        Me.mnuStuWithoutBook.Name = "mnuStuWithoutBook"
        resources.ApplyResources(Me.mnuStuWithoutBook, "mnuStuWithoutBook")
        '
        'mnuItemTeleInterview
        '
        Me.mnuItemTeleInterview.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAddTeleInterview, Me.mnuAddTeleInterviewMul, Me.ToolStripSeparator1, Me.mnuTeleInterviewType, Me.mnuTeleInterviewSearch})
        Me.mnuItemTeleInterview.Name = "mnuItemTeleInterview"
        resources.ApplyResources(Me.mnuItemTeleInterview, "mnuItemTeleInterview")
        '
        'mnuAddTeleInterview
        '
        Me.mnuAddTeleInterview.Name = "mnuAddTeleInterview"
        resources.ApplyResources(Me.mnuAddTeleInterview, "mnuAddTeleInterview")
        '
        'mnuAddTeleInterviewMul
        '
        Me.mnuAddTeleInterviewMul.Name = "mnuAddTeleInterviewMul"
        resources.ApplyResources(Me.mnuAddTeleInterviewMul, "mnuAddTeleInterviewMul")
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        resources.ApplyResources(Me.ToolStripSeparator1, "ToolStripSeparator1")
        '
        'mnuTeleInterviewType
        '
        Me.mnuTeleInterviewType.Name = "mnuTeleInterviewType"
        resources.ApplyResources(Me.mnuTeleInterviewType, "mnuTeleInterviewType")
        '
        'mnuTeleInterviewSearch
        '
        Me.mnuTeleInterviewSearch.Name = "mnuTeleInterviewSearch"
        resources.ApplyResources(Me.mnuTeleInterviewSearch, "mnuTeleInterviewSearch")
        '
        'mnuGrade
        '
        Me.mnuGrade.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSetClassPaper, Me.mnuStuGrade, Me.mnuStuGradeMul, Me.mnuPostGrade})
        Me.mnuGrade.Name = "mnuGrade"
        resources.ApplyResources(Me.mnuGrade, "mnuGrade")
        '
        'mnuSetClassPaper
        '
        Me.mnuSetClassPaper.Name = "mnuSetClassPaper"
        resources.ApplyResources(Me.mnuSetClassPaper, "mnuSetClassPaper")
        '
        'mnuStuGrade
        '
        Me.mnuStuGrade.Name = "mnuStuGrade"
        resources.ApplyResources(Me.mnuStuGrade, "mnuStuGrade")
        '
        'mnuStuGradeMul
        '
        Me.mnuStuGradeMul.Name = "mnuStuGradeMul"
        resources.ApplyResources(Me.mnuStuGradeMul, "mnuStuGradeMul")
        '
        'mnuPostGrade
        '
        Me.mnuPostGrade.Name = "mnuPostGrade"
        resources.ApplyResources(Me.mnuPostGrade, "mnuPostGrade")
        '
        'mnuAssignment
        '
        Me.mnuAssignment.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAssignDone, Me.mnuAssignDoneMul, Me.ToolStripSeparator2, Me.mnuSetAssign, Me.mnuStuDoneAssign, Me.mnuStuNotDoneAssign})
        Me.mnuAssignment.Name = "mnuAssignment"
        resources.ApplyResources(Me.mnuAssignment, "mnuAssignment")
        '
        'mnuAssignDone
        '
        Me.mnuAssignDone.Name = "mnuAssignDone"
        resources.ApplyResources(Me.mnuAssignDone, "mnuAssignDone")
        '
        'mnuAssignDoneMul
        '
        Me.mnuAssignDoneMul.Name = "mnuAssignDoneMul"
        resources.ApplyResources(Me.mnuAssignDoneMul, "mnuAssignDoneMul")
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        resources.ApplyResources(Me.ToolStripSeparator2, "ToolStripSeparator2")
        '
        'mnuSetAssign
        '
        Me.mnuSetAssign.Name = "mnuSetAssign"
        resources.ApplyResources(Me.mnuSetAssign, "mnuSetAssign")
        '
        'mnuStuDoneAssign
        '
        Me.mnuStuDoneAssign.Name = "mnuStuDoneAssign"
        resources.ApplyResources(Me.mnuStuDoneAssign, "mnuStuDoneAssign")
        '
        'mnuStuNotDoneAssign
        '
        Me.mnuStuNotDoneAssign.Name = "mnuStuNotDoneAssign"
        resources.ApplyResources(Me.mnuStuNotDoneAssign, "mnuStuNotDoneAssign")
        '
        'mnuItemAccounting
        '
        Me.mnuItemAccounting.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPayStaToday, Me.mnuPayStaByMonth, Me.mnuPayStaByYear, Me.mnuPayStaByClass, Me.mnuPayStaBySubClass, Me.mnuPayStaByDate, Me.mnuIndvSales, Me.mnuIndvFeeReceive, Me.mnuFeeOweSta, Me.mnuStuMBSta, Me.mnuStuMKSta, Me.mnuExpense, Me.mnuSearchPayRec, Me.mnuPayKeepRecByClass, Me.mnuSearchFeeDiscount, Me.mnuSearchChequeExpire, Me.mnuReceiptModifyRec, Me.mnuDiscModRec})
        Me.mnuItemAccounting.Name = "mnuItemAccounting"
        resources.ApplyResources(Me.mnuItemAccounting, "mnuItemAccounting")
        '
        'mnuPayStaToday
        '
        Me.mnuPayStaToday.Name = "mnuPayStaToday"
        resources.ApplyResources(Me.mnuPayStaToday, "mnuPayStaToday")
        '
        'mnuPayStaByMonth
        '
        Me.mnuPayStaByMonth.Name = "mnuPayStaByMonth"
        resources.ApplyResources(Me.mnuPayStaByMonth, "mnuPayStaByMonth")
        '
        'mnuPayStaByYear
        '
        Me.mnuPayStaByYear.Name = "mnuPayStaByYear"
        resources.ApplyResources(Me.mnuPayStaByYear, "mnuPayStaByYear")
        '
        'mnuPayStaByClass
        '
        Me.mnuPayStaByClass.Name = "mnuPayStaByClass"
        resources.ApplyResources(Me.mnuPayStaByClass, "mnuPayStaByClass")
        '
        'mnuPayStaBySubClass
        '
        Me.mnuPayStaBySubClass.Name = "mnuPayStaBySubClass"
        resources.ApplyResources(Me.mnuPayStaBySubClass, "mnuPayStaBySubClass")
        '
        'mnuPayStaByDate
        '
        Me.mnuPayStaByDate.Name = "mnuPayStaByDate"
        resources.ApplyResources(Me.mnuPayStaByDate, "mnuPayStaByDate")
        '
        'mnuIndvSales
        '
        Me.mnuIndvSales.Name = "mnuIndvSales"
        resources.ApplyResources(Me.mnuIndvSales, "mnuIndvSales")
        '
        'mnuIndvFeeReceive
        '
        Me.mnuIndvFeeReceive.Name = "mnuIndvFeeReceive"
        resources.ApplyResources(Me.mnuIndvFeeReceive, "mnuIndvFeeReceive")
        '
        'mnuFeeOweSta
        '
        Me.mnuFeeOweSta.Name = "mnuFeeOweSta"
        resources.ApplyResources(Me.mnuFeeOweSta, "mnuFeeOweSta")
        '
        'mnuStuMBSta
        '
        Me.mnuStuMBSta.Name = "mnuStuMBSta"
        resources.ApplyResources(Me.mnuStuMBSta, "mnuStuMBSta")
        '
        'mnuStuMKSta
        '
        Me.mnuStuMKSta.Name = "mnuStuMKSta"
        resources.ApplyResources(Me.mnuStuMKSta, "mnuStuMKSta")
        '
        'mnuExpense
        '
        Me.mnuExpense.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuExpenseGrp, Me.mnuExpenseNew, Me.mnuExpenseJournalRec, Me.mnuExpenseJournalSta, Me.mnuExpenseRemarks})
        Me.mnuExpense.Name = "mnuExpense"
        resources.ApplyResources(Me.mnuExpense, "mnuExpense")
        '
        'mnuExpenseGrp
        '
        Me.mnuExpenseGrp.Name = "mnuExpenseGrp"
        resources.ApplyResources(Me.mnuExpenseGrp, "mnuExpenseGrp")
        '
        'mnuExpenseNew
        '
        Me.mnuExpenseNew.Name = "mnuExpenseNew"
        resources.ApplyResources(Me.mnuExpenseNew, "mnuExpenseNew")
        '
        'mnuExpenseJournalRec
        '
        Me.mnuExpenseJournalRec.Name = "mnuExpenseJournalRec"
        resources.ApplyResources(Me.mnuExpenseJournalRec, "mnuExpenseJournalRec")
        '
        'mnuExpenseJournalSta
        '
        Me.mnuExpenseJournalSta.Name = "mnuExpenseJournalSta"
        resources.ApplyResources(Me.mnuExpenseJournalSta, "mnuExpenseJournalSta")
        '
        'mnuExpenseRemarks
        '
        Me.mnuExpenseRemarks.Name = "mnuExpenseRemarks"
        resources.ApplyResources(Me.mnuExpenseRemarks, "mnuExpenseRemarks")
        '
        'mnuSearchPayRec
        '
        Me.mnuSearchPayRec.Name = "mnuSearchPayRec"
        resources.ApplyResources(Me.mnuSearchPayRec, "mnuSearchPayRec")
        '
        'mnuPayKeepRecByClass
        '
        Me.mnuPayKeepRecByClass.Name = "mnuPayKeepRecByClass"
        resources.ApplyResources(Me.mnuPayKeepRecByClass, "mnuPayKeepRecByClass")
        '
        'mnuSearchFeeDiscount
        '
        Me.mnuSearchFeeDiscount.Name = "mnuSearchFeeDiscount"
        resources.ApplyResources(Me.mnuSearchFeeDiscount, "mnuSearchFeeDiscount")
        '
        'mnuSearchChequeExpire
        '
        Me.mnuSearchChequeExpire.Name = "mnuSearchChequeExpire"
        resources.ApplyResources(Me.mnuSearchChequeExpire, "mnuSearchChequeExpire")
        '
        'mnuReceiptModifyRec
        '
        Me.mnuReceiptModifyRec.Name = "mnuReceiptModifyRec"
        resources.ApplyResources(Me.mnuReceiptModifyRec, "mnuReceiptModifyRec")
        '
        'mnuDiscModRec
        '
        Me.mnuDiscModRec.Name = "mnuDiscModRec"
        resources.ApplyResources(Me.mnuDiscModRec, "mnuDiscModRec")
        '
        'mnuItemAdvanced
        '
        Me.mnuItemAdvanced.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuUsrPwd, Me.mnuPunchCheckFeeOwe, Me.mnuPunchShowPhoto, Me.mnuThisClassName, Me.mnuReceiptFormat, Me.mnuCustFieldName, Me.mnuSchoolFieldName})
        Me.mnuItemAdvanced.Name = "mnuItemAdvanced"
        resources.ApplyResources(Me.mnuItemAdvanced, "mnuItemAdvanced")
        '
        'mnuUsrPwd
        '
        Me.mnuUsrPwd.Name = "mnuUsrPwd"
        resources.ApplyResources(Me.mnuUsrPwd, "mnuUsrPwd")
        '
        'mnuPunchCheckFeeOwe
        '
        Me.mnuPunchCheckFeeOwe.Name = "mnuPunchCheckFeeOwe"
        resources.ApplyResources(Me.mnuPunchCheckFeeOwe, "mnuPunchCheckFeeOwe")
        '
        'mnuPunchShowPhoto
        '
        Me.mnuPunchShowPhoto.Name = "mnuPunchShowPhoto"
        resources.ApplyResources(Me.mnuPunchShowPhoto, "mnuPunchShowPhoto")
        '
        'mnuThisClassName
        '
        Me.mnuThisClassName.Name = "mnuThisClassName"
        resources.ApplyResources(Me.mnuThisClassName, "mnuThisClassName")
        '
        'mnuReceiptFormat
        '
        Me.mnuReceiptFormat.Name = "mnuReceiptFormat"
        resources.ApplyResources(Me.mnuReceiptFormat, "mnuReceiptFormat")
        '
        'mnuCustFieldName
        '
        Me.mnuCustFieldName.Name = "mnuCustFieldName"
        resources.ApplyResources(Me.mnuCustFieldName, "mnuCustFieldName")
        '
        'mnuSchoolFieldName
        '
        Me.mnuSchoolFieldName.Name = "mnuSchoolFieldName"
        resources.ApplyResources(Me.mnuSchoolFieldName, "mnuSchoolFieldName")
        '
        'mnuItemVision
        '
        Me.mnuItemVision.Name = "mnuItemVision"
        resources.ApplyResources(Me.mnuItemVision, "mnuItemVision")
        '
        'StatusStrip1
        '
        Me.StatusStrip1.BackColor = System.Drawing.SystemColors.Control
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.tsLabelUsrName, Me.ToolStripStatusLabel2, Me.ToolStripLblTime})
        resources.ApplyResources(Me.StatusStrip1, "StatusStrip1")
        Me.StatusStrip1.Name = "StatusStrip1"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        resources.ApplyResources(Me.ToolStripStatusLabel1, "ToolStripStatusLabel1")
        '
        'tsLabelUsrName
        '
        Me.tsLabelUsrName.Name = "tsLabelUsrName"
        resources.ApplyResources(Me.tsLabelUsrName, "tsLabelUsrName")
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        resources.ApplyResources(Me.ToolStripStatusLabel2, "ToolStripStatusLabel2")
        Me.ToolStripStatusLabel2.Spring = True
        '
        'ToolStripLblTime
        '
        Me.ToolStripLblTime.Name = "ToolStripLblTime"
        resources.ApplyResources(Me.ToolStripLblTime, "ToolStripLblTime")
        '
        'tabctrlMain
        '
        resources.ApplyResources(Me.tabctrlMain, "tabctrlMain")
        Me.tabctrlMain.Multiline = True
        Me.tabctrlMain.Name = "tabctrlMain"
        Me.tabctrlMain.SelectedIndex = 0
        '
        'timerClock
        '
        Me.timerClock.Enabled = True
        Me.timerClock.Interval = 1000
        '
        'BackgroundWorker1
        '
        Me.BackgroundWorker1.WorkerReportsProgress = True
        Me.BackgroundWorker1.WorkerSupportsCancellation = True
        '
        'BackgroundWorker2
        '
        '
        'BackgroundWorker3
        '
        '
        'BackgroundWorker4
        '
        '
        'BackgroundWorker5
        '
        '
        'BackgroundWorker6
        '
        '
        'BackgroundWorker7
        '
        '
        'BackgroundWorker8
        '
        '
        'toolstrFile
        '
        resources.ApplyResources(Me.toolstrFile, "toolstrFile")
        Me.toolstrFile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolstrbutClear, Me.toolstrbutRelogin})
        Me.toolstrFile.Name = "toolstrFile"
        '
        'toolstrbutClear
        '
        Me.toolstrbutClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutClear.Image = Global.CsolClient.My.Resources.Resources.tool01clearScreen
        resources.ApplyResources(Me.toolstrbutClear, "toolstrbutClear")
        Me.toolstrbutClear.Name = "toolstrbutClear"
        '
        'toolstrbutRelogin
        '
        Me.toolstrbutRelogin.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutRelogin.Image = Global.CsolClient.My.Resources.Resources.tool02reLogin
        resources.ApplyResources(Me.toolstrbutRelogin, "toolstrbutRelogin")
        Me.toolstrbutRelogin.Name = "toolstrbutRelogin"
        '
        'toolstrAcc
        '
        Me.toolstrAcc.AllowDrop = True
        resources.ApplyResources(Me.toolstrAcc, "toolstrAcc")
        Me.toolstrAcc.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolstrbutPayStaToday})
        Me.toolstrAcc.Name = "toolstrAcc"
        '
        'toolstrbutPayStaToday
        '
        Me.toolstrbutPayStaToday.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutPayStaToday.Image = Global.CsolClient.My.Resources.Resources.tool13payStaToday
        resources.ApplyResources(Me.toolstrbutPayStaToday, "toolstrbutPayStaToday")
        Me.toolstrbutPayStaToday.Name = "toolstrbutPayStaToday"
        '
        'toolstrAttend
        '
        resources.ApplyResources(Me.toolstrAttend, "toolstrAttend")
        Me.toolstrAttend.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolstrbutStuHere, Me.toolstrbutStuAbsent})
        Me.toolstrAttend.Name = "toolstrAttend"
        '
        'toolstrbutStuHere
        '
        Me.toolstrbutStuHere.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutStuHere.Image = Global.CsolClient.My.Resources.Resources.tool08StuHere
        resources.ApplyResources(Me.toolstrbutStuHere, "toolstrbutStuHere")
        Me.toolstrbutStuHere.Name = "toolstrbutStuHere"
        '
        'toolstrbutStuAbsent
        '
        Me.toolstrbutStuAbsent.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutStuAbsent.Image = Global.CsolClient.My.Resources.Resources.tool09StuNotHere
        resources.ApplyResources(Me.toolstrbutStuAbsent, "toolstrbutStuAbsent")
        Me.toolstrbutStuAbsent.Name = "toolstrbutStuAbsent"
        '
        'toolstrClass
        '
        resources.ApplyResources(Me.toolstrClass, "toolstrClass")
        Me.toolstrClass.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolstrbutPunchClass, Me.toolstrbutPunchMakeup, Me.toolstrbutBook})
        Me.toolstrClass.Name = "toolstrClass"
        '
        'toolstrbutPunchClass
        '
        Me.toolstrbutPunchClass.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutPunchClass.Image = Global.CsolClient.My.Resources.Resources.tool10PunchClass
        resources.ApplyResources(Me.toolstrbutPunchClass, "toolstrbutPunchClass")
        Me.toolstrbutPunchClass.Name = "toolstrbutPunchClass"
        '
        'toolstrbutPunchMakeup
        '
        Me.toolstrbutPunchMakeup.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutPunchMakeup.Image = Global.CsolClient.My.Resources.Resources.tool11PunchMakeup
        resources.ApplyResources(Me.toolstrbutPunchMakeup, "toolstrbutPunchMakeup")
        Me.toolstrbutPunchMakeup.Name = "toolstrbutPunchMakeup"
        '
        'toolstrbutBook
        '
        Me.toolstrbutBook.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutBook.Image = Global.CsolClient.My.Resources.Resources.tool12BookIssue
        resources.ApplyResources(Me.toolstrbutBook, "toolstrbutBook")
        Me.toolstrbutBook.Name = "toolstrbutBook"
        '
        'toolstrStu
        '
        Me.toolstrStu.AllowDrop = True
        resources.ApplyResources(Me.toolstrStu, "toolstrStu")
        Me.toolstrStu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.toolstrbutNewStu, Me.toolstrbutSearchStu, Me.toolstrbutClassSeat, Me.toolstrbutClassStu, Me.toolstrbutStuPhoto})
        Me.toolstrStu.Name = "toolstrStu"
        '
        'toolstrbutNewStu
        '
        Me.toolstrbutNewStu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutNewStu.Image = Global.CsolClient.My.Resources.Resources.tool03NewStu
        resources.ApplyResources(Me.toolstrbutNewStu, "toolstrbutNewStu")
        Me.toolstrbutNewStu.Name = "toolstrbutNewStu"
        '
        'toolstrbutSearchStu
        '
        Me.toolstrbutSearchStu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutSearchStu.Image = Global.CsolClient.My.Resources.Resources.tool06SearchStu
        resources.ApplyResources(Me.toolstrbutSearchStu, "toolstrbutSearchStu")
        Me.toolstrbutSearchStu.Name = "toolstrbutSearchStu"
        '
        'toolstrbutClassSeat
        '
        Me.toolstrbutClassSeat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutClassSeat.Image = Global.CsolClient.My.Resources.Resources.tool04ClassSeat
        resources.ApplyResources(Me.toolstrbutClassSeat, "toolstrbutClassSeat")
        Me.toolstrbutClassSeat.Name = "toolstrbutClassSeat"
        '
        'toolstrbutClassStu
        '
        Me.toolstrbutClassStu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutClassStu.Image = Global.CsolClient.My.Resources.Resources.tool05ClassStu
        resources.ApplyResources(Me.toolstrbutClassStu, "toolstrbutClassStu")
        Me.toolstrbutClassStu.Name = "toolstrbutClassStu"
        '
        'toolstrbutStuPhoto
        '
        Me.toolstrbutStuPhoto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.toolstrbutStuPhoto.Image = Global.CsolClient.My.Resources.Resources.tool07StuPhoto
        resources.ApplyResources(Me.toolstrbutStuPhoto, "toolstrbutStuPhoto")
        Me.toolstrbutStuPhoto.Name = "toolstrbutStuPhoto"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.toolstrClass)
        Me.FlowLayoutPanel1.Controls.Add(Me.toolstrFile)
        Me.FlowLayoutPanel1.Controls.Add(Me.toolstrAttend)
        Me.FlowLayoutPanel1.Controls.Add(Me.toolstrAcc)
        Me.FlowLayoutPanel1.Controls.Add(Me.toolstrStu)
        resources.ApplyResources(Me.FlowLayoutPanel1, "FlowLayoutPanel1")
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        '
        'mdiMain
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.tabctrlMain)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.mnuStripMain)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.mnuStripMain
        Me.Name = "mdiMain"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnuStripMain.ResumeLayout(False)
        Me.mnuStripMain.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.toolstrFile.ResumeLayout(False)
        Me.toolstrFile.PerformLayout()
        Me.toolstrAcc.ResumeLayout(False)
        Me.toolstrAcc.PerformLayout()
        Me.toolstrAttend.ResumeLayout(False)
        Me.toolstrAttend.PerformLayout()
        Me.toolstrClass.ResumeLayout(False)
        Me.toolstrClass.PerformLayout()
        Me.toolstrStu.ResumeLayout(False)
        Me.toolstrStu.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuStripMain As System.Windows.Forms.MenuStrip
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents mnuItemFile As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemBasics As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemStudent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemAttendance As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemBook As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemAccounting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaToday As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaByMonth As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaByYear As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaByClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaBySubClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayStaByDate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuIndvSales As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuIndvFeeReceive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFeeOweSta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuMBSta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuMKSta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpense As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpenseGrp As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpenseNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuItemAdvanced As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpenseJournalRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpenseJournalSta As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExpenseRemarks As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchPayRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayKeepRecByClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchFeeDiscount As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchChequeExpire As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReceiptModifyRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuUsrPwd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchCheckFeeOwe As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchShowPhoto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuThisClassName As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReceiptFormat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCustFieldName As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSchoolFieldName As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuChangePwd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuFindPrinter As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuReLogin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExit As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetClassroom As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetSchool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNewStudent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchStudent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTakePhoto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSeatMapping As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchStuBySchool As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchStuByClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearchStuByMulClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPotentialStu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuSiblings As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClassTransferRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuCardReIssue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClassRetainAnalysis As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImportStudent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuNameRepeat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchBulletin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchMakeupClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCheckSeatMap As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetClassContent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStudentHere As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuAbsent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuShowUpRate As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAddNewNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNoteRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPunchRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBookIssuePunch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuMulBookIssuePunch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetBookToIssue As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCheckBookRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuWithoutBook As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabctrlMain As System.Windows.Forms.TabControl
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsLabelUsrName As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents mnuItemTeleInterview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAddTeleInterview As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAddTeleInterviewMul As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuTeleInterviewType As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTeleInterviewSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDiscModRec As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCardReader As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuGrade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetClassPaper As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuGrade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignment As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuGradeMul As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPostGrade As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignDone As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuAssignDoneMul As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuDoneAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuStuNotDoneAssign As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuItemVision As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents timerClock As System.Windows.Forms.Timer
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripLblTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker3 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker4 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker5 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker6 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker7 As System.ComponentModel.BackgroundWorker
    Friend WithEvents BackgroundWorker8 As System.ComponentModel.BackgroundWorker
    Friend WithEvents toolstrFile As System.Windows.Forms.ToolStrip
    Friend WithEvents toolstrbutClear As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutRelogin As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrAcc As System.Windows.Forms.ToolStrip
    Friend WithEvents toolstrbutPayStaToday As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrAttend As System.Windows.Forms.ToolStrip
    Friend WithEvents toolstrbutStuHere As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutStuAbsent As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrClass As System.Windows.Forms.ToolStrip
    Friend WithEvents toolstrbutPunchClass As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutPunchMakeup As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutBook As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrStu As System.Windows.Forms.ToolStrip
    Friend WithEvents toolstrbutNewStu As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutSearchStu As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutClassSeat As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutClassStu As System.Windows.Forms.ToolStripButton
    Friend WithEvents toolstrbutStuPhoto As System.Windows.Forms.ToolStripButton
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel

End Class
