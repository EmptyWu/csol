﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetAuthorityPwd
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetAuthorityPwd))
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPhoto = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.mnuResetPwd = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDeleteCard = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuDisable = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuEnable = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuIssueCard = New System.Windows.Forms.ToolStripMenuItem
        Me.chkboxShowDisable = New System.Windows.Forms.CheckBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuPhoto
        '
        Me.mnuPhoto.Name = "mnuPhoto"
        resources.ApplyResources(Me.mnuPhoto, "mnuPhoto")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'mnuResetPwd
        '
        Me.mnuResetPwd.Name = "mnuResetPwd"
        resources.ApplyResources(Me.mnuResetPwd, "mnuResetPwd")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        resources.ApplyResources(Me.mnuNew, "mnuNew")
        '
        'mnuDeleteCard
        '
        Me.mnuDeleteCard.Name = "mnuDeleteCard"
        resources.ApplyResources(Me.mnuDeleteCard, "mnuDeleteCard")
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuDetails, Me.mnuDisable, Me.mnuEnable, Me.mnuPhoto, Me.mnuIssueCard, Me.mnuDeleteCard, Me.mnuResetPwd, Me.mnuDelete, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuDisable
        '
        Me.mnuDisable.Name = "mnuDisable"
        resources.ApplyResources(Me.mnuDisable, "mnuDisable")
        '
        'mnuEnable
        '
        Me.mnuEnable.Name = "mnuEnable"
        resources.ApplyResources(Me.mnuEnable, "mnuEnable")
        '
        'mnuIssueCard
        '
        Me.mnuIssueCard.Name = "mnuIssueCard"
        resources.ApplyResources(Me.mnuIssueCard, "mnuIssueCard")
        '
        'chkboxShowDisable
        '
        resources.ApplyResources(Me.chkboxShowDisable, "chkboxShowDisable")
        Me.chkboxShowDisable.Name = "chkboxShowDisable"
        Me.chkboxShowDisable.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'frmSetAuthorityPwd
        '
        Me.AllowDrop = True
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.chkboxShowDisable)
        Me.Name = "frmSetAuthorityPwd"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPhoto As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents mnuResetPwd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDeleteCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDisable As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuEnable As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuIssueCard As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkboxShowDisable As System.Windows.Forms.CheckBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
