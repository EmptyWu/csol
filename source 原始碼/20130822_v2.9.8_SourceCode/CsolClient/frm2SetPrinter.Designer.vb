﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetPrinter
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lstbox = New System.Windows.Forms.ListBox
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butContent = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'lstbox
        '
        Me.lstbox.FormattingEnabled = True
        Me.lstbox.ItemHeight = 12
        Me.lstbox.Location = New System.Drawing.Point(12, 13)
        Me.lstbox.Name = "lstbox"
        Me.lstbox.Size = New System.Drawing.Size(285, 208)
        Me.lstbox.TabIndex = 0
        '
        'butConfirm
        '
        Me.butConfirm.Location = New System.Drawing.Point(308, 15)
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.Size = New System.Drawing.Size(82, 28)
        Me.butConfirm.TabIndex = 1
        Me.butConfirm.Text = "確定"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butContent
        '
        Me.butContent.Location = New System.Drawing.Point(308, 61)
        Me.butContent.Name = "butContent"
        Me.butContent.Size = New System.Drawing.Size(82, 28)
        Me.butContent.TabIndex = 2
        Me.butContent.Text = "內容"
        Me.butContent.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.Location = New System.Drawing.Point(308, 193)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 28)
        Me.butCancel.TabIndex = 3
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2SetPrinter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(399, 238)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butContent)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.lstbox)
        Me.Name = "frm2SetPrinter"
        Me.Text = "設定印表機"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lstbox As System.Windows.Forms.ListBox
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butContent As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
