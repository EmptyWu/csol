﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiscountSearch
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmDiscountSearch))
        Me.dgvRec = New System.Windows.Forms.DataGridView
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tboxId = New System.Windows.Forms.TextBox
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.Label2 = New System.Windows.Forms.Label
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvRec
        '
        Me.dgvRec.AllowUserToAddRows = False
        Me.dgvRec.AllowUserToDeleteRows = False
        Me.dgvRec.AllowUserToResizeRows = False
        Me.dgvRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvRec, "dgvRec")
        Me.dgvRec.MultiSelect = False
        Me.dgvRec.Name = "dgvRec"
        Me.dgvRec.ReadOnly = True
        Me.dgvRec.RowHeadersVisible = False
        Me.dgvRec.RowTemplate.Height = 15
        Me.dgvRec.RowTemplate.ReadOnly = True
        Me.dgvRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRec.ShowCellToolTips = False
        Me.dgvRec.ShowEditingIcon = False
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'tboxId
        '
        resources.ApplyResources(Me.tboxId, "tboxId")
        Me.tboxId.Name = "tboxId"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuSelectCol, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'frmDiscountSearch
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dgvRec)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tboxId)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmDiscountSearch"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvRec As System.Windows.Forms.DataGridView
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tboxId As System.Windows.Forms.TextBox
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
