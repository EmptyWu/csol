﻿Public Class frm2NoteDisplay
    Dim bytDisStyle As Byte
    Dim intTime As Integer
    Dim intCount As Integer = 0

    'dis = 0, full screen; dis = 1, msgbox
    Public Sub New(ByVal dis As Byte, ByVal t As Integer, ByVal c As String, _
                   ByVal name As String)
        InitializeComponent()

        bytDisStyle = dis
        intTime = t
        lblStuName.Text = name
        lblNote.Text = c

    End Sub

    Private Sub frm2NoteDisplay_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If bytDisStyle = 0 Then
            Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        End If
        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        intCount = intCount + 1
        If intCount > intTime Then
            Me.Close()
        End If
    End Sub
End Class