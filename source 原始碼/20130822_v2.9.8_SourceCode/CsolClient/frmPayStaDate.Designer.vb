﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayStaDate
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayStaDate))
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tbox6 = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.tbox5 = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.tbox4 = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tbox3 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbox2 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbox1 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbox0 = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dgvMaster = New System.Windows.Forms.DataGridView
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.tboxTotal = New System.Windows.Forms.TextBox
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label3 = New System.Windows.Forms.Label
        Me.butListSta = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.radbutByDate = New System.Windows.Forms.RadioButton
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.radbutThisMonth = New System.Windows.Forms.RadioButton
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tbox6)
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.tbox5)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.tbox4)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.tbox3)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.tbox2)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.tbox1)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.tbox0)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.dgvMaster)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'tbox6
        '
        resources.ApplyResources(Me.tbox6, "tbox6")
        Me.tbox6.Name = "tbox6"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'tbox5
        '
        resources.ApplyResources(Me.tbox5, "tbox5")
        Me.tbox5.Name = "tbox5"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'tbox4
        '
        resources.ApplyResources(Me.tbox4, "tbox4")
        Me.tbox4.Name = "tbox4"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tbox3
        '
        resources.ApplyResources(Me.tbox3, "tbox3")
        Me.tbox3.Name = "tbox3"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tbox2
        '
        resources.ApplyResources(Me.tbox2, "tbox2")
        Me.tbox2.Name = "tbox2"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tbox1
        '
        resources.ApplyResources(Me.tbox1, "tbox1")
        Me.tbox1.Name = "tbox1"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tbox0
        '
        resources.ApplyResources(Me.tbox0, "tbox0")
        Me.tbox0.Name = "tbox0"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvMaster, "dgvMaster")
        Me.dgvMaster.MultiSelect = False
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowTemplate.Height = 15
        Me.dgvMaster.RowTemplate.ReadOnly = True
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.ShowCellToolTips = False
        Me.dgvMaster.ShowEditingIcon = False
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'tboxTotal
        '
        resources.ApplyResources(Me.tboxTotal, "tboxTotal")
        Me.tboxTotal.Name = "tboxTotal"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'radbutByDate
        '
        resources.ApplyResources(Me.radbutByDate, "radbutByDate")
        Me.radbutByDate.Name = "radbutByDate"
        Me.radbutByDate.UseVisualStyleBackColor = True
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'radbutThisMonth
        '
        resources.ApplyResources(Me.radbutThisMonth, "radbutThisMonth")
        Me.radbutThisMonth.Checked = True
        Me.radbutThisMonth.Name = "radbutThisMonth"
        Me.radbutThisMonth.TabStop = True
        Me.radbutThisMonth.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuExportAll, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        resources.ApplyResources(Me.mnuExportAll, "mnuExportAll")
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'PrintDocument1
        '
        '
        'frmPayStaDate
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.tboxTotal)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radbutByDate)
        Me.Controls.Add(Me.radbutThisMonth)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmPayStaDate"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tbox6 As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents tbox5 As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbox4 As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbox3 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbox2 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tbox1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbox0 As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxTotal As System.Windows.Forms.TextBox
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents radbutByDate As System.Windows.Forms.RadioButton
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radbutThisMonth As System.Windows.Forms.RadioButton
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
