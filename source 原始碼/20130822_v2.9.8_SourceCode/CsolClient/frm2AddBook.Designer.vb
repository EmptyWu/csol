﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddBook
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2AddBook))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.radbutNoClearDate = New System.Windows.Forms.RadioButton
        Me.radbutClear = New System.Windows.Forms.RadioButton
        Me.Label9 = New System.Windows.Forms.Label
        Me.dtPickClear = New System.Windows.Forms.DateTimePicker
        Me.chkboxClearFee = New System.Windows.Forms.CheckBox
        Me.tboxPrice = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxStock = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.dtpickReg = New System.Windows.Forms.DateTimePicker
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.dtpickBefore = New System.Windows.Forms.DateTimePicker
        Me.radbutMulti = New System.Windows.Forms.RadioButton
        Me.radbutMultiNo = New System.Windows.Forms.RadioButton
        Me.butDelAll = New System.Windows.Forms.Button
        Me.chkboxRegDate = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.butDel = New System.Windows.Forms.Button
        Me.butAdd = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radbutNoClearDate)
        Me.GroupBox1.Controls.Add(Me.radbutClear)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.dtPickClear)
        Me.GroupBox1.Controls.Add(Me.chkboxClearFee)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'radbutNoClearDate
        '
        resources.ApplyResources(Me.radbutNoClearDate, "radbutNoClearDate")
        Me.radbutNoClearDate.Checked = True
        Me.radbutNoClearDate.Name = "radbutNoClearDate"
        Me.radbutNoClearDate.TabStop = True
        Me.radbutNoClearDate.UseVisualStyleBackColor = True
        '
        'radbutClear
        '
        resources.ApplyResources(Me.radbutClear, "radbutClear")
        Me.radbutClear.Name = "radbutClear"
        Me.radbutClear.UseVisualStyleBackColor = True
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'dtPickClear
        '
        resources.ApplyResources(Me.dtPickClear, "dtPickClear")
        Me.dtPickClear.Name = "dtPickClear"
        '
        'chkboxClearFee
        '
        resources.ApplyResources(Me.chkboxClearFee, "chkboxClearFee")
        Me.chkboxClearFee.Name = "chkboxClearFee"
        Me.chkboxClearFee.UseVisualStyleBackColor = True
        '
        'tboxPrice
        '
        resources.ApplyResources(Me.tboxPrice, "tboxPrice")
        Me.tboxPrice.Name = "tboxPrice"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxStock
        '
        resources.ApplyResources(Me.tboxStock, "tboxStock")
        Me.tboxStock.Name = "tboxStock"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'dtpickReg
        '
        resources.ApplyResources(Me.dtpickReg, "dtpickReg")
        Me.dtpickReg.Name = "dtpickReg"
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'dtpickBefore
        '
        resources.ApplyResources(Me.dtpickBefore, "dtpickBefore")
        Me.dtpickBefore.Name = "dtpickBefore"
        '
        'radbutMulti
        '
        resources.ApplyResources(Me.radbutMulti, "radbutMulti")
        Me.radbutMulti.Checked = True
        Me.radbutMulti.Name = "radbutMulti"
        Me.radbutMulti.TabStop = True
        Me.radbutMulti.UseVisualStyleBackColor = True
        '
        'radbutMultiNo
        '
        resources.ApplyResources(Me.radbutMultiNo, "radbutMultiNo")
        Me.radbutMultiNo.Name = "radbutMultiNo"
        Me.radbutMultiNo.UseVisualStyleBackColor = True
        '
        'butDelAll
        '
        resources.ApplyResources(Me.butDelAll, "butDelAll")
        Me.butDelAll.Name = "butDelAll"
        Me.butDelAll.UseVisualStyleBackColor = True
        '
        'chkboxRegDate
        '
        resources.ApplyResources(Me.chkboxRegDate, "chkboxRegDate")
        Me.chkboxRegDate.Name = "chkboxRegDate"
        Me.chkboxRegDate.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.radbutMulti)
        Me.GroupBox5.Controls.Add(Me.butDelAll)
        Me.GroupBox5.Controls.Add(Me.radbutMultiNo)
        Me.GroupBox5.Controls.Add(Me.butDel)
        Me.GroupBox5.Controls.Add(Me.butAdd)
        Me.GroupBox5.Controls.Add(Me.dgv)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'butDel
        '
        resources.ApplyResources(Me.butDel, "butDel")
        Me.butDel.Name = "butDel"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butAdd
        '
        resources.ApplyResources(Me.butAdd, "butAdd")
        Me.butAdd.Name = "butAdd"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'tboxName
        '
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.dtpickReg)
        Me.GroupBox2.Controls.Add(Me.chkboxRegDate)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'frm2AddBook
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.tboxPrice)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxStock)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.dtpickBefore)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.GroupBox2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddBook"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutNoClearDate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutClear As System.Windows.Forms.RadioButton
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents dtPickClear As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkboxClearFee As System.Windows.Forms.CheckBox
    Friend WithEvents tboxPrice As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxStock As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dtpickReg As System.Windows.Forms.DateTimePicker
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents dtpickBefore As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutMulti As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMultiNo As System.Windows.Forms.RadioButton
    Friend WithEvents butDelAll As System.Windows.Forms.Button
    Friend WithEvents chkboxRegDate As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents butDel As System.Windows.Forms.Button
    Friend WithEvents butAdd As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
End Class
