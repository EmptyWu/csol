﻿Namespace HTTPHandlers
    Public Class AssignDone
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetClassAssignmentList"
                    GetClassAssignmentList()
                Case "GetStuAssignByAssignId"
                    GetStuAssignByAssignId()
                    
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/AssignDone"
            End Get
        End Property

        Private Sub GetClassAssignmentList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim classID As New ArrayList

            classID.AddRange(RequestParams("classID").Split(","))
            Dim GetClassAssignmentList As DataTable = DataBaseAccess.AssignDone.GetClassAssignmentList(classID)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetClassAssignmentList", CSOL.Convert.DataTableToXmlString(GetClassAssignmentList))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetClassAssignmentList", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetStuAssignByAssignId()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim assignID As Integer = RequestParams("assignmentId")
            Dim sc As Integer = RequestParams("sc")
            Dim GetStuAssignByAssignId As DataTable = DataBaseAccess.AssignDone.GetStuAssignByAssignId(assignID, sc)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetStuAssignByAssignId", CSOL.Convert.DataTableToXmlString(GetStuAssignByAssignId))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetStuAssignByAssignId", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

    End Class
End Namespace

