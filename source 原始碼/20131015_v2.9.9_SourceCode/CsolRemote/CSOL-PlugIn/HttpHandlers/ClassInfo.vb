﻿Namespace HTTPHandlers
    Public Class ClassInfo
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetBookListByUsr"
                    GetBookListByUsr()
                Case "GetBookListByUsr2"
                    GetBookListByUsr2()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/ClassInfo"
            End Get
        End Property

        Private Sub GetBookListByUsr()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim ClassName As String = RequestParams("ClassName")

            Dim BookList As DataTable = DataBaseAccess.ClassInfo.GetBookListByUsr(ClassName)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("BookList", CSOL.Convert.DataTableToXmlString(BookList))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            'Me.m_Context.Response.StatusCode = StatusCode
            'Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
            '    sw.Write(ResponseBody)
            'End Using

            Me.m_Context.Response.StatusCode = StatusCode
            'Me.m_Context.Response.ContentType = "application/octet-stream"
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("BookList", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)


        End Sub


        Private Sub GetBookListByUsr2()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim ClassName As String = RequestParams("ClassName")

            Dim BookList As DataTable = DataBaseAccess.ClassInfo.GetBookListByUsr2(ClassName)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("BookList", CSOL.Convert.DataTableToXmlString(BookList))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)


            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("BookList", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)


        End Sub
    End Class
End Namespace

