﻿Namespace DataBaseAccess
    Public Class StuOweSta
        Public Shared Function ListStuOweByClass(ByVal ClassID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrclassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrclassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrclassid)

            Dim sq As New System.Text.StringBuilder()
            sq.Append("select pa.amount, pa.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("'0' as discount, sc.classid, pa.subclassid, tc.seatnum, '0' as backamount, ")
            sq.Append("'0' as keepamount, us2.name as salesperson, si.schoolgrade, pa.datetime, ")
            sq.Append("pa.paymethod, us1.name as handler, pa.extra, '' as discountremarks, ")
            sq.Append("pa.remarks, si.engname, pa.id, pa.receiptnum, pa.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from payrec as pa inner join ")
            sq.Append("subclass as sc on pa.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on pa.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on pa.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on pa.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (pa.stuid = tc.stuid and ")
            sq.Append("pa.subclassid = tc.subclassid) ")

            sq.AppendFormat("where (sc.classid in ({0})) ", str)
            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("mb.amount as backamount, '0' as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from moneybackpayrec as mp inner join ")
            sq.Append("moneybackclassinfo as mb on mp.mbclassinfoid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.classid in ({0})) ", str)

            sq.Append("union all ")
            sq.Append("select mp.amount, mb.stuid, si.name as stuname, sc.name as subclassname, ")
            sq.Append("mb.discount, sc.classid, mb.subclassid, tc.seatnum, ")
            sq.Append("'0' as backamount, mb.amount as keepamount, us2.name as salesperson, ")
            sq.Append("si.schoolgrade, mp.datetime, ")
            sq.Append("mp.paymethod, us1.name as handler, mp.extra, mb.discountremarks, ")
            sq.Append("mp.remarks, si.engname, mp.id, mp.receiptnum, mp.handlerid, ")
            sq.Append("si.birthday, si.dadname, si.mumname, si.tel1, si.tel2, si.dadmobile, ")
            sq.Append("si.mummobile, si.mobile, si.school, si.schoolclass, si.graduatefrom, ")
            sq.Append("si.introid, si.introname, si.ic, si.email, si.createdate, si.primarysch, si.juniorsch, si.highsch, si.university, si.sex, ")
            sq.Append("si.currentsch, si.schgroup ")
            sq.Append("from keeppayrec as mp inner join ")
            sq.Append("keeppaysubclassrec as mb on mp.kpclassrecid = mb.id inner join ")
            sq.Append("subclass as sc on mb.subclassid = sc.id inner join ")
            sq.Append("class as cl on sc.classid = cl.id inner join ")
            sq.Append("stuinfo as si on mb.stuid = si.id left outer join ")
            sq.Append("orguser as us1 on mb.handlerid = us1.id left outer join ")
            sq.Append("orguser as us2 on mb.salesid = us2.id inner join ")
            sq.Append("stuclass as tc on (mb.stuid = tc.stuid and ")
            sq.Append("mb.subclassid = tc.subclassid) ")
            sq.AppendFormat("where (sc.classid in ({0})) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function ListStuDiscByClass(ByVal ClassID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrClassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT distinct FD.Discount, FD.Remarks, FD.SubClassID, FD.StuID ")
            sq.Append("FROM FeeDiscount AS FD ")
            sq.Append("INNER JOIN SubClass AS SC on FD.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE SC.ClassID in ({0})", str)
            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function


        Public Shared Function ListStuBySubClass(ByVal subclassid As ArrayList) As DataTable


            Dim dt As New DataTable
            Dim arrClassid(subclassid.Count - 1) As String
            For i As Integer = 0 To subclassid.Count - 1
                arrClassid(i) = subclassid(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)

            Dim sq As New System.Text.StringBuilder()

            sq.Append("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode, ")
            sq.Append("SI.Address2, Cancel, SI.IC, ")
            sq.Append("PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email, SI.CardNum,StuType, School, ")
            sq.Append("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University, ")
            sq.Append("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sq.Append("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks,SI.Customize1 AS ComeInfo, ")
            sq.Append("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark, ")
            sq.Append("SC.SubClassID, SU.ClassID, SU.Name AS SubClassName, SC.SeatNum, OU.Name AS SalesPerson, ")
            sq.Append("SC.FeeOwe,SC.RegisterDate,OU.Name AS SalesName ")
            sq.Append("FROM StuInfo AS SI LEFT OUTER JOIN ")
            sq.Append("StuClass AS SC ON SC.StuID = SI.ID INNER JOIN ")
            sq.Append("SubClass AS SU ON SC.SubClassID = SU.ID LEFT OUTER JOIN ")
            sq.Append("OrgUser AS OU ON SC.SalesID = OU.ID ")
           
            sq.AppendFormat("WHERE SC.SubClassID in ({0}) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

    End Class

End Namespace



