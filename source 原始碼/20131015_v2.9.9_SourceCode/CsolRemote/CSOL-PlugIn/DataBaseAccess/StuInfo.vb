﻿Namespace DataBaseAccess
    Public Class StuInfo
        Public Shared Function SaveItem(ByVal StuID As Integer, ByVal No As String, ByVal Name As String, ByVal EngName As String, _
                                        ByVal PID As String, ByVal Type As String, ByVal Sex As String, ByVal Birth As Date, ByVal School As String, _
                                        ByVal Phone As String, ByVal Mobile As String, ByVal Address As String, _
                                        ByVal SalesGroup As String, ByVal SalesManager As String, ByVal Sales As String, ByVal Comment As String, _
                                        ByVal HandlerID As String, ByVal WorkStationID As String) As Integer

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @No VARCHAR(50), @Name NVARCHAR(50), @EngName VARCHAR(50),")
            sb.AppendLine("    @PID VARCHAR(50), @Type NVARCHAR(50), @Sex VARCHAR(50), @Birth DATETIME, @School NVARCHAR(MAX),")
            sb.AppendLine("    @Phone VARCHAR(50), @Mobile VARCHAR(50), @Address NVARCHAR(400),")
            sb.AppendLine("    @SalesGroup NVARCHAR(50), @SalesManager NVARCHAR(50), @Sales NVARCHAR(50), @Comment NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @StuID = {0}, @No = '{1}', @Name = N'{2}', @EngName = '{3}'," & vbCrLf, StuID, No, Name, EngName)
            sb.AppendFormat("    @PID = '{0}', @Type = N'{1}', @Sex = '{2}', @Birth = '{3}', @School = N'{4}'," & vbCrLf, PID, Type, Sex, Birth.ToString("yyyy/MM/dd"), School)
            sb.AppendFormat("    @Phone = '{0}', @Mobile = '{1}', @Address = N'{2}'," & vbCrLf, Phone, Mobile, Address)
            sb.AppendFormat("    @SalesGroup = N'{0}', @SalesManager = N'{1}', @Sales = N'{2}', @Comment = N'{3}'," & vbCrLf, SalesGroup, SalesManager, Sales, Comment)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            If StuID = -1 Then
                sb.AppendLine("INSERT INTO StuInfo2(No, Name, EngName, PID, Type, Sex, Birth, School, Phone, Mobile, Address, SalesGroup, SalesManager, Sales, Comment, Removed, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("VALUES(@No, @Name, @EngName, @PID, @Type, @Sex, @Birth, @School, @Phone, @Mobile, @Address, @SalesGroup, @SalesManager, @Sales, @Comment, 0, @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
                sb.AppendLine("SELECT @StuID = @@Identity")
            Else
                sb.AppendLine("UPDATE StuInfo2 SET No = @No, Name = @Name, EngName = @EngName, PID = @PID, Type = @Type, Sex = @Sex, Birth = @Birth, School = @School, Phone = @Phone, Mobile = @Mobile, Address = @Address, SalesGroup = @SalesGroup, SalesManager = @SalesManager, Sales = @Sales, Comment = @Comment, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department")
                sb.AppendLine("WHERE ID = @StuID")
            End If
            sb.AppendLine("SELECT @StuID AS StuID")

            Dim sql As String = sb.ToString()


            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    StuID = dt.Rows(0)("StuID")
                End If
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return StuID
        End Function

        Public Shared Function RemoveStu(ByVal StuID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT,")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @StuID = {0}," & vbCrLf, StuID)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE StuInfo2 SET Removed = 1, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @StuID")

            Dim sql As String = sb.ToString()

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetNextStuNo() As String
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT No FROM StuInfo2 WHERE Removed = 0 ORDER BY CreateDate DESC")

            Dim sql As String = sb.ToString()

            Dim StuNo As String = String.Format("{0:000}00001", Now.Year - 1911)
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    StuNo = dt.Rows(0)("No")
                    Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(StuNo, "\d+$")
                    If matches.Count = 0 Then
                        Dim i As Integer = 1
                        Do
                            StuNo &= i.ToString("d5")
                            i += 1
                        Loop While IsStuNoExists(StuNo)
                    Else
                        Do
                            Dim str As String = System.Text.RegularExpressions.Regex.Matches(StuNo, "\d+$")(0).Value
                            Dim digit As Integer = Integer.Parse(str) + 1
                            Dim format As String = String.Format("d{0}", str.Length)
                            StuNo = StuNo.Replace(str, digit.ToString(format))
                        Loop While IsStuNoExists(StuNo)
                    End If
                End If
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return StuNo
        End Function

        Public Shared Function IsStuNoExists(ByVal StuNo As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @No VARCHAR(50)")
            sb.AppendFormat("SELECT @No = '{0}'", StuNo)

            sb.AppendLine("SELECT COUNT(1) FROM StuInfo2 WHERE No = @No AND Removed = 0")

            Dim sql As String = sb.ToString()


            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)(0) > 0
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
                Return True
            End Try
        End Function

        Public Shared Function GetStuCount(ByVal filter As String) As Integer
            If filter.Trim() <> "" Then
                filter = String.Format(" WHERE {0} AND Removed = 0", filter)
            Else
                filter = " WHERE Removed = 0"
            End If

            Dim sb As New System.Text.StringBuilder()
            sb.AppendFormat("SELECT COUNT(1) AS [Count] FROM StuInfo2{0}" & vbCrLf, filter)

            Dim sql As String = sb.ToString()


            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)("Count")
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
                Return -1
            End Try
        End Function

        Public Shared Function GetStu(ByVal filter As String, ByVal page As Integer, ByVal rows As Integer) As DataTable
            Dim sort As String = "ModifyDate DESC"
            Return GetStu(filter, sort, page, rows)
        End Function

        Public Shared Function GetStu(ByVal filter As String, ByVal sort As String, ByVal page As Integer, ByVal rows As Integer) As DataTable
            If filter.Trim() <> "" Then
                filter = String.Format(" WHERE {0} AND Removed = 0", filter)
            Else
                filter = " WHERE Removed = 0"
            End If

            Dim startIndex As Integer = (page - 1) * rows + 1
            Dim endIndex As Integer = page * rows
            Dim sb As New System.Text.StringBuilder()
            sb.AppendFormat("WITH si AS (SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS RowID, * FROM StuInfo2{1})" & vbCrLf, sort, filter)
            sb.AppendFormat("SELECT RowID, ID, No, Name, PID, Type, Sex, Birth, School, Phone, Mobile, Address, Comment, SalesGroup, SalesManager, Sales FROM si WHERE RowID BETWEEN {0} AND {1}" & vbCrLf, startIndex, endIndex)

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("Stu")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetStuPayRec(ByVal filter As String) As DataTable

            Dim sb As New System.Text.StringBuilder
            sb.AppendLine("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode,")
            sb.AppendLine("SI.Address2, Cancel, SI.IC, ")
            sb.AppendLine(" PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email, SI.CardNum,StuType, School, ")
            sb.AppendLine("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University,")
            sb.AppendLine("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sb.AppendLine("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks,SI.Customize1 AS ComeInfo,")
            sb.AppendLine("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark,")
            sb.AppendLine("SC.SubClassID, SU.ClassID, SU.Name AS SubClassName, SC.SeatNum, OU.Name AS SalesPerson,")
            sb.AppendLine("SC.FeeOwe,SC.RegisterDate,OU.Name AS SalesName")
            sb.AppendLine("FROM StuInfo AS SI LEFT OUTER JOIN")
            sb.AppendLine("StuClass AS SC ON SC.StuID = SI.ID INNER JOIN")
            sb.AppendLine("SubClass AS SU ON SC.SubClassID = SU.ID LEFT OUTER JOIN")
            sb.AppendLine("OrgUser AS OU ON SC.SalesID = OU.ID")
            sb.AppendFormat("WHERE (SC.SubClassID = '{0}')", filter)

            Dim sql As String = sb.ToString

            Dim dt As New DataTable("Stu")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetStuInfo(ByVal StuID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT")
            sb.AppendFormat("SELECT @StuID = {0}" & vbCrLf, StuID)
            sb.AppendLine("SELECT * FROM StuInfo2 WHERE ID = @StuID")

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("Stu")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Stu"
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetAddressBook(ByVal StuID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT")
            sb.AppendFormat("SELECT @StuID = {0}" & vbCrLf, StuID)
            sb.AppendLine("SELECT ID, StuID, Title, Name, Job, Phone, Mobile, Address, Comment FROM AddressBook WHERE StuID = @StuID AND Removed = 0")

            Dim sql As String = sb.ToString()


            Dim dt As New DataTable("AddressBook")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "AddressBook"
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function AddAddressBook(ByVal StuID As Integer, ByVal Title As String, ByVal Name As String, ByVal Job As String, _
                                      ByVal Phone As String, ByVal Mobile As String, ByVal Address As String, ByVal Comment As String) As Integer

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @Title VARCHAR(50), @Name NVARCHAR(50), @Job NVARCHAR(50),")
            sb.AppendLine("    @Phone VARCHAR(50), @Mobile VARCHAR(50), @Address NVARCHAR(400), @Comment VARCHAR(MAX)")

            sb.AppendFormat("SELECT @StuID = {0}, @Title = '{1}', @Name = N'{2}', @Job = '{3}'," & vbCrLf, StuID, Title, Name, Job)
            sb.AppendFormat("    @Phone = '{0}', @Mobile = '{1}', @Address = N'{2}', @Comment = '{3}'" & vbCrLf, Phone, Mobile, Address, Comment)

            sb.AppendLine("INSERT INTO AddressBook(StuID, Title, Name, Job, Phone, Mobile, Address, Comment, Removed)")
            sb.AppendLine("VALUES(@StuID, @Title, @Name, @Job, @Phone, @Mobile, @Address, @Comment, 0)")

            sb.AppendLine("SELECT @@Identity AS AddressBookID")

            Dim sql As String = sb.ToString()

            Dim AddressBookID As Integer = -1
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    AddressBookID = dt.Rows(0)("AddressBookID")
                End If
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return AddressBookID
        End Function

        Public Shared Function RemoveAddressBook(ByVal AddressBookID As Integer) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @AddressBookID INT")
            sb.AppendFormat("SELECT @AddressBookID = {0}" & vbCrLf, AddressBookID)

            sb.AppendLine("UPDATE AddressBook SET Removed = 1 WHERE ID = @AddressBookID")

            Dim sql As String = sb.ToString()


            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetStuBySubClass(ByVal subclassid As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(subclassid.Count - 1) As String
            For i As Integer = 0 To subclassid.Count - 1
                arrClassid(i) = subclassid(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)

            Dim sq As New System.Text.StringBuilder()

            sq.Append("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode, ")
            sq.Append("SI.Address2, Cancel, SI.IC, ")
            sq.Append("PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email, SI.CardNum,StuType, School, ")
            sq.Append("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University, ")
            sq.Append("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sq.Append("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks,SI.Customize1 AS ComeInfo, ")
            sq.Append("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark,")
            sq.Append("SC.SubClassID, SU.ClassID, SU.Name AS SubClassName, SC.SeatNum, OU.Name AS SalesPerson, ")
            sq.Append("SC.FeeOwe,SC.RegisterDate,OU.Name AS SalesName ")
            sq.Append("FROM StuInfo AS SI LEFT OUTER JOIN ")
            sq.Append("StuClass AS SC ON SC.StuID = SI.ID INNER JOIN ")
            sq.Append("SubClass AS SU ON SC.SubClassID = SU.ID LEFT OUTER JOIN ")
            sq.Append("OrgUser AS OU ON SC.SalesID = OU.ID ")
            sq.AppendFormat("WHERE SC.SubClassID in ({0}) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetStuSibList() As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT ST.StuID, ST.Rank, SS.Appellation, ST.Name, ST.Birthday, ST.School,  ")
            sq.Append("ST.Remarks, SC.SubClassID, SU.ClassID,SU.Name AS SubClassName, ")
            sq.Append("CL.Name AS ClassName,SI.StuType, SI.Name AS StuName,SI.SchoolGrade, ")
            sq.Append("SI.CurrentSch , SI.EngName, SI.Birthday as StuBirthday, ")
            sq.Append("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, ")
            sq.Append("SI.Mobile, SI.School as StuSchool, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName,SI.IC, SI.Email, SI.PrimarySch, SI.JuniorSch, ")
            sq.Append("SI.HighSch, SI.University, SI.Remarks as StuRemarks ")
            sq.Append("FROM StuSibling AS ST  ")
            sq.Append("INNER JOIN StuInfo AS SI ON ST.StuID = SI.ID ")
            sq.Append("LEFT OUTER JOIN StuClass AS SC ON SI.ID = SC.StuID ")
            sq.Append("INNER JOIN SubClass AS SU ON SC.SubClassID = SU.ID ")
            sq.Append("INNER JOIN StuSibType AS SS ON ST.SibType = SS.ID ")
            sq.Append("INNER JOIN Class AS CL ON SU.ClassID = CL.ID ")

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetStuSibList(ByVal lstfilter As List(Of String)) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("Select * from ( ")
            sq.Append("SELECT ST.StuID, ST.Rank, SS.Appellation, ST.Name, ST.Birthday, ST.School,  ")
            sq.Append("ST.Remarks, SC.SubClassID, SU.ClassID,SU.Name AS SubClassName, ")
            sq.Append("CL.Name AS ClassName,SI.StuType, SI.Name AS StuName,SI.SchoolGrade, ")
            sq.Append("SI.CurrentSch , SI.EngName, SI.Birthday as StuBirthday, ")
            sq.Append("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, ")
            sq.Append("SI.Mobile, SI.School as StuSchool, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName,SI.IC, SI.Email, SI.PrimarySch, SI.JuniorSch, ")
            sq.Append("SI.HighSch, SI.University, SI.Remarks as StuRemarks ")
            sq.Append("FROM StuSibling AS ST  ")
            sq.Append("INNER JOIN StuInfo AS SI ON ST.StuID = SI.ID ")
            sq.Append("LEFT OUTER JOIN StuClass AS SC ON SI.ID = SC.StuID ")
            sq.Append("INNER JOIN SubClass AS SU ON SC.SubClassID = SU.ID ")
            sq.Append("INNER JOIN StuSibType AS SS ON ST.SibType = SS.ID ")
            sq.Append("INNER JOIN Class AS CL ON SU.ClassID = CL.ID ")
            sq.Append(") ta")

            sq.Append(" Where " & String.Join("And ", lstfilter.ToArray))

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function
        Public Shared Function GetStuByClass(ByVal subclassid As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(subclassid.Count - 1) As String
            For i As Integer = 0 To subclassid.Count - 1
                arrClassid(i) = subclassid(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)

            Dim sq As New System.Text.StringBuilder()

            sq.Append("SELECT SI.ID, SI.[Name], SI.EngName, SI.Sex, SI.Birthday, SI.Address, SI.PostalCode, SI.Address2, ")
            sq.Append("PostalCode2, Tel1, Tel2, OfficeTel, SI.Mobile, SI.Email,SI.CardNum, StuType, School, ")
            sq.Append("SchoolClass, SchoolGrade,  SC.Cancel, PrimarySch, JuniorSch, HighSch, University, ")
            sq.Append("CurrentSch, SchGroup, GraduateFrom, DadName, MumName, DadJob, MumJob, ")
            sq.Append("DadMobile, MumMobile, IntroID, IntroName, CreateDate, SI.Remarks, ")
            sq.Append("SC.SubClassID, SU.Name as SubClassName, SC.SeatNum,SI.Remarks,SI.Customize1 AS ComeInfo, ")
            sq.Append("SI.Customize2 AS ClassNum,SI.Customize3 AS Accommodation,SI.EnrollSch,SI.PunchCardNum,SI.HseeMark, ")
            sq.Append("SC.RegisterDate,OU.Name AS SalesName ")
            sq.Append("FROM StuInfo AS SI INNER JOIN ")
            sq.Append("StuClass AS SC ON SI.ID = SC.StuID INNER JOIN ")
            sq.Append("SubClass AS SU ON SU.ID = SC.SubClassID LEFT JOIN ")
            sq.Append("OrgUser AS OU ON OU.ID = SC.SalesID ")
            sq.AppendFormat("WHERE SU.ClassID in ({0}) ", str)

            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Shared Function GetStuPaperByStuId(ByVal stuid As String) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT SI.ID AS StuID,SI.Name AS StuName,SC.Name AS SubClassName,EP.Name AS PaperName,ISNULL(SG.Mark,-1) AS Mark,")
            sb.AppendLine("EP.DateTime AS DateTime,CL.[End] AS [End]") '-----2011/1/27
            sb.AppendLine("FROM StuInfo SI")
            sb.AppendLine("INNER JOIN StuClass STC ON STC.StuID = SI.ID")
            sb.AppendLine("INNER JOIN PaperOfClass	AS PC ON PC.SubClassID = STC.SubClassID")
            sb.AppendLine("INNER JOIN SubClass SC ON SC.ID = STC.SubClassID")
            sb.AppendLine("INNER JOIN Class AS CL ON CL.ID = SC.ClassID")
            sb.AppendLine("INNER JOIN ExamPaper AS EP ON EP.ID = PC.PaperID")
            sb.AppendLine("LEFT JOIN StuGrade AS SG ON SG.StuID = SI.ID AND SG.PaperID = PC.PaperID")
            sb.AppendLine(String.Format("WHERE SI.ID = '{0}'", stuid))
            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

        Public Shared Function GetStuAssignByStuId(ByVal Stuid As String) As DataTable
            Dim dt As New DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT SI.ID AS StuID,SI.Name AS StuName,SC.Name AS SubClassName,AG.Name, ")
            sb.AppendLine("(SELECT COUNT(ID) FROM StuAssignment WHERE StuID=SI.ID AND AssignmentID = AC.AssignmentID) AS Mark, ")
            sb.AppendLine("(SELECT DateTime FROM StuAssignment WHERE StuID=SI.ID AND AssignmentID = AC.AssignmentID) AS DateTime, ")
            sb.AppendLine("CS.[End] AS [End]")
            sb.AppendLine("FROM StuInfo AS SI ")
            sb.AppendLine("INNER JOIN StuClass AS ST ON ST.StuID = SI.ID ")
            sb.AppendLine("INNER JOIN SubClass AS SC ON SC.ID = ST.SubClassID ")
            sb.AppendLine("INNER JOIN AssignmentOfClass AS AC ON AC.SubClassID = ST.SubClassID ")
            sb.AppendLine("INNER JOIN Assignment AS AG ON AG.ID = AC.AssignmentID ")
            sb.AppendLine("INNER JOIN ClassSession AS CS ON CS.SubClassID = ST.SubClassID")
            sb.AppendLine(String.Format("WHERE SI.ID = '{0}' ", Stuid))
            Dim sql As String = sb.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function
    End Class
End Namespace