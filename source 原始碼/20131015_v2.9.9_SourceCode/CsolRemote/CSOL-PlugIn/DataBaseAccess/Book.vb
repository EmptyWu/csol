﻿Namespace DataBaseAccess
    Public Class Book
        Public Shared Function GetBookMulit(ByVal bookid As Integer) As Boolean
            Dim result As New Boolean
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.AppendFormat("SELECT * FROM BOOK WHERE ID = {0} ", bookid)
            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            If dt.Rows.Count > 0 Then
                If dt.Rows(0).Item("MultiClass") = 1 Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
                'Throw New Exception("There IS NO Book")
            End If
        End Function

        Public Shared Function GetBookRecByUsr(ByVal ClassId As Integer, ByVal BookId As Integer) As DataTable
            Dim dt As New DataTable
            Dim multiclass As Boolean = GetBookMulit(BookId)
            Dim sq As New System.Text.StringBuilder()

            If multiclass Then
                sq.AppendLine("SELECT BK.ID, BK.Name, BI.ClassID, BI.StuID, BI.DateTime, ")
                sq.AppendLine("ST.SubClassID, SI.Name AS StuName, SC.Name AS SubClassName, BK.Stock,  ")
                sq.AppendLine("SI.SchoolGrade, SI.EngName, SI.Birthday, SI.Remarks, ")
                sq.AppendLine("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile,  ")
                sq.AppendLine("SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName,  ")
                sq.AppendLine("SI.IC, SI.Email, ST.Cancel, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University,  ")
                sq.AppendLine("SI.Sex, SI.DadJob, SI.MumJob, SI.CreateDate,  ")
                sq.AppendLine("SI.CurrentSch, SI.SchGroup, SI.Address, SI.PostalCode, SI.Address2, SI.PostalCode2,  ")
                sq.AppendLine("SI.OfficeTel, BI.ID as RecID ,CL.Name as ClassName")
                sq.AppendLine("FROM BookIssueRec AS BI inner join ")
                sq.AppendLine("	Book AS BK on BI.BookID = BK.ID INNER JOIN ")
                sq.AppendLine("SubClass AS SC ON SC.ClassID = BI.ClassID inner join ")
                sq.AppendLine("StuInfo as SI on BI.StuID = SI.ID inner join ")
                sq.AppendLine("StuClass AS ST on ST.StuID = SI.ID AND ST.SubClassID = SC.ID ")
                sq.AppendLine("inner join Class as CL on SC.ClassID = CL.ID")
                sq.AppendFormat("WHERE BK.ID = {0} ", BookId)

            Else
                sq.AppendLine("SELECT BK.ID, BK.Name, BI.ClassID, BI.StuID, BI.DateTime, ")
                sq.AppendLine("ST.SubClassID, SI.Name AS StuName, SC.Name AS SubClassName, BK.Stock,  ")
                sq.AppendLine("SI.SchoolGrade, SI.EngName, SI.Birthday, SI.Remarks, ")
                sq.AppendLine("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile,  ")
                sq.AppendLine("SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName,  ")
                sq.AppendLine("SI.IC, SI.Email, ST.Cancel, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University,  ")
                sq.AppendLine("SI.Sex, SI.DadJob, SI.MumJob, SI.CreateDate,  ")
                sq.AppendLine("SI.CurrentSch, SI.SchGroup, SI.Address, SI.PostalCode, SI.Address2, SI.PostalCode2,  ")
                sq.AppendLine("SI.OfficeTel, BI.ID as RecID ,CL.Name as ClassName")
                sq.AppendLine("FROM BookIssueRec AS BI inner join ")
                sq.AppendLine("	Book AS BK on BI.BookID = BK.ID INNER JOIN ")
                sq.AppendLine("SubClass AS SC ON SC.ClassID = BI.ClassID inner join ")
                sq.AppendLine("StuInfo as SI on BI.StuID = SI.ID inner join ")
                sq.AppendLine("StuClass AS ST on ST.StuID = SI.ID AND ST.SubClassID = SC.ID ")
                sq.AppendLine("inner join Class as CL on SC.ClassID = CL.ID")
                sq.AppendFormat("WHERE BI.ClassID = {0} And BK.ID = {1} ", classid, BookId)
            End If
            Dim sql As String = sq.ToString()

            Try
                dt = CSOL.DataBase.LoadToDataTable(Sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try

            Return dt
        End Function

        Public Sub New()

        End Sub
    End Class
End Namespace
