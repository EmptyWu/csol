﻿Namespace DataBaseAccess
    Public Class StuGrade
        Public Shared Function GetClassPaperList(ByVal ClassID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrClassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT DISTINCT  EP.ID, EP.Name, PC.SubClassID ")
            sq.Append("FROM ExamPaper AS EP ")
            sq.Append("INNER JOIN PaperOfClass AS PC ON EP.ID = PC.PaperID ")
            sq.Append("inner join SubClass as SC on PC.SubClassID = SC.ID ")
            sq.AppendFormat("WHERE SC.ClassID = '{0}' ", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

        Public Shared Function GetStuGrades_sql(ByVal PaperID As Integer, ByVal sc As Integer) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT DISTINCT	SI.ID, SI.Name, SI.EngName, ST.SeatNum, SI.PrimarySch, SI.JuniorSch, ")
            sq.Append("SI.HighSch, SI.University, SG.Mark, SG.Remarks, SG.DateTime, ")
            sq.Append("EP.ID AS PaperID, EP.Name AS PaperName, EP.DateTime AS PaperDate, ST.SubClassID, ")
            sq.Append("SC.Name as SubClassName, ")
            sq.Append("OU.Name AS Handler, SI.School, ST.Cancel, SI.Sex, SI.Birthday, SI.Address, ")
            sq.Append("SI.PostalCode, SI.Address2, SI.PostalCode2, SI.Tel1, SI.Tel2, SI.OfficeTel, ")
            sq.Append("SI.Mobile, SI.Email, SI.SchoolClass, SI.SchoolGrade, SI.CurrentSch, ")
            sq.Append("SI.GraduateFrom, SI.DadName, SI.MumName, SI.DadJob, SI.MumJob, ")
            sq.Append("SI.DadMobile, SI.MumMobile, SI.IntroID, SI.IntroName, SI.IC, SI.CreateDate, ")
            sq.Append("ST.RegisterDate, SI.CardNum, SI.Customize3, SI.SchGroup, ")
            sq.Append("SI.GraduateFrom AS Expr1, OU2.Name AS SalesPerson, SI.Customize2,  ")
            sq.Append("SI.Customize1, SI.EnrollSch, SI.PunchCardNum, SI.HseeMark, SH.Name As CurrentSch ")
            sq.Append("FROM StuClass AS ST ")
            sq.Append("INNER JOIN SubClass As SC on ST.SubClassID = SC.ID  ")
            sq.Append("inner join StuInfo AS SI ON ST.StuID = SI.ID ")
            sq.AppendFormat("LEFT OUTER JOIN StuGrade AS SG ON ST.StuID = SG.StuID AND  SG.PaperID = '{0}' ", PaperID)
            sq.Append("LEFT OUTER JOIN ExamPaper AS EP ON SG.PaperID = EP.ID ")
            sq.Append("LEFT OUTER JOIN OrgUser AS OU ON SG.InputBy = OU.ID ")
            sq.Append("LEFT OUTER JOIN OrgUser AS OU2 ON ST.SalesID = OU2.ID ")
            sq.Append("left outer join School as SH on SI.CurrentSch = SH.ID ")
            sq.AppendFormat("left outer join SubClassContent As SCC on SCC.SubClassID = '{0}'  ", sc)
            sq.Append("left outer join ClassContent as CC on SCC.ContentID = CC.ID ")
            sq.AppendFormat("WHERE ST.SubClassID = '{0}'   ", sc)
            sq.Append("ORDER BY  SG.Mark DESC ")

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

    End Class

End Namespace
