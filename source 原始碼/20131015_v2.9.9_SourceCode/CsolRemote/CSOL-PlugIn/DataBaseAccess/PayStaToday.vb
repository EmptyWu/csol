﻿Namespace DataBaseAccess
    Public Class PayStaToday
        Public Shared Function GetClassAssignmentList(ByVal ClassID As ArrayList) As DataTable
            Dim dt As New DataTable
            Dim arrClassid(ClassID.Count - 1) As String
            For i As Integer = 0 To ClassID.Count - 1
                arrClassid(i) = ClassID(i).ToString
            Next
            Dim str As String = String.Join(",", arrClassid)
            Dim sq As New System.Text.StringBuilder()
            'sq.Append("SELECT DISTINCT  EP.ID, EP.Name, PC.SubClassID, EP.EndDate ")
            'sq.Append("FROM             Assignment AS EP INNER JOIN ")
            'sq.Append("AssignmentOfClass AS PC ON EP.ID = PC.AssignmentID inner join ")
            'sq.Append("SubClass as SC on PC.SubClassID = SC.ID ")
            'sq.AppendFormat("WHERE            SC.ClassID = '{0}' ", str)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

        Public Shared Function GetStuAssignByAssignId(ByVal assignID As Integer, ByVal sc As Integer) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            'sq.Append("SELECT DISTINCT  SI.ID, SI.Name, ST.SeatNum, SC.Name as SubClassName, ")
            'sq.Append("(SELECT COUNT(ID) FROM StuAssignment ")
            'sq.Append("WHERE(AssignmentID = SG.AssignmentID) and StuID = SI.ID) AS Mark, ")
            'sq.Append("SG.AssignmentID, ST.SubClassID, SI.School,si.EngName,si.Tel1,si.Tel2, ")
            'sq.Append("si.Email,si.GraduateFrom,si.DadName,si.MumName,si.DadMobile,si.MumMobile, ")
            'sq.Append("SI.IntroID,SI.IntroName,si.SchoolGrade,si.SchoolClass , SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            'sq.Append("FROM StuClass As ST  ")
            'sq.Append("inner join SubClass As SC on ST.SubClassID = SC.ID ")
            'sq.Append("inner join StuInfo as SI on ST.StuID = SI.ID ")
            'sq.Append("left outer join	StuAssignment as SG on ST.StuID = ST.StuID  ")

            'sq.AppendFormat("AND SG.AssignmentID =  '{0}' ", assignID)
            'sq.AppendFormat("WHERE ST.SubClassID =  '{0}'  ", sc)
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

        Public Shared Function GetPayStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, ")
            sq.Append("SI.SchoolGrade, PA.DateTime AS PayDateTime, PA.PayMethod, PA.Amount, ")
            sq.Append("US1.Name AS Handler, US2.Name AS SalesPerson, PA.Extra, ")
            sq.Append("PA.Remarks, SC.ClassID,CL.Name AS ClassName, PA.SubClassID, SI.EngName, SI.Birthday, ")
            sq.Append("SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, ")
            sq.Append("SI.Mobile, SI.School, SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, ")
            sq.Append("SI.IC, SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University ")
            sq.Append("FROM PayRec AS PA ")
            sq.Append("INNER JOIN SubClass AS SC ON PA.SubClassID = SC.ID ")
            sq.Append("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
            sq.Append("INNER JOIN StuInfo AS SI ON PA.StuID = SI.ID ")
            sq.Append("left outer join OrgUser AS US1 ON PA.HandlerID = US1.ID ")
            sq.Append("left outer join OrgUser AS US2 ON PA.SalesID = US2.ID ")
            sq.Append("INNER JOIN StuClass AS TC ON PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID ")
            sq.AppendFormat("WHERE (PA.DateTime BETWEEN '{0}' AND '{1}')  ", dateFrom.ToString("yyyy/MM/dd 00:00:00.000"), dateTo.ToString("yyyy/MM/dd 23:59:59.999"))
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function

        Public Shared Function GetMKeepStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
            sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name As Handler, ")
            sq.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount, ")
            sq.Append("SC.ClassID, CL.Name AS ClassName, MB.SubClassID, SI.EngName, ")
            sq.Append("SI.Birthday, SI.DadName, SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, ")
            sq.Append("SI.MumMobile, SI.SchoolGrade, SI.SchoolClass, SI.GraduateFrom, ")
            sq.Append("SI.IntroID, SI.IntroName, SI.IC, SI.Email, TC.RegisterDate, ")
            sq.Append("SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
            sq.Append("FROM KeepPaySubClassRec AS MB ")
            sq.Append("INNER JOIN KeepPayRec AS BP ON MB.ID = BP.KPClassRecID ")
            sq.Append("INNER JOIN SubClass AS SC ON MB.SubClassID = SC.ID ")
            sq.Append("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
            sq.Append("INNER JOIN StuInfo AS SI ON MB.StuID = SI.ID ")
            sq.Append("left outer join OrgUser As US ON BP.HandlerID = US.ID ")
            sq.Append("INNER JOIN StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")
            
            sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') ", dateFrom.ToString("yyyy/MM/dd 00:00:00.000"), dateTo.ToString("yyyy/MM/dd 23:59:59.999"))

            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function


        Public Shared Function GetMBackStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim dt As New DataTable
            Dim sq As New System.Text.StringBuilder()
            sq.Append("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.Mobile, SI.School, ")
            sq.Append("BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount As PayAmount, BP.Extra, US.Name AS Handler, ")
            sq.Append("BP.Remarks, SC.Name AS SubClassName, MB.Amount AS Amount, SC.ClassID, ")
            sq.Append("CL.Name AS ClassName, MB.SubClassID, SI.EngName, SI.Birthday, SI.DadName, ")
            sq.Append("SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, ")
            sq.Append("SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, ")
            sq.Append("SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime ")
            sq.Append("FROM MoneyBackClassInfo AS MB ")
            sq.Append("INNER JOIN MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID ")
            sq.Append("INNER JOIN SubClass AS SC ON MB.SubClassID = SC.ID ")
            sq.Append("INNER JOIN Class AS CL ON SC.ClassID = CL.ID ")
            sq.Append("INNER JOIN StuInfo AS SI ON MB.StuID = SI.ID ")
            sq.Append("left outer join OrgUser AS US ON BP.HandlerID = US.ID ")
            sq.Append("INNER JOIN StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) ")

            sq.AppendFormat("WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') OR (BP.DateTime BETWEEN '{0}' AND '{1}') ", dateFrom.ToString("yyyy/MM/dd 00:00:00.000"), dateTo.ToString("yyyy/MM/dd 23:59:59.999"))
            
            Dim sql As String = sq.ToString()
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
            Return dt
        End Function
    End Class
End Namespace
