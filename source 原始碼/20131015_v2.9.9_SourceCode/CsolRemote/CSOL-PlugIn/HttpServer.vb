﻿Imports System.Net
Imports System.Net.Sockets

Public Class HttpServer
    Private m_Server As HttpListener
    Private m_Started As Boolean = False
    Private m_Handlers As New List(Of IHttpHandler)
    Private m_HandlerPaths As New List(Of String)

    Public Sub RegisterHandler(ByVal handler As Object)
        If Not Me.m_Handlers.Contains(handler) Then
            If Not Me.m_HandlerPaths.Contains(CType(handler, IHttpHandler).VirtualPath) Then
                Me.m_Handlers.Add(handler)
                Me.m_HandlerPaths.Add(CType(handler, IHttpHandler).VirtualPath)
            End If
        End If
    End Sub

    Public Sub New(ByVal prefixes() As String)
        Start(prefixes)
    End Sub

    Private Sub Start(ByVal prefixes() As String)
        If m_Started Then
            Return
        End If

        If m_Server Is Nothing Then
            m_Server = New HttpListener()
        End If

        For Each prefix As String In prefixes
            Me.m_Server.Prefixes.Add(prefix)
        Next
        m_Started = True
        m_Server.Start()

        Dim result As IAsyncResult = m_Server.BeginGetContext(New AsyncCallback(AddressOf WebRequestCallback), m_Server)
    End Sub

    Private Sub [Stop]()
        If m_Server IsNot Nothing Then
            m_Server.Stop()
            m_Server = Nothing
            m_Started = False
        End If
    End Sub

    Private Sub WebRequestCallback(ByVal result As IAsyncResult)
        If m_Server Is Nothing Then
            Return
        End If

        Dim context As HttpListenerContext = m_Server.EndGetContext(result)
        m_Server.BeginGetContext(New AsyncCallback(AddressOf WebRequestCallback), m_Server)
        ProcessRequest(context)
    End Sub

    Private Sub ProcessRequest(ByVal context As HttpListenerContext)
        context.Response.ContentType = "text/html; charset=utf-8"

        Dim handled As Boolean = False

        If context.Request.Url.AbsolutePath.StartsWith("/www") Then
            Dim path As String = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, System.Web.HttpUtility.UrlDecode(context.Request.Url.AbsolutePath.Replace("/", "\")))
            If System.IO.File.Exists(path) Then
                If path.EndsWith(".html") Or path.EndsWith(".htm") Then
                Else
                    context.Response.ContentType = "application/octet-stream"
                    Dim filename As String = System.IO.Path.GetFileName(path)
                    context.Response.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", System.Web.HttpUtility.UrlEncode(context.Request.ContentEncoding.GetBytes(filename))))
                End If


                Using fs As New System.IO.FileStream(path, IO.FileMode.Open, IO.FileAccess.Read)
                    context.Response.ContentLength64 = fs.Length
                    Try
                        Dim buffer(&H1000) As Byte
                        Dim dataToRead As Integer = fs.Length
                        Dim length As Integer = 0
                        While dataToRead > 0
                            length = fs.Read(buffer, 0, buffer.Length)
                            context.Response.OutputStream.Write(buffer, 0, length)
                            context.Response.OutputStream.Flush()

                            ReDim buffer(&H1000)
                            dataToRead -= length
                        End While
                    Catch ex As Exception
                        CSOL.Logger.LogError(ex.Message)
                    End Try
                End Using

                handled = True
            End If
        Else
            Try
                Dim path As String = context.Request.Url.AbsolutePath

                For Each handler As IHttpHandler In Me.m_Handlers
                    If path.EndsWith(handler.VirtualPath) Then
                        handler.Handle(context)
                        handled = True
                        Exit For
                    End If
                Next
            Catch ex As Exception
                context.Response.StatusCode = System.Net.HttpStatusCode.InternalServerError
                context.Response.Close()
                CSOL.Logger.LogError(ex.Message)
            End Try
        End If

        If handled = False Then
            Try
                context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                context.Response.Close()
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
        Else
            Try
                If context.Response.OutputStream.CanWrite Then
                    context.Response.OutputStream.Close()
                End If
            Catch ex As Exception
                CSOL.Logger.LogError(ex.Message)
            End Try
        End If
    End Sub

    Public Shared Function ParseQuery(ByVal query As String) As System.Collections.Specialized.NameValueCollection
        Dim nv As New System.Collections.Specialized.NameValueCollection()

        If query.StartsWith("?") Then
            query = query.Remove(0, 1)
        End If

        If query.Trim() = "" Then
            Return nv
        End If

        Dim params() As String = query.Split("&")
        For Each param As String In params
            Dim pair() As String = param.Split("=")
            If pair.Length = 1 Then
                nv.Add(System.Web.HttpUtility.UrlDecode(pair(0)), "")
            Else
                nv.Add(System.Web.HttpUtility.UrlDecode(pair(0)), System.Web.HttpUtility.UrlDecode(String.Join("=", pair, 1, pair.Length - 1)))
            End If
        Next
        Return nv
    End Function

    Public Shared Function GetPostBody(ByVal request As System.Net.HttpListenerRequest) As String
        Dim result As String = ""
        If request.InputStream.CanRead Then
            result = New System.IO.StreamReader(request.InputStream).ReadToEnd()
        End If
        Return result
    End Function

    Public Shared Function GetAction(ByVal query As System.Collections.Specialized.NameValueCollection) As String
        If Array.IndexOf(query.AllKeys, "act") = -1 Then
            Return ""
        Else
            Return query("act")
        End If
    End Function
End Class

Public Class TestHandler
    Implements IHttpHandler

    Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
        Dim input As String = HttpServer.GetPostBody(Context.Request)
        Dim response As String = String.Format("<h1>Method:{0}</h1>", Context.Request.HttpMethod)
        response &= String.Format("<h2>Body:{0}</h2>", System.Web.HttpUtility.UrlDecode(input))
        response &= String.Format("<h2>Query:{0}<h2>", System.Web.HttpUtility.UrlDecode(Context.Request.Url.Query))
        Dim sw As New System.IO.StreamWriter(Context.Response.OutputStream)
        sw.Write(response)
        sw.Close()
    End Sub

    Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
        Get
            Return "/test"
        End Get
    End Property
End Class
