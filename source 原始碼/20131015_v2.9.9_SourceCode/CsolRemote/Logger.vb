﻿Namespace CSOL
    Public Class Logger
        Private Shared m_Path As String
        Public Shared Property Path() As String
            Get
                Return My.Application.Info.DirectoryPath 'm_Path
            End Get
            Set(ByVal value As String)
                m_Path = value
            End Set
        End Property

        Public Shared Sub Log(ByVal Message As String)
            Log("", Message)
        End Sub

        Public Shared Sub Log(ByVal Prefix As String, ByVal Message As String)
            If System.IO.Directory.Exists(Path) Then
                Dim file As String = String.Format("{0}\Log\{1}.log", Path, Now.ToString("yyyyMMdd"))
                Dim content As String = String.Format("{0}{1}{2}{3}", Now.ToString("HH:mm:ss.fff"), vbTab, Message, vbCrLf)
                If Prefix <> "" Then
                    file = String.Format("{0}\Log\{1}-{2}.log", Path, Prefix, Now.ToString("yyyyMMdd"))
                    EnsureFolder(file)
                End If

                Try
                    System.IO.File.AppendAllText(file, content)
                Catch ex As Exception
                    CSOL.Logger.LogError(ex.Message)
                End Try
            End If
        End Sub
        Private Shared Sub EnsureFolder(ByVal file As String)
            Dim path As String = System.IO.Directory.GetParent(file).ToString
            If Not My.Computer.FileSystem.DirectoryExists(path) Then
                My.Computer.FileSystem.CreateDirectory(path)
            End If
        End Sub

        Public Shared Sub LogError(ByVal Message As String)
            Log("Error", Message)
        End Sub

        Public Shared Sub LogMsg(ByVal Message As String)
            Log("Msg", Message)
        End Sub
        Public Shared Sub LogMessage(ByVal Message As String)
            Log("Message", Message)
        End Sub

    End Class
End Namespace
