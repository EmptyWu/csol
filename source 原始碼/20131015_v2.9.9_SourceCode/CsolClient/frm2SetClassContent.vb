﻿Public Class frm2SetClassContent
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dtCont As New DataTable
    Private intClass As Integer = 0
    Private intSubClass As Integer = 0
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dtDetails As New DataTable
    Private dtBackSta As New DataTable
    Private dtKeepSta As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frm2SetClassContent
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        InitClassList()
        ShowClassList()
    End Sub

    Private Sub RefreshData()
        Try
            dtCont = frmMain.GetContents
            RefreshTable()
            LoadColumnText()
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub RefreshTable()
        Dim intSubClass As Integer
        Dim intClass As Integer
        Dim strFilter1 As String = c_EndColumnName & _
                        ">'" & Now.ToString & "' AND "
        Dim strFilter2 As String = c_ClassIDColumnName & "="
        Dim strFilter3 As String = c_SubClassIDColumnName & "="

        Dim dtCont2 As New DataTable
        Dim dtContArr As New ArrayList
        dtCont2 = dtCont.Clone
        For i As Integer = 0 To dtCont.Rows.Count - 1
            If Not dtContArr.Contains(dtCont.Rows(i).Item("ID")) Then
                dtContArr.Add(dtCont.Rows(i).Item("ID"))
                dtCont2.Rows.Add(dtCont.Rows(i).ItemArray)
            End If
        Next

        Try
            If dtCont.Rows.Count = 0 Then
                dgvCont.DataSource = dtCont.DefaultView
                Exit Sub
            End If

            If chkboxShowPastCont.Checked Then
                If cboxClass.SelectedIndex > -1 Then
                    If cboxSubClass.SelectedIndex > -1 Then
                        If cboxSubClass.SelectedIndex = 0 Then
                            intClass = lstClassShown(cboxClass.SelectedIndex)
                            dtCont2.DefaultView.RowFilter = strFilter2 & intClass.ToString
                            dgvCont.DataSource = dtCont2.DefaultView
                        Else
                            intSubClass = lstSubClassShown(cboxSubClass.SelectedIndex)
                            dtCont.DefaultView.RowFilter = strFilter3 & intSubClass.ToString
                            dgvCont.DataSource = dtCont.DefaultView
                        End If
                    End If
                End If
            Else
                If cboxClass.SelectedIndex > -1 Then
                    If cboxSubClass.SelectedIndex > -1 Then
                        If cboxSubClass.SelectedIndex = 0 Then
                            intClass = lstClassShown(cboxClass.SelectedIndex)
                            dtCont2.DefaultView.RowFilter = strFilter1 & strFilter2 & intClass.ToString
                            dgvCont.DataSource = dtCont2.DefaultView
                        Else
                            intSubClass = lstSubClassShown(cboxSubClass.SelectedIndex)
                            dtCont.DefaultView.RowFilter = strFilter1 & strFilter3 & intSubClass.ToString
                            dgvCont.DataSource = dtCont.DefaultView
                        End If
                    End If
                End If
            End If

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Function GetClassFee(ByVal intId As Integer) As Integer
        GetClassFee = 0
        For index As Integer = 0 To dtSubClassList.Rows.Count - 1
            If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intId Then
                GetClassFee = dtSubClassList.Rows(index).Item(c_FeeColumnName)
                Exit For
            End If
        Next

    End Function

    Private Sub InitClassList()
        Try
            Dim ds As DataSet = frmMain.GetClassInfoSet
            'dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
            '         c_NameColumnName, c_EndColumnName)
            'dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)
            dtClassList = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                     c_NameColumnName, c_EndColumnName)
            dtSubClassList = ds.Tables(c_SubClassListDataTableName)
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        cboxClass.Items.Clear()
        cboxSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now Then
                        cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtDetails.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DateTimeColumnName, GetType(System.DateTime))
        dtDetails.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
        dtDetails.Columns.Add(c_DiscountRemarksColumnName, GetType(System.String))
        dtDetails.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
    End Sub

    Private Sub LoadColumnText()
        dgvCont.Columns.Item(c_IDColumnName).Visible = False
        dgvCont.Columns.Item(c_SubClassIDColumnName).Visible = False
        dgvCont.Columns.Item(c_ClassIDColumnName).Visible = False
        dgvCont.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.classContent
        dgvCont.Columns.Item(c_StartColumnName).HeaderText = My.Resources.startDate
        dgvCont.Columns.Item(c_StartColumnName).DefaultCellStyle.Format = "d"
        dgvCont.Columns.Item(c_EndColumnName).HeaderText = My.Resources.endDate
        dgvCont.Columns.Item(c_EndColumnName).DefaultCellStyle.Format = "d"

    End Sub


    Private Sub InitClassSelection()
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClassShown.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim intC As Integer
            Dim intSc As Integer
            intC = lstClassShown(cboxClass.SelectedIndex)
            lstSubClassShown.Add(-1)
            cboxSubClass.Items.Add(My.Resources.all)

            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                Try
                    If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                        intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                        lstSubClassShown.Add(intSc)
                        cboxSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName).trim)

                    End If
                Catch ex As Exception
                    CSOL.Logger.LogError(ex.Message)
                End Try
            Next
            cboxSubClass.SelectedIndex = 0
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub chkboxShowPastCont_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPastCont.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub butAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butAdd.Click
        Dim intClass As Integer
        Dim lstId As New ArrayList
        Dim lstName As New ArrayList 
        If cboxClass.SelectedIndex > -1 Then
            intClass = lstClassShown(cboxClass.SelectedIndex)
            For index As Integer = 0 To cboxSubClass.Items.Count - 1
                If index > 0 Then
                    lstName.Add(cboxSubClass.Items(index))
                    lstId.Add(lstSubClassShown(index))
                End If
            Next
        Else
            Exit Sub
        End If
        frmMain.SetOkState(False)
        Dim frm As New frm2AddContent(-1, "", intClass, Now, Now, New ArrayList, _
                                      lstName, lstId)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            frmMain.RefreshClassContentTb()
            'frmMain.RefreshClassContentTbHelper()
            RefreshData()
        End If
    End Sub

    Private Sub butMod_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butMod.Click
        Dim intClass As Integer
        Dim lstId As New ArrayList
        Dim lstName As New ArrayList
        Dim lstcont As New ArrayList
        Dim intId As Integer

        If cboxClass.SelectedIndex > -1 And dgvCont.SelectedRows.Count > 0 Then
            intClass = lstClassShown(cboxClass.SelectedIndex)
            For index As Integer = 0 To cboxSubClass.Items.Count - 1
                If index > 0 Then
                    lstName.Add(cboxSubClass.Items(index))
                    lstId.Add(lstSubClassShown(index))
                End If
            Next
            intId = dgvCont.SelectedRows(0).Cells(c_IDColumnName).Value
            For index As Integer = 0 To dtCont.Rows.Count - 1
                If dtCont.Rows(index).Item(c_IDColumnName) = intId Then
                    If Not dtCont.Rows(index).Item(c_SubClassIDColumnName) Is Nothing Then
                        lstcont.Add(dtCont.Rows(index).Item(c_SubClassIDColumnName))
                    End If
                End If
            Next
        Else
            Exit Sub
        End If
        frmMain.SetOkState(False)
        Dim frm As New frm2AddContent(intId, dgvCont.SelectedRows(0).Cells(c_ContentColumnName).Value, _
                                intClass, dgvCont.SelectedRows(0).Cells(c_StartColumnName).Value, _
                                dgvCont.SelectedRows(0).Cells(c_EndColumnName).Value, _
                                lstcont, lstName, lstId)
        frm.ShowDialog()
        If frmMain.GetOkState Then

            frmMain.RefreshClassContentTb()
            'frmMain.RefreshClassContentTbHelper()
            RefreshData()
        End If
    End Sub

    Private Sub butDel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butDel.Click
        Dim intId As Integer
        If dgvCont.SelectedRows.Count > 0 Then
            intId = dgvCont.SelectedRows(0).Cells(c_IDColumnName).Value
            objCsol.DeleteClassContent(intId)

            'Dim isBusy As Boolean = False
            'If frmMain.BackgroundWorker8.IsBusy Then
            '    Do Until Not frmMain.BackgroundWorker8.IsBusy
            '        isBusy = False
            '    Loop
            'End If

            'If Not isBusy Then
            '    frmMain.BackgroundWorker8.RunWorkerAsync()
            'End If
        End If

        frmMain.RefreshClassContentTb()
        'frmMain.RefreshClassContentTbHelper()
        RefreshData()
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub butVideo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butVideo.Click

    End Sub

    Private Sub butPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butPrint.Click
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvCont.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvCont.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvCont.Rows.Count

            Dim oRow As DataGridViewRow = dgvCont.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvCont.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvCont.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvCont.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvCont.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvCont.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvCont.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvCont.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvCont.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvCont.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

End Class