﻿Public Class frm2SeatMap
    Private dtClassRoomInfo As New DataTable
    Private dtSeatNaList As New DataTable
    Private dtSeatTakenList As New DataTable
    Private dsSeatRegInfo As New DataSet
    Private intSubClassId As Integer = -1
    Private MySeat As String = ""               '----------- 2010/11/18 新增 MySeat by Vic
    Private stuid As String = ""                '----------- 2010/11/18 新增 stuid by Vic

    Private intRowCnt As Integer = 0
    Private intColCnt As Integer = 0
    Private intPwCnt As Integer = 0
    Private intState As Integer = -1
    Private intDisStyle As Integer = -1

    Private lstColumn As New ArrayList
    Private lstRow As New ArrayList
    Private lstPw As New ArrayList
    Private lstConvex As New ArrayList
    Private lstNaCol As New ArrayList
    Private lstNaRow As New ArrayList
    Private lstTk As New ArrayList
    Private lstName As New ArrayList

    Private lstAbc() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ".Split(",")
    Private blEmptySeat As Boolean = False


    Public Sub New(ByVal intScId As Integer)
        InitializeComponent()
        'Me.dgvSeat.Rows(2).Cells(2).Selected = True
        intSubClassId = intScId
        If intSubClassId > -1 Then
            RefreshData()
            LoadColumnText()
        End If
        tboxSeatNa.Text = c_SeatNaText
    End Sub

    Public Sub New(ByVal intScId As Integer, ByVal strid As String) '---------------------2010/11/18 從frmStuInfo導入 strid(學號) by Vic
        stuid = strid                                               '---------------------2010/11/18 新增 stuid by Vic
        InitializeComponent()
        'Me.dgvSeat.Rows(2).Cells(2).Selected = True
        intSubClassId = intScId
        If intSubClassId > -1 Then
            RefreshData()
            LoadColumnText()
        End If
        tboxSeatNa.Text = c_SeatNaText
    End Sub

    Friend Sub RefreshData()

        Dim strCompute1 As String = "COUNT(" + c_ColumnNumColumnName + ")"
        Dim strCompute2 As String = c_StateColumnName & "=0"

        Try
            dsSeatRegInfo = objCsol.GetSeatRegInfo(intSubClassId)
            dtClassRoomInfo = dsSeatRegInfo.Tables(c_ClassRoomInfoTableName)
            dtSeatNaList = dsSeatRegInfo.Tables(c_SeatNaListTableName)
            dtSeatTakenList = dsSeatRegInfo.Tables(c_SeatTakenListTableName)
            MySeat = getMySeat()


            If dtClassRoomInfo.Rows.Count > 0 Then
                intRowCnt = dtClassRoomInfo.Rows(0).Item(c_LineCntColumnName)
                intColCnt = dtClassRoomInfo.Rows(0).Item(c_ColumnCntColumnName)
                intDisStyle = dtClassRoomInfo.Rows(0).Item(c_DisStyleColumnName)
                intPwCnt = dtSeatNaList.Compute(strCompute1, strCompute2)
                lstPw = New ArrayList
                lstNaCol = New ArrayList
                lstNaRow = New ArrayList
                lstTk = New ArrayList
                lstName = New ArrayList
                lstConvex = New ArrayList

                Dim intS = 0
                For index As Integer = 0 To dtSeatNaList.Rows.Count - 1
                    intS = dtSeatNaList.Rows(index).Item(c_StateColumnName)
                    If intS = c_SeatNaStatePw Then
                        lstPw.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                    ElseIf intS = c_SeatNaStateConvex Then
                        lstConvex.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    ElseIf intS = c_SeatNaStateNotseat Then
                        lstNaCol.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                        lstNaRow.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    End If
                Next

                For index As Integer = 0 To dtSeatTakenList.Rows.Count - 1
                    lstTk.Add(dtSeatTakenList.Rows(index).Item(c_SeatNumColumnName).ToString.Trim)
                    lstName.Add(dtSeatTakenList.Rows(index).Item(c_StuNameColumnName))
                Next

                If intRowCnt > 0 And intColCnt + intPwCnt > 0 Then
                    InitTable()
                    RefreshTable()
                End If
            End If

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dgvSeat.ColumnCount = intColCnt + intPwCnt
        dgvSeat.RowCount = intRowCnt

        FillArrayList(1, intColCnt + intPwCnt, lstColumn)
        FillArrayList(1, intRowCnt, lstRow)

        Dim i As Integer = 0

        Select Case intDisStyle
            Case 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        lstColumn(index) = lstAbc(i).ToString
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next

            Case 2
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i + 1 < 10 Then
                            lstColumn(index) = "0" & (i + 1).ToString
                        Else
                            lstColumn(index) = (i + 1).ToString
                        End If
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    lstRow(index) = lstAbc(index).ToString
                Next

            Case 3
                i = lstAbc.Count - 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i > 0 Then
                            lstColumn(index) = lstAbc(i).ToString
                            i = i - 1
                        End If
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next
        End Select

        For index As Integer = 0 To dgvSeat.ColumnCount - 1
            If index < lstColumn.Count Then
                dgvSeat.Columns(index).HeaderText = lstColumn(index)
                If lstColumn(index) = "" Then
                    dgvSeat.Columns(index).DefaultCellStyle.BackColor = Color.Silver
                End If
                dgvSeat.Columns(index).Width = 40
                dgvSeat.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            End If
        Next

        For index As Integer = 0 To dgvSeat.RowCount - 1
            If index < lstRow.Count Then
                dgvSeat.Rows(index).HeaderCell.Value = lstRow(index)
            End If
            If lstConvex.IndexOf(index) > -1 Then
                dgvSeat.Rows(index).HeaderCell.Style.BackColor = Color.Black
            End If
        Next

        If lstNaCol.Count > 0 Then
            For index As Integer = 0 To lstNaCol.Count - 1
                dgvSeat.Rows(lstNaRow(index)).Cells(lstNaCol(index)).value = c_SeatNaText
            Next
        End If
        '-------------------------------------------------------------------------------------2010/11/18 by Vic ↓
        'Dim first As DataGridViewCell

        'first = Me.dgvSeat.CurrentCell
        'Me.dgvSeat.Rows(dgvSeat.RowCount - 1).Cells(dgvSeat.ColumnCount - 1).Selected = True
        'Me.dgvSeat.FirstDisplayedCell = first

        'dtSeatTakenList = dsSeatRegInfo.Tables(c_SeatTakenListTableName)
        'Dim aa As Integer
        'For aa = 0 To dtSeatTakenList.Rows.Count - 1
        '    If MySeat = dtSeatTakenList.Rows(aa).Item("SeatNum") Then
        '        Exit For
        '    End If
        'Next


        If MySeat <> "" Then
            Dim colnum, rownum As Integer 'row & column
            Dim num As String = ""  '數字
            Dim data As String = "" '字母
            Dim matchdata As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(MySeat, "[A-Z]")
            Dim matchnum As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(MySeat, "[0-9]")
            MySeat = MySeat.Trim()

            For c As Integer = 0 To matchdata.Count - 1
                Dim strtest As String = matchdata.Item(c).Value
                data += strtest
            Next

            For c As Integer = 0 To matchnum.Count - 1
                Dim strtest As String = matchnum.Item(c).Value
                num += strtest
            Next
            colnum = Array.IndexOf(lstColumn.ToArray(), data)
            rownum = Array.IndexOf(lstRow.ToArray(), num)
            Me.dgvSeat.Rows(rownum).Cells(colnum).Selected = True
        Else
            'Dim first As DataGridViewCell
            'first = Me.dgvSeat.CurrentCell
            'Me.dgvSeat.Rows(dgvSeat.RowCount - 1).Cells(dgvSeat.ColumnCount - 1).Selected = True
            'Me.dgvSeat.FirstDisplayedCell = first
        End If
        '-------------------------------------------------------------------------------------2010/11/18 by Vic ↑

    End Sub

    Private Sub RefreshTable()
        Dim strSeat As String = ""
        Dim idx As Integer = -1
        For index As Integer = 0 To dgvSeat.Columns.Count - 1
            For j As Integer = 0 To dgvSeat.Rows.Count - 1
                If Not dgvSeat.Columns(index).HeaderText = "" And _
                    Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
                    Select Case intDisStyle
                        Case 1, 3
                            strSeat = dgvSeat.Columns(index).HeaderText & _
                                    dgvSeat.Rows(j).HeaderCell.Value
                        Case 2
                            strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
                                    dgvSeat.Columns(index).HeaderText
                    End Select
                    idx = lstTk.IndexOf(strSeat)
                    If idx > -1 Then 'taken
                        dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxTk.BackColor
                        If chkboxName.Checked = True Then
                            dgvSeat.Rows(j).Cells(index).Value = lstName(idx)
                        Else
                            dgvSeat.Rows(j).Cells(index).Value = strSeat
                        End If
                    Else 'available
                        dgvSeat.Rows(j).Cells(index).Value = strSeat
                    End If
                End If
            Next

        Next
    End Sub

    Private Sub LoadColumnText()
        'To be added
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        Dim strSeat As String = ""
        'MsgBox(dgvSeat.CurrentCell)
        If dgvSeat.CurrentCell Is Nothing Then
            MsgBox("未選擇座位，如不需選擇請指定「空座位」 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Return
        Else
            strSeat = dgvSeat.CurrentCell.Value.ToString.Trim
        End If

        Dim dtSeatTakenList As DataTable = objCsol.GetSeatTakenList(intSubClassId)
        For i As Integer = 0 To dtSeatTakenList.Rows.Count - 1
            If strSeat = dtSeatTakenList.Rows(i).Item("SeatNum").ToString.Trim Then
                MsgBox("此座位已被劃位，請重新開啟座位表後重選座位 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                Exit Sub
            End If
        Next
        If strSeat = "" Or strSeat = c_SeatNaText Then
            MsgBox(My.Resources.msgSeatNa, _
                    MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Else
            If dgvSeat.CurrentCell.Style.BackColor = tboxTk.BackColor Then
                MsgBox(My.Resources.msgSeatTaken, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Else
                frmMain.SetSeatNum(strSeat)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        frmMain.SetOkState(False)
        Me.Close()
    End Sub

    Private Sub chkboxName_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxName.CheckedChanged
        RefreshTable()
    End Sub


    Private Sub butEmptySeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEmptySeat.Click
        frmMain.SetSeatNum("")
        frmMain.SetOkState(True)
        Me.Close()
    End Sub

    Private Function getMySeat() As String
        Dim result As String = ""
        For Rnum As Integer = 0 To dtSeatTakenList.Rows.Count - 1
            If stuid = dtSeatTakenList.Rows(Rnum).Item("stuid").ToString Then 'stuid 是學生的學號
                result = dtSeatTakenList.Rows(Rnum).Item(c_SeatNumColumnName)
                Exit For
            End If
        Next
        Return result
    End Function
   
End Class