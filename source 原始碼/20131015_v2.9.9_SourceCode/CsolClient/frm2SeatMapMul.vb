﻿Public Class frm2SeatMapMul
    Private dtClassRoomInfo As New DataTable
    Private dtSeatNaList As New DataTable
    Private dtSeatTakenList As New DataTable
    Private dsSeatRegInfo As New DataSet
    Private lstSubClass As New ArrayList
    Private lstSubClassName As New ArrayList
    Private lstSubClassShown As New ArrayList

    Private intRowCnt As Integer = 0
    Private intColCnt As Integer = 0
    Private intPwCnt As Integer = 0
    Private intState As Integer = -1
    Private intDisStyle As Integer = -1

    Private lstColumn As New ArrayList
    Private lstRow As New ArrayList
    Private lstPw As New ArrayList
    Private lstConvex As New ArrayList
    Private lstNaCol As New ArrayList
    Private lstNaRow As New ArrayList
    Private lstTk As New ArrayList
    Private lstName As New ArrayList

    Private lstAbc() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ".Split(",")

    Public Sub New(ByVal lstScId As ArrayList, ByVal lstScName As ArrayList)
        InitializeComponent()
        lstSubClassShown = lstScId
        lstSubClassName = lstScName
        If lstSubClassShown.Count > 0 Then
            RefreshData()
            LoadColumnText()
            chklstSubClass.Items.Clear()
            For index As Integer = 0 To lstSubClassShown.Count - 1
                chklstSubClass.Items.Add(lstSubClassName(index), True)
            Next
        End If
        tboxSeatNa.Text = c_SeatNaText
    End Sub

    Friend Sub RefreshData()

        Dim strCompute1 As String = "COUNT(" + c_ColumnNumColumnName + ")"
        Dim strCompute2 As String = c_StateColumnName & "=0"

        Try
            dsSeatRegInfo = objCsol.GetSeatRegInfo(lstSubClassShown(0))
            dtClassRoomInfo = dsSeatRegInfo.Tables(c_ClassRoomInfoTableName)
            dtSeatNaList = dsSeatRegInfo.Tables(c_SeatNaListTableName)
            dtSeatTakenList = dsSeatRegInfo.Tables(c_SeatTakenListTableName)

            If dtClassRoomInfo.Rows.Count > 0 Then
                intRowCnt = dtClassRoomInfo.Rows(0).Item(c_LineCntColumnName)
                intColCnt = dtClassRoomInfo.Rows(0).Item(c_ColumnCntColumnName)
                intDisStyle = dtClassRoomInfo.Rows(0).Item(c_DisStyleColumnName)
                intPwCnt = dtSeatNaList.Compute(strCompute1, strCompute2)
                lstPw = New ArrayList
                lstNaCol = New ArrayList
                lstNaRow = New ArrayList
                lstTk = New ArrayList
                lstName = New ArrayList
                lstConvex = New ArrayList

                Dim intS = 0
                For index As Integer = 0 To dtSeatNaList.Rows.Count - 1
                    intS = dtSeatNaList.Rows(index).Item(c_StateColumnName)
                    If intS = c_SeatNaStatePw Then
                        lstPw.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                    ElseIf intS = c_SeatNaStateConvex Then
                        lstConvex.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    ElseIf intS = c_SeatNaStateNotseat Then
                        lstNaCol.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                        lstNaRow.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    End If
                Next

                For index As Integer = 0 To dtSeatTakenList.Rows.Count - 1
                    lstTk.Add(dtSeatTakenList.Rows(index).Item(c_SeatNumColumnName))
                    lstName.Add(dtSeatTakenList.Rows(index).Item(c_StuNameColumnName))
                Next

                If intRowCnt > 0 And intColCnt + intPwCnt > 0 Then
                    InitTable()
                    RefreshTable()
                End If
            End If

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dgvSeat.ColumnCount = intColCnt + intPwCnt
        dgvSeat.RowCount = intRowCnt

        FillArrayList(1, intColCnt + intPwCnt, lstColumn)
        FillArrayList(1, intRowCnt, lstRow)

        Dim i As Integer = 0

        Select Case intDisStyle
            Case 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        lstColumn(index) = lstAbc(i).ToString
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next

            Case 2
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i + 1 < 10 Then
                            lstColumn(index) = "0" & (i + 1).ToString
                        Else
                            lstColumn(index) = (i + 1).ToString
                        End If
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    lstRow(index) = lstAbc(index).ToString
                Next

            Case 3
                i = lstAbc.Count - 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i > 0 Then
                            lstColumn(index) = lstAbc(i).ToString
                            i = i - 1
                        End If
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next
        End Select

        For index As Integer = 0 To dgvSeat.ColumnCount - 1
            If index < lstColumn.Count Then
                dgvSeat.Columns(index).HeaderText = lstColumn(index)
                If lstColumn(index) = "" Then
                    dgvSeat.Columns(index).DefaultCellStyle.BackColor = Color.Silver
                End If
                dgvSeat.Columns(index).Width = 40
                dgvSeat.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            End If
        Next

        For index As Integer = 0 To dgvSeat.RowCount - 1
            If index < lstRow.Count Then
                dgvSeat.Rows(index).HeaderCell.Value = lstRow(index)
            End If
            If lstConvex.IndexOf(index) > -1 Then
                dgvSeat.Rows(index).HeaderCell.Style.BackColor = Color.Aqua
            End If
        Next

        If lstNaCol.Count > 0 Then
            For index As Integer = 0 To lstNaCol.Count - 1
                dgvSeat.Rows(lstNaRow(index)).Cells(lstNaCol(index)).value = c_SeatNaText
            Next
        End If

    End Sub

    Private Sub RefreshTable()
        Dim strSeat As String = ""
        Dim idx As Integer = -1
        For index As Integer = 0 To dgvSeat.Columns.Count - 1
            For j As Integer = 0 To dgvSeat.Rows.Count - 1
                If Not dgvSeat.Columns(index).HeaderText = "" And _
                    Not dgvSeat.Rows(j).Cells(index).Value = c_SeatNaText Then
                    Select Case intDisStyle
                        Case 1, 3
                            strSeat = dgvSeat.Columns(index).HeaderText & _
                                    dgvSeat.Rows(j).HeaderCell.Value
                        Case 2
                            strSeat = dgvSeat.Rows(j).HeaderCell.Value & _
                                    dgvSeat.Columns(index).HeaderText
                    End Select
                    idx = lstTk.IndexOf(strSeat)
                    If idx > -1 Then 'taken
                        dgvSeat.Rows(j).Cells(index).Style.BackColor = tboxTk.BackColor
                        If chkboxName.Checked = True Then
                            dgvSeat.Rows(j).Cells(index).Value = lstName(idx)
                        Else
                            dgvSeat.Rows(j).Cells(index).Value = strSeat
                        End If
                    Else 'available
                        dgvSeat.Rows(j).Cells(index).Value = strSeat
                    End If
                End If
            Next

        Next
    End Sub

    Private Sub LoadColumnText()
        'To be added
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        Dim strSeat As String = ""

        If Not dgvSeat.CurrentCell Is Nothing Then
            strSeat = dgvSeat.CurrentCell.Value
        End If

        If strSeat = "" Or strSeat = c_SeatNaText Then
            MsgBox(My.Resources.msgSeatNa, _
                    MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Else
            If dgvSeat.CurrentCell.Style.BackColor = tboxTk.BackColor Then
                MsgBox(My.Resources.msgSeatTaken, _
                    MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Else
                frmMain.SetSeatNum(strSeat)
                lstSubClass.Clear()
                For index As Integer = 0 To chklstSubClass.Items.Count - 1
                    If chklstSubClass.GetItemChecked(index) Then
                        lstSubClass.Add(lstSubClassShown(index))
                    End If
                Next
                If lstSubClass.Count > 0 Then
                    frmMain.SetOkState(True)
                    frmMain.SetCurrentList(lstSubClass)
                End If

                Me.Close()
            End If
        End If

    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        frmMain.SetOkState(False)
        Me.Close()
    End Sub

    Private Sub chkboxName_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxName.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub butEmptySeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butEmptySeat.Click
        frmMain.SetSeatNum("")
        lstSubClass.Clear()
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            If chklstSubClass.GetItemChecked(index) Then
                lstSubClass.Add(lstSubClassShown(index))
            End If
        Next
        If lstSubClass.Count > 0 Then
            frmMain.SetOkState(True)
            frmMain.SetCurrentList(lstSubClass)
        End If

        Me.Close()
    End Sub
End Class