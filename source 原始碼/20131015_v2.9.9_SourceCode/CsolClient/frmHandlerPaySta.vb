﻿Public Class frmHandlerPaySta
    Private dateNow As Date = Now
    Private dtPaySta As New DataTable
    Private dtClassMaster As New DataTable
    Private dtClassDetails As New DataTable
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmHandlerPaySta
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmHandlerPaySta
        SetDate()
        
        'RefreshData()
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        ExportDgvToExcel(dgvClassMaster)
        ExportDgvToExcel(dgvMaster)
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        ExportDgvToExcel(dgvClassDetails)
        ExportDgvToExcel(dgvDetails)
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        PrintDocument2.DefaultPageSettings.Landscape = True
        PrintDocument2.Print()
    End Sub

    Friend Sub SetDate()
        dtpickFrom.Value = dateNow
        dtpickTo.Value = dateNow
        lblThisMonth.Text = GetROCYear(Now).ToString & My.Resources.year _
                            & Now.Month.ToString & My.Resources.month
        dtpickDate.Value = dateNow
        lblEmployee.Text = frmMain.GetUsrName
    End Sub

    Private Sub frmHandlerPaySta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmHandlerPaySta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intClass As Integer
        Dim strToday As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_ReceiptNumColumnName + ")"
        Dim strCompute4 As String
        Dim strCompute5 As String

        Try
            If radbutByDate.Checked = True Then
                dtPaySta = objCsol.ListPersonalHandle(frmMain.GetUsrId, GetDateStart(dtpickDate.Value), GetDateEnd(dtpickDate.Value))
            ElseIf radbutThisMonth.Checked = True Then
                dtPaySta = objCsol.ListPersonalHandle(frmMain.GetUsrId, _
                                New Date(Now.Year, Now.Month, 1), _
                                New Date(Now.Year, Now.Month, dhDaysInMonth(Now) - 1))
            ElseIf radbutCustDate.Checked = True Then
                dtPaySta = objCsol.ListPersonalHandle(frmMain.GetUsrId, _
                                                     GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            End If

            dtMaster = New DataTable()
            dtDetails = New DataTable()
            dtClassMaster = New DataTable()
            dtClassDetails = New DataTable()
            dtClassMaster = dtPaySta.DefaultView.ToTable(True, c_ClassIDColumnName, _
                                                        c_ClassNameColumnName)
            dtClassDetails = dtPaySta.Clone
            dtClassDetails.Merge(dtPaySta)
            dtDetails = dtPaySta.Clone
            dtDetails.Merge(dtPaySta)
            InitTable()

            strToday = Now.Year.ToString & "/" & Now.Month.ToString & "/" & _
                       Now.Day.ToString

            For index As Integer = 0 To dtClassMaster.Rows.Count - 1
                intClass = dtClassMaster.Rows(index).Item(c_ClassIDColumnName)
                strCompute2 = c_ClassIDColumnName & "=" & intClass.ToString
                strCompute4 = c_ClassIDColumnName & "=" & intClass.ToString & _
                                " AND " & c_RegDateColumnName & "='" & strToday & "'"
                intTotal = dtPaySta.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtPaySta.Compute(strCompute1, strCompute2)
                    If chkboxNewStuSta.Checked = True Then
                        intTotal3 = dtPaySta.Compute(strCompute3, strCompute4)
                    Else
                        intTotal3 = 0
                    End If
                    dtClassMaster.Rows(index).Item(c_DataCntColumnName) = intTotal
                    dtClassMaster.Rows(index).Item(c_NewCntColumnName) = intTotal3
                    dtClassMaster.Rows(index).Item(c_AmountColumnName) = intTotal2
                End If
            Next
            If dtClassMaster.Rows.Count > 0 Then
                strCompute2 = "SUM(" + c_DataCntColumnName + ")"
                strCompute4 = "SUM(" + c_NewCntColumnName + ")"
                strCompute5 = "SUM(" + c_AmountColumnName + ")"
                dtClassMaster.Rows.Add(-1, My.Resources.totalAmt, _
                    dtClassMaster.Compute(strCompute2, ""), _
                    dtClassMaster.Compute(strCompute4, ""), _
                    dtClassMaster.Compute(strCompute5, ""))
            Else
                dtClassMaster.Rows.Add(-1, My.Resources.totalAmt, 0, 0, 0)
            End If

            dgvClassMaster.DataSource = dtClassMaster
            If dgvClassMaster.RowCount > 0 Then
                dgvClassMaster.CurrentCell = dgvClassMaster.Rows.Item(dtClassMaster.Rows.Count - 1).Cells(c_ClassNameColumnName)
            End If
            dtClassDetails.DefaultView.RowFilter = ""
            dgvClassDetails.DataSource = dtClassDetails.DefaultView
            dgvClassMaster.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvClassDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"

            If dtDetails.Rows.Count > 0 Then
                strCompute2 = c_PayMethodColumnName + "=1"
                intTotal = dtDetails.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                    dtMaster.Rows(0).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(0).Item(c_AmountColumnName) = intTotal2
                End If
                strCompute2 = c_PayMethodColumnName + "=2"
                intTotal = dtDetails.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                    dtMaster.Rows(1).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(1).Item(c_AmountColumnName) = intTotal2
                End If
                strCompute2 = c_PayMethodColumnName + "=3"
                intTotal = dtDetails.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                    dtMaster.Rows(2).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(2).Item(c_AmountColumnName) = intTotal2
                End If
                strCompute2 = c_PayMethodColumnName + "=4"
                intTotal = dtDetails.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                    dtMaster.Rows(3).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(3).Item(c_AmountColumnName) = intTotal2
                End If
                strCompute2 = c_PayMethodColumnName + "=6"
                intTotal = dtDetails.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtDetails.Compute(strCompute1, strCompute2)
                    dtMaster.Rows(4).Item(c_DataCntColumnName) = intTotal
                    dtMaster.Rows(4).Item(c_AmountColumnName) = intTotal2
                End If
            End If

            If dtMaster.Rows.Count > 0 Then
                strCompute2 = "SUM(" + c_DataCntColumnName + ")"
                strCompute5 = "SUM(" + c_AmountColumnName + ")"
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, _
                    dtMaster.Compute(strCompute2, ""), _
                    dtMaster.Compute(strCompute5, ""))
            Else
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, 0, 0)
            End If

            dgvMaster.DataSource = dtMaster
            'If dgvMaster.RowCount > 0 Then
            '    dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.Rows.Count - 1).Cells(c_IDColumnName)
            'End If
            dtDetails.DefaultView.RowFilter = ""
            dgvDetails.DataSource = dtDetails.DefaultView
            dgvMaster.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtClassMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtClassMaster.Columns.Add(c_NewCntColumnName, GetType(System.Int32))
        dtClassMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))

        dtMaster.Columns.Add(c_IDColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_PayMethodColumnName, GetType(System.String))
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dtMaster.Rows.Add(1, My.Resources.cash, 0, 0)
        dtMaster.Rows.Add(2, My.Resources.cheque, 0, 0)
        dtMaster.Rows.Add(3, My.Resources.transfer, 0, 0)
        dtMaster.Rows.Add(4, My.Resources.creditCard, 0, 0)
        dtMaster.Rows.Add(6, My.Resources.none, 0, 0)

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvClassMaster.Columns
            col.visible = False
        Next
        If dgvClassMaster.Columns.Count > 0 Then

            dgvClassMaster.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvClassMaster.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvClassMaster.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvClassMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 1
            dgvClassMaster.Columns.Item(c_DataCntColumnName).Visible = True
            dgvClassMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
            dgvClassMaster.Columns.Item(c_NewCntColumnName).DisplayIndex = 2
            dgvClassMaster.Columns.Item(c_NewCntColumnName).Visible = True
            dgvClassMaster.Columns.Item(c_NewCntColumnName).HeaderText = My.Resources.newCnt
            dgvClassMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            dgvClassMaster.Columns.Item(c_AmountColumnName).Visible = True
            dgvClassMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.totalAmt
        End If
        For Each col In dgvClassDetails.Columns
            col.visible = False
        Next
        If dgvClassDetails.ColumnCount > 0 Then

            dgvClassDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
            dgvClassDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgvClassDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
            dgvClassDetails.Columns.Item(c_StuIDColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvClassDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
            dgvClassDetails.Columns.Item(c_StuNameColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvClassDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
            dgvClassDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvClassDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 4
            dgvClassDetails.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvClassDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 5
            dgvClassDetails.Columns.Item(c_PayMethodColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
            dgvClassDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 6
            dgvClassDetails.Columns.Item(c_AmountColumnName).Visible = True
            dgvClassDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        End If
        For Each col In dgvMaster.Columns
            col.visible = False
        Next
        If dgvMaster.ColumnCount > 0 Then

            dgvMaster.Columns.Item(c_PayMethodColumnName).DisplayIndex = 0
            dgvMaster.Columns.Item(c_PayMethodColumnName).Visible = True
            dgvMaster.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
            dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 1
            dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
            dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
            dgvMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 2
            dgvMaster.Columns.Item(c_AmountColumnName).Visible = True
            dgvMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.totalAmt
        End If
        For Each col In dgvDetails.Columns
            col.visible = False
        Next
        If dgvDetails.Rows.Count > 0 Then
            
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgvDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
            dgvDetails.Columns.Item(c_StuIDColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
            dgvDetails.Columns.Item(c_StuNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
            dgvDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 4
            dgvDetails.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 5
            dgvDetails.Columns.Item(c_PayMethodColumnName).Visible = True
            dgvDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
            dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 6
            dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmHandlerPaySta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub

    Private Sub dgvClassMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClassMaster.SelectionChanged
        Dim intClassId As Integer
        If dgvClassMaster.SelectedRows.Count > 0 Then
            intClassId = dgvClassMaster.SelectedRows(0).Cells(c_ClassIDColumnName).Value
            If intClassId > -1 Then
                dtClassDetails.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString
            Else
                dtClassDetails.DefaultView.RowFilter = ""
            End If
            dgvClassDetails.DataSource = dtClassDetails.DefaultView
        End If
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intId = dgvMaster.SelectedRows(0).Cells(c_IDColumnName).Value
            If intId > -1 Then
                dtDetails.DefaultView.RowFilter = c_PayMethodColumnName + "=" + intId.ToString
            Else
                dtDetails.DefaultView.RowFilter = ""
            End If
            dgvDetails.DataSource = dtDetails.DefaultView
        End If
    End Sub

End Class