﻿Imports Microsoft.Office.Interop
Imports System.Data.OleDb

Public Class frm2ImportSch
    Dim path As String = ""
    Dim fileExt As String = ""

    Private Sub frm2ImportSch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        cboxName.SelectedIndex = 0
        cboxAddr.SelectedIndex = 0
        cboxTel.SelectedIndex = 0
        cboxContact.SelectedIndex = 0
    End Sub

    Private Sub butFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFile.Click
        Dim fileName As String = ""
        Dim openFileDialog1 As New OpenFileDialog()
        Dim xcFileInfo As IO.FileInfo

        openFileDialog1.InitialDirectory = "c:\"
        If radbutExcel.Checked Then
            openFileDialog1.Filter = "Excel files (*.xls)|*.xls|All files (*.*)|*.*"
        Else
            openFileDialog1.Filter = "Access files (*.mdb)|*.mdb|All files (*.*)|*.*"
        End If
        openFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                path = openFileDialog1.FileName
                tboxFile.Text = path
                fileName = path.Substring(path.LastIndexOf("\\") + 1)
                fileExt = fileName.Substring(fileName.LastIndexOf(".") + 1)
                If fileExt.ToLower() = "xls" Or fileExt.ToLower = "xlsx" Then
                    xcFileInfo = New IO.FileInfo(path)
                    Dim myExcel As Excel.Application
                    Dim myWorkBookCollection As Excel.Workbooks ' Workbook-collection (note the 's' at the end)        
                    Dim myWorkBook As Excel.Workbook ' Single Workbook (spreadsheet-collection)        
                    Dim myWorkSheet As Excel.Worksheet ' Single spreadsheet        
                    ' Initialize the interface to Excel.exe        
                    myExcel = New Excel.Application
                    If myExcel Is Nothing Then
                        dgv.Columns.Clear()
                        dgv.Rows.Clear()
                        Exit Sub
                    End If
                    ' initialise access to Excel's workbook collection        
                    myWorkBookCollection = myExcel.Workbooks
                    'open spreadsheet from disk        
                    myWorkBook = myWorkBookCollection.Open(xcFileInfo.FullName, , False)
                    'get 1st sheet from workbook        
                    myWorkSheet = myWorkBook.Sheets.Item(1)
                    'alter contents of 1st cell   
                    Dim rowCount As Integer = myWorkSheet.UsedRange.Rows.Count
                    Dim colCount As Integer = myWorkSheet.UsedRange.Columns.Count
                    dgv.Rows.Clear()
                    If colCount > 4 Then
                        colCount = 4
                    End If
                    For index As Integer = 1 To rowCount
                        dgv.Rows.Add(myWorkSheet.Range("A" & index).Value, _
                                    myWorkSheet.Range("B" & index).Value, _
                                    myWorkSheet.Range("C" & index).Value, _
                                    myWorkSheet.Range("D" & index).Value)
                    Next


                    myExcel.Quit()
                ElseIf fileExt.ToLower = "mdb" Then
                    Dim CN As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                                  "Data Source=" & path & ";")
                    CN.Open()
                    cboxTable.Items.Clear()
                    For Each R As DataRow In CN.GetSchema("Tables", New String() {Nothing, Nothing, Nothing, "Table"}).Rows
                        cboxTable.Items.Add(R.Item(2).ToString)
                    Next
                    CN.Close()
                    CN.Dispose()
                    cboxTable.SelectedIndex = -1
                End If

            Catch Ex As Exception
                CSOL.Logger.LogError(Ex.Message)
            End Try
        End If
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub butImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butImport.Click
        Try
            If dgv.Rows.Count > 0 Then
                Dim i1 As Integer = 0
                Dim i2 As Integer = 1
                Dim i3 As Integer = 2
                Dim i4 As Integer = 3
                Dim type As Integer = 1
                If cboxName.SelectedIndex > -1 Then
                    i1 = cboxName.SelectedIndex
                End If
                If cboxTel.SelectedIndex > -1 Then
                    i2 = cboxTel.SelectedIndex
                End If
                If cboxAddr.SelectedIndex > -1 Then
                    i3 = cboxAddr.SelectedIndex
                End If
                If cboxContact.SelectedIndex > -1 Then
                    i4 = cboxContact.SelectedIndex
                End If
                If radbutJun.Checked Then
                    type = 2
                ElseIf radbutHig.Checked Then
                    type = 3
                ElseIf radbutUni.Checked Then
                    type = 4
                End If

                For index As Integer = 0 To dgv.Rows.Count - 1
                    objCsol.AddSchool(GetDbString(dgv.Rows(index).Cells(i1).Value), _
                                        type, _
                                         GetDbString(dgv.Rows(index).Cells(i2).Value), _
                                        GetDbString(dgv.Rows(index).Cells(i3).Value), _
                                        GetDbString(dgv.Rows(index).Cells(i4).Value))
                Next
                frmMain.SetOkState(True)
                frmMain.RefreshSchool()
                Me.Close()
            End If
        Catch ex As Exception
            CSOL.Logger.LogError(ex.Message)
            MsgBox(My.Resources.msgImportError & ": " & ex.ToString, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End Try
    End Sub



    Private Sub cboxTable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxTable.SelectedIndexChanged
        If fileExt.ToLower = "mdb" Then
            If cboxTable.Items.Count > 0 And cboxTable.SelectedIndex > -1 Then
                Dim theOleDbCommand As New OleDbCommand("SELECT * FROM " & cboxTable.SelectedItem.ToString, _
                    New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & ";"))
                theOleDbCommand.Connection.Open()
                Dim theReader As OleDbDataReader = theOleDbCommand.ExecuteReader(CommandBehavior.CloseConnection)
                While theReader.Read() = True
                    dgv.Rows.Add(theReader(0).ToString())
                End While
            End If
        End If
    End Sub
End Class