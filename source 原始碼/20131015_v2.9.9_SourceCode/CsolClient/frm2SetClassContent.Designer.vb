﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetClassContent
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SetClassContent))
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.chkboxShowPastCont = New System.Windows.Forms.CheckBox
        Me.dgvCont = New System.Windows.Forms.DataGridView
        Me.butAdd = New System.Windows.Forms.Button
        Me.butMod = New System.Windows.Forms.Button
        Me.butDel = New System.Windows.Forms.Button
        Me.butClose = New System.Windows.Forms.Button
        Me.butVideo = New System.Windows.Forms.Button
        Me.butPrint = New System.Windows.Forms.Button
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvCont, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'chkboxShowPastCont
        '
        resources.ApplyResources(Me.chkboxShowPastCont, "chkboxShowPastCont")
        Me.chkboxShowPastCont.Checked = True
        Me.chkboxShowPastCont.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxShowPastCont.Name = "chkboxShowPastCont"
        Me.chkboxShowPastCont.UseVisualStyleBackColor = True
        '
        'dgvCont
        '
        Me.dgvCont.AllowUserToAddRows = False
        Me.dgvCont.AllowUserToDeleteRows = False
        Me.dgvCont.AllowUserToResizeRows = False
        Me.dgvCont.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvCont.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvCont, "dgvCont")
        Me.dgvCont.Name = "dgvCont"
        Me.dgvCont.ReadOnly = True
        Me.dgvCont.RowHeadersVisible = False
        Me.dgvCont.RowTemplate.Height = 15
        Me.dgvCont.RowTemplate.ReadOnly = True
        Me.dgvCont.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCont.ShowCellToolTips = False
        Me.dgvCont.ShowEditingIcon = False
        '
        'butAdd
        '
        resources.ApplyResources(Me.butAdd, "butAdd")
        Me.butAdd.Name = "butAdd"
        Me.butAdd.UseVisualStyleBackColor = True
        '
        'butMod
        '
        resources.ApplyResources(Me.butMod, "butMod")
        Me.butMod.Name = "butMod"
        Me.butMod.UseVisualStyleBackColor = True
        '
        'butDel
        '
        resources.ApplyResources(Me.butDel, "butDel")
        Me.butDel.Name = "butDel"
        Me.butDel.UseVisualStyleBackColor = True
        '
        'butClose
        '
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'butVideo
        '
        resources.ApplyResources(Me.butVideo, "butVideo")
        Me.butVideo.Name = "butVideo"
        Me.butVideo.UseVisualStyleBackColor = True
        '
        'butPrint
        '
        resources.ApplyResources(Me.butPrint, "butPrint")
        Me.butPrint.Name = "butPrint"
        Me.butPrint.UseVisualStyleBackColor = True
        '
        'PrintDocument1
        '
        '
        'frm2SetClassContent
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.butVideo)
        Me.Controls.Add(Me.butPrint)
        Me.Controls.Add(Me.butDel)
        Me.Controls.Add(Me.butMod)
        Me.Controls.Add(Me.butAdd)
        Me.Controls.Add(Me.dgvCont)
        Me.Controls.Add(Me.chkboxShowPastCont)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label11)
        Me.Name = "frm2SetClassContent"
        CType(Me.dgvCont, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPastCont As System.Windows.Forms.CheckBox
    Friend WithEvents dgvCont As System.Windows.Forms.DataGridView
    Friend WithEvents butAdd As System.Windows.Forms.Button
    Friend WithEvents butMod As System.Windows.Forms.Button
    Friend WithEvents butDel As System.Windows.Forms.Button
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents butVideo As System.Windows.Forms.Button
    Friend WithEvents butPrint As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
End Class
