﻿Public Class frmShowupRate
    Dim dtClass As New DataTable
    Dim dtSubClass As New DataTable
    Dim dtContent As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstContent As New ArrayList
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmNameRepeatStu
    Private sUserName As String = frmMain.GetUsrName
    Private intSc As Integer = 0
    Private intContent As Integer = 0
    Private dateStart As Date = Now
    Private dateEnd As Date = Now

    Public Sub New()
        InitializeComponent()
        Me.Text = My.Resources.frmShowupRate
        'dtClass = frmMain.GetClassList
        'dtSubClass = frmMain.GetSubClassList
        'dtContent = frmMain.GetContents

        'For index As Integer = 0 To dtClass.Rows.Count - 1
        '    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
        '    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        'Next
        RefreshUsrData()
        cboxClass.SelectedIndex = -1
        dgv.Rows.Add("", "", "0", "0", "0%")
    End Sub
    Friend Sub RefreshUsrData()
        dtContent = frmMain.GetContents

        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        For index As Integer = 0 To dtClass.Rows.Count - 1
            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            
        Next
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = 0
        End If
    End Sub
    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1
            cboxSubClass.Text = ""
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            If Not chkboxShowAll.Checked Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                cboxContent.SelectedIndex = -1
            Else
                intSc = lstSubClass(cboxSubClass.SelectedIndex)
                intContent = -1
                dateStart = GetDateStart(dtpickFrom.Value)
                dateEnd = GetDateEnd(dtpickTo.Value)
                RefreshData()
            End If

        End If
    End Sub

    Private Sub frmShowupRate_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmShowupRate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim lst As Array = objCsol.GetClassRate(intSc, intContent, dateStart, dateEnd)
        dgv.Rows(0).Cells(c_SubClassNameColumnName).Value = cboxSubClass.SelectedItem
        dgv.Rows(0).Cells(c_ContentColumnName).Value = cboxContent.SelectedItem
        dgv.Rows(0).Cells(c_ExpectedCntColumnName).Value = lst(0)
        dgv.Rows(0).Cells(c_ActualCntColumnName).Value = lst(1)
        dgv.Rows(0).Cells(c_ShowupRateColumnName).Value = lst(2)
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then

            dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgv.Columns.Item(c_ContentColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_ContentColumnName).Visible = True
            dgv.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.classContent
            dgv.Columns.Item(c_ExpectedCntColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_ExpectedCntColumnName).Visible = True
            dgv.Columns.Item(c_ExpectedCntColumnName).HeaderText = My.Resources.expectedStuCnt
            dgv.Columns.Item(c_ActualCntColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_ActualCntColumnName).Visible = True
            dgv.Columns.Item(c_ActualCntColumnName).HeaderText = My.Resources.actualStuCnt
            dgv.Columns.Item(c_ShowupRateColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_ShowupRateColumnName).Visible = True
            dgv.Columns.Item(c_ShowupRateColumnName).HeaderText = My.Resources.showupRate
        End If
    End Sub

    Private Sub frmShowupRate_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExport.Click
       ExportDgvToExcel(dgv)
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        Try
            PrintDocument1.Print()
        Catch ex As Exception
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub cboxContent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxContent.SelectedIndexChanged
        If cboxContent.SelectedIndex > -1 Then
            intContent = lstContent(cboxContent.SelectedIndex)
            If cboxSubClass.SelectedIndex > -1 Then
                intSc = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_IDColumnName) = intContent Then
                        dateStart = dtContent.Rows(index).Item(c_StartColumnName)
                        dateEnd = dtContent.Rows(index).Item(c_EndColumnName)
                        dateStart = GetDateStart(dateStart)
                        dateEnd = GetDateEnd(dateEnd)
                        RefreshData()
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub chkboxShowAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowAll.CheckedChanged
        If chkboxShowAll.Checked = False Then
            lblDate.Visible = False
            dtpickFrom.Visible = False
            dtpickTo.Visible = False
        Else
            lblDate.Visible = True
            dtpickFrom.Visible = True
            dtpickTo.Visible = True
        End If
    End Sub
End Class