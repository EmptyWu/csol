﻿Public Class frmStuGradeMul
    Private dtPaper As New DataTable
    Private dtPaper2 As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstPaper As New ArrayList

    Private dtData As New DataTable
    Private dtData2 As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Private dtSchoolType As New DataTable
    Private dtSchool As New DataTable
    Private lstSch As New ArrayList
    Private lstPaperShown As New ArrayList

    Private intSearchIndex As Integer = 0
    Private strKw As String = ""

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmStuGradeMul
            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)

            InitList()
            InitSchList()
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If GetDateEnd(dtClass.Rows(index).Item(c_EndColumnName)) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        If lstClass.Count > 0 Then
            cboxClass.SelectedIndex = 0
        Else
            cboxClass.SelectedIndex = -1
        End If

    End Sub

    Private Sub InitSchList()
        dtSchoolType = frmMain.GetSchTypes
        dtSchool = frmMain.GetSchList
        cboxSchool.Items.Clear()
        For index As Integer = 0 To dtSchoolType.Rows.Count - 1
            cboxSchool.Items.Add(dtSchoolType.Rows(index).Item(c_NameColumnName))
            lstSch.Add(dtSchoolType.Rows(index).Item(c_IDColumnName))
        Next
        If dtSchoolType.Rows.Count > 1 Then
            cboxSchool.SelectedIndex = 2
        End If
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshPaperList()
    End Sub


    Private Sub RefreshPaperList()
        Try
            cboxSubClass.Items.Clear()
            lstSubClass.Clear()
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            If lstClass.Count = 0 Then
                Exit Sub
            End If
            intClass = lstClass(cboxClass.SelectedIndex)

            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClass.Add(row.Item(c_IDColumnName))
                        cboxSubClass.Items.Add(row.Item(c_NameColumnName))
                    End If
                Next
                dtPaper = objCsol.GetClassPaperList(intClass)
                dtPaper2 = dtPaper.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName)
            End If

            cboxSubClass.SelectedIndex = 0

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        chklstPaper.Items.Clear()
        lstPaperShown.Clear()
        lstPaper.Clear()
        dgv.DataSource = Nothing
        If cboxSubClass.SelectedIndex > -1 Then

            If cboxSubClass.SelectedIndex = 0 Then
                For Each row As DataRow In dtPaper2.Rows
                    lstPaperShown.Add(row.Item(c_IDColumnName))
                    chklstPaper.Items.Add(row.Item(c_NameColumnName), False)
                Next
            Else
                Dim id As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For Each row As DataRow In dtPaper.Rows
                    If row.Item(c_SubClassIDColumnName) = id Then
                        lstPaperShown.Add(row.Item(c_IDColumnName))
                        chklstPaper.Items.Add(row.Item(c_NameColumnName), False)
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub RefreshPaperData()
        Dim lst As New ArrayList
        If cboxSubClass.SelectedIndex = 0 Then
            For i As Integer = 1 To lstSubClass.Count - 1
                lst.Add(lstSubClass(i))
            Next
        ElseIf cboxSubClass.SelectedIndex > 0 Then
            lst.Add(lstSubClass(cboxSubClass.SelectedIndex))
        End If

        If lstPaper.Count = 0 Or lst.Count = 0 Then
            'ShowInfo(My.Resources.msgNoSelection)
            dgv.DataSource = Nothing
            Exit Sub
        End If


        'dtData2 = objCsol.GetStuGradesMul(lstPaper, lst, intClass)
        dtData2 = GetStuGradesMul(lstPaper, lst, intClass)
        MakeTable()

        dtData.DefaultView.RowFilter = "Cancel=0"                                       '100318
        dgv.DataSource = dtData.DefaultView
        LoadColumnText()

    End Sub

    Private Sub MakeTable()
        Try
            If dtData2.Rows.Count > 0 Then
                dtData = dtData2.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_EngNameColumnName, c_SeatNumColumnName, c_Cust2ColumnName, _
                                                     c_SchoolColumnName, c_PrimarySchColumnName, c_JuniorSchColumnName, _
                                                     c_HighSchColumnName, c_UniversityColumnName, c_SubClassNameColumnName, c_SubClassIDColumnName _
                                                     , c_CancelColumnName)                  '100318

                For i As Integer = 0 To lstPaper.Count - 1
                    dtData.Columns.Add("Paper" & i.ToString, GetType(System.String))
                    dtData.Columns.Add("Remarks" & i.ToString, GetType(System.String))
                Next
                Dim mark As Single = 0
                Dim remarks As String = ""
                For Each row As DataRow In dtData.Rows
                    For i As Integer = 0 To lstPaper.Count - 1S
                        GetMark(mark, remarks, row.Item(c_IDColumnName), lstPaper(i))
                        If mark <> c_NullMark Then
                            If CSng(CInt(mark)) = mark Then
                                row.Item("Paper" & i.ToString) = CInt(mark).ToString
                            Else
                                row.Item("Paper" & i.ToString) = mark.ToString("##.00")
                            End If
                            row.Item("Remarks" & i.ToString) = remarks
                        End If
                    Next
                Next


            End If
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub
    '---------------------------------------------
    Public Function GetStuGrades(ByVal paperId As Integer, ByVal sc As ArrayList) As DataTable
        Dim dt As DataTable
        If sc.Count > 1 Then
            'dt = obj.GetStuGrades(paperId, sc(0))
            dt = GetStuGrades_sql(paperId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                'dt.Merge(obj.GetStuGrades(paperId, sc(index)))
                dt.Merge(GetStuGrades_sql(paperId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            'dt = obj.GetStuGrades(paperId, sc(0))
            dt = GetStuGrades_sql(paperId, sc(0))
        Else
            'dt = obj.GetStuGrades(paperId, -1)
            dt = GetStuGrades_sql(paperId, -1)
        End If

        Return dt

    End Function

    Public Function GetStuGrades_sql(ByVal PaperID As Integer, ByVal sc As Integer) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("PaperID", PaperID)
        RequestParams.Add("sc", sc)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("StuGrade", "GetStuGrades_sql", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetStuGrades_sql").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetStuGrades_sql"))
        Catch ex As Exception
            CSOL.Logger.LogError(ex.Message)
        Finally
        End Try
        Return dt
    End Function

    Public Function GetStuGradesMul(ByVal paperId As ArrayList, ByVal sc As ArrayList, ByVal c As Integer) As DataTable
        'Dim obj As New ExamPaper
        Dim dt As DataTable
        Dim dtPaper As DataTable = GetClassPaperList(c)
        Dim lst As New ArrayList
        Dim s As Integer = 0
        Dim paper As Integer = 0

        If paperId.Count > 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
            For index As Integer = 1 To paperId.Count - 1
                paper = paperId(index)
                lst = New ArrayList
                For i As Integer = 0 To sc.Count - 1
                    s = sc(i)
                    If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                               c_IDColumnName & "=" & paper.ToString) > 0 Then
                        lst.Add(s)
                    End If
                Next
                dt.Merge(GetStuGrades(paper, lst))
            Next
        ElseIf paperId.Count = 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
        Else
            dt = GetStuGrades(-1, lst)
        End If

        Return dt

    End Function

    Public Function GetClassPaperList(ByVal classId As Integer) As System.Data.DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("classid", intClass)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("StuGrade", "GetClassPaperList", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetClassPaperList").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetClassPaperList"))
        Catch ex As Exception
            CSOL.Logger.LogError(ex.Message)
        Finally
        End Try
        Return dt

    End Function

    '---------------------------------------------
    Private Sub GetMark(ByRef mark As Single, ByRef remarks As String, ByVal stuid As String, ByVal paper As Integer)

        Dim c As Array = dtData2.Select(c_IDColumnName & "=" & stuid & " AND " & _
                                               c_PaperIDColumnName & "=" & paper.ToString)
        If c.Length > 0 Then
            For Each row As DataRow In c
                mark = GetDbSingle(row.Item(c_MarkColumnName))
                remarks = GetDbString(row.Item(c_RemarksColumnName))
                Exit For
            Next
        Else
            mark = c_NullMark
            remarks = ""
        End If
    End Sub


    Private Sub RefreshSchDisplay()
        Dim sch As Integer = 0
        If cboxSchool.SelectedIndex > -1 Then
            sch = lstSch(cboxSchool.SelectedIndex)
        End If
        Select Case sch
            Case 1
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_PrimarySchColumnName).Value)
                Next
            Case 2
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_JuniorSchColumnName).Value)
                Next
            Case 3
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_HighSchColumnName).Value)
                Next
            Case 4
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_UniversityColumnName).Value)
                Next
            Case Else
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_PrimarySchColumnName).Value)
                Next
        End Select
    End Sub

    Private Sub LoadColumnText()
        For Each col As DataGridViewColumn In dgv.Columns
            col.Visible = False
            col.ReadOnly = True
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            If radbutChineseName.Checked Then
                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name
            ElseIf radbutEngName.Checked Then
                dgv.Columns.Item(c_EngNameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_EngNameColumnName).Visible = True
                dgv.Columns.Item(c_EngNameColumnName).HeaderText = My.Resources.engName
            End If
            dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_SeatNumColumnName).Visible = True
            dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum

            dgv.Columns.Item(c_Cust2ColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_Cust2ColumnName).Visible = True
            dgv.Columns.Item(c_Cust2ColumnName).HeaderText = My.Resources.classNum

            RefreshSchDisplay()
            dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_SchoolColumnName).Visible = True
            dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            If lstSubClass.Count > 1 Then

                dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 5
                dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            End If
            Dim idx As Integer = 6
            For i As Integer = 0 To lstPaper.Count - 1
                dgv.Columns.Item("Paper" & i.ToString).DisplayIndex = idx
                dgv.Columns.Item("Paper" & i.ToString).Visible = True
                dgv.Columns.Item("Paper" & i.ToString).HeaderText = GetPaperName(lstPaper(i))
                dgv.Columns.Item("Paper" & i.ToString).ReadOnly = False
                idx = idx + 1
                If chkboxShowRemarks.Checked Then
                    dgv.Columns.Item("Remarks" & i.ToString).DisplayIndex = idx
                    dgv.Columns.Item("Remarks" & i.ToString).Visible = True
                    dgv.Columns.Item("Remarks" & i.ToString).HeaderText = My.Resources.remarks
                    dgv.Columns.Item("Remarks" & i.ToString).ReadOnly = False
                    idx = idx + 1
                End If
            Next
            

        End If
    End Sub

    Private Function GetPaperName(ByVal i As Integer) As String
        Dim idx As Integer = lstPaperShown.IndexOf(i)
        If idx > -1 Then
            Return chklstPaper.Items(idx)
        Else
            Return ""
        End If
    End Function

    Private Function GetSchoolName(ByVal i As Integer) As String
        Dim r As String = ""
        If dtSchool.Rows.Count > 0 Then
            For index As Integer = 0 To dtSchool.Rows.Count - 1
                If dtSchool.Rows(index).Item(c_IDColumnName) = i Then
                    r = dtSchool.Rows(index).Item(c_NameColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub cboxSchool_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxSchool.SelectedIndexChanged
        If cboxSchool.SelectedIndex > -1 Then
            RefreshSchDisplay()
        End If
    End Sub

    Private Sub radbutChineseName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutChineseName.CheckedChanged
        LoadColumnText()
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmStuGradeMul_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmStuGradeMul_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuGradeMul_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        dgv.BeginEdit(True)
    End Sub

    Private Sub dgv_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        Dim intC As Integer
        Dim intR As Integer
        Dim stuid As String = ""
        Dim paper As Integer
        Dim mark As Single
        Dim remarks As String = ""
        Dim idx As Integer = 0
        Dim sc As Integer = 0

        Try
            intC = e.ColumnIndex
            intR = e.RowIndex
            If intC > 10 Then

                idx = (intC - 11) \ 2 - 1

                stuid = dgv.Rows(intR).Cells(c_IDColumnName).Value
                paper = lstPaper(idx)
                sc = dgv.Rows(intR).Cells(c_SubClassIDColumnName).Value

                If DBNull.Value.Equals(dgv.Rows(intR).Cells("Paper" & idx.ToString).Value) Then
                    If DBNull.Value.Equals(dgv.Rows(intR).Cells("Remarks" & idx.ToString).Value) Then
                        objCsol.DeleteStuGrade(stuid, paper)
                        Exit Sub
                    ElseIf dgv.Rows(intR).Cells("Remarks" & idx.ToString).Value.ToString.Trim = "" Then
                        objCsol.DeleteStuGrade(stuid, paper)
                        Exit Sub
                    Else
                        mark = c_NullMark
                    End If
                Else
                    If Not IsNumeric(dgv.Rows(intR).Cells("Paper" & idx.ToString).Value) Then
                        dgv.Rows(intR).Cells("Paper" & idx.ToString).Value = ""
                        Exit Sub
                    End If
                    mark = CSng(dgv.Rows(intR).Cells("Paper" & idx.ToString).Value)
                    remarks = GetDbString(dgv.Rows(intR).Cells("Remarks" & idx.ToString).Value)
                End If
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                       c_IDColumnName & "=" & paper.ToString) = 0 Then
                    ShowInfo(My.Resources.msgScNoPaper)
                    dgv.Rows(intR).Cells("Paper" & idx.ToString).Value = ""
                    dgv.Rows(intR).Cells("Remarks" & idx.ToString).Value = ""
                    Exit Sub
                End If
            End If

        Catch ex As Exception
            ShowInfo(My.Resources.msgCheckDataFormat)
            CSOL.Logger.LogError(ex.Message)
            Exit Sub
        End Try

        Try
            If stuid.Length = 8 Then
                If radbutNow.Checked Then
                    objCsol.UpdateStuGrade(stuid, paper, mark, Now, remarks, frmMain.GetUsrId)
                Else
                    objCsol.UpdateStuGrade(stuid, paper, mark, dtPickInput.Value, remarks, frmMain.GetUsrId)
                End If

            End If

        Catch ex As Exception
            CSOL.Logger.LogError(ex.Message)
        End Try


    End Sub

    Private Sub chklstPaper_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstPaper.ItemCheck
        Dim intC As Integer
        intC = lstPaperShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstPaper.Add(intC)
        Else
            lstPaper.Remove(intC)
        End If

        RefreshPaperData()
    End Sub

    Private Sub chkboxShowRemarks_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowRemarks.CheckedChanged
        LoadColumnText()
    End Sub

    Private Sub mnuNewPaper_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNewPaper.Click
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2ExamPaper(-1, True, "", "", Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshPaperList()
            dgv.DataSource = Nothing
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(12)
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub


    Private Sub linklblAddPaper_LinkClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.LinkLabelLinkClickedEventArgs) Handles linklblAddPaper.LinkClicked
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2ExamPaper(-1, True, "", "", Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshPaperList()
            dgv.DataSource = Nothing
            frmMain.SetOkState(False)
        End If
    End Sub
End Class