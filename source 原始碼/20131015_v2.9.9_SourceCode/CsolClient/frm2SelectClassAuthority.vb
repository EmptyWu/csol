﻿Public Class frm2SelectClassAuthority
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable

    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList

    Private lstSubClass As New ArrayList
    Private lstSubClassShown As New ArrayList

    Public Sub New()
        InitializeComponent()
        InitClassList()
        frmMain.SetOkState(False)
        'radbutAll.Checked = True
        radbutSelected.Checked = True
    End Sub

    Private Sub InitClassList()
        Try
            dtClass = frmMain.GetClassList
            dtSubClass = frmMain.GetSubClassList

            If Not chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                        chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            End If
            If chklstClass.Items.Count > 0 Then
                chklstClass.SelectedIndex = -1
            End If

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassList()
    End Sub

    Private Sub RefreshClassList()
        Try
            If dtClass.Rows.Count = 0 Then
                Exit Sub
            End If

            chklstClass.Items.Clear()
            chklstSubClass.Items.Clear()
            lstClass.Clear()
            lstSubClass.Clear()
            lstSubClassShown.Clear()

            If Not chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                        chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            End If
            If chklstClass.Items.Count > 0 Then
                chklstClass.SelectedIndex = -1
            End If

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub chklstClass_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chklstClass.SelectedIndexChanged
        chklstSubClass.Items.Clear()
        lstSubClassShown.Clear()
        For i As Integer = 0 To chklstClass.Items.Count - 1
            If chklstClass.GetItemChecked(i) Then
                Dim j As Integer = lstClass(i)
                For Index As Integer = 0 To dtSubClass.Rows.Count - 1
                    If dtSubClass.Rows(Index).Item(c_ClassIDColumnName) = j Then
                        chklstSubClass.Items.Add(dtSubClass.Rows(Index).Item(c_NameColumnName).trim, True)
                        lstSubClassShown.Add(dtSubClass.Rows(Index).Item(c_IDColumnName))
                    End If
                Next
            End If
        Next
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        Dim classID As New ArrayList
        Dim className As New ArrayList
        For i As Integer = 0 To chklstClass.Items.Count - 1
            If chklstClass.GetItemChecked(i) Then
                Dim j As Integer = lstClass(i)
                classID.Add(j)
                className.Add(chklstClass.Items(i))

                RefreshSubClassList()
                If lstSubClass.Count > 0 Then
                    frmMain.SetCurrentList(lstSubClass)
                Else
                    MsgBox(My.Resources.msgNoSubClassSelected, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                End If
            End If
        Next
        frmMain.SetCurrentValueArr(classID)
        frmMain.SetCurrentStringArr(className)
        frmMain.SetOkState(True)
        Me.Close()
    End Sub
    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub radbutAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutAll.CheckedChanged
        If radbutAll.Checked Then
            For i As Integer = 0 To chklstSubClass.Items.Count - 1
                chklstSubClass.SetItemChecked(i, True)
            Next
        End If
    End Sub


    Private Sub radbutSelected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutSelected.CheckedChanged
        If radbutSelected.Checked Then
            For i As Integer = 0 To chklstSubClass.Items.Count - 1
                chklstSubClass.SetItemChecked(i, False)
            Next
        End If
    End Sub
End Class