﻿Public Class frmModifyClassRoom
    Private intRowCnt As Integer = 1
    Private intColCnt As Integer = 1
    Private intPwCnt As Integer = 0
    Private intState As Integer = -1
    Private intDisStyle As Integer = 1
    Private intId As Integer = 0
    Private dtSeatNaList As New DataTable

    Private lstColumn As New ArrayList
    Private lstRow As New ArrayList
    Private lstPw As New ArrayList
    Private lstConvex As New ArrayList
    Private lstNaCol As New ArrayList
    Private lstNaRow As New ArrayList

    'Private lstAbc() As Char = New String("ABCDEFGHIJKLMNOPQRSTUVWXYZ").ToArray

    Private lstAbc() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,AA,AB,AC,AD,AE,AF,AG,AH,AI,AJ,AK,AL,AM,AN,AO,AP,AQ,AR,AS,AT,AU,AV,AW,AX,AY,AZ".Split(",")


    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmModifyClassRoom
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New(ByVal id As Integer, ByVal name As String, _
                                   ByVal row As Integer, ByVal col As Integer, _
                                   ByVal style As Integer)
        InitializeComponent()

        Me.Text = My.Resources.frmModifyClassRoom
        intRowCnt = row
        intColCnt = col
        intId = id
        intDisStyle = style
        SetDisStyle(intDisStyle)
        tboxColCnt.Text = intColCnt.ToString
        tboxRowCnt.Text = intRowCnt.ToString
        tboxName.Text = name

        RefreshData()
        InitTable()
        LoadColumnText()
    End Sub

    Private Sub InitData()
        lstColumn = New ArrayList
        lstRow = New ArrayList
        lstPw = New ArrayList
        lstNaCol = New ArrayList
        lstNaRow = New ArrayList
        lstConvex = New ArrayList

    End Sub

    Private Sub frmModifyClassRoom_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmModifyClassRoom_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSeatMap_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        If intId > 0 Then
            Dim strCompute1 As String = "COUNT(" + c_ColumnNumColumnName + ")"
            Dim strCompute2 As String = c_StateColumnName & "=0"

            Try
                dtSeatNaList = New DataTable
                dtSeatNaList = objCsol.GetSeatNaList(intId)

                intPwCnt = dtSeatNaList.Compute(strCompute1, strCompute2)
                InitData()

                Dim intS = 0
                For index As Integer = 0 To dtSeatNaList.Rows.Count - 1
                    intS = dtSeatNaList.Rows(index).Item(c_StateColumnName)
                    If intS = c_SeatNaStatePw Then
                        lstPw.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                    ElseIf intS = c_SeatNaStateConvex Then
                        lstConvex.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    ElseIf intS = c_SeatNaStateNotseat Then
                        lstNaCol.Add(CInt(dtSeatNaList.Rows(index).Item(c_ColumnNumColumnName)))
                        lstNaRow.Add(CInt(dtSeatNaList.Rows(index).Item(c_RowNumColumnName)))
                    End If
                Next

            Catch ex As ApplicationException
                CSOL.Logger.LogError(ex.Message)
            End Try
        End If

    End Sub

    Private Sub InitTable()
        dgvSeat.Rows.Clear()
        dgvSeat.Columns.Clear()
        intPwCnt = lstPw.Count

        dgvSeat.ColumnCount = intColCnt + intPwCnt
        dgvSeat.RowCount = intRowCnt

        FillArrayList(1, intColCnt + intPwCnt, lstColumn)
        FillArrayList(1, intRowCnt, lstRow)

        Dim i As Integer = 0

        Select Case intDisStyle
            Case 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        lstColumn(index) = lstAbc(i).ToString
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next

            Case 2
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i + 1 < 10 Then
                            lstColumn(index) = "0" & (i + 1).ToString
                        Else
                            lstColumn(index) = (i + 1).ToString
                        End If
                        i = i + 1
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    lstRow(index) = lstAbc(index).ToString
                Next

            Case 3
                i = lstAbc.Count - 1
                For index As Integer = 0 To lstColumn.Count - 1
                    If lstPw.IndexOf(index) > -1 Then
                        lstColumn(index) = ""
                    Else
                        If i > 0 Then
                            lstColumn(index) = lstAbc(i).ToString
                            i = i - 1
                        End If
                    End If
                Next

                For index As Integer = 0 To lstRow.Count - 1
                    If index + 1 < 10 Then
                        lstRow(index) = "0" & (index + 1).ToString
                    Else
                        lstRow(index) = (index + 1).ToString
                    End If
                Next
        End Select

        For index As Integer = 0 To dgvSeat.ColumnCount - 1
            If index < lstColumn.Count Then
                dgvSeat.Columns(index).HeaderText = lstColumn(index)
                If lstColumn(index) = "" Then
                    dgvSeat.Columns(index).DefaultCellStyle.BackColor = Color.Silver
                End If
                dgvSeat.Columns(index).Width = 40
                dgvSeat.Columns(index).SortMode = DataGridViewColumnSortMode.NotSortable
            End If
        Next

        For index As Integer = 0 To dgvSeat.RowCount - 1
            If index < lstRow.Count Then
                dgvSeat.Rows(index).HeaderCell.Value = lstRow(index)
            End If
            If lstConvex.IndexOf(index) > -1 Then
                dgvSeat.Rows(index).HeaderCell.Style.BackColor = Color.Aqua
            End If
        Next

        If lstNaCol.Count > 0 Then
            For index As Integer = 0 To lstNaCol.Count - 1
                dgvSeat.Rows(lstNaRow(index)).Cells(lstNaCol(index)).value = c_SeatNaText
            Next
        End If

        dgvSeat.RowHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft

    End Sub

    Private Sub LoadColumnText()
        'To be added
    End Sub

    Private Sub tboxColCnt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxColCnt.TextChanged

        If Not tboxColCnt.Text = "" Then



            If CInt(tboxColCnt.Text) > 1 Then

                intColCnt = CInt(tboxColCnt.Text)
                InitData()
                InitTable()
            End If
        End If
    End Sub

    Private Sub tboxRowCnt_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxRowCnt.TextChanged
        If Not tboxRowCnt.Text = "" Then
            If CInt(tboxRowCnt.Text) > 1 Then
                intRowCnt = CInt(tboxRowCnt.Text)
                InitData()
                InitTable()
            End If
        End If
    End Sub

    Private Sub mnuInsertPw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuInsertPw.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        If intIdx > -1 Then
            lstPw.Add(intIdx)
            For index As Integer = 0 To lstNaCol.Count - 1
                If lstNaCol(index) >= intIdx Then
                    lstNaCol(index) = lstNaCol(index) + 1
                End If
            Next
            InitTable()
        End If
    End Sub

    Private Sub mnuRemovePw_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuRemovePw.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        If intIdx > -1 And dgvSeat.Columns(intIdx).HeaderText = "" Then
            lstPw.Remove(intIdx)
            For index As Integer = 0 To lstNaCol.Count - 1
                If lstNaCol(index) > intIdx Then
                    lstNaCol(index) = lstNaCol(index) - 1
                End If
            Next
            InitTable()
        End If
    End Sub

    Private Sub mnuSetToNoSeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetToNoSeat.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        Dim intIdx2 As Integer = dgvSeat.CurrentCell.RowIndex

        If intIdx > -1 And Not dgvSeat.Columns(intIdx).HeaderText = "" Then
            If intIdx2 > -1 And Not dgvSeat.Rows(intIdx2).Cells(intIdx).Value = c_SeatNaText Then
                lstNaCol.Add(intIdx)
                lstNaRow.Add(intIdx2)
                dgvSeat.Rows(intIdx2).Cells(intIdx).Value = c_SeatNaText
            End If
        End If
    End Sub

    Private Sub mnuCancelNoSeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuCancelNoSeat.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        Dim intIdx2 As Integer = dgvSeat.CurrentCell.RowIndex
        Dim intIdx3 As Integer

        If intIdx > -1 And Not dgvSeat.Columns(intIdx).HeaderText = "" Then
            If intIdx2 > -1 And dgvSeat.Rows(intIdx2).Cells(intIdx).Value = c_SeatNaText Then
                intIdx3 = GetNaIdx(intIdx, intIdx2)
                If intIdx3 > -1 Then
                    lstNaCol.RemoveAt(intIdx3)
                    lstNaRow.RemoveAt(intIdx3)
                    dgvSeat.Rows(intIdx2).Cells(intIdx).Value = ""
                End If
            End If
        End If
    End Sub

    Private Function GetNaIdx(ByVal col As Integer, ByVal row As Integer)
        Dim idx As Integer = -1

        For index As Integer = 0 To lstNaCol.Count - 1
            If lstNaCol(index) = col And lstNaRow(index) = row Then
                idx = index
                Exit For
            End If
        Next
        Return idx
    End Function

    Private Sub mnuSetDisStyle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetDisStyle.Click
        Dim frm As New frm2SetClassRoomStyle
        frm.ShowDialog()
        If intDisStyle <> GetDisStyle() Then
            intDisStyle = GetDisStyle()
            InitTable()
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub mnuSetConvex_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetConvex.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        Dim intIdx2 As Integer = dgvSeat.CurrentCell.RowIndex

        If intIdx > -1 Then
            If intIdx2 > -1 And Not dgvSeat.Rows(intIdx2).HeaderCell.Style.BackColor = Color.Aqua Then
                lstConvex.Add(intIdx2)
                dgvSeat.Rows(intIdx2).HeaderCell.Style.BackColor = Color.Blue
            End If
        End If
    End Sub

    Private Sub mnuCancelConvex_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCancelConvex.Click
        Dim intIdx As Integer = dgvSeat.CurrentCell.ColumnIndex
        Dim intIdx2 As Integer = dgvSeat.CurrentCell.RowIndex
        Dim intIdx3 As Integer
        If intIdx > -1 Then
            If intIdx2 > -1 And dgvSeat.Rows(intIdx2).HeaderCell.Style.BackColor = Color.Aqua Then
                intIdx3 = lstConvex.IndexOf(intIdx2)
                If intIdx3 > -1 Then
                    lstConvex.RemoveAt(intIdx3)
                    dgvSeat.Rows(intIdx2).HeaderCell.Style.BackColor = Color.Silver
                End If
            End If
        End If
    End Sub

    Private Sub mnuSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSave.Click
        If tboxName.Text = "" Then
            MsgBox(My.Resources.msgNoCrName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        Else
            objCsol.ModifyClassRoom(intRowCnt, intColCnt, tboxName.Text, _
                             intDisStyle, lstPw, lstNaCol, lstNaRow, _
                                lstConvex, intId)
            frmMain.RefreshClassRoomListFrm()
            Me.Close()
        End If
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvSeat.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvSeat.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvSeat.Rows.Count

            Dim oRow As DataGridViewRow = dgvSeat.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvSeat.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvSeat.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvSeat.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            If oCell.Value Is Nothing Then
                                e.Graphics.DrawString("", oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            Else
                                e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            End If

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvSeat.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvSeat.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvSeat.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvSeat.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

End Class