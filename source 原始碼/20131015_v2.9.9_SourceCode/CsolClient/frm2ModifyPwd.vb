﻿Public Class frm2ModifyPwd

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        If tboxAccount.Text.Trim.Length > 0 Then
            If tboxPwd1.Text.Trim.Length > 0 And tboxPwd1.Text.Trim.Length < 12 Then
                If tboxPwd1.Text.Trim = tboxPwd2.Text.Trim Then
                    Dim r As Integer = objCsol.ChgUsrPwd(tboxAccount.Text.Trim, _
                                             tboxOldPwd.Text.Trim, tboxPwd1.Text.Trim)
                    If r = 1 Then
                        MsgBox(My.Resources.msgModifySuccess, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                        Me.Close()
                    Else
                        MsgBox(My.Resources.msgModifyFail, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                    End If
                Else
                    MsgBox(My.Resources.msgNewPwdMismatch, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                End If
            Else
                MsgBox(My.Resources.msgInvalidPwdLength, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        Else
            MsgBox(My.Resources.msgInvalidAccnt, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub
End Class