﻿Public Class frmSetAssignment
    Private dtAssign As New DataTable
    Private dt As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private ds As New DataSet

    Public Sub New()
        InitializeComponent()
        Try
            Me.Text = My.Resources.frmSetAssignment
            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            InitList()
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub frmSetAssignment_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetAssignment_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub InitList()
        Try
            cboxClass.Items.Clear()
            lstClass.Clear()
            cboxClass.Items.Add(My.Resources.all)
            lstClass.Add(-1)
            If chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                        cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            End If
            cboxClass.SelectedIndex = -1

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub RefreshData()
        Try
            If lstClass.Count > 0 And cboxClass.SelectedIndex > -1 Then
                Dim lst As New ArrayList
                If cboxClass.SelectedIndex > 0 Then
                    lst.Add(lstClass(cboxClass.SelectedIndex))
                Else
                    lst = lstClass
                End If
                dt = objCsol.GetClassAssignment(lst)
                dtAssign = dt.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_ClassNameColumnName, _
                                                  c_StartDateColumnName, c_ContentColumnName, c_StuCntColumnName, _
                                                  c_EndDateColumnName, c_ContentIDColumnName)

                dgv.DataSource = dtAssign

                LoadColumnText()
            Else
                dgv.DataSource = Nothing
            End If
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub LoadColumnText()
        Try
            For Each col In dgv.Columns
                col.visible = False
            Next
            If dgv.Rows.Count > 0 Then
                dgv.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_ClassNameColumnName).Visible = True
                dgv.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.assignmentName
                dgv.Columns.Item(c_ContentColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_ContentColumnName).Visible = True
                dgv.Columns.Item(c_ContentColumnName).HeaderText = My.Resources.linkContent
                dgv.Columns.Item(c_StartDateColumnName).DisplayIndex = 3
                dgv.Columns.Item(c_StartDateColumnName).Visible = True
                dgv.Columns.Item(c_StartDateColumnName).HeaderText = My.Resources.assignmentDate
                dgv.Columns.Item(c_StartDateColumnName).DefaultCellStyle.Format = "d"
                dgv.Columns.Item(c_EndDateColumnName).DisplayIndex = 4
                dgv.Columns.Item(c_EndDateColumnName).Visible = True
                dgv.Columns.Item(c_EndDateColumnName).HeaderText = My.Resources.assignmentDue
                dgv.Columns.Item(c_EndDateColumnName).DefaultCellStyle.Format = "d"

                dgv.Columns.Item(c_StuCntColumnName).DisplayIndex = 5
                dgv.Columns.Item(c_StuCntColumnName).Visible = True
                dgv.Columns.Item(c_StuCntColumnName).HeaderText = My.Resources.stuPassCnt
            End If
            RefreshIndexDisplay()
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub frmSetAssignment_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2Assignment(-1, True, "", Now, Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim stuCount As Integer = dgv.SelectedRows(0).Cells(c_StuCntColumnName).Value
            If stuCount = 0 Then
                If MsgBox("確定刪除 ?", MsgBoxStyle.OkCancel, My.Resources.msgRemindTitle) = MsgBoxResult.Ok Then
                    objCsol.DeleteAssignment(id)
                    RefreshData()
                End If
            Else
                MsgBox("已有學生作業通過資料，無法刪除 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub mnuMod_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuMod.Click
        Try
            If dgv.SelectedRows.Count > 0 Then
                Dim lst As New ArrayList
                Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value

                For index As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(index).Item(c_IDColumnName) = id Then
                        lst.Add(dt.Rows(index).Item(c_SubClassIDColumnName))
                    End If
                Next
                frmMain.SetOkState(False)
                Dim frm As New frm2Assignment(id, False, dgv.SelectedRows(0).Cells(c_NameColumnName).Value, _
                                              dgv.SelectedRows(0).Cells(c_EndDateColumnName).Value, _
                                          dgv.SelectedRows(0).Cells(c_StartDateColumnName).Value, _
                                         dgv.SelectedRows(0).Cells(c_ContentIDColumnName).Value, lst)
                frm.ShowDialog()
                If frmMain.GetOkState Then
                    RefreshData()
                    frmMain.SetOkState(False)
                End If
            End If
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub


    Private Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        RefreshIndexDisplay()
    End Sub


    Private Sub RefreshIndexDisplay()
        Dim total As String = dgv.Rows.Count.ToString
        Dim current As String = ""
        If dgv.Rows.Count > 0 Then
            If Not dgv.CurrentRow Is Nothing Then
                current = (dgv.CurrentRow.Index + 1).ToString()
                tboxIndex.Text = current & "/" & total
            Else
                tboxIndex.Clear()
            End If
        Else
            tboxIndex.Clear()
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        Dim frm As New frm2SearchAssignment
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To dgv.Rows.Count - 1
                If dgv.Rows(index).Cells(c_NameColumnName).Value.ToString.Contains(name) Then
                    dgv.CurrentCell = dgv.Rows(index).Cells(c_NameColumnName)
                    Exit Sub
                End If
            Next
        End If
    End Sub
End Class