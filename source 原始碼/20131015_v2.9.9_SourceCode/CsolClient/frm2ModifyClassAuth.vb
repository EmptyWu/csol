﻿Public Class frm2ModifyClassAuth
    Private intClass As Integer
    Private dtSubClass As New DataTable
    Private lstSubClassShown As New ArrayList
    Private lstSubClass As New ArrayList

    Public Sub New(ByVal id As Integer, ByVal lst As ArrayList)

        InitializeComponent()
        intClass = id
        lstSubClass = lst
        InitClassList()

        frmMain.SetOkState(False)
    End Sub

    Private Sub InitClassList()
        Try
            dtSubClass = frmMain.GetSubClassList
            lstSubClassShown.Clear()
            chklstSubClass.Items.Clear()
            For Index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(Index).Item(c_ClassIDColumnName) = intClass Then
                    chklstSubClass.Items.Add(dtSubClass.Rows(Index).Item(c_NameColumnName).trim, False)
                    lstSubClassShown.Add(dtSubClass.Rows(Index).Item(c_IDColumnName))
                End If
            Next

            'If lstSubClass.Count = 0 Then
            radbutAll.Checked = True
            ' Else
            'radbutSelected.Checked = True
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If lstSubClass.IndexOf(lstSubClassShown(index)) > -1 Then
                    chklstSubClass.SetItemChecked(index, True)
                End If
            Next
            ' End If
        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        'If radbutAll.Checked Then
        '    frmMain.SetCurrentValue(-1)
        '    frmMain.SetOkState(True)
        '    Me.Close()
        'Else
        'If radbutSelected.Checked Then
        RefreshSubClassList()
        If lstSubClass.Count > 0 Then
            'frmMain.SetCurrentValue(0)
            frmMain.SetCurrentValue(intClass)
            frmMain.SetCurrentList(lstSubClass)
            frmMain.SetOkState(True)
            Me.Close()
        Else
            MsgBox(My.Resources.msgNoSubClassSelected, _
                    MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
        'End If
    End Sub

    Private Sub radbutAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutAll.CheckedChanged
        If radbutAll.Checked Then
            For i As Integer = 0 To chklstSubClass.Items.Count - 1
                chklstSubClass.SetItemChecked(i, True)
            Next
        End If
    End Sub

    Private Sub radbutSelected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutSelected.CheckedChanged
        If radbutSelected.Checked Then
            For i As Integer = 0 To chklstSubClass.Items.Count - 1
                chklstSubClass.SetItemChecked(i, False)
            Next
        End If
    End Sub
End Class