﻿Public Class frm2UpdClass
    Private intId As Integer


    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not tboxName.Text.Trim = "" And intId > -1 Then
            Dim intFee As Integer
            If IsNumeric(tboxFee.Text) Then
                intFee = CInt(tboxFee.Text)
                If intFee < 0 Then
                    intFee = 0
                End If
            Else
                intFee = 0
            End If

            If intFee = 0 Then
                Dim result As MsgBoxResult = _
                            MsgBox(My.Resources.msgClassFeeEmpty, _
                                    MsgBoxStyle.YesNo, My.Resources.msgTitle)
                If result = MsgBoxResult.Yes Then

                    objCsol.ModifyClass(intId, tboxName.Text, GetDateStart(dtpickFrom.Value), _
                             GetDateEnd(dtpickTo.Value), intFee)
                    frmMain.SetOkState(True)
                    Me.Close()
                End If
            Else
                objCsol.ModifyClass(intId, tboxName.Text, GetDateStart(dtpickFrom.Value), _
                             GetDateEnd(dtpickTo.Value), intFee)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        Else
            ShowMsg()
        End If
        frmMain.BackgroundWorker7.RunWorkerAsync()
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoClassName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Public Sub New(ByVal ID As Integer, ByVal Name As String, _
                           ByVal dateStart As Date, _
                         ByVal dateEnd As Date, ByVal Fee As Integer)

        InitializeComponent()
        intId = ID
        tboxName.Text = Name
        dtpickFrom.Value = dateStart
        dtpickTo.Value = dateEnd
        tboxFee.Text = Fee.ToString

    End Sub
End Class