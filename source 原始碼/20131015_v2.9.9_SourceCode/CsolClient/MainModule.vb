﻿Imports System.Xml
Imports System.Configuration
Imports System.Runtime.Remoting
Imports System.Runtime.Remoting.Channels
Imports System.Runtime.Remoting.Channels.Tcp
Imports Csol

Module MainModule
    Friend objCsol As ICsol
    Friend frmMain As mdiMain

    Public Sub Main()

        SetCfgFilePath()
        Dim channel As New TcpClientChannel()
        ChannelServices.RegisterChannel(channel, False)
        RemotingConfiguration.RegisterWellKnownClientType(GetType(ICsol), GetServerUrl())

        Dim objRemote As Object
        objRemote = Activator.GetObject(GetType(ICsol), GetServerUrl())
        objCsol = CType(objRemote, ICsol)


        '20100424 auto update by sherry start
        ExeRefresh()
        'GetUpdates()
        '20100424 auto update by sherry end

        frmMain = New mdiMain
        frmMain.Show()
        If Not frmMain Is Nothing Then
            If frmMain.Created = True Then
                Application.EnableVisualStyles() 'modified by owen 0119
                Application.Run(frmMain)
            End If
        End If


    End Sub
    '20100424 auto update by sherry start
    Private Sub ExeRefresh()
        Dim s1 As String
        Dim s2 As String
        Dim s3 As String
        Dim s4 As String
        On Error Resume Next

        s1 = "TNT"
        If Len(Application.StartupPath) > 3 Then
            s1 = Application.StartupPath + "\" + Trim(c_AppName) + ".exe"
            s3 = Application.StartupPath + "\" + c_MidExeName + ".EXE"
        Else
            s1 = Application.StartupPath + Trim(c_AppName) + ".exe"
            s3 = Application.StartupPath + c_MidExeName + ".EXE"
        End If

        s4 = "TNT"
        s4 = FileDateTime(s1)
        s2 = "TNT"
        Dim ExePath As String = objCsol.GetAutoUpdatePath
        s2 = FileDateTime(ExePath + c_AppName + ".exe")
        If s2 = "TNT" Then
            'MsgBox("No share folder or application found for the newest CsolClient.exe on server.")
        End If

        If s2 = "TNT" Or s4 = "TNT" Then Exit Sub

        If CDate(s2) > CDate(s4) Then
            FileCopy(ExePath + c_MidExeName + ".EXE", s3)
            s1 = Shell(s3 + " " + ExePath + "," + c_AppName + ".EXE", vbNormalFocus)
            End
        End If
    End Sub
    '20100424 auto update by sherry end

    Private Sub GetUpdates()
        System.IO.Directory.CreateDirectory(String.Format("{0}\update", My.Application.Info.DirectoryPath))

        Dim bw As New System.ComponentModel.BackgroundWorker()
        AddHandler bw.DoWork, AddressOf bwUpdate_DoWork
        AddHandler bw.RunWorkerCompleted, AddressOf bwUpdate_RunWorkerCompleted
        bw.RunWorkerAsync()
    End Sub

    Private Sub bwUpdate_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
        Dim list() As String = objCsol.GetUpdateList(New Date(1980, 1, 1))
        For Each item As String In list
            Dim bs() As Byte = objCsol.GetUpdateFile(item)
            If bs IsNot Nothing Then
                Dim path As String = String.Format("{0}\update\{1}", My.Application.Info.DirectoryPath, item)
                System.IO.File.Delete(path)
                System.IO.File.WriteAllBytes(path, bs)
            End If
        Next

        Dim files() As String = System.IO.Directory.GetFiles(String.Format("{0}\update", My.Application.Info.DirectoryPath))
        For Each file As String In files
            Dim ext As String = System.IO.Path.GetExtension(file).ToLower()

            If ext = ".dll" Or ext = ".exe" Then
                'Do Nothing
            Else
                Dim dest As String = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, System.IO.Path.GetFileName(file))
                System.IO.File.Copy(file, dest, True)
            End If
        Next
    End Sub

    Private Sub bwUpdate_RunWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
        Dim path As String = String.Format("{0}\WaitForCopy.exe", My.Application.Info.DirectoryPath)
        If System.IO.File.Exists(path) Then
            Dim pi As New System.Diagnostics.ProcessStartInfo(path)
            pi.WindowStyle = ProcessWindowStyle.Hidden
            System.Diagnostics.Process.Start(pi)
        End If
    End Sub

    Private Function GetServerUrl() As String
        Dim xdConfig As New XmlDocument
        Dim strUrl As String
        strUrl = ""
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "SERVERURL") Then
                strUrl = xnTarget.Attributes("url").Value
            End If
        Next
        Return strUrl
    End Function


End Module
