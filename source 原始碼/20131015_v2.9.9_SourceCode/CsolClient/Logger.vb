﻿Namespace CSOL
    Public Class Logger
        Public Shared Sub LogMessage(ByVal msg As String)
            objCsol.ILogMessage(msg)
            'Dim file As String = String.Format("{0}\Log\Message_{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
            'EnsureFolder(file)
            'log(msg, file)
        End Sub
        Public Shared Sub LogMessage(ByVal id As String, ByVal msg As String)
            objCsol.ILogMessage(id & ":" & msg)
        End Sub
        Public Shared Sub LogError(ByVal msg As String)
            objCsol.ILogError(msg)
            'Dim file As String = String.Format("{0}\Log\Error_{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
            'EnsureFolder(file)
            'log(msg, file)
        End Sub
        Public Shared Sub LogError(ByVal id As String, ByVal msg As String)
            objCsol.ILogError(id & ":" & msg)
        End Sub

        Private Shared Sub log(ByVal msg As String, ByVal file As String)
            Dim text As String = String.Format("{0}{1}{2}{3}", Now.ToString("HH:mm:ss.fff"), vbTab, msg, vbCrLf)
            My.Computer.FileSystem.WriteAllText(file, text, True)

        End Sub

        Private Shared Sub EnsureFolder(ByVal file As String)
            Dim path As String = System.IO.Directory.GetParent(file).ToString
            If Not My.Computer.FileSystem.DirectoryExists(path) Then
                My.Computer.FileSystem.CreateDirectory(path)
            End If
        End Sub
    End Class
End Namespace

