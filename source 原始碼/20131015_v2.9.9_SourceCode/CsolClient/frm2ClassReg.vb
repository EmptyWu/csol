﻿Public Class frm2ClassReg
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dtSalesList As New DataTable
    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstSalesShown As New ArrayList
    Private strStuId As String
    Private intFee As Integer = 0
    Private strSeat As String = ""
    Private intSubClassId As Integer
    Private strSalesId As String
    Private lstSubClass As New ArrayList
    Private lstSubClassName As New ArrayList
    Private lstStuSubClass As New ArrayList

    Public Sub New(ByVal id As String)
        InitializeComponent()

        Me.Text = My.Resources.frmClassReg
        strStuId = id
        InitClassList()
        ShowClassList()
    End Sub

    Private Sub InitClassList()
        Try
            dtSubClassList = frmMain.GetSubClassList
            dtClassList = frmMain.GetClassList
            dtSalesList = frmMain.GetSalesList
            lstStuSubClass = frmMain.GetlstStuSubClass

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()

        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList
        lstSalesShown = New ArrayList

        chklstClass.Items.Clear()
        chklstSubClass.Items.Clear()
        lstboxSales.Items.Clear()

        Try
            Dim dateClass As Date
            Dim dateClass2 As Date

            For index As Integer = 0 To dtClassList.Rows.Count - 1
                dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                dateClass2 = dtClassList.Rows(index).Item(c_StartColumnName)
                If dateClass < Now And dateClass2 < Now Then
                    If chkboxPast.Checked = True Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim, False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If
                ElseIf dateClass2 <= Now And dateClass >= Now Then
                    If chkboxOpened.Checked = True Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim, False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If
                ElseIf dateClass2 > Now And dateClass > Now Then
                    If chkboxOpenNot.Checked = True Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim, False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If
                End If
            Next

            InitClassSelection()
            RefreshClassList()

            lstboxSales.Items.Add(My.Resources.none)
            lstSalesShown.Add("")
            For index As Integer = 0 To dtSalesList.Rows.Count - 1
                lstboxSales.Items.Add(dtSalesList.Rows(index).Item(c_NameColumnName).trim)
                lstSalesShown.Add(dtSalesList.Rows(index).Item(c_IDColumnName))
            Next

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            CSOL.Logger.LogError(ex.Message)
        End Try
    End Sub

    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            'chklstClass.SetItemChecked(0, True)
            'chklstClass.SelectedItem = chklstClass.Items(0)

            'If lstboxSubClass.Items.Count > 0 Then
            'lstboxSubClass.SelectedIndex = 0
            'End If
        End If
    End Sub

    Private Sub chkboxPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName).trim, False)
                End If
            Next
        Else
            lstClass.Remove(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClassList.Rows(index).Item(c_NameColumnName).trim)
                End If
            Next
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        frmMain.SetOkState(False)
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        lstSubClass.Clear()
        lstSubClassName.Clear()

        If chklstSubClass.Items.Count > 0 And strStuId.Length = 8 Then
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) Then
                    intSubClassId = lstSubClassShown(index)
                    lstSubClass.Add(intSubClassId)
                    lstSubClassName.Add(chklstSubClass.Items(index).ToString.Trim)
                End If
            Next
            For i As Integer = 0 To lstSubClass.Count - 1
                Dim subClassID As Integer = lstSubClass.Item(i)
                Dim subClassName As String = lstSubClassName.Item(i)
                For j As Integer = 0 To lstStuSubClass.Count - 1
                    Dim stuSubClassID As Integer = lstStuSubClass.Item(j)
                    If subClassID = stuSubClassID Then
                        MsgBox("請勿重複報名相同班級班別： " + subClassName + " !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                        Exit Sub
                    End If
                Next
            Next
            If lstSubClass.Count = 1 Then
                intSubClassId = lstSubClass(0)
                If lstboxSales.Items.Count > 0 And lstboxSales.SelectedIndex > 0 Then
                    strSalesId = lstSalesShown(lstboxSales.SelectedIndex)
                Else
                    strSalesId = ""
                End If
                frmMain.SetOkState(True)
                If chkboxShowSeat.Checked = True Then
                    frmMain.SetOkState(False)
                    Dim subfrmSeatMap As New frm2SeatMap(intSubClassId)
                    subfrmSeatMap.ShowDialog()
                End If
                If frmMain.GetOkState Then
                    AddClassReg()
                End If
            ElseIf lstSubClass.Count > 1 Then
                If lstboxSales.Items.Count > 0 And lstboxSales.SelectedIndex > 0 Then
                    strSalesId = lstSalesShown(lstboxSales.SelectedIndex)
                Else
                    strSalesId = ""
                End If
                frmMain.SetOkState(True)
                If chkboxShowSeat.Checked = True Then
                    frmMain.SetOkState(False)
                    Dim subfrmSeatMapMul As New frm2SeatMapMul(lstSubClass, lstSubClassName)
                    subfrmSeatMapMul.ShowDialog()
                End If
                If frmMain.GetOkState Then
                    frmMain.SetCurrentList(lstSubClass)
                    AddClassRegMul()
                End If

            End If
        End If
    End Sub

    Public Sub AddClassReg()
        intFee = frmMain.GetClassFee(intSubClassId)
        objCsol.AddClassReg(strStuId, intSubClassId, frmMain.GetSeatNum, 0, intFee, "", strSalesId)
        frmMain.SetSeatNum("")
        frmMain.SetOkState(True)
        Me.Close()
    End Sub

    Public Sub AddClassRegMul()
        Dim lstFee As New ArrayList
        lstSubClass = frmMain.GetCurrentList
        For index As Integer = 0 To lstSubClass.Count - 1
            intFee = frmMain.GetClassFee(lstSubClass(index))
            lstFee.Add(intFee)
        Next

        If lstSubClass.Count > 0 Then
            objCsol.AddClassReg(strStuId, lstSubClass, frmMain.GetSeatNum, 0, lstFee, "", strSalesId)
            frmMain.SetSeatNum("")
            frmMain.SetOkState(True)
            Me.Close()
        End If
    End Sub

    Private Sub chkboxOpened_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxOpened.CheckedChanged
        ShowClassList()
    End Sub


    Private Sub chkboxOpenNot_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxOpenNot.CheckedChanged
        ShowClassList()
    End Sub
End Class