﻿Public Class frmSetSchool
    Dim dtSchool As New DataTable

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSetSchool
        RefreshData()

    End Sub

    Private Sub frmSetSchool_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetSchool_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        dtSchool = frmMain.GetSchList
        RefreshTable()
        LoadColumnText()
    End Sub

    Private Sub RefreshTable()
        If dtSchool.Rows.Count > 0 Then
            If radbutNoFilter.Checked Then
                dtSchool.DefaultView.RowFilter = ""
            ElseIf radbutFilter1.Checked Then
                dtSchool.DefaultView.RowFilter = c_TypeIdColumnName & "=1"
            ElseIf radbutFilter2.Checked Then
                dtSchool.DefaultView.RowFilter = c_TypeIdColumnName & "=2"
            ElseIf radbutFilter3.Checked Then
                dtSchool.DefaultView.RowFilter = c_TypeIdColumnName & "=3"
            ElseIf radbutFilter4.Checked Then
                dtSchool.DefaultView.RowFilter = c_TypeIdColumnName & "=4"
            End If
            dgvSta.DataSource = dtSchool.DefaultView
        End If
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvSta.Columns
            col.visible = False
        Next
        If dgvSta.Rows.Count > 0 Then
            dgvSta.Columns.Item(c_NameColumnName).DisplayIndex = 0
            dgvSta.Columns.Item(c_NameColumnName).Visible = True
            dgvSta.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name
            dgvSta.Columns.Item(c_PhoneColumnName).DisplayIndex = 1
            dgvSta.Columns.Item(c_PhoneColumnName).Visible = True
            dgvSta.Columns.Item(c_PhoneColumnName).HeaderText = My.Resources.tel
            dgvSta.Columns.Item(c_AddressColumnName).DisplayIndex = 2
            dgvSta.Columns.Item(c_AddressColumnName).Visible = True
            dgvSta.Columns.Item(c_AddressColumnName).HeaderText = My.Resources.address
            dgvSta.Columns.Item(c_PersonContactColumnName).DisplayIndex = 3
            dgvSta.Columns.Item(c_PersonContactColumnName).Visible = True
            dgvSta.Columns.Item(c_PersonContactColumnName).HeaderText = My.Resources.contactPerson
            dgvSta.Columns.Item(c_StuCntColumnName).DisplayIndex = 4
            dgvSta.Columns.Item(c_StuCntColumnName).Visible = True
            dgvSta.Columns.Item(c_StuCntColumnName).HeaderText = My.Resources.headCnt
        End If
    End Sub

    Private Sub frmSetSchool_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub radbutNoFilter_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutNoFilter.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub radbutFilter1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutFilter1.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub radbutFilter2_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutFilter2.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub radbutFilter3_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutFilter3.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub radbutFilter4_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutFilter4.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        If Not frmMain.CheckAuth(80) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim frm As New frm2AddSchool(-1, "", 0, "", "", "")
        frm.ShowDialog()
        RefreshData()
    End Sub

    Private Sub mnuModify_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuModify.Click
        If Not frmMain.CheckAuth(81) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvSta.SelectedRows.Count > 0 Then
            Dim intId As Integer = dgvSta.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim Name As String = dgvSta.SelectedRows(0).Cells(c_NameColumnName).Value
            Dim Type As Integer = dgvSta.SelectedRows(0).Cells(c_TypeIdColumnName).Value
            Dim Phone As String = dgvSta.SelectedRows(0).Cells(c_PhoneColumnName).Value
            Dim address As String = dgvSta.SelectedRows(0).Cells(c_AddressColumnName).Value
            Dim contact As String = dgvSta.SelectedRows(0).Cells(c_PersonContactColumnName).Value

            Dim frm As New frm2AddSchool(intId, Name, Type, Phone, address, contact)
            frm.ShowDialog()
            RefreshData()
        End If
    End Sub

    Private Sub mnuRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuRemove.Click
        If Not frmMain.CheckAuth(82) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvSta.SelectedRows.Count > 0 Then
            Dim StuCnt As Integer = CInt(dgvSta.SelectedRows(0).Cells(c_StuCntColumnName).Value)
            If StuCnt = 0 Then
                Dim intId As Integer = dgvSta.SelectedRows(0).Cells(c_IDColumnName).Value
                objCsol.DeleteSchool(intId)
                frmMain.RefreshSchool()
                RefreshData()
            Else
                MessageBox.Show(My.Resources.msgDelSchStnConNotZero, My.Resources.msgDelSchTitle, MessageBoxButtons.OK)
            End If

        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgvSta)

    End Sub

    Private Sub mnuImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImport.Click
        Dim frm As New frm2ImportSch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        Dim frm As New frm2SchSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim name As String = frmMain.GetCurrentString
            For index As Integer = 0 To dgvSta.Rows.Count - 1
                If dgvSta.Rows(index).Cells(c_NameColumnName).Value.ToString.Contains(name) Then
                    dgvSta.CurrentCell = dgvSta.Rows(index).Cells(c_NameColumnName)
                    Exit Sub
                End If
            Next
        End If
    End Sub
End Class