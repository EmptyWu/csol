﻿Public Class frmMain

    Private Sub nicon_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles nicon.MouseDoubleClick
        If Me.Visible Then
            Me.Hide()
            mnuHide.Text = "Show"
        Else
            Me.Show()
            mnuHide.Text = "Hide"
            Me.WindowState = Windows.Forms.FormWindowState.Normal
        End If
    End Sub

    Private Sub mnuHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuHide.Click
        If mnuHide.Text.ToUpper = "SHOW" Then
            Me.Show()
            mnuHide.Text = "Hide"
            Me.WindowState = Windows.Forms.FormWindowState.Normal
        Else
            Me.Hide()
            mnuHide.Text = "Show"
        End If
    End Sub

    Private Sub mnuExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        nicon.Visible = False
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        nicon.Visible = False
        System.Windows.Forms.Application.Exit()
    End Sub

    Private Sub frmMain_SizeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.SizeChanged
        If Me.WindowState = Windows.Forms.FormWindowState.Minimized Then
            Me.Hide()
            mnuHide.Text = "Show"
        End If
    End Sub
End Class