﻿Namespace HTTPHandlers
    Public Class StuInfo
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "SaveItem"
                    SaveItem()
                Case "RemoveStu"
                    RemoveStu()
                Case "GetNextStuNo"
                    GetNextStuNo()
                Case "GetStu"
                    GetStu()
                Case "GetStuCompressed"
                    GetStuCompressed()
                Case "GetStuInfo"
                    GetStuInfo()
                Case "GetStuPicture"
                    GetStuPicture()
                Case "GetAddressBook"
                    GetAddressBook()
                Case "AddAddressBook"
                    AddAddressBook()
                Case "RemoveAddressBook"
                    RemoveAddressBook()
                Case "GetStuMulClass"
                    GetStuMulClass()
                Case "GetStuAssign"
                    GetStuAssign()
                Case "GetStuPaper"
                    GetStuPaper()
                    'Case "SearchStu"
                    '    SearchStu()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/StuInfo"
            End Get
        End Property

        Private Sub SaveItem()
            Dim PicturePath As String = String.Format("{0}\Images\StuPictures", My.Application.Info.DirectoryPath)
            System.IO.Directory.CreateDirectory(PicturePath)

            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim No As String = RequestParams("No")
                Dim Name As String = RequestParams("Name")
                Dim EngName As String = ""
                Dim Identity As String = RequestParams("Identity")
                Dim Type As String = RequestParams("Type")
                Dim Sex As String = RequestParams("Sex")
                Dim Birth As Date = Date.Parse(RequestParams("Birth"))
                Dim School As String = RequestParams("School")
                If School.Split(" ").Length <> 4 Then
                    Throw New Exception("Invalid School")
                End If
                Dim Phone As String = RequestParams("Phone")
                Dim Mobile As String = RequestParams("Mobile")
                Dim Address As String = RequestParams("Address")
                If Address.Split(" ").Length <> 2 Then
                    Throw New Exception("Invalid Address")
                End If

                Dim SalesGroup As String = RequestParams("SalesGroup")
                Dim SalesManager As String = RequestParams("SalesManager")
                Dim Sales As String = RequestParams("Sales")
                Dim Comment As String = RequestParams("Comment")
                Dim Picture As System.Drawing.Image = CSOL.Convert.Base64StringToImage(RequestParams("Picture"))

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                StuID = DataBaseAccess.StuInfo.SaveItem(StuID, No, Name, EngName, Identity, Type, Sex, Birth, School, Phone, Mobile, Address, SalesGroup, SalesManager, Sales, Comment, HandlerID, WorkStationID)
                If StuID <= 0 Then
                    ResponseParams.Add("Status", "Error")
                Else
                    Picture.Save(String.Format("{0}\Stu-{1}.png", PicturePath, StuID), System.Drawing.Imaging.ImageFormat.Png)

                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("StuID", StuID)
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Public Sub RemoveStu()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                If DataBaseAccess.StuInfo.RemoveStu(StuID, HandlerID, WorkStationID) Then
                    ResponseParams.Add("Status", "OK")
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Public Sub GetNextStuNo()
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuNo As String = DataBaseAccess.StuInfo.GetNextStuNo()
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuNo", StuNo)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Public Sub GetStu()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            'Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            'If SessionID Is Nothing Then
            '    StatusCode = System.Net.HttpStatusCode.BadRequest
            '    ResponseParams.Add("Status", StatusCode.ToString())
            'Else
            Dim Rows As Integer = RequestParams("Rows")
            Dim Page As Integer = RequestParams("Page")
            Dim FilterType As String = RequestParams("FilterType")
            Dim Filter As String = ""
            Select Case FilterType
                Case "預設"
                    Dim StuNo As String = RequestParams("StuNo").Replace("*", "%")
                    Dim Name As String = RequestParams("Name").Replace("*", "%")
                    Dim Phone As String = RequestParams("Phone").Replace("*", "%")
                    Dim Birth As String = RequestParams("Birth")
                    Dim RecDate As String = RequestParams("RecDate")
                    If StuNo <> "" Then
                        Filter &= String.Format("No LIKE '{0}%'", StuNo)
                    End If

                    If Name <> "" Then
                        If Filter <> "" Then Filter &= " AND "
                        Filter &= String.Format("Name LIKE '{0}%'", Name)
                    End If

                    If Phone <> "" Then
                        If Filter <> "" Then Filter &= " AND "
                        Filter &= String.Format("ID IN (SELECT DISTINCT StuID FROM csol.dbo.AddressBook WHERE Phone LIKE '{0}%' OR Mobile LIKE '{0}%')", Phone)
                    End If

                    Select Case Birth
                        Case "本日壽星"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "MONTH(Birth) = MONTH(GETDATE()) AND DAY(Birth) = DAY(GETDATE())"
                        Case "本月壽星"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "MONTH(Birth) = MONTH(GETDATE())"
                    End Select

                    Select Case RecDate
                        Case "本日新增"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE()) AND DAY(CreateDate) = DAY(GETDATE())"
                        Case "本日修改"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE()) AND DAY(ModifyDate) = DAY(GETDATE())"
                        Case "本月新增"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE())"
                        Case "本月修改"
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE())"
                    End Select
            End Select

            Dim StuCount As Integer = DataBaseAccess.StuInfo.GetStuCount(Filter)
            Dim Stu As DataTable = DataBaseAccess.StuInfo.GetStu(Filter, Page, Rows)

            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("StuCount", StuCount)
            ResponseParams.Add("Stu", CSOL.Convert.DataTableToXmlString(Stu))
            'End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Public Sub GetStuMulClass()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SubClassID As String = RequestParams("SubClassID")
           
            Dim dtStuMulClass As DataTable = DataBaseAccess.StuInfo.GetStuPayRec(SubClassID)


            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("StuMulClassInfo", CSOL.Convert.DataTableToXmlString(dtStuMulClass))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using








            'Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            'Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            'Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            'Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            'Dim ResponseBody As String = ""

            'Dim SubClassID As String = RequestParams("SubClassID")

            'Dim dtStuMulClass As DataTable = DataBaseAccess.StuInfo.GetStuPayRec(SubClassID)


            'ResponseParams.Add("Status", "OK")
            'ResponseParams.Add("StuMulClassInfo", CSOL.Convert.DataTableToXmlString(dtStuMulClass))

            'ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            'Me.m_Context.Response.StatusCode = StatusCode
            'Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
            '    sw.Write(ResponseBody)
            'End Using









            ' Dim StuCount As Integer = DataBaseAccess.StuInfo.GetStuCount(Filter)

            'Dim Rows As Integer = RequestParams("Rows")
            'Dim Page As Integer = RequestParams("Page")
            'Dim FilterType As String = RequestParams("FilterType")
            'Dim Filter As String = ""
            'Select Case FilterType
            '    Case "預設"
            '        Dim StuNo As String = RequestParams("StuNo").Replace("*", "%")
            '        Dim Name As String = RequestParams("Name").Replace("*", "%")
            '        Dim Phone As String = RequestParams("Phone").Replace("*", "%")
            '        Dim Birth As String = RequestParams("Birth")
            '        Dim RecDate As String = RequestParams("RecDate")
            '        If StuNo <> "" Then
            '            Filter &= String.Format("No LIKE '{0}%'", StuNo)
            '        End If

            '        If Name <> "" Then
            '            If Filter <> "" Then Filter &= " AND "
            '            Filter &= String.Format("Name LIKE '{0}%'", Name)
            '        End If

            '        If Phone <> "" Then
            '            If Filter <> "" Then Filter &= " AND "
            '            Filter &= String.Format("ID IN (SELECT DISTINCT StuID FROM csol.dbo.AddressBook WHERE Phone LIKE '{0}%' OR Mobile LIKE '{0}%')", Phone)
            '        End If

            '        Select Case Birth
            '            Case "本日壽星"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "MONTH(Birth) = MONTH(GETDATE()) AND DAY(Birth) = DAY(GETDATE())"
            '            Case "本月壽星"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "MONTH(Birth) = MONTH(GETDATE())"
            '        End Select

            '        Select Case RecDate
            '            Case "本日新增"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE()) AND DAY(CreateDate) = DAY(GETDATE())"
            '            Case "本日修改"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE()) AND DAY(ModifyDate) = DAY(GETDATE())"
            '            Case "本月新增"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE())"
            '            Case "本月修改"
            '                If Filter <> "" Then Filter &= " AND "
            '                Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE())"
            '        End Select
            'End Select

            'Dim StuCount As Integer = DataBaseAccess.StuInfo.GetStuCount(Filter)
            'Dim Stu As DataTable = DataBaseAccess.StuInfo.GetStu(Filter, Page, Rows)

        End Sub

        Private Sub GetStuAssign()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)


            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim StuId As String = RequestParams("StuId")

            Dim dtStuAssign As DataTable = DataBaseAccess.StuInfo.GetStuAssignByStuId(StuId)


            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("StuAssign", CSOL.Convert.DataTableToXmlString(dtStuAssign))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetStuPaper()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)


            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim StuId As String = RequestParams("StuId")

            Dim dtStuPaper As DataTable = DataBaseAccess.StuInfo.GetStuPaperByStuId(StuId)


            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("StuPaper", CSOL.Convert.DataTableToXmlString(dtStuPaper))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub


        'Private Sub SearchStu()
        '    Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
        '    Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)


        '    Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
        '    Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
        '    Dim ResponseBody As String = ""

        '    Dim StuId As String = RequestParams("StuId")

        '    Dim dtStuPaper As DataTable = DataBaseAccess.StuInfo.GetStuPaperByStuId(StuId)


        '    ResponseParams.Add("Status", "OK")
        '    ResponseParams.Add("StuPaper", CSOL.Convert.DataTableToXmlString(dtStuPaper))

        '    ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

        '    Me.m_Context.Response.StatusCode = StatusCode
        '    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
        '        sw.Write(ResponseBody)
        '    End Using
        'End Sub


        Public Sub GetStuCompressed()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim Rows As Integer = RequestParams("Rows")
                Dim Page As Integer = RequestParams("Page")
                Dim FilterType As String = RequestParams("FilterType")
                Dim Filter As String = ""
                Select Case FilterType
                    Case "預設"
                        Dim StuNo As String = RequestParams("StuNo").Replace("*", "%")
                        Dim Name As String = RequestParams("Name").Replace("*", "%")
                        Dim Phone As String = RequestParams("Phone").Replace("*", "%")
                        Dim Birth As String = RequestParams("Birth")
                        Dim RecDate As String = RequestParams("RecDate")
                        If StuNo <> "" Then
                            Filter &= String.Format("No LIKE '{0}%'", StuNo)
                        End If

                        If Name <> "" Then
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= String.Format("Name LIKE '{0}%'", Name)
                        End If

                        If Phone <> "" Then
                            If Filter <> "" Then Filter &= " AND "
                            Filter &= String.Format("ID IN (SELECT DISTINCT StuID FROM csol.dbo.AddressBook WHERE Phone LIKE '{0}%' OR Mobile LIKE '{0}%')", Phone)
                        End If

                        Select Case Birth
                            Case "本日壽星"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "MONTH(Birth) = MONTH(GETDATE()) AND DAY(Birth) = DAY(GETDATE())"
                            Case "本月壽星"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "MONTH(Birth) = MONTH(GETDATE())"
                        End Select

                        Select Case RecDate
                            Case "本日新增"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE()) AND DAY(CreateDate) = DAY(GETDATE())"
                            Case "本日修改"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE()) AND DAY(ModifyDate) = DAY(GETDATE())"
                            Case "本月新增"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "YEAR(CreateDate) = YEAR(GETDATE()) AND MONTH(CreateDate) = MONTH(GETDATE())"
                            Case "本月修改"
                                If Filter <> "" Then Filter &= " AND "
                                Filter &= "YEAR(ModifyDate) = YEAR(GETDATE()) AND MONTH(ModifyDate) = MONTH(GETDATE())"
                        End Select
                End Select

                Dim StuCount As Integer = DataBaseAccess.StuInfo.GetStuCount(Filter)
                Dim Stu As DataTable = DataBaseAccess.StuInfo.GetStu(Filter, Page, Rows)

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuCount", StuCount)
                ResponseParams.Add("Stu", CSOL.Convert.DataTableToXmlString(Stu))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("Stu", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub

        Private Sub GetStuInfo()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim StuInfo As DataTable = DataBaseAccess.StuInfo.GetStuInfo(StuID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuInfo", CSOL.Convert.DataTableToXmlString(StuInfo))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetStuPicture()
            Dim PicturePath As String = String.Format("{0}\Images\StuPictures", My.Application.Info.DirectoryPath)

            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim pic As String = ""
                Dim path As String = String.Format("{0}\Stu-{1}.png", PicturePath, StuID)
                If System.IO.File.Exists(path) Then
                    pic = CSOL.Convert.ImageToBase64String(System.Drawing.Image.FromFile(path), System.Drawing.Imaging.ImageFormat.Png)
                End If

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Picture", pic)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetAddressBook()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim AddressBook As DataTable = DataBaseAccess.StuInfo.GetAddressBook(StuID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("AddressBook", CSOL.Convert.DataTableToXmlString(AddressBook))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub AddAddressBook()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim Title As String = RequestParams("Title")
                Dim Name As String = RequestParams("Name")
                Dim Job As String = RequestParams("Job")
                Dim Phone As String = RequestParams("Phone")
                Dim Mobile As String = RequestParams("Mobile")
                Dim Address As String = RequestParams("Address")
                Dim Comment As String = RequestParams("Comment")

                If DataBaseAccess.StuInfo.AddAddressBook(StuID, Title, Name, Job, Phone, Mobile, Address, Comment) > 0 Then
                    ResponseParams.Add("Status", "OK")
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub RemoveAddressBook()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim AddressBookID As Integer = RequestParams("AddressBookID")

                If DataBaseAccess.StuInfo.RemoveAddressBook(AddressBookID) Then
                    ResponseParams.Add("Status", "OK")
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        
    End Class
End Namespace