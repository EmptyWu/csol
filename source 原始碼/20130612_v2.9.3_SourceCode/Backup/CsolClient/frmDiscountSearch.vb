﻿Public Class frmDiscountSearch
    Private dtSubClassList As New DataTable
    Private dtClassList As New DataTable
    Private dtPaySta As New DataTable
    Private intClass As Integer = 0
    Private intSubClass As Integer = 0
    Private lstClassShown As New ArrayList
    Private lstClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmDiscountSearch
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmDiscountSearch
        InitClassList()
        ShowClassList()
        InitColSelections()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvRec.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvRec.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvRec.Rows.Count

            Dim oRow As DataGridViewRow = dgvRec.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvRec.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvRec.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvRec.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvRec.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvRec.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvRec.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvRec.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmDiscountSearch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmDiscountSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub RefreshData()
        Try

            dtPaySta = objCsol.ListDiscountStaByClass(lstClass)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub RefreshTable()
        Dim strCompute2 As String = ""

        If dtPaySta.Rows.Count = 0 Then
            dgvRec.DataSource = Nothing
            Exit Sub
        End If

        If cboxSubClass.SelectedIndex = 0 And cboxClass.SelectedIndex > -1 Then
            strCompute2 = c_ClassIDColumnName & _
                "=" & lstClassShown(cboxClass.SelectedIndex).ToString
            dtPaySta.DefaultView.RowFilter = strCompute2
        Else
            If cboxSubClass.SelectedIndex > 0 Then
                strCompute2 = c_SubClassIDColumnName & _
                    "=" & lstSubClassShown(cboxSubClass.SelectedIndex).ToString
                dtPaySta.DefaultView.RowFilter = strCompute2
            Else
                dtPaySta.DefaultView.RowFilter = ""
            End If
        End If

        dgvRec.DataSource = dtPaySta

        LoadColumnText()
        dgvRec.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "$#,##0"
    End Sub

    Private Sub InitClassList()
        Try
            dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
            dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        cboxClass.Items.Clear()
        cboxSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        cboxClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName).trim)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvRec.Columns
            col.visible = False
        Next
        If dgvRec.Rows.Count > 0 Then

            dgvRec.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvRec.Columns.Item(c_StuIDColumnName).Visible = True
            dgvRec.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvRec.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvRec.Columns.Item(c_StuNameColumnName).Visible = True
            dgvRec.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvRec.Columns.Item(c_SchoolColumnName).DisplayIndex = 2
            dgvRec.Columns.Item(c_SchoolColumnName).Visible = True
            dgvRec.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgvRec.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
            dgvRec.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvRec.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvRec.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgvRec.Columns.Item(c_Tel1ColumnName).Visible = True
            dgvRec.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgvRec.Columns.Item(c_Tel2ColumnName).DisplayIndex = 5
            dgvRec.Columns.Item(c_Tel2ColumnName).Visible = True
            dgvRec.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            dgvRec.Columns.Item(c_DiscountColumnName).DisplayIndex = 6
            dgvRec.Columns.Item(c_DiscountColumnName).Visible = True
            dgvRec.Columns.Item(c_DiscountColumnName).HeaderText = My.Resources.discount
            dgvRec.Columns.Item(c_RemarksColumnName).DisplayIndex = 7
            dgvRec.Columns.Item(c_RemarksColumnName).Visible = True
            dgvRec.Columns.Item(c_RemarksColumnName).HeaderText = My.Resources.discRemarks
        End If
        Dim i As Integer = 8
        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow(index) = 1 Then
                dgvRec.Columns.Item(lstColName(index)).DisplayIndex = i
                dgvRec.Columns.Item(lstColName(index)).Visible = True
                dgvRec.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                i = i + 1
            End If
        Next
        If dgvRec.Columns.Item(c_SchoolColumnName).Visible = True Then
            Dim id As Integer
            Select Case intColSchType
                Case 0
                    For index As Integer = 0 To dgvRec.Rows.Count - 1
                        id = dgvRec.Rows(index).Cells(c_PrimarySchColumnName).Value
                        dgvRec.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 1
                    For index As Integer = 0 To dgvRec.Rows.Count - 1
                        id = dgvRec.Rows(index).Cells(c_JuniorSchColumnName).Value
                        dgvRec.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 2
                    For index As Integer = 0 To dgvRec.Rows.Count - 1
                        id = dgvRec.Rows(index).Cells(c_HighSchColumnName).Value
                        dgvRec.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 3
                    For index As Integer = 0 To dgvRec.Rows.Count - 1
                        id = dgvRec.Rows(index).Cells(c_UniversityColumnName).Value
                        dgvRec.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
            End Select
        End If
    End Sub

    Private Sub InitColSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvRec.Columns.Count > 7 Then
            Dim lst As New ArrayList
            For index As Integer = 8 To dgvRec.Columns.Count - 1
                If dgvRec.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub InitClassSelection()
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmDiscountSearch_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        Dim intC As Integer
        Dim intSc As Integer
        If cboxClass.SelectedIndex = -1 Then
            dgvRec.DataSource = Nothing
            Exit Sub
        End If
        intC = lstClassShown(cboxClass.SelectedIndex)
        lstClass = New ArrayList
        lstClass.Add(intC)
        lstSubClassShown = New ArrayList
        cboxSubClass.Items.Clear()
        lstSubClassShown.Add(-1)
        cboxSubClass.Items.Add(My.Resources.all)

        For index As Integer = 0 To dtSubClassList.Rows.Count - 1
            If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                lstSubClassShown.Add(intSc)
                cboxSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName).trim)
            End If
        Next

        cboxSubClass.SelectedIndex = -1

    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        RefreshData()
        RefreshTable()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgvRec)
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgvRec.SelectedRows.Count > 0 Then
            Dim id As String = dgvRec.SelectedRows(0).Cells(c_StuIDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub
End Class