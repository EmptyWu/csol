﻿Public Class frmStuAbsentToday
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Dim intType As Integer = 0
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private dt As New DataTable
    Private dt2 As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuAbsentToday
    Private sUserName As String = frmMain.GetUsrName
    Private dt3 As New DataTable

    Public Sub New()
        InitializeComponent()

        RefreshData()
        InitSelections()
        Me.Text = My.Resources.frmStuAbsentToday
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(0) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuAbsentToday_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuAbsentToday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Friend Sub RefreshData()
        dtContent = frmMain.GetContents
        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)


        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
               
            Next
        Else

            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))

                End If
            Next
        End If
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
            cboxSubClass.SelectedIndex = -1
            cboxSubClass.Text = ""
            cboxContent.SelectedIndex = -1
            cboxContent.Text = ""
        End If

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.stuName
            dgv.Columns.Item(c_BirthdayColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_BirthdayColumnName).Visible = True
            dgv.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
            dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_SchoolColumnName).Visible = True
            dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_Tel1ColumnName).Visible = True
            dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
        End If
        Dim i As Integer = 5
        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow(index) = 1 Then
                dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                dgv.Columns.Item(lstColName(index)).Visible = True
                dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                i = i + 1
            End If
        Next
        If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
            Dim id As Integer
            Select Case intColSchType
                Case 0
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 1
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 2
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 3
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
            End Select
        End If

        If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
            For index As Integer = 0 To dgv.Rows.Count - 1
                If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                    If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                        dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                    End If
                End If
            Next
        End If
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 4 Then
            Dim lst As New ArrayList
            For index As Integer = 5 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub frmStuAbsentToday_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1
            cboxSubClass.Text = ""
            If chkboxShowAll.Checked = False Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_ClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                cboxContent.SelectedIndex = -1
            End If
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(11)
    End Sub

    Private Sub DeleteStu(ByVal id As String)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(index).Item(c_IDColumnName) = id Then
                dtStu.Rows.RemoveAt(index)
            End If
        Next
        RefreshTable()
    End Sub

    'Private Sub RefreshTable()
    '    dt2 = New DataTable
    '    dt2 = dtStu.Clone 'stu of this subclass
    '    Dim strCompute1 As String = "COUNT(" + c_StuIDColumnName + ")"
    '    Dim strCompute2 As String = c_StuIDColumnName + "='"
    '    Dim id As String = ""

    '    For Each row In dt.Rows
    '        id = row.item(c_IDColumnName)
    '        If dtStu.Compute(strCompute1, strCompute2 & id & "'") = 0 Then
    '            dt2.ImportRow(row)
    '        End If
    '    Next
    '    For i As Integer = 0 To dtStu.Rows.Count - 1
    '        If dtStu.Rows(i).Item("MakeUpClassID").ToString.Trim = "-1" Then

    '        End If
    '    Next
    '    dtStu.DefaultView.RowFilter = "MakeUpClassID=-1"

    '    'dgv.DataSource = dtStu.DefaultView
    '    dgv.DataSource = dt2
    '    dt3 = dtStu.DefaultView.ToTable(True, c_StuIDColumnName)
    '    Dim countNum As Integer = 0
    '    For i As Integer = 0 To dtStu.Rows.Count - 1
    '        If dtStu.Rows(i).Item("MakeUpClassID").ToString.Trim = "0" Then
    '            countNum += 1
    '        End If
    '    Next
    '    tboxSta.Text = dt.Rows.Count.ToString & "/" & countNum & "/" & dt.Rows.Count - countNum

    '    LoadColumnText()
    'End Sub
    Private Sub RefreshTable()
        dt2 = New DataTable
        dt2 = dt.Clone 'stu of this subclass
        Dim strCompute1 As String = "COUNT(" + c_StuIDColumnName + ")"
        Dim strCompute2 As String = c_StuIDColumnName + "='"
        Dim id As String = ""
        Dim dtStuNoMakeUp As New DataTable
        dtStuNoMakeUp = dtStu.Clone
        For i As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(i).Item("MakeUpClassID").ToString = "0" Then
                dtStuNoMakeUp.Rows.Add(dtStu.Rows(i).ItemArray)
            End If
        Next
     

        For Each row In dt.Rows
            id = row.item(c_IDColumnName)
            If dtStuNoMakeUp.Compute(strCompute1, strCompute2 & id & "'") = 0 Then
                dt2.ImportRow(row)
            End If
        Next

        dgv.DataSource = dt2
        dt3 = dtStu.DefaultView.ToTable(True, c_StuIDColumnName)
        Dim countNum As Integer = 0
        For i As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(i).Item("MakeUpClassID").ToString.Trim = "0" Then
                countNum += 1
            End If
        Next
        tboxSta.Text = dt.Rows.Count.ToString & "/" & countNum & "/" & dt.Rows.Count - countNum

        LoadColumnText()
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            If Not chkboxShowAll.Checked Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                cboxContent.SelectedIndex = -1
            Else
                Dim sc As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                dtStu = objCsol.GetSubClassPunchRecNoCont(sc, GetDateStart(Now), _
                                                   GetDateEnd(Now))
                dt = objCsol.ListStuBySubClass(sc)
                RefreshTable()
            End If
        End If
    End Sub

    Private Sub chkboxShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowAll.CheckedChanged
        If Not chkboxShowAll.Checked Then
            If cboxSubClass.SelectedIndex > -1 Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                cboxContent.SelectedIndex = -1
            End If
        Else
            If cboxClass.SelectedIndex > 0 Then
                Dim i As Integer = lstClass(cboxClass.SelectedIndex)
                cboxContent.Items.Clear()
                lstContent.Clear()
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_ClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                cboxContent.SelectedIndex = -1
            End If
        End If
    End Sub

    Private Sub cboxContent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxContent.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            Dim sc As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            If chkboxShowAll.Checked = False Then
                Dim c As Integer = 0
                If cboxContent.SelectedIndex > -1 Then
                    c = lstContent(cboxContent.SelectedIndex)
                End If

                dtStu = objCsol.GetSubClassPunchRec(sc, c, GetDateStart(Now), _
                                                    GetDateEnd(Now))
            Else
                dtStu = objCsol.GetSubClassPunchRecNoCont(sc, GetDateStart(Now), _
                                                    GetDateEnd(Now))
            End If
            dt = objCsol.ListStuBySubClass(sc)
            RefreshTable()
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(10) Then
            ExportDgvToExcel(dgv)
        Else
            frmMain.ShowNoAuthMsg()
        End If

    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshData()
    End Sub
End Class