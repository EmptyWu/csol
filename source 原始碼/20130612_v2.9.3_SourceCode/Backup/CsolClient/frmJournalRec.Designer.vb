﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmJournalRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmJournalRec))
        Me.dgvRec = New System.Windows.Forms.DataGridView
        Me.cboxGrp = New System.Windows.Forms.ComboBox
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.radbutNoDate = New System.Windows.Forms.RadioButton
        Me.radbutCustDate = New System.Windows.Forms.RadioButton
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuAdd = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuModify = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.butFilter = New System.Windows.Forms.Button
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvRec
        '
        Me.dgvRec.AllowUserToAddRows = False
        Me.dgvRec.AllowUserToDeleteRows = False
        Me.dgvRec.AllowUserToResizeRows = False
        Me.dgvRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvRec, "dgvRec")
        Me.dgvRec.MultiSelect = False
        Me.dgvRec.Name = "dgvRec"
        Me.dgvRec.RowHeadersVisible = False
        Me.dgvRec.RowTemplate.Height = 15
        Me.dgvRec.RowTemplate.ReadOnly = True
        Me.dgvRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvRec.ShowCellToolTips = False
        Me.dgvRec.ShowEditingIcon = False
        '
        'cboxGrp
        '
        Me.cboxGrp.FormattingEnabled = True
        resources.ApplyResources(Me.cboxGrp, "cboxGrp")
        Me.cboxGrp.Name = "cboxGrp"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'radbutNoDate
        '
        resources.ApplyResources(Me.radbutNoDate, "radbutNoDate")
        Me.radbutNoDate.Name = "radbutNoDate"
        Me.radbutNoDate.UseVisualStyleBackColor = True
        '
        'radbutCustDate
        '
        resources.ApplyResources(Me.radbutCustDate, "radbutCustDate")
        Me.radbutCustDate.Checked = True
        Me.radbutCustDate.Name = "radbutCustDate"
        Me.radbutCustDate.TabStop = True
        Me.radbutCustDate.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAdd, Me.mnuModify, Me.mnuDelete, Me.mnuExport, Me.mnuPrint, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuAdd
        '
        Me.mnuAdd.Name = "mnuAdd"
        resources.ApplyResources(Me.mnuAdd, "mnuAdd")
        '
        'mnuModify
        '
        Me.mnuModify.Name = "mnuModify"
        resources.ApplyResources(Me.mnuModify, "mnuModify")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'butFilter
        '
        resources.ApplyResources(Me.butFilter, "butFilter")
        Me.butFilter.Name = "butFilter"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'PrintDocument1
        '
        '
        'frmJournalRec
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.dgvRec)
        Me.Controls.Add(Me.cboxGrp)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.radbutNoDate)
        Me.Controls.Add(Me.radbutCustDate)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.butFilter)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Name = "frmJournalRec"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvRec As System.Windows.Forms.DataGridView
    Friend WithEvents cboxGrp As System.Windows.Forms.ComboBox
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents radbutNoDate As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCustDate As System.Windows.Forms.RadioButton
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuAdd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuModify As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
