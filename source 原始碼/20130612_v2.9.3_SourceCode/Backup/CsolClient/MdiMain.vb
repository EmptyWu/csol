﻿Imports Csol
Imports System.Text
Imports System.IO
Imports System.ComponentModel

Public Class mdiMain
    Private subfrmPaySta As frmPayStaToday
    Private subfrmPayStaClass As frmPayStaClass
    Private subfrmPayStaSubClass As frmPayStaSubClass
    Private subfrmPayStaDate As frmPayStaDate
    Private subfrmSalesSta As frmSalesSta
    Private subfrmHandlerPaySta As frmHandlerPaySta
    Private subfrmStuOweSta As frmStuOweSta
    Private subfrmStuMBSta As frmStuMBSta
    Private subfrmStuMKSta As frmStuMKSta
    Private subfrmJournalRec As frmJournalRec
    Private subfrmJournalSta As frmJournalSta
    Private subfrmPayRecSearch As frmPayRecSearch
    Private subfrmClassPayStaRec As frmClassPayStaRec
    Private subfrmDiscountSearch As frmDiscountSearch
    Private subfrmChequeValidSearch As frmChequeValidSearch
    Private subfrmStuRecSearch As frmStuRecSearch
    Private subfrm2SearchClass As frm2SearchClass

    Private subfrmAddClassRoom As frmAddClassRoom
    Private subfrmAddStu As frmAddStu
    Private subfrmBookIssueRec As frmBookIssueRec
    Private subfrmCardReissueRec As frmCardReissueRec
    Private subfrmClassContRate As frmClassContRate
    Private subfrmClassPunch As frmClassPunch
    Private subfrmClassToday As frmClassToday
    Private subfrmClassTransfer As frmClassTransfer
    Private subfrmFamilySearch As frmFamilySearch
    Private subfrmModifyClassRoom As frmModifyClassRoom
    Private subfrmNameRepeatStu As frmNameRepeatStu
    Private subfrmPotentialStu As frmPotentialStu
    Private subfrmPunchBoard As frmPunchBoard
    Private subfrmPunchMakeup As frmPunchMakeup
    Private subfrmPunchRec As frmPunchRec
    Private subfrmReceiptModRec As frmReceiptModRec
    Private subfrmDiscModRec As frmDiscModRec
    Private subfrmSeatCount As frmSeatCount
    Private subfrmSeatPlan As frmSeatPlan
    Private subfrmSetAuthorityPwd As frmSetAuthorityPwd
    Private subfrmSetBook As frmSetBook
    Private subfrmSetClass As frmSetClass
    Private subfrmSetClassRoom As frmSetClassRoom
    Private subfrmSetSchool As frmSetSchool
    Private subfrmShowupRate As frmShowupRate
    Private subfrmStuAbsentToday As frmStuAbsentToday
    Private subfrmStuBookIssue As frmStuBookIssue
    Private subfrmStuHereToday As frmStuHereToday
    Private subfrmStuInfo As frmStuInfo
    Private subfrmStuNoBook As frmStuNoBook
    Private subfrmStuNoteRec As frmStuNoteRec
    Private subfrmStuSearchClass As frmStuSearchClass
    Private subfrmStuSearchMulClass As frmStuSearchMulClass
    Private subfrmStuSearchSchool As frmStuSearchSchool
    Private subfrmUsrInfo As frmUsrInfo
    Private subfrmJournalGrp As frm2JournalGrp
    Private subfrmAddJournalRec As frm2AddJournalRec
    Private subfrmTeleInterviewSearch As frmTeleInterviewSearch
    Private subfrmSetAssignment As frmSetAssignment
    Private subfrmSetClassPaper As frmSetClassPaper
    Private subfrmPostGrade As frmPostGrade
    Private subfrmAssignDone As frmAssignDone
    Private subfrmAssignDoneMul As frmAssignDoneMul
    Private subfrmStuDoneAssign As frmStuDoneAssign
    Private subfrmStuNoAssign As frmStuNoAssign
    Private subfrmStuGradeMul As frmStuGradeMul
    Private subfrmStuGrade As frmStuGrade

    Private strUsrId As String = ""
    Private strUsrName As String = ""
    Private strCurrentStuId As String = ""
    Private strSeatNum As String = ""

    Private dtJournalGrp As New DataTable
    Private dtDiscountDict As New DataTable
    Private dtRemarksDict As New DataTable
    Private dtDiscountDictType As New DataTable
    Private blOk As Boolean = False
    Private strCurrentString As String = ""
    Private strCurrentStringArr As ArrayList

    Private intCurrentValue As Integer = 0
    Private intCurrentValueArr As ArrayList

    Private dtBook As New DataTable
    Private dtBookNoUsrID As New DataTable

    Private subfrm2SearchStu2 As New frm2SearchStuSmall
    Private dtClassContent As New DataTable
    Private dtClassSession As New DataTable
    Private lstCurrent As New ArrayList
    Private lstCurrent2 As New ArrayList
    Private dtSchool As New DataTable
    Private dtReceiptPrint As New DataTable
    Private dtUsrAuth As New DataTable
    Private dtClassAuth As New DataTable
    Private dtSubClassAuth As New DataTable
    Private dtAllClassAuth As New DataTable
    Private dtSchGrp As New DataTable
    Private dtSibType As New DataTable
    Private dtSchType As New DataTable
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private dtSales As New DataTable
    Private dtSubjects As New DataTable
    Private dtClassRoom As New DataTable
    Private dtTeleInterviewType As New DataTable
    Private dtUserNameList As New DataTable
    Private dsClassInfo As New DataSet

    Private dtClassPunchBook As New DataTable
    Private dtClassPunchClass As New DataTable
    Private dtClassPunchSubClass As New DataTable
    Private dtClassPunchContent As New DataTable
    Private dtClassPunchSession As New DataTable

    Private lstStuSubClass As New ArrayList

    Private blBooks As Boolean = False
    Private blBooksWithUsrID As Boolean = False
    Private blClass As Boolean = False
    Private blContent As Boolean = False
    Private blDisc As Boolean = False
    Private blJournal As Boolean = False
    Private blReceipt As Boolean = False
    Private blSchGrp As Boolean = False
    Private blSch As Boolean = False
    Private blSchType As Boolean = False
    Private blSession As Boolean = False
    Private blSibType As Boolean = False
    Private blUsrAuth As Boolean = False
    Private blTeleInterviewType As Boolean = False
    Private blUserNameList As Boolean = False
    Private blClassInfo As Boolean = False
    Private blClassPunch As Boolean = False


    Private Sub mnuPayStaToday_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayStaToday.Click
        ShowSubFramPaySta(Now, Now)
    End Sub

    Private Sub mnuPayStaByMonth_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayStaByMonth.Click
        Dim dateFr As Date
        Dim dateTo As Date

        dateFr = New Date(Now.Year, Now.Month, 1)
        dateTo = New Date(Now.Year, Now.Month, dhDaysInMonth(Now))

        ShowSubFramPaySta(dateFr, dateTo)
    End Sub

    Friend Sub CloseTab(ByRef page As TabPage)
        tabctrlMain.TabPages.Remove(page)
    End Sub

    Private Sub ShowSubFramPaySta(ByVal dateFr As Date, ByVal dateTo As Date)
        If CheckAuth(21) Then
            If Not subfrmPaySta Is Nothing Then
                If subfrmPaySta.Created = True Then
                    SelectTab(subfrmPaySta.Text)
                    subfrmPaySta.SetDate(dateFr, dateTo)
                    subfrmPaySta.RefreshData()
                    subfrmPaySta.BringToFront()
                Else
                    subfrmPaySta = New frmPayStaToday(dateFr, dateTo)
                    AddMyTabPage(subfrmPaySta)
                End If
            Else
                subfrmPaySta = New frmPayStaToday(dateFr, dateTo)
                AddMyTabPage(subfrmPaySta)
            End If
        Else
            ShowNoAuthMsg()
        End If
    End Sub

    Private Sub AddMyTabPage(ByRef subfrm As Form)
        tabctrlMain.TabPages.Add(New TabPage())
        subfrm.Tag = tabctrlMain.TabPages(tabctrlMain.TabCount - 1)
        subfrm.MdiParent = Me
        subfrm.Tag.Text = subfrm.Text
        subfrm.Show()
    End Sub

    Private Sub FindSubFrom(ByRef subfrm As Form)
        SelectTab(subfrm.Text)
        subfrm.BringToFront()
    End Sub

    Private Sub mnuPayStaByYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayStaByYear.Click
        Dim dateFr As Date
        Dim dateTo As Date

        dateFr = New Date(Now.Year, 1, 1)
        dateTo = New Date(Now.Year, 12, 31)

        ShowSubFramPaySta(dateFr, dateTo)
    End Sub

    Private Sub tabctrlMain_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tabctrlMain.SelectedIndexChanged
        If tabctrlMain.TabCount > 0 Then
            Dim strText As String
            strText = tabctrlMain.SelectedTab.Text
            If strText.Length > 2 Then
                If strText.Substring(strText.Length - 3, 3) = My.Resources.frmStuInfo And Not subfrmStuInfo Is Nothing Then
                    subfrmStuInfo.BringToFront()
                    Exit Sub
                End If
            End If

            If strText = My.Resources.frmPaySta And Not subfrmPaySta Is Nothing Then
                subfrmPaySta.BringToFront()
            ElseIf strText = My.Resources.frmPayStaClass And Not subfrmPayStaClass Is Nothing Then
                subfrmPayStaClass.BringToFront()
            ElseIf strText = My.Resources.frmPayStaSubClass And Not subfrmPayStaSubClass Is Nothing Then
                subfrmPayStaSubClass.BringToFront()
            ElseIf strText = My.Resources.frmPayStaDate And Not subfrmPayStaDate Is Nothing Then
                subfrmPayStaDate.BringToFront()
            ElseIf strText = My.Resources.frmSalesSta And Not subfrmSalesSta Is Nothing Then
                subfrmSalesSta.BringToFront()
            ElseIf strText = My.Resources.frmHandlerPaySta And Not subfrmHandlerPaySta Is Nothing Then
                subfrmHandlerPaySta.BringToFront()
            ElseIf strText = My.Resources.frmStuOweSta And Not subfrmStuOweSta Is Nothing Then
                subfrmStuOweSta.BringToFront()
            ElseIf strText = My.Resources.frmStuMBSta And Not subfrmStuMBSta Is Nothing Then
                subfrmStuMBSta.BringToFront()
            ElseIf strText = My.Resources.frmStuMKSta And Not subfrmStuMKSta Is Nothing Then
                subfrmStuMKSta.BringToFront()
            ElseIf strText = My.Resources.frmJournalRec And Not subfrmJournalRec Is Nothing Then
                subfrmJournalRec.BringToFront()
            ElseIf strText = My.Resources.frmJournalSta And Not subfrmJournalSta Is Nothing Then
                subfrmJournalSta.BringToFront()
            ElseIf strText = My.Resources.frmPayRecSearch And Not subfrmPayRecSearch Is Nothing Then
                subfrmPayRecSearch.BringToFront()
            ElseIf strText = My.Resources.frmClassPayStaRec And Not subfrmClassPayStaRec Is Nothing Then
                subfrmClassPayStaRec.BringToFront()
            ElseIf strText = My.Resources.frmDiscountSearch And Not subfrmDiscountSearch Is Nothing Then
                subfrmDiscountSearch.BringToFront()
            ElseIf strText = My.Resources.frmChequeValidSearch And Not subfrmChequeValidSearch Is Nothing Then
                subfrmChequeValidSearch.BringToFront()
            ElseIf strText = My.Resources.frmAddClassRoom And Not subfrmAddClassRoom Is Nothing Then
                subfrmAddClassRoom.BringToFront()
            ElseIf strText = My.Resources.frmAddStu And Not subfrmAddStu Is Nothing Then
                subfrmAddStu.BringToFront()
            ElseIf strText = My.Resources.frmBookIssueRec And Not subfrmBookIssueRec Is Nothing Then
                subfrmBookIssueRec.BringToFront()
            ElseIf strText = My.Resources.frmCardReissueRec And Not subfrmCardReissueRec Is Nothing Then
                subfrmCardReissueRec.BringToFront()
            ElseIf strText = My.Resources.frmClassContRate And Not subfrmClassContRate Is Nothing Then
                subfrmClassContRate.BringToFront()
            ElseIf strText = My.Resources.frmClassPunch And Not subfrmClassPunch Is Nothing Then
                subfrmClassPunch.BringToFront()
            ElseIf strText = My.Resources.frmClassToday And Not subfrmClassToday Is Nothing Then
                subfrmClassToday.BringToFront()
            ElseIf strText = My.Resources.frmClassTransfer And Not subfrmClassTransfer Is Nothing Then
                subfrmClassTransfer.BringToFront()
            ElseIf strText = My.Resources.frmFamilySearch And Not subfrmFamilySearch Is Nothing Then
                subfrmFamilySearch.BringToFront()
            ElseIf strText = My.Resources.frmModifyClassRoom And Not subfrmModifyClassRoom Is Nothing Then
                subfrmModifyClassRoom.BringToFront()
            ElseIf strText = My.Resources.frmNameRepeatStu And Not subfrmNameRepeatStu Is Nothing Then
                subfrmNameRepeatStu.BringToFront()
            ElseIf strText = My.Resources.frmPotentialStu And Not subfrmPotentialStu Is Nothing Then
                subfrmPotentialStu.BringToFront()
            ElseIf strText = My.Resources.frmPunchBoard And Not subfrmPunchBoard Is Nothing Then
                subfrmPunchBoard.BringToFront()
            ElseIf strText = My.Resources.frmPunchMakeup And Not subfrmPunchMakeup Is Nothing Then
                subfrmPunchMakeup.BringToFront()
            ElseIf strText = My.Resources.frmPunchRec And Not subfrmPunchRec Is Nothing Then
                subfrmPunchRec.BringToFront()
            ElseIf strText = My.Resources.frmReceiptModRec And Not subfrmReceiptModRec Is Nothing Then
                subfrmReceiptModRec.BringToFront()
            ElseIf strText = My.Resources.frmDiscModRec And Not subfrmDiscModRec Is Nothing Then
                subfrmDiscModRec.BringToFront()
            ElseIf strText = My.Resources.frmSeatCount And Not subfrmSeatCount Is Nothing Then
                subfrmSeatCount.BringToFront()
            ElseIf strText = My.Resources.frmSeatPlan And Not subfrmSeatPlan Is Nothing Then
                subfrmSeatPlan.BringToFront()
            ElseIf strText = My.Resources.frmSetAuthorityPwd And Not subfrmSetAuthorityPwd Is Nothing Then
                subfrmSetAuthorityPwd.BringToFront()
            ElseIf strText = My.Resources.frmSetBook And Not subfrmSetBook Is Nothing Then
                subfrmSetBook.BringToFront()
            ElseIf strText = My.Resources.frmSetClass And Not subfrmSetClass Is Nothing Then
                subfrmSetClass.BringToFront()
            ElseIf strText = My.Resources.frmSetClassRoom And Not subfrmSetClassRoom Is Nothing Then
                subfrmSetClassRoom.BringToFront()
            ElseIf strText = My.Resources.frmSetSchool And Not subfrmSetSchool Is Nothing Then
                subfrmSetSchool.BringToFront()
            ElseIf strText = My.Resources.frmShowupRate And Not subfrmShowupRate Is Nothing Then
                subfrmShowupRate.BringToFront()
            ElseIf strText = My.Resources.frmStuAbsentToday And Not subfrmStuAbsentToday Is Nothing Then
                subfrmStuAbsentToday.BringToFront()
            ElseIf strText = My.Resources.frmStuBookIssue And Not subfrmStuBookIssue Is Nothing Then
                subfrmStuBookIssue.BringToFront()
            ElseIf strText = My.Resources.frmStuHereToday And Not subfrmStuHereToday Is Nothing Then
                subfrmStuHereToday.BringToFront()
            ElseIf strText = My.Resources.frmStuNoBook And Not subfrmStuNoBook Is Nothing Then
                subfrmStuNoBook.BringToFront()
            ElseIf strText = My.Resources.frmStuNoteRec And Not subfrmStuNoteRec Is Nothing Then
                subfrmStuNoteRec.BringToFront()
            ElseIf strText = My.Resources.frmStuSearchClass And Not subfrmStuSearchClass Is Nothing Then
                subfrmStuSearchClass.BringToFront()
            ElseIf strText = My.Resources.frmStuSearchMulClass And Not subfrmStuSearchMulClass Is Nothing Then
                subfrmStuSearchMulClass.BringToFront()
            ElseIf strText = My.Resources.frmStuSearchSchool And Not subfrmStuSearchSchool Is Nothing Then
                subfrmStuSearchSchool.BringToFront()
            ElseIf strText = My.Resources.frmUsrInfo And Not subfrmUsrInfo Is Nothing Then
                subfrmUsrInfo.BringToFront()
            ElseIf strText = My.Resources.frmTeleInterviewSearch And Not subfrmTeleInterviewSearch Is Nothing Then
                subfrmTeleInterviewSearch.BringToFront()
            ElseIf strText = My.Resources.frmStuRecSearch And Not subfrmStuRecSearch Is Nothing Then
                subfrmStuRecSearch.BringToFront()
            ElseIf strText = My.Resources.frmSetAssignment And Not subfrmSetAssignment Is Nothing Then
                subfrmSetAssignment.BringToFront()
            ElseIf strText = My.Resources.frmSetClassPaper And Not subfrmSetClassPaper Is Nothing Then
                subfrmSetClassPaper.BringToFront()
            ElseIf strText = My.Resources.frmPostGrade And Not subfrmPostGrade Is Nothing Then
                subfrmPostGrade.BringToFront()
            ElseIf strText = My.Resources.frmAssignDone And Not subfrmAssignDone Is Nothing Then
                subfrmAssignDone.BringToFront()
            ElseIf strText = My.Resources.frmAssignDoneMul And Not subfrmAssignDoneMul Is Nothing Then
                subfrmAssignDoneMul.BringToFront()
            ElseIf strText = My.Resources.frmStuDoneAssign And Not subfrmStuDoneAssign Is Nothing Then
                subfrmStuDoneAssign.BringToFront()
            ElseIf strText = My.Resources.frmStuNoAssign And Not subfrmStuNoAssign Is Nothing Then
                subfrmStuNoAssign.BringToFront()
            ElseIf strText = My.Resources.frmStuGradeMul And Not subfrmStuGradeMul Is Nothing Then
                subfrmStuGradeMul.BringToFront()
            ElseIf strText = My.Resources.frmStuGrade And Not subfrmStuGrade Is Nothing Then
                subfrmStuGrade.BringToFront()
            End If
        End If
    End Sub

    Public Sub SelectTab(ByVal text As String)
        Dim strText As String
        For index As Integer = 0 To tabctrlMain.TabCount - 1
            strText = tabctrlMain.TabPages(index).Text
            If strText = text Then
                tabctrlMain.SelectedIndex = index
            End If
            If strText.Length > 2 Then
                If strText.Substring(strText.Length - 3, 3) = text Then
                    tabctrlMain.SelectedIndex = index
                End If
            End If
        Next
    End Sub

    Public Sub ChangeTabText(ByVal text As String, ByVal newText As String)
        Dim strText As String
        For index As Integer = 0 To tabctrlMain.TabCount - 1
            strText = tabctrlMain.TabPages(index).Text

            If strText.Length > 2 And text.Length > 2 Then
                If strText.Substring(strText.Length - 3, 3) = text.Substring(text.Length - 3, 3) Then
                    tabctrlMain.TabPages(index).Text = newText
                End If
            End If
        Next
    End Sub

    Private Sub mnuPayStaByDate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayStaByDate.Click
        If CheckAuth(28) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmPayStaDate Is Nothing Then
            If subfrmPayStaDate.Created = True Then
                FindSubFrom(subfrmPayStaDate)
            Else
                subfrmPayStaDate = New frmPayStaDate()
                AddMyTabPage(subfrmPayStaDate)
            End If
        Else
            subfrmPayStaDate = New frmPayStaDate()
            AddMyTabPage(subfrmPayStaDate)
        End If
    End Sub

    Private Sub mnuPayStaByClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayStaByClass.Click
        If CheckAuth(27) Then
            If Not subfrmPayStaClass Is Nothing Then
                If subfrmPayStaClass.Created = True Then
                    FindSubFrom(subfrmPayStaClass)
                Else
                    subfrmPayStaClass = New frmPayStaClass()
                    AddMyTabPage(subfrmPayStaClass)
                End If
            Else
                subfrmPayStaClass = New frmPayStaClass()
                AddMyTabPage(subfrmPayStaClass)
            End If
        Else
            ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuPayStaBySubClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayStaBySubClass.Click
        If CheckAuth(27) Then
            If Not subfrmPayStaSubClass Is Nothing Then
                If subfrmPayStaSubClass.Created = True Then
                    FindSubFrom(subfrmPayStaSubClass)
                Else
                    subfrmPayStaSubClass = New frmPayStaSubClass()
                    AddMyTabPage(subfrmPayStaSubClass)
                End If
            Else
                subfrmPayStaSubClass = New frmPayStaSubClass()
                AddMyTabPage(subfrmPayStaSubClass)
            End If
        Else
            ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuIndvSales_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuIndvSales.Click
        If CheckAuth(52) And CheckAuth(51) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmSalesSta Is Nothing Then
            If subfrmSalesSta.Created = True Then
                FindSubFrom(subfrmSalesSta)
            Else
                subfrmSalesSta = New frmSalesSta()
                AddMyTabPage(subfrmSalesSta)
            End If
        Else
            subfrmSalesSta = New frmSalesSta()
            AddMyTabPage(subfrmSalesSta)
        End If
    End Sub

    Private Sub mnuIndvFeeReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuIndvFeeReceive.Click
        If CheckAuth(31) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmHandlerPaySta Is Nothing Then
            If subfrmHandlerPaySta.Created = True Then
                FindSubFrom(subfrmHandlerPaySta)
            Else
                subfrmHandlerPaySta = New frmHandlerPaySta()
                AddMyTabPage(subfrmHandlerPaySta)
            End If
        Else
            subfrmHandlerPaySta = New frmHandlerPaySta()
            AddMyTabPage(subfrmHandlerPaySta)
        End If
    End Sub

    Private Sub mnuFeeOweSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFeeOweSta.Click
        If CheckAuth(24) Then
            If Not subfrmStuOweSta Is Nothing Then
                If subfrmStuOweSta.Created = True Then
                    FindSubFrom(subfrmStuOweSta)
                Else
                    subfrmStuOweSta = New frmStuOweSta()
                    AddMyTabPage(subfrmStuOweSta)
                End If
            Else
                subfrmStuOweSta = New frmStuOweSta()
                AddMyTabPage(subfrmStuOweSta)
            End If
        Else
            ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuStuMBSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuMBSta.Click
        If Not subfrmStuMBSta Is Nothing Then
            If subfrmStuMBSta.Created = True Then
                FindSubFrom(subfrmStuMBSta)
            Else
                subfrmStuMBSta = New frmStuMBSta()
                AddMyTabPage(subfrmStuMBSta)
            End If
        Else
            subfrmStuMBSta = New frmStuMBSta()
            AddMyTabPage(subfrmStuMBSta)
        End If
    End Sub

    Private Sub mnuStuMKSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuMKSta.Click
        If Not subfrmStuMKSta Is Nothing Then
            If subfrmStuMKSta.Created = True Then
                FindSubFrom(subfrmStuMKSta)
            Else
                subfrmStuMKSta = New frmStuMKSta()
                AddMyTabPage(subfrmStuMKSta)
            End If
        Else
            subfrmStuMKSta = New frmStuMKSta()
            AddMyTabPage(subfrmStuMKSta)
        End If
    End Sub

    Private Sub mnuExpenseJournalRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExpenseJournalRec.Click
        If CheckAuth(38) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmJournalRec Is Nothing Then
            If subfrmJournalRec.Created = True Then
                FindSubFrom(subfrmJournalRec)
            Else
                subfrmJournalRec = New frmJournalRec()
                AddMyTabPage(subfrmJournalRec)
            End If
        Else
            subfrmJournalRec = New frmJournalRec()
            AddMyTabPage(subfrmJournalRec)
        End If
    End Sub

    Private Sub mnuExpenseJournalSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExpenseJournalSta.Click
        If CheckAuth(41) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmJournalSta Is Nothing Then
            If subfrmJournalSta.Created = True Then
                FindSubFrom(subfrmJournalSta)
            Else
                subfrmJournalSta = New frmJournalSta()
                AddMyTabPage(subfrmJournalSta)
            End If
        Else
            subfrmJournalSta = New frmJournalSta()
            AddMyTabPage(subfrmJournalSta)
        End If
    End Sub

    Private Sub mnuSearchPayRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchPayRec.Click
        If CheckAuth(20) Then
            If Not subfrmPayRecSearch Is Nothing Then
                If subfrmPayRecSearch.Created = True Then
                    FindSubFrom(subfrmPayRecSearch)
                Else
                    subfrmPayRecSearch = New frmPayRecSearch()
                    AddMyTabPage(subfrmPayRecSearch)
                End If
            Else
                subfrmPayRecSearch = New frmPayRecSearch()
                AddMyTabPage(subfrmPayRecSearch)
            End If
        Else
            ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuPayKeepRecByClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPayKeepRecByClass.Click
        If CheckAuth(29) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmClassPayStaRec Is Nothing Then
            If subfrmClassPayStaRec.Created = True Then
                FindSubFrom(subfrmClassPayStaRec)
            Else
                subfrmClassPayStaRec = New frmClassPayStaRec()
                AddMyTabPage(subfrmClassPayStaRec)
            End If
        Else
            subfrmClassPayStaRec = New frmClassPayStaRec()
            AddMyTabPage(subfrmClassPayStaRec)
        End If
    End Sub

    Private Sub mnuSearchFeeDiscount_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchFeeDiscount.Click
        If Not subfrmDiscountSearch Is Nothing Then
            If subfrmDiscountSearch.Created = True Then
                FindSubFrom(subfrmDiscountSearch)
            Else
                subfrmDiscountSearch = New frmDiscountSearch()
                AddMyTabPage(subfrmDiscountSearch)
            End If
        Else
            subfrmDiscountSearch = New frmDiscountSearch()
            AddMyTabPage(subfrmDiscountSearch)
        End If
    End Sub

    Private Sub mnuSearchChequeExpire_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchChequeExpire.Click
        If Not subfrmChequeValidSearch Is Nothing Then
            If subfrmChequeValidSearch.Created = True Then
                FindSubFrom(subfrmChequeValidSearch)
            Else
                subfrmChequeValidSearch = New frmChequeValidSearch()
                AddMyTabPage(subfrmChequeValidSearch)
            End If
        Else
            subfrmChequeValidSearch = New frmChequeValidSearch()
            AddMyTabPage(subfrmChequeValidSearch)
        End If
    End Sub

    Private Sub mnuSetClassroom_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetClassroom.Click
        If CheckAuth(69) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmSetClassRoom Is Nothing Then
            If subfrmSetClassRoom.Created = True Then
                FindSubFrom(subfrmSetClassRoom)
            Else
                subfrmSetClassRoom = New frmSetClassRoom()
                AddMyTabPage(subfrmSetClassRoom)
            End If
        Else
            subfrmSetClassRoom = New frmSetClassRoom()
            AddMyTabPage(subfrmSetClassRoom)
        End If
    End Sub

    Private Sub mnuSetClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetClass.Click
        If CheckAuth(73) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmSetClass Is Nothing Then
            If subfrmSetClass.Created = True Then
                FindSubFrom(subfrmSetClass)
            Else
                subfrmSetClass = New frmSetClass()
                AddMyTabPage(subfrmSetClass)
            End If
        Else
            subfrmSetClass = New frmSetClass()
            AddMyTabPage(subfrmSetClass)
        End If
    End Sub

    Private Sub mnuSetSchool_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetSchool.Click
        If CheckAuth(79) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmSetSchool Is Nothing Then
            If subfrmSetSchool.Created = True Then
                FindSubFrom(subfrmSetSchool)
            Else
                subfrmSetSchool = New frmSetSchool()
                AddMyTabPage(subfrmSetSchool)
            End If
        Else
            subfrmSetSchool = New frmSetSchool()
            AddMyTabPage(subfrmSetSchool)
        End If
    End Sub

    Private Sub mnuNewStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNewStudent.Click
        NewStudent()
    End Sub

    Private Sub NewStudent()
        If CheckAuth(59) = False And CheckAuth(67) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmAddStu Is Nothing Then
            If subfrmAddStu.Created = True Then
                FindSubFrom(subfrmAddStu)
            Else
                subfrmAddStu = New frmAddStu()
                AddMyTabPage(subfrmAddStu)
            End If
        Else
            subfrmAddStu = New frmAddStu()
            AddMyTabPage(subfrmAddStu)
        End If
    End Sub

    Private Sub mnuCheckSeatMap_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCheckSeatMap.Click
        ClassSeatMap()
    End Sub

    Private Sub ClassSeatMap()
        If Not subfrmSeatCount Is Nothing Then
            If subfrmSeatCount.Created = True Then
                FindSubFrom(subfrmSeatCount)
            Else
                subfrmSeatCount = New frmSeatCount()
                AddMyTabPage(subfrmSeatCount)
            End If
        Else
            subfrmSeatCount = New frmSeatCount()
            AddMyTabPage(subfrmSeatCount)
        End If
    End Sub

    Public Sub ShowNoAuthMsg()
        MsgBox(My.Resources.msgNoAuth, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Public Sub ShowOnlyAdminMsg()
        MsgBox(My.Resources.msgOnlyAdmin, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Public Function CheckAuth(ByVal itemId As Integer) As Boolean
        If strUsrId = "admin" Then
            Return True
        End If
        If blUsrAuth = False Then
            RefreshAuth()

        End If
        If dtUsrAuth.Rows.Count = 0 Then Return False
        Dim c1 As String = "COUNT (" & c_IDColumnName & ")"
        Dim c2 As String = c_ItemIDColumnName & "=" & itemId.ToString
        If dtUsrAuth.Compute(c1, c2) > 0 Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function CheckAuthAdmin() As Boolean
        If strUsrId.ToLower = "admin" Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub mnuCheckBookRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCheckBookRec.Click
        If Not subfrmBookIssueRec Is Nothing Then
            If subfrmBookIssueRec.Created = True Then
                FindSubFrom(subfrmBookIssueRec)
            Else
                subfrmBookIssueRec = New frmBookIssueRec()
                AddMyTabPage(subfrmBookIssueRec)
            End If
        Else
            subfrmBookIssueRec = New frmBookIssueRec()
            AddMyTabPage(subfrmBookIssueRec)
        End If
    End Sub

    Private Sub mnuAddNewNote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAddNewNote.Click
        Dim frm As New frm2AddNote
        frm.ShowDialog()
    End Sub

    Private Sub mnuBookIssuePunch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuBookIssuePunch.Click
        BookIssuePunch()
    End Sub

    Private Sub BookIssuePunch()
        If Not subfrmStuBookIssue Is Nothing Then
            If subfrmStuBookIssue.Created = True Then
                FindSubFrom(subfrmStuBookIssue)
            Else
                subfrmStuBookIssue = New frmStuBookIssue()
                AddMyTabPage(subfrmStuBookIssue)
            End If
        Else
            subfrmStuBookIssue = New frmStuBookIssue()
            AddMyTabPage(subfrmStuBookIssue)
        End If
    End Sub

    Private Sub mnuClassRetainAnalysis_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClassRetainAnalysis.Click
        If Not subfrmClassContRate Is Nothing Then
            If subfrmClassContRate.Created = True Then
                FindSubFrom(subfrmClassContRate)
            Else
                subfrmClassContRate = New frmClassContRate()
                AddMyTabPage(subfrmClassContRate)
            End If
        Else
            subfrmClassContRate = New frmClassContRate()
            AddMyTabPage(subfrmClassContRate)
        End If
    End Sub

    Private Sub mnuClassTransferRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClassTransferRec.Click
        If Not subfrmClassTransfer Is Nothing Then
            If subfrmClassTransfer.Created = True Then
                FindSubFrom(subfrmClassTransfer)
            Else
                subfrmClassTransfer = New frmClassTransfer()
                AddMyTabPage(subfrmClassTransfer)
            End If
        Else
            subfrmClassTransfer = New frmClassTransfer()
            AddMyTabPage(subfrmClassTransfer)
        End If
    End Sub

    Private Sub mnuMulBookIssuePunch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuMulBookIssuePunch.Click
        'If Not subfrmDiscountSearch Is Nothing Then
        '    If subfrmDiscountSearch.Created = True Then
        '        FindSubFrom(subfrmDiscountSearch)
        '    Else
        '        subfrmDiscountSearch = New frmDiscountSearch()
        '        AddMyTabPage(subfrmDiscountSearch)
        '    End If
        'Else
        '    subfrmDiscountSearch = New frmDiscountSearch()
        '    AddMyTabPage(subfrmDiscountSearch)
        'End If
    End Sub

    Private Sub mnuNoteRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNoteRec.Click
        If Not subfrmStuNoteRec Is Nothing Then
            If subfrmStuNoteRec.Created = True Then
                FindSubFrom(subfrmStuNoteRec)
            Else
                subfrmStuNoteRec = New frmStuNoteRec()
                AddMyTabPage(subfrmStuNoteRec)
            End If
        Else
            subfrmStuNoteRec = New frmStuNoteRec()
            AddMyTabPage(subfrmStuNoteRec)
        End If
    End Sub

    Private Sub mnuPotentialStu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPotentialStu.Click
        If CheckAuth(64) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmPotentialStu Is Nothing Then
            If subfrmPotentialStu.Created = True Then
                FindSubFrom(subfrmPotentialStu)
            Else
                subfrmPotentialStu = New frmPotentialStu()
                AddMyTabPage(subfrmPotentialStu)
            End If
        Else
            subfrmPotentialStu = New frmPotentialStu()
            AddMyTabPage(subfrmPotentialStu)
        End If
    End Sub

    Private Sub mnuPunchBulletin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPunchBulletin.Click
        If Not subfrmPunchBoard Is Nothing Then
            If subfrmPunchBoard.Created = True Then
                FindSubFrom(subfrmPunchBoard)
            Else
                subfrmPunchBoard = New frmPunchBoard()
                AddMyTabPage(subfrmPunchBoard)
            End If
        Else
            subfrmPunchBoard = New frmPunchBoard()
            AddMyTabPage(subfrmPunchBoard)
        End If
    End Sub

    Private Sub mnuPunchClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPunchClass.Click
        PunchClass()
    End Sub

    Private Sub PunchClass()
        If Not subfrmClassPunch Is Nothing Then
            If subfrmClassPunch.Created = True Then
                FindSubFrom(subfrmClassPunch)
            Else
                subfrmClassPunch = New frmClassPunch()
                AddMyTabPage(subfrmClassPunch)
            End If
        Else
            subfrmClassPunch = New frmClassPunch()
            AddMyTabPage(subfrmClassPunch)
        End If
    End Sub

    Private Sub mnuPunchMakeupClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPunchMakeupClass.Click
        PunchMakeupClass()
    End Sub

    Private Sub PunchMakeupClass()
        If Not subfrmPunchMakeup Is Nothing Then
            If subfrmPunchMakeup.Created = True Then
                FindSubFrom(subfrmPunchMakeup)
            Else
                subfrmPunchMakeup = New frmPunchMakeup()
                AddMyTabPage(subfrmPunchMakeup)
            End If
        Else
            subfrmPunchMakeup = New frmPunchMakeup()
            AddMyTabPage(subfrmPunchMakeup)
        End If
    End Sub

    Private Sub mnuPunchRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPunchRec.Click
        If CheckAuth(46) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmPunchRec Is Nothing Then
            If subfrmPunchRec.Created = True Then
                FindSubFrom(subfrmPunchRec)
            Else
                subfrmPunchRec = New frmPunchRec()
                AddMyTabPage(subfrmPunchRec)
            End If
        Else
            subfrmPunchRec = New frmPunchRec()
            AddMyTabPage(subfrmPunchRec)
        End If
    End Sub


    Private Sub mnuSearchStuByClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchStuByClass.Click
        SearchStuByClass()
    End Sub

    Private Sub SearchStuByClass()
        If Not subfrmStuSearchClass Is Nothing Then
            If subfrmStuSearchClass.Created = True Then
                FindSubFrom(subfrmStuSearchClass)
            Else
                subfrmStuSearchClass = New frmStuSearchClass()
                AddMyTabPage(subfrmStuSearchClass)
            End If
        Else
            subfrmStuSearchClass = New frmStuSearchClass()
            AddMyTabPage(subfrmStuSearchClass)
        End If
    End Sub

    Private Sub mnuSearchStuByMulClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchStuByMulClass.Click
        If Not subfrmStuSearchMulClass Is Nothing Then
            If subfrmStuSearchMulClass.Created = True Then
                FindSubFrom(subfrmStuSearchMulClass)
            Else
                subfrmStuSearchMulClass = New frmStuSearchMulClass()
                AddMyTabPage(subfrmStuSearchMulClass)
            End If
        Else
            subfrmStuSearchMulClass = New frmStuSearchMulClass()
            AddMyTabPage(subfrmStuSearchMulClass)
        End If
    End Sub

    Private Sub mnuSearchStuBySchool_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchStuBySchool.Click
        If Not subfrmStuSearchSchool Is Nothing Then
            If subfrmStuSearchSchool.Created = True Then
                FindSubFrom(subfrmStuSearchSchool)
            Else
                subfrmStuSearchSchool = New frmStuSearchSchool()
                AddMyTabPage(subfrmStuSearchSchool)
            End If
        Else
            subfrmStuSearchSchool = New frmStuSearchSchool()
            AddMyTabPage(subfrmStuSearchSchool)
        End If
    End Sub

    Private Sub mnuSeatMapping_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSeatMapping.Click
        If Not subfrmSeatPlan Is Nothing Then
            If subfrmSeatPlan.Created = True Then
                FindSubFrom(subfrmSeatPlan)
            Else
                subfrmSeatPlan = New frmSeatPlan()
                AddMyTabPage(subfrmSeatPlan)
            End If
        Else
            subfrmSeatPlan = New frmSeatPlan()
            AddMyTabPage(subfrmSeatPlan)
        End If

        'If Not subfrmStuSearchClass Is Nothing Then
        '    If subfrmStuSearchClass.Created = True Then
        '        FindSubFrom(subfrmStuSearchClass)
        '    Else
        '        subfrmStuSearchClass = New frmStuSearchClass()
        '        AddMyTabPage(subfrmStuSearchClass)
        '    End If
        'Else
        '    subfrmStuSearchClass = New frmStuSearchClass()
        '    AddMyTabPage(subfrmStuSearchClass)
        'End If

    End Sub

    Private Sub mnuSetBookToIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetBookToIssue.Click
        If Not subfrmSetBook Is Nothing Then
            If subfrmSetBook.Created = True Then
                FindSubFrom(subfrmSetBook)
            Else
                subfrmSetBook = New frmSetBook()
                AddMyTabPage(subfrmSetBook)
            End If
        Else
            subfrmSetBook = New frmSetBook()
            AddMyTabPage(subfrmSetBook)
        End If
    End Sub

    Private Sub mnuShowUpRate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuShowUpRate.Click
        If Not subfrmShowupRate Is Nothing Then
            If subfrmShowupRate.Created = True Then
                FindSubFrom(subfrmShowupRate)
            Else
                subfrmShowupRate = New frmShowupRate()
                AddMyTabPage(subfrmShowupRate)
            End If
        Else
            subfrmShowupRate = New frmShowupRate()
            AddMyTabPage(subfrmShowupRate)
        End If
    End Sub

    Private Sub mnuStuAbsent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuAbsent.Click
        StuAbsent()
    End Sub

    Private Sub StuAbsent()
        If Not subfrmStuAbsentToday Is Nothing Then
            If subfrmStuAbsentToday.Created = True Then
                FindSubFrom(subfrmStuAbsentToday)
            Else
                subfrmStuAbsentToday = New frmStuAbsentToday()
                AddMyTabPage(subfrmStuAbsentToday)
            End If
        Else
            subfrmStuAbsentToday = New frmStuAbsentToday()
            AddMyTabPage(subfrmStuAbsentToday)
        End If
    End Sub

    Private Sub mnuStuCardReIssue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuCardReIssue.Click
        If Not subfrmCardReissueRec Is Nothing Then
            If subfrmCardReissueRec.Created = True Then
                FindSubFrom(subfrmCardReissueRec)
            Else
                subfrmCardReissueRec = New frmCardReissueRec()
                AddMyTabPage(subfrmCardReissueRec)
            End If
        Else
            subfrmCardReissueRec = New frmCardReissueRec()
            AddMyTabPage(subfrmCardReissueRec)
        End If
    End Sub

    Private Sub mnuStudentHere_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStudentHere.Click
        StudentHere()
    End Sub

    Private Sub StudentHere()
        If Not subfrmStuHereToday Is Nothing Then
            If subfrmStuHereToday.Created = True Then
                FindSubFrom(subfrmStuHereToday)
            Else
                subfrmStuHereToday = New frmStuHereToday()
                AddMyTabPage(subfrmStuHereToday)
            End If
        Else
            subfrmStuHereToday = New frmStuHereToday()
            AddMyTabPage(subfrmStuHereToday)
        End If
    End Sub

    Private Sub mnuStuInfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuInfo.Click
        If Not subfrmStuRecSearch Is Nothing Then
            If subfrmStuRecSearch.Created = True Then
                FindSubFrom(subfrmStuRecSearch)
            Else
                subfrmStuRecSearch = New frmStuRecSearch()
                AddMyTabPage(subfrmStuRecSearch)
            End If
        Else
            subfrmStuRecSearch = New frmStuRecSearch()
            AddMyTabPage(subfrmStuRecSearch)
        End If
    End Sub

    Private Sub mnuStuNameRepeat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuNameRepeat.Click
        If Not subfrmNameRepeatStu Is Nothing Then
            If subfrmNameRepeatStu.Created = True Then
                FindSubFrom(subfrmNameRepeatStu)
            Else
                subfrmNameRepeatStu = New frmNameRepeatStu()
                AddMyTabPage(subfrmNameRepeatStu)
            End If
        Else
            subfrmNameRepeatStu = New frmNameRepeatStu()
            AddMyTabPage(subfrmNameRepeatStu)
        End If
    End Sub

    Private Sub mnuStuSiblings_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuSiblings.Click
        If Not subfrmFamilySearch Is Nothing Then
            If subfrmFamilySearch.Created = True Then
                FindSubFrom(subfrmFamilySearch)
            Else
                subfrmFamilySearch = New frmFamilySearch()
                AddMyTabPage(subfrmFamilySearch)
            End If
        Else
            subfrmFamilySearch = New frmFamilySearch()
            AddMyTabPage(subfrmFamilySearch)
        End If
    End Sub

    Private Sub mnuStuWithoutBook_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuWithoutBook.Click
        If Not subfrmStuNoBook Is Nothing Then
            If subfrmStuNoBook.Created = True Then
                FindSubFrom(subfrmStuNoBook)
            Else
                subfrmStuNoBook = New frmStuNoBook()
                AddMyTabPage(subfrmStuNoBook)
            End If
        Else
            subfrmStuNoBook = New frmStuNoBook()
            AddMyTabPage(subfrmStuNoBook)
        End If
    End Sub

    Public Function GetUsrId() As String
        Return strUsrId
    End Function

    Public Sub SetUsrId(ByVal id As String)
        strUsrId = id
    End Sub

    Public Sub SetUsrName(ByVal id As String)
        strUsrName = id
        tsLabelUsrName.Text = My.Resources.user & ": " & id
    End Sub

    Public Function GetUsrName() As String
        Return strUsrName
    End Function

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub RefreshData()
        blBooks = False
        blClassInfo = False
        blContent = False
        blSession = False
        blUsrAuth = False
    End Sub

    Public Sub RefreshAuth()
        dtUsrAuth = objCsol.ListUsrAuthority(strUsrId)
        blUsrAuth = True
    End Sub

    Public Sub RefreshClassAuth()
        Dim ds As DataSet
        ds = objCsol.ListUsrAllAuthority(strUsrId)
        dtUsrAuth = ds.Tables(c_UsrAuthorityTableName)
        dtClassAuth = ds.Tables(c_ClassAuthorityTableName)
        dtSubClassAuth = ds.Tables(c_SubClassAuthorityTableName)
        dtAllClassAuth = ds.Tables(c_AllClassAuthorityTableName)
    End Sub

    Public Sub RefreshSchool()
        dtSchool = objCsol.ListSchools
        blSch = True
    End Sub

    Public Sub RefreshSchoolGrp()
        dtSchGrp = objCsol.ListSchGrp
        blSchGrp = True
    End Sub

    Public Sub RefreshSchoolType()
        dtSchType = objCsol.ListSchType
        blSchType = True
    End Sub

    Public Sub RefreshSibType()
        dtSibType = objCsol.ListSibType
        blSibType = True
    End Sub

    Public Sub RefreshClassRegInfo()
        Dim ds As DataSet = objCsol.GetClassRegInfo
        dtClass = ds.Tables(c_ClassListDataTableName)
        dtSubClass = ds.Tables(c_SubClassListDataTableName)
        dtSales = ds.Tables(c_SalesListDataTableName)
        dtClassRoom = ds.Tables(c_ClassRoomListTableName)
        dtSubjects = ds.Tables(c_SubjectsTableName)
        blClass = True
    End Sub

    Public Sub RefreshClass()
        Dim dt As DataTable = objCsol.GetClassList()
        dtClass = dt
    End Sub

    Public Sub RefreshClassPunchStuInfo()
        dtClassPunchClass = objCsol.GetClassPunchClass(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
        dtClassPunchSubClass = objCsol.GetClassPunchSubClass(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
        dtClassPunchSession = objCsol.GetClassPunchSession(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
    End Sub
    Public Sub RefreshClassPunchBookContent()
        dtClassPunchBook = objCsol.GetClassPunchBook(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
        dtClassPunchContent = objCsol.GetClassPunchContent(CInt(IIf(Now.DayOfWeek = 0, 7, Now.DayOfWeek)))
    End Sub
    Public Sub RefreshClassInfoSet()
        dsClassInfo = objCsol.ListClassInfoSet(GetUsrId)
        blClassInfo = True
    End Sub

    Public Sub RefreshTeleInterviewType()
        dtTeleInterviewType = objCsol.ListTeleInterviewType
        blTeleInterviewType = True
    End Sub

    Public Sub RefreshUserNameList()
        dtUserNameList = objCsol.ListUserNameList
        blUserNameList = True
    End Sub

    Public Sub RefreshJournalGrp()
        dtJournalGrp = objCsol.ListJournalGrp
        blJournal = True
    End Sub

    Public Function GetJournalGrp() As DataTable
        If blJournal = False Then
            RefreshJournalGrp()

        End If
        Return dtJournalGrp
    End Function

    Public Sub RefreshDictInfo()
        dtDiscountDict = objCsol.ListDiscountDict
        dtDiscountDictType = objCsol.ListDiscountDictType
        dtRemarksDict = objCsol.ListReceiptNoteDict
        blDisc = True
    End Sub

    Public Sub RefreshReceiptPrintOpt()
        dtReceiptPrint = objCsol.GetReceiptPrint
        blReceipt = True
    End Sub

    Public Function GetReceiptPrint() As DataTable
        If blReceipt = False Then
            RefreshReceiptPrintOpt()

        End If
        Return dtReceiptPrint
    End Function

    Public Function GetDiscountDict() As DataTable
        If blDisc = False Then
            RefreshDictInfo()

        End If
        GetDiscountDict = dtDiscountDict
    End Function

    Public Function GetDiscountDictType() As DataTable
        If blDisc = False Then
            RefreshDictInfo()
        End If
        GetDiscountDictType = dtDiscountDictType
    End Function

    Public Function GetRemarkdsDict() As DataTable
        If blDisc = False Then
            RefreshDictInfo()
        End If
        GetRemarkdsDict = dtRemarksDict
    End Function

    Public Function GetSchList() As DataTable
        If blSch = False Then
            RefreshSchool()

        End If
        Return dtSchool
    End Function

    Public Function GetSchTypes() As DataTable
        If blSchType = False Then
            RefreshSchoolType()

        End If
        Return dtSchType
    End Function

    Public Function GetSchGrpList() As DataTable
        If blSchGrp = False Then
            RefreshSchoolGrp()

        End If
        Return dtSchGrp
    End Function

    Public Function GetSibTypeList() As DataTable
        If blSibType = False Then
            RefreshSibType()

        End If
        Return dtSibType
    End Function

    Public Function GetClassList() As DataTable
        If blClass = False Then
            RefreshClassRegInfo()

        End If
        Return dtClass
    End Function

    Public Function GetClassList_Fast() As DataTable
        If blClass = False Then
            RefreshClass()
        End If
        Return dtClass
    End Function

    Public Function GetClassInfoSet() As DataSet
        If blClassInfo = False Then
            RefreshClassInfoSet()
        ElseIf blClassInfo = True Then
            RefreshClassInfoSet()
        End If
        Return dsClassInfo
    End Function

    Public Function GetClassRooms() As DataTable
        If blClass = False Then
            RefreshClassRegInfo()
        End If
        Return dtClassRoom
    End Function

    Public Function GetTeleInterviewType() As DataTable
        If blTeleInterviewType = False Then
            RefreshTeleInterviewType()
        End If
        Return dtTeleInterviewType
    End Function

    Public Function GetUserNameList() As DataTable
        If blUserNameList = False Then
            RefreshUserNameList()
        End If
        Return dtUserNameList
    End Function

    Public Function GetSubjects() As DataTable
        If blClass = False Then
            RefreshClassRegInfo()
        End If
        Return dtSubjects
    End Function

    Public Function GetSubClassList() As DataTable
        If blClass = False Then
            RefreshClassRegInfo()
        End If
        Return dtSubClass
    End Function

    Public Function GetSalesList() As DataTable
        If blClass = False Then
            RefreshClassRegInfo()
        End If
        Return dtSales
    End Function

    Public Function GetClassFee(ByVal subClassId As Integer) As Integer
        Dim dt As DataTable
        Dim dt2 As DataTable
        Dim id As Integer
        Dim fee As Integer

        dt = GetSubClassList()
        For index As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(index).Item(c_IDColumnName) = subClassId Then
                id = dt.Rows(index).Item(c_ClassIDColumnName)
                Exit For
            End If
        Next

        dt2 = GetClassList()
        For index As Integer = 0 To dt2.Rows.Count - 1
            If dt2.Rows(index).Item(c_IDColumnName) = id Then
                fee = dt2.Rows(index).Item(c_FeeColumnName)
                Exit For
            End If
        Next

        Return fee
    End Function

    Public Function GetClassFee2(ByVal classId As Integer) As Integer
        Dim dt2 As DataTable
        Dim fee As Integer

        dt2 = GetClassList()
        For index As Integer = 0 To dt2.Rows.Count - 1
            If dt2.Rows(index).Item(c_IDColumnName) = classId Then
                fee = dt2.Rows(index).Item(c_FeeColumnName)
                Exit For
            End If
        Next

        Return fee
    End Function

    Public Sub SetCurrentStu(ByVal strId As String)
        strCurrentStuId = strId
    End Sub

    Public Function GetCurrentStu() As String
        Return strCurrentStuId
    End Function

    Public Sub SetSeatNum(ByVal strSt As String)
        strSeatNum = strSt

    End Sub

    Public Function GetSeatNum() As String

        Return strSeatNum
    End Function

    Public Sub DisplayStuInfo(ByVal strId As String, ByVal intAction As Integer)
        If intAction = c_DisplayStuInfoActionPay Then
            Dim result As MsgBoxResult = _
                        MsgBox(My.Resources.msgAskPay, MsgBoxStyle.YesNo, My.Resources.msgTitle)
            If result = MsgBoxResult.Yes Then

                If Not subfrmStuInfo Is Nothing Then
                    If subfrmStuInfo.Created = True Then
                        subfrmStuInfo.RefreshAllData(strId)
                        FindSubFrom(subfrmStuInfo)
                        ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                    Else
                        subfrmStuInfo = New frmStuInfo(strId, intAction)
                        AddMyTabPage(subfrmStuInfo)
                    End If
                Else
                    subfrmStuInfo = New frmStuInfo(strId, intAction)
                    AddMyTabPage(subfrmStuInfo)
                End If
            Else
                If Not subfrmStuInfo Is Nothing Then
                    If subfrmStuInfo.Created = True Then
                        subfrmStuInfo.RefreshAllData(strId)
                        FindSubFrom(subfrmStuInfo)
                        ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                    Else
                        subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
                        AddMyTabPage(subfrmStuInfo)
                    End If
                Else
                    subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
                    AddMyTabPage(subfrmStuInfo)
                End If
            End If
        ElseIf intAction = c_DisplayStuInfoActionReg Then
            Dim result As MsgBoxResult = MsgBox(My.Resources.msgAskReg, MsgBoxStyle.YesNo, My.Resources.msgTitle)
            If result = MsgBoxResult.Yes Then

                If Not subfrmStuInfo Is Nothing Then
                    If subfrmStuInfo.Created = True Then
                        subfrmStuInfo.RefreshAllData(strId)
                        FindSubFrom(subfrmStuInfo)
                        ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                    Else
                        subfrmStuInfo = New frmStuInfo(strId, intAction)
                        AddMyTabPage(subfrmStuInfo)
                    End If
                Else
                    subfrmStuInfo = New frmStuInfo(strId, intAction)
                    AddMyTabPage(subfrmStuInfo)
                End If
            Else
                If Not subfrmStuInfo Is Nothing Then
                    If subfrmStuInfo.Created = True Then
                        subfrmStuInfo.RefreshAllData(strId)
                        FindSubFrom(subfrmStuInfo)
                        ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                    Else
                        subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
                        AddMyTabPage(subfrmStuInfo)
                    End If
                Else
                    subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
                    AddMyTabPage(subfrmStuInfo)
                End If
            End If
        ElseIf intAction = c_DisplayStuInfoActionOld Then
            If Not subfrmStuInfo Is Nothing Then
                If subfrmStuInfo.Created = True Then
                    subfrmStuInfo.RefreshAllData(strId)
                    FindSubFrom(subfrmStuInfo)
                    ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                Else
                    subfrmStuInfo = New frmStuInfo(strId, intAction)
                    AddMyTabPage(subfrmStuInfo)
                End If
            Else
                subfrmStuInfo = New frmStuInfo(strId, intAction)
                AddMyTabPage(subfrmStuInfo)
            End If
        Else
            If Not subfrmStuInfo Is Nothing Then
                If subfrmStuInfo.Created = True Then
                    subfrmStuInfo.Close()
                    subfrmStuInfo = New frmStuInfo(strId, intAction)
                    AddMyTabPage(subfrmStuInfo)
                    '    subfrmStuInfo.RefreshAllData(strId)
                    '    FindSubFrom(subfrmStuInfo)
                    '    ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
                Else
                    subfrmStuInfo = New frmStuInfo(strId, intAction)
                    AddMyTabPage(subfrmStuInfo)
                End If
            Else
                subfrmStuInfo = New frmStuInfo(strId, intAction)
                AddMyTabPage(subfrmStuInfo)
            End If
        End If
    End Sub

    Public Sub DisplayNextStuInfo(ByVal strId As String)

        If Not subfrmStuInfo Is Nothing Then
            If subfrmStuInfo.Created = True Then
                subfrmStuInfo.RefreshAllData(strId)
                FindSubFrom(subfrmStuInfo)
                ChangeTabText(subfrmStuInfo.Text, subfrmStuInfo.Text)
            Else
                subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
                AddMyTabPage(subfrmStuInfo)
            End If
        Else
            subfrmStuInfo = New frmStuInfo(strId, c_DisplayStuInfoActionNo)
            AddMyTabPage(subfrmStuInfo)
        End If

    End Sub

    Public Sub DisplayUsrInfo(ByRef d As DataTable)
        If Not subfrmUsrInfo Is Nothing Then
            If subfrmUsrInfo.Created = True Then
                subfrmUsrInfo.RefreshData(d)
                FindSubFrom(subfrmUsrInfo)
                ChangeTabText(subfrmUsrInfo.Text, subfrmUsrInfo.Text)
            Else
                subfrmUsrInfo = New frmUsrInfo(d)
                AddMyTabPage(subfrmUsrInfo)
            End If
        Else
            subfrmUsrInfo = New frmUsrInfo(d)
            AddMyTabPage(subfrmUsrInfo)
        End If
    End Sub

    Public Sub RefreshUsrListForm()
        If blOk Then
            If Not subfrmSetAuthorityPwd Is Nothing Then
                If subfrmSetAuthorityPwd.Created Then
                    subfrmSetAuthorityPwd.RefreshData()
                End If
            End If
            blOk = False
        End If
    End Sub

    Private Sub toolstrbutNewStu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutNewStu.Click
        NewStudent()
    End Sub

    Private Sub mnuExpenseGrp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExpenseGrp.Click
        subfrmJournalGrp = New frm2JournalGrp()
        subfrmJournalGrp.ShowDialog()
        RefreshJournalGrp()
    End Sub

    Private Sub mnuExpenseNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExpenseNew.Click
        If CheckAuth(35) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        subfrmAddJournalRec = New frm2AddJournalRec()
        subfrmAddJournalRec.ShowDialog()
    End Sub

    Public Sub ShowAddClassRoom()
        If Not subfrmAddClassRoom Is Nothing Then
            If subfrmAddClassRoom.Created = True Then
                FindSubFrom(subfrmAddClassRoom)
            Else
                subfrmAddClassRoom = New frmAddClassRoom()
                AddMyTabPage(subfrmAddClassRoom)
            End If
        Else
            subfrmAddClassRoom = New frmAddClassRoom()
            AddMyTabPage(subfrmAddClassRoom)
        End If
    End Sub

    Public Sub RefreshClassRoomListFrm()
        If Not subfrmSetClassRoom Is Nothing Then
            If subfrmSetClassRoom.Created = True Then
                subfrmSetClassRoom.RefreshData()
            End If
        End If
    End Sub

    Public Sub RefreshClassContentTb()
        dtClassContent = objCsol.ListClassContents(strUsrId)
        blContent = True
    End Sub
    Public Sub RefreshClassContentTbNoByUsr()
        dtClassContent = objCsol.ListClassContents()
        blContent = True
    End Sub

    Public Sub RefreshBookTb()
        dtBook = objCsol.ListBooks(strUsrId)
        blBooks = True
    End Sub

    Public Sub RefreshBookStock(ByVal id As Integer, ByVal s As Integer)
        For i As Integer = 0 To dtBook.Rows.Count - 1
            If dtBook.Rows(i).Item(c_IDColumnName) = id Then
                dtBook.Rows(i).Item(c_StockColumnName) = s
                Exit For
            End If
        Next
    End Sub

    Public Sub DelBook(ByVal id As Integer)
        'For i As Integer = 0 To dtBook.Rows.Count - 1
        '    If dtBook.Rows(i).Item(c_IDColumnName) = id Then
        '        dtBook.Rows.RemoveAt(i)
        '        Exit For
        '    End If
        'Next
        Dim lst As ArrayList = New ArrayList
        For i As Integer = 0 To dtBook.Rows.Count - 1
            If dtBook.Rows(i).Item(c_IDColumnName) = id Then
                lst.Add(i)
            End If
        Next
        For i As Integer = 0 To lst.Count - 1
            dtBook.Rows.RemoveAt(lst(i) - i)
        Next

    End Sub

    Public Sub RefreshBookTbWithoutUsrID()
        dtBookNoUsrID = objCsol.ListBooksWhithoutUsrID
        blBooksWithUsrID = True
    End Sub


    Public Function GetClassPunchClass() As DataTable
        Return dtClassPunchClass
    End Function
    Public Function GetClassPunchSubClass() As DataTable
        Return dtClassPunchSubClass
    End Function
    Public Function GetClassPunchSession() As DataTable
        Return dtClassPunchSession
    End Function
    Public Function GetClassPunchBook() As DataTable
        Return dtClassPunchBook
    End Function
    Public Function GetClassPunchContent() As DataTable
        Return dtClassPunchContent
    End Function


    Public Function GetBooks() As DataTable

        RefreshBookTb()
        'If blBooks = False Then
        '    RefreshBookTb()

        'End If
        Return dtBook
    End Function
    Public Function GetRefreshBooks() As DataTable
        RefreshBookTb()
        Return dtBook

    End Function

    Public Function GetRefreshBooks(ByVal classid As Integer) As DataTable
        Dim dt As New DataTable
        dt = objCsol.ListBookksByClassId(strUsrId, classid)
        Return dt

    End Function

    Public Function GetBooksNoUsrID() As DataTable
        If blBooksWithUsrID = False Then
            RefreshBookTbWithoutUsrID()

        End If
        Return dtBookNoUsrID
    End Function

    Public Function GetContents() As DataTable
        If blContent = False Then
            RefreshClassContentTb()

        End If
        Return dtClassContent
    End Function
    Public Function GetContentsNoByUsr() As DataTable
        Dim dt As New DataTable
        dt = objCsol.ListClassContents()
        Return dt
    End Function


    Public Sub RefreshClassSessionTb()
        dtClassSession = objCsol.ListClassSessions(strUsrId)
        blSession = True
    End Sub

    Public Function GetSessions() As DataTable
        If blSession = False Then
            RefreshClassSessionTb()

        End If
        Return dtClassSession
    End Function

    Public Sub RefreshClassListFrm()
        If Not subfrmSetClass Is Nothing Then
            If subfrmSetClass.Created = True Then
                subfrmSetClass.RefreshData()
            End If
        End If
    End Sub

    Public Sub ShowModifyClassRoom(ByVal id As Integer, ByVal name As String, _
                                   ByVal row As Integer, ByVal col As Integer, _
                                   ByVal style As Integer)
        If Not subfrmModifyClassRoom Is Nothing Then
            If subfrmModifyClassRoom.Created = True Then
                FindSubFrom(subfrmModifyClassRoom)
            Else
                subfrmModifyClassRoom = New frmModifyClassRoom(id, name, row, col, style)
                AddMyTabPage(subfrmModifyClassRoom)
            End If
        Else
            subfrmModifyClassRoom = New frmModifyClassRoom(id, name, row, col, style)
            AddMyTabPage(subfrmModifyClassRoom)
        End If
    End Sub

    Public Sub ShowClassToday()
        If Not subfrmClassToday Is Nothing Then
            If subfrmClassToday.Created = True Then
                FindSubFrom(subfrmClassToday)
            Else
                subfrmClassToday = New frmClassToday()
                AddMyTabPage(subfrmClassToday)
            End If
        Else
            subfrmClassToday = New frmClassToday()
            AddMyTabPage(subfrmClassToday)
        End If
    End Sub

    Public Sub SetlstStuSubClass(ByVal x As ArrayList)
        lstStuSubClass.Clear()
        lstStuSubClass = x
    End Sub

    Public Function GetlstStuSubClass() As ArrayList
        Return lstStuSubClass
    End Function

    Public Sub SetOkState(ByVal ok As Boolean)
        blOk = ok
    End Sub

    Public Function GetOkState() As Boolean
        Return blOk
    End Function

    Public Sub SetCurrentStringArr(ByVal s As ArrayList)
        strCurrentStringArr = s
    End Sub

    Public Function GetCurrentStringArr() As ArrayList
        Return strCurrentStringArr
    End Function

    Public Sub SetCurrentString(ByVal s As String)
        strCurrentString = s
    End Sub

    Public Function GetCurrentString() As String
        Return strCurrentString
    End Function

    Public Sub SetCurrentValueArr(ByVal i As ArrayList)
        intCurrentValueArr = i
    End Sub

    Public Function GetCurrentValueArr() As ArrayList
        Return intCurrentValueArr
    End Function

    Public Sub SetCurrentValue(ByVal i As Integer)
        intCurrentValue = i
    End Sub

    Public Function GetCurrentValue() As Integer
        Return intCurrentValue
    End Function

    Private Sub mnuSearchStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearchStudent.Click
        SearchStudent()
    End Sub

    Private Sub SearchStudent()
        Dim frm As New frm2SearchStu
        frm.ShowDialog()
        If blOk Then
            blOk = False
            DisplayStuInfo(strCurrentStuId, 0)
        End If
    End Sub

    Public Sub ShowSearchStu2(ByVal frmId As Integer)
        If Not subfrm2SearchStu2 Is Nothing Then
            If subfrm2SearchStu2.Created = True Then
                subfrm2SearchStu2.BringToFront()
            Else
                subfrm2SearchStu2 = New frm2SearchStuSmall
                'subfrm2SearchStu2.MdiParent = Me
                'subfrm2SearchStu2.WindowState = FormWindowState.Normal
                subfrm2SearchStu2.Show()
            End If
        Else
            subfrm2SearchStu2 = New frm2SearchStuSmall
            'subfrm2SearchStu2.MdiParent = Me
            'subfrm2SearchStu2.WindowState = FormWindowState.Normal
            subfrm2SearchStu2.Show()
        End If
        subfrm2SearchStu2.SetFrmId(frmId)
    End Sub

    Public Sub CallSearchFrm(ByVal id As Integer, _
                             ByVal action As Integer, ByVal kw As String, _
                             ByVal type As Integer)
        Select Case id
            Case 1
                If Not subfrmStuSearchSchool Is Nothing Then
                    If subfrmStuSearchSchool.Created Then
                        subfrmStuSearchSchool.SearchStu(action, kw, type)
                    End If
                End If
            Case 2
                If Not subfrmStuSearchClass Is Nothing Then
                    If subfrmStuSearchClass.Created Then
                        subfrmStuSearchClass.SearchStu(action, kw, type)
                    End If
                End If
            Case 3
                If Not subfrmStuSearchMulClass Is Nothing Then
                    If subfrmStuSearchMulClass.Created Then
                        subfrmStuSearchMulClass.SearchStu(action, kw, type)
                    End If
                End If
            Case 4
                If Not subfrmPotentialStu Is Nothing Then
                    If subfrmPotentialStu.Created Then
                        subfrmPotentialStu.SearchStu(action, kw, type)
                    End If
                End If
            Case 5
                If Not subfrmFamilySearch Is Nothing Then
                    If subfrmFamilySearch.Created Then
                        subfrmFamilySearch.SearchStu(action, kw, type)
                    End If
                End If
            Case 6
                If Not subfrmStuHereToday Is Nothing Then
                    If subfrmStuHereToday.Created Then
                        subfrmStuHereToday.SearchStu(action, kw, type)
                    End If
                End If
            Case 7
                If Not subfrmClassTransfer Is Nothing Then
                    If subfrmClassTransfer.Created Then
                        subfrmClassTransfer.SearchStu(action, kw, type)
                    End If
                End If
            Case 8
                If Not subfrmPunchRec Is Nothing Then
                    If subfrmPunchRec.Created Then
                        subfrmPunchRec.SearchStu(action, kw, type)
                    End If
                End If
            Case 9
                If Not subfrmBookIssueRec Is Nothing Then
                    If subfrmBookIssueRec.Created Then
                        subfrmBookIssueRec.SearchStu(action, kw, type)
                    End If
                End If
            Case 10
                If Not subfrmTeleInterviewSearch Is Nothing Then
                    If subfrmTeleInterviewSearch.Created Then
                        subfrmTeleInterviewSearch.SearchStu(action, kw, type)
                    End If
                End If
            Case 11
                If Not subfrmStuAbsentToday Is Nothing Then
                    If subfrmStuAbsentToday.Created Then
                        subfrmStuAbsentToday.SearchStu(action, kw, type)
                    End If
                End If
            Case 12
                If Not subfrmStuGradeMul Is Nothing Then
                    If subfrmStuGradeMul.Created Then
                        subfrmStuGradeMul.SearchStu(action, kw, type)
                    End If
                End If
            Case 13
                If Not subfrmStuDoneAssign Is Nothing Then
                    If subfrmStuDoneAssign.Created Then
                        subfrmStuDoneAssign.SearchStu(action, kw, type)
                    End If
                End If
        End Select
    End Sub

    Public Sub ShowSearchClass(ByVal frmId As Integer)
        If Not subfrm2SearchClass Is Nothing Then
            If subfrm2SearchClass.Created = True Then
                subfrm2SearchClass.BringToFront()
            Else
                subfrm2SearchClass = New frm2SearchClass
                'subfrm2SearchClass.MdiParent = Me
                'subfrm2SearchClass.WindowState = FormWindowState.Normal
                subfrm2SearchClass.Show()
            End If
        Else
            subfrm2SearchClass = New frm2SearchClass
            'subfrm2SearchClass.MdiParent = Me
            'subfrm2SearchClass.WindowState = FormWindowState.Normal
            subfrm2SearchClass.Show()
        End If
        subfrm2SearchClass.SetFrmId(frmId)
    End Sub

    Public Sub CallSearchClassFrm(ByVal id As Integer, _
                             ByVal action As Integer, ByVal kw As String, _
                             ByVal type As Integer)
        Select Case id
            Case 1
                If Not subfrmSetClass Is Nothing Then
                    If subfrmSetClass.Created Then
                        subfrmSetClass.SearchClass(action, kw, type)
                    End If
                End If
        End Select
    End Sub

    Public Function GetCurrentList() As ArrayList
        Return lstCurrent
    End Function

    Public Sub SetCurrentList(ByVal lst As ArrayList)
        lstCurrent = lst
    End Sub

    Public Function GetCurrentList2() As ArrayList
        Return lstCurrent2
    End Function

    Public Sub SetCurrentList2(ByVal lst As ArrayList)
        lstCurrent2 = lst
    End Sub

    Public Function GetSchName(ByVal id As Integer) As String
        If blSch = False Then
            RefreshSchool()
        End If
        Dim s As String = ""
        If dtSchool.Rows.Count > 0 Then
            'For index As Integer = 0 To dtSchool.Rows.Count - 1
            '    If dtSchool.Rows(index).Item(c_IDColumnName) = id Then
            '        s = dtSchool.Rows(index).Item(c_NameColumnName)
            '    End If
            'Next
            Dim rows() As DataRow = dtSchool.Select(String.Format("{0} = {1}", c_IDColumnName, id))
            If rows.Count > 0 Then
                s = rows(0).Item(c_NameColumnName)
            End If
        End If
        Return s
    End Function

    '20100516 sherry image version A
    Private Sub UploadImg()
        Dim dt As DataTable = objCsol.GetStuIdList
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim c1 As String = "COUNT (" & c_IDColumnName & ")"
        Dim c2 As String = c_IDColumnName & "="
        Dim id As String = ""
        Dim f As String = GetPhotoPath()
        Dim f2 As String = ""
        Dim fs As FileStream = Nothing
        Dim picture As Byte()

        'Try
        Dim di As New IO.DirectoryInfo(GetPhotoPath())
        Dim diar1 As IO.FileInfo() = di.GetFiles()
        Dim dra As IO.FileInfo
        Dim temp As Long
        Dim fi As FileInfo
        Dim lung As Integer

        'list the names of all files in the specified directory
        For Each dra In diar1
            If dra.Name.Length >= 12 Then
                id = dra.Name.Substring(0, 8)
                If dt.Compute(c1, c2 & id) > 0 Then
                    'Try
                    f2 = dra.FullName
                    fs = New FileStream(f2, FileMode.Open)
                    fi = New FileInfo(f2)
                    temp = fi.Length
                    lung = Convert.ToInt32(temp)

                    picture = New Byte(lung - 1) {}
                    fs.Read(picture, 0, lung)
                    fs.Close()
                    objCsol.UploadImg(id, picture)
                    'Catch ex As Exception
                    'Continue For
                    'End Try
                End If
            End If
        Next

        'Catch ec As Exception
        'do nothing here
        'End Try

    End Sub

    Private Sub mdiMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '20100516 sherry image version A start

        'LoadConfigData()
        'UploadImg()
        'MessageBox.Show("舊照片上載完畢, 請通知MIS更換為可使用的客戶端版本, 謝謝!")
        'Application.Exit()
        '20100516 sherry image version A end

        Dim frm As New frm2Login
        frm.ShowDialog()
        If blOk Then
            If intCurrentValue = 1 Then
                MsgBox(My.Resources.msgHasFailLogin & vbNewLine & strCurrentString, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
            BackgroundWorker1.RunWorkerAsync()  '起始背景執行的呼叫
            BackgroundWorker2.RunWorkerAsync()
            BackgroundWorker3.RunWorkerAsync()
            BackgroundWorker4.RunWorkerAsync()
            BackgroundWorker5.RunWorkerAsync()
            BackgroundWorker6.RunWorkerAsync()
            BackgroundWorker7.RunWorkerAsync()
            BackgroundWorker8.RunWorkerAsync()
        Else
            Application.Exit()
        End If
    End Sub

    Private Sub InitPrinter()
        'LoadConfigPrinter()
        If GetConfigPrinter.Length = 0 Then
            If Printing.PrinterSettings.InstalledPrinters.Count > 0 Then
                Dim s As String = Printing.PrinterSettings.InstalledPrinters(0)
                SetConfigPrinter(s)
            End If
        End If
    End Sub

    Private Sub mnuReLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuReLogin.Click
        ReLogin()
    End Sub
    Private Sub ReLogin()
        Dim frm As New frm2Login
        frm.ShowDialog()
        If blOk Then
            If intCurrentValue = 1 Then
                MsgBox(My.Resources.msgHasFailLogin & vbNewLine & strCurrentString, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
            For Each child In Me.MdiChildren
                child.Close()
            Next

            RefreshData()
        Else
            Application.Exit()
        End If
    End Sub
    Private Sub mnuChangePwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuChangePwd.Click
        Dim frm As New frm2ModifyPwd
        frm.ShowDialog()
    End Sub

    Private Sub mnuExit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExit.Click
        Application.Exit()
    End Sub

    Private Sub mnuUsrPwd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuUsrPwd.Click

        If CheckAuth(42) = False And CheckAuth(43) = False And CheckAuth(44) = False And CheckAuth(45) = False And CheckAuth(84) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If

        If Not subfrmSetAuthorityPwd Is Nothing Then
            If subfrmSetAuthorityPwd.Created = True Then
                FindSubFrom(subfrmSetAuthorityPwd)
            Else
                subfrmSetAuthorityPwd = New frmSetAuthorityPwd()
                AddMyTabPage(subfrmSetAuthorityPwd)
            End If
        Else
            subfrmSetAuthorityPwd = New frmSetAuthorityPwd()
            AddMyTabPage(subfrmSetAuthorityPwd)
        End If

    End Sub

    Private Sub mnuSetClassContent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetClassContent.Click
        Dim frm As New frm2SetClassContent
        frm.ShowDialog()
    End Sub

    Private Sub mnuFindPrinter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuFindPrinter.Click
        Dim frm As New frm2SetPrinter
        frm.ShowDialog()
    End Sub

    Private Sub toolstrbutStuPhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutStuPhoto.Click
        Dim frm1 As New frm2EnterStuId
        frm1.ShowDialog()
        If blOk Then
            Dim frm As New frm2Photo(strCurrentStuId)
            frm.ShowDialog()
        End If
    End Sub

    Private Sub mnuTakePhoto_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTakePhoto.Click
        Dim frm1 As New frm2EnterStuId
        frm1.ShowDialog()
        If blOk Then
            Dim frm As New frm2Photo(strCurrentStuId)
            frm.ShowDialog()
        End If
    End Sub

    Private Sub toolstrbutBook_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutBook.Click
        BookIssuePunch()
    End Sub

    Private Sub toolstrbutClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutClear.Click
        For Each child In Me.MdiChildren
            child.Close()
        Next
    End Sub

    Private Sub toolstrbutRelogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutRelogin.Click
        ReLogin()
    End Sub

    Private Sub toolstrbutClassSeat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutClassSeat.Click
        ClassSeatMap()
    End Sub

    Private Sub toolstrbutClassStu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutClassStu.Click
        SearchStuByClass()
    End Sub

    Private Sub toolstrbutSearchStu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutSearchStu.Click
        SearchStudent()
    End Sub

    Private Sub toolstrbutStuHere_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutStuHere.Click
        StudentHere()
    End Sub

    Private Sub toolstrbutStuAbsent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutStuAbsent.Click
        StuAbsent()
    End Sub

    Private Sub toolstrbutPunchClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutPunchClass.Click
        PunchClass()
    End Sub

    Private Sub toolstrbutPayStaToday_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutPayStaToday.Click
        ShowSubFramPaySta(Now, Now)
    End Sub

    Private Sub mnuReceiptFormat_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuReceiptFormat.Click
        If CheckAuth(56) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        Dim frm As New frm2SetPrint
        frm.ShowDialog()
        RefreshReceiptPrintOpt()
    End Sub

    Private Sub toolstrbutPunchMakeup_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles toolstrbutPunchMakeup.Click
        PunchMakeupClass()
    End Sub

    Private Sub mnuReceiptModifyRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuReceiptModifyRec.Click
        If CheckAuthAdmin() = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmReceiptModRec Is Nothing Then
            If subfrmReceiptModRec.Created = True Then
                FindSubFrom(subfrmReceiptModRec)
            Else
                subfrmReceiptModRec = New frmReceiptModRec()
                AddMyTabPage(subfrmReceiptModRec)
            End If
        Else
            subfrmReceiptModRec = New frmReceiptModRec()
            AddMyTabPage(subfrmReceiptModRec)
        End If
    End Sub

    Private Sub mnuImportStudent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuImportStudent.Click
        Dim frm1 As New frm2ImportStu
        frm1.ShowDialog()
    End Sub

    Private Sub mnuTeleInterviewSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTeleInterviewSearch.Click
        If Not subfrmTeleInterviewSearch Is Nothing Then
            If subfrmTeleInterviewSearch.Created = True Then
                FindSubFrom(subfrmTeleInterviewSearch)
            Else
                subfrmTeleInterviewSearch = New frmTeleInterviewSearch()
                AddMyTabPage(subfrmTeleInterviewSearch)
            End If
        Else
            subfrmTeleInterviewSearch = New frmTeleInterviewSearch()
            AddMyTabPage(subfrmTeleInterviewSearch)
        End If
    End Sub

    Private Sub mnuAddTeleInterview_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAddTeleInterview.Click
        Dim frm As New frm2AddTeleInterview
        frm.ShowDialog()
    End Sub

    Private Sub mnuAddTeleInterviewMul_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAddTeleInterviewMul.Click
        Dim frm As New frm2AddTeleInterviewClass
        frm.ShowDialog()
    End Sub

    Private Sub mnuTeleInterviewType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuTeleInterviewType.Click
        Dim frm As New frm2TeleInterviewType
        frm.ShowDialog()
    End Sub

    Private Sub mnuDiscModRec_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDiscModRec.Click
        If CheckAuth(83) = False Then
            ShowNoAuthMsg()
            Exit Sub
        End If
        If Not subfrmDiscModRec Is Nothing Then
            If subfrmDiscModRec.Created = True Then
                FindSubFrom(subfrmDiscModRec)
            Else
                subfrmDiscModRec = New frmDiscModRec()
                AddMyTabPage(subfrmDiscModRec)
            End If
        Else
            subfrmDiscModRec = New frmDiscModRec()
            AddMyTabPage(subfrmDiscModRec)
        End If
    End Sub

    Private Sub mnuCardReader_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuCardReader.Click
        Dim frm As New frm2SetCardReader
        frm.ShowDialog()
    End Sub

    Private Sub mnuSetAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetAssign.Click
        If Not subfrmSetAssignment Is Nothing Then
            If subfrmSetAssignment.Created = True Then
                FindSubFrom(subfrmSetAssignment)
            Else
                subfrmSetAssignment = New frmSetAssignment()
                AddMyTabPage(subfrmSetAssignment)
            End If
        Else
            subfrmSetAssignment = New frmSetAssignment()
            AddMyTabPage(subfrmSetAssignment)
        End If
    End Sub

    Private Sub mnuSetClassPaper_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSetClassPaper.Click
        If Not subfrmSetClassPaper Is Nothing Then
            If subfrmSetClassPaper.Created = True Then
                FindSubFrom(subfrmSetClassPaper)
            Else
                subfrmSetClassPaper = New frmSetClassPaper()
                AddMyTabPage(subfrmSetClassPaper)
            End If
        Else
            subfrmSetClassPaper = New frmSetClassPaper()
            AddMyTabPage(subfrmSetClassPaper)
        End If
    End Sub

    Private Sub mnuPostGrade_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPostGrade.Click
        If Not subfrmPostGrade Is Nothing Then
            If subfrmPostGrade.Created = True Then
                FindSubFrom(subfrmPostGrade)
            Else
                subfrmPostGrade = New frmPostGrade()
                AddMyTabPage(subfrmPostGrade)
            End If
        Else
            subfrmPostGrade = New frmPostGrade()
            AddMyTabPage(subfrmPostGrade)
        End If
    End Sub

    Private Sub mnuAssignDone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAssignDone.Click
        If Not subfrmAssignDone Is Nothing Then
            If subfrmAssignDone.Created = True Then
                FindSubFrom(subfrmAssignDone)
            Else
                subfrmAssignDone = New frmAssignDone()
                AddMyTabPage(subfrmAssignDone)
            End If
        Else
            subfrmAssignDone = New frmAssignDone()
            AddMyTabPage(subfrmAssignDone)
        End If
    End Sub

    Private Sub mnuAssignDoneMul_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAssignDoneMul.Click
        If Not subfrmAssignDoneMul Is Nothing Then
            If subfrmAssignDoneMul.Created = True Then
                FindSubFrom(subfrmAssignDoneMul)
            Else
                subfrmAssignDoneMul = New frmAssignDoneMul()
                AddMyTabPage(subfrmAssignDoneMul)
            End If
        Else
            subfrmAssignDoneMul = New frmAssignDoneMul()
            AddMyTabPage(subfrmAssignDoneMul)
        End If
    End Sub

    Private Sub mnuStuDoneAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuDoneAssign.Click
        If Not subfrmStuDoneAssign Is Nothing Then
            If subfrmStuDoneAssign.Created = True Then
                FindSubFrom(subfrmStuDoneAssign)
            Else
                subfrmStuDoneAssign = New frmStuDoneAssign()
                AddMyTabPage(subfrmStuDoneAssign)
            End If
        Else
            subfrmStuDoneAssign = New frmStuDoneAssign()
            AddMyTabPage(subfrmStuDoneAssign)
        End If
    End Sub

    Private Sub mnuStuNotDoneAssign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuNotDoneAssign.Click
        If Not subfrmStuNoAssign Is Nothing Then
            If subfrmStuNoAssign.Created = True Then
                FindSubFrom(subfrmStuNoAssign)
            Else
                subfrmStuNoAssign = New frmStuNoAssign()
                AddMyTabPage(subfrmStuNoAssign)
            End If
        Else
            subfrmStuNoAssign = New frmStuNoAssign()
            AddMyTabPage(subfrmStuNoAssign)
        End If
    End Sub

    Private Sub mnuStuGrade_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuGrade.Click
        If Not subfrmStuGrade Is Nothing Then
            If subfrmStuGrade.Created = True Then
                FindSubFrom(subfrmStuGrade)
            Else
                subfrmStuGrade = New frmStuGrade()
                AddMyTabPage(subfrmStuGrade)
            End If
        Else
            subfrmStuGrade = New frmStuGrade()
            AddMyTabPage(subfrmStuGrade)
        End If
    End Sub

    Private Sub mnuStuGradeMul_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuStuGradeMul.Click
        If Not subfrmStuGradeMul Is Nothing Then
            If subfrmStuGradeMul.Created = True Then
                FindSubFrom(subfrmStuGradeMul)
            Else
                subfrmStuGradeMul = New frmStuGradeMul()
                AddMyTabPage(subfrmStuGradeMul)
            End If
        Else
            subfrmStuGradeMul = New frmStuGradeMul()
            AddMyTabPage(subfrmStuGradeMul)
        End If
    End Sub

    Private Sub mnuItemVision_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuItemVision.Click
        Dim path As String = "UpdateList.log"
        Dim row As String = My.Computer.FileSystem.ReadAllText(My.Application.Info.DirectoryPath + "\" + path, Encoding.GetEncoding("BIG5"))
        Dim frm As New frm2Vision(row)
        frm.ShowDialog()
    End Sub

    Private Sub timerClock_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerClock.Tick
        ToolStripLblTime.Text = String.Format("{0}{1}{2}", "現在時間：", vbTab, Now.ToString("yyyy/MM/dd") & "(" & Now.DayOfWeek.ToString & ")" & Now.ToString("HH:mm:ss"))
    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        LoadConfigData()
        InitPrinter()
    End Sub

    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork
        RefreshClassRegInfo()
    End Sub

    Private Sub BackgroundWorker3_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker3.DoWork
        RefreshClassContentTb()
    End Sub
    Private Sub BackgroundWorker4_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker4.DoWork
        RefreshClassSessionTb()
    End Sub
    Private Sub BackgroundWorker5_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker5.DoWork
        RefreshBookTb()
    End Sub
    Private Sub BackgroundWorker6_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker6.DoWork
        RefreshSchool()
    End Sub

    Private Sub BackgroundWorker7_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker7.DoWork
        RefreshClassPunchStuInfo()
    End Sub

    Private Sub BackgroundWorker8_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker8.DoWork
        RefreshClassPunchBookContent()
    End Sub

   
    
    
End Class
