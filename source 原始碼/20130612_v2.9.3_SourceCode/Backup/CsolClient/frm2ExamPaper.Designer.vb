﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ExamPaper
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxAbbr = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.lblClass = New System.Windows.Forms.Label
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboxContent = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.radbutTick = New System.Windows.Forms.RadioButton
        Me.radbutAll = New System.Windows.Forms.RadioButton
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.GroupBox5.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(17, 88)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(65, 12)
        Me.Label6.TabIndex = 62
        Me.Label6.Text = "試卷縮寫："
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tboxAbbr
        '
        Me.tboxAbbr.Location = New System.Drawing.Point(88, 83)
        Me.tboxAbbr.Name = "tboxAbbr"
        Me.tboxAbbr.Size = New System.Drawing.Size(76, 22)
        Me.tboxAbbr.TabIndex = 63
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(17, 116)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(65, 12)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "試卷日期："
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(17, 59)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(65, 12)
        Me.Label7.TabIndex = 64
        Me.Label7.Text = "試卷名稱："
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'butConfirm
        '
        Me.butConfirm.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butConfirm.Location = New System.Drawing.Point(331, 14)
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.Size = New System.Drawing.Size(82, 25)
        Me.butConfirm.TabIndex = 56
        Me.butConfirm.Text = "確定"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(331, 53)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 57
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(88, 110)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(206, 22)
        Me.dtpickDate.TabIndex = 60
        '
        'tboxName
        '
        Me.tboxName.Location = New System.Drawing.Point(88, 56)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.Size = New System.Drawing.Size(206, 22)
        Me.tboxName.TabIndex = 59
        '
        'lblClass
        '
        Me.lblClass.AutoSize = True
        Me.lblClass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblClass.Location = New System.Drawing.Point(17, 14)
        Me.lblClass.Name = "lblClass"
        Me.lblClass.Size = New System.Drawing.Size(65, 12)
        Me.lblClass.TabIndex = 58
        Me.lblClass.Text = "試卷班級："
        Me.lblClass.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(88, 35)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 67
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(88, 10)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(206, 20)
        Me.cboxClass.TabIndex = 68
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(166, 86)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(197, 12)
        Me.Label2.TabIndex = 69
        Me.Label2.Text = "（結合寄發成績單使用。可不輸入）"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'cboxContent
        '
        Me.cboxContent.FormattingEnabled = True
        Me.cboxContent.Location = New System.Drawing.Point(88, 137)
        Me.cboxContent.Name = "cboxContent"
        Me.cboxContent.Size = New System.Drawing.Size(206, 20)
        Me.cboxContent.TabIndex = 70
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(17, 142)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 71
        Me.Label4.Text = "上課內容："
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'radbutTick
        '
        Me.radbutTick.AutoSize = True
        Me.radbutTick.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutTick.Location = New System.Drawing.Point(23, 48)
        Me.radbutTick.Name = "radbutTick"
        Me.radbutTick.Size = New System.Drawing.Size(167, 16)
        Me.radbutTick.TabIndex = 42
        Me.radbutTick.Text = "只設定給下列有勾選的班別"
        Me.radbutTick.UseVisualStyleBackColor = True
        '
        'radbutAll
        '
        Me.radbutAll.AutoSize = True
        Me.radbutAll.Checked = True
        Me.radbutAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutAll.Location = New System.Drawing.Point(23, 21)
        Me.radbutAll.Name = "radbutAll"
        Me.radbutAll.Size = New System.Drawing.Size(119, 16)
        Me.radbutAll.TabIndex = 43
        Me.radbutAll.TabStop = True
        Me.radbutAll.Text = "設定給全部的班別"
        Me.radbutAll.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.chklstSubClass)
        Me.GroupBox5.Controls.Add(Me.radbutAll)
        Me.GroupBox5.Controls.Add(Me.radbutTick)
        Me.GroupBox5.Location = New System.Drawing.Point(8, 168)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(410, 232)
        Me.GroupBox5.TabIndex = 55
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "適用對象"
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        Me.chklstSubClass.Location = New System.Drawing.Point(23, 74)
        Me.chklstSubClass.Name = "chklstSubClass"
        Me.chklstSubClass.Size = New System.Drawing.Size(269, 140)
        Me.chklstSubClass.TabIndex = 44
        '
        'frm2ExamPaper
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(441, 419)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboxContent)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxAbbr)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.lblClass)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2ExamPaper"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "新增試卷"
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxAbbr As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents lblClass As System.Windows.Forms.Label
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboxContent As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents radbutTick As System.Windows.Forms.RadioButton
    Friend WithEvents radbutAll As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
End Class
