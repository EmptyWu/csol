﻿Public Class frmAssignDoneMul
    Private dtAssignment As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstAssignment As New ArrayList

    Private dtData As New DataTable
    Private dtData2 As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private lstClassFee As New ArrayList
    Private intColSchType As Integer = 1

    Private lstAssignmentShown As New ArrayList

    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmAssignDoneMul
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()
        InitColSelections()
        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmAssignDoneMul
            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)

            InitList()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        Try

            ExportDgvToExcel(dgv)

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub InitList()
        Try
            lstboxClass.Items.Clear()
            lstClass.Clear()

            If chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                        lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            End If

            If lstClass.Count > 0 Then
                lstboxClass.SelectedIndex = 0
            End If

            RefreshScAndAssignList()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub lstboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstboxClass.SelectedIndexChanged
        RefreshScAndAssignList()
    End Sub

    Private Sub RefreshScAndAssignList()
        Try
            chklstSubClass.Items.Clear()
            lstSubClassShown.Clear()
            lstSubClass.Clear()
            If lstClass.Count = 0 Then
                Exit Sub
            End If
            intClass = lstClass(lstboxClass.SelectedIndex)
            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClassShown.Add(row.Item(c_IDColumnName))
                        chklstSubClass.Items.Add(row.Item(c_NameColumnName), False)
                    End If
                Next

                lstAssignment.Clear()
                lstAssignmentShown.Clear()
                chklstAssignment.Items.Clear()

                dtAssignment = objCsol.GetClassAssignmentList(intClass)
                If dtAssignment.Rows.Count = 0 Then
                    dgv.DataSource = Nothing
                End If
            Else
                dgv.DataSource = Nothing
            End If

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        Try
            Dim intC As Integer
            Dim intSc As Integer
            intC = lstSubClassShown(e.Index)
            lstAssignment.Clear()
            lstAssignmentShown.Clear()
            chklstAssignment.Items.Clear()
            dgv.DataSource = Nothing

            If e.CurrentValue = 0 And e.NewValue = 1 Then
                lstSubClass.Add(intC)
            Else
                lstSubClass.Remove(intC)
            End If
            If chkboxShowPastAssign.Checked Then
                For j As Integer = 0 To lstSubClass.Count - 1
                    intC = lstSubClass(j)

                    For index As Integer = 0 To dtAssignment.Rows.Count - 1
                        If dtAssignment.Rows(index).Item(c_SubClassIDColumnName) = intC Then
                            intSc = dtAssignment.Rows(index).Item(c_IDColumnName)
                            If lstAssignmentShown.Contains(intSc) = False Then
                                lstAssignmentShown.Add(intSc)
                                chklstAssignment.Items.Add(dtAssignment.Rows(index).Item(c_NameColumnName), False)
                            End If
                        End If
                    Next
                Next
            Else
                For j As Integer = 0 To lstSubClass.Count - 1
                    intC = lstSubClass(j)

                    For index As Integer = 0 To dtAssignment.Rows.Count - 1
                        If dtAssignment.Rows(index).Item(c_SubClassIDColumnName) = intC And _
                        GetDateEnd(dtAssignment.Rows(index).Item(c_EndDateColumnName)) >= Now Then
                            intSc = dtAssignment.Rows(index).Item(c_IDColumnName)
                            If lstAssignmentShown.Contains(intSc) = False Then
                                lstAssignmentShown.Add(intSc)
                                chklstAssignment.Items.Add(dtAssignment.Rows(index).Item(c_NameColumnName), False)
                            End If
                        End If
                    Next

                Next
            End If

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshAssignmentList()
        Try
            chklstAssignment.Items.Clear()
            lstAssignment.Clear()
            lstAssignmentShown.Clear()
            dgv.DataSource = Nothing
            Dim intC As Integer
            Dim intSc As Integer
            If lstSubClass.Count > 0 Then
                For i As Integer = 0 To lstSubClass.Count - 1
                    intC = lstSubClass(i)
                    If chkboxShowPastAssign.Checked Then
                        For index As Integer = 0 To dtAssignment.Rows.Count - 1
                            If dtAssignment.Rows(index).Item(c_SubClassIDColumnName) = intC Then
                                intSc = dtAssignment.Rows(index).Item(c_IDColumnName)
                                If lstAssignmentShown.Contains(intSc) = False Then
                                    lstAssignmentShown.Add(intSc)
                                    chklstAssignment.Items.Add(dtAssignment.Rows(index).Item(c_NameColumnName), False)
                                End If
                            End If
                        Next
                    Else
                        For index As Integer = 0 To dtAssignment.Rows.Count - 1
                            If dtAssignment.Rows(index).Item(c_SubClassIDColumnName) = intC And _
                            GetDateEnd(dtAssignment.Rows(index).Item(c_EndDateColumnName)) >= Now Then
                                intSc = dtAssignment.Rows(index).Item(c_IDColumnName)
                                If lstAssignmentShown.Contains(intSc) = False Then
                                    lstAssignmentShown.Add(intSc)
                                    chklstAssignment.Items.Add(dtAssignment.Rows(index).Item(c_NameColumnName), False)
                                End If
                            End If
                        Next
                    End If
                Next
            End If

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub



    Private Sub RefreshAssignmentData()
        Try

            If lstAssignment.Count = 0 Or lstSubClass.Count = 0 Then
                'ShowInfo(My.Resources.msgNoSelection)
                dgv.DataSource = Nothing
                Exit Sub
            End If

            'dtData2 = objCsol.GetStuAssignmentsMul(lstAssignment, lstSubClass, intClass)
            dtData2 = GetStuAssignmentsMul(lstAssignment, lstSubClass, intClass)

            MakeTable()

            'dgv.DataSource = dtData.DefaultView
            dgv.DataSource = dtData
            LoadColumnText()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    '----------------------------------------------------------------------

    Function GetStuAssignments(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable
        'Dim obj As New Assignment
        Dim dt As New DataTable
        If sc.Count > 1 Then
            'dt = obj.GetStuAssignments(assignmentId, sc(0))
            dt = GetStuAssignments_sql(assignmentId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                'dt.Merge(obj.GetStuAssignments(assignmentId, sc(index)))
                dt.Merge(GetStuAssignments_sql(assignmentId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            'dt = obj.GetStuAssignments(assignmentId, sc(0))
            dt = GetStuAssignments_sql(assignmentId, sc(0))
        Else
            'dt = obj.GetStuAssignments(assignmentId, -1)
            dt = GetStuAssignments_sql(assignmentId, -1)
        End If

        Return dt
    End Function

    Function GetStuAssignmentsMul(ByVal assignmentId As ArrayList, ByVal sc As ArrayList, ByVal intClass As Integer) As DataTable
        '    Dim obj As New Assignment
        Dim dt As New DataTable
        Dim dtAssignment As DataTable = GetClassAssignmentList(intClass)
        Dim lst As New ArrayList
        Dim s As Integer = 0
        Dim Assignment As Integer = 0

        If assignmentId.Count > 1 Then
            Assignment = assignmentId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuAssignments(Assignment, lst)
            For index As Integer = 1 To assignmentId.Count - 1
                Assignment = assignmentId(index)
                lst = New ArrayList
                For i As Integer = 0 To sc.Count - 1
                    s = sc(i)
                    If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                               c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                        lst.Add(s)
                    End If
                Next
                dt.Merge(GetStuAssignments(Assignment, lst))
            Next
        ElseIf assignmentId.Count = 1 Then
            Assignment = assignmentId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuAssignments(Assignment, lst)
        Else
            dt = GetStuAssignments(-1, lst)
        End If

        Return dt

    End Function

    Private Shared Function GetClassAssignmentList(ByVal intClass As Integer) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("classid", intClass)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("AssignDone", "GetClassAssignmentList", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetClassAssignmentList").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetClassAssignmentList"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetStuAssignments_sql(ByVal assignmentId As Integer, ByVal sc As Integer) As DataTable

        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("assignmentId", assignmentId)
        RequestParams.Add("sc", sc)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("AssignDone", "GetStuAssignByAssignId", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetStuAssignByAssignId").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetStuAssignByAssignId"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Sub InitColSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 3 Then
            Dim lst As New ArrayList
            For index As Integer = 4 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub
    '--------------------------------------------------------

    Private Sub MakeTable()
        Try
            If dtData2.Rows.Count > 0 Then
                dtData = dtData2.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_SeatNumColumnName, c_SubClassNameColumnName, _
                                                    c_SubClassIDColumnName, c_EngNameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, _
                                                    c_EmailColumnName, c_SchoolColumnName, c_GraduateFromColumnName, c_DadNameColumnName, _
                                                    c_MumNameColumnName, c_DadMobileColumnName, c_MumMobileColumnName, c_IntroIDColumnName, _
                                                    c_IntroNameColumnName, c_SchoolGradeColumnName, c_SchoolClassColumnName, c_PrimarySchColumnName, _
                                                    c_JuniorSchColumnName, c_HighSchColumnName, c_UniversityColumnName)


                For i As Integer = 0 To lstAssignment.Count - 1
                    dtData.Columns.Add("Assignment" & i.ToString, GetType(System.String))
                Next
                Dim mark As Integer
                For Each row As DataRow In dtData.Rows
                    For i As Integer = 0 To lstAssignment.Count - 1S
                        mark = GetMark(row.Item(c_IDColumnName), lstAssignment(i))
                        If mark = c_Pass Then
                            row.Item("Assignment" & i.ToString) = My.Resources.pass
                        Else
                            row.Item("Assignment" & i.ToString) = ""
                        End If
                    Next
                Next
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Function GetMark(ByVal stuid As String, ByVal Assignment As Integer) As Integer
        Try
            Dim c As Array = dtData2.Select(c_IDColumnName & "=" & stuid & " AND " & _
                                                    c_AssignmentIDColumnName & "=" & Assignment.ToString)
            Dim result As Integer = c_Fail
            If c.Length > 0 Then
                For Each row As DataRow In c
                    If GetIntValue(row.Item(c_MarkColumnName)) > 0 Then
                        result = c_Pass
                        Exit For
                    End If
                Next
            End If
            Return result
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Function


    Private Sub LoadColumnText()
        Try
            For Each col In dgv.Columns
                col.visible = False
            Next
            If dgv.Rows.Count > 0 Then
                dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_IDColumnName).Visible = True
                dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID

                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name

                dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_SeatNumColumnName).Visible = True
                dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum

                'dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
                'dgv.Columns.Item(c_SchoolColumnName).Visible = True
                'dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school

                If lstSubClass.Count > 1 Then
                    dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
                    dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                    dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
                End If

                Dim idx As Integer = 4
                For j As Integer = 0 To lstAssignment.Count - 1
                    dgv.Columns.Item("Assignment" & j.ToString).DisplayIndex = idx
                    dgv.Columns.Item("Assignment" & j.ToString).Visible = True
                    dgv.Columns.Item("Assignment" & j.ToString).HeaderText = GetAssignmentName(lstAssignment(j))
                    dgv.Columns.Item("Assignment" & j.ToString).ReadOnly = False
                    idx = idx + 1
                Next

                Dim i As Integer = 3

                For index As Integer = 0 To lstColName.Count - 1
                    If lstColShow(index) = 1 Then
                        dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                        dgv.Columns.Item(lstColName(index)).Visible = True
                        dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                        i = i + 1
                    End If
                Next


                If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                    Dim id As Integer
                    Select Case intColSchType
                        Case 0
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = GetIntValue(dgv.Rows(index).Cells(c_PrimarySchColumnName).Value)
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                        Case 1
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = GetIntValue(dgv.Rows(index).Cells(c_JuniorSchColumnName).Value)
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                        Case 2
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = GetIntValue(dgv.Rows(index).Cells(c_HighSchColumnName).Value)
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                        Case 3
                            For index As Integer = 0 To dgv.Rows.Count - 1
                                id = GetIntValue(dgv.Rows(index).Cells(c_UniversityColumnName).Value)
                                dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                            Next
                    End Select
                End If


            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Function GetAssignmentName(ByVal i As Integer) As String
        Dim idx As Integer = lstAssignmentShown.IndexOf(i)
        If idx > -1 Then
            Return chklstAssignment.Items(idx)
        Else
            Return ""
        End If
    End Function

    Private Function GetAssignmentEndDate(ByVal i As Integer) As Boolean
        Dim lst As Array
        Dim dt As Date
        Try
            lst = dtAssignment.Select(c_IDColumnName & "=" & i.ToString)
            If lst.Length > 0 Then
                dt = lst(0).Item(c_EndDateColumnName)
                If GetDateEnd(dt) >= Now Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return True
            End If
        Catch ex As Exception
            Return True
        End Try
    End Function

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmAssignDoneMul_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmAssignDoneMul_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmAssignDoneMul_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim intC As Integer
        Dim intR As Integer
        Dim stuid As String = ""
        Dim Assignment As Integer
        Dim mark As Integer
        Dim remarks As String = ""
        Dim idx As Integer = 0
        Dim sc As Integer = 0

        Try
            intC = e.ColumnIndex
            intR = e.RowIndex
            If intC > 22 Then
                idx = intC - 23
                stuid = dgv.Rows(intR).Cells(c_IDColumnName).Value
                Assignment = lstAssignment(idx)
                sc = dgv.Rows(intR).Cells(c_SubClassIDColumnName).Value
                remarks = dgv.Rows(intR).Cells("Assignment" & idx.ToString).Value.ToString.Trim

                If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                       c_IDColumnName & "=" & Assignment.ToString) = 0 Then
                    ShowInfo(My.Resources.msgScNoAssignment)
                    dgv.Rows(intR).Cells("Assignment" & idx.ToString).Value = ""
                    Exit Sub
                End If

                If remarks.Length > 0 Then
                    dgv.Rows(intR).Cells("Assignment" & idx.ToString).Value = ""
                    mark = c_Fail
                Else
                    dgv.Rows(intR).Cells("Assignment" & idx.ToString).Value = My.Resources.pass
                    mark = c_Pass
                End If
            End If

        Catch ex As Exception
            ShowInfo(My.Resources.msgCheckDataFormat)
            AddErrorLog(ex.ToString)
            Exit Sub
        End Try

        Try
            If stuid.Length = 8 Then
                objCsol.UpdateStuAssignment(stuid, Assignment, Now, mark)

            End If

        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try

    End Sub

    Private Sub chklstAssignment_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstAssignment.ItemCheck
        Dim intC As Integer
        intC = lstAssignmentShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstAssignment.Add(intC)
        Else
            lstAssignment.Remove(intC)
        End If

        RefreshAssignmentData()
    End Sub

    Private Sub chkboxShowPastAssign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPastAssign.CheckedChanged
        RefreshAssignmentList()
    End Sub

    
End Class