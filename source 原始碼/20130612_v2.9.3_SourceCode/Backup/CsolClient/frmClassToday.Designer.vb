﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClassToday
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClassToday))
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.dgvSta = New System.Windows.Forms.DataGridView
        Me.butListSta = New System.Windows.Forms.Button
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.butClose = New System.Windows.Forms.Button
        CType(Me.dgvSta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'dgvSta
        '
        Me.dgvSta.AllowUserToAddRows = False
        Me.dgvSta.AllowUserToDeleteRows = False
        Me.dgvSta.AllowUserToResizeRows = False
        Me.dgvSta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvSta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvSta, "dgvSta")
        Me.dgvSta.MultiSelect = False
        Me.dgvSta.Name = "dgvSta"
        Me.dgvSta.RowHeadersVisible = False
        Me.dgvSta.RowTemplate.Height = 15
        Me.dgvSta.RowTemplate.ReadOnly = True
        Me.dgvSta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSta.ShowCellToolTips = False
        Me.dgvSta.ShowEditingIcon = False
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'butClose
        '
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'frmClassToday
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.dgvSta)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Name = "frmClassToday"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvSta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents dgvSta As System.Windows.Forms.DataGridView
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents butClose As System.Windows.Forms.Button
End Class
