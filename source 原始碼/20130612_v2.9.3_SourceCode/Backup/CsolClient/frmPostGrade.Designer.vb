﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPostGrade
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuStuInfo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.chkboxShowRemarks = New System.Windows.Forms.CheckBox
        Me.chkboxComputeTotal = New System.Windows.Forms.CheckBox
        Me.chkboxShowCancel = New System.Windows.Forms.CheckBox
        Me.chkboxShowHasGrade = New System.Windows.Forms.CheckBox
        Me.chkboxShowO = New System.Windows.Forms.CheckBox
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.chkboxShowAttend = New System.Windows.Forms.CheckBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.cboxFontSize = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboxFont = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chkboxNoAbsent = New System.Windows.Forms.CheckBox
        Me.chkboxNoDigits = New System.Windows.Forms.CheckBox
        Me.chkboxAutoSize = New System.Windows.Forms.CheckBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.linklblNoSc = New System.Windows.Forms.LinkLabel
        Me.linklblAllSc = New System.Windows.Forms.LinkLabel
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.chklstClass = New System.Windows.Forms.CheckedListBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.linklblNoPaper = New System.Windows.Forms.LinkLabel
        Me.linklblAllPaper = New System.Windows.Forms.LinkLabel
        Me.chklstPaper = New System.Windows.Forms.CheckedListBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.tboxHigh = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxTop = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxMid = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxLow = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxBottom = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.MenuStrip1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuStuInfo, Me.mnuSelectCol, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1028, 24)
        Me.MenuStrip1.TabIndex = 60
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuStuInfo
        '
        Me.mnuStuInfo.Name = "mnuStuInfo"
        Me.mnuStuInfo.Size = New System.Drawing.Size(68, 20)
        Me.mnuStuInfo.Text = "學生資料"
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        Me.mnuSelectCol.Size = New System.Drawing.Size(68, 20)
        Me.mnuSelectCol.Text = "選擇欄位"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(68, 20)
        Me.mnuPrint.Text = "列印成績"
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出成績"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'PrintDocument1
        '
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.chkboxShowRemarks)
        Me.GroupBox3.Controls.Add(Me.chkboxComputeTotal)
        Me.GroupBox3.Controls.Add(Me.chkboxShowCancel)
        Me.GroupBox3.Controls.Add(Me.chkboxShowHasGrade)
        Me.GroupBox3.Controls.Add(Me.chkboxShowO)
        Me.GroupBox3.Controls.Add(Me.dtpickDate)
        Me.GroupBox3.Controls.Add(Me.chkboxShowAttend)
        Me.GroupBox3.Location = New System.Drawing.Point(8, 24)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(1002, 36)
        Me.GroupBox3.TabIndex = 50
        Me.GroupBox3.TabStop = False
        '
        'chkboxShowRemarks
        '
        Me.chkboxShowRemarks.AutoSize = True
        Me.chkboxShowRemarks.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowRemarks.Location = New System.Drawing.Point(725, 12)
        Me.chkboxShowRemarks.Name = "chkboxShowRemarks"
        Me.chkboxShowRemarks.Size = New System.Drawing.Size(156, 16)
        Me.chkboxShowRemarks.TabIndex = 63
        Me.chkboxShowRemarks.Text = "如果沒有成績則顯示備註"
        Me.chkboxShowRemarks.UseVisualStyleBackColor = True
        '
        'chkboxComputeTotal
        '
        Me.chkboxComputeTotal.AutoSize = True
        Me.chkboxComputeTotal.Checked = True
        Me.chkboxComputeTotal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxComputeTotal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxComputeTotal.Location = New System.Drawing.Point(646, 12)
        Me.chkboxComputeTotal.Name = "chkboxComputeTotal"
        Me.chkboxComputeTotal.Size = New System.Drawing.Size(72, 16)
        Me.chkboxComputeTotal.TabIndex = 62
        Me.chkboxComputeTotal.Text = "計算總分"
        Me.chkboxComputeTotal.UseVisualStyleBackColor = True
        '
        'chkboxShowCancel
        '
        Me.chkboxShowCancel.AutoSize = True
        Me.chkboxShowCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowCancel.Location = New System.Drawing.Point(555, 12)
        Me.chkboxShowCancel.Name = "chkboxShowCancel"
        Me.chkboxShowCancel.Size = New System.Drawing.Size(84, 16)
        Me.chkboxShowCancel.TabIndex = 61
        Me.chkboxShowCancel.Text = "顯示不上了"
        Me.chkboxShowCancel.UseVisualStyleBackColor = True
        '
        'chkboxShowHasGrade
        '
        Me.chkboxShowHasGrade.AutoSize = True
        Me.chkboxShowHasGrade.Checked = True
        Me.chkboxShowHasGrade.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxShowHasGrade.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowHasGrade.Location = New System.Drawing.Point(440, 12)
        Me.chkboxShowHasGrade.Name = "chkboxShowHasGrade"
        Me.chkboxShowHasGrade.Size = New System.Drawing.Size(108, 16)
        Me.chkboxShowHasGrade.TabIndex = 60
        Me.chkboxShowHasGrade.Text = "只顯示有成績者"
        Me.chkboxShowHasGrade.UseVisualStyleBackColor = True
        '
        'chkboxShowO
        '
        Me.chkboxShowO.AutoSize = True
        Me.chkboxShowO.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowO.Location = New System.Drawing.Point(281, 12)
        Me.chkboxShowO.Name = "chkboxShowO"
        Me.chkboxShowO.Size = New System.Drawing.Size(152, 16)
        Me.chkboxShowO.TabIndex = 59
        Me.chkboxShowO.Text = "中文姓名第二個字改成O"
        Me.chkboxShowO.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(148, 9)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(126, 22)
        Me.dtpickDate.TabIndex = 58
        '
        'chkboxShowAttend
        '
        Me.chkboxShowAttend.AutoSize = True
        Me.chkboxShowAttend.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowAttend.Location = New System.Drawing.Point(9, 12)
        Me.chkboxShowAttend.Name = "chkboxShowAttend"
        Me.chkboxShowAttend.Size = New System.Drawing.Size(132, 16)
        Me.chkboxShowAttend.TabIndex = 57
        Me.chkboxShowAttend.Text = "只顯示當日有出席者"
        Me.chkboxShowAttend.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.cboxFontSize)
        Me.GroupBox4.Controls.Add(Me.Label2)
        Me.GroupBox4.Controls.Add(Me.cboxFont)
        Me.GroupBox4.Controls.Add(Me.Label1)
        Me.GroupBox4.Controls.Add(Me.chkboxNoAbsent)
        Me.GroupBox4.Controls.Add(Me.chkboxNoDigits)
        Me.GroupBox4.Controls.Add(Me.chkboxAutoSize)
        Me.GroupBox4.Location = New System.Drawing.Point(9, 59)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(1002, 36)
        Me.GroupBox4.TabIndex = 61
        Me.GroupBox4.TabStop = False
        '
        'cboxFontSize
        '
        Me.cboxFontSize.FormattingEnabled = True
        Me.cboxFontSize.Items.AddRange(New Object() {"9", "10", "11", "12", "14", "16", "18", "20"})
        Me.cboxFontSize.Location = New System.Drawing.Point(260, 12)
        Me.cboxFontSize.Name = "cboxFontSize"
        Me.cboxFontSize.Size = New System.Drawing.Size(117, 20)
        Me.cboxFontSize.TabIndex = 67
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(198, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 66
        Me.Label2.Text = "列印大小"
        '
        'cboxFont
        '
        Me.cboxFont.FormattingEnabled = True
        Me.cboxFont.Location = New System.Drawing.Point(72, 12)
        Me.cboxFont.Name = "cboxFont"
        Me.cboxFont.Size = New System.Drawing.Size(117, 20)
        Me.cboxFont.TabIndex = 65
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(10, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 64
        Me.Label1.Text = "列印字型"
        '
        'chkboxNoAbsent
        '
        Me.chkboxNoAbsent.AutoSize = True
        Me.chkboxNoAbsent.Checked = True
        Me.chkboxNoAbsent.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxNoAbsent.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxNoAbsent.Location = New System.Drawing.Point(752, 14)
        Me.chkboxNoAbsent.Name = "chkboxNoAbsent"
        Me.chkboxNoAbsent.Size = New System.Drawing.Size(132, 16)
        Me.chkboxNoAbsent.TabIndex = 63
        Me.chkboxNoAbsent.Text = "缺考不列入個人平均"
        Me.chkboxNoAbsent.UseVisualStyleBackColor = True
        '
        'chkboxNoDigits
        '
        Me.chkboxNoDigits.AutoSize = True
        Me.chkboxNoDigits.Checked = True
        Me.chkboxNoDigits.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxNoDigits.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxNoDigits.Location = New System.Drawing.Point(575, 14)
        Me.chkboxNoDigits.Name = "chkboxNoDigits"
        Me.chkboxNoDigits.Size = New System.Drawing.Size(168, 16)
        Me.chkboxNoDigits.TabIndex = 62
        Me.chkboxNoDigits.Text = "分數是整數時不顯示小數點"
        Me.chkboxNoDigits.UseVisualStyleBackColor = True
        '
        'chkboxAutoSize
        '
        Me.chkboxAutoSize.AutoSize = True
        Me.chkboxAutoSize.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxAutoSize.Location = New System.Drawing.Point(386, 14)
        Me.chkboxAutoSize.Name = "chkboxAutoSize"
        Me.chkboxAutoSize.Size = New System.Drawing.Size(180, 16)
        Me.chkboxAutoSize.TabIndex = 60
        Me.chkboxAutoSize.Text = "列印時自動調整寬度到整張紙"
        Me.chkboxAutoSize.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.linklblNoSc)
        Me.GroupBox1.Controls.Add(Me.linklblAllSc)
        Me.GroupBox1.Controls.Add(Me.chklstSubClass)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 343)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(199, 225)
        Me.GroupBox1.TabIndex = 63
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "班別"
        '
        'linklblNoSc
        '
        Me.linklblNoSc.AutoSize = True
        Me.linklblNoSc.Location = New System.Drawing.Point(145, 2)
        Me.linklblNoSc.Name = "linklblNoSc"
        Me.linklblNoSc.Size = New System.Drawing.Size(41, 12)
        Me.linklblNoSc.TabIndex = 21
        Me.linklblNoSc.TabStop = True
        Me.linklblNoSc.Text = "全不選"
        '
        'linklblAllSc
        '
        Me.linklblAllSc.AutoSize = True
        Me.linklblAllSc.Location = New System.Drawing.Point(112, 2)
        Me.linklblAllSc.Name = "linklblAllSc"
        Me.linklblAllSc.Size = New System.Drawing.Size(29, 12)
        Me.linklblAllSc.TabIndex = 20
        Me.linklblAllSc.TabStop = True
        Me.linklblAllSc.Text = "全選"
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        Me.chklstSubClass.HorizontalScrollbar = True
        Me.chklstSubClass.Location = New System.Drawing.Point(11, 17)
        Me.chklstSubClass.Name = "chklstSubClass"
        Me.chklstSubClass.Size = New System.Drawing.Size(178, 191)
        Me.chklstSubClass.TabIndex = 17
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.chklstClass)
        Me.GroupBox7.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox7.Location = New System.Drawing.Point(8, 101)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(200, 234)
        Me.GroupBox7.TabIndex = 62
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "班級"
        '
        'chklstClass
        '
        Me.chklstClass.CheckOnClick = True
        Me.chklstClass.FormattingEnabled = True
        Me.chklstClass.HorizontalScrollbar = True
        Me.chklstClass.Location = New System.Drawing.Point(11, 17)
        Me.chklstClass.Name = "chklstClass"
        Me.chklstClass.Size = New System.Drawing.Size(179, 208)
        Me.chklstClass.TabIndex = 17
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(125, 0)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPast.TabIndex = 16
        Me.chkboxShowPast.Text = "顯示過時"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.linklblNoPaper)
        Me.GroupBox5.Controls.Add(Me.linklblAllPaper)
        Me.GroupBox5.Controls.Add(Me.chklstPaper)
        Me.GroupBox5.Location = New System.Drawing.Point(217, 98)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(229, 470)
        Me.GroupBox5.TabIndex = 74
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "試卷"
        '
        'linklblNoPaper
        '
        Me.linklblNoPaper.AutoSize = True
        Me.linklblNoPaper.Location = New System.Drawing.Point(174, 1)
        Me.linklblNoPaper.Name = "linklblNoPaper"
        Me.linklblNoPaper.Size = New System.Drawing.Size(41, 12)
        Me.linklblNoPaper.TabIndex = 23
        Me.linklblNoPaper.TabStop = True
        Me.linklblNoPaper.Text = "全不選"
        '
        'linklblAllPaper
        '
        Me.linklblAllPaper.AutoSize = True
        Me.linklblAllPaper.Location = New System.Drawing.Point(141, 1)
        Me.linklblAllPaper.Name = "linklblAllPaper"
        Me.linklblAllPaper.Size = New System.Drawing.Size(29, 12)
        Me.linklblAllPaper.TabIndex = 22
        Me.linklblAllPaper.TabStop = True
        Me.linklblAllPaper.Text = "全選"
        '
        'chklstPaper
        '
        Me.chklstPaper.CheckOnClick = True
        Me.chklstPaper.FormattingEnabled = True
        Me.chklstPaper.HorizontalScrollbar = True
        Me.chklstPaper.Location = New System.Drawing.Point(11, 17)
        Me.chklstPaper.Name = "chklstPaper"
        Me.chklstPaper.Size = New System.Drawing.Size(208, 446)
        Me.chklstPaper.TabIndex = 17
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(452, 106)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(558, 462)
        Me.dgv.TabIndex = 75
        '
        'tboxHigh
        '
        Me.tboxHigh.Location = New System.Drawing.Point(204, 574)
        Me.tboxHigh.Name = "tboxHigh"
        Me.tboxHigh.Size = New System.Drawing.Size(56, 22)
        Me.tboxHigh.TabIndex = 79
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(131, 577)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(67, 12)
        Me.Label4.TabIndex = 78
        Me.Label4.Text = "前標(高標): "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxTop
        '
        Me.tboxTop.Location = New System.Drawing.Point(55, 574)
        Me.tboxTop.Name = "tboxTop"
        Me.tboxTop.Size = New System.Drawing.Size(60, 22)
        Me.tboxTop.TabIndex = 77
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(14, 577)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 76
        Me.Label3.Text = "頂標: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxMid
        '
        Me.tboxMid.Location = New System.Drawing.Point(312, 574)
        Me.tboxMid.Name = "tboxMid"
        Me.tboxMid.Size = New System.Drawing.Size(60, 22)
        Me.tboxMid.TabIndex = 81
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(271, 577)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 12)
        Me.Label5.TabIndex = 80
        Me.Label5.Text = "均標: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxLow
        '
        Me.tboxLow.Location = New System.Drawing.Point(462, 574)
        Me.tboxLow.Name = "tboxLow"
        Me.tboxLow.Size = New System.Drawing.Size(56, 22)
        Me.tboxLow.TabIndex = 83
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(389, 577)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(67, 12)
        Me.Label6.TabIndex = 82
        Me.Label6.Text = "後標(低標): "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxBottom
        '
        Me.tboxBottom.Location = New System.Drawing.Point(574, 574)
        Me.tboxBottom.Name = "tboxBottom"
        Me.tboxBottom.Size = New System.Drawing.Size(60, 22)
        Me.tboxBottom.TabIndex = 85
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(533, 577)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 12)
        Me.Label7.TabIndex = 84
        Me.Label7.Text = "底標: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frmPostGrade
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.tboxBottom)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tboxLow)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxMid)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.tboxHigh)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.tboxTop)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Name = "frmPostGrade"
        Me.Text = "張貼考試成績"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuStuInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxShowRemarks As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxComputeTotal As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowCancel As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowHasGrade As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowO As System.Windows.Forms.CheckBox
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkboxShowAttend As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxNoAbsent As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxNoDigits As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxAutoSize As System.Windows.Forms.CheckBox
    Friend WithEvents cboxFontSize As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboxFont As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstPaper As System.Windows.Forms.CheckedListBox
    Friend WithEvents linklblNoSc As System.Windows.Forms.LinkLabel
    Friend WithEvents linklblAllSc As System.Windows.Forms.LinkLabel
    Friend WithEvents linklblNoPaper As System.Windows.Forms.LinkLabel
    Friend WithEvents linklblAllPaper As System.Windows.Forms.LinkLabel
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents tboxHigh As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxTop As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxMid As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxLow As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxBottom As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
