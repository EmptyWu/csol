﻿Public Class frmFamilySearch
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmFamilySearch
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()
        RefreshData()
        InitSelections()
        Me.Text = My.Resources.frmFamilySearch
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(5)
    End Sub

    Private Sub frmFamilySearch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmFamilySearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()

        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        'dtStu = objCsol.ListStudentSiblings
        cboxClass.Items.Clear()
        lstClass.Clear()
        'cboxClass.Items.Add(My.Resources.all)
        'lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        cboxClass.SelectedIndex = -1

    End Sub

   

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged

        dtStu = objCsol.ListStudentSiblings

        Dim lstfilter As New List(Of String)
        If RdoBtnFormal.Checked = True Then
            lstfilter.Add(String.Format("{0} = 1 ", c_StuTypeColumnName))
        Else
            lstfilter.Add(String.Format("{0} = 0 ", c_StuTypeColumnName))
        End If
        If cboxSubClass.Text = "全部" Then
            lstfilter.Add("ClassName = '" & cboxClass.Text.ToString.Trim & "'")
        Else
            lstfilter.Add("ClassName = '" & cboxClass.Text.ToString.Trim & "'")
            lstfilter.Add("SubClassName = '" & cboxSubClass.Text.ToString.Trim & "'")
        End If

        'If cboxSubClass.Text = "全部" Then
        '    If RdoBtnFormal.Checked = True Then
        '        lstfilter.Add(String.Format("{0} = 1 AND ClassName = '" & cboxClass.Text.ToString.Trim & "'", c_StuTypeColumnName))
        '    Else
        '        lstfilter.Add(String.Format("{0} = 0 AND ClassName = '" & cboxClass.Text.ToString.Trim & "'", c_StuTypeColumnName))
        '    End If
        'Else
        '    If RdoBtnFormal.Checked = True Then
        '        lstfilter.Add(String.Format("{0} = 1 AND ClassName = '" & cboxClass.Text.ToString.Trim & "' AND SubClassName = '" & _
        '                                    cboxSubClass.Text.ToString.Trim & "'", c_StuTypeColumnName))
        '    Else
        '        lstfilter.Add(String.Format("{0} = 0 AND ClassName = '" & cboxClass.Text.ToString.Trim & "' AND SubClassName = '" & _
        '                                    cboxSubClass.Text.ToString.Trim & "'", c_StuTypeColumnName))
        '    End If
        'End If

        dtStu.DefaultView.RowFilter = String.Join("And ", lstfilter.ToArray)
        dgv.DataSource = dtStu.DefaultView

        RefreshTable()
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        cboxClass.Items.Clear()
        lstClass.Clear()
        cboxClass.Items.Add(My.Resources.all)
        lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        cboxClass.SelectedIndex = -1
        cboxClass.Text = ""
        cboxSubClass.Text = ""
    End Sub

    Private Sub RefreshTable()
        If dtStu.Rows.Count = 0 Then
            dgv.DataSource = dtStu.DefaultView
            LoadColumnText()
            Exit Sub
        End If

        LoadColumnText()
    End Sub
    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then

            dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_StuIDColumnName).Visible = True
            dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_StuNameColumnName).Visible = True
            dgv.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgv.Columns.Item(c_AppellationColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_AppellationColumnName).Visible = True
            dgv.Columns.Item(c_AppellationColumnName).HeaderText = My.Resources.appellation
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgv.Columns.Item(c_BirthdayColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_BirthdayColumnName).Visible = True
            dgv.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
            dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_SchoolColumnName).Visible = True
            dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgv.Columns.Item(c_RemarksColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_RemarksColumnName).Visible = True
            dgv.Columns.Item(c_RemarksColumnName).HeaderText = My.Resources.remarks
            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgv.Columns.Item(lstColName(index)).Visible = True
                    dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
            If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
                For index As Integer = 0 To dgv.Rows.Count - 1
                    If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                        If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                            dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                        End If
                    End If
                Next
            End If
            If dgv.Columns.Item(c_StuSchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgv.Rows(index).Cells(c_StuSchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgv.Rows(index).Cells(c_StuSchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                            dgv.Rows(index).Cells(c_StuSchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                            dgv.Rows(index).Cells(c_StuSchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
            If dgv.Columns.Item(c_SchoolGradeColumnName).Visible = True Then
                For index As Integer = 0 To dgv.Rows.Count - 1
                    Dim key_Current As String = dgv.Rows(index).Cells(c_CurrentSchColumnName).Value.ToString.Trim()
                    Dim key_Grade As String = dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value.ToString.Trim()
                    Select Case key_Current
                        Case "0"
                            Continue For
                        Case "1"
                            Select Case key_Grade
                                Case "0"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "1"
                                    Continue For
                                Case "1"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "2"
                                    Continue For
                                Case "2"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "3"
                                    Continue For
                                Case "3"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "4"
                                    Continue For
                                Case "4"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "5"
                                    Continue For
                                Case "5"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "6"
                                    Continue For
                                Case Else
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "-1"
                                    Continue For
                            End Select
                        Case "2"
                            Select Case key_Grade
                                Case "0"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "1"
                                    Continue For
                                Case "1"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "2"
                                    Continue For
                                Case "2"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "3"
                                    Continue For
                                Case Else
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "-1"
                                    Continue For
                            End Select
                        Case "3"
                            Select Case key_Grade
                                Case "0"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "1"
                                    Continue For
                                Case "1"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "2"
                                    Continue For
                                Case "2"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "3"
                                    Continue For
                                Case Else
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "-1"
                                    Continue For
                            End Select
                        Case "4"
                            Select Case key_Grade
                                Case "0"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "1"
                                    Continue For
                                Case "1"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "2"
                                    Continue For
                                Case "2"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "3"
                                    Continue For
                                Case "3"
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "4"
                                    Continue For
                                Case Else
                                    dgv.Rows(index).Cells(c_SchoolGradeColumnName).Value = "-1"
                                    Continue For
                            End Select
                        Case Else
                            Continue For
                    End Select
                Next
            End If
        End If
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_StuBirthdayColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_StuSchoolColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_StuRemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 6 Then
            Dim lst As New ArrayList
            For index As Integer = 7 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub frmFamilySearch_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub RdoBtnFormal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdoBtnFormal.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub RdoBtnPotential_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RdoBtnPotential.CheckedChanged
        RefreshTable()
    End Sub

    'Private Sub chkboxFormal_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxFormal.CheckedChanged
    '    RefreshTable()
    'End Sub

    'Private Sub chkboxPotential_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxPotential.CheckedChanged
    '    RefreshTable()
    'End Sub

    Private Sub mnuExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        ExportDgvToExcel(dgv)
    End Sub

    
End Class