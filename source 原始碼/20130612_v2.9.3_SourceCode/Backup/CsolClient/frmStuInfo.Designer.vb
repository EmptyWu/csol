﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuInfo
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument3 = New System.Drawing.Printing.PrintDocument
        Me.prtdocReceipt = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument4 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument5 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument6 = New System.Drawing.Printing.PrintDocument
        Me.tpgClose = New System.Windows.Forms.TabPage
        Me.tpgAbsentRec = New System.Windows.Forms.TabPage
        Me.dgvAbsentRec = New System.Windows.Forms.DataGridView
        Me.butAbsentPrint = New System.Windows.Forms.Button
        Me.butAbsentExport = New System.Windows.Forms.Button
        Me.chkboxAbsentShowPast = New System.Windows.Forms.CheckBox
        Me.cboxAbsentClass = New System.Windows.Forms.ComboBox
        Me.Label49 = New System.Windows.Forms.Label
        Me.tpgTeleInterview = New System.Windows.Forms.TabPage
        Me.butTeleExport = New System.Windows.Forms.Button
        Me.butTeleDel = New System.Windows.Forms.Button
        Me.butTelePrint = New System.Windows.Forms.Button
        Me.butTeleMod = New System.Windows.Forms.Button
        Me.butTeleAdd = New System.Windows.Forms.Button
        Me.chkboxTeleDate = New System.Windows.Forms.CheckBox
        Me.Label52 = New System.Windows.Forms.Label
        Me.cboxTeleType = New System.Windows.Forms.ComboBox
        Me.dgvTeleRec = New System.Windows.Forms.DataGridView
        Me.dtpickTele = New System.Windows.Forms.DateTimePicker
        Me.butTeleListSta = New System.Windows.Forms.Button
        Me.tpgClassTransferRec = New System.Windows.Forms.TabPage
        Me.dgvChange = New System.Windows.Forms.DataGridView
        Me.chkboxChangeShowPast = New System.Windows.Forms.CheckBox
        Me.cboxChangeClass = New System.Windows.Forms.ComboBox
        Me.Label48 = New System.Windows.Forms.Label
        Me.tpgBookRec = New System.Windows.Forms.TabPage
        Me.GroupBox17 = New System.Windows.Forms.GroupBox
        Me.butBookPrint2 = New System.Windows.Forms.Button
        Me.butBookExport2 = New System.Windows.Forms.Button
        Me.cbBookNotReceivePast = New System.Windows.Forms.CheckBox
        Me.dgvBookNotReceive = New System.Windows.Forms.DataGridView
        Me.GroupBox16 = New System.Windows.Forms.GroupBox
        Me.butBookPrint1 = New System.Windows.Forms.Button
        Me.butBookExport1 = New System.Windows.Forms.Button
        Me.cbBookReceivePast = New System.Windows.Forms.CheckBox
        Me.dgvBookReceive = New System.Windows.Forms.DataGridView
        Me.cbBookPastClass = New System.Windows.Forms.CheckBox
        Me.cboxBookClass = New System.Windows.Forms.ComboBox
        Me.Label51 = New System.Windows.Forms.Label
        Me.tpgPunchRec = New System.Windows.Forms.TabPage
        Me.dgvAttRec = New System.Windows.Forms.DataGridView
        Me.butAttPrint2 = New System.Windows.Forms.Button
        Me.butAttExport2 = New System.Windows.Forms.Button
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label47 = New System.Windows.Forms.Label
        Me.radbutAttShowRange = New System.Windows.Forms.RadioButton
        Me.radbutAttShowAll = New System.Windows.Forms.RadioButton
        Me.Label46 = New System.Windows.Forms.Label
        Me.dgvAttClass = New System.Windows.Forms.DataGridView
        Me.butAttPrint = New System.Windows.Forms.Button
        Me.butAttExport = New System.Windows.Forms.Button
        Me.chkboxAttShowPast = New System.Windows.Forms.CheckBox
        Me.cboxAttClass = New System.Windows.Forms.ComboBox
        Me.Label45 = New System.Windows.Forms.Label
        Me.tpgKBRec = New System.Windows.Forms.TabPage
        Me.GroupBox15 = New System.Windows.Forms.GroupBox
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.tpgKBPay = New System.Windows.Forms.TabPage
        Me.dgvKBPayRec = New System.Windows.Forms.DataGridView
        Me.tpgKBShowUp = New System.Windows.Forms.TabPage
        Me.dgvKBAttendance = New System.Windows.Forms.DataGridView
        Me.tpgKBBookRec = New System.Windows.Forms.TabPage
        Me.dgvKBBook = New System.Windows.Forms.DataGridView
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.butKBDelete = New System.Windows.Forms.Button
        Me.butKBRemarks = New System.Windows.Forms.Button
        Me.butKBMod = New System.Windows.Forms.Button
        Me.dgvKB = New System.Windows.Forms.DataGridView
        Me.tpgPay = New System.Windows.Forms.TabPage
        Me.chkboxNoAskPrintReceipt = New System.Windows.Forms.CheckBox
        Me.grpboxExtra = New System.Windows.Forms.GroupBox
        Me.lblCheque = New System.Windows.Forms.Label
        Me.dtpickCheque = New System.Windows.Forms.DateTimePicker
        Me.tboxPayExtraInfo = New System.Windows.Forms.TextBox
        Me.butPayPrintOpt = New System.Windows.Forms.Button
        Me.butPayAck = New System.Windows.Forms.Button
        Me.butPayPrintTogether = New System.Windows.Forms.Button
        Me.butPayTogether = New System.Windows.Forms.Button
        Me.butPayOthers = New System.Windows.Forms.Button
        Me.butPayPrint = New System.Windows.Forms.Button
        Me.butPayKeep = New System.Windows.Forms.Button
        Me.butPayBack = New System.Windows.Forms.Button
        Me.butPayDelete = New System.Windows.Forms.Button
        Me.butPayModify = New System.Windows.Forms.Button
        Me.butPaySave = New System.Windows.Forms.Button
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.btnDiscCancel = New System.Windows.Forms.Button
        Me.butModDisc = New System.Windows.Forms.Button
        Me.tboxReceiptRemarks = New System.Windows.Forms.TextBox
        Me.butReceiptRemarks = New System.Windows.Forms.Button
        Me.Label42 = New System.Windows.Forms.Label
        Me.tboxDiscountRemarks = New System.Windows.Forms.TextBox
        Me.butDiscountRemarks = New System.Windows.Forms.Button
        Me.Label41 = New System.Windows.Forms.Label
        Me.tboxDiscount = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.dgvPayDetails = New System.Windows.Forms.DataGridView
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.chkboxShowPast2 = New System.Windows.Forms.CheckBox
        Me.dgvClass2 = New System.Windows.Forms.DataGridView
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.radbutMethod5 = New System.Windows.Forms.RadioButton
        Me.radbutMethod4 = New System.Windows.Forms.RadioButton
        Me.radbutMethod3 = New System.Windows.Forms.RadioButton
        Me.radbutMethod2 = New System.Windows.Forms.RadioButton
        Me.radbutMethod1 = New System.Windows.Forms.RadioButton
        Me.tboxPayToday = New System.Windows.Forms.TextBox
        Me.Label50 = New System.Windows.Forms.Label
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.dtpickMakeupDate = New System.Windows.Forms.DateTimePicker
        Me.radbutNormal = New System.Windows.Forms.RadioButton
        Me.radbutMakeup = New System.Windows.Forms.RadioButton
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.tboxReceiptNum = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tpgInfo = New System.Windows.Forms.TabPage
        Me.lblImgInfo = New System.Windows.Forms.Label
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tboxRemark4 = New System.Windows.Forms.TextBox
        Me.tboxRemark3 = New System.Windows.Forms.TextBox
        Me.tboxRemark2 = New System.Windows.Forms.TextBox
        Me.tboxRemark1 = New System.Windows.Forms.TextBox
        Me.tboxRank4 = New System.Windows.Forms.TextBox
        Me.tboxRank3 = New System.Windows.Forms.TextBox
        Me.tboxRank2 = New System.Windows.Forms.TextBox
        Me.tboxRank1 = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.Label38 = New System.Windows.Forms.Label
        Me.tboxSchool4 = New System.Windows.Forms.TextBox
        Me.cboxFamily4 = New System.Windows.Forms.ComboBox
        Me.tboxBirth4d = New System.Windows.Forms.TextBox
        Me.tboxBirth4m = New System.Windows.Forms.TextBox
        Me.tboxBirth4y = New System.Windows.Forms.TextBox
        Me.tboxName4 = New System.Windows.Forms.TextBox
        Me.tboxSchool3 = New System.Windows.Forms.TextBox
        Me.cboxFamily3 = New System.Windows.Forms.ComboBox
        Me.tboxBirth3d = New System.Windows.Forms.TextBox
        Me.tboxBirth3m = New System.Windows.Forms.TextBox
        Me.tboxBirth3y = New System.Windows.Forms.TextBox
        Me.tboxName3 = New System.Windows.Forms.TextBox
        Me.tboxSchool2 = New System.Windows.Forms.TextBox
        Me.cboxFamily2 = New System.Windows.Forms.ComboBox
        Me.tboxBirth2d = New System.Windows.Forms.TextBox
        Me.tboxBirth2m = New System.Windows.Forms.TextBox
        Me.tboxBirth2y = New System.Windows.Forms.TextBox
        Me.tboxName2 = New System.Windows.Forms.TextBox
        Me.tboxSchool1 = New System.Windows.Forms.TextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.cboxFamily1 = New System.Windows.Forms.ComboBox
        Me.tboxBirth1d = New System.Windows.Forms.TextBox
        Me.tboxBirth1m = New System.Windows.Forms.TextBox
        Me.tboxBirth1y = New System.Windows.Forms.TextBox
        Me.Label39 = New System.Windows.Forms.Label
        Me.tboxName1 = New System.Windows.Forms.TextBox
        Me.Label43 = New System.Windows.Forms.Label
        Me.Label44 = New System.Windows.Forms.Label
        Me.butDelPic = New System.Windows.Forms.Button
        Me.butAddPic = New System.Windows.Forms.Button
        Me.butSavePic = New System.Windows.Forms.Button
        Me.butWebcam = New System.Windows.Forms.Button
        Me.butChangeId = New System.Windows.Forms.Button
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutNowUni = New System.Windows.Forms.RadioButton
        Me.radbutNowHig = New System.Windows.Forms.RadioButton
        Me.tboxClass = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboxGroup = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cboxGrade = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.butUni = New System.Windows.Forms.Button
        Me.cboxUniversity = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.butHig = New System.Windows.Forms.Button
        Me.cboxHighSch = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.butJun = New System.Windows.Forms.Button
        Me.cboxJuniorSch = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.butPri = New System.Windows.Forms.Button
        Me.cboxPrimarySch = New System.Windows.Forms.ComboBox
        Me.radbutNowJun = New System.Windows.Forms.RadioButton
        Me.radbutNowPri = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.butPrint = New System.Windows.Forms.Button
        Me.butNext = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.butSales = New System.Windows.Forms.Button
        Me.butClassCancel = New System.Windows.Forms.Button
        Me.butClassChange = New System.Windows.Forms.Button
        Me.butSeatNum = New System.Windows.Forms.Button
        Me.butDelClass = New System.Windows.Forms.Button
        Me.butReg = New System.Windows.Forms.Button
        Me.chkboxShowNotOpen = New System.Windows.Forms.CheckBox
        Me.chkboxShowOpen = New System.Windows.Forms.CheckBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.dgvClass = New System.Windows.Forms.DataGridView
        Me.butPrevious = New System.Windows.Forms.Button
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.rtboxRemarks = New System.Windows.Forms.RichTextBox
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem
        Me.butSearch = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.picbox = New System.Windows.Forms.PictureBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cboxSex = New System.Windows.Forms.ComboBox
        Me.tboxBirthD = New System.Windows.Forms.TextBox
        Me.tboxBirthM = New System.Windows.Forms.TextBox
        Me.tboxBirthY = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxEngName = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.butDelCard = New System.Windows.Forms.Button
        Me.butIssueCard = New System.Windows.Forms.Button
        Me.radbutPotStu = New System.Windows.Forms.RadioButton
        Me.radbutFormalStu = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxCreateDate = New System.Windows.Forms.TextBox
        Me.lblCreateDate = New System.Windows.Forms.Label
        Me.tboxStuName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblStuId = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.tboxPunchCardNum = New System.Windows.Forms.TextBox
        Me.Label53 = New System.Windows.Forms.Label
        Me.tboxEnrollSch = New System.Windows.Forms.TextBox
        Me.Label54 = New System.Windows.Forms.Label
        Me.tboxHseeMark = New System.Windows.Forms.TextBox
        Me.Label55 = New System.Windows.Forms.Label
        Me.tboxAdd1b = New System.Windows.Forms.TextBox
        Me.tboxAdd2b = New System.Windows.Forms.TextBox
        Me.tboxClassId = New System.Windows.Forms.TextBox
        Me.tboxStay = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.tboxDadTitle = New System.Windows.Forms.TextBox
        Me.tboxRegInfo = New System.Windows.Forms.TextBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.tboxMumTitle = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.tboxMumMobile = New System.Windows.Forms.TextBox
        Me.tboxIntroName = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.tboxGradJunior = New System.Windows.Forms.TextBox
        Me.tboxMumName = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.tboxIc = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.tboxAdd1a = New System.Windows.Forms.TextBox
        Me.tboxEmail = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.tboxAdd2a = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.tboxIntroId = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.tboxMobile = New System.Windows.Forms.TextBox
        Me.tboxDadMobile = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.tboxDadName = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.tboxTel1 = New System.Windows.Forms.TextBox
        Me.tboxOfficeTel = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.tboxTel2 = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.tabCtrl = New System.Windows.Forms.TabControl
        Me.tabAssign = New System.Windows.Forms.TabPage
        Me.btnRefreshAssgin = New System.Windows.Forms.Button
        Me.GroupBox19 = New System.Windows.Forms.GroupBox
        Me.dgvAssignNoPass = New System.Windows.Forms.DataGridView
        Me.GroupBox18 = New System.Windows.Forms.GroupBox
        Me.dgvAssignPass = New System.Windows.Forms.DataGridView
        Me.Label56 = New System.Windows.Forms.Label
        Me.chkboxAssignShowPast = New System.Windows.Forms.CheckBox
        Me.cboxAssignClass = New System.Windows.Forms.ComboBox
        Me.tabGrade = New System.Windows.Forms.TabPage
        Me.GroupBox23 = New System.Windows.Forms.GroupBox
        Me.dgvPaperNoPass = New System.Windows.Forms.DataGridView
        Me.GroupBox22 = New System.Windows.Forms.GroupBox
        Me.dgvPaperPass = New System.Windows.Forms.DataGridView
        Me.chkboxPaperShowPast = New System.Windows.Forms.CheckBox
        Me.cboxPaperClass = New System.Windows.Forms.ComboBox
        Me.Label58 = New System.Windows.Forms.Label
        Me.Button1 = New System.Windows.Forms.Button
        Me.GroupBox20 = New System.Windows.Forms.GroupBox
        Me.DataGridView1 = New System.Windows.Forms.DataGridView
        Me.GroupBox21 = New System.Windows.Forms.GroupBox
        Me.DataGridView2 = New System.Windows.Forms.DataGridView
        Me.Label57 = New System.Windows.Forms.Label
        Me.CheckBox1 = New System.Windows.Forms.CheckBox
        Me.ComboBox1 = New System.Windows.Forms.ComboBox
        Me.tpgAbsentRec.SuspendLayout()
        CType(Me.dgvAbsentRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgTeleInterview.SuspendLayout()
        CType(Me.dgvTeleRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgClassTransferRec.SuspendLayout()
        CType(Me.dgvChange, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgBookRec.SuspendLayout()
        Me.GroupBox17.SuspendLayout()
        CType(Me.dgvBookNotReceive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox16.SuspendLayout()
        CType(Me.dgvBookReceive, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgPunchRec.SuspendLayout()
        CType(Me.dgvAttRec, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvAttClass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgKBRec.SuspendLayout()
        Me.GroupBox15.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.tpgKBPay.SuspendLayout()
        CType(Me.dgvKBPayRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgKBShowUp.SuspendLayout()
        CType(Me.dgvKBAttendance, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgKBBookRec.SuspendLayout()
        CType(Me.dgvKBBook, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox14.SuspendLayout()
        CType(Me.dgvKB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpgPay.SuspendLayout()
        Me.grpboxExtra.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        CType(Me.dgvPayDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox11.SuspendLayout()
        CType(Me.dgvClass2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.tpgInfo.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.tabCtrl.SuspendLayout()
        Me.tabAssign.SuspendLayout()
        Me.GroupBox19.SuspendLayout()
        CType(Me.dgvAssignNoPass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox18.SuspendLayout()
        CType(Me.dgvAssignPass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tabGrade.SuspendLayout()
        Me.GroupBox23.SuspendLayout()
        CType(Me.dgvPaperNoPass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox22.SuspendLayout()
        CType(Me.dgvPaperPass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox20.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox21.SuspendLayout()
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'PrintDocument3
        '
        '
        'prtdocReceipt
        '
        '
        'PrintDocument4
        '
        '
        'PrintDocument5
        '
        '
        'PrintDocument6
        '
        '
        'tpgClose
        '
        Me.tpgClose.Location = New System.Drawing.Point(4, 25)
        Me.tpgClose.Name = "tpgClose"
        Me.tpgClose.Size = New System.Drawing.Size(1017, 570)
        Me.tpgClose.TabIndex = 7
        Me.tpgClose.Text = "關閉"
        Me.tpgClose.UseVisualStyleBackColor = True
        '
        'tpgAbsentRec
        '
        Me.tpgAbsentRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgAbsentRec.Controls.Add(Me.dgvAbsentRec)
        Me.tpgAbsentRec.Controls.Add(Me.butAbsentPrint)
        Me.tpgAbsentRec.Controls.Add(Me.butAbsentExport)
        Me.tpgAbsentRec.Controls.Add(Me.chkboxAbsentShowPast)
        Me.tpgAbsentRec.Controls.Add(Me.cboxAbsentClass)
        Me.tpgAbsentRec.Controls.Add(Me.Label49)
        Me.tpgAbsentRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgAbsentRec.Name = "tpgAbsentRec"
        Me.tpgAbsentRec.Size = New System.Drawing.Size(1017, 570)
        Me.tpgAbsentRec.TabIndex = 6
        Me.tpgAbsentRec.Text = "缺課紀錄"
        Me.tpgAbsentRec.UseVisualStyleBackColor = True
        '
        'dgvAbsentRec
        '
        Me.dgvAbsentRec.AllowUserToAddRows = False
        Me.dgvAbsentRec.AllowUserToDeleteRows = False
        Me.dgvAbsentRec.AllowUserToResizeRows = False
        Me.dgvAbsentRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAbsentRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAbsentRec.Location = New System.Drawing.Point(18, 52)
        Me.dgvAbsentRec.MultiSelect = False
        Me.dgvAbsentRec.Name = "dgvAbsentRec"
        Me.dgvAbsentRec.ReadOnly = True
        Me.dgvAbsentRec.RowHeadersVisible = False
        Me.dgvAbsentRec.RowTemplate.Height = 24
        Me.dgvAbsentRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAbsentRec.ShowCellToolTips = False
        Me.dgvAbsentRec.ShowEditingIcon = False
        Me.dgvAbsentRec.Size = New System.Drawing.Size(971, 495)
        Me.dgvAbsentRec.TabIndex = 209
        '
        'butAbsentPrint
        '
        Me.butAbsentPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAbsentPrint.Location = New System.Drawing.Point(507, 12)
        Me.butAbsentPrint.Name = "butAbsentPrint"
        Me.butAbsentPrint.Size = New System.Drawing.Size(55, 25)
        Me.butAbsentPrint.TabIndex = 208
        Me.butAbsentPrint.Text = "列印"
        Me.butAbsentPrint.UseVisualStyleBackColor = True
        '
        'butAbsentExport
        '
        Me.butAbsentExport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAbsentExport.Location = New System.Drawing.Point(577, 12)
        Me.butAbsentExport.Name = "butAbsentExport"
        Me.butAbsentExport.Size = New System.Drawing.Size(65, 25)
        Me.butAbsentExport.TabIndex = 207
        Me.butAbsentExport.Text = "匯出"
        Me.butAbsentExport.UseVisualStyleBackColor = True
        '
        'chkboxAbsentShowPast
        '
        Me.chkboxAbsentShowPast.AutoSize = True
        Me.chkboxAbsentShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxAbsentShowPast.Location = New System.Drawing.Point(380, 17)
        Me.chkboxAbsentShowPast.Name = "chkboxAbsentShowPast"
        Me.chkboxAbsentShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxAbsentShowPast.TabIndex = 206
        Me.chkboxAbsentShowPast.Text = "顯示過時班級"
        Me.chkboxAbsentShowPast.UseVisualStyleBackColor = True
        '
        'cboxAbsentClass
        '
        Me.cboxAbsentClass.FormattingEnabled = True
        Me.cboxAbsentClass.Location = New System.Drawing.Point(57, 15)
        Me.cboxAbsentClass.Name = "cboxAbsentClass"
        Me.cboxAbsentClass.Size = New System.Drawing.Size(289, 20)
        Me.cboxAbsentClass.TabIndex = 205
        '
        'Label49
        '
        Me.Label49.AutoSize = True
        Me.Label49.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label49.Location = New System.Drawing.Point(16, 18)
        Me.Label49.Name = "Label49"
        Me.Label49.Size = New System.Drawing.Size(35, 12)
        Me.Label49.TabIndex = 204
        Me.Label49.Text = "班級: "
        Me.Label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tpgTeleInterview
        '
        Me.tpgTeleInterview.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgTeleInterview.Controls.Add(Me.butTeleExport)
        Me.tpgTeleInterview.Controls.Add(Me.butTeleDel)
        Me.tpgTeleInterview.Controls.Add(Me.butTelePrint)
        Me.tpgTeleInterview.Controls.Add(Me.butTeleMod)
        Me.tpgTeleInterview.Controls.Add(Me.butTeleAdd)
        Me.tpgTeleInterview.Controls.Add(Me.chkboxTeleDate)
        Me.tpgTeleInterview.Controls.Add(Me.Label52)
        Me.tpgTeleInterview.Controls.Add(Me.cboxTeleType)
        Me.tpgTeleInterview.Controls.Add(Me.dgvTeleRec)
        Me.tpgTeleInterview.Controls.Add(Me.dtpickTele)
        Me.tpgTeleInterview.Controls.Add(Me.butTeleListSta)
        Me.tpgTeleInterview.Location = New System.Drawing.Point(4, 25)
        Me.tpgTeleInterview.Name = "tpgTeleInterview"
        Me.tpgTeleInterview.Size = New System.Drawing.Size(1017, 570)
        Me.tpgTeleInterview.TabIndex = 8
        Me.tpgTeleInterview.Text = "電訪紀錄"
        Me.tpgTeleInterview.UseVisualStyleBackColor = True
        '
        'butTeleExport
        '
        Me.butTeleExport.AutoSize = True
        Me.butTeleExport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTeleExport.Location = New System.Drawing.Point(362, 429)
        Me.butTeleExport.Name = "butTeleExport"
        Me.butTeleExport.Size = New System.Drawing.Size(82, 25)
        Me.butTeleExport.TabIndex = 170
        Me.butTeleExport.Text = "匯出"
        Me.butTeleExport.UseVisualStyleBackColor = True
        '
        'butTeleDel
        '
        Me.butTeleDel.AutoSize = True
        Me.butTeleDel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTeleDel.Location = New System.Drawing.Point(186, 429)
        Me.butTeleDel.Name = "butTeleDel"
        Me.butTeleDel.Size = New System.Drawing.Size(82, 25)
        Me.butTeleDel.TabIndex = 169
        Me.butTeleDel.Text = "刪除"
        Me.butTeleDel.UseVisualStyleBackColor = True
        '
        'butTelePrint
        '
        Me.butTelePrint.AutoSize = True
        Me.butTelePrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTelePrint.Location = New System.Drawing.Point(274, 429)
        Me.butTelePrint.Name = "butTelePrint"
        Me.butTelePrint.Size = New System.Drawing.Size(82, 25)
        Me.butTelePrint.TabIndex = 168
        Me.butTelePrint.Text = "列印"
        Me.butTelePrint.UseVisualStyleBackColor = True
        '
        'butTeleMod
        '
        Me.butTeleMod.AutoSize = True
        Me.butTeleMod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTeleMod.Location = New System.Drawing.Point(98, 429)
        Me.butTeleMod.Name = "butTeleMod"
        Me.butTeleMod.Size = New System.Drawing.Size(82, 25)
        Me.butTeleMod.TabIndex = 167
        Me.butTeleMod.Text = "修改"
        Me.butTeleMod.UseVisualStyleBackColor = True
        '
        'butTeleAdd
        '
        Me.butTeleAdd.AutoSize = True
        Me.butTeleAdd.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTeleAdd.Location = New System.Drawing.Point(10, 429)
        Me.butTeleAdd.Name = "butTeleAdd"
        Me.butTeleAdd.Size = New System.Drawing.Size(82, 25)
        Me.butTeleAdd.TabIndex = 166
        Me.butTeleAdd.Text = "新增"
        Me.butTeleAdd.UseVisualStyleBackColor = True
        '
        'chkboxTeleDate
        '
        Me.chkboxTeleDate.AutoSize = True
        Me.chkboxTeleDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxTeleDate.Location = New System.Drawing.Point(308, 15)
        Me.chkboxTeleDate.Name = "chkboxTeleDate"
        Me.chkboxTeleDate.Size = New System.Drawing.Size(72, 16)
        Me.chkboxTeleDate.TabIndex = 165
        Me.chkboxTeleDate.Text = "電訪日期"
        Me.chkboxTeleDate.UseVisualStyleBackColor = True
        '
        'Label52
        '
        Me.Label52.AutoSize = True
        Me.Label52.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label52.Location = New System.Drawing.Point(8, 16)
        Me.Label52.Name = "Label52"
        Me.Label52.Size = New System.Drawing.Size(59, 12)
        Me.Label52.TabIndex = 164
        Me.Label52.Text = "電訪類別: "
        Me.Label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxTeleType
        '
        Me.cboxTeleType.FormattingEnabled = True
        Me.cboxTeleType.Location = New System.Drawing.Point(67, 13)
        Me.cboxTeleType.Name = "cboxTeleType"
        Me.cboxTeleType.Size = New System.Drawing.Size(217, 20)
        Me.cboxTeleType.TabIndex = 163
        '
        'dgvTeleRec
        '
        Me.dgvTeleRec.AllowUserToAddRows = False
        Me.dgvTeleRec.AllowUserToDeleteRows = False
        Me.dgvTeleRec.AllowUserToResizeRows = False
        Me.dgvTeleRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvTeleRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTeleRec.Location = New System.Drawing.Point(10, 46)
        Me.dgvTeleRec.MultiSelect = False
        Me.dgvTeleRec.Name = "dgvTeleRec"
        Me.dgvTeleRec.RowHeadersVisible = False
        Me.dgvTeleRec.RowTemplate.Height = 15
        Me.dgvTeleRec.RowTemplate.ReadOnly = True
        Me.dgvTeleRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvTeleRec.ShowCellToolTips = False
        Me.dgvTeleRec.ShowEditingIcon = False
        Me.dgvTeleRec.Size = New System.Drawing.Size(997, 368)
        Me.dgvTeleRec.TabIndex = 162
        '
        'dtpickTele
        '
        Me.dtpickTele.Location = New System.Drawing.Point(386, 11)
        Me.dtpickTele.Name = "dtpickTele"
        Me.dtpickTele.Size = New System.Drawing.Size(119, 22)
        Me.dtpickTele.TabIndex = 160
        '
        'butTeleListSta
        '
        Me.butTeleListSta.AutoSize = True
        Me.butTeleListSta.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butTeleListSta.Location = New System.Drawing.Point(530, 10)
        Me.butTeleListSta.Name = "butTeleListSta"
        Me.butTeleListSta.Size = New System.Drawing.Size(82, 25)
        Me.butTeleListSta.TabIndex = 159
        Me.butTeleListSta.Text = "執行篩選"
        Me.butTeleListSta.UseVisualStyleBackColor = True
        '
        'tpgClassTransferRec
        '
        Me.tpgClassTransferRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgClassTransferRec.Controls.Add(Me.dgvChange)
        Me.tpgClassTransferRec.Controls.Add(Me.chkboxChangeShowPast)
        Me.tpgClassTransferRec.Controls.Add(Me.cboxChangeClass)
        Me.tpgClassTransferRec.Controls.Add(Me.Label48)
        Me.tpgClassTransferRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgClassTransferRec.Name = "tpgClassTransferRec"
        Me.tpgClassTransferRec.Size = New System.Drawing.Size(1017, 570)
        Me.tpgClassTransferRec.TabIndex = 5
        Me.tpgClassTransferRec.Text = "轉班紀錄"
        Me.tpgClassTransferRec.UseVisualStyleBackColor = True
        '
        'dgvChange
        '
        Me.dgvChange.AllowUserToAddRows = False
        Me.dgvChange.AllowUserToDeleteRows = False
        Me.dgvChange.AllowUserToResizeRows = False
        Me.dgvChange.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvChange.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvChange.Location = New System.Drawing.Point(19, 48)
        Me.dgvChange.MultiSelect = False
        Me.dgvChange.Name = "dgvChange"
        Me.dgvChange.ReadOnly = True
        Me.dgvChange.RowHeadersVisible = False
        Me.dgvChange.RowTemplate.Height = 24
        Me.dgvChange.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvChange.ShowCellToolTips = False
        Me.dgvChange.ShowEditingIcon = False
        Me.dgvChange.Size = New System.Drawing.Size(971, 501)
        Me.dgvChange.TabIndex = 207
        '
        'chkboxChangeShowPast
        '
        Me.chkboxChangeShowPast.AutoSize = True
        Me.chkboxChangeShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxChangeShowPast.Location = New System.Drawing.Point(381, 15)
        Me.chkboxChangeShowPast.Name = "chkboxChangeShowPast"
        Me.chkboxChangeShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxChangeShowPast.TabIndex = 206
        Me.chkboxChangeShowPast.Text = "顯示過時班級"
        Me.chkboxChangeShowPast.UseVisualStyleBackColor = True
        '
        'cboxChangeClass
        '
        Me.cboxChangeClass.FormattingEnabled = True
        Me.cboxChangeClass.Location = New System.Drawing.Point(59, 13)
        Me.cboxChangeClass.Name = "cboxChangeClass"
        Me.cboxChangeClass.Size = New System.Drawing.Size(279, 20)
        Me.cboxChangeClass.TabIndex = 205
        '
        'Label48
        '
        Me.Label48.AutoSize = True
        Me.Label48.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label48.Location = New System.Drawing.Point(17, 14)
        Me.Label48.Name = "Label48"
        Me.Label48.Size = New System.Drawing.Size(35, 12)
        Me.Label48.TabIndex = 204
        Me.Label48.Text = "班級: "
        Me.Label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tpgBookRec
        '
        Me.tpgBookRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgBookRec.Controls.Add(Me.GroupBox17)
        Me.tpgBookRec.Controls.Add(Me.GroupBox16)
        Me.tpgBookRec.Controls.Add(Me.cbBookPastClass)
        Me.tpgBookRec.Controls.Add(Me.cboxBookClass)
        Me.tpgBookRec.Controls.Add(Me.Label51)
        Me.tpgBookRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgBookRec.Name = "tpgBookRec"
        Me.tpgBookRec.Size = New System.Drawing.Size(1017, 570)
        Me.tpgBookRec.TabIndex = 4
        Me.tpgBookRec.Text = "領書紀錄"
        Me.tpgBookRec.UseVisualStyleBackColor = True
        '
        'GroupBox17
        '
        Me.GroupBox17.Controls.Add(Me.butBookPrint2)
        Me.GroupBox17.Controls.Add(Me.butBookExport2)
        Me.GroupBox17.Controls.Add(Me.cbBookNotReceivePast)
        Me.GroupBox17.Controls.Add(Me.dgvBookNotReceive)
        Me.GroupBox17.Location = New System.Drawing.Point(17, 311)
        Me.GroupBox17.Name = "GroupBox17"
        Me.GroupBox17.Size = New System.Drawing.Size(994, 252)
        Me.GroupBox17.TabIndex = 215
        Me.GroupBox17.TabStop = False
        Me.GroupBox17.Text = "未領書籍"
        '
        'butBookPrint2
        '
        Me.butBookPrint2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butBookPrint2.Location = New System.Drawing.Point(6, 221)
        Me.butBookPrint2.Name = "butBookPrint2"
        Me.butBookPrint2.Size = New System.Drawing.Size(55, 25)
        Me.butBookPrint2.TabIndex = 215
        Me.butBookPrint2.Text = "列印"
        Me.butBookPrint2.UseVisualStyleBackColor = True
        '
        'butBookExport2
        '
        Me.butBookExport2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butBookExport2.Location = New System.Drawing.Point(76, 221)
        Me.butBookExport2.Name = "butBookExport2"
        Me.butBookExport2.Size = New System.Drawing.Size(65, 25)
        Me.butBookExport2.TabIndex = 214
        Me.butBookExport2.Text = "匯出"
        Me.butBookExport2.UseVisualStyleBackColor = True
        '
        'cbBookNotReceivePast
        '
        Me.cbBookNotReceivePast.AutoSize = True
        Me.cbBookNotReceivePast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cbBookNotReceivePast.Location = New System.Drawing.Point(76, 0)
        Me.cbBookNotReceivePast.Name = "cbBookNotReceivePast"
        Me.cbBookNotReceivePast.Size = New System.Drawing.Size(96, 16)
        Me.cbBookNotReceivePast.TabIndex = 212
        Me.cbBookNotReceivePast.Text = "顯示過時書籍"
        Me.cbBookNotReceivePast.UseVisualStyleBackColor = True
        '
        'dgvBookNotReceive
        '
        Me.dgvBookNotReceive.AllowUserToAddRows = False
        Me.dgvBookNotReceive.AllowUserToDeleteRows = False
        Me.dgvBookNotReceive.AllowUserToResizeRows = False
        Me.dgvBookNotReceive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvBookNotReceive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBookNotReceive.Location = New System.Drawing.Point(7, 18)
        Me.dgvBookNotReceive.MultiSelect = False
        Me.dgvBookNotReceive.Name = "dgvBookNotReceive"
        Me.dgvBookNotReceive.ReadOnly = True
        Me.dgvBookNotReceive.RowHeadersVisible = False
        Me.dgvBookNotReceive.RowTemplate.Height = 24
        Me.dgvBookNotReceive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBookNotReceive.ShowCellToolTips = False
        Me.dgvBookNotReceive.ShowEditingIcon = False
        Me.dgvBookNotReceive.Size = New System.Drawing.Size(971, 197)
        Me.dgvBookNotReceive.TabIndex = 0
        '
        'GroupBox16
        '
        Me.GroupBox16.Controls.Add(Me.butBookPrint1)
        Me.GroupBox16.Controls.Add(Me.butBookExport1)
        Me.GroupBox16.Controls.Add(Me.cbBookReceivePast)
        Me.GroupBox16.Controls.Add(Me.dgvBookReceive)
        Me.GroupBox16.Location = New System.Drawing.Point(17, 53)
        Me.GroupBox16.Name = "GroupBox16"
        Me.GroupBox16.Size = New System.Drawing.Size(994, 252)
        Me.GroupBox16.TabIndex = 214
        Me.GroupBox16.TabStop = False
        Me.GroupBox16.Text = "已領書籍"
        '
        'butBookPrint1
        '
        Me.butBookPrint1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butBookPrint1.Location = New System.Drawing.Point(6, 221)
        Me.butBookPrint1.Name = "butBookPrint1"
        Me.butBookPrint1.Size = New System.Drawing.Size(55, 25)
        Me.butBookPrint1.TabIndex = 215
        Me.butBookPrint1.Text = "列印"
        Me.butBookPrint1.UseVisualStyleBackColor = True
        '
        'butBookExport1
        '
        Me.butBookExport1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butBookExport1.Location = New System.Drawing.Point(76, 221)
        Me.butBookExport1.Name = "butBookExport1"
        Me.butBookExport1.Size = New System.Drawing.Size(65, 25)
        Me.butBookExport1.TabIndex = 214
        Me.butBookExport1.Text = "匯出"
        Me.butBookExport1.UseVisualStyleBackColor = True
        '
        'cbBookReceivePast
        '
        Me.cbBookReceivePast.AutoSize = True
        Me.cbBookReceivePast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cbBookReceivePast.Location = New System.Drawing.Point(76, 0)
        Me.cbBookReceivePast.Name = "cbBookReceivePast"
        Me.cbBookReceivePast.Size = New System.Drawing.Size(96, 16)
        Me.cbBookReceivePast.TabIndex = 212
        Me.cbBookReceivePast.Text = "顯示過時書籍"
        Me.cbBookReceivePast.UseVisualStyleBackColor = True
        '
        'dgvBookReceive
        '
        Me.dgvBookReceive.AllowUserToAddRows = False
        Me.dgvBookReceive.AllowUserToDeleteRows = False
        Me.dgvBookReceive.AllowUserToResizeRows = False
        Me.dgvBookReceive.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvBookReceive.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvBookReceive.Location = New System.Drawing.Point(7, 18)
        Me.dgvBookReceive.MultiSelect = False
        Me.dgvBookReceive.Name = "dgvBookReceive"
        Me.dgvBookReceive.ReadOnly = True
        Me.dgvBookReceive.RowHeadersVisible = False
        Me.dgvBookReceive.RowTemplate.Height = 24
        Me.dgvBookReceive.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvBookReceive.ShowCellToolTips = False
        Me.dgvBookReceive.ShowEditingIcon = False
        Me.dgvBookReceive.Size = New System.Drawing.Size(971, 197)
        Me.dgvBookReceive.TabIndex = 0
        '
        'cbBookPastClass
        '
        Me.cbBookPastClass.AutoSize = True
        Me.cbBookPastClass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.cbBookPastClass.Location = New System.Drawing.Point(373, 18)
        Me.cbBookPastClass.Name = "cbBookPastClass"
        Me.cbBookPastClass.Size = New System.Drawing.Size(96, 16)
        Me.cbBookPastClass.TabIndex = 211
        Me.cbBookPastClass.Text = "顯示過時班級"
        Me.cbBookPastClass.UseVisualStyleBackColor = True
        '
        'cboxBookClass
        '
        Me.cboxBookClass.FormattingEnabled = True
        Me.cboxBookClass.Location = New System.Drawing.Point(62, 14)
        Me.cboxBookClass.Name = "cboxBookClass"
        Me.cboxBookClass.Size = New System.Drawing.Size(283, 20)
        Me.cboxBookClass.TabIndex = 210
        '
        'Label51
        '
        Me.Label51.AutoSize = True
        Me.Label51.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label51.Location = New System.Drawing.Point(21, 15)
        Me.Label51.Name = "Label51"
        Me.Label51.Size = New System.Drawing.Size(35, 12)
        Me.Label51.TabIndex = 209
        Me.Label51.Text = "班級: "
        Me.Label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tpgPunchRec
        '
        Me.tpgPunchRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgPunchRec.Controls.Add(Me.dgvAttRec)
        Me.tpgPunchRec.Controls.Add(Me.butAttPrint2)
        Me.tpgPunchRec.Controls.Add(Me.butAttExport2)
        Me.tpgPunchRec.Controls.Add(Me.dtpickTo)
        Me.tpgPunchRec.Controls.Add(Me.dtpickFrom)
        Me.tpgPunchRec.Controls.Add(Me.Label47)
        Me.tpgPunchRec.Controls.Add(Me.radbutAttShowRange)
        Me.tpgPunchRec.Controls.Add(Me.radbutAttShowAll)
        Me.tpgPunchRec.Controls.Add(Me.Label46)
        Me.tpgPunchRec.Controls.Add(Me.dgvAttClass)
        Me.tpgPunchRec.Controls.Add(Me.butAttPrint)
        Me.tpgPunchRec.Controls.Add(Me.butAttExport)
        Me.tpgPunchRec.Controls.Add(Me.chkboxAttShowPast)
        Me.tpgPunchRec.Controls.Add(Me.cboxAttClass)
        Me.tpgPunchRec.Controls.Add(Me.Label45)
        Me.tpgPunchRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgPunchRec.Name = "tpgPunchRec"
        Me.tpgPunchRec.Size = New System.Drawing.Size(1017, 570)
        Me.tpgPunchRec.TabIndex = 3
        Me.tpgPunchRec.Text = "出勤紀錄"
        Me.tpgPunchRec.UseVisualStyleBackColor = True
        '
        'dgvAttRec
        '
        Me.dgvAttRec.AllowUserToAddRows = False
        Me.dgvAttRec.AllowUserToDeleteRows = False
        Me.dgvAttRec.AllowUserToResizeRows = False
        Me.dgvAttRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAttRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAttRec.Location = New System.Drawing.Point(26, 329)
        Me.dgvAttRec.MultiSelect = False
        Me.dgvAttRec.Name = "dgvAttRec"
        Me.dgvAttRec.ReadOnly = True
        Me.dgvAttRec.RowHeadersVisible = False
        Me.dgvAttRec.RowTemplate.Height = 24
        Me.dgvAttRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAttRec.ShowCellToolTips = False
        Me.dgvAttRec.ShowEditingIcon = False
        Me.dgvAttRec.Size = New System.Drawing.Size(971, 228)
        Me.dgvAttRec.TabIndex = 212
        '
        'butAttPrint2
        '
        Me.butAttPrint2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAttPrint2.Location = New System.Drawing.Point(554, 298)
        Me.butAttPrint2.Name = "butAttPrint2"
        Me.butAttPrint2.Size = New System.Drawing.Size(55, 25)
        Me.butAttPrint2.TabIndex = 211
        Me.butAttPrint2.Text = "列印"
        Me.butAttPrint2.UseVisualStyleBackColor = True
        '
        'butAttExport2
        '
        Me.butAttExport2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAttExport2.Location = New System.Drawing.Point(624, 298)
        Me.butAttExport2.Name = "butAttExport2"
        Me.butAttExport2.Size = New System.Drawing.Size(65, 25)
        Me.butAttExport2.TabIndex = 210
        Me.butAttExport2.Text = "匯出"
        Me.butAttExport2.UseVisualStyleBackColor = True
        '
        'dtpickTo
        '
        Me.dtpickTo.Location = New System.Drawing.Point(415, 299)
        Me.dtpickTo.Name = "dtpickTo"
        Me.dtpickTo.Size = New System.Drawing.Size(119, 22)
        Me.dtpickTo.TabIndex = 209
        '
        'dtpickFrom
        '
        Me.dtpickFrom.Location = New System.Drawing.Point(270, 299)
        Me.dtpickFrom.Name = "dtpickFrom"
        Me.dtpickFrom.Size = New System.Drawing.Size(119, 22)
        Me.dtpickFrom.TabIndex = 208
        '
        'Label47
        '
        Me.Label47.AutoSize = True
        Me.Label47.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label47.Location = New System.Drawing.Point(398, 304)
        Me.Label47.Name = "Label47"
        Me.Label47.Size = New System.Drawing.Size(11, 12)
        Me.Label47.TabIndex = 207
        Me.Label47.Text = "~"
        Me.Label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'radbutAttShowRange
        '
        Me.radbutAttShowRange.AutoSize = True
        Me.radbutAttShowRange.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutAttShowRange.Location = New System.Drawing.Point(189, 302)
        Me.radbutAttShowRange.Name = "radbutAttShowRange"
        Me.radbutAttShowRange.Size = New System.Drawing.Size(71, 16)
        Me.radbutAttShowRange.TabIndex = 206
        Me.radbutAttShowRange.Text = "顯示範圍"
        Me.radbutAttShowRange.UseVisualStyleBackColor = True
        '
        'radbutAttShowAll
        '
        Me.radbutAttShowAll.AutoSize = True
        Me.radbutAttShowAll.Checked = True
        Me.radbutAttShowAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutAttShowAll.Location = New System.Drawing.Point(101, 302)
        Me.radbutAttShowAll.Name = "radbutAttShowAll"
        Me.radbutAttShowAll.Size = New System.Drawing.Size(71, 16)
        Me.radbutAttShowAll.TabIndex = 205
        Me.radbutAttShowAll.TabStop = True
        Me.radbutAttShowAll.Text = "顯示全部"
        Me.radbutAttShowAll.UseVisualStyleBackColor = True
        '
        'Label46
        '
        Me.Label46.AutoSize = True
        Me.Label46.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label46.Location = New System.Drawing.Point(29, 304)
        Me.Label46.Name = "Label46"
        Me.Label46.Size = New System.Drawing.Size(71, 12)
        Me.Label46.TabIndex = 204
        Me.Label46.Text = "有來補習班: "
        Me.Label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dgvAttClass
        '
        Me.dgvAttClass.AllowUserToAddRows = False
        Me.dgvAttClass.AllowUserToDeleteRows = False
        Me.dgvAttClass.AllowUserToResizeRows = False
        Me.dgvAttClass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAttClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAttClass.Location = New System.Drawing.Point(26, 37)
        Me.dgvAttClass.MultiSelect = False
        Me.dgvAttClass.Name = "dgvAttClass"
        Me.dgvAttClass.ReadOnly = True
        Me.dgvAttClass.RowHeadersVisible = False
        Me.dgvAttClass.RowTemplate.Height = 24
        Me.dgvAttClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAttClass.ShowCellToolTips = False
        Me.dgvAttClass.ShowEditingIcon = False
        Me.dgvAttClass.Size = New System.Drawing.Size(971, 255)
        Me.dgvAttClass.TabIndex = 203
        '
        'butAttPrint
        '
        Me.butAttPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAttPrint.Location = New System.Drawing.Point(484, 8)
        Me.butAttPrint.Name = "butAttPrint"
        Me.butAttPrint.Size = New System.Drawing.Size(55, 25)
        Me.butAttPrint.TabIndex = 202
        Me.butAttPrint.Text = "列印"
        Me.butAttPrint.UseVisualStyleBackColor = True
        '
        'butAttExport
        '
        Me.butAttExport.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAttExport.Location = New System.Drawing.Point(554, 7)
        Me.butAttExport.Name = "butAttExport"
        Me.butAttExport.Size = New System.Drawing.Size(65, 25)
        Me.butAttExport.TabIndex = 201
        Me.butAttExport.Text = "匯出"
        Me.butAttExport.UseVisualStyleBackColor = True
        '
        'chkboxAttShowPast
        '
        Me.chkboxAttShowPast.AutoSize = True
        Me.chkboxAttShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxAttShowPast.Location = New System.Drawing.Point(357, 13)
        Me.chkboxAttShowPast.Name = "chkboxAttShowPast"
        Me.chkboxAttShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxAttShowPast.TabIndex = 7
        Me.chkboxAttShowPast.Text = "顯示過時班級"
        Me.chkboxAttShowPast.UseVisualStyleBackColor = True
        '
        'cboxAttClass
        '
        Me.cboxAttClass.FormattingEnabled = True
        Me.cboxAttClass.Location = New System.Drawing.Point(65, 11)
        Me.cboxAttClass.Name = "cboxAttClass"
        Me.cboxAttClass.Size = New System.Drawing.Size(269, 20)
        Me.cboxAttClass.TabIndex = 6
        '
        'Label45
        '
        Me.Label45.AutoSize = True
        Me.Label45.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label45.Location = New System.Drawing.Point(24, 12)
        Me.Label45.Name = "Label45"
        Me.Label45.Size = New System.Drawing.Size(35, 12)
        Me.Label45.TabIndex = 5
        Me.Label45.Text = "班級: "
        Me.Label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tpgKBRec
        '
        Me.tpgKBRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgKBRec.Controls.Add(Me.GroupBox15)
        Me.tpgKBRec.Controls.Add(Me.GroupBox14)
        Me.tpgKBRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgKBRec.Name = "tpgKBRec"
        Me.tpgKBRec.Size = New System.Drawing.Size(1017, 570)
        Me.tpgKBRec.TabIndex = 2
        Me.tpgKBRec.Text = "退費紀錄"
        Me.tpgKBRec.UseVisualStyleBackColor = True
        '
        'GroupBox15
        '
        Me.GroupBox15.Controls.Add(Me.TabControl1)
        Me.GroupBox15.Location = New System.Drawing.Point(7, 208)
        Me.GroupBox15.Name = "GroupBox15"
        Me.GroupBox15.Size = New System.Drawing.Size(994, 341)
        Me.GroupBox15.TabIndex = 194
        Me.GroupBox15.TabStop = False
        '
        'TabControl1
        '
        Me.TabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.TabControl1.Controls.Add(Me.tpgKBPay)
        Me.TabControl1.Controls.Add(Me.tpgKBShowUp)
        Me.TabControl1.Controls.Add(Me.tpgKBBookRec)
        Me.TabControl1.Location = New System.Drawing.Point(6, 11)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(982, 324)
        Me.TabControl1.TabIndex = 0
        '
        'tpgKBPay
        '
        Me.tpgKBPay.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgKBPay.Controls.Add(Me.dgvKBPayRec)
        Me.tpgKBPay.Location = New System.Drawing.Point(4, 25)
        Me.tpgKBPay.Name = "tpgKBPay"
        Me.tpgKBPay.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgKBPay.Size = New System.Drawing.Size(974, 295)
        Me.tpgKBPay.TabIndex = 0
        Me.tpgKBPay.Text = "繳費紀錄"
        '
        'dgvKBPayRec
        '
        Me.dgvKBPayRec.AllowUserToAddRows = False
        Me.dgvKBPayRec.AllowUserToDeleteRows = False
        Me.dgvKBPayRec.AllowUserToResizeRows = False
        Me.dgvKBPayRec.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKBPayRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKBPayRec.Location = New System.Drawing.Point(7, 6)
        Me.dgvKBPayRec.MultiSelect = False
        Me.dgvKBPayRec.Name = "dgvKBPayRec"
        Me.dgvKBPayRec.ReadOnly = True
        Me.dgvKBPayRec.RowHeadersVisible = False
        Me.dgvKBPayRec.RowTemplate.Height = 24
        Me.dgvKBPayRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvKBPayRec.ShowCellToolTips = False
        Me.dgvKBPayRec.ShowEditingIcon = False
        Me.dgvKBPayRec.Size = New System.Drawing.Size(961, 284)
        Me.dgvKBPayRec.TabIndex = 1
        '
        'tpgKBShowUp
        '
        Me.tpgKBShowUp.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgKBShowUp.Controls.Add(Me.dgvKBAttendance)
        Me.tpgKBShowUp.Location = New System.Drawing.Point(4, 25)
        Me.tpgKBShowUp.Name = "tpgKBShowUp"
        Me.tpgKBShowUp.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgKBShowUp.Size = New System.Drawing.Size(974, 295)
        Me.tpgKBShowUp.TabIndex = 1
        Me.tpgKBShowUp.Text = "出勤紀錄"
        '
        'dgvKBAttendance
        '
        Me.dgvKBAttendance.AllowUserToAddRows = False
        Me.dgvKBAttendance.AllowUserToDeleteRows = False
        Me.dgvKBAttendance.AllowUserToResizeRows = False
        Me.dgvKBAttendance.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKBAttendance.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKBAttendance.Location = New System.Drawing.Point(7, 6)
        Me.dgvKBAttendance.MultiSelect = False
        Me.dgvKBAttendance.Name = "dgvKBAttendance"
        Me.dgvKBAttendance.ReadOnly = True
        Me.dgvKBAttendance.RowHeadersVisible = False
        Me.dgvKBAttendance.RowTemplate.Height = 24
        Me.dgvKBAttendance.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvKBAttendance.ShowCellToolTips = False
        Me.dgvKBAttendance.ShowEditingIcon = False
        Me.dgvKBAttendance.Size = New System.Drawing.Size(961, 284)
        Me.dgvKBAttendance.TabIndex = 2
        '
        'tpgKBBookRec
        '
        Me.tpgKBBookRec.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgKBBookRec.Controls.Add(Me.dgvKBBook)
        Me.tpgKBBookRec.Location = New System.Drawing.Point(4, 25)
        Me.tpgKBBookRec.Name = "tpgKBBookRec"
        Me.tpgKBBookRec.Size = New System.Drawing.Size(974, 295)
        Me.tpgKBBookRec.TabIndex = 2
        Me.tpgKBBookRec.Text = "領書記錄"
        '
        'dgvKBBook
        '
        Me.dgvKBBook.AllowUserToAddRows = False
        Me.dgvKBBook.AllowUserToDeleteRows = False
        Me.dgvKBBook.AllowUserToResizeRows = False
        Me.dgvKBBook.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKBBook.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKBBook.Location = New System.Drawing.Point(7, 6)
        Me.dgvKBBook.MultiSelect = False
        Me.dgvKBBook.Name = "dgvKBBook"
        Me.dgvKBBook.ReadOnly = True
        Me.dgvKBBook.RowHeadersVisible = False
        Me.dgvKBBook.RowTemplate.Height = 24
        Me.dgvKBBook.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvKBBook.ShowCellToolTips = False
        Me.dgvKBBook.ShowEditingIcon = False
        Me.dgvKBBook.Size = New System.Drawing.Size(961, 284)
        Me.dgvKBBook.TabIndex = 2
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.butKBDelete)
        Me.GroupBox14.Controls.Add(Me.butKBRemarks)
        Me.GroupBox14.Controls.Add(Me.butKBMod)
        Me.GroupBox14.Controls.Add(Me.dgvKB)
        Me.GroupBox14.Location = New System.Drawing.Point(7, 9)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(994, 193)
        Me.GroupBox14.TabIndex = 193
        Me.GroupBox14.TabStop = False
        Me.GroupBox14.Text = "所有已退費班級班級"
        '
        'butKBDelete
        '
        Me.butKBDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butKBDelete.Location = New System.Drawing.Point(237, 162)
        Me.butKBDelete.Name = "butKBDelete"
        Me.butKBDelete.Size = New System.Drawing.Size(94, 25)
        Me.butKBDelete.TabIndex = 199
        Me.butKBDelete.Text = "取消退費"
        Me.butKBDelete.UseVisualStyleBackColor = True
        '
        'butKBRemarks
        '
        Me.butKBRemarks.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butKBRemarks.Location = New System.Drawing.Point(122, 162)
        Me.butKBRemarks.Name = "butKBRemarks"
        Me.butKBRemarks.Size = New System.Drawing.Size(94, 25)
        Me.butKBRemarks.TabIndex = 198
        Me.butKBRemarks.Text = "輸入備註"
        Me.butKBRemarks.UseVisualStyleBackColor = True
        '
        'butKBMod
        '
        Me.butKBMod.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butKBMod.Location = New System.Drawing.Point(7, 162)
        Me.butKBMod.Name = "butKBMod"
        Me.butKBMod.Size = New System.Drawing.Size(94, 25)
        Me.butKBMod.TabIndex = 197
        Me.butKBMod.Text = "修改"
        Me.butKBMod.UseVisualStyleBackColor = True
        '
        'dgvKB
        '
        Me.dgvKB.AllowUserToAddRows = False
        Me.dgvKB.AllowUserToDeleteRows = False
        Me.dgvKB.AllowUserToResizeRows = False
        Me.dgvKB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvKB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvKB.Location = New System.Drawing.Point(7, 18)
        Me.dgvKB.MultiSelect = False
        Me.dgvKB.Name = "dgvKB"
        Me.dgvKB.ReadOnly = True
        Me.dgvKB.RowHeadersVisible = False
        Me.dgvKB.RowTemplate.Height = 24
        Me.dgvKB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvKB.ShowCellToolTips = False
        Me.dgvKB.ShowEditingIcon = False
        Me.dgvKB.Size = New System.Drawing.Size(971, 138)
        Me.dgvKB.TabIndex = 0
        '
        'tpgPay
        '
        Me.tpgPay.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgPay.Controls.Add(Me.chkboxNoAskPrintReceipt)
        Me.tpgPay.Controls.Add(Me.grpboxExtra)
        Me.tpgPay.Controls.Add(Me.butPayPrintOpt)
        Me.tpgPay.Controls.Add(Me.butPayAck)
        Me.tpgPay.Controls.Add(Me.butPayPrintTogether)
        Me.tpgPay.Controls.Add(Me.butPayTogether)
        Me.tpgPay.Controls.Add(Me.butPayOthers)
        Me.tpgPay.Controls.Add(Me.butPayPrint)
        Me.tpgPay.Controls.Add(Me.butPayKeep)
        Me.tpgPay.Controls.Add(Me.butPayBack)
        Me.tpgPay.Controls.Add(Me.butPayDelete)
        Me.tpgPay.Controls.Add(Me.butPayModify)
        Me.tpgPay.Controls.Add(Me.butPaySave)
        Me.tpgPay.Controls.Add(Me.GroupBox13)
        Me.tpgPay.Controls.Add(Me.GroupBox12)
        Me.tpgPay.Controls.Add(Me.GroupBox11)
        Me.tpgPay.Controls.Add(Me.GroupBox10)
        Me.tpgPay.Controls.Add(Me.GroupBox9)
        Me.tpgPay.Controls.Add(Me.GroupBox8)
        Me.tpgPay.Location = New System.Drawing.Point(4, 25)
        Me.tpgPay.Name = "tpgPay"
        Me.tpgPay.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgPay.Size = New System.Drawing.Size(1017, 570)
        Me.tpgPay.TabIndex = 1
        Me.tpgPay.Text = "繳費作業"
        Me.tpgPay.UseVisualStyleBackColor = True
        '
        'chkboxNoAskPrintReceipt
        '
        Me.chkboxNoAskPrintReceipt.AutoSize = True
        Me.chkboxNoAskPrintReceipt.Location = New System.Drawing.Point(841, 538)
        Me.chkboxNoAskPrintReceipt.Name = "chkboxNoAskPrintReceipt"
        Me.chkboxNoAskPrintReceipt.Size = New System.Drawing.Size(144, 16)
        Me.chkboxNoAskPrintReceipt.TabIndex = 208
        Me.chkboxNoAskPrintReceipt.Text = "不要問我是否列印收據"
        Me.chkboxNoAskPrintReceipt.UseVisualStyleBackColor = True
        '
        'grpboxExtra
        '
        Me.grpboxExtra.Controls.Add(Me.lblCheque)
        Me.grpboxExtra.Controls.Add(Me.dtpickCheque)
        Me.grpboxExtra.Controls.Add(Me.tboxPayExtraInfo)
        Me.grpboxExtra.ForeColor = System.Drawing.Color.Blue
        Me.grpboxExtra.Location = New System.Drawing.Point(801, 6)
        Me.grpboxExtra.Name = "grpboxExtra"
        Me.grpboxExtra.Size = New System.Drawing.Size(200, 65)
        Me.grpboxExtra.TabIndex = 207
        Me.grpboxExtra.TabStop = False
        Me.grpboxExtra.Text = "支票號碼"
        '
        'lblCheque
        '
        Me.lblCheque.AutoSize = True
        Me.lblCheque.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCheque.Location = New System.Drawing.Point(16, 43)
        Me.lblCheque.Name = "lblCheque"
        Me.lblCheque.Size = New System.Drawing.Size(47, 12)
        Me.lblCheque.TabIndex = 60
        Me.lblCheque.Text = "到期日: "
        Me.lblCheque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpickCheque
        '
        Me.dtpickCheque.Location = New System.Drawing.Point(65, 39)
        Me.dtpickCheque.Name = "dtpickCheque"
        Me.dtpickCheque.Size = New System.Drawing.Size(119, 22)
        Me.dtpickCheque.TabIndex = 59
        '
        'tboxPayExtraInfo
        '
        Me.tboxPayExtraInfo.Location = New System.Drawing.Point(18, 14)
        Me.tboxPayExtraInfo.Name = "tboxPayExtraInfo"
        Me.tboxPayExtraInfo.Size = New System.Drawing.Size(166, 22)
        Me.tboxPayExtraInfo.TabIndex = 5
        '
        'butPayPrintOpt
        '
        Me.butPayPrintOpt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayPrintOpt.Location = New System.Drawing.Point(749, 533)
        Me.butPayPrintOpt.Name = "butPayPrintOpt"
        Me.butPayPrintOpt.Size = New System.Drawing.Size(76, 25)
        Me.butPayPrintOpt.TabIndex = 205
        Me.butPayPrintOpt.Text = "列印選項"
        Me.butPayPrintOpt.UseVisualStyleBackColor = True
        '
        'butPayAck
        '
        Me.butPayAck.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayAck.Location = New System.Drawing.Point(667, 533)
        Me.butPayAck.Name = "butPayAck"
        Me.butPayAck.Size = New System.Drawing.Size(76, 25)
        Me.butPayAck.TabIndex = 204
        Me.butPayAck.Text = "繳費通知"
        Me.butPayAck.UseVisualStyleBackColor = True
        '
        'butPayPrintTogether
        '
        Me.butPayPrintTogether.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayPrintTogether.Location = New System.Drawing.Point(585, 533)
        Me.butPayPrintTogether.Name = "butPayPrintTogether"
        Me.butPayPrintTogether.Size = New System.Drawing.Size(76, 25)
        Me.butPayPrintTogether.TabIndex = 203
        Me.butPayPrintTogether.Text = "合併列印"
        Me.butPayPrintTogether.UseVisualStyleBackColor = True
        '
        'butPayTogether
        '
        Me.butPayTogether.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayTogether.Location = New System.Drawing.Point(503, 533)
        Me.butPayTogether.Name = "butPayTogether"
        Me.butPayTogether.Size = New System.Drawing.Size(76, 25)
        Me.butPayTogether.TabIndex = 202
        Me.butPayTogether.Text = "合併繳費"
        Me.butPayTogether.UseVisualStyleBackColor = True
        '
        'butPayOthers
        '
        Me.butPayOthers.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayOthers.Location = New System.Drawing.Point(421, 533)
        Me.butPayOthers.Name = "butPayOthers"
        Me.butPayOthers.Size = New System.Drawing.Size(76, 25)
        Me.butPayOthers.TabIndex = 201
        Me.butPayOthers.Text = "繳其他費用"
        Me.butPayOthers.UseVisualStyleBackColor = True
        Me.butPayOthers.Visible = False
        '
        'butPayPrint
        '
        Me.butPayPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayPrint.Location = New System.Drawing.Point(290, 533)
        Me.butPayPrint.Name = "butPayPrint"
        Me.butPayPrint.Size = New System.Drawing.Size(65, 25)
        Me.butPayPrint.TabIndex = 200
        Me.butPayPrint.Text = "列印"
        Me.butPayPrint.UseVisualStyleBackColor = True
        '
        'butPayKeep
        '
        Me.butPayKeep.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayKeep.Location = New System.Drawing.Point(356, 533)
        Me.butPayKeep.Name = "butPayKeep"
        Me.butPayKeep.Size = New System.Drawing.Size(65, 25)
        Me.butPayKeep.TabIndex = 199
        Me.butPayKeep.Text = "保留"
        Me.butPayKeep.UseVisualStyleBackColor = True
        Me.butPayKeep.Visible = False
        '
        'butPayBack
        '
        Me.butPayBack.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayBack.Location = New System.Drawing.Point(220, 533)
        Me.butPayBack.Name = "butPayBack"
        Me.butPayBack.Size = New System.Drawing.Size(65, 25)
        Me.butPayBack.TabIndex = 198
        Me.butPayBack.Text = "退費"
        Me.butPayBack.UseVisualStyleBackColor = True
        '
        'butPayDelete
        '
        Me.butPayDelete.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayDelete.Location = New System.Drawing.Point(149, 533)
        Me.butPayDelete.Name = "butPayDelete"
        Me.butPayDelete.Size = New System.Drawing.Size(65, 25)
        Me.butPayDelete.TabIndex = 197
        Me.butPayDelete.Text = "刪除"
        Me.butPayDelete.UseVisualStyleBackColor = True
        '
        'butPayModify
        '
        Me.butPayModify.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPayModify.Location = New System.Drawing.Point(78, 533)
        Me.butPayModify.Name = "butPayModify"
        Me.butPayModify.Size = New System.Drawing.Size(65, 25)
        Me.butPayModify.TabIndex = 196
        Me.butPayModify.Text = "修改"
        Me.butPayModify.UseVisualStyleBackColor = True
        '
        'butPaySave
        '
        Me.butPaySave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPaySave.Location = New System.Drawing.Point(9, 533)
        Me.butPaySave.Name = "butPaySave"
        Me.butPaySave.Size = New System.Drawing.Size(65, 25)
        Me.butPaySave.TabIndex = 195
        Me.butPaySave.Text = "儲存"
        Me.butPaySave.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.btnDiscCancel)
        Me.GroupBox13.Controls.Add(Me.butModDisc)
        Me.GroupBox13.Controls.Add(Me.tboxReceiptRemarks)
        Me.GroupBox13.Controls.Add(Me.butReceiptRemarks)
        Me.GroupBox13.Controls.Add(Me.Label42)
        Me.GroupBox13.Controls.Add(Me.tboxDiscountRemarks)
        Me.GroupBox13.Controls.Add(Me.butDiscountRemarks)
        Me.GroupBox13.Controls.Add(Me.Label41)
        Me.GroupBox13.Controls.Add(Me.tboxDiscount)
        Me.GroupBox13.Controls.Add(Me.Label2)
        Me.GroupBox13.Location = New System.Drawing.Point(7, 252)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(994, 84)
        Me.GroupBox13.TabIndex = 194
        Me.GroupBox13.TabStop = False
        '
        'btnDiscCancel
        '
        Me.btnDiscCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.btnDiscCancel.Location = New System.Drawing.Point(221, 11)
        Me.btnDiscCancel.Name = "btnDiscCancel"
        Me.btnDiscCancel.Size = New System.Drawing.Size(65, 26)
        Me.btnDiscCancel.TabIndex = 203
        Me.btnDiscCancel.Text = "取消優待"
        Me.btnDiscCancel.UseVisualStyleBackColor = True
        '
        'butModDisc
        '
        Me.butModDisc.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butModDisc.Location = New System.Drawing.Point(150, 11)
        Me.butModDisc.Name = "butModDisc"
        Me.butModDisc.Size = New System.Drawing.Size(65, 26)
        Me.butModDisc.TabIndex = 202
        Me.butModDisc.Text = "優改優待"
        Me.butModDisc.UseVisualStyleBackColor = True
        '
        'tboxReceiptRemarks
        '
        Me.tboxReceiptRemarks.Location = New System.Drawing.Point(150, 47)
        Me.tboxReceiptRemarks.Name = "tboxReceiptRemarks"
        Me.tboxReceiptRemarks.Size = New System.Drawing.Size(828, 22)
        Me.tboxReceiptRemarks.TabIndex = 200
        '
        'butReceiptRemarks
        '
        Me.butReceiptRemarks.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butReceiptRemarks.Location = New System.Drawing.Point(76, 47)
        Me.butReceiptRemarks.Name = "butReceiptRemarks"
        Me.butReceiptRemarks.Size = New System.Drawing.Size(65, 26)
        Me.butReceiptRemarks.TabIndex = 199
        Me.butReceiptRemarks.Text = "常用詞庫"
        Me.butReceiptRemarks.UseVisualStyleBackColor = True
        '
        'Label42
        '
        Me.Label42.AutoSize = True
        Me.Label42.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label42.Location = New System.Drawing.Point(17, 52)
        Me.Label42.Name = "Label42"
        Me.Label42.Size = New System.Drawing.Size(59, 12)
        Me.Label42.TabIndex = 198
        Me.Label42.Text = "收據備註: "
        Me.Label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxDiscountRemarks
        '
        Me.tboxDiscountRemarks.Location = New System.Drawing.Point(442, 13)
        Me.tboxDiscountRemarks.Multiline = True
        Me.tboxDiscountRemarks.Name = "tboxDiscountRemarks"
        Me.tboxDiscountRemarks.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.tboxDiscountRemarks.Size = New System.Drawing.Size(536, 22)
        Me.tboxDiscountRemarks.TabIndex = 197
        '
        'butDiscountRemarks
        '
        Me.butDiscountRemarks.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDiscountRemarks.Location = New System.Drawing.Point(371, 11)
        Me.butDiscountRemarks.Name = "butDiscountRemarks"
        Me.butDiscountRemarks.Size = New System.Drawing.Size(65, 26)
        Me.butDiscountRemarks.TabIndex = 196
        Me.butDiscountRemarks.Text = "常用詞庫"
        Me.butDiscountRemarks.UseVisualStyleBackColor = True
        '
        'Label41
        '
        Me.Label41.AutoSize = True
        Me.Label41.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label41.Location = New System.Drawing.Point(292, 18)
        Me.Label41.Name = "Label41"
        Me.Label41.Size = New System.Drawing.Size(83, 12)
        Me.Label41.TabIndex = 6
        Me.Label41.Text = "學費優待備註: "
        Me.Label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxDiscount
        '
        Me.tboxDiscount.Location = New System.Drawing.Point(76, 13)
        Me.tboxDiscount.Name = "tboxDiscount"
        Me.tboxDiscount.Size = New System.Drawing.Size(65, 22)
        Me.tboxDiscount.TabIndex = 5
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(17, 18)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(59, 12)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "學費優待: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.dgvPayDetails)
        Me.GroupBox12.Location = New System.Drawing.Point(7, 342)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(994, 185)
        Me.GroupBox12.TabIndex = 193
        Me.GroupBox12.TabStop = False
        Me.GroupBox12.Text = "繳費明細"
        '
        'dgvPayDetails
        '
        Me.dgvPayDetails.AllowUserToAddRows = False
        Me.dgvPayDetails.AllowUserToDeleteRows = False
        Me.dgvPayDetails.AllowUserToResizeRows = False
        Me.dgvPayDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPayDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPayDetails.Location = New System.Drawing.Point(7, 18)
        Me.dgvPayDetails.MultiSelect = False
        Me.dgvPayDetails.Name = "dgvPayDetails"
        Me.dgvPayDetails.ReadOnly = True
        Me.dgvPayDetails.RowHeadersVisible = False
        Me.dgvPayDetails.RowTemplate.Height = 24
        Me.dgvPayDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPayDetails.Size = New System.Drawing.Size(971, 161)
        Me.dgvPayDetails.TabIndex = 0
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.chkboxShowPast2)
        Me.GroupBox11.Controls.Add(Me.dgvClass2)
        Me.GroupBox11.Location = New System.Drawing.Point(7, 73)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(994, 180)
        Me.GroupBox11.TabIndex = 192
        Me.GroupBox11.TabStop = False
        Me.GroupBox11.Text = "報名班級"
        '
        'chkboxShowPast2
        '
        Me.chkboxShowPast2.AutoSize = True
        Me.chkboxShowPast2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast2.Location = New System.Drawing.Point(100, 0)
        Me.chkboxShowPast2.Name = "chkboxShowPast2"
        Me.chkboxShowPast2.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast2.TabIndex = 1
        Me.chkboxShowPast2.Text = "顯示過時班級"
        Me.chkboxShowPast2.UseVisualStyleBackColor = True
        '
        'dgvClass2
        '
        Me.dgvClass2.AllowUserToAddRows = False
        Me.dgvClass2.AllowUserToDeleteRows = False
        Me.dgvClass2.AllowUserToResizeRows = False
        Me.dgvClass2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClass2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClass2.Location = New System.Drawing.Point(7, 16)
        Me.dgvClass2.MultiSelect = False
        Me.dgvClass2.Name = "dgvClass2"
        Me.dgvClass2.ReadOnly = True
        Me.dgvClass2.RowHeadersVisible = False
        DataGridViewCellStyle1.NullValue = Nothing
        Me.dgvClass2.RowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvClass2.RowTemplate.Height = 24
        Me.dgvClass2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClass2.ShowCellToolTips = False
        Me.dgvClass2.ShowEditingIcon = False
        Me.dgvClass2.Size = New System.Drawing.Size(971, 158)
        Me.dgvClass2.TabIndex = 0
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.radbutMethod5)
        Me.GroupBox10.Controls.Add(Me.radbutMethod4)
        Me.GroupBox10.Controls.Add(Me.radbutMethod3)
        Me.GroupBox10.Controls.Add(Me.radbutMethod2)
        Me.GroupBox10.Controls.Add(Me.radbutMethod1)
        Me.GroupBox10.Controls.Add(Me.tboxPayToday)
        Me.GroupBox10.Controls.Add(Me.Label50)
        Me.GroupBox10.Location = New System.Drawing.Point(487, 6)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(308, 65)
        Me.GroupBox10.TabIndex = 189
        Me.GroupBox10.TabStop = False
        '
        'radbutMethod5
        '
        Me.radbutMethod5.AutoSize = True
        Me.radbutMethod5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMethod5.Location = New System.Drawing.Point(238, 40)
        Me.radbutMethod5.Name = "radbutMethod5"
        Me.radbutMethod5.Size = New System.Drawing.Size(59, 16)
        Me.radbutMethod5.TabIndex = 11
        Me.radbutMethod5.Text = "保留單"
        Me.radbutMethod5.UseVisualStyleBackColor = True
        Me.radbutMethod5.Visible = False
        '
        'radbutMethod4
        '
        Me.radbutMethod4.AutoSize = True
        Me.radbutMethod4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMethod4.Location = New System.Drawing.Point(173, 40)
        Me.radbutMethod4.Name = "radbutMethod4"
        Me.radbutMethod4.Size = New System.Drawing.Size(47, 16)
        Me.radbutMethod4.TabIndex = 10
        Me.radbutMethod4.Text = "其他"
        Me.radbutMethod4.UseVisualStyleBackColor = True
        '
        'radbutMethod3
        '
        Me.radbutMethod3.AutoSize = True
        Me.radbutMethod3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMethod3.Location = New System.Drawing.Point(120, 40)
        Me.radbutMethod3.Name = "radbutMethod3"
        Me.radbutMethod3.Size = New System.Drawing.Size(47, 16)
        Me.radbutMethod3.TabIndex = 9
        Me.radbutMethod3.Text = "匯款"
        Me.radbutMethod3.UseVisualStyleBackColor = True
        '
        'radbutMethod2
        '
        Me.radbutMethod2.AutoSize = True
        Me.radbutMethod2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMethod2.Location = New System.Drawing.Point(67, 40)
        Me.radbutMethod2.Name = "radbutMethod2"
        Me.radbutMethod2.Size = New System.Drawing.Size(47, 16)
        Me.radbutMethod2.TabIndex = 8
        Me.radbutMethod2.Text = "支票"
        Me.radbutMethod2.UseVisualStyleBackColor = True
        '
        'radbutMethod1
        '
        Me.radbutMethod1.AutoSize = True
        Me.radbutMethod1.Checked = True
        Me.radbutMethod1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMethod1.Location = New System.Drawing.Point(14, 40)
        Me.radbutMethod1.Name = "radbutMethod1"
        Me.radbutMethod1.Size = New System.Drawing.Size(47, 16)
        Me.radbutMethod1.TabIndex = 7
        Me.radbutMethod1.TabStop = True
        Me.radbutMethod1.Text = "現金"
        Me.radbutMethod1.UseVisualStyleBackColor = True
        '
        'tboxPayToday
        '
        Me.tboxPayToday.Location = New System.Drawing.Point(76, 12)
        Me.tboxPayToday.Name = "tboxPayToday"
        Me.tboxPayToday.Size = New System.Drawing.Size(104, 22)
        Me.tboxPayToday.TabIndex = 5
        '
        'Label50
        '
        Me.Label50.AutoSize = True
        Me.Label50.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label50.Location = New System.Drawing.Point(17, 15)
        Me.Label50.Name = "Label50"
        Me.Label50.Size = New System.Drawing.Size(59, 12)
        Me.Label50.TabIndex = 4
        Me.Label50.Text = "今日繳費: "
        Me.Label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.dtpickMakeupDate)
        Me.GroupBox9.Controls.Add(Me.radbutNormal)
        Me.GroupBox9.Controls.Add(Me.radbutMakeup)
        Me.GroupBox9.Location = New System.Drawing.Point(241, 6)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(238, 65)
        Me.GroupBox9.TabIndex = 189
        Me.GroupBox9.TabStop = False
        '
        'dtpickMakeupDate
        '
        Me.dtpickMakeupDate.Checked = False
        Me.dtpickMakeupDate.Location = New System.Drawing.Point(90, 32)
        Me.dtpickMakeupDate.Name = "dtpickMakeupDate"
        Me.dtpickMakeupDate.Size = New System.Drawing.Size(119, 22)
        Me.dtpickMakeupDate.TabIndex = 57
        Me.dtpickMakeupDate.Value = New Date(2010, 6, 4, 0, 0, 0, 0)
        '
        'radbutNormal
        '
        Me.radbutNormal.AutoSize = True
        Me.radbutNormal.Checked = True
        Me.radbutNormal.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutNormal.Location = New System.Drawing.Point(13, 11)
        Me.radbutNormal.Name = "radbutNormal"
        Me.radbutNormal.Size = New System.Drawing.Size(71, 16)
        Me.radbutNormal.TabIndex = 8
        Me.radbutNormal.TabStop = True
        Me.radbutNormal.Text = "一般收據"
        Me.radbutNormal.UseVisualStyleBackColor = True
        '
        'radbutMakeup
        '
        Me.radbutMakeup.AutoSize = True
        Me.radbutMakeup.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutMakeup.Location = New System.Drawing.Point(13, 38)
        Me.radbutMakeup.Name = "radbutMakeup"
        Me.radbutMakeup.Size = New System.Drawing.Size(71, 16)
        Me.radbutMakeup.TabIndex = 7
        Me.radbutMakeup.Text = "補開收據"
        Me.radbutMakeup.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.tboxReceiptNum)
        Me.GroupBox8.Controls.Add(Me.Label4)
        Me.GroupBox8.Location = New System.Drawing.Point(7, 6)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(228, 65)
        Me.GroupBox8.TabIndex = 188
        Me.GroupBox8.TabStop = False
        '
        'tboxReceiptNum
        '
        Me.tboxReceiptNum.Location = New System.Drawing.Point(76, 21)
        Me.tboxReceiptNum.Name = "tboxReceiptNum"
        Me.tboxReceiptNum.Size = New System.Drawing.Size(104, 22)
        Me.tboxReceiptNum.TabIndex = 5
        Me.tboxReceiptNum.Text = "0980000888"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(17, 24)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "收據編號: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tpgInfo
        '
        Me.tpgInfo.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpgInfo.Controls.Add(Me.lblImgInfo)
        Me.tpgInfo.Controls.Add(Me.GroupBox6)
        Me.tpgInfo.Controls.Add(Me.butDelPic)
        Me.tpgInfo.Controls.Add(Me.butAddPic)
        Me.tpgInfo.Controls.Add(Me.butSavePic)
        Me.tpgInfo.Controls.Add(Me.butWebcam)
        Me.tpgInfo.Controls.Add(Me.butChangeId)
        Me.tpgInfo.Controls.Add(Me.GroupBox3)
        Me.tpgInfo.Controls.Add(Me.butPrint)
        Me.tpgInfo.Controls.Add(Me.butNext)
        Me.tpgInfo.Controls.Add(Me.GroupBox5)
        Me.tpgInfo.Controls.Add(Me.butPrevious)
        Me.tpgInfo.Controls.Add(Me.GroupBox7)
        Me.tpgInfo.Controls.Add(Me.butSearch)
        Me.tpgInfo.Controls.Add(Me.butSave)
        Me.tpgInfo.Controls.Add(Me.picbox)
        Me.tpgInfo.Controls.Add(Me.GroupBox2)
        Me.tpgInfo.Controls.Add(Me.GroupBox1)
        Me.tpgInfo.Controls.Add(Me.GroupBox4)
        Me.tpgInfo.Location = New System.Drawing.Point(4, 25)
        Me.tpgInfo.Name = "tpgInfo"
        Me.tpgInfo.Padding = New System.Windows.Forms.Padding(3)
        Me.tpgInfo.Size = New System.Drawing.Size(1017, 570)
        Me.tpgInfo.TabIndex = 0
        Me.tpgInfo.Text = "學生資料"
        Me.tpgInfo.UseVisualStyleBackColor = True
        '
        'lblImgInfo
        '
        Me.lblImgInfo.AutoSize = True
        Me.lblImgInfo.Location = New System.Drawing.Point(836, 117)
        Me.lblImgInfo.Name = "lblImgInfo"
        Me.lblImgInfo.Size = New System.Drawing.Size(0, 12)
        Me.lblImgInfo.TabIndex = 207
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Panel1)
        Me.GroupBox6.Location = New System.Drawing.Point(705, 268)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(305, 162)
        Me.GroupBox6.TabIndex = 205
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "家庭狀況"
        '
        'Panel1
        '
        Me.Panel1.AutoScroll = True
        Me.Panel1.Controls.Add(Me.tboxRemark4)
        Me.Panel1.Controls.Add(Me.tboxRemark3)
        Me.Panel1.Controls.Add(Me.tboxRemark2)
        Me.Panel1.Controls.Add(Me.tboxRemark1)
        Me.Panel1.Controls.Add(Me.tboxRank4)
        Me.Panel1.Controls.Add(Me.tboxRank3)
        Me.Panel1.Controls.Add(Me.tboxRank2)
        Me.Panel1.Controls.Add(Me.tboxRank1)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.tboxSchool4)
        Me.Panel1.Controls.Add(Me.cboxFamily4)
        Me.Panel1.Controls.Add(Me.tboxBirth4d)
        Me.Panel1.Controls.Add(Me.tboxBirth4m)
        Me.Panel1.Controls.Add(Me.tboxBirth4y)
        Me.Panel1.Controls.Add(Me.tboxName4)
        Me.Panel1.Controls.Add(Me.tboxSchool3)
        Me.Panel1.Controls.Add(Me.cboxFamily3)
        Me.Panel1.Controls.Add(Me.tboxBirth3d)
        Me.Panel1.Controls.Add(Me.tboxBirth3m)
        Me.Panel1.Controls.Add(Me.tboxBirth3y)
        Me.Panel1.Controls.Add(Me.tboxName3)
        Me.Panel1.Controls.Add(Me.tboxSchool2)
        Me.Panel1.Controls.Add(Me.cboxFamily2)
        Me.Panel1.Controls.Add(Me.tboxBirth2d)
        Me.Panel1.Controls.Add(Me.tboxBirth2m)
        Me.Panel1.Controls.Add(Me.tboxBirth2y)
        Me.Panel1.Controls.Add(Me.tboxName2)
        Me.Panel1.Controls.Add(Me.tboxSchool1)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.cboxFamily1)
        Me.Panel1.Controls.Add(Me.tboxBirth1d)
        Me.Panel1.Controls.Add(Me.tboxBirth1m)
        Me.Panel1.Controls.Add(Me.tboxBirth1y)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Controls.Add(Me.tboxName1)
        Me.Panel1.Controls.Add(Me.Label43)
        Me.Panel1.Controls.Add(Me.Label44)
        Me.Panel1.Location = New System.Drawing.Point(6, 15)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(307, 141)
        Me.Panel1.TabIndex = 45
        '
        'tboxRemark4
        '
        Me.tboxRemark4.Location = New System.Drawing.Point(327, 88)
        Me.tboxRemark4.Name = "tboxRemark4"
        Me.tboxRemark4.Size = New System.Drawing.Size(196, 22)
        Me.tboxRemark4.TabIndex = 72
        '
        'tboxRemark3
        '
        Me.tboxRemark3.Location = New System.Drawing.Point(327, 65)
        Me.tboxRemark3.Name = "tboxRemark3"
        Me.tboxRemark3.Size = New System.Drawing.Size(196, 22)
        Me.tboxRemark3.TabIndex = 71
        '
        'tboxRemark2
        '
        Me.tboxRemark2.Location = New System.Drawing.Point(327, 43)
        Me.tboxRemark2.Name = "tboxRemark2"
        Me.tboxRemark2.Size = New System.Drawing.Size(196, 22)
        Me.tboxRemark2.TabIndex = 70
        '
        'tboxRemark1
        '
        Me.tboxRemark1.Location = New System.Drawing.Point(327, 20)
        Me.tboxRemark1.Name = "tboxRemark1"
        Me.tboxRemark1.Size = New System.Drawing.Size(196, 22)
        Me.tboxRemark1.TabIndex = 69
        '
        'tboxRank4
        '
        Me.tboxRank4.Location = New System.Drawing.Point(297, 88)
        Me.tboxRank4.Name = "tboxRank4"
        Me.tboxRank4.Size = New System.Drawing.Size(28, 22)
        Me.tboxRank4.TabIndex = 68
        Me.tboxRank4.Visible = False
        '
        'tboxRank3
        '
        Me.tboxRank3.Location = New System.Drawing.Point(297, 65)
        Me.tboxRank3.Name = "tboxRank3"
        Me.tboxRank3.Size = New System.Drawing.Size(28, 22)
        Me.tboxRank3.TabIndex = 67
        Me.tboxRank3.Visible = False
        '
        'tboxRank2
        '
        Me.tboxRank2.Location = New System.Drawing.Point(297, 43)
        Me.tboxRank2.Name = "tboxRank2"
        Me.tboxRank2.Size = New System.Drawing.Size(28, 22)
        Me.tboxRank2.TabIndex = 66
        Me.tboxRank2.Visible = False
        '
        'tboxRank1
        '
        Me.tboxRank1.Location = New System.Drawing.Point(297, 20)
        Me.tboxRank1.Name = "tboxRank1"
        Me.tboxRank1.Size = New System.Drawing.Size(28, 22)
        Me.tboxRank1.TabIndex = 65
        Me.tboxRank1.Visible = False
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label37.Location = New System.Drawing.Point(379, 7)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(29, 12)
        Me.Label37.TabIndex = 64
        Me.Label37.Text = "備註"
        Me.Label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label38.Location = New System.Drawing.Point(295, 5)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(29, 12)
        Me.Label38.TabIndex = 63
        Me.Label38.Text = "排行"
        Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label38.Visible = False
        '
        'tboxSchool4
        '
        Me.tboxSchool4.Location = New System.Drawing.Point(210, 90)
        Me.tboxSchool4.Name = "tboxSchool4"
        Me.tboxSchool4.Size = New System.Drawing.Size(81, 22)
        Me.tboxSchool4.TabIndex = 62
        '
        'cboxFamily4
        '
        Me.cboxFamily4.FormattingEnabled = True
        Me.cboxFamily4.Location = New System.Drawing.Point(8, 92)
        Me.cboxFamily4.Name = "cboxFamily4"
        Me.cboxFamily4.Size = New System.Drawing.Size(42, 20)
        Me.cboxFamily4.TabIndex = 61
        '
        'tboxBirth4d
        '
        Me.tboxBirth4d.Location = New System.Drawing.Point(176, 92)
        Me.tboxBirth4d.Name = "tboxBirth4d"
        Me.tboxBirth4d.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth4d.TabIndex = 60
        '
        'tboxBirth4m
        '
        Me.tboxBirth4m.Location = New System.Drawing.Point(144, 92)
        Me.tboxBirth4m.Name = "tboxBirth4m"
        Me.tboxBirth4m.Size = New System.Drawing.Size(26, 22)
        Me.tboxBirth4m.TabIndex = 59
        '
        'tboxBirth4y
        '
        Me.tboxBirth4y.Location = New System.Drawing.Point(110, 92)
        Me.tboxBirth4y.Name = "tboxBirth4y"
        Me.tboxBirth4y.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth4y.TabIndex = 58
        '
        'tboxName4
        '
        Me.tboxName4.Location = New System.Drawing.Point(56, 92)
        Me.tboxName4.Name = "tboxName4"
        Me.tboxName4.Size = New System.Drawing.Size(48, 22)
        Me.tboxName4.TabIndex = 57
        '
        'tboxSchool3
        '
        Me.tboxSchool3.Location = New System.Drawing.Point(210, 67)
        Me.tboxSchool3.Name = "tboxSchool3"
        Me.tboxSchool3.Size = New System.Drawing.Size(81, 22)
        Me.tboxSchool3.TabIndex = 56
        '
        'cboxFamily3
        '
        Me.cboxFamily3.FormattingEnabled = True
        Me.cboxFamily3.Location = New System.Drawing.Point(8, 69)
        Me.cboxFamily3.Name = "cboxFamily3"
        Me.cboxFamily3.Size = New System.Drawing.Size(42, 20)
        Me.cboxFamily3.TabIndex = 55
        '
        'tboxBirth3d
        '
        Me.tboxBirth3d.Location = New System.Drawing.Point(176, 69)
        Me.tboxBirth3d.Name = "tboxBirth3d"
        Me.tboxBirth3d.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth3d.TabIndex = 54
        '
        'tboxBirth3m
        '
        Me.tboxBirth3m.Location = New System.Drawing.Point(144, 69)
        Me.tboxBirth3m.Name = "tboxBirth3m"
        Me.tboxBirth3m.Size = New System.Drawing.Size(26, 22)
        Me.tboxBirth3m.TabIndex = 53
        '
        'tboxBirth3y
        '
        Me.tboxBirth3y.Location = New System.Drawing.Point(110, 69)
        Me.tboxBirth3y.Name = "tboxBirth3y"
        Me.tboxBirth3y.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth3y.TabIndex = 52
        '
        'tboxName3
        '
        Me.tboxName3.Location = New System.Drawing.Point(56, 69)
        Me.tboxName3.Name = "tboxName3"
        Me.tboxName3.Size = New System.Drawing.Size(48, 22)
        Me.tboxName3.TabIndex = 51
        '
        'tboxSchool2
        '
        Me.tboxSchool2.Location = New System.Drawing.Point(210, 45)
        Me.tboxSchool2.Name = "tboxSchool2"
        Me.tboxSchool2.Size = New System.Drawing.Size(81, 22)
        Me.tboxSchool2.TabIndex = 50
        '
        'cboxFamily2
        '
        Me.cboxFamily2.FormattingEnabled = True
        Me.cboxFamily2.Location = New System.Drawing.Point(8, 47)
        Me.cboxFamily2.Name = "cboxFamily2"
        Me.cboxFamily2.Size = New System.Drawing.Size(42, 20)
        Me.cboxFamily2.TabIndex = 49
        '
        'tboxBirth2d
        '
        Me.tboxBirth2d.Location = New System.Drawing.Point(176, 47)
        Me.tboxBirth2d.Name = "tboxBirth2d"
        Me.tboxBirth2d.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth2d.TabIndex = 48
        '
        'tboxBirth2m
        '
        Me.tboxBirth2m.Location = New System.Drawing.Point(144, 47)
        Me.tboxBirth2m.Name = "tboxBirth2m"
        Me.tboxBirth2m.Size = New System.Drawing.Size(26, 22)
        Me.tboxBirth2m.TabIndex = 47
        '
        'tboxBirth2y
        '
        Me.tboxBirth2y.Location = New System.Drawing.Point(110, 47)
        Me.tboxBirth2y.Name = "tboxBirth2y"
        Me.tboxBirth2y.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth2y.TabIndex = 46
        '
        'tboxName2
        '
        Me.tboxName2.Location = New System.Drawing.Point(56, 47)
        Me.tboxName2.Name = "tboxName2"
        Me.tboxName2.Size = New System.Drawing.Size(48, 22)
        Me.tboxName2.TabIndex = 45
        '
        'tboxSchool1
        '
        Me.tboxSchool1.Location = New System.Drawing.Point(210, 22)
        Me.tboxSchool1.Name = "tboxSchool1"
        Me.tboxSchool1.Size = New System.Drawing.Size(81, 22)
        Me.tboxSchool1.TabIndex = 44
        '
        'Label40
        '
        Me.Label40.AutoSize = True
        Me.Label40.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label40.Location = New System.Drawing.Point(236, 7)
        Me.Label40.Name = "Label40"
        Me.Label40.Size = New System.Drawing.Size(29, 12)
        Me.Label40.TabIndex = 43
        Me.Label40.Text = "學校"
        Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxFamily1
        '
        Me.cboxFamily1.FormattingEnabled = True
        Me.cboxFamily1.Location = New System.Drawing.Point(8, 24)
        Me.cboxFamily1.Name = "cboxFamily1"
        Me.cboxFamily1.Size = New System.Drawing.Size(42, 20)
        Me.cboxFamily1.TabIndex = 42
        '
        'tboxBirth1d
        '
        Me.tboxBirth1d.Location = New System.Drawing.Point(176, 24)
        Me.tboxBirth1d.Name = "tboxBirth1d"
        Me.tboxBirth1d.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth1d.TabIndex = 41
        '
        'tboxBirth1m
        '
        Me.tboxBirth1m.Location = New System.Drawing.Point(144, 24)
        Me.tboxBirth1m.Name = "tboxBirth1m"
        Me.tboxBirth1m.Size = New System.Drawing.Size(26, 22)
        Me.tboxBirth1m.TabIndex = 40
        '
        'tboxBirth1y
        '
        Me.tboxBirth1y.Location = New System.Drawing.Point(110, 24)
        Me.tboxBirth1y.Name = "tboxBirth1y"
        Me.tboxBirth1y.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirth1y.TabIndex = 39
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label39.Location = New System.Drawing.Point(120, 7)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(32, 12)
        Me.Label39.TabIndex = 38
        Me.Label39.Text = "生日 "
        Me.Label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxName1
        '
        Me.tboxName1.Location = New System.Drawing.Point(56, 24)
        Me.tboxName1.Name = "tboxName1"
        Me.tboxName1.Size = New System.Drawing.Size(48, 22)
        Me.tboxName1.TabIndex = 37
        '
        'Label43
        '
        Me.Label43.AutoSize = True
        Me.Label43.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label43.Location = New System.Drawing.Point(54, 7)
        Me.Label43.Name = "Label43"
        Me.Label43.Size = New System.Drawing.Size(29, 12)
        Me.Label43.TabIndex = 36
        Me.Label43.Text = "姓名"
        Me.Label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label44
        '
        Me.Label44.AutoSize = True
        Me.Label44.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label44.Location = New System.Drawing.Point(3, 10)
        Me.Label44.Name = "Label44"
        Me.Label44.Size = New System.Drawing.Size(32, 12)
        Me.Label44.TabIndex = 35
        Me.Label44.Text = "稱謂 "
        Me.Label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butDelPic
        '
        Me.butDelPic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelPic.Location = New System.Drawing.Point(951, 233)
        Me.butDelPic.Name = "butDelPic"
        Me.butDelPic.Size = New System.Drawing.Size(46, 26)
        Me.butDelPic.TabIndex = 204
        Me.butDelPic.Text = "刪除"
        Me.butDelPic.UseVisualStyleBackColor = True
        '
        'butAddPic
        '
        Me.butAddPic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAddPic.Location = New System.Drawing.Point(870, 233)
        Me.butAddPic.Name = "butAddPic"
        Me.butAddPic.Size = New System.Drawing.Size(75, 26)
        Me.butAddPic.TabIndex = 203
        Me.butAddPic.Text = "從檔案加入"
        Me.butAddPic.UseVisualStyleBackColor = True
        '
        'butSavePic
        '
        Me.butSavePic.Enabled = False
        Me.butSavePic.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSavePic.Location = New System.Drawing.Point(813, 233)
        Me.butSavePic.Name = "butSavePic"
        Me.butSavePic.Size = New System.Drawing.Size(52, 26)
        Me.butSavePic.TabIndex = 202
        Me.butSavePic.Text = "儲存"
        Me.butSavePic.UseVisualStyleBackColor = True
        '
        'butWebcam
        '
        Me.butWebcam.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butWebcam.Location = New System.Drawing.Point(721, 233)
        Me.butWebcam.Name = "butWebcam"
        Me.butWebcam.Size = New System.Drawing.Size(86, 26)
        Me.butWebcam.TabIndex = 201
        Me.butWebcam.Text = "開啟WebCam"
        Me.butWebcam.UseVisualStyleBackColor = True
        '
        'butChangeId
        '
        Me.butChangeId.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butChangeId.Location = New System.Drawing.Point(139, 487)
        Me.butChangeId.Name = "butChangeId"
        Me.butChangeId.Size = New System.Drawing.Size(65, 26)
        Me.butChangeId.TabIndex = 197
        Me.butChangeId.Text = "換學號"
        Me.butChangeId.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutNowUni)
        Me.GroupBox3.Controls.Add(Me.radbutNowHig)
        Me.GroupBox3.Controls.Add(Me.tboxClass)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.cboxGroup)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.cboxGrade)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.butUni)
        Me.GroupBox3.Controls.Add(Me.cboxUniversity)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.butHig)
        Me.GroupBox3.Controls.Add(Me.cboxHighSch)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.butJun)
        Me.GroupBox3.Controls.Add(Me.cboxJuniorSch)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.butPri)
        Me.GroupBox3.Controls.Add(Me.cboxPrimarySch)
        Me.GroupBox3.Controls.Add(Me.radbutNowJun)
        Me.GroupBox3.Controls.Add(Me.radbutNowPri)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label13)
        Me.GroupBox3.Location = New System.Drawing.Point(6, 270)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(203, 207)
        Me.GroupBox3.TabIndex = 189
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "學校"
        '
        'radbutNowUni
        '
        Me.radbutNowUni.AutoSize = True
        Me.radbutNowUni.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutNowUni.Location = New System.Drawing.Point(133, 185)
        Me.radbutNowUni.Name = "radbutNowUni"
        Me.radbutNowUni.Size = New System.Drawing.Size(47, 16)
        Me.radbutNowUni.TabIndex = 10
        Me.radbutNowUni.TabStop = True
        Me.radbutNowUni.Text = "大學"
        Me.radbutNowUni.UseVisualStyleBackColor = True
        '
        'radbutNowHig
        '
        Me.radbutNowHig.AutoSize = True
        Me.radbutNowHig.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutNowHig.Location = New System.Drawing.Point(58, 185)
        Me.radbutNowHig.Name = "radbutNowHig"
        Me.radbutNowHig.Size = New System.Drawing.Size(47, 16)
        Me.radbutNowHig.TabIndex = 9
        Me.radbutNowHig.TabStop = True
        Me.radbutNowHig.Text = "高中"
        Me.radbutNowHig.UseVisualStyleBackColor = True
        '
        'tboxClass
        '
        Me.tboxClass.Location = New System.Drawing.Point(58, 117)
        Me.tboxClass.Name = "tboxClass"
        Me.tboxClass.Size = New System.Drawing.Size(78, 22)
        Me.tboxClass.TabIndex = 5
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label16.Location = New System.Drawing.Point(11, 122)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(35, 12)
        Me.Label16.TabIndex = 26
        Me.Label16.Text = "班級: "
        Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxGroup
        '
        Me.cboxGroup.FormattingEnabled = True
        Me.cboxGroup.Location = New System.Drawing.Point(58, 139)
        Me.cboxGroup.Name = "cboxGroup"
        Me.cboxGroup.Size = New System.Drawing.Size(95, 20)
        Me.cboxGroup.TabIndex = 6
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label15.Location = New System.Drawing.Point(11, 143)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(35, 12)
        Me.Label15.TabIndex = 24
        Me.Label15.Text = "類組: "
        Me.Label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxGrade
        '
        Me.cboxGrade.FormattingEnabled = True
        Me.cboxGrade.Items.AddRange(New Object() {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13"})
        Me.cboxGrade.Location = New System.Drawing.Point(58, 97)
        Me.cboxGrade.Name = "cboxGrade"
        Me.cboxGrade.Size = New System.Drawing.Size(95, 20)
        Me.cboxGrade.TabIndex = 4
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label14.Location = New System.Drawing.Point(11, 102)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(35, 12)
        Me.Label14.TabIndex = 22
        Me.Label14.Text = "年級: "
        Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butUni
        '
        Me.butUni.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butUni.Location = New System.Drawing.Point(159, 79)
        Me.butUni.Name = "butUni"
        Me.butUni.Size = New System.Drawing.Size(21, 20)
        Me.butUni.TabIndex = 21
        Me.butUni.Text = "..."
        Me.butUni.UseVisualStyleBackColor = True
        '
        'cboxUniversity
        '
        Me.cboxUniversity.FormattingEnabled = True
        Me.cboxUniversity.Location = New System.Drawing.Point(58, 77)
        Me.cboxUniversity.Name = "cboxUniversity"
        Me.cboxUniversity.Size = New System.Drawing.Size(95, 20)
        Me.cboxUniversity.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label12.Location = New System.Drawing.Point(11, 81)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(35, 12)
        Me.Label12.TabIndex = 19
        Me.Label12.Text = "大學: "
        Me.Label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butHig
        '
        Me.butHig.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butHig.Location = New System.Drawing.Point(159, 57)
        Me.butHig.Name = "butHig"
        Me.butHig.Size = New System.Drawing.Size(21, 20)
        Me.butHig.TabIndex = 18
        Me.butHig.Text = "..."
        Me.butHig.UseVisualStyleBackColor = True
        '
        'cboxHighSch
        '
        Me.cboxHighSch.FormattingEnabled = True
        Me.cboxHighSch.Location = New System.Drawing.Point(58, 57)
        Me.cboxHighSch.Name = "cboxHighSch"
        Me.cboxHighSch.Size = New System.Drawing.Size(95, 20)
        Me.cboxHighSch.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label11.Location = New System.Drawing.Point(11, 61)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(35, 12)
        Me.Label11.TabIndex = 16
        Me.Label11.Text = "高中: "
        Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butJun
        '
        Me.butJun.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butJun.Location = New System.Drawing.Point(159, 37)
        Me.butJun.Name = "butJun"
        Me.butJun.Size = New System.Drawing.Size(21, 20)
        Me.butJun.TabIndex = 15
        Me.butJun.Text = "..."
        Me.butJun.UseVisualStyleBackColor = True
        '
        'cboxJuniorSch
        '
        Me.cboxJuniorSch.FormattingEnabled = True
        Me.cboxJuniorSch.Location = New System.Drawing.Point(58, 37)
        Me.cboxJuniorSch.Name = "cboxJuniorSch"
        Me.cboxJuniorSch.Size = New System.Drawing.Size(95, 20)
        Me.cboxJuniorSch.TabIndex = 1
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label9.Location = New System.Drawing.Point(11, 42)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(35, 12)
        Me.Label9.TabIndex = 13
        Me.Label9.Text = "國中: "
        Me.Label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butPri
        '
        Me.butPri.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPri.Location = New System.Drawing.Point(159, 15)
        Me.butPri.Name = "butPri"
        Me.butPri.Size = New System.Drawing.Size(21, 20)
        Me.butPri.TabIndex = 12
        Me.butPri.Text = "..."
        Me.butPri.UseVisualStyleBackColor = True
        '
        'cboxPrimarySch
        '
        Me.cboxPrimarySch.FormattingEnabled = True
        Me.cboxPrimarySch.Location = New System.Drawing.Point(58, 17)
        Me.cboxPrimarySch.Name = "cboxPrimarySch"
        Me.cboxPrimarySch.Size = New System.Drawing.Size(95, 20)
        Me.cboxPrimarySch.TabIndex = 0
        '
        'radbutNowJun
        '
        Me.radbutNowJun.AutoSize = True
        Me.radbutNowJun.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutNowJun.Location = New System.Drawing.Point(133, 166)
        Me.radbutNowJun.Name = "radbutNowJun"
        Me.radbutNowJun.Size = New System.Drawing.Size(47, 16)
        Me.radbutNowJun.TabIndex = 8
        Me.radbutNowJun.TabStop = True
        Me.radbutNowJun.Text = "國中"
        Me.radbutNowJun.UseVisualStyleBackColor = True
        '
        'radbutNowPri
        '
        Me.radbutNowPri.AutoSize = True
        Me.radbutNowPri.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutNowPri.Location = New System.Drawing.Point(58, 168)
        Me.radbutNowPri.Name = "radbutNowPri"
        Me.radbutNowPri.Size = New System.Drawing.Size(47, 16)
        Me.radbutNowPri.TabIndex = 7
        Me.radbutNowPri.TabStop = True
        Me.radbutNowPri.Text = "國小"
        Me.radbutNowPri.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(11, 168)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 12)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "現在是: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label13.Location = New System.Drawing.Point(11, 21)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(35, 12)
        Me.Label13.TabIndex = 0
        Me.Label13.Text = "國小: "
        Me.Label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butPrint
        '
        Me.butPrint.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPrint.Location = New System.Drawing.Point(139, 516)
        Me.butPrint.Name = "butPrint"
        Me.butPrint.Size = New System.Drawing.Size(64, 26)
        Me.butPrint.TabIndex = 200
        Me.butPrint.Text = "列印本頁"
        Me.butPrint.UseVisualStyleBackColor = True
        '
        'butNext
        '
        Me.butNext.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butNext.Location = New System.Drawing.Point(70, 516)
        Me.butNext.Name = "butNext"
        Me.butNext.Size = New System.Drawing.Size(65, 26)
        Me.butNext.TabIndex = 199
        Me.butNext.Text = "下一個"
        Me.butNext.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.butSales)
        Me.GroupBox5.Controls.Add(Me.butClassCancel)
        Me.GroupBox5.Controls.Add(Me.butClassChange)
        Me.GroupBox5.Controls.Add(Me.butSeatNum)
        Me.GroupBox5.Controls.Add(Me.butDelClass)
        Me.GroupBox5.Controls.Add(Me.butReg)
        Me.GroupBox5.Controls.Add(Me.chkboxShowNotOpen)
        Me.GroupBox5.Controls.Add(Me.chkboxShowOpen)
        Me.GroupBox5.Controls.Add(Me.chkboxShowPast)
        Me.GroupBox5.Controls.Add(Me.dgvClass)
        Me.GroupBox5.Location = New System.Drawing.Point(209, 270)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(490, 272)
        Me.GroupBox5.TabIndex = 191
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "已報名班級"
        '
        'butSales
        '
        Me.butSales.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSales.Location = New System.Drawing.Point(358, 240)
        Me.butSales.Name = "butSales"
        Me.butSales.Size = New System.Drawing.Size(65, 26)
        Me.butSales.TabIndex = 15
        Me.butSales.Text = "業務"
        Me.butSales.UseVisualStyleBackColor = True
        '
        'butClassCancel
        '
        Me.butClassCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butClassCancel.Location = New System.Drawing.Point(287, 240)
        Me.butClassCancel.Name = "butClassCancel"
        Me.butClassCancel.Size = New System.Drawing.Size(65, 26)
        Me.butClassCancel.TabIndex = 14
        Me.butClassCancel.Text = "不上了"
        Me.butClassCancel.UseVisualStyleBackColor = True
        '
        'butClassChange
        '
        Me.butClassChange.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butClassChange.Location = New System.Drawing.Point(218, 240)
        Me.butClassChange.Name = "butClassChange"
        Me.butClassChange.Size = New System.Drawing.Size(65, 26)
        Me.butClassChange.TabIndex = 13
        Me.butClassChange.Text = "轉班"
        Me.butClassChange.UseVisualStyleBackColor = True
        '
        'butSeatNum
        '
        Me.butSeatNum.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSeatNum.Location = New System.Drawing.Point(147, 240)
        Me.butSeatNum.Name = "butSeatNum"
        Me.butSeatNum.Size = New System.Drawing.Size(65, 26)
        Me.butSeatNum.TabIndex = 12
        Me.butSeatNum.Text = "座號"
        Me.butSeatNum.UseVisualStyleBackColor = True
        '
        'butDelClass
        '
        Me.butDelClass.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelClass.Location = New System.Drawing.Point(76, 240)
        Me.butDelClass.Name = "butDelClass"
        Me.butDelClass.Size = New System.Drawing.Size(65, 26)
        Me.butDelClass.TabIndex = 11
        Me.butDelClass.Text = "刪除"
        Me.butDelClass.UseVisualStyleBackColor = True
        '
        'butReg
        '
        Me.butReg.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butReg.Location = New System.Drawing.Point(7, 240)
        Me.butReg.Name = "butReg"
        Me.butReg.Size = New System.Drawing.Size(65, 26)
        Me.butReg.TabIndex = 10
        Me.butReg.Text = "我要報名"
        Me.butReg.UseVisualStyleBackColor = True
        '
        'chkboxShowNotOpen
        '
        Me.chkboxShowNotOpen.AutoSize = True
        Me.chkboxShowNotOpen.Checked = True
        Me.chkboxShowNotOpen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxShowNotOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowNotOpen.Location = New System.Drawing.Point(268, 0)
        Me.chkboxShowNotOpen.Name = "chkboxShowNotOpen"
        Me.chkboxShowNotOpen.Size = New System.Drawing.Size(84, 16)
        Me.chkboxShowNotOpen.TabIndex = 3
        Me.chkboxShowNotOpen.Text = "未開課班級"
        Me.chkboxShowNotOpen.UseVisualStyleBackColor = True
        '
        'chkboxShowOpen
        '
        Me.chkboxShowOpen.AutoSize = True
        Me.chkboxShowOpen.Checked = True
        Me.chkboxShowOpen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxShowOpen.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowOpen.Location = New System.Drawing.Point(178, 0)
        Me.chkboxShowOpen.Name = "chkboxShowOpen"
        Me.chkboxShowOpen.Size = New System.Drawing.Size(84, 16)
        Me.chkboxShowOpen.TabIndex = 2
        Me.chkboxShowOpen.Text = "已開課班級"
        Me.chkboxShowOpen.UseVisualStyleBackColor = True
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(100, 0)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(72, 16)
        Me.chkboxShowPast.TabIndex = 1
        Me.chkboxShowPast.Text = "過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'dgvClass
        '
        Me.dgvClass.AllowUserToAddRows = False
        Me.dgvClass.AllowUserToDeleteRows = False
        Me.dgvClass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClass.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvClass.Location = New System.Drawing.Point(7, 18)
        Me.dgvClass.MultiSelect = False
        Me.dgvClass.Name = "dgvClass"
        Me.dgvClass.ReadOnly = True
        Me.dgvClass.RowHeadersVisible = False
        Me.dgvClass.RowTemplate.Height = 24
        Me.dgvClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClass.Size = New System.Drawing.Size(477, 216)
        Me.dgvClass.TabIndex = 0
        '
        'butPrevious
        '
        Me.butPrevious.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butPrevious.Location = New System.Drawing.Point(6, 516)
        Me.butPrevious.Name = "butPrevious"
        Me.butPrevious.Size = New System.Drawing.Size(58, 26)
        Me.butPrevious.TabIndex = 198
        Me.butPrevious.Text = "上一個"
        Me.butPrevious.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.rtboxRemarks)
        Me.GroupBox7.Location = New System.Drawing.Point(705, 436)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(305, 106)
        Me.GroupBox7.TabIndex = 194
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "備註"
        '
        'rtboxRemarks
        '
        Me.rtboxRemarks.AutoWordSelection = True
        Me.rtboxRemarks.ContextMenuStrip = Me.ContextMenuStrip1
        Me.rtboxRemarks.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rtboxRemarks.EnableAutoDragDrop = True
        Me.rtboxRemarks.Location = New System.Drawing.Point(3, 18)
        Me.rtboxRemarks.Name = "rtboxRemarks"
        Me.rtboxRemarks.Size = New System.Drawing.Size(299, 85)
        Me.rtboxRemarks.TabIndex = 0
        Me.rtboxRemarks.Text = ""
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.ToolStripMenuItem2, Me.ToolStripMenuItem3})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(101, 70)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(100, 22)
        Me.ToolStripMenuItem1.Text = "剪下"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(100, 22)
        Me.ToolStripMenuItem2.Text = "複製"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(100, 22)
        Me.ToolStripMenuItem3.Text = "貼上"
        '
        'butSearch
        '
        Me.butSearch.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSearch.Location = New System.Drawing.Point(77, 487)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(58, 26)
        Me.butSearch.TabIndex = 196
        Me.butSearch.Text = "搜尋"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(6, 487)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(65, 26)
        Me.butSave.TabIndex = 195
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'picbox
        '
        Me.picbox.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.picbox.Location = New System.Drawing.Point(714, 10)
        Me.picbox.Name = "picbox"
        Me.picbox.Size = New System.Drawing.Size(296, 217)
        Me.picbox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.picbox.TabIndex = 192
        Me.picbox.TabStop = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboxSex)
        Me.GroupBox2.Controls.Add(Me.tboxBirthD)
        Me.GroupBox2.Controls.Add(Me.tboxBirthM)
        Me.GroupBox2.Controls.Add(Me.tboxBirthY)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.tboxEngName)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 161)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(203, 103)
        Me.GroupBox2.TabIndex = 188
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "資料"
        '
        'cboxSex
        '
        Me.cboxSex.FormattingEnabled = True
        Me.cboxSex.Items.AddRange(New Object() {"男", "女"})
        Me.cboxSex.Location = New System.Drawing.Point(76, 12)
        Me.cboxSex.Name = "cboxSex"
        Me.cboxSex.Size = New System.Drawing.Size(104, 20)
        Me.cboxSex.TabIndex = 6
        '
        'tboxBirthD
        '
        Me.tboxBirthD.Location = New System.Drawing.Point(152, 68)
        Me.tboxBirthD.Name = "tboxBirthD"
        Me.tboxBirthD.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirthD.TabIndex = 10
        '
        'tboxBirthM
        '
        Me.tboxBirthM.Location = New System.Drawing.Point(110, 68)
        Me.tboxBirthM.Name = "tboxBirthM"
        Me.tboxBirthM.Size = New System.Drawing.Size(36, 22)
        Me.tboxBirthM.TabIndex = 9
        '
        'tboxBirthY
        '
        Me.tboxBirthY.Location = New System.Drawing.Point(76, 68)
        Me.tboxBirthY.Name = "tboxBirthY"
        Me.tboxBirthY.Size = New System.Drawing.Size(28, 22)
        Me.tboxBirthY.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(6, 72)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(70, 12)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "生日 (民國): "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxEngName
        '
        Me.tboxEngName.Location = New System.Drawing.Point(76, 41)
        Me.tboxEngName.Name = "tboxEngName"
        Me.tboxEngName.Size = New System.Drawing.Size(104, 22)
        Me.tboxEngName.TabIndex = 7
        Me.tboxEngName.Visible = False
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label8.Location = New System.Drawing.Point(11, 41)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(59, 12)
        Me.Label8.TabIndex = 2
        Me.Label8.Text = "英文姓名: "
        Me.Label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Label8.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label10.Location = New System.Drawing.Point(35, 16)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(35, 12)
        Me.Label10.TabIndex = 0
        Me.Label10.Text = "性別: "
        Me.Label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butDelCard)
        Me.GroupBox1.Controls.Add(Me.butIssueCard)
        Me.GroupBox1.Controls.Add(Me.radbutPotStu)
        Me.GroupBox1.Controls.Add(Me.radbutFormalStu)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.tboxCreateDate)
        Me.GroupBox1.Controls.Add(Me.lblCreateDate)
        Me.GroupBox1.Controls.Add(Me.tboxStuName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblStuId)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(203, 151)
        Me.GroupBox1.TabIndex = 3
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "基本資料"
        '
        'butDelCard
        '
        Me.butDelCard.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butDelCard.Location = New System.Drawing.Point(110, 121)
        Me.butDelCard.Name = "butDelCard"
        Me.butDelCard.Size = New System.Drawing.Size(86, 26)
        Me.butDelCard.TabIndex = 5
        Me.butDelCard.Text = "作廢上課證"
        Me.butDelCard.UseVisualStyleBackColor = True
        '
        'butIssueCard
        '
        Me.butIssueCard.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butIssueCard.Location = New System.Drawing.Point(6, 121)
        Me.butIssueCard.Name = "butIssueCard"
        Me.butIssueCard.Size = New System.Drawing.Size(91, 26)
        Me.butIssueCard.TabIndex = 4
        Me.butIssueCard.Text = "發上課證"
        Me.butIssueCard.UseVisualStyleBackColor = True
        '
        'radbutPotStu
        '
        Me.radbutPotStu.AutoSize = True
        Me.radbutPotStu.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutPotStu.Location = New System.Drawing.Point(126, 100)
        Me.radbutPotStu.Name = "radbutPotStu"
        Me.radbutPotStu.Size = New System.Drawing.Size(71, 16)
        Me.radbutPotStu.TabIndex = 3
        Me.radbutPotStu.TabStop = True
        Me.radbutPotStu.Text = "潛在學生"
        Me.radbutPotStu.UseVisualStyleBackColor = True
        '
        'radbutFormalStu
        '
        Me.radbutFormalStu.AutoSize = True
        Me.radbutFormalStu.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutFormalStu.Location = New System.Drawing.Point(52, 100)
        Me.radbutFormalStu.Name = "radbutFormalStu"
        Me.radbutFormalStu.Size = New System.Drawing.Size(71, 16)
        Me.radbutFormalStu.TabIndex = 2
        Me.radbutFormalStu.TabStop = True
        Me.radbutFormalStu.Text = "正式學生"
        Me.radbutFormalStu.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(17, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(35, 12)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "身分: "
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxCreateDate
        '
        Me.tboxCreateDate.Location = New System.Drawing.Point(76, 21)
        Me.tboxCreateDate.Name = "tboxCreateDate"
        Me.tboxCreateDate.Size = New System.Drawing.Size(104, 22)
        Me.tboxCreateDate.TabIndex = 0
        '
        'lblCreateDate
        '
        Me.lblCreateDate.AutoSize = True
        Me.lblCreateDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblCreateDate.Location = New System.Drawing.Point(17, 24)
        Me.lblCreateDate.Name = "lblCreateDate"
        Me.lblCreateDate.Size = New System.Drawing.Size(59, 12)
        Me.lblCreateDate.TabIndex = 0
        Me.lblCreateDate.Text = "建檔日期: "
        Me.lblCreateDate.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxStuName
        '
        Me.tboxStuName.Location = New System.Drawing.Point(76, 72)
        Me.tboxStuName.Name = "tboxStuName"
        Me.tboxStuName.Size = New System.Drawing.Size(104, 22)
        Me.tboxStuName.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(35, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "姓名: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblStuId
        '
        Me.lblStuId.AutoSize = True
        Me.lblStuId.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.lblStuId.Location = New System.Drawing.Point(76, 53)
        Me.lblStuId.Name = "lblStuId"
        Me.lblStuId.Size = New System.Drawing.Size(53, 12)
        Me.lblStuId.TabIndex = 1
        Me.lblStuId.Text = "20000001"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(35, 52)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "學號: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.tboxPunchCardNum)
        Me.GroupBox4.Controls.Add(Me.Label53)
        Me.GroupBox4.Controls.Add(Me.tboxEnrollSch)
        Me.GroupBox4.Controls.Add(Me.Label54)
        Me.GroupBox4.Controls.Add(Me.tboxHseeMark)
        Me.GroupBox4.Controls.Add(Me.Label55)
        Me.GroupBox4.Controls.Add(Me.tboxAdd1b)
        Me.GroupBox4.Controls.Add(Me.tboxAdd2b)
        Me.GroupBox4.Controls.Add(Me.tboxClassId)
        Me.GroupBox4.Controls.Add(Me.tboxStay)
        Me.GroupBox4.Controls.Add(Me.Label32)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.tboxDadTitle)
        Me.GroupBox4.Controls.Add(Me.tboxRegInfo)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.tboxMumTitle)
        Me.GroupBox4.Controls.Add(Me.Label35)
        Me.GroupBox4.Controls.Add(Me.Label36)
        Me.GroupBox4.Controls.Add(Me.tboxMumMobile)
        Me.GroupBox4.Controls.Add(Me.tboxIntroName)
        Me.GroupBox4.Controls.Add(Me.Label27)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Controls.Add(Me.tboxGradJunior)
        Me.GroupBox4.Controls.Add(Me.tboxMumName)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.tboxIc)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.Label31)
        Me.GroupBox4.Controls.Add(Me.tboxAdd1a)
        Me.GroupBox4.Controls.Add(Me.tboxEmail)
        Me.GroupBox4.Controls.Add(Me.Label23)
        Me.GroupBox4.Controls.Add(Me.tboxAdd2a)
        Me.GroupBox4.Controls.Add(Me.Label24)
        Me.GroupBox4.Controls.Add(Me.Label25)
        Me.GroupBox4.Controls.Add(Me.tboxIntroId)
        Me.GroupBox4.Controls.Add(Me.Label26)
        Me.GroupBox4.Controls.Add(Me.tboxMobile)
        Me.GroupBox4.Controls.Add(Me.tboxDadMobile)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.tboxDadName)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Controls.Add(Me.tboxTel1)
        Me.GroupBox4.Controls.Add(Me.tboxOfficeTel)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.tboxTel2)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label21)
        Me.GroupBox4.Location = New System.Drawing.Point(209, 4)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(490, 260)
        Me.GroupBox4.TabIndex = 190
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "通訊錄"
        '
        'tboxPunchCardNum
        '
        Me.tboxPunchCardNum.Location = New System.Drawing.Point(392, 39)
        Me.tboxPunchCardNum.Name = "tboxPunchCardNum"
        Me.tboxPunchCardNum.Size = New System.Drawing.Size(81, 22)
        Me.tboxPunchCardNum.TabIndex = 14
        '
        'Label53
        '
        Me.Label53.AutoSize = True
        Me.Label53.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label53.Location = New System.Drawing.Point(327, 40)
        Me.Label53.Name = "Label53"
        Me.Label53.Size = New System.Drawing.Size(59, 12)
        Me.Label53.TabIndex = 54
        Me.Label53.Text = "劃卡編號: "
        Me.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxEnrollSch
        '
        Me.tboxEnrollSch.Location = New System.Drawing.Point(232, 17)
        Me.tboxEnrollSch.Name = "tboxEnrollSch"
        Me.tboxEnrollSch.Size = New System.Drawing.Size(81, 22)
        Me.tboxEnrollSch.TabIndex = 7
        '
        'Label54
        '
        Me.Label54.AutoSize = True
        Me.Label54.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label54.Location = New System.Drawing.Point(176, 20)
        Me.Label54.Name = "Label54"
        Me.Label54.Size = New System.Drawing.Size(59, 12)
        Me.Label54.TabIndex = 52
        Me.Label54.Text = "錄取學校: "
        Me.Label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxHseeMark
        '
        Me.tboxHseeMark.Location = New System.Drawing.Point(232, 39)
        Me.tboxHseeMark.Name = "tboxHseeMark"
        Me.tboxHseeMark.Size = New System.Drawing.Size(81, 22)
        Me.tboxHseeMark.TabIndex = 8
        '
        'Label55
        '
        Me.Label55.AutoSize = True
        Me.Label55.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label55.Location = New System.Drawing.Point(176, 42)
        Me.Label55.Name = "Label55"
        Me.Label55.Size = New System.Drawing.Size(59, 12)
        Me.Label55.TabIndex = 50
        Me.Label55.Text = "基測分數: "
        Me.Label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxAdd1b
        '
        Me.tboxAdd1b.Location = New System.Drawing.Point(118, 177)
        Me.tboxAdd1b.Name = "tboxAdd1b"
        Me.tboxAdd1b.Size = New System.Drawing.Size(355, 22)
        Me.tboxAdd1b.TabIndex = 22
        '
        'tboxAdd2b
        '
        Me.tboxAdd2b.Location = New System.Drawing.Point(118, 201)
        Me.tboxAdd2b.Name = "tboxAdd2b"
        Me.tboxAdd2b.Size = New System.Drawing.Size(355, 22)
        Me.tboxAdd2b.TabIndex = 24
        '
        'tboxClassId
        '
        Me.tboxClassId.Location = New System.Drawing.Point(392, 127)
        Me.tboxClassId.Name = "tboxClassId"
        Me.tboxClassId.Size = New System.Drawing.Size(81, 22)
        Me.tboxClassId.TabIndex = 18
        '
        'tboxStay
        '
        Me.tboxStay.Location = New System.Drawing.Point(392, 149)
        Me.tboxStay.Name = "tboxStay"
        Me.tboxStay.Size = New System.Drawing.Size(81, 22)
        Me.tboxStay.TabIndex = 19
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label32.Location = New System.Drawing.Point(351, 152)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(35, 12)
        Me.Label32.TabIndex = 39
        Me.Label32.Text = "住宿: "
        Me.Label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label33.Location = New System.Drawing.Point(327, 130)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(59, 12)
        Me.Label33.TabIndex = 38
        Me.Label33.Text = "班級編號: "
        Me.Label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxDadTitle
        '
        Me.tboxDadTitle.Location = New System.Drawing.Point(392, 61)
        Me.tboxDadTitle.Name = "tboxDadTitle"
        Me.tboxDadTitle.Size = New System.Drawing.Size(81, 22)
        Me.tboxDadTitle.TabIndex = 15
        '
        'tboxRegInfo
        '
        Me.tboxRegInfo.Location = New System.Drawing.Point(392, 105)
        Me.tboxRegInfo.Name = "tboxRegInfo"
        Me.tboxRegInfo.Size = New System.Drawing.Size(81, 22)
        Me.tboxRegInfo.TabIndex = 17
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label34.Location = New System.Drawing.Point(327, 108)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(59, 12)
        Me.Label34.TabIndex = 35
        Me.Label34.Text = "來班資訊: "
        Me.Label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxMumTitle
        '
        Me.tboxMumTitle.Location = New System.Drawing.Point(392, 83)
        Me.tboxMumTitle.Name = "tboxMumTitle"
        Me.tboxMumTitle.Size = New System.Drawing.Size(81, 22)
        Me.tboxMumTitle.TabIndex = 16
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label35.Location = New System.Drawing.Point(327, 86)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(59, 12)
        Me.Label35.TabIndex = 33
        Me.Label35.Text = "媽媽職業: "
        Me.Label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label36
        '
        Me.Label36.AutoSize = True
        Me.Label36.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label36.Location = New System.Drawing.Point(327, 64)
        Me.Label36.Name = "Label36"
        Me.Label36.Size = New System.Drawing.Size(59, 12)
        Me.Label36.TabIndex = 32
        Me.Label36.Text = "爸爸職業: "
        Me.Label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxMumMobile
        '
        Me.tboxMumMobile.Location = New System.Drawing.Point(232, 127)
        Me.tboxMumMobile.Name = "tboxMumMobile"
        Me.tboxMumMobile.Size = New System.Drawing.Size(81, 22)
        Me.tboxMumMobile.TabIndex = 12
        '
        'tboxIntroName
        '
        Me.tboxIntroName.Location = New System.Drawing.Point(232, 149)
        Me.tboxIntroName.Name = "tboxIntroName"
        Me.tboxIntroName.Size = New System.Drawing.Size(81, 22)
        Me.tboxIntroName.TabIndex = 13
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label27.Location = New System.Drawing.Point(164, 152)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(71, 12)
        Me.Label27.TabIndex = 29
        Me.Label27.Text = "介紹人姓名: "
        Me.Label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label28.Location = New System.Drawing.Point(176, 130)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(59, 12)
        Me.Label28.TabIndex = 28
        Me.Label28.Text = "媽媽手機: "
        Me.Label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxGradJunior
        '
        Me.tboxGradJunior.Location = New System.Drawing.Point(232, 61)
        Me.tboxGradJunior.Name = "tboxGradJunior"
        Me.tboxGradJunior.Size = New System.Drawing.Size(81, 22)
        Me.tboxGradJunior.TabIndex = 9
        '
        'tboxMumName
        '
        Me.tboxMumName.Location = New System.Drawing.Point(232, 105)
        Me.tboxMumName.Name = "tboxMumName"
        Me.tboxMumName.Size = New System.Drawing.Size(81, 22)
        Me.tboxMumName.TabIndex = 11
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label29.Location = New System.Drawing.Point(176, 108)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(59, 12)
        Me.Label29.TabIndex = 25
        Me.Label29.Text = "媽媽姓名: "
        Me.Label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxIc
        '
        Me.tboxIc.Location = New System.Drawing.Point(232, 83)
        Me.tboxIc.Name = "tboxIc"
        Me.tboxIc.Size = New System.Drawing.Size(81, 22)
        Me.tboxIc.TabIndex = 10
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label30.Location = New System.Drawing.Point(188, 86)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(47, 12)
        Me.Label30.TabIndex = 23
        Me.Label30.Text = "身分證: "
        Me.Label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label31.Location = New System.Drawing.Point(176, 64)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(59, 12)
        Me.Label31.TabIndex = 22
        Me.Label31.Text = "畢業國中: "
        Me.Label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxAdd1a
        '
        Me.tboxAdd1a.Location = New System.Drawing.Point(76, 178)
        Me.tboxAdd1a.Name = "tboxAdd1a"
        Me.tboxAdd1a.Size = New System.Drawing.Size(36, 22)
        Me.tboxAdd1a.TabIndex = 21
        '
        'tboxEmail
        '
        Me.tboxEmail.Location = New System.Drawing.Point(76, 227)
        Me.tboxEmail.Name = "tboxEmail"
        Me.tboxEmail.Size = New System.Drawing.Size(397, 22)
        Me.tboxEmail.TabIndex = 27
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label23.Location = New System.Drawing.Point(39, 231)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(38, 12)
        Me.Label23.TabIndex = 19
        Me.Label23.Text = "Email: "
        Me.Label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxAdd2a
        '
        Me.tboxAdd2a.Location = New System.Drawing.Point(76, 202)
        Me.tboxAdd2a.Name = "tboxAdd2a"
        Me.tboxAdd2a.Size = New System.Drawing.Size(36, 22)
        Me.tboxAdd2a.TabIndex = 23
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label24.Location = New System.Drawing.Point(36, 207)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(41, 12)
        Me.Label24.TabIndex = 17
        Me.Label24.Text = "地址2: "
        Me.Label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label25.Location = New System.Drawing.Point(36, 181)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(41, 12)
        Me.Label25.TabIndex = 16
        Me.Label25.Text = "地址1: "
        Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxIntroId
        '
        Me.tboxIntroId.Location = New System.Drawing.Point(76, 149)
        Me.tboxIntroId.Name = "tboxIntroId"
        Me.tboxIntroId.Size = New System.Drawing.Size(81, 22)
        Me.tboxIntroId.TabIndex = 6
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label26.Location = New System.Drawing.Point(6, 152)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(71, 12)
        Me.Label26.TabIndex = 14
        Me.Label26.Text = "介紹人學號: "
        Me.Label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxMobile
        '
        Me.tboxMobile.Location = New System.Drawing.Point(76, 83)
        Me.tboxMobile.Name = "tboxMobile"
        Me.tboxMobile.Size = New System.Drawing.Size(81, 22)
        Me.tboxMobile.TabIndex = 3
        '
        'tboxDadMobile
        '
        Me.tboxDadMobile.Location = New System.Drawing.Point(76, 127)
        Me.tboxDadMobile.Name = "tboxDadMobile"
        Me.tboxDadMobile.Size = New System.Drawing.Size(81, 22)
        Me.tboxDadMobile.TabIndex = 5
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label17.Location = New System.Drawing.Point(18, 130)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(59, 12)
        Me.Label17.TabIndex = 11
        Me.Label17.Text = "爸爸手機: "
        Me.Label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxDadName
        '
        Me.tboxDadName.Location = New System.Drawing.Point(76, 105)
        Me.tboxDadName.Name = "tboxDadName"
        Me.tboxDadName.Size = New System.Drawing.Size(81, 22)
        Me.tboxDadName.TabIndex = 4
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label20.Location = New System.Drawing.Point(18, 108)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(59, 12)
        Me.Label20.TabIndex = 9
        Me.Label20.Text = "爸爸姓名: "
        Me.Label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label22.Location = New System.Drawing.Point(18, 86)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(59, 12)
        Me.Label22.TabIndex = 8
        Me.Label22.Text = "學生手機: "
        Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxTel1
        '
        Me.tboxTel1.Location = New System.Drawing.Point(76, 17)
        Me.tboxTel1.Name = "tboxTel1"
        Me.tboxTel1.Size = New System.Drawing.Size(81, 22)
        Me.tboxTel1.TabIndex = 0
        '
        'tboxOfficeTel
        '
        Me.tboxOfficeTel.Location = New System.Drawing.Point(76, 61)
        Me.tboxOfficeTel.Name = "tboxOfficeTel"
        Me.tboxOfficeTel.Size = New System.Drawing.Size(81, 22)
        Me.tboxOfficeTel.TabIndex = 2
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label18.Location = New System.Drawing.Point(18, 64)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(59, 12)
        Me.Label18.TabIndex = 4
        Me.Label18.Text = "公司電話: "
        Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tboxTel2
        '
        Me.tboxTel2.Location = New System.Drawing.Point(76, 39)
        Me.tboxTel2.Name = "tboxTel2"
        Me.tboxTel2.Size = New System.Drawing.Size(81, 22)
        Me.tboxTel2.TabIndex = 1
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label19.Location = New System.Drawing.Point(12, 42)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 12)
        Me.Label19.TabIndex = 2
        Me.Label19.Text = "家裡電話2: "
        Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label21.Location = New System.Drawing.Point(12, 20)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(65, 12)
        Me.Label21.TabIndex = 0
        Me.Label21.Text = "家裡電話1: "
        Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'tabCtrl
        '
        Me.tabCtrl.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.tabCtrl.Controls.Add(Me.tpgInfo)
        Me.tabCtrl.Controls.Add(Me.tpgPay)
        Me.tabCtrl.Controls.Add(Me.tpgKBRec)
        Me.tabCtrl.Controls.Add(Me.tpgPunchRec)
        Me.tabCtrl.Controls.Add(Me.tpgBookRec)
        Me.tabCtrl.Controls.Add(Me.tpgClassTransferRec)
        Me.tabCtrl.Controls.Add(Me.tpgTeleInterview)
        Me.tabCtrl.Controls.Add(Me.tpgAbsentRec)
        Me.tabCtrl.Controls.Add(Me.tabAssign)
        Me.tabCtrl.Controls.Add(Me.tabGrade)
        Me.tabCtrl.Controls.Add(Me.tpgClose)
        Me.tabCtrl.Location = New System.Drawing.Point(1, 0)
        Me.tabCtrl.Name = "tabCtrl"
        Me.tabCtrl.SelectedIndex = 0
        Me.tabCtrl.Size = New System.Drawing.Size(1025, 599)
        Me.tabCtrl.TabIndex = 0
        '
        'tabAssign
        '
        Me.tabAssign.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tabAssign.Controls.Add(Me.btnRefreshAssgin)
        Me.tabAssign.Controls.Add(Me.GroupBox19)
        Me.tabAssign.Controls.Add(Me.GroupBox18)
        Me.tabAssign.Controls.Add(Me.Label56)
        Me.tabAssign.Controls.Add(Me.chkboxAssignShowPast)
        Me.tabAssign.Controls.Add(Me.cboxAssignClass)
        Me.tabAssign.Location = New System.Drawing.Point(4, 25)
        Me.tabAssign.Name = "tabAssign"
        Me.tabAssign.Padding = New System.Windows.Forms.Padding(3)
        Me.tabAssign.Size = New System.Drawing.Size(1017, 570)
        Me.tabAssign.TabIndex = 9
        Me.tabAssign.Text = "作業記錄"
        Me.tabAssign.UseVisualStyleBackColor = True
        '
        'btnRefreshAssgin
        '
        Me.btnRefreshAssgin.Location = New System.Drawing.Point(485, 20)
        Me.btnRefreshAssgin.Name = "btnRefreshAssgin"
        Me.btnRefreshAssgin.Size = New System.Drawing.Size(75, 23)
        Me.btnRefreshAssgin.TabIndex = 212
        Me.btnRefreshAssgin.Text = "即時更新"
        Me.btnRefreshAssgin.UseVisualStyleBackColor = True
        '
        'GroupBox19
        '
        Me.GroupBox19.Controls.Add(Me.dgvAssignNoPass)
        Me.GroupBox19.Location = New System.Drawing.Point(7, 316)
        Me.GroupBox19.Name = "GroupBox19"
        Me.GroupBox19.Size = New System.Drawing.Size(994, 229)
        Me.GroupBox19.TabIndex = 211
        Me.GroupBox19.TabStop = False
        Me.GroupBox19.Text = "沒通過的作業"
        '
        'dgvAssignNoPass
        '
        Me.dgvAssignNoPass.AllowUserToAddRows = False
        Me.dgvAssignNoPass.AllowUserToDeleteRows = False
        Me.dgvAssignNoPass.AllowUserToResizeRows = False
        Me.dgvAssignNoPass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAssignNoPass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssignNoPass.Location = New System.Drawing.Point(6, 13)
        Me.dgvAssignNoPass.MultiSelect = False
        Me.dgvAssignNoPass.Name = "dgvAssignNoPass"
        Me.dgvAssignNoPass.ReadOnly = True
        Me.dgvAssignNoPass.RowHeadersVisible = False
        Me.dgvAssignNoPass.RowTemplate.Height = 24
        Me.dgvAssignNoPass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssignNoPass.ShowCellToolTips = False
        Me.dgvAssignNoPass.ShowEditingIcon = False
        Me.dgvAssignNoPass.Size = New System.Drawing.Size(982, 210)
        Me.dgvAssignNoPass.TabIndex = 0
        '
        'GroupBox18
        '
        Me.GroupBox18.Controls.Add(Me.dgvAssignPass)
        Me.GroupBox18.Location = New System.Drawing.Point(7, 55)
        Me.GroupBox18.Name = "GroupBox18"
        Me.GroupBox18.Size = New System.Drawing.Size(994, 255)
        Me.GroupBox18.TabIndex = 210
        Me.GroupBox18.TabStop = False
        Me.GroupBox18.Text = "通過的作業"
        '
        'dgvAssignPass
        '
        Me.dgvAssignPass.AllowUserToAddRows = False
        Me.dgvAssignPass.AllowUserToDeleteRows = False
        Me.dgvAssignPass.AllowUserToResizeRows = False
        Me.dgvAssignPass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvAssignPass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAssignPass.Location = New System.Drawing.Point(6, 15)
        Me.dgvAssignPass.MultiSelect = False
        Me.dgvAssignPass.Name = "dgvAssignPass"
        Me.dgvAssignPass.ReadOnly = True
        Me.dgvAssignPass.RowHeadersVisible = False
        Me.dgvAssignPass.RowTemplate.Height = 24
        Me.dgvAssignPass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvAssignPass.ShowCellToolTips = False
        Me.dgvAssignPass.ShowEditingIcon = False
        Me.dgvAssignPass.Size = New System.Drawing.Size(982, 234)
        Me.dgvAssignPass.TabIndex = 0
        '
        'Label56
        '
        Me.Label56.AutoSize = True
        Me.Label56.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label56.Location = New System.Drawing.Point(21, 23)
        Me.Label56.Name = "Label56"
        Me.Label56.Size = New System.Drawing.Size(35, 12)
        Me.Label56.TabIndex = 209
        Me.Label56.Text = "班級: "
        Me.Label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'chkboxAssignShowPast
        '
        Me.chkboxAssignShowPast.AutoSize = True
        Me.chkboxAssignShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxAssignShowPast.Location = New System.Drawing.Point(360, 23)
        Me.chkboxAssignShowPast.Name = "chkboxAssignShowPast"
        Me.chkboxAssignShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxAssignShowPast.TabIndex = 208
        Me.chkboxAssignShowPast.Text = "顯示過時班級"
        Me.chkboxAssignShowPast.UseVisualStyleBackColor = True
        '
        'cboxAssignClass
        '
        Me.cboxAssignClass.FormattingEnabled = True
        Me.cboxAssignClass.Location = New System.Drawing.Point(62, 20)
        Me.cboxAssignClass.Name = "cboxAssignClass"
        Me.cboxAssignClass.Size = New System.Drawing.Size(279, 20)
        Me.cboxAssignClass.TabIndex = 207
        '
        'tabGrade
        '
        Me.tabGrade.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tabGrade.Controls.Add(Me.GroupBox23)
        Me.tabGrade.Controls.Add(Me.GroupBox22)
        Me.tabGrade.Controls.Add(Me.chkboxPaperShowPast)
        Me.tabGrade.Controls.Add(Me.cboxPaperClass)
        Me.tabGrade.Controls.Add(Me.Label58)
        Me.tabGrade.Location = New System.Drawing.Point(4, 25)
        Me.tabGrade.Name = "tabGrade"
        Me.tabGrade.Size = New System.Drawing.Size(1017, 570)
        Me.tabGrade.TabIndex = 10
        Me.tabGrade.Text = "成績記錄"
        '
        'GroupBox23
        '
        Me.GroupBox23.Controls.Add(Me.dgvPaperNoPass)
        Me.GroupBox23.Location = New System.Drawing.Point(7, 286)
        Me.GroupBox23.Name = "GroupBox23"
        Me.GroupBox23.Size = New System.Drawing.Size(994, 272)
        Me.GroupBox23.TabIndex = 5
        Me.GroupBox23.TabStop = False
        Me.GroupBox23.Text = "缺考記錄"
        '
        'dgvPaperNoPass
        '
        Me.dgvPaperNoPass.AllowUserToAddRows = False
        Me.dgvPaperNoPass.AllowUserToDeleteRows = False
        Me.dgvPaperNoPass.AllowUserToResizeRows = False
        Me.dgvPaperNoPass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPaperNoPass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaperNoPass.Location = New System.Drawing.Point(6, 21)
        Me.dgvPaperNoPass.MultiSelect = False
        Me.dgvPaperNoPass.Name = "dgvPaperNoPass"
        Me.dgvPaperNoPass.ReadOnly = True
        Me.dgvPaperNoPass.RowHeadersVisible = False
        Me.dgvPaperNoPass.RowTemplate.Height = 24
        Me.dgvPaperNoPass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPaperNoPass.ShowCellToolTips = False
        Me.dgvPaperNoPass.ShowEditingIcon = False
        Me.dgvPaperNoPass.Size = New System.Drawing.Size(982, 245)
        Me.dgvPaperNoPass.TabIndex = 0
        '
        'GroupBox22
        '
        Me.GroupBox22.Controls.Add(Me.dgvPaperPass)
        Me.GroupBox22.Location = New System.Drawing.Point(7, 54)
        Me.GroupBox22.Name = "GroupBox22"
        Me.GroupBox22.Size = New System.Drawing.Size(994, 226)
        Me.GroupBox22.TabIndex = 4
        Me.GroupBox22.TabStop = False
        Me.GroupBox22.Text = "成績記錄"
        '
        'dgvPaperPass
        '
        Me.dgvPaperPass.AllowUserToAddRows = False
        Me.dgvPaperPass.AllowUserToDeleteRows = False
        Me.dgvPaperPass.AllowUserToResizeRows = False
        Me.dgvPaperPass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvPaperPass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPaperPass.Location = New System.Drawing.Point(6, 21)
        Me.dgvPaperPass.MultiSelect = False
        Me.dgvPaperPass.Name = "dgvPaperPass"
        Me.dgvPaperPass.ReadOnly = True
        Me.dgvPaperPass.RowHeadersVisible = False
        Me.dgvPaperPass.RowTemplate.Height = 24
        Me.dgvPaperPass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPaperPass.ShowCellToolTips = False
        Me.dgvPaperPass.ShowEditingIcon = False
        Me.dgvPaperPass.Size = New System.Drawing.Size(982, 199)
        Me.dgvPaperPass.TabIndex = 0
        '
        'chkboxPaperShowPast
        '
        Me.chkboxPaperShowPast.AutoSize = True
        Me.chkboxPaperShowPast.Location = New System.Drawing.Point(257, 16)
        Me.chkboxPaperShowPast.Name = "chkboxPaperShowPast"
        Me.chkboxPaperShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxPaperShowPast.TabIndex = 2
        Me.chkboxPaperShowPast.Text = "顯示過時班級"
        Me.chkboxPaperShowPast.UseVisualStyleBackColor = True
        '
        'cboxPaperClass
        '
        Me.cboxPaperClass.FormattingEnabled = True
        Me.cboxPaperClass.Location = New System.Drawing.Point(40, 14)
        Me.cboxPaperClass.Name = "cboxPaperClass"
        Me.cboxPaperClass.Size = New System.Drawing.Size(196, 20)
        Me.cboxPaperClass.TabIndex = 1
        '
        'Label58
        '
        Me.Label58.AutoSize = True
        Me.Label58.Location = New System.Drawing.Point(5, 17)
        Me.Label58.Name = "Label58"
        Me.Label58.Size = New System.Drawing.Size(29, 12)
        Me.Label58.TabIndex = 0
        Me.Label58.Text = "班級"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(485, 20)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 212
        Me.Button1.Text = "即時更新"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'GroupBox20
        '
        Me.GroupBox20.Controls.Add(Me.DataGridView1)
        Me.GroupBox20.Location = New System.Drawing.Point(7, 316)
        Me.GroupBox20.Name = "GroupBox20"
        Me.GroupBox20.Size = New System.Drawing.Size(994, 229)
        Me.GroupBox20.TabIndex = 211
        Me.GroupBox20.TabStop = False
        Me.GroupBox20.Text = "沒通過的作業"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToResizeRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Location = New System.Drawing.Point(6, 13)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.RowHeadersVisible = False
        Me.DataGridView1.RowTemplate.Height = 24
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView1.ShowCellToolTips = False
        Me.DataGridView1.ShowEditingIcon = False
        Me.DataGridView1.Size = New System.Drawing.Size(982, 210)
        Me.DataGridView1.TabIndex = 0
        '
        'GroupBox21
        '
        Me.GroupBox21.Controls.Add(Me.DataGridView2)
        Me.GroupBox21.Location = New System.Drawing.Point(7, 55)
        Me.GroupBox21.Name = "GroupBox21"
        Me.GroupBox21.Size = New System.Drawing.Size(994, 255)
        Me.GroupBox21.TabIndex = 210
        Me.GroupBox21.TabStop = False
        Me.GroupBox21.Text = "通過的作業"
        '
        'DataGridView2
        '
        Me.DataGridView2.AllowUserToAddRows = False
        Me.DataGridView2.AllowUserToDeleteRows = False
        Me.DataGridView2.AllowUserToResizeRows = False
        Me.DataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.DataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView2.Location = New System.Drawing.Point(6, 15)
        Me.DataGridView2.MultiSelect = False
        Me.DataGridView2.Name = "DataGridView2"
        Me.DataGridView2.ReadOnly = True
        Me.DataGridView2.RowHeadersVisible = False
        Me.DataGridView2.RowTemplate.Height = 24
        Me.DataGridView2.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.DataGridView2.ShowCellToolTips = False
        Me.DataGridView2.ShowEditingIcon = False
        Me.DataGridView2.Size = New System.Drawing.Size(982, 234)
        Me.DataGridView2.TabIndex = 0
        '
        'Label57
        '
        Me.Label57.AutoSize = True
        Me.Label57.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label57.Location = New System.Drawing.Point(21, 23)
        Me.Label57.Name = "Label57"
        Me.Label57.Size = New System.Drawing.Size(35, 12)
        Me.Label57.TabIndex = 209
        Me.Label57.Text = "班級: "
        Me.Label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.CheckBox1.Location = New System.Drawing.Point(360, 23)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(96, 16)
        Me.CheckBox1.TabIndex = 208
        Me.CheckBox1.Text = "顯示過時班級"
        Me.CheckBox1.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(62, 20)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(279, 20)
        Me.ComboBox1.TabIndex = 207
        '
        'frmStuInfo
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 595)
        Me.Controls.Add(Me.tabCtrl)
        Me.Name = "frmStuInfo"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "frmStuInfo"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.tpgAbsentRec.ResumeLayout(False)
        Me.tpgAbsentRec.PerformLayout()
        CType(Me.dgvAbsentRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgTeleInterview.ResumeLayout(False)
        Me.tpgTeleInterview.PerformLayout()
        CType(Me.dgvTeleRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgClassTransferRec.ResumeLayout(False)
        Me.tpgClassTransferRec.PerformLayout()
        CType(Me.dgvChange, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgBookRec.ResumeLayout(False)
        Me.tpgBookRec.PerformLayout()
        Me.GroupBox17.ResumeLayout(False)
        Me.GroupBox17.PerformLayout()
        CType(Me.dgvBookNotReceive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox16.ResumeLayout(False)
        Me.GroupBox16.PerformLayout()
        CType(Me.dgvBookReceive, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgPunchRec.ResumeLayout(False)
        Me.tpgPunchRec.PerformLayout()
        CType(Me.dgvAttRec, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvAttClass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgKBRec.ResumeLayout(False)
        Me.GroupBox15.ResumeLayout(False)
        Me.TabControl1.ResumeLayout(False)
        Me.tpgKBPay.ResumeLayout(False)
        CType(Me.dgvKBPayRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgKBShowUp.ResumeLayout(False)
        CType(Me.dgvKBAttendance, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgKBBookRec.ResumeLayout(False)
        CType(Me.dgvKBBook, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox14.ResumeLayout(False)
        CType(Me.dgvKB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpgPay.ResumeLayout(False)
        Me.tpgPay.PerformLayout()
        Me.grpboxExtra.ResumeLayout(False)
        Me.grpboxExtra.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        CType(Me.dgvPayDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        CType(Me.dgvClass2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.tpgInfo.ResumeLayout(False)
        Me.tpgInfo.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.tabCtrl.ResumeLayout(False)
        Me.tabAssign.ResumeLayout(False)
        Me.tabAssign.PerformLayout()
        Me.GroupBox19.ResumeLayout(False)
        CType(Me.dgvAssignNoPass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox18.ResumeLayout(False)
        CType(Me.dgvAssignPass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tabGrade.ResumeLayout(False)
        Me.tabGrade.PerformLayout()
        Me.GroupBox23.ResumeLayout(False)
        CType(Me.dgvPaperNoPass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox22.ResumeLayout(False)
        CType(Me.dgvPaperPass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox20.ResumeLayout(False)
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox21.ResumeLayout(False)
        CType(Me.DataGridView2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument3 As System.Drawing.Printing.PrintDocument
    Friend WithEvents prtdocReceipt As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument4 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument5 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument6 As System.Drawing.Printing.PrintDocument
    Friend WithEvents tpgClose As System.Windows.Forms.TabPage
    Friend WithEvents tpgAbsentRec As System.Windows.Forms.TabPage
    Friend WithEvents dgvAbsentRec As System.Windows.Forms.DataGridView
    Friend WithEvents butAbsentPrint As System.Windows.Forms.Button
    Friend WithEvents butAbsentExport As System.Windows.Forms.Button
    Friend WithEvents chkboxAbsentShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxAbsentClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label49 As System.Windows.Forms.Label
    Friend WithEvents tpgTeleInterview As System.Windows.Forms.TabPage
    Friend WithEvents butTeleExport As System.Windows.Forms.Button
    Friend WithEvents butTeleDel As System.Windows.Forms.Button
    Friend WithEvents butTelePrint As System.Windows.Forms.Button
    Friend WithEvents butTeleMod As System.Windows.Forms.Button
    Friend WithEvents butTeleAdd As System.Windows.Forms.Button
    Friend WithEvents chkboxTeleDate As System.Windows.Forms.CheckBox
    Friend WithEvents Label52 As System.Windows.Forms.Label
    Friend WithEvents cboxTeleType As System.Windows.Forms.ComboBox
    Friend WithEvents dgvTeleRec As System.Windows.Forms.DataGridView
    Friend WithEvents dtpickTele As System.Windows.Forms.DateTimePicker
    Friend WithEvents butTeleListSta As System.Windows.Forms.Button
    Friend WithEvents tpgClassTransferRec As System.Windows.Forms.TabPage
    Friend WithEvents dgvChange As System.Windows.Forms.DataGridView
    Friend WithEvents chkboxChangeShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxChangeClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label48 As System.Windows.Forms.Label
    Friend WithEvents tpgBookRec As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox17 As System.Windows.Forms.GroupBox
    Friend WithEvents butBookPrint2 As System.Windows.Forms.Button
    Friend WithEvents butBookExport2 As System.Windows.Forms.Button
    Friend WithEvents cbBookNotReceivePast As System.Windows.Forms.CheckBox
    Friend WithEvents dgvBookNotReceive As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox16 As System.Windows.Forms.GroupBox
    Friend WithEvents butBookPrint1 As System.Windows.Forms.Button
    Friend WithEvents butBookExport1 As System.Windows.Forms.Button
    Friend WithEvents cbBookReceivePast As System.Windows.Forms.CheckBox
    Friend WithEvents dgvBookReceive As System.Windows.Forms.DataGridView
    Friend WithEvents cbBookPastClass As System.Windows.Forms.CheckBox
    Friend WithEvents cboxBookClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label51 As System.Windows.Forms.Label
    Friend WithEvents tpgPunchRec As System.Windows.Forms.TabPage
    Friend WithEvents dgvAttRec As System.Windows.Forms.DataGridView
    Friend WithEvents butAttPrint2 As System.Windows.Forms.Button
    Friend WithEvents butAttExport2 As System.Windows.Forms.Button
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label47 As System.Windows.Forms.Label
    Friend WithEvents radbutAttShowRange As System.Windows.Forms.RadioButton
    Friend WithEvents radbutAttShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents Label46 As System.Windows.Forms.Label
    Friend WithEvents dgvAttClass As System.Windows.Forms.DataGridView
    Friend WithEvents butAttPrint As System.Windows.Forms.Button
    Friend WithEvents butAttExport As System.Windows.Forms.Button
    Friend WithEvents chkboxAttShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxAttClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label45 As System.Windows.Forms.Label
    Friend WithEvents tpgKBRec As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox15 As System.Windows.Forms.GroupBox
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents tpgKBPay As System.Windows.Forms.TabPage
    Friend WithEvents dgvKBPayRec As System.Windows.Forms.DataGridView
    Friend WithEvents tpgKBShowUp As System.Windows.Forms.TabPage
    Friend WithEvents dgvKBAttendance As System.Windows.Forms.DataGridView
    Friend WithEvents tpgKBBookRec As System.Windows.Forms.TabPage
    Friend WithEvents dgvKBBook As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents butKBDelete As System.Windows.Forms.Button
    Friend WithEvents butKBRemarks As System.Windows.Forms.Button
    Friend WithEvents butKBMod As System.Windows.Forms.Button
    Friend WithEvents dgvKB As System.Windows.Forms.DataGridView
    Friend WithEvents tpgPay As System.Windows.Forms.TabPage
    Friend WithEvents chkboxNoAskPrintReceipt As System.Windows.Forms.CheckBox
    Friend WithEvents grpboxExtra As System.Windows.Forms.GroupBox
    Friend WithEvents lblCheque As System.Windows.Forms.Label
    Friend WithEvents dtpickCheque As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxPayExtraInfo As System.Windows.Forms.TextBox
    Friend WithEvents butPayPrintOpt As System.Windows.Forms.Button
    Friend WithEvents butPayAck As System.Windows.Forms.Button
    Friend WithEvents butPayPrintTogether As System.Windows.Forms.Button
    Friend WithEvents butPayTogether As System.Windows.Forms.Button
    Friend WithEvents butPayOthers As System.Windows.Forms.Button
    Friend WithEvents butPayPrint As System.Windows.Forms.Button
    Friend WithEvents butPayKeep As System.Windows.Forms.Button
    Friend WithEvents butPayBack As System.Windows.Forms.Button
    Friend WithEvents butPayDelete As System.Windows.Forms.Button
    Friend WithEvents butPayModify As System.Windows.Forms.Button
    Friend WithEvents butPaySave As System.Windows.Forms.Button
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents butModDisc As System.Windows.Forms.Button
    Friend WithEvents tboxReceiptRemarks As System.Windows.Forms.TextBox
    Friend WithEvents butReceiptRemarks As System.Windows.Forms.Button
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents tboxDiscountRemarks As System.Windows.Forms.TextBox
    Friend WithEvents butDiscountRemarks As System.Windows.Forms.Button
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents tboxDiscount As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPayDetails As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents chkboxShowPast2 As System.Windows.Forms.CheckBox
    Friend WithEvents dgvClass2 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutMethod5 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMethod4 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMethod3 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMethod2 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMethod1 As System.Windows.Forms.RadioButton
    Friend WithEvents tboxPayToday As System.Windows.Forms.TextBox
    Friend WithEvents Label50 As System.Windows.Forms.Label
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents dtpickMakeupDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents radbutNormal As System.Windows.Forms.RadioButton
    Friend WithEvents radbutMakeup As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxReceiptNum As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tpgInfo As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tboxRemark4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark1 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank1 As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents tboxSchool4 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily4 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth4d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth4m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth4y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool3 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily3 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth3d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth3m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth3y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool2 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily2 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth2d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth2m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth2y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool1 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cboxFamily1 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth1d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth1m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth1y As System.Windows.Forms.TextBox
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents tboxName1 As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents Label44 As System.Windows.Forms.Label
    Friend WithEvents butDelPic As System.Windows.Forms.Button
    Friend WithEvents butAddPic As System.Windows.Forms.Button
    Friend WithEvents butSavePic As System.Windows.Forms.Button
    Friend WithEvents butWebcam As System.Windows.Forms.Button
    Friend WithEvents butChangeId As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutNowUni As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNowHig As System.Windows.Forms.RadioButton
    Friend WithEvents tboxClass As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboxGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboxGrade As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents butUni As System.Windows.Forms.Button
    Friend WithEvents cboxUniversity As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents butHig As System.Windows.Forms.Button
    Friend WithEvents cboxHighSch As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents butJun As System.Windows.Forms.Button
    Friend WithEvents cboxJuniorSch As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents butPri As System.Windows.Forms.Button
    Friend WithEvents cboxPrimarySch As System.Windows.Forms.ComboBox
    Friend WithEvents radbutNowJun As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNowPri As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents butPrint As System.Windows.Forms.Button
    Friend WithEvents butNext As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents butSales As System.Windows.Forms.Button
    Friend WithEvents butClassCancel As System.Windows.Forms.Button
    Friend WithEvents butClassChange As System.Windows.Forms.Button
    Friend WithEvents butSeatNum As System.Windows.Forms.Button
    Friend WithEvents butDelClass As System.Windows.Forms.Button
    Friend WithEvents butReg As System.Windows.Forms.Button
    Friend WithEvents chkboxShowNotOpen As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowOpen As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents dgvClass As System.Windows.Forms.DataGridView
    Friend WithEvents butPrevious As System.Windows.Forms.Button
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents rtboxRemarks As System.Windows.Forms.RichTextBox
    Friend WithEvents butSearch As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cboxSex As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirthD As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirthM As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirthY As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxEngName As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents butDelCard As System.Windows.Forms.Button
    Friend WithEvents butIssueCard As System.Windows.Forms.Button
    Friend WithEvents radbutPotStu As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFormalStu As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxCreateDate As System.Windows.Forms.TextBox
    Friend WithEvents lblCreateDate As System.Windows.Forms.Label
    Friend WithEvents tboxStuName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblStuId As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxAdd1b As System.Windows.Forms.TextBox
    Friend WithEvents tboxAdd2b As System.Windows.Forms.TextBox
    Friend WithEvents tboxClassId As System.Windows.Forms.TextBox
    Friend WithEvents tboxStay As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents tboxDadTitle As System.Windows.Forms.TextBox
    Friend WithEvents tboxRegInfo As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents tboxMumTitle As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents tboxMumMobile As System.Windows.Forms.TextBox
    Friend WithEvents tboxIntroName As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents tboxGradJunior As System.Windows.Forms.TextBox
    Friend WithEvents tboxMumName As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents tboxIc As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents tboxAdd1a As System.Windows.Forms.TextBox
    Friend WithEvents tboxEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents tboxAdd2a As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tboxIntroId As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents tboxMobile As System.Windows.Forms.TextBox
    Friend WithEvents tboxDadMobile As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tboxDadName As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents tboxTel1 As System.Windows.Forms.TextBox
    Friend WithEvents tboxOfficeTel As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents tboxTel2 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tabCtrl As System.Windows.Forms.TabControl
    Friend WithEvents tboxPunchCardNum As System.Windows.Forms.TextBox
    Friend WithEvents Label53 As System.Windows.Forms.Label
    Friend WithEvents tboxEnrollSch As System.Windows.Forms.TextBox
    Friend WithEvents Label54 As System.Windows.Forms.Label
    Friend WithEvents tboxHseeMark As System.Windows.Forms.TextBox
    Friend WithEvents Label55 As System.Windows.Forms.Label
    Friend WithEvents lblImgInfo As System.Windows.Forms.Label
    Friend WithEvents btnDiscCancel As System.Windows.Forms.Button
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tabAssign As System.Windows.Forms.TabPage
    Friend WithEvents tabGrade As System.Windows.Forms.TabPage
    Friend WithEvents Label56 As System.Windows.Forms.Label
    Friend WithEvents chkboxAssignShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents cboxAssignClass As System.Windows.Forms.ComboBox
    Friend WithEvents GroupBox18 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox19 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRefreshAssgin As System.Windows.Forms.Button
    Friend WithEvents dgvAssignPass As System.Windows.Forms.DataGridView
    Friend WithEvents dgvAssignNoPass As System.Windows.Forms.DataGridView
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents GroupBox20 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox21 As System.Windows.Forms.GroupBox
    Friend WithEvents DataGridView2 As System.Windows.Forms.DataGridView
    Friend WithEvents Label57 As System.Windows.Forms.Label
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents ComboBox1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboxPaperClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label58 As System.Windows.Forms.Label
    Friend WithEvents chkboxPaperShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox23 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPaperNoPass As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox22 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPaperPass As System.Windows.Forms.DataGridView
End Class
