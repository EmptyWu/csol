﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCardReissueRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.butFilter = New System.Windows.Forms.Button
        Me.radbutID = New System.Windows.Forms.RadioButton
        Me.radbutName = New System.Windows.Forms.RadioButton
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.radbutDate = New System.Windows.Forms.RadioButton
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label2 = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        Me.mnuExportData.Size = New System.Drawing.Size(68, 20)
        Me.mnuExportData.Text = "匯出資料"
        '
        'tboxID
        '
        Me.tboxID.Location = New System.Drawing.Point(68, 32)
        Me.tboxID.Name = "tboxID"
        Me.tboxID.Size = New System.Drawing.Size(84, 22)
        Me.tboxID.TabIndex = 65
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(68, 20)
        Me.mnuPrint.Text = "列印資料"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(13, 69)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(1004, 495)
        Me.dgv.TabIndex = 61
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuPrint, Me.mnuExportData, Me.mnuClose})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(1027, 24)
        Me.MenuStrip1.TabIndex = 60
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        Me.mnuDetails.Size = New System.Drawing.Size(68, 20)
        Me.mnuDetails.Text = "學生資料"
        '
        'butFilter
        '
        Me.butFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFilter.Location = New System.Drawing.Point(656, 30)
        Me.butFilter.Name = "butFilter"
        Me.butFilter.Size = New System.Drawing.Size(101, 25)
        Me.butFilter.TabIndex = 66
        Me.butFilter.Text = "執行篩選"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'radbutID
        '
        Me.radbutID.AutoSize = True
        Me.radbutID.Location = New System.Drawing.Point(15, 35)
        Me.radbutID.Name = "radbutID"
        Me.radbutID.Size = New System.Drawing.Size(47, 16)
        Me.radbutID.TabIndex = 67
        Me.radbutID.TabStop = True
        Me.radbutID.Text = "學號"
        Me.radbutID.UseVisualStyleBackColor = True
        '
        'radbutName
        '
        Me.radbutName.AutoSize = True
        Me.radbutName.Location = New System.Drawing.Point(166, 35)
        Me.radbutName.Name = "radbutName"
        Me.radbutName.Size = New System.Drawing.Size(47, 16)
        Me.radbutName.TabIndex = 69
        Me.radbutName.TabStop = True
        Me.radbutName.Text = "姓名"
        Me.radbutName.UseVisualStyleBackColor = True
        '
        'tboxName
        '
        Me.tboxName.Location = New System.Drawing.Point(219, 32)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.Size = New System.Drawing.Size(84, 22)
        Me.tboxName.TabIndex = 68
        '
        'radbutDate
        '
        Me.radbutDate.AutoSize = True
        Me.radbutDate.Location = New System.Drawing.Point(320, 35)
        Me.radbutDate.Name = "radbutDate"
        Me.radbutDate.Size = New System.Drawing.Size(47, 16)
        Me.radbutDate.TabIndex = 71
        Me.radbutDate.TabStop = True
        Me.radbutDate.Text = "日期"
        Me.radbutDate.UseVisualStyleBackColor = True
        '
        'dtpickTo
        '
        Me.dtpickTo.Location = New System.Drawing.Point(515, 32)
        Me.dtpickTo.Name = "dtpickTo"
        Me.dtpickTo.Size = New System.Drawing.Size(119, 22)
        Me.dtpickTo.TabIndex = 74
        '
        'dtpickFrom
        '
        Me.dtpickFrom.Location = New System.Drawing.Point(370, 32)
        Me.dtpickFrom.Name = "dtpickFrom"
        Me.dtpickFrom.Size = New System.Drawing.Size(119, 22)
        Me.dtpickFrom.TabIndex = 73
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(498, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(11, 12)
        Me.Label2.TabIndex = 72
        Me.Label2.Text = "~"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'PrintDocument1
        '
        '
        'frmCardReissueRec
        '
        Me.AcceptButton = Me.butFilter
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1027, 623)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radbutDate)
        Me.Controls.Add(Me.radbutName)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.radbutID)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.butFilter)
        Me.Name = "frmCardReissueRec"
        Me.Text = "frmCardReissueRec"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents radbutID As System.Windows.Forms.RadioButton
    Friend WithEvents radbutName As System.Windows.Forms.RadioButton
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents radbutDate As System.Windows.Forms.RadioButton
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
