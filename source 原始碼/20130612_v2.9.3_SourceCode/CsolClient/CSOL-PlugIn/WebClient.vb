﻿Namespace CSOL
    Public Class WebClient
        Inherits System.Net.WebClient
        Private m_CookieContainer As System.Net.CookieContainer
        Private m_UserAgent As String
        Private m_Timeout As Integer

        Public Property CookieContainer() As System.Net.CookieContainer
            Get
                Return Me.m_CookieContainer
            End Get
            Set(ByVal value As System.Net.CookieContainer)
                Me.m_CookieContainer = value
            End Set
        End Property

        Public Property UserAgent() As String
            Get
                Return Me.m_UserAgent
            End Get
            Set(ByVal value As String)
                Me.m_UserAgent = value
            End Set
        End Property

        Public Property Timeout() As Integer
            Get
                Return Me.m_Timeout
            End Get
            Set(ByVal value As Integer)
                Me.m_Timeout = value
            End Set
        End Property

        Public Sub New()
            Me.m_Timeout = -1
            Me.m_UserAgent = "CSOL WebClient"
            Me.m_CookieContainer = New System.Net.CookieContainer()
        End Sub

        Protected Overrides Function GetWebRequest(ByVal address As System.Uri) As System.Net.WebRequest
            Dim request As System.Net.WebRequest = MyBase.GetWebRequest(address)
            If request.GetType().Equals(GetType(System.Net.HttpWebRequest)) Then
                CType(request, System.Net.HttpWebRequest).CookieContainer = Me.m_CookieContainer
                CType(request, System.Net.HttpWebRequest).UserAgent = Me.m_UserAgent
                CType(request, System.Net.HttpWebRequest).Timeout = Me.m_Timeout
            End If

            Return request
        End Function
    End Class
End Namespace
