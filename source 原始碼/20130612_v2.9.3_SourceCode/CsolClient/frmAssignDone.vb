﻿Public Class frmAssignDone
    Private dtAssignment As New DataTable
    Private dtAssignment2 As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstAssignment As New ArrayList

    Private dtData As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmAssignDone
            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)

            InitList()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub InitList()
        Try
            cboxClass.Items.Clear()
            lstClass.Clear()
            cboxSubClass.Items.Clear()
            lstSubClass.Clear()
            cboxAssignment.Items.Clear()
            lstAssignment.Clear()
            cboxAssignment.Text = ""

            If chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If GetDateEnd(dtClass.Rows(index).Item(c_EndColumnName)) >= Now Then
                        cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            End If

            If lstClass.Count > 0 Then
                cboxClass.SelectedIndex = 0
            Else
                cboxClass.SelectedIndex = -1
            End If

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshScAndAssignList()
    End Sub

    Private Sub RefreshScAndAssignList()
        Try
            cboxSubClass.Items.Clear()
            lstSubClass.Clear()
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            If lstClass.Count = 0 Then
                Exit Sub
            End If
            intClass = lstClass(cboxClass.SelectedIndex)

            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClass.Add(row.Item(c_IDColumnName))
                        cboxSubClass.Items.Add(row.Item(c_NameColumnName))
                    End If
                Next
                dtAssignment = objCsol.GetClassAssignmentList(intClass)
                dtAssignment2 = objCsol.GetClassAssignmentList(intClass).DefaultView.ToTable(True, c_IDColumnName, _
                                                                    c_NameColumnName, c_EndDateColumnName)
            End If

            cboxSubClass.SelectedIndex = 0
            RefreshAssignmentList()

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub


    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        RefreshAssignmentList()
    End Sub

    Private Sub RefreshAssignmentList()
        Try
            cboxAssignment.Items.Clear()
            lstAssignment.Clear()
            cboxAssignment.Text = ""
            dgv.DataSource = Nothing
            If cboxSubClass.SelectedIndex > -1 Then
                If cboxSubClass.SelectedIndex = 0 Then
                    If chkboxShowPastAssign.Checked Then
                        For Each row As DataRow In dtAssignment2.Rows
                            cboxAssignment.Items.Add(row.Item(c_NameColumnName))
                            lstAssignment.Add(row.Item(c_IDColumnName))
                        Next
                    Else
                        For Each row As DataRow In dtAssignment2.Rows
                            If GetDateEnd(row.Item(c_EndDateColumnName)) >= Now Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    End If
                Else
                    Dim id As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                    If chkboxShowPastAssign.Checked Then
                        For Each row As DataRow In dtAssignment.Rows
                            If row.Item(c_SubClassIDColumnName) = id Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    Else
                        For Each row As DataRow In dtAssignment.Rows
                            If GetDateEnd(row.Item(c_EndDateColumnName)) >= Now And row.Item(c_SubClassIDColumnName) = id Then
                                cboxAssignment.Items.Add(row.Item(c_NameColumnName))
                                lstAssignment.Add(row.Item(c_IDColumnName))
                            End If
                        Next
                    End If
                End If

                If lstAssignment.Count > 0 Then
                    cboxAssignment.SelectedIndex = 0
                Else
                    dgv.DataSource = Nothing
                End If
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub cboxAssignment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxAssignment.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub RefreshData()
        Try
            If cboxAssignment.SelectedIndex > -1 Then
                Dim Assignment As Integer = lstAssignment(cboxAssignment.SelectedIndex)
                Dim lst As New ArrayList
                Dim sc As Integer = 0
                If cboxSubClass.SelectedIndex = 0 Then
                    For i As Integer = 1 To lstSubClass.Count - 1
                        sc = lstSubClass(i)
                        If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                            lst.Add(sc)
                        End If
                    Next
                ElseIf cboxSubClass.SelectedIndex > 0 Then
                    sc = lstSubClass(cboxSubClass.SelectedIndex)
                    If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                       c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                        lst.Add(sc)
                    End If
                End If
                dtData = objCsol.GetStuAssignments(Assignment, lst).DefaultView.ToTable(True, c_SubClassIDColumnName, _
                                                                                                c_IDColumnName, _
                                                                                                c_NameColumnName, _
                                                                                                c_SeatNumColumnName, _
                                                                                                c_SchoolColumnName, _
                                                                                                c_MarkColumnName, _
                                                                                                c_SubClassNameColumnName)

                If cboxSubClass.SelectedIndex > 0 Then
                    dtData.DefaultView.RowFilter = c_SubClassIDColumnName & "=" & lstSubClass(cboxSubClass.SelectedIndex).ToString
                End If
                dgv.DataSource = dtData.DefaultView
                LoadColumnText()

            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub LoadColumnText()
        Try
            For Each col As DataGridViewColumn In dgv.Columns
                col.Visible = False
            Next
            If dgv.Rows.Count > 0 Then
                dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_IDColumnName).Visible = True
                dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID

                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name

                dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_SeatNumColumnName).Visible = True
                dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum

                'here the school column is only for text display purpose
                dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
                dgv.Columns.Item(c_SchoolColumnName).Visible = True
                dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.passOrNot
                For Each row As DataGridViewRow In dgv.Rows
                    If row.Cells(c_MarkColumnName).Value > 0 Then
                        row.Cells(c_SchoolColumnName).Value = My.Resources.pass
                    Else
                        row.Cells(c_SchoolColumnName).Value = ""
                    End If
                Next
                If lstSubClass.Count > 0 And cboxSubClass.SelectedIndex = 0 Then
                    dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 4
                    dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                    dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
                End If
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub frmAssignDone_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmAssignDone_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmAssignDone_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim intC As Integer
        Dim intR As Integer
        Dim stuid As String = ""
        Dim Assignment As Integer
        Dim mark As Integer = 0
        Dim remarks As String = ""
        Try
            intC = e.ColumnIndex
            intR = e.RowIndex
            If intC = 4 Then
                stuid = dgv.Rows(intR).Cells(c_IDColumnName).Value
                Assignment = lstAssignment(cboxAssignment.SelectedIndex)
                remarks = dgv.Rows(intR).Cells(c_SchoolColumnName).Value.ToString.Trim
                If remarks.Length > 0 Then
                    dgv.Rows(intR).Cells(c_SchoolColumnName).Value = ""
                    mark = c_Fail
                Else
                    dgv.Rows(intR).Cells(c_SchoolColumnName).Value = My.Resources.pass
                    mark = c_Pass
                End If
            End If

        Catch ex As Exception
            ShowInfo(My.Resources.msgCheckDataFormat)
            AddErrorLog(ex.ToString)
            Exit Sub
        End Try

        Try
            If stuid.Length = 8 Then
                objCsol.UpdateStuAssignment(stuid, Assignment, Now, mark)
            End If

        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try

    End Sub

    '20100511 sherry 13
    Private Sub RefreshStuGrade(ByVal stuid As String, ByVal mark As Integer)
        Dim intR As Integer = -1
        For i As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(i).Cells(c_IDColumnName).Value.ToString.Trim.Equals(stuid) Then
                intR = i
                Exit For
            End If
        Next

        If intR > -1 Then

            If mark = c_Pass Then
                dgv.Rows(intR).Cells(c_SchoolColumnName).Value = My.Resources.pass
                dgv.Rows(intR).Cells(c_MarkColumnName).Value = mark
            Else
                dgv.Rows(intR).Cells(c_SchoolColumnName).Value = ""
                dgv.Rows(intR).Cells(c_MarkColumnName).Value = 0
            End If
        End If

    End Sub

    '20100511 sherry 13
    Private Sub DelStuGrade(ByVal stuid As String)
        Dim intR As Integer = -1
        For i As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(i).Cells(c_IDColumnName).Value.ToString.Trim.Equals(stuid) Then
                intR = i
                Exit For
            End If
        Next

        If intR > -1 Then
            dgv.Rows(intR).Cells(c_MarkColumnName).Value = 0
            dgv.Rows(intR).Cells(c_SchoolColumnName).Value = ""
        End If

    End Sub

    Private Sub tboxID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged
        If tboxID.Text.Trim.Length = 8 Then
            Dim stuid As String = tboxID.Text.Trim
            tboxName.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_IDColumnName) = stuid Then
                    tboxIDName.Text = row.Item(c_NameColumnName)
                    flag = True
                    Exit For
                End If
            Next
            If flag = False Then
                ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
                Exit Sub
            End If
            tboxIDMark.Focus()
        End If
    End Sub

    Private Sub tboxName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxName.TextChanged
        If tboxName.Text.Trim.Length > 1 Then
            Dim stuname As String = tboxName.Text.Trim
            tboxNameID.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_NameColumnName) = stuname Then
                    tboxNameID.Text = row.Item(c_IDColumnName)
                    tboxNameMark.Focus()
                    flag = True
                    Exit For
                End If
            Next

        End If
    End Sub

    Private Sub tboxSeatNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxSeatNum.TextChanged
        If tboxSeatNum.Text.Trim.Length > 1 Then
            Dim seat As String = tboxSeatNum.Text.Trim.ToUpper
            tboxSeatName.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_SeatNumColumnName).ToString.Trim.ToUpper = seat Then
                    tboxSeatName.Text = row.Item(c_NameColumnName)
                    tboxSeatMark.Focus()
                    flag = True
                    Exit For
                End If
            Next

        End If
    End Sub

    Private Sub tboxIDMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxIDMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeID()
            tboxID.Clear()
            tboxIDMark.Clear()
            tboxIDName.Clear()
            tboxID.Focus()
        End If

    End Sub

    Private Sub UpdGradeID()
        Try
            Dim flag As Boolean = False
            Dim Assignment As Integer = lstAssignment(cboxAssignment.SelectedIndex)
            Dim mark As Integer = CInt(tboxIDMark.Text.Trim)
            Dim stuid As String = tboxID.Text.Trim

            If stuid.Length = 8 Then
                For Each row As DataRow In dtData.Rows
                    If row.Item(c_IDColumnName) = stuid Then
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
                    Exit Sub
                End If

                If mark <> 1 Then
                    mark = 2
                End If

                objCsol.UpdateStuAssignment(stuid, Assignment, Now, mark)
                '20100512 sherry 14
                RefreshStuGrade(stuid, mark)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub UpdGradeName()
        Try
            Dim flag As Boolean = False
            Dim Assignment As Integer = lstAssignment(cboxAssignment.SelectedIndex)
            Dim mark As Integer = CInt(tboxNameMark.Text.Trim)
            Dim stuid As String = tboxNameID.Text.Trim

            If stuid.Length = 8 Then
                If mark <> 1 Then
                    mark = 2
                End If

                objCsol.UpdateStuAssignment(stuid, Assignment, Now, mark)
                '20100512 sherry 14
                RefreshStuGrade(stuid, mark)
            Else
                ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub UpdGradeSeat()
        Try
            Dim flag As Boolean = False
            Dim Assignment As Integer = lstAssignment(cboxAssignment.SelectedIndex)
            Dim mark As Integer = CInt(tboxSeatMark.Text.Trim)
            Dim seat As String = tboxSeatNum.Text.Trim.ToUpper
            Dim stuid As String = ""

            If seat.Length > 1 Then
                For Each row As DataRow In dtData.Rows
                    If row.Item(c_SeatNumColumnName).ToString.ToUpper.Trim = seat Then
                        stuid = row.Item(c_IDColumnName)
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Or stuid.Length <> 8 Then
                    ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
                    Exit Sub
                End If

                If mark <> 1 Then
                    mark = 2
                End If

                objCsol.UpdateStuAssignment(stuid, Assignment, Now, mark)
                '20100512 sherry 14
                RefreshStuGrade(stuid, mark)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub tboxNameMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxNameMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeName()
            tboxName.Clear()
            tboxNameMark.Clear()
            tboxNameID.Clear()
            tboxName.Focus()
        End If
    End Sub

    Private Sub tboxSeatMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxSeatMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeSeat()
            tboxSeatMark.Clear()
            tboxSeatName.Clear()
            tboxSeatNum.Clear()
            tboxSeatNum.Focus()
        End If
    End Sub

    Private Sub chkboxShowPastAssign_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPastAssign.CheckedChanged
        RefreshAssignmentList()
    End Sub

End Class