﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetPrintSize
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SetPrintSize))
        Me.Label1 = New System.Windows.Forms.Label
        Me.radbut9 = New System.Windows.Forms.RadioButton
        Me.radbut12 = New System.Windows.Forms.RadioButton
        Me.radbut14 = New System.Windows.Forms.RadioButton
        Me.radbut16 = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Name = "Label1"
        '
        'radbut9
        '
        resources.ApplyResources(Me.radbut9, "radbut9")
        Me.radbut9.Name = "radbut9"
        Me.radbut9.TabStop = True
        Me.radbut9.UseVisualStyleBackColor = True
        '
        'radbut12
        '
        resources.ApplyResources(Me.radbut12, "radbut12")
        Me.radbut12.Name = "radbut12"
        Me.radbut12.TabStop = True
        Me.radbut12.UseVisualStyleBackColor = True
        '
        'radbut14
        '
        resources.ApplyResources(Me.radbut14, "radbut14")
        Me.radbut14.Name = "radbut14"
        Me.radbut14.TabStop = True
        Me.radbut14.UseVisualStyleBackColor = True
        '
        'radbut16
        '
        resources.ApplyResources(Me.radbut16, "radbut16")
        Me.radbut16.Name = "radbut16"
        Me.radbut16.TabStop = True
        Me.radbut16.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.Red
        Me.Label2.Name = "Label2"
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2SetPrintSize
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.radbut16)
        Me.Controls.Add(Me.radbut14)
        Me.Controls.Add(Me.radbut12)
        Me.Controls.Add(Me.radbut9)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SetPrintSize"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radbut9 As System.Windows.Forms.RadioButton
    Friend WithEvents radbut12 As System.Windows.Forms.RadioButton
    Friend WithEvents radbut14 As System.Windows.Forms.RadioButton
    Friend WithEvents radbut16 As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
