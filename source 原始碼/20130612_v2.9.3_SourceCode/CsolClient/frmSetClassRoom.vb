﻿Public Class frmSetClassRoom
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private dsData As New DataSet
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmSetClassRoom
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmSetClassRoom
        RefreshData()

    End Sub

    Private Sub frmSetClassRoom_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetClassRoom_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        dsData = objCsol.ListClassRooms()
        dtMaster = dsData.Tables(c_ClassRoomListTableName)
        dtDetails = dsData.Tables(c_ClassRoomUsageListTableName)

        dtMaster.Columns.Add(c_SeatCntColumnName, GetType(System.Int32))
        Dim i As Integer = 0
        Dim l As Integer = 0
        Dim c As Integer = 0
        Dim p As Integer = 0
        Dim s As Integer = 0
        For index As Integer = 0 To dtMaster.Rows.Count - 1
            l = dtMaster.Rows(index).Item(c_LineCntColumnName)
            c = dtMaster.Rows(index).Item(c_ColumnCntColumnName)
            p = dtMaster.Rows(index).Item(c_PwCntColumnName)
            'c = c - p
            s = dtMaster.Rows(index).Item(c_SeatNACntColumnName)
            i = c * l - s
            dtMaster.Rows(index).Item(c_SeatCntColumnName) = i
        Next

        dgvMaster.DataSource = dtMaster
        If dgvMaster.RowCount > 0 Then
            dgvMaster.CurrentCell = dgvMaster.Rows.Item(0).Cells(c_NameColumnName)
        End If
        dtDetails.Columns.Add(c_DayOfWeekNameColumnName, GetType(System.String))
        Dim wd As Integer = 0
        For Each row As DataRow In dtDetails.Rows
            wd = GetIntValue(row.Item(c_DayOfWeekColumnName).ToString)
            row.Item(c_DayOfWeekNameColumnName) = GetWeekDayName(wd, 0)
        Next
        'If dtMaster.Rows.Count > 0 Then    
        '    dtDetails.DefaultView.RowFilter = c_ClassRoomIDColumnName & "=" & dtMaster.Rows(0).Item(c_IDColumnName)
        'Else
        '    dtDetails.DefaultView.RowFilter = c_ClassRoomIDColumnName & "=-1"
        'End If
        'dgvDetails.DataSource = dtDetails.DefaultView
        Dim strF As String = ""
        If dtMaster.Rows.Count > 0 Then
            strF = c_ClassRoomIDColumnName & "=" & dtMaster.Rows(0).Item(c_IDColumnName)
        Else
            strF = c_ClassRoomIDColumnName & "=-1"
        End If

        If Not chkboxShowPast.Checked Then
            strF = strF & " AND " & c_ClassEndColumnName & _
                     ">'" & Now.ToString & "'"
        End If

        dtDetails.DefaultView.RowFilter = strF
        dgvDetails.DataSource = dtDetails.DefaultView
        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        dgvMaster.Columns.Item(c_IDColumnName).Visible = False
        dgvMaster.Columns.Item(c_DisStyleColumnName).Visible = False
        dgvMaster.Columns.Item(c_SeatNACntColumnName).Visible = False
        dgvMaster.Columns.Item(c_PwCntColumnName).Visible = False
        dgvMaster.Columns.Item(c_NameColumnName).HeaderText = My.Resources.classRoomName
        dgvMaster.Columns.Item(c_LineCntColumnName).HeaderText = My.Resources.rowCnt
        dgvMaster.Columns.Item(c_ColumnCntColumnName).HeaderText = My.Resources.colCnt
        dgvMaster.Columns.Item(c_SeatCntColumnName).HeaderText = My.Resources.seatCnt

        dgvDetails.Columns.Item(c_ClassRoomIDColumnName).Visible = False
        dgvDetails.Columns.Item(c_DayOfWeekColumnName).Visible = False
        dgvDetails.Columns.Item(c_DayOfWeekNameColumnName).DisplayIndex = 0
        dgvDetails.Columns.Item(c_DayOfWeekNameColumnName).HeaderText = My.Resources.weekDay
        dgvDetails.Columns.Item(c_StartColumnName).DisplayIndex = 1
        dgvDetails.Columns.Item(c_StartColumnName).HeaderText = My.Resources.sessionStart

        'dgvDetails.Columns.Item(c_StartColumnName).Visible = False
        dgvDetails.Columns.Item(c_StartColumnName).DefaultCellStyle.Format = "HH:mm"

        dgvDetails.Columns.Item(c_EndColumnName).DisplayIndex = 2
        dgvDetails.Columns.Item(c_EndColumnName).HeaderText = My.Resources.sessionEnd

        'dgvDetails.Columns.Item(c_EndColumnName).Visible = False
        dgvDetails.Columns.Item(c_EndColumnName).DefaultCellStyle.Format = "HH:mm"

        dgvDetails.Columns.Item(c_ClassNameColumnName).DisplayIndex = 3
        dgvDetails.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.nclass
        dgvDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 4
        dgvDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClass
        dgvDetails.Columns.Item(c_ClassStartColumnName).DisplayIndex = 5
        dgvDetails.Columns.Item(c_ClassStartColumnName).HeaderText = My.Resources.startDate
        dgvDetails.Columns.Item(c_ClassEndColumnName).DisplayIndex = 6
        dgvDetails.Columns.Item(c_ClassEndColumnName).HeaderText = My.Resources.endDate
        dgvDetails.Columns.Item(c_ClassEndColumnName).DefaultCellStyle.Format = "d"



    End Sub

    Private Sub frmSetClassRoom_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intId = dgvMaster.SelectedRows(0).Cells(c_IDColumnName).Value
            If intId > -1 Then
                'dtDetails.DefaultView.RowFilter = c_ClassRoomIDColumnName + "=" + intId.ToString
                'dgvDetails.DataSource = dtDetails.DefaultView
                Dim strF As String = ""
                strF = c_ClassRoomIDColumnName & "=" & intId.ToString
                If Not chkboxShowPast.Checked Then
                    strF = strF & " AND " & c_ClassEndColumnName & _
                             ">'" & Now.ToString & "'"
                End If
                dtDetails.DefaultView.RowFilter = strF
                dgvDetails.DataSource = dtDetails.DefaultView
            Else
                dtDetails.DefaultView.RowFilter = ""
                dgvDetails.DataSource = dtDetails.DefaultView
            End If
        End If
    End Sub

    Private Sub mnuAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuAdd.Click
        If Not frmMain.CheckAuth(70) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        frmMain.ShowAddClassRoom()
    End Sub

    Private Sub mnuModify_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuModify.Click
        If Not frmMain.CheckAuth(71) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvMaster.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvMaster.SelectedRows(0).Index
            Dim id As Integer = dtMaster.Rows(idx).Item(c_IDColumnName)
            Dim name As String = dtMaster.Rows(idx).Item(c_NameColumnName)
            Dim row As Integer = dtMaster.Rows(idx).Item(c_LineCntColumnName)
            Dim col As Integer = dtMaster.Rows(idx).Item(c_ColumnCntColumnName)
            Dim style As Integer = dtMaster.Rows(idx).Item(c_DisStyleColumnName)
            frmMain.ShowModifyClassRoom(id, name, row, col, style)
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(72) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvMaster.SelectedRows.Count > 0 Then
            Dim idx As Integer = dgvMaster.SelectedRows(0).Index
            Dim id As Integer = dtMaster.Rows(idx).Item(c_IDColumnName)
            objCsol.DeleteClassRoom(id)
            RefreshData()
        End If
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshData()
    End Sub
End Class