﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuSearchMulClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuSearchMulClass))
        Me.chkboxShowCancel = New System.Windows.Forms.CheckBox
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPayRemind = New System.Windows.Forms.ToolStripMenuItem
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.btnExc = New System.Windows.Forms.Button
        Me.butSelNone = New System.Windows.Forms.Button
        Me.chklstSubClass = New System.Windows.Forms.CheckedListBox
        Me.butSelAll = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.chklstClass = New System.Windows.Forms.CheckedListBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.radbutShowAll = New System.Windows.Forms.RadioButton
        Me.radbutShowOne = New System.Windows.Forms.RadioButton
        Me.radbutShow1Sbj = New System.Windows.Forms.RadioButton
        Me.cboxSubject = New System.Windows.Forms.ComboBox
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.tboxCount = New System.Windows.Forms.TextBox
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'chkboxShowCancel
        '
        resources.ApplyResources(Me.chkboxShowCancel, "chkboxShowCancel")
        Me.chkboxShowCancel.Name = "chkboxShowCancel"
        Me.chkboxShowCancel.UseVisualStyleBackColor = True
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        resources.ApplyResources(Me.mnuNote, "mnuNote")
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuSelectCol, Me.mnuDelete, Me.mnuPrint, Me.mnuExportData, Me.mnuPayRemind, Me.mnuNote, Me.mnuClose})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        resources.ApplyResources(Me.mnuExportData, "mnuExportData")
        '
        'mnuPayRemind
        '
        Me.mnuPayRemind.Name = "mnuPayRemind"
        resources.ApplyResources(Me.mnuPayRemind, "mnuPayRemind")
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnExc)
        Me.GroupBox3.Controls.Add(Me.butSelNone)
        Me.GroupBox3.Controls.Add(Me.chklstSubClass)
        Me.GroupBox3.Controls.Add(Me.butSelAll)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'btnExc
        '
        resources.ApplyResources(Me.btnExc, "btnExc")
        Me.btnExc.Name = "btnExc"
        Me.btnExc.UseVisualStyleBackColor = True
        '
        'butSelNone
        '
        resources.ApplyResources(Me.butSelNone, "butSelNone")
        Me.butSelNone.Name = "butSelNone"
        Me.butSelNone.UseVisualStyleBackColor = True
        '
        'chklstSubClass
        '
        Me.chklstSubClass.CheckOnClick = True
        Me.chklstSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstSubClass, "chklstSubClass")
        Me.chklstSubClass.Name = "chklstSubClass"
        '
        'butSelAll
        '
        resources.ApplyResources(Me.butSelAll, "butSelAll")
        Me.butSelAll.Name = "butSelAll"
        Me.butSelAll.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chklstClass)
        Me.GroupBox1.Controls.Add(Me.chkboxShowPast)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'chklstClass
        '
        Me.chklstClass.CheckOnClick = True
        Me.chklstClass.FormattingEnabled = True
        resources.ApplyResources(Me.chklstClass, "chklstClass")
        Me.chklstClass.Name = "chklstClass"
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'radbutShowAll
        '
        resources.ApplyResources(Me.radbutShowAll, "radbutShowAll")
        Me.radbutShowAll.Checked = True
        Me.radbutShowAll.Name = "radbutShowAll"
        Me.radbutShowAll.TabStop = True
        Me.radbutShowAll.UseVisualStyleBackColor = True
        '
        'radbutShowOne
        '
        resources.ApplyResources(Me.radbutShowOne, "radbutShowOne")
        Me.radbutShowOne.Name = "radbutShowOne"
        Me.radbutShowOne.UseVisualStyleBackColor = True
        '
        'radbutShow1Sbj
        '
        resources.ApplyResources(Me.radbutShow1Sbj, "radbutShow1Sbj")
        Me.radbutShow1Sbj.Name = "radbutShow1Sbj"
        Me.radbutShow1Sbj.UseVisualStyleBackColor = True
        '
        'cboxSubject
        '
        Me.cboxSubject.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubject, "cboxSubject")
        Me.cboxSubject.Name = "cboxSubject"
        '
        'PrintDocument1
        '
        '
        'tboxCount
        '
        resources.ApplyResources(Me.tboxCount, "tboxCount")
        Me.tboxCount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tboxCount.Name = "tboxCount"
        Me.tboxCount.ReadOnly = True
        '
        'frmStuSearchMulClass
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.tboxCount)
        Me.Controls.Add(Me.cboxSubject)
        Me.Controls.Add(Me.radbutShow1Sbj)
        Me.Controls.Add(Me.radbutShowOne)
        Me.Controls.Add(Me.radbutShowAll)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.chkboxShowCancel)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgv)
        Me.Name = "frmStuSearchMulClass"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents chkboxShowCancel As System.Windows.Forms.CheckBox
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPayRemind As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents butSelNone As System.Windows.Forms.Button
    Friend WithEvents chklstSubClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents butSelAll As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstClass As System.Windows.Forms.CheckedListBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents radbutShowAll As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShowOne As System.Windows.Forms.RadioButton
    Friend WithEvents radbutShow1Sbj As System.Windows.Forms.RadioButton
    Friend WithEvents cboxSubject As System.Windows.Forms.ComboBox
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents tboxCount As System.Windows.Forms.TextBox
    Friend WithEvents btnExc As System.Windows.Forms.Button
End Class
