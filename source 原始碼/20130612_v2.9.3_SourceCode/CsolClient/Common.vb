﻿Imports System.Configuration
Imports System.Xml
Imports System
Imports System.IO

Module Common
    Private intDisStyle As Integer
    Private strPrinter As String = ""
    Private strClassTitle As String = ""
    Private intPrintSize As Integer = 9
    Private strCfgFilePath As String
    Private strCardReader As String

    Sub AddErrorLog(ByVal msg As String)
        Try
            Dim oWrite As New StreamWriter(c_LogFileName, True)

            If msg.Length > 0 Then
                oWrite.WriteLine()
                oWrite.WriteLine(Now.ToString)
                oWrite.WriteLine(msg)
                oWrite.Close()
            End If
        Catch ex As Exception

        End Try

    End Sub

    Sub ShowInfo(ByVal msg As String)
        MsgBox(msg, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Sub SetCfgFilePath()
        strCfgFilePath = Application.StartupPath & c_remoteClientCfgFilePath
    End Sub

    Function GetCfgFilePath() As String
        Return strCfgFilePath
    End Function

    Function GetDbString(ByVal obj As Object) As String
        If obj Is Nothing Then
            Return ""
        End If
        If DBNull.Value.Equals(obj) Then
            Return ""
        Else
            Return obj.ToString.Trim
        End If
    End Function

    Function GetDbDate(ByVal obj As Object) As Date
        If DBNull.Value.Equals(obj) Then
            Return Now
        Else
            Return CDate(obj)
        End If
    End Function

    Function GetDbSingle(ByVal obj As Object) As Single
        If DBNull.Value.Equals(obj) Then
            Return c_NullMark
        Else
            If IsNumeric(obj) Then
                Return CSng(obj)
            Else
                Return c_NullMark
            End If
        End If
    End Function

    Function GetMinDate() As Date
        Dim d As Date = New Date(1900, 1, 1)
        Return d
    End Function

    Sub SetDefaultPrinter(ByVal strDevice As String)
        If strDevice.Length > 0 Then
            Try
                Const strCls = "Win32_Printer" ' WMI Class
                GetObject("winmgmts:").InstancesOf(strCls)(strCls & ".DeviceID=""" & strDevice & """").SetDefaultPrinter()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Sub ExportDgvToExcel(ByRef dgv1 As DataGridView)
        If dgv1 Is Nothing Then
            Exit Sub
        End If

        If dgv1.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim clsExcelReport As New clsExcelReport

        Dim dgv As New DataGridView
        Dim i As Integer = 0
        Dim dgvNewCol As New DataGridViewColumn

        For Each col As DataGridViewColumn In dgv1.Columns
            If col.Visible = True Then
                i = i + 1
            End If
        Next

        For j As Integer = 0 To i
            For Each col As DataGridViewColumn In dgv1.Columns
                If col.DisplayIndex = j And col.Visible = True Then
                    dgvNewCol = New DataGridViewColumn()
                    dgvNewCol = col.Clone
                    dgvNewCol.Name = col.HeaderText
                    dgv.Columns.Add(dgvNewCol)
                    Exit For
                End If
            Next
        Next
        Dim newRow As New DataGridViewRow
        Dim newCell As DataGridViewCell
        Dim colIdx As Integer
        For Each row As DataGridViewRow In dgv1.Rows
            newRow = New DataGridViewRow
            For index As Integer = 0 To i
                For Each cell As DataGridViewCell In row.Cells
                    colIdx = cell.ColumnIndex
                    If dgv1.Columns(colIdx).DisplayIndex = index And _
                        dgv1.Columns(colIdx).Visible = True Then
                        newCell = cell.Clone
                        newCell.Value = cell.Value
                        newRow.Cells.Add(newCell)
                        Exit For
                    End If
                Next
            Next
            dgv.Rows.Add(newRow)
        Next

        clsExcelReport.openExcelReport(dgv, True, False)
    End Sub

    Sub ExportDgvToExcel2(ByRef dgv1 As DataGridView)
        If dgv1 Is Nothing Then
            Exit Sub
        End If
        If dgv1.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim clsExcelReport As New clsExcelReport

        Dim dgv As New DataGridView
        Dim i As Integer = 0
        Dim dgvNewCol As New DataGridViewColumn

        For Each col As DataGridViewColumn In dgv1.Columns
            If col.Visible = True Then
                i = i + 1
            End If
        Next

        For j As Integer = 0 To i - 1
            For Each col As DataGridViewColumn In dgv1.Columns
                If col.DisplayIndex = j And col.Visible = True Then
                    dgvNewCol = New DataGridViewColumn()
                    dgvNewCol = col.Clone
                    dgvNewCol.Name = col.HeaderText
                    dgv.Columns.Add(dgvNewCol)
                    Exit For
                End If
            Next
        Next
        Dim newRow As New DataGridViewRow
        Dim newCell As DataGridViewCell
        Dim colIdx As Integer
        For Each row As DataGridViewRow In dgv1.Rows
            newRow = New DataGridViewRow
            For index As Integer = 0 To i - 1
                For Each cell As DataGridViewCell In row.Cells
                    colIdx = cell.ColumnIndex
                    If dgv1.Columns(colIdx).DisplayIndex = index And _
                        dgv1.Columns(colIdx).Visible = True Then
                        newCell = cell.Clone
                        newCell.Value = cell.Value

                        If Not newCell.Value = Nothing Then                                                                         '100316
                            If newCell.Value.ToString.Substring(1, 1) = "0" Or newCell.Value.ToString.Substring(1, 1) = "1" Or _
                            newCell.Value.ToString.Substring(1, 1) = "2" Or newCell.Value.ToString.Substring(1, 1) = "3" Then
                                newCell.Value = " "
                            End If
                        End If                                                                                                      '100316

                        newRow.Cells.Add(newCell)
                        Exit For
                    End If
                Next
            Next
            dgv.Rows.Add(newRow)
        Next

        clsExcelReport.openExcelReport(dgv, True, False)
    End Sub

    Sub GetPositiveIndex(ByRef i As Integer)
        If i < 1 Then
            i = 0
        Else
            i = i - 1
        End If
    End Sub

    Function GetIntValue(ByVal s As Object) As Integer
        Dim i As Integer = 0
        If Not TypeOf s Is DBNull Then
            If Not s.ToString.Trim = "" Then
                If IsNumeric(s.ToString.Trim) Then
                    i = CInt(s.ToString.Trim)
                End If
            End If
        End If

        Return i
    End Function

    Function GetSngValue(ByVal s As Object) As String
        Dim i As String = 0
        If Not TypeOf s Is DBNull Then
            If Not s.ToString.Trim = "" Then
                If IsNumeric(s.ToString.Trim) Then
                    i = CSng(s.ToString.Trim).ToString()
                End If
            Else
                i = ""
            End If
        End If

        Return i.ToString()
    End Function

    Function CheckFileExist(ByVal strPath As String) As Boolean
        If Dir$(strPath) <> "" Then
            CheckFileExist = True
        Else
            CheckFileExist = False
        End If
    End Function

    Sub LoadConfigPrinter()
        Dim xdConfig As New XmlDocument
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                strPrinter = xnTarget.Attributes("name").Value
                CreateObject("WScript.Network").SetDefaultPrinter(strPrinter)
                Exit For
            End If
        Next
    End Sub

    Sub LoadConfigData()
        Dim xdConfig As New XmlDocument
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                strPrinter = xnTarget.Attributes("name").Value
                CreateObject("WScript.Network").SetDefaultPrinter(strPrinter)
            End If
            If (xnTarget.Name.ToUpper = "CLASS") Then
                strClassTitle = xnTarget.Attributes("name").Value
            End If
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                intPrintSize = CInt(xnTarget.Attributes("size").Value)
            End If
            If (xnTarget.Name.ToUpper = "RFREADER") Then
                strCardReader = xnTarget.Attributes("type").Value
            End If
        Next

    End Sub

    Sub LoadClassTitle()
        Dim xdConfig As New XmlDocument
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "CLASS") Then
                strClassTitle = xnTarget.Attributes("name").Value
                Exit For
            End If
        Next
    End Sub

    Sub LoadPrintSize()
        Dim xdConfig As New XmlDocument
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                intPrintSize = CInt(xnTarget.Attributes("size").Value)
                Exit For
            End If
        Next
    End Sub

    Sub SetPrintSize(ByVal i As Integer)
        Dim xdConfig As New XmlDocument
        intPrintSize = i
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                xnTarget.Attributes("size").Value = intPrintSize.ToString
                xdConfig.Save(GetCfgFilePath)
                Exit For
            End If
        Next
    End Sub

    Function GetPrintSize() As Integer
        Return intPrintSize
    End Function

    Function GetClassTitle() As String
        Return "上課證交換憑證印花稅總繳4801030177"
    End Function

    Function GetPhotoPath() As String
        Dim xdConfig As New XmlDocument
        Dim s As String = ""
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PHOTOPATH") Then
                s = xnTarget.Attributes("path").Value
                Exit For
            End If
        Next
        Return s
    End Function

    Function GetConfigPrinter() As String
        Return strPrinter
    End Function

    Sub SetConfigPrinter(ByVal s As String)
        Dim xdConfig As New XmlDocument
        strPrinter = s
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "PRINTER") Then
                xnTarget.Attributes("name").Value = strPrinter
                xdConfig.Save(GetCfgFilePath)
                Exit For
            End If
        Next
        CreateObject("WScript.Network").SetDefaultPrinter(s)
    End Sub

    Function GetConfigCardReader() As String
        Return strCardReader
    End Function

    Sub SetConfigCardReader(ByVal s As String)
        Dim xdConfig As New XmlDocument
        strCardReader = s
        xdConfig.Load(GetCfgFilePath)

        Dim xnTarget As XmlNode
        For Each xnTarget In xdConfig.SelectSingleNode("/configuration").ChildNodes
            If (xnTarget.Name.ToUpper = "RFREADER") Then
                xnTarget.Attributes("type").Value = strCardReader
                xdConfig.Save(GetCfgFilePath)
                Exit For
            End If
        Next

    End Sub

    Sub SetDisStyle(ByVal s As Integer)
        intDisStyle = s
    End Sub

    Function GetDisStyle() As Integer
        Return intDisStyle
    End Function

    Function GetTimeString(ByVal d As Date) As String
        Return d.ToString("HH:mm:ss")
    End Function

    ' Return the number of days in the specified month.
    Function dhDaysInMonth(ByVal dtmDate As Date) As Integer
        dhDaysInMonth = (DateSerial(dtmDate.Year, _
                        dtmDate.Month + 1, 1) - _
                        DateSerial(dtmDate.Year, dtmDate.Month, 1)).TotalDays
    End Function

    Function GetDateStart(ByVal dtmDate As Date) As Date
        GetDateStart = New Date(dtmDate.Year, dtmDate.Month, dtmDate.Day, 0, 0, 1)
    End Function
    Function GetDate(ByVal dtmDate As Date) As Date

        GetDate = New Date(dtmDate.Year, dtmDate.Month, dtmDate.Day)

    End Function

    Function GetDateEnd(ByVal dtmDate As Date) As Date
        GetDateEnd = New Date(dtmDate.Year, dtmDate.Month, dtmDate.Day, 23, 59, 0)
    End Function

    Function GetROCYear(ByVal dtmDate As Date) As Integer
        GetROCYear = CInt((dtmDate.Year - 1911).ToString)
    End Function

    Function GetROCDateFull(ByVal dt As Date) As String
        Dim s As String = ""
        Dim m As String = ""
        If dt.Month < 10 Then
            m = "0" & dt.Month.ToString
        Else
            m = dt.Month.ToString
        End If
        s = GetROCYear(dt) & My.Resources.year & m & _
            My.Resources.month
        If dt.Day < 10 Then
            m = "0" & dt.Day.ToString
        Else
            m = dt.Day.ToString
        End If
        s = s & m & My.Resources.day
        Return s
    End Function

    Function GetROCDateNum(ByVal dt As Date) As String
        Dim s As String = ""
        Dim m As String = ""
        If dt.Month < 10 Then
            m = "0" & dt.Month.ToString
        Else
            m = dt.Month.ToString
        End If
        s = GetROCYear(dt) & m
        If dt.Day < 10 Then
            m = "0" & dt.Day.ToString
        Else
            m = dt.Day.ToString
        End If
        s = s & m
        Return s
    End Function

    Function GetDateNum(ByVal dt As Date) As String
        Dim s As String = ""
        Dim m As String = ""
        If dt.Month < 10 Then
            m = "0" & dt.Month.ToString
        Else
            m = dt.Month.ToString
        End If
        s = dt.Year.ToString & "/" & m
        If dt.Day < 10 Then
            m = "0" & dt.Day.ToString
        Else
            m = dt.Day.ToString
        End If
        s = s & "/" & m
        Return s
    End Function

    Function GetTimeNum(ByVal dt As Date) As String
        Dim s As String = ""
        Dim m As String = ""
        If dt.Hour = 0 Then
            m = "00"
        ElseIf dt.Hour < 10 And dt.Hour > 0 Then
            m = "0" & dt.Hour.ToString
        Else
            m = dt.Hour.ToString
        End If
        s = m
        If dt.Minute = 0 Then
            m = "00"
        ElseIf dt.Minute < 10 And dt.Minute > 0 Then
            m = "0" & dt.Minute.ToString
        Else
            m = dt.Minute.ToString
        End If
        s = s & m
        Return s
    End Function

    Function GetHourNum(ByVal dt As Date) As String
        Dim m As String = ""
        If dt.Hour = 0 Then
            m = "00"
        ElseIf dt.Hour < 10 And dt.Hour > 0 Then
            m = "0" & dt.Hour.ToString
        Else
            m = dt.Hour.ToString
        End If
        Return m
    End Function

    Function GetMinuteNum(ByVal dt As Date) As String
        Dim m As String = ""
        If dt.Minute = 0 Then
            m = "00"
        ElseIf dt.Minute < 10 And dt.Minute > 0 Then
            m = "0" & dt.Minute.ToString
        Else
            m = dt.Minute.ToString
        End If
        Return m
    End Function

    Function GetTimeNumNow() As String
        Dim s As String = ""
        Dim m As String = ""
        If Now.Hour = 0 Then
            m = "00"
        ElseIf Now.Hour < 10 And Now.Hour > 0 Then
            m = "0" & Now.Hour.ToString
        Else
            m = Now.Hour.ToString
        End If
        s = m
        If Now.Minute = 0 Then
            m = "00"
        ElseIf Now.Minute < 10 And Now.Minute > 0 Then
            m = "0" & Now.Minute.ToString
        Else
            m = Now.Minute.ToString
        End If
        s = s & ":" & m
        If Now.Second = 0 Then
            m = "00"
        ElseIf Now.Second < 10 And Now.Second > 0 Then
            m = "0" & Now.Second.ToString
        Else
            m = Now.Second.ToString
        End If
        s = s & ":" & m
        Return s
    End Function

    Function GetWeekDayName(ByVal w As Integer, ByVal eng As Byte) As String
        Dim s As String = ""
        If eng = 0 Then
            Select Case w
                Case 1
                    s = My.Resources.one
                Case 2
                    s = My.Resources.two
                Case 3
                    s = My.Resources.three
                Case 4
                    s = My.Resources.four
                Case 5
                    s = My.Resources.five
                Case 6
                    s = My.Resources.six
                Case 7, 0
                    s = My.Resources.sunday
            End Select
        Else
            Select Case w
                Case 1
                    s = "Mon"
                Case 2
                    s = "Tue"
                Case 3
                    s = "Wed"
                Case 4
                    s = "Thu"
                Case 5
                    s = "Fri"
                Case 6
                    s = "Sat"
                Case 7, 0
                    s = "Sun"
            End Select
        End If
        Return s
    End Function

    Function GetExcelCol(ByVal w As Integer) As String
        Dim s As String = ""
        Select Case w
            Case 1
                s = "A"
            Case 2
                s = "B"
            Case 3
                s = "C"
            Case 4
                s = "D"
            Case 5
                s = "E"
            Case 6
                s = "F"
            Case 7
                s = "G"
            Case 8
                s = "H"
            Case 9
                s = "I"
            Case 10
                s = "J"
            Case 11
                s = "K"
            Case 12
                s = "L"
            Case 13
                s = "M"
            Case 14
                s = "N"
            Case 15
                s = "O"
            Case 16
                s = "P"
            Case 17
                s = "Q"
            Case 18
                s = "R"
            Case 19
                s = "S"
            Case 20
                s = "T"
            Case 21
                s = "U"
            Case 22
                s = "V"
            Case 23
                s = "W"
            Case 24
                s = "X"
            Case 25
                s = "Y"
            Case 26
                s = "Z"
            Case 27
                s = "AA"
            Case 28
                s = "AB"
            Case 29
                s = "AC"
            Case 30
                s = "AD"
            Case 31
                s = "AE"
            Case 32
                s = "AF"
            Case 33
                s = "AG"
            Case Else
                s = "A"
        End Select
        
        Return s
    End Function

    'type=1, string; 2, integer 0
    Sub FillArrayList(ByVal type As Integer, ByVal count As Integer, _
                           ByRef lst As ArrayList)
        lst = New ArrayList
        For index As Integer = 0 To count - 1
            If type = 1 Then
                lst.Add("")
            ElseIf type = 2 Then
                lst.Add(0)
            End If
        Next
    End Sub
End Module
