﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPayStaToday
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPayStaToday))
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrintAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrintDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetPrintSize = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportAll = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.butByMethod1 = New System.Windows.Forms.Button
        Me.txtboxTotal1 = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.dgvClassPay = New System.Windows.Forms.DataGridView
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.butSelectCol1 = New System.Windows.Forms.Button
        Me.radbutNewStu = New System.Windows.Forms.RadioButton
        Me.radbutAllStu = New System.Windows.Forms.RadioButton
        Me.dgvClassPayDetails = New System.Windows.Forms.DataGridView
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.butByMethod2 = New System.Windows.Forms.Button
        Me.txtboxTotal2 = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.dgvClassKB = New System.Windows.Forms.DataGridView
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.butSelectCol2 = New System.Windows.Forms.Button
        Me.dgvClassKBDetails = New System.Windows.Forms.DataGridView
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.butListSta = New System.Windows.Forms.Button
        Me.cbTodayStuSta = New System.Windows.Forms.CheckBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtboxTotalAmt = New System.Windows.Forms.TextBox
        Me.txtboxTotalWithMBack = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.PrintDocument2 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvClassPay, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvClassPayDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvClassKB, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvClassKBDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrintAll, Me.mnuPrintDetails, Me.mnuSetPrintSize, Me.mnuExportAll, Me.mnuExportDetails, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrintAll
        '
        Me.mnuPrintAll.Name = "mnuPrintAll"
        resources.ApplyResources(Me.mnuPrintAll, "mnuPrintAll")
        '
        'mnuPrintDetails
        '
        Me.mnuPrintDetails.Name = "mnuPrintDetails"
        resources.ApplyResources(Me.mnuPrintDetails, "mnuPrintDetails")
        '
        'mnuSetPrintSize
        '
        Me.mnuSetPrintSize.Name = "mnuSetPrintSize"
        resources.ApplyResources(Me.mnuSetPrintSize, "mnuSetPrintSize")
        '
        'mnuExportAll
        '
        Me.mnuExportAll.Name = "mnuExportAll"
        resources.ApplyResources(Me.mnuExportAll, "mnuExportAll")
        '
        'mnuExportDetails
        '
        Me.mnuExportDetails.Name = "mnuExportDetails"
        resources.ApplyResources(Me.mnuExportDetails, "mnuExportDetails")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.butByMethod1)
        Me.GroupBox1.Controls.Add(Me.txtboxTotal1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.dgvClassPay)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'butByMethod1
        '
        resources.ApplyResources(Me.butByMethod1, "butByMethod1")
        Me.butByMethod1.Name = "butByMethod1"
        Me.butByMethod1.UseVisualStyleBackColor = True
        '
        'txtboxTotal1
        '
        resources.ApplyResources(Me.txtboxTotal1, "txtboxTotal1")
        Me.txtboxTotal1.Name = "txtboxTotal1"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'dgvClassPay
        '
        Me.dgvClassPay.AllowUserToAddRows = False
        Me.dgvClassPay.AllowUserToDeleteRows = False
        Me.dgvClassPay.AllowUserToResizeRows = False
        Me.dgvClassPay.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassPay.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassPay, "dgvClassPay")
        Me.dgvClassPay.MultiSelect = False
        Me.dgvClassPay.Name = "dgvClassPay"
        Me.dgvClassPay.RowHeadersVisible = False
        Me.dgvClassPay.RowTemplate.Height = 15
        Me.dgvClassPay.RowTemplate.ReadOnly = True
        Me.dgvClassPay.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassPay.ShowCellToolTips = False
        Me.dgvClassPay.ShowEditingIcon = False
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.butSelectCol1)
        Me.GroupBox2.Controls.Add(Me.radbutNewStu)
        Me.GroupBox2.Controls.Add(Me.radbutAllStu)
        Me.GroupBox2.Controls.Add(Me.dgvClassPayDetails)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'butSelectCol1
        '
        resources.ApplyResources(Me.butSelectCol1, "butSelectCol1")
        Me.butSelectCol1.Name = "butSelectCol1"
        Me.butSelectCol1.UseVisualStyleBackColor = True
        '
        'radbutNewStu
        '
        resources.ApplyResources(Me.radbutNewStu, "radbutNewStu")
        Me.radbutNewStu.Name = "radbutNewStu"
        Me.radbutNewStu.UseVisualStyleBackColor = True
        '
        'radbutAllStu
        '
        resources.ApplyResources(Me.radbutAllStu, "radbutAllStu")
        Me.radbutAllStu.Checked = True
        Me.radbutAllStu.Name = "radbutAllStu"
        Me.radbutAllStu.TabStop = True
        Me.radbutAllStu.UseVisualStyleBackColor = True
        '
        'dgvClassPayDetails
        '
        Me.dgvClassPayDetails.AllowUserToAddRows = False
        Me.dgvClassPayDetails.AllowUserToDeleteRows = False
        Me.dgvClassPayDetails.AllowUserToResizeRows = False
        Me.dgvClassPayDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassPayDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassPayDetails, "dgvClassPayDetails")
        Me.dgvClassPayDetails.MultiSelect = False
        Me.dgvClassPayDetails.Name = "dgvClassPayDetails"
        Me.dgvClassPayDetails.RowHeadersVisible = False
        Me.dgvClassPayDetails.RowTemplate.Height = 15
        Me.dgvClassPayDetails.RowTemplate.ReadOnly = True
        Me.dgvClassPayDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassPayDetails.ShowCellToolTips = False
        Me.dgvClassPayDetails.ShowEditingIcon = False
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.butByMethod2)
        Me.GroupBox3.Controls.Add(Me.txtboxTotal2)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.dgvClassKB)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'butByMethod2
        '
        resources.ApplyResources(Me.butByMethod2, "butByMethod2")
        Me.butByMethod2.Name = "butByMethod2"
        Me.butByMethod2.UseVisualStyleBackColor = True
        '
        'txtboxTotal2
        '
        resources.ApplyResources(Me.txtboxTotal2, "txtboxTotal2")
        Me.txtboxTotal2.Name = "txtboxTotal2"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'dgvClassKB
        '
        Me.dgvClassKB.AllowUserToAddRows = False
        Me.dgvClassKB.AllowUserToDeleteRows = False
        Me.dgvClassKB.AllowUserToResizeRows = False
        Me.dgvClassKB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassKB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassKB, "dgvClassKB")
        Me.dgvClassKB.MultiSelect = False
        Me.dgvClassKB.Name = "dgvClassKB"
        Me.dgvClassKB.RowHeadersVisible = False
        Me.dgvClassKB.RowTemplate.Height = 15
        Me.dgvClassKB.RowTemplate.ReadOnly = True
        Me.dgvClassKB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassKB.ShowCellToolTips = False
        Me.dgvClassKB.ShowEditingIcon = False
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.butSelectCol2)
        Me.GroupBox4.Controls.Add(Me.dgvClassKBDetails)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'butSelectCol2
        '
        resources.ApplyResources(Me.butSelectCol2, "butSelectCol2")
        Me.butSelectCol2.Name = "butSelectCol2"
        Me.butSelectCol2.UseVisualStyleBackColor = True
        '
        'dgvClassKBDetails
        '
        Me.dgvClassKBDetails.AllowUserToAddRows = False
        Me.dgvClassKBDetails.AllowUserToDeleteRows = False
        Me.dgvClassKBDetails.AllowUserToResizeRows = False
        Me.dgvClassKBDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClassKBDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClassKBDetails, "dgvClassKBDetails")
        Me.dgvClassKBDetails.MultiSelect = False
        Me.dgvClassKBDetails.Name = "dgvClassKBDetails"
        Me.dgvClassKBDetails.RowHeadersVisible = False
        Me.dgvClassKBDetails.RowTemplate.Height = 15
        Me.dgvClassKBDetails.RowTemplate.ReadOnly = True
        Me.dgvClassKBDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassKBDetails.ShowCellToolTips = False
        Me.dgvClassKBDetails.ShowEditingIcon = False
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'butListSta
        '
        resources.ApplyResources(Me.butListSta, "butListSta")
        Me.butListSta.Name = "butListSta"
        Me.butListSta.UseVisualStyleBackColor = True
        '
        'cbTodayStuSta
        '
        resources.ApplyResources(Me.cbTodayStuSta, "cbTodayStuSta")
        Me.cbTodayStuSta.Checked = True
        Me.cbTodayStuSta.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbTodayStuSta.Name = "cbTodayStuSta"
        Me.cbTodayStuSta.UseVisualStyleBackColor = True
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'txtboxTotalAmt
        '
        resources.ApplyResources(Me.txtboxTotalAmt, "txtboxTotalAmt")
        Me.txtboxTotalAmt.Name = "txtboxTotalAmt"
        '
        'txtboxTotalWithMBack
        '
        resources.ApplyResources(Me.txtboxTotalWithMBack, "txtboxTotalWithMBack")
        Me.txtboxTotalWithMBack.Name = "txtboxTotalWithMBack"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'PrintDocument1
        '
        '
        'PrintDocument2
        '
        '
        'frmPayStaToday
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(254, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(170, Byte), Integer))
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.txtboxTotalWithMBack)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.txtboxTotalAmt)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cbTodayStuSta)
        Me.Controls.Add(Me.butListSta)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.mnustrTop)
        Me.MainMenuStrip = Me.mnustrTop
        Me.Name = "frmPayStaToday"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvClassPay, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.dgvClassPayDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvClassKB, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgvClassKBDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrintAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrintDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetPrintSize As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportAll As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents butListSta As System.Windows.Forms.Button
    Friend WithEvents cbTodayStuSta As System.Windows.Forms.CheckBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtboxTotalAmt As System.Windows.Forms.TextBox
    Friend WithEvents txtboxTotalWithMBack As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents butByMethod1 As System.Windows.Forms.Button
    Friend WithEvents txtboxTotal1 As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dgvClassPay As System.Windows.Forms.DataGridView
    Friend WithEvents butSelectCol1 As System.Windows.Forms.Button
    Friend WithEvents radbutNewStu As System.Windows.Forms.RadioButton
    Friend WithEvents radbutAllStu As System.Windows.Forms.RadioButton
    Friend WithEvents dgvClassPayDetails As System.Windows.Forms.DataGridView
    Friend WithEvents butByMethod2 As System.Windows.Forms.Button
    Friend WithEvents txtboxTotal2 As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents dgvClassKB As System.Windows.Forms.DataGridView
    Friend WithEvents butSelectCol2 As System.Windows.Forms.Button
    Friend WithEvents dgvClassKBDetails As System.Windows.Forms.DataGridView
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents PrintDocument2 As System.Drawing.Printing.PrintDocument
End Class
