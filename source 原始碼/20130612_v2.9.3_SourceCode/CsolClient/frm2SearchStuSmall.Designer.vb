﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SearchStuSmall
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSearch = New System.Windows.Forms.Button
        Me.tboxTel = New System.Windows.Forms.TextBox
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butSearch2 = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(192, 59)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(62, 25)
        Me.butCancel.TabIndex = 17
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSearch
        '
        Me.butSearch.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSearch.Location = New System.Drawing.Point(192, 6)
        Me.butSearch.Name = "butSearch"
        Me.butSearch.Size = New System.Drawing.Size(62, 25)
        Me.butSearch.TabIndex = 16
        Me.butSearch.Text = "找第一筆"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'tboxTel
        '
        Me.tboxTel.Location = New System.Drawing.Point(53, 62)
        Me.tboxTel.Name = "tboxTel"
        Me.tboxTel.Size = New System.Drawing.Size(123, 22)
        Me.tboxTel.TabIndex = 15
        '
        'tboxName
        '
        Me.tboxName.Location = New System.Drawing.Point(53, 34)
        Me.tboxName.Name = "tboxName"
        Me.tboxName.Size = New System.Drawing.Size(123, 22)
        Me.tboxName.TabIndex = 14
        '
        'tboxID
        '
        Me.tboxID.Location = New System.Drawing.Point(53, 9)
        Me.tboxID.Name = "tboxID"
        Me.tboxID.Size = New System.Drawing.Size(123, 22)
        Me.tboxID.TabIndex = 13
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(12, 65)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "電話: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(12, 37)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "姓名: "
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(12, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 10
        Me.Label1.Text = "學號: "
        '
        'butSearch2
        '
        Me.butSearch2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSearch2.Location = New System.Drawing.Point(192, 31)
        Me.butSearch2.Name = "butSearch2"
        Me.butSearch2.Size = New System.Drawing.Size(62, 25)
        Me.butSearch2.TabIndex = 18
        Me.butSearch2.Text = "找下一筆"
        Me.butSearch2.UseVisualStyleBackColor = True
        '
        'frm2SearchStuSmall
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(267, 97)
        Me.Controls.Add(Me.butSearch2)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSearch)
        Me.Controls.Add(Me.tboxTel)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SearchStuSmall"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "搜尋"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSearch As System.Windows.Forms.Button
    Friend WithEvents tboxTel As System.Windows.Forms.TextBox
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butSearch2 As System.Windows.Forms.Button
End Class
