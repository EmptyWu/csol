﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetPrint
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.radbutTodayFeeNo = New System.Windows.Forms.RadioButton
        Me.radbutTodayFee = New System.Windows.Forms.RadioButton
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.radbutReceiptCust = New System.Windows.Forms.RadioButton
        Me.radbutReceipt = New System.Windows.Forms.RadioButton
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutCallOweNo = New System.Windows.Forms.RadioButton
        Me.radbutCallOwe = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.radbutOweNo = New System.Windows.Forms.RadioButton
        Me.radbutOwe = New System.Windows.Forms.RadioButton
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.radbutRight = New System.Windows.Forms.RadioButton
        Me.radbutLeft = New System.Windows.Forms.RadioButton
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.radbutCert = New System.Windows.Forms.RadioButton
        Me.radbutCertNo = New System.Windows.Forms.RadioButton
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.radbutTimeNo = New System.Windows.Forms.RadioButton
        Me.radbutTime = New System.Windows.Forms.RadioButton
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.radbutNumNo = New System.Windows.Forms.RadioButton
        Me.radbutNum = New System.Windows.Forms.RadioButton
        Me.GroupBox10 = New System.Windows.Forms.GroupBox
        Me.radbutSalesNo = New System.Windows.Forms.RadioButton
        Me.radbutSales = New System.Windows.Forms.RadioButton
        Me.GroupBox11 = New System.Windows.Forms.GroupBox
        Me.radbutChinese = New System.Windows.Forms.RadioButton
        Me.radbutEnglish = New System.Windows.Forms.RadioButton
        Me.GroupBox12 = New System.Windows.Forms.GroupBox
        Me.radbutBothNo = New System.Windows.Forms.RadioButton
        Me.radbutBoth = New System.Windows.Forms.RadioButton
        Me.GroupBox13 = New System.Windows.Forms.GroupBox
        Me.radbutCallTimeNo = New System.Windows.Forms.RadioButton
        Me.radbutCallTime = New System.Windows.Forms.RadioButton
        Me.GroupBox14 = New System.Windows.Forms.GroupBox
        Me.radbutB5 = New System.Windows.Forms.RadioButton
        Me.radbutA4 = New System.Windows.Forms.RadioButton
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.tboxCallReceipt = New System.Windows.Forms.TextBox
        Me.tboxOwe = New System.Windows.Forms.TextBox
        Me.tboxCert = New System.Windows.Forms.TextBox
        Me.GroupBox9 = New System.Windows.Forms.GroupBox
        Me.radbutTotal = New System.Windows.Forms.RadioButton
        Me.radbutTotalNo = New System.Windows.Forms.RadioButton
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        Me.GroupBox10.SuspendLayout()
        Me.GroupBox11.SuspendLayout()
        Me.GroupBox12.SuspendLayout()
        Me.GroupBox13.SuspendLayout()
        Me.GroupBox14.SuspendLayout()
        Me.GroupBox9.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.radbutTodayFee)
        Me.GroupBox1.Controls.Add(Me.radbutTodayFeeNo)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'radbutTodayFeeNo
        '
        Me.radbutTodayFeeNo.AutoSize = True
        Me.radbutTodayFeeNo.Location = New System.Drawing.Point(7, 18)
        Me.radbutTodayFeeNo.Name = "radbutTodayFeeNo"
        Me.radbutTodayFeeNo.Size = New System.Drawing.Size(215, 16)
        Me.radbutTodayFeeNo.TabIndex = 0
        Me.radbutTodayFeeNo.Text = "列印這個學生在這個班繳過的的學費"
        Me.radbutTodayFeeNo.UseVisualStyleBackColor = True
        '
        'radbutTodayFee
        '
        Me.radbutTodayFee.AutoSize = True
        Me.radbutTodayFee.Checked = True
        Me.radbutTodayFee.Location = New System.Drawing.Point(271, 18)
        Me.radbutTodayFee.Name = "radbutTodayFee"
        Me.radbutTodayFee.Size = New System.Drawing.Size(131, 16)
        Me.radbutTodayFee.TabIndex = 1
        Me.radbutTodayFee.TabStop = True
        Me.radbutTodayFee.Text = "只列印今日繳的學費"
        Me.radbutTodayFee.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tboxCallReceipt)
        Me.GroupBox2.Controls.Add(Me.radbutReceiptCust)
        Me.GroupBox2.Controls.Add(Me.radbutReceipt)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 43)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        '
        'radbutReceiptCust
        '
        Me.radbutReceiptCust.AutoSize = True
        Me.radbutReceiptCust.Location = New System.Drawing.Point(271, 18)
        Me.radbutReceiptCust.Name = "radbutReceiptCust"
        Me.radbutReceiptCust.Size = New System.Drawing.Size(149, 16)
        Me.radbutReceiptCust.TabIndex = 1
        Me.radbutReceiptCust.Text = "我要把收據兩個字改成: "
        Me.radbutReceiptCust.UseVisualStyleBackColor = True
        '
        'radbutReceipt
        '
        Me.radbutReceipt.AutoSize = True
        Me.radbutReceipt.Checked = True
        Me.radbutReceipt.Location = New System.Drawing.Point(7, 18)
        Me.radbutReceipt.Name = "radbutReceipt"
        Me.radbutReceipt.Size = New System.Drawing.Size(145, 16)
        Me.radbutReceipt.TabIndex = 0
        Me.radbutReceipt.TabStop = True
        Me.radbutReceipt.Text = "抬頭為XX補習班""收據"""
        Me.radbutReceipt.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.tboxOwe)
        Me.GroupBox3.Controls.Add(Me.radbutCallOweNo)
        Me.GroupBox3.Controls.Add(Me.radbutCallOwe)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 125)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        '
        'radbutCallOweNo
        '
        Me.radbutCallOweNo.AutoSize = True
        Me.radbutCallOweNo.Checked = True
        Me.radbutCallOweNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutCallOweNo.Name = "radbutCallOweNo"
        Me.radbutCallOweNo.Size = New System.Drawing.Size(113, 16)
        Me.radbutCallOweNo.TabIndex = 1
        Me.radbutCallOweNo.TabStop = True
        Me.radbutCallOweNo.Text = "我要把尚欠改成: "
        Me.radbutCallOweNo.UseVisualStyleBackColor = True
        '
        'radbutCallOwe
        '
        Me.radbutCallOwe.AutoSize = True
        Me.radbutCallOwe.Location = New System.Drawing.Point(7, 18)
        Me.radbutCallOwe.Name = "radbutCallOwe"
        Me.radbutCallOwe.Size = New System.Drawing.Size(107, 16)
        Me.radbutCallOwe.TabIndex = 0
        Me.radbutCallOwe.Text = "尚欠就叫做尚欠"
        Me.radbutCallOwe.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.radbutOweNo)
        Me.GroupBox4.Controls.Add(Me.radbutOwe)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 84)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox4.TabIndex = 2
        Me.GroupBox4.TabStop = False
        '
        'radbutOweNo
        '
        Me.radbutOweNo.AutoSize = True
        Me.radbutOweNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutOweNo.Name = "radbutOweNo"
        Me.radbutOweNo.Size = New System.Drawing.Size(107, 16)
        Me.radbutOweNo.TabIndex = 1
        Me.radbutOweNo.Text = "不列印尚欠金額"
        Me.radbutOweNo.UseVisualStyleBackColor = True
        '
        'radbutOwe
        '
        Me.radbutOwe.AutoSize = True
        Me.radbutOwe.Checked = True
        Me.radbutOwe.Location = New System.Drawing.Point(7, 18)
        Me.radbutOwe.Name = "radbutOwe"
        Me.radbutOwe.Size = New System.Drawing.Size(95, 16)
        Me.radbutOwe.TabIndex = 0
        Me.radbutOwe.TabStop = True
        Me.radbutOwe.Text = "列印尚欠金額"
        Me.radbutOwe.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.radbutRight)
        Me.GroupBox5.Controls.Add(Me.radbutLeft)
        Me.GroupBox5.Location = New System.Drawing.Point(12, 321)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox5.TabIndex = 7
        Me.GroupBox5.TabStop = False
        '
        'radbutRight
        '
        Me.radbutRight.AutoSize = True
        Me.radbutRight.Location = New System.Drawing.Point(271, 18)
        Me.radbutRight.Name = "radbutRight"
        Me.radbutRight.Size = New System.Drawing.Size(95, 16)
        Me.radbutRight.TabIndex = 1
        Me.radbutRight.TabStop = True
        Me.radbutRight.Text = "收據編號靠右"
        Me.radbutRight.UseVisualStyleBackColor = True
        '
        'radbutLeft
        '
        Me.radbutLeft.AutoSize = True
        Me.radbutLeft.Checked = True
        Me.radbutLeft.Location = New System.Drawing.Point(7, 18)
        Me.radbutLeft.Name = "radbutLeft"
        Me.radbutLeft.Size = New System.Drawing.Size(95, 16)
        Me.radbutLeft.TabIndex = 0
        Me.radbutLeft.TabStop = True
        Me.radbutLeft.Text = "收據編號靠左"
        Me.radbutLeft.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.tboxCert)
        Me.GroupBox6.Controls.Add(Me.radbutCert)
        Me.GroupBox6.Controls.Add(Me.radbutCertNo)
        Me.GroupBox6.Location = New System.Drawing.Point(12, 280)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox6.TabIndex = 6
        Me.GroupBox6.TabStop = False
        '
        'radbutCert
        '
        Me.radbutCert.AutoSize = True
        Me.radbutCert.Location = New System.Drawing.Point(271, 18)
        Me.radbutCert.Name = "radbutCert"
        Me.radbutCert.Size = New System.Drawing.Size(95, 16)
        Me.radbutCert.TabIndex = 1
        Me.radbutCert.TabStop = True
        Me.radbutCert.Text = "列印立案證號"
        Me.radbutCert.UseVisualStyleBackColor = True
        '
        'radbutCertNo
        '
        Me.radbutCertNo.AutoSize = True
        Me.radbutCertNo.Checked = True
        Me.radbutCertNo.Location = New System.Drawing.Point(7, 18)
        Me.radbutCertNo.Name = "radbutCertNo"
        Me.radbutCertNo.Size = New System.Drawing.Size(107, 16)
        Me.radbutCertNo.TabIndex = 0
        Me.radbutCertNo.TabStop = True
        Me.radbutCertNo.Text = "不列印立案證號"
        Me.radbutCertNo.UseVisualStyleBackColor = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.radbutTimeNo)
        Me.GroupBox7.Controls.Add(Me.radbutTime)
        Me.GroupBox7.Location = New System.Drawing.Point(12, 239)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox7.TabIndex = 5
        Me.GroupBox7.TabStop = False
        '
        'radbutTimeNo
        '
        Me.radbutTimeNo.AutoSize = True
        Me.radbutTimeNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutTimeNo.Name = "radbutTimeNo"
        Me.radbutTimeNo.Size = New System.Drawing.Size(107, 16)
        Me.radbutTimeNo.TabIndex = 1
        Me.radbutTimeNo.TabStop = True
        Me.radbutTimeNo.Text = "不列印修業期限"
        Me.radbutTimeNo.UseVisualStyleBackColor = True
        '
        'radbutTime
        '
        Me.radbutTime.AutoSize = True
        Me.radbutTime.Checked = True
        Me.radbutTime.Location = New System.Drawing.Point(7, 18)
        Me.radbutTime.Name = "radbutTime"
        Me.radbutTime.Size = New System.Drawing.Size(95, 16)
        Me.radbutTime.TabIndex = 0
        Me.radbutTime.TabStop = True
        Me.radbutTime.Text = "列印修業期限"
        Me.radbutTime.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.radbutNumNo)
        Me.GroupBox8.Controls.Add(Me.radbutNum)
        Me.GroupBox8.Location = New System.Drawing.Point(12, 198)
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox8.TabIndex = 4
        Me.GroupBox8.TabStop = False
        '
        'radbutNumNo
        '
        Me.radbutNumNo.AutoSize = True
        Me.radbutNumNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutNumNo.Name = "radbutNumNo"
        Me.radbutNumNo.Size = New System.Drawing.Size(107, 16)
        Me.radbutNumNo.TabIndex = 1
        Me.radbutNumNo.TabStop = True
        Me.radbutNumNo.Text = "不列印收據編號"
        Me.radbutNumNo.UseVisualStyleBackColor = True
        '
        'radbutNum
        '
        Me.radbutNum.AutoSize = True
        Me.radbutNum.Checked = True
        Me.radbutNum.Location = New System.Drawing.Point(7, 18)
        Me.radbutNum.Name = "radbutNum"
        Me.radbutNum.Size = New System.Drawing.Size(95, 16)
        Me.radbutNum.TabIndex = 0
        Me.radbutNum.TabStop = True
        Me.radbutNum.Tag = ""
        Me.radbutNum.Text = "列印收據編號"
        Me.radbutNum.UseVisualStyleBackColor = True
        '
        'GroupBox10
        '
        Me.GroupBox10.Controls.Add(Me.radbutSalesNo)
        Me.GroupBox10.Controls.Add(Me.radbutSales)
        Me.GroupBox10.Location = New System.Drawing.Point(12, 522)
        Me.GroupBox10.Name = "GroupBox10"
        Me.GroupBox10.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox10.TabIndex = 12
        Me.GroupBox10.TabStop = False
        '
        'radbutSalesNo
        '
        Me.radbutSalesNo.AutoSize = True
        Me.radbutSalesNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutSalesNo.Name = "radbutSalesNo"
        Me.radbutSalesNo.Size = New System.Drawing.Size(107, 16)
        Me.radbutSalesNo.TabIndex = 1
        Me.radbutSalesNo.TabStop = True
        Me.radbutSalesNo.Text = "不列印業務人員"
        Me.radbutSalesNo.UseVisualStyleBackColor = True
        '
        'radbutSales
        '
        Me.radbutSales.AutoSize = True
        Me.radbutSales.Checked = True
        Me.radbutSales.Location = New System.Drawing.Point(7, 18)
        Me.radbutSales.Name = "radbutSales"
        Me.radbutSales.Size = New System.Drawing.Size(95, 16)
        Me.radbutSales.TabIndex = 0
        Me.radbutSales.TabStop = True
        Me.radbutSales.Text = "列印業務人員"
        Me.radbutSales.UseVisualStyleBackColor = True
        '
        'GroupBox11
        '
        Me.GroupBox11.Controls.Add(Me.radbutChinese)
        Me.GroupBox11.Controls.Add(Me.radbutEnglish)
        Me.GroupBox11.Location = New System.Drawing.Point(12, 482)
        Me.GroupBox11.Name = "GroupBox11"
        Me.GroupBox11.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox11.TabIndex = 11
        Me.GroupBox11.TabStop = False
        '
        'radbutChinese
        '
        Me.radbutChinese.AutoSize = True
        Me.radbutChinese.Location = New System.Drawing.Point(271, 18)
        Me.radbutChinese.Name = "radbutChinese"
        Me.radbutChinese.Size = New System.Drawing.Size(95, 16)
        Me.radbutChinese.TabIndex = 1
        Me.radbutChinese.TabStop = True
        Me.radbutChinese.Text = "星期使用中文"
        Me.radbutChinese.UseVisualStyleBackColor = True
        '
        'radbutEnglish
        '
        Me.radbutEnglish.AutoSize = True
        Me.radbutEnglish.Checked = True
        Me.radbutEnglish.Location = New System.Drawing.Point(7, 18)
        Me.radbutEnglish.Name = "radbutEnglish"
        Me.radbutEnglish.Size = New System.Drawing.Size(95, 16)
        Me.radbutEnglish.TabIndex = 0
        Me.radbutEnglish.TabStop = True
        Me.radbutEnglish.Text = "星期使用英文"
        Me.radbutEnglish.UseVisualStyleBackColor = True
        '
        'GroupBox12
        '
        Me.GroupBox12.Controls.Add(Me.radbutBothNo)
        Me.GroupBox12.Controls.Add(Me.radbutBoth)
        Me.GroupBox12.Location = New System.Drawing.Point(12, 441)
        Me.GroupBox12.Name = "GroupBox12"
        Me.GroupBox12.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox12.TabIndex = 10
        Me.GroupBox12.TabStop = False
        '
        'radbutBothNo
        '
        Me.radbutBothNo.AutoSize = True
        Me.radbutBothNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutBothNo.Name = "radbutBothNo"
        Me.radbutBothNo.Size = New System.Drawing.Size(131, 16)
        Me.radbutBothNo.TabIndex = 1
        Me.radbutBothNo.TabStop = True
        Me.radbutBothNo.Text = "收據列印只列印上聯"
        Me.radbutBothNo.UseVisualStyleBackColor = True
        '
        'radbutBoth
        '
        Me.radbutBoth.AutoSize = True
        Me.radbutBoth.Checked = True
        Me.radbutBoth.Location = New System.Drawing.Point(7, 18)
        Me.radbutBoth.Name = "radbutBoth"
        Me.radbutBoth.Size = New System.Drawing.Size(119, 16)
        Me.radbutBoth.TabIndex = 0
        Me.radbutBoth.TabStop = True
        Me.radbutBoth.Text = "收據列印上下兩聯"
        Me.radbutBoth.UseVisualStyleBackColor = True
        '
        'GroupBox13
        '
        Me.GroupBox13.Controls.Add(Me.radbutCallTimeNo)
        Me.GroupBox13.Controls.Add(Me.radbutCallTime)
        Me.GroupBox13.Location = New System.Drawing.Point(12, 400)
        Me.GroupBox13.Name = "GroupBox13"
        Me.GroupBox13.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox13.TabIndex = 9
        Me.GroupBox13.TabStop = False
        '
        'radbutCallTimeNo
        '
        Me.radbutCallTimeNo.AutoSize = True
        Me.radbutCallTimeNo.Location = New System.Drawing.Point(271, 18)
        Me.radbutCallTimeNo.Name = "radbutCallTimeNo"
        Me.radbutCallTimeNo.Size = New System.Drawing.Size(215, 16)
        Me.radbutCallTimeNo.TabIndex = 1
        Me.radbutCallTimeNo.TabStop = True
        Me.radbutCallTimeNo.Text = "合併列印使用上課時段取代修業期限"
        Me.radbutCallTimeNo.UseVisualStyleBackColor = True
        '
        'radbutCallTime
        '
        Me.radbutCallTime.AutoSize = True
        Me.radbutCallTime.Checked = True
        Me.radbutCallTime.Location = New System.Drawing.Point(7, 18)
        Me.radbutCallTime.Name = "radbutCallTime"
        Me.radbutCallTime.Size = New System.Drawing.Size(143, 16)
        Me.radbutCallTime.TabIndex = 0
        Me.radbutCallTime.TabStop = True
        Me.radbutCallTime.Text = "合併列印使用修業期限"
        Me.radbutCallTime.UseVisualStyleBackColor = True
        '
        'GroupBox14
        '
        Me.GroupBox14.Controls.Add(Me.radbutB5)
        Me.GroupBox14.Controls.Add(Me.radbutA4)
        Me.GroupBox14.Location = New System.Drawing.Point(12, 359)
        Me.GroupBox14.Name = "GroupBox14"
        Me.GroupBox14.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox14.TabIndex = 8
        Me.GroupBox14.TabStop = False
        '
        'radbutB5
        '
        Me.radbutB5.AutoSize = True
        Me.radbutB5.Checked = True
        Me.radbutB5.Location = New System.Drawing.Point(271, 18)
        Me.radbutB5.Name = "radbutB5"
        Me.radbutB5.Size = New System.Drawing.Size(121, 16)
        Me.radbutB5.TabIndex = 1
        Me.radbutB5.TabStop = True
        Me.radbutB5.Text = "收據紙張大小為B5"
        Me.radbutB5.UseVisualStyleBackColor = True
        '
        'radbutA4
        '
        Me.radbutA4.AutoSize = True
        Me.radbutA4.Location = New System.Drawing.Point(7, 18)
        Me.radbutA4.Name = "radbutA4"
        Me.radbutA4.Size = New System.Drawing.Size(121, 16)
        Me.radbutA4.TabIndex = 0
        Me.radbutA4.TabStop = True
        Me.radbutA4.Text = "收據紙張大小為A4"
        Me.radbutA4.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(594, 55)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(75, 29)
        Me.butCancel.TabIndex = 130
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(594, 14)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(75, 29)
        Me.butSave.TabIndex = 129
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'tboxCallReceipt
        '
        Me.tboxCallReceipt.Location = New System.Drawing.Point(417, 17)
        Me.tboxCallReceipt.Name = "tboxCallReceipt"
        Me.tboxCallReceipt.Size = New System.Drawing.Size(111, 22)
        Me.tboxCallReceipt.TabIndex = 2
        '
        'tboxOwe
        '
        Me.tboxOwe.Location = New System.Drawing.Point(417, 17)
        Me.tboxOwe.Name = "tboxOwe"
        Me.tboxOwe.Size = New System.Drawing.Size(111, 22)
        Me.tboxOwe.TabIndex = 3
        Me.tboxOwe.Tag = ""
        Me.tboxOwe.Text = "餘額"
        '
        'tboxCert
        '
        Me.tboxCert.Location = New System.Drawing.Point(417, 17)
        Me.tboxCert.Name = "tboxCert"
        Me.tboxCert.Size = New System.Drawing.Size(111, 22)
        Me.tboxCert.TabIndex = 4
        Me.tboxCert.Tag = "餘額"
        '
        'GroupBox9
        '
        Me.GroupBox9.Controls.Add(Me.radbutTotal)
        Me.GroupBox9.Controls.Add(Me.radbutTotalNo)
        Me.GroupBox9.Location = New System.Drawing.Point(12, 165)
        Me.GroupBox9.Name = "GroupBox9"
        Me.GroupBox9.Size = New System.Drawing.Size(558, 43)
        Me.GroupBox9.TabIndex = 131
        Me.GroupBox9.TabStop = False
        '
        'radbutTotal
        '
        Me.radbutTotal.AutoSize = True
        Me.radbutTotal.Location = New System.Drawing.Point(271, 18)
        Me.radbutTotal.Name = "radbutTotal"
        Me.radbutTotal.Size = New System.Drawing.Size(143, 16)
        Me.radbutTotal.TabIndex = 1
        Me.radbutTotal.TabStop = True
        Me.radbutTotal.Text = "合併列印僅顯示總金額"
        Me.radbutTotal.UseVisualStyleBackColor = True
        '
        'radbutTotalNo
        '
        Me.radbutTotalNo.AutoSize = True
        Me.radbutTotalNo.Checked = True
        Me.radbutTotalNo.Location = New System.Drawing.Point(7, 18)
        Me.radbutTotalNo.Name = "radbutTotalNo"
        Me.radbutTotalNo.Size = New System.Drawing.Size(143, 16)
        Me.radbutTotalNo.TabIndex = 0
        Me.radbutTotalNo.TabStop = True
        Me.radbutTotalNo.Tag = ""
        Me.radbutTotalNo.Text = "合併列印顯示各班金額"
        Me.radbutTotalNo.UseVisualStyleBackColor = True
        '
        'frm2SetPrint
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(683, 573)
        Me.Controls.Add(Me.GroupBox9)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.GroupBox10)
        Me.Controls.Add(Me.GroupBox11)
        Me.Controls.Add(Me.GroupBox12)
        Me.Controls.Add(Me.GroupBox13)
        Me.Controls.Add(Me.GroupBox14)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frm2SetPrint"
        Me.Text = "收據列印選項"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox7.PerformLayout()
        Me.GroupBox8.ResumeLayout(False)
        Me.GroupBox8.PerformLayout()
        Me.GroupBox10.ResumeLayout(False)
        Me.GroupBox10.PerformLayout()
        Me.GroupBox11.ResumeLayout(False)
        Me.GroupBox11.PerformLayout()
        Me.GroupBox12.ResumeLayout(False)
        Me.GroupBox12.PerformLayout()
        Me.GroupBox13.ResumeLayout(False)
        Me.GroupBox13.PerformLayout()
        Me.GroupBox14.ResumeLayout(False)
        Me.GroupBox14.PerformLayout()
        Me.GroupBox9.ResumeLayout(False)
        Me.GroupBox9.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutTodayFeeNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutTodayFee As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutReceiptCust As System.Windows.Forms.RadioButton
    Friend WithEvents radbutReceipt As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutCallOweNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCallOwe As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutOweNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutOwe As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutRight As System.Windows.Forms.RadioButton
    Friend WithEvents radbutLeft As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutCert As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCertNo As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutTimeNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutTime As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutNumNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNum As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox10 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutSalesNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutSales As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox11 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutChinese As System.Windows.Forms.RadioButton
    Friend WithEvents radbutEnglish As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox12 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutBothNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutBoth As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox13 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutCallTimeNo As System.Windows.Forms.RadioButton
    Friend WithEvents radbutCallTime As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox14 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutB5 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutA4 As System.Windows.Forms.RadioButton
    Friend WithEvents tboxCallReceipt As System.Windows.Forms.TextBox
    Friend WithEvents tboxOwe As System.Windows.Forms.TextBox
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents tboxCert As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox9 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutTotal As System.Windows.Forms.RadioButton
    Friend WithEvents radbutTotalNo As System.Windows.Forms.RadioButton
End Class
