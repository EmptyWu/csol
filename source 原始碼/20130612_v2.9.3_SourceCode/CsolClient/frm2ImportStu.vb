﻿Imports Microsoft.Office.Interop
Imports System.Data.OleDb

Public Class frm2ImportStu
    Dim path As String = ""
    Dim fileExt As String = ""
    Dim dt As New DataTable
    Dim dtStu As New DataTable
    Dim dtStu2 As New DataTable 'Without repeated IDs
    Dim lst As New ArrayList
    Dim myExcel As Excel.Application
    Dim myWorkBookCollection As Excel.Workbooks ' Workbook-collection (note the 's' at the end)        
    Dim myWorkBook As Excel.Workbook ' Single Workbook (spreadsheet-collection)        
    Dim myWorkSheet As Excel.Worksheet
    Dim lstSeq As New ArrayList
    Dim lstCol As New ArrayList
    Dim lstColHead As New ArrayList
    Dim dtStu3 As New DataTable

    Private Sub frm2ImportStu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If Not myExcel Is Nothing Then
            myExcel.Quit()
        End If
    End Sub

    Private Sub frm2ImportStu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
    End Sub

    Private Sub butFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butFile.Click
        Dim fileName As String = ""
        Dim openFileDialog1 As New OpenFileDialog()
        Dim xcFileInfo As IO.FileInfo

        openFileDialog1.InitialDirectory = "c:\"
        'If radbutExcel.Checked Then
        openFileDialog1.Filter = "Excel files (*.xls)|*.xls|All files (*.*)|*.*"
        'Else
        'openFileDialog1.Filter = "Access files (*.mdb)|*.mdb|All files (*.*)|*.*"
        'End If
        openFileDialog1.FilterIndex = 1
        openFileDialog1.RestoreDirectory = True

        If openFileDialog1.ShowDialog() = System.Windows.Forms.DialogResult.OK Then
            Try
                path = openFileDialog1.FileName
                tboxFile.Text = path
                fileName = path.Substring(path.LastIndexOf("\\") + 1)

                fileExt = fileName.Substring(fileName.LastIndexOf(".") + 1)
                If fileExt.ToLower() = "xls" Or fileExt.ToLower = "xlsx" Then
                    xcFileInfo = New IO.FileInfo(path)
                     ' Single spreadsheet        
                    ' Initialize the interface to Excel.exe        
                    myExcel = New Excel.Application
                    If myExcel Is Nothing Then
                        dt = New DataTable
                        dgvRaw.Columns.Clear()
                        dgvRaw.Rows.Clear()
                        Exit Sub
                    End If
                    ' initialise access to Excel's workbook collection        
                    myWorkBookCollection = myExcel.Workbooks
                    'open spreadsheet from disk        
                    myWorkBook = myWorkBookCollection.Open(xcFileInfo.FullName, , False)
                    'get 1st sheet from workbook
                    Dim intSheetCnt As Integer = myWorkBook.Sheets.Count
                    cboxTable.Items.Clear()
                    For index As Integer = 1 To intSheetCnt
                        myWorkSheet = myWorkBook.Sheets.Item(index)
                        cboxTable.Items.Add(myWorkSheet.Name)
                    Next
                    cboxTable.SelectedIndex = -1

                ElseIf fileExt.ToLower = "mdb" Then
                    Dim CN As New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;" & _
                                                  "Data Source=" & path & ";")
                    CN.Open()
                    cboxTable.Items.Clear()
                    For Each R As DataRow In CN.GetSchema("Tables", New String() {Nothing, Nothing, Nothing, "Table"}).Rows
                        cboxTable.Items.Add(R.Item(2).ToString)
                    Next
                    CN.Close()
                    CN.Dispose()
                    cboxTable.SelectedIndex = -1
                End If


            Catch Ex As Exception
                MsgBox(My.Resources.msgFileError, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End Try
        End If
    End Sub

    Private Sub butClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butClose.Click
        Me.Close()
    End Sub

    Private Sub butImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butImport.Click
        Dim result As MsgBoxResult = _
            MsgBox(My.Resources.msgImportStuConfirm, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
        If result = MsgBoxResult.No Then
            Exit Sub
        End If

        If dtStu2.Rows.Count = 0 Then
            MsgBox(My.Resources.msgImportStuNoData, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Exit Sub
        End If

        Dim dtFinal As New DataTable
        dtFinal.Columns.Add(c_IDColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_NameColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_EngNameColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_SexColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_BirthdayColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_Tel1ColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_Tel2ColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_OfficeTelColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_DadNameColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_MumNameColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_DadMobileColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_MumMobileColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_MobileColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_PostalCodeColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_AddressColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_EmailColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_PrimarySchColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_JuniorSchColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_HighSchColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_UniversityColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_SchoolGradeColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_SchoolClassColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_SchGroupColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_GraduateFromColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_ICColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_IntroNameColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_RemarksColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_DadTitleColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_MumTitleColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_Cust1ColumnName, GetType(System.String))
        dtFinal.Columns.Add(c_Cust2ColumnName, GetType(System.String))

        If lstCol.Count = dtStu2.Columns.Count Then
            For Each row As DataRow In dtStu2.Rows
                Dim row2 As DataRow = dtFinal.NewRow
                For index As Integer = 0 To row.ItemArray.Count - 1
                    row2.Item(lstCol(index)) = row.ItemArray(index).ToString
                Next
                Dim listSch As DataTable = frmMain.GetSchList
                For i As Integer = 0 To listSch.Rows.Count - 1
                    If row.Item("國小").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("國中").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("高中").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("大學").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                Next
                dtFinal.Rows.Add(row2)
            Next
        Else
            For Each row As DataRow In dtStu2.Rows
                Dim row2 As DataRow = dtFinal.NewRow
                row2.Item(c_IDColumnName) = row.Item(cboxID.Tag)
                For index As Integer = 1 To row.ItemArray.Count - 1
                    row2.Item(lstCol(index - 1)) = row.ItemArray(index).ToString
                Next
                Dim listSch As DataTable = frmMain.GetSchList
                For i As Integer = 0 To listSch.Rows.Count - 1
                    If row.Item("國小").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("國中").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("高中").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                    If row.Item("大學").ToString.Trim = listSch.Rows(i).Item("Name").ToString.Trim Then
                        row2.Item("PrimarySch") = listSch.Rows(i).Item("ID")
                    End If
                Next
                dtFinal.Rows.Add(row2)
            Next
        End If

        Dim lstResult As ArrayList
        Dim btStuType As Byte = c_StuTypeFormal
        Dim blReplace As Boolean = False
        If radbutPotential.Checked Then
            btStuType = c_StuTypePotential
        End If

        If radbutUpdate.Checked Then
            blReplace = True
        End If
        lstResult = objCsol.ImportStu(dtFinal, btStuType, blReplace)
        If lstResult.Count > 0 Then
            For index As Integer = 0 To lstResult.Count - 1
                dgv.Rows(lstResult(index)).DefaultCellStyle.BackColor = Color.Red
            Next
        End If

        MsgBox(My.Resources.msgImportStuDone, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)

    End Sub

    Private Sub cboxTable_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxTable.SelectedIndexChanged
        If fileExt.ToLower = "mdb" Then
            If cboxTable.Items.Count > 0 And cboxTable.SelectedIndex > -1 Then
                Dim theOleDbCommand As New OleDbCommand("SELECT * FROM " & cboxTable.SelectedItem.ToString, _
                    New OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & path & ";"))
                theOleDbCommand.Connection.Open()
                Dim theReader As OleDbDataReader = theOleDbCommand.ExecuteReader(CommandBehavior.CloseConnection)
                While theReader.Read() = True
                    dgv.Rows.Add(theReader(0).ToString())
                End While
            End If
        Else
            If cboxTable.SelectedIndex > -1 And Not myExcel Is Nothing Then
                myWorkSheet = myWorkBook.Sheets.Item(cboxTable.SelectedIndex + 1)
                dtStu = New DataTable
                dgv.Columns.Clear()
                dgv.Rows.Clear()
                'alter contents of 1st cell   
                Dim rowCount As Integer = myWorkSheet.UsedRange.Rows.Count
                Dim colCount As Integer = myWorkSheet.UsedRange.Columns.Count
                If rowCount < 2 Or colCount = 0 Then
                    dt = New DataTable
                    dgvRaw.Columns.Clear()
                    dgvRaw.Rows.Clear()
                    Exit Sub
                End If
                If colCount > 31 Then
                    colCount = 31
                End If
                dt = New DataTable
                lst = New ArrayList
                For index As Integer = 1 To colCount
                    dt.Columns.Add(myWorkSheet.Range(GetExcelCol(index) & 1).Value, _
                                    GetType(System.String))
                    lst.Add(myWorkSheet.Range(GetExcelCol(index) & 1).Value)
                Next
                For index As Integer = 2 To rowCount
                    Dim row As Data.DataRow
                    row = dt.NewRow
                    For idx As Integer = 1 To colCount
                        row.Item(idx - 1) = myWorkSheet.Range(GetExcelCol(idx) & index).Value
                    Next
                    dt.Rows.Add(row)
                Next
                dgvRaw.DataSource = dt
                For Each ctl As Control In Panel1.Controls
                    If TypeOf ctl Is ComboBox Then
                        Dim tryCombo As ComboBox = TryCast(ctl, ComboBox)
                        Dim i As Integer = -1
                        If tryCombo IsNot Nothing Then
                            For index As Integer = 0 To lst.Count - 1
                                tryCombo.Items.Add(lst(index))
                                If lst(index).Equals(tryCombo.Tag.ToString) Then
                                    i = index
                                End If
                            Next
                            tryCombo.SelectedIndex = i
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub butPreview_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butPreview.Click
        lstSeq = New ArrayList
        lstCol = New ArrayList
        lstColHead = New ArrayList
        Dim intStuCnt As Integer = -1

        If tboxStuYear.Text.Trim.Length < 1 And cboxID.SelectedIndex = -1 Then
            MsgBox(My.Resources.msgImportStuNoId, MsgBoxStyle.OkOnly, _
                   My.Resources.msgRemindTitle)
            Exit Sub
        End If

        If Not IsNumeric(tboxStuYear.Text.Trim) And cboxID.SelectedIndex = -1 Then
            MsgBox(My.Resources.msgImportStuNoId, MsgBoxStyle.OkOnly, _
                   My.Resources.msgRemindTitle)
            Exit Sub
        End If

        dtStu = New DataTable
        dtStu2 = New DataTable
        dtStu3 = New DataTable
        If tboxStuYear.Text.Trim.Length = 0 Then
            If cboxID.SelectedIndex > -1 Then
                lstColHead.Add(cboxID.Tag)
                lstCol.Add(c_IDColumnName)
                lstSeq.Add(cboxID.SelectedIndex)
            End If
        Else
            dtStu.Columns.Add(My.Resources.stuID, GetType(System.String))
            intStuCnt = objCsol.CountStu(tboxStuYear.Text.Trim)
        End If

        If cboxName.SelectedIndex > -1 Then
            lstColHead.Add(cboxName.Tag)
            lstCol.Add(c_NameColumnName)
            lstSeq.Add(cboxName.SelectedIndex)
        End If

        If cboxEngName.SelectedIndex > -1 Then
            lstColHead.Add(cboxEngName.Tag)
            lstCol.Add(c_EngNameColumnName)
            lstSeq.Add(cboxEngName.SelectedIndex)
        End If

        If cboxSex.SelectedIndex > -1 Then
            lstColHead.Add(cboxSex.Tag)
            lstCol.Add(c_SexColumnName)
            lstSeq.Add(cboxSex.SelectedIndex)
        End If

        If cboxBirth.SelectedIndex > -1 Then
            lstColHead.Add(cboxBirth.Tag)
            lstCol.Add(c_BirthdayColumnName)
            lstSeq.Add(cboxBirth.SelectedIndex)
        End If

        If cboxTel1.SelectedIndex > -1 Then
            lstColHead.Add(cboxTel1.Tag)
            lstCol.Add(c_Tel1ColumnName)
            lstSeq.Add(cboxTel1.SelectedIndex)
        End If

        If cboxTel2.SelectedIndex > -1 Then
            lstColHead.Add(cboxTel2.Tag)
            lstCol.Add(c_Tel2ColumnName)
            lstSeq.Add(cboxTel2.SelectedIndex)
        End If

        If cboxOfficeTel.SelectedIndex > -1 Then
            lstColHead.Add(cboxOfficeTel.Tag)
            lstCol.Add(c_OfficeTelColumnName)
            lstSeq.Add(cboxOfficeTel.SelectedIndex)
        End If

        If cboxDad.SelectedIndex > -1 Then
            lstColHead.Add(cboxDad.Tag)
            lstCol.Add(c_DadNameColumnName)
            lstSeq.Add(cboxDad.SelectedIndex)
        End If

        If cboxMum.SelectedIndex > -1 Then
            lstColHead.Add(cboxMum.Tag)
            lstCol.Add(c_MumNameColumnName)
            lstSeq.Add(cboxMum.SelectedIndex)
        End If

        If cboxDadMobile.SelectedIndex > -1 Then
            lstColHead.Add(cboxDadMobile.Tag)
            lstCol.Add(c_DadMobileColumnName)
            lstSeq.Add(cboxDadMobile.SelectedIndex)
        End If

        If cboxMumMobile.SelectedIndex > -1 Then
            lstColHead.Add(cboxMumMobile.Tag)
            lstCol.Add(c_MumMobileColumnName)
            lstSeq.Add(cboxMumMobile.SelectedIndex)
        End If

        If cboxStuMobile.SelectedIndex > -1 Then
            lstColHead.Add(cboxStuMobile.Tag)
            lstCol.Add(c_MobileColumnName)
            lstSeq.Add(cboxStuMobile.SelectedIndex)
        End If

        If cboxPostalCode.SelectedIndex > -1 Then
            lstColHead.Add(cboxPostalCode.Tag)
            lstCol.Add(c_PostalCodeColumnName)
            lstSeq.Add(cboxPostalCode.SelectedIndex)
        End If

        If cboxAddr.SelectedIndex > -1 Then
            lstColHead.Add(cboxAddr.Tag)
            lstCol.Add(c_AddressColumnName)
            lstSeq.Add(cboxAddr.SelectedIndex)
        End If

        If cboxEmail.SelectedIndex > -1 Then
            lstColHead.Add(cboxEmail.Tag)
            lstCol.Add(c_EmailColumnName)
            lstSeq.Add(cboxEmail.SelectedIndex)
        End If

        If cboxPri.SelectedIndex > -1 Then
            lstColHead.Add(cboxPri.Tag)
            lstCol.Add(c_PrimarySchColumnName)
            lstSeq.Add(cboxPri.SelectedIndex)
        End If

        If cboxJunior.SelectedIndex > -1 Then
            lstColHead.Add(cboxJunior.Tag)
            lstCol.Add(c_JuniorSchColumnName)
            lstSeq.Add(cboxJunior.SelectedIndex)
        End If

        If cboxHigh.SelectedIndex > -1 Then
            lstColHead.Add(cboxHigh.Tag)
            lstCol.Add(c_HighSchColumnName)
            lstSeq.Add(cboxHigh.SelectedIndex)
        End If

        If cboxUni.SelectedIndex > -1 Then
            lstColHead.Add(cboxUni.Tag)
            lstCol.Add(c_UniversityColumnName)
            lstSeq.Add(cboxUni.SelectedIndex)
        End If

        If cboxGrade.SelectedIndex > -1 Then
            lstColHead.Add(cboxGrade.Tag)
            lstCol.Add(c_SchoolGradeColumnName)
            lstSeq.Add(cboxGrade.SelectedIndex)
        End If

        If cboxClass.SelectedIndex > -1 Then
            lstColHead.Add(cboxClass.Tag)
            lstCol.Add(c_SchoolClassColumnName)
            lstSeq.Add(cboxClass.SelectedIndex)
        End If

        If cboxType.SelectedIndex > -1 Then
            lstColHead.Add(cboxType.Tag)
            lstCol.Add(c_SchGroupColumnName)
            lstSeq.Add(cboxType.SelectedIndex)
        End If

        If cboxGraduate.SelectedIndex > -1 Then
            lstColHead.Add(cboxGraduate.Tag)
            lstCol.Add(c_GraduateFromColumnName)
            lstSeq.Add(cboxGraduate.SelectedIndex)
        End If

        If cboxIC.SelectedIndex > -1 Then
            lstColHead.Add(cboxIC.Tag)
            lstCol.Add(c_ICColumnName)
            lstSeq.Add(cboxIC.SelectedIndex)
        End If

        If cboxIntro.SelectedIndex > -1 Then
            lstColHead.Add(cboxIntro.Tag)
            lstCol.Add(c_IntroNameColumnName)
            lstSeq.Add(cboxIntro.SelectedIndex)
        End If

        If cboxRemarks.SelectedIndex > -1 Then
            lstColHead.Add(cboxRemarks.Tag)
            lstCol.Add(c_RemarksColumnName)
            lstSeq.Add(cboxRemarks.SelectedIndex)
        End If

        If cboxDadJob.SelectedIndex > -1 Then
            lstColHead.Add(cboxDadJob.Tag)
            lstCol.Add(c_DadTitleColumnName)
            lstSeq.Add(cboxDadJob.SelectedIndex)
        End If

        If cboxMumJob.SelectedIndex > -1 Then
            lstColHead.Add(cboxMumJob.Tag)
            lstCol.Add(c_MumTitleColumnName)
            lstSeq.Add(cboxMumJob.SelectedIndex)
        End If

        If cboxClassInfo.SelectedIndex > -1 Then
            lstColHead.Add(cboxClassInfo.Tag)
            lstCol.Add(c_Cust1ColumnName)
            lstSeq.Add(cboxClassInfo.SelectedIndex)
        End If

        If cboxClassID.SelectedIndex > -1 Then
            lstColHead.Add(cboxClassID.Tag)
            lstCol.Add(c_Cust2ColumnName)
            lstSeq.Add(cboxClassID.SelectedIndex)
        End If

        For index As Integer = 0 To lstColHead.Count - 1
            dtStu.Columns.Add(lstColHead(index), GetType(System.String))
        Next
        dtStu2 = dtStu.Clone
        dtStu3 = dt.Clone

        If intStuCnt > -1 Then
            Dim strStuId As String = ""
            Dim strYear As String = tboxStuYear.Text.Trim
            Dim intCount As Integer
            Dim strCount As String
            Dim strCompute1 As String = "COUNT(" + cboxID.SelectedItem.ToString.Trim + ")"
            Dim strCompute2 As String = ""
            Dim intFirst As Integer = -1
            Dim intLast As Integer = -1
            For Each row As DataRow In dt.Rows
                strCompute2 = cboxID.SelectedItem.ToString.Trim + "=" + row.Item(cboxID.SelectedItem.ToString.Trim)
                If dt.Compute(strCompute1, strCompute2) > 1 Then
                    If dtStu3.Compute(strCompute1, strCompute2) = 0 Then
                        If radbutIgnore.Checked Then
                            Dim row4 As DataRow = dtStu3.NewRow
                            row4.ItemArray = row.ItemArray
                            dtStu3.Rows.Add(row4)
                        ElseIf radbutUpdate.Checked Then
                            Dim row5 As DataRow = dtStu3.NewRow
                            For Each row2 As DataRow In dt.Rows
                                If row2.Item(cboxID.SelectedItem.ToString.Trim) = row.Item(cboxID.SelectedItem.ToString.Trim) Then
                                    row5.ItemArray = row2.ItemArray
                                End If
                            Next
                            dtStu3.Rows.Add(row5)
                        End If
                    End If
                Else
                    Dim row3 As DataRow = dtStu3.NewRow
                    row3.ItemArray = row.ItemArray
                    dtStu3.Rows.Add(row3)
                End If
            Next

            Do While strYear.Length < 3
                strYear = "0" & strYear
            Loop
            intCount = intStuCnt + 1
            For index As Integer = 0 To dtStu3.Rows.Count - 1
                Dim row As Data.DataRow
                row = dtStu.NewRow
                Do While strStuId = ""
                    strCount = intCount.ToString
                    Do While strCount.Length < 5
                        strCount = "0" & strCount
                    Loop
                    strStuId = strYear & strCount
                Loop
                row.Item(0) = strStuId
                strStuId = ""
                For idx As Integer = 0 To lstColHead.Count - 1
                    row.Item(idx + 1) = dtStu3.Rows(index).Item(lstSeq(idx))
                Next
                dtStu.Rows.Add(row)
                intCount = intCount + 1
            Next

            dtStu2.Merge(dtStu)
        Else
            For index As Integer = 0 To dt.Rows.Count - 1
                Dim row As Data.DataRow
                row = dtStu.NewRow
                For idx As Integer = 0 To lstColHead.Count - 1
                    row.Item(idx) = dt.Rows(index).Item(lstSeq(idx))
                Next
                dtStu.Rows.Add(row)
            Next

            Dim strCompute1 As String = "COUNT(" + cboxID.SelectedItem.ToString.Trim + ")"
            Dim strCompute2 As String = ""
            Dim intFirst As Integer = -1
            Dim intLast As Integer = -1
            For Each row As DataRow In dtStu.Rows
                strCompute2 = cboxID.SelectedItem.ToString.Trim + "=" + row.Item(cboxID.SelectedItem.ToString.Trim)
                If dtStu.Compute(strCompute1, strCompute2) > 1 Then
                    If dtStu2.Compute(strCompute1, strCompute2) = 0 Then
                        If radbutIgnore.Checked Then
                            Dim row4 As DataRow = dtStu2.NewRow
                            row4.ItemArray = row.ItemArray
                            dtStu2.Rows.Add(row4)
                        ElseIf radbutUpdate.Checked Then
                            Dim row5 As DataRow = dtStu2.NewRow
                            For Each row2 As DataRow In dtStu.Rows
                                If row2.Item(cboxID.SelectedItem.ToString.Trim) = row.Item(cboxID.SelectedItem.ToString.Trim) Then
                                    row5.ItemArray = row2.ItemArray
                                End If
                            Next
                            dtStu2.Rows.Add(row5)
                        End If
                    End If
                Else
                    Dim row3 As DataRow = dtStu2.NewRow
                    row3.ItemArray = row.ItemArray
                    dtStu2.Rows.Add(row3)
                End If
            Next
        End If

        dgv.DataSource = dtStu2
    End Sub
End Class