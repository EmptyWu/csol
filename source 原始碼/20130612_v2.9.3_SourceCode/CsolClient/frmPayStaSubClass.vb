﻿Public Class frmPayStaSubClass
    Private dtSubClassList As New DataTable
    Private dtSubClassSta As New DataTable
    Private dtClassList As New DataTable
    Private dateNow As Date = Now
    Private dsPaySta As New DataSet
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dtClassPaySta As New DataTable
    Private dtClassMKSta As New DataTable
    Private dtClassMBSta As New DataTable
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private lstClassFee As New ArrayList
    Private intColSchType As Integer = 1
    Private dtSubClass As New DataTable

    Dim intPay0 As Integer
    Dim intPay1 As Integer
    Dim intPay2 As Integer
    Dim intPay3 As Integer
    Dim intPay4 As Integer
    Dim intPay5 As Integer
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPayStaSubClass
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPayStaSubClass
        SetDate()
        InitClassList()
        ShowClassList()
        'RefreshData()
        InitColSelections()
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument2.DefaultPageSettings.Landscape = True
            PrintDocument2.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Friend Sub SetDate()
        dtpickFrom.Value = dateNow
        dtpickTo.Value = dateNow
    End Sub

    Private Sub frmPayStaSubClass_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPayStaSubClass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intClass As Integer
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_ReceiptNumColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String
        Dim strCompute6 As String
        Dim strCompute7 As String
        Dim stuOweCount As New ArrayList
        Dim test As New System.Collections.Specialized.NameValueCollection

        intPay0 = 0
        intPay1 = 0
        intPay2 = 0
        intPay3 = 0
        intPay4 = 0
        intPay5 = 0

        Try
            '====================================
            If radbutByDate.Checked = True Then
                'dsPaySta = objCsol.ListPayStaBySubClass(lstClass, lstSubClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))


                dtClassPaySta = GetdtClassPaySta(lstClass, lstSubClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                dtClassMKSta = GetdtClassMKSta(lstClass, lstSubClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                dtClassMBSta = GetdtClassMBSta(lstClass, lstSubClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))

            ElseIf radbutByDateNo.Checked = True Then
                'dsPaySta = objCsol.ListPayStaBySubClass(lstClass, lstSubClass)
                dtClassPaySta = GetdtClassPayStaNoDate(lstClass, lstSubClass)
                dtClassMKSta = GetdtClassMKStaNoDate(lstClass, lstSubClass)
                dtClassMBSta = GetdtClassMBStaNoDate(lstClass, lstSubClass)
            End If

            'dtClassPaySta = dsPaySta.Tables.Item(c_PayStaByDateTableName)
            'dtClassMKSta = dsPaySta.Tables.Item(c_MKeepStaByDateTableName)
            'dtClassMBSta = dsPaySta.Tables.Item(c_MBackStaByDateTableName)



            '====================================

            '====================================


            If radbutByDate.Checked = True Then
                'stuOweCount = GetStuOweCount(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                test = getStuOweCount(GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutByDateNo.Checked = True Then
                'stuOweCount = getStuOweCount()
                test = getStuOweCount()
            End If
            'stuOweCount = GetStuOweCount()

            '====================================

            dtMaster = New DataTable()
            dtDetails = New DataTable()
            'dtDetails = dtClassPaySta.Clone()
            dtDetails = dtClassMBSta.Clone()
            dtDetails.Merge(dtClassMKSta)
            dtDetails.Merge(dtClassMBSta)
            dtDetails.Merge(dtClassPaySta)
            tbox0.Text = "0"
            tbox1.Text = "0"
            tbox2.Text = "0"
            tbox3.Text = "0"
            tbox4.Text = "0"
            tbox5.Text = "0"
            InitTable()
            dtSubClassSta.Clear()
            '=====================
            'dtSubClassSta = objCsol.ListSubClassSta(lstSubClass)


            If radbutByDate.Checked = True Then
                dtSubClassSta = GetListSubClassSta(lstSubClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            ElseIf radbutByDateNo.Checked = True Then
                dtSubClassSta = GetListSubClassStaByNoDate(lstSubClass)
            End If

            '=====================
            For index As Integer = 0 To dtSubClassSta.Rows.Count - 1
                intClass = dtSubClassSta.Rows(index).Item(c_IDColumnName)
                If lstSubClass.IndexOf(intClass) > -1 Then
                    dtSubClassSta.Rows(index).Item(c_FeeClrCntColumnName) = test(dtSubClassSta.Rows(index).Item("ID").ToString)

                    'dtSubClassSta.Rows(index).Item(c_FeeClrCntColumnName) = stuOweCount(index)
                    dtMaster.Rows.Add(dtSubClassSta.Rows(index).Item(c_IDColumnName), _
                                      dtSubClassSta.Rows(index).Item(c_NameColumnName), _
                                      dtSubClassSta.Rows(index).Item(c_StuRegCntColumnName), _
                                      dtSubClassSta.Rows(index).Item(c_FeeClrCntColumnName), _
                                      0, 0, 0, 0, 0)
                End If
            Next

            For i = 0 To dtMaster.Rows.Count - 1
                intClass = dtMaster.Rows(i).Item(c_IDColumnName)
                intTotal = dtMaster.Rows(i).Item(c_StuRegCntColumnName) - _
                    dtMaster.Rows(i).Item(c_FeeClrCntColumnName)
                dtMaster.Rows(i).Item(c_FeeOweCntColumnName) = intTotal
                strCompute2 = c_SubClassIDColumnName + "=" + intClass.ToString
                If dtClassMKSta.Rows.Count > 0 Then
                    intTotal = dtClassMKSta.Compute(strCompute3, strCompute2)
                Else
                    intTotal = 0
                End If
                If dtClassMBSta.Rows.Count > 0 Then
                    intTotal = intTotal + dtClassMBSta.Compute(strCompute3, strCompute2)
                End If
                dtMaster.Rows(i).Item(c_DataCntColumnName) = intTotal
                If dtClassMKSta.Rows.Count > 0 Then
                    If dtClassMKSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = dtClassMKSta.Compute(strCompute1, strCompute2)
                    Else
                        intTotal = 0
                    End If
                Else
                    intTotal = 0
                End If
                If dtClassMBSta.Rows.Count > 0 Then
                    If dtClassMBSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = intTotal + dtClassMBSta.Compute(strCompute1, strCompute2)
                    End If
                End If
                If dtClassPaySta.Rows.Count > 0 Then
                    If dtClassPaySta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = intTotal + dtClassPaySta.Compute(strCompute1, strCompute2)
                    End If
                End If
                dtMaster.Rows(i).Item(c_TotalPayColumnName) = intTotal
                If dtClassMBSta.Rows.Count > 0 Then
                    If dtClassMBSta.Compute(strCompute3, strCompute2) > 0 Then
                        intTotal = dtClassMBSta.Compute(strCompute4, strCompute2)
                    Else
                        intTotal = 0
                    End If
                Else
                    intTotal = 0
                End If
                dtMaster.Rows(i).Item(c_TotalBackColumnName) = intTotal
                intTotal2 = dtMaster.Rows(i).Item(c_TotalPayColumnName)
                dtMaster.Rows(i).Item(c_AmountColumnName) = intTotal2 - intTotal
            Next

            strCompute1 = "SUM(" + c_StuRegCntColumnName + ")"
            strCompute2 = "SUM(" + c_FeeClrCntColumnName + ")"
            strCompute3 = "SUM(" + c_FeeOweCntColumnName + ")"
            strCompute4 = "SUM(" + c_DataCntColumnName + ")"
            strCompute5 = "SUM(" + c_TotalPayColumnName + ")"
            strCompute6 = "SUM(" + c_TotalBackColumnName + ")"
            strCompute7 = "SUM(" + c_AmountColumnName + ")"

            If dtMaster.Rows.Count > 0 Then
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, dtMaster.Compute(strCompute1, ""), _
                                    dtMaster.Compute(strCompute2, ""), dtMaster.Compute(strCompute3, ""), _
                                    dtMaster.Compute(strCompute4, ""), dtMaster.Compute(strCompute5, ""), _
                                    dtMaster.Compute(strCompute6, ""), dtMaster.Compute(strCompute7, ""))
            Else
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, 0, 0, 0, 0, 0, 0, 0)
            End If


            'dgvMaster.DataSource = dtMaster
            'If dgvMaster.RowCount > 0 Then
            '    dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells(c_NameColumnName)
            'End If

            'dtDetails.DefaultView.RowFilter = ""
            'dgvDetails.DataSource = dtDetails

            'RefreshPayMethodSta(dtClassPaySta, c_AmountColumnName)
            'RefreshPayMethodSta(dtClassMKSta, c_AmountColumnName)
            'RefreshPayMethodSta(dtClassMBSta, c_AmountColumnName)

            'tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_AmountColumnName).Value).ToString

            'tbox0.Text = Format(intPay0, "$#,##0")
            'tbox1.Text = Format(intPay1, "$#,##0")
            'tbox2.Text = Format(intPay2, "$#,##0")
            'tbox3.Text = Format(intPay3, "$#,##0")
            'tbox4.Text = Format(intPay4, "$#,##0")
            'tbox5.Text = Format(intPay5, "$#,##0")
            'tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            'tboxTotal.Text = tbox6.Text

            'dgvMaster.Columns(c_TotalPayColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_TotalBackColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvMaster.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvDetails.Columns(c_BackAmountColumnName).DefaultCellStyle.Format = "$#,##0"


            'LoadColumnText()
            '----------------------------------------------------------------------------------
            Dim dtM As DataTable = dtMaster.Clone
            dtM.Columns("Name").ColumnName = "班級名稱"
            dtM.Columns("StuRegCnt").ColumnName = "報名人數"
            dtM.Columns("FeeClrCnt").ColumnName = "繳清人數"
            dtM.Columns("FeeOweCnt").ColumnName = "尚欠人數"
            dtM.Columns("DataCnt").ColumnName = "退保人數"
            dtM.Columns("TotalPay").ColumnName = "繳費總額"
            dtM.Columns("TotalBack").ColumnName = "退保總額"
            dtM.Columns("Amount").ColumnName = "總計"
            For Each row As DataRow In dtMaster.Rows
                dtM.Rows.Add(row.ItemArray)
            Next


            dgvMaster.DataSource = dtM
            If dgvMaster.RowCount > 0 Then
                dgvMaster.CurrentCell = dgvMaster.Rows.Item(dgvMaster.RowCount - 1).Cells("班級名稱")
            End If
            dgvMaster.Columns.Item(c_IDColumnName).Visible = False

            tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells("總計").Value).ToString
            dgvMaster.Columns("繳費總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("退保總額").DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns("總計").DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()

            RefreshPayMethodSta(dtClassPaySta, c_AmountColumnName)
            RefreshPayMethodSta(dtClassMKSta, c_AmountColumnName)
            RefreshPayMethodSta(dtClassMBSta, c_AmountColumnName)

            tbox0.Text = Format(intPay0, "$#,##0")
            tbox1.Text = Format(intPay1, "$#,##0")
            tbox2.Text = Format(intPay2, "$#,##0")
            tbox3.Text = Format(intPay3, "$#,##0")
            tbox4.Text = Format(intPay4, "$#,##0")
            tbox5.Text = Format(intPay5, "$#,##0")
            tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            tboxTotal.Text = tbox6.Text

            dgvDetails.Columns("繳費金額").DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns("退費金額").DefaultCellStyle.Format = "$#,##0"


            '----------------------------------------------------------------------------------
        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub



    Private Shared Function GetdtClassMBStaNoDate(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassMBStaNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassMBStaNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassMBStaNoDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetdtClassMBSta(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList, ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassMBSta", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassMBSta").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassMBSta"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function


    Private Shared Function GetdtClassMKStaNoDate(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassMKStaNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassMKStaNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassMKStaNoDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetdtClassMKSta(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList, ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassMKSta", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassMKSta").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassMKSta"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetdtClassPayStaNoDate(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassPayStaNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassPayStaNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassPayStaNoDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetdtClassPaySta(ByVal lstClass As ArrayList, ByVal lstSubClass As ArrayList, ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstSubClass.Count - 1
            list.Add(lstSubClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetdtClassPaySta", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetdtClassPaySta").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetdtClassPaySta"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListSubClassStaByNoDate(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetListSubClassStaByNoDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetListSubClassStaByNoDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetListSubClassStaByNoDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListSubClassSta(ByVal lstClass As ArrayList, ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetListSubClassSta", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetListSubClassSta").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetListSubClassSta"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Sub RefreshPayMethodSta(ByRef dt As DataTable, ByVal colName As String)
        Dim intSc As Integer
        For index As Integer = 0 To dt.Rows.Count - 1
            intSc = dt.Rows(index).Item(c_SubClassIDColumnName)
            If lstSubClass.IndexOf(intSc) > -1 Then
                Select Case dt.Rows(index).Item(c_PayMethodColumnName)
                    Case c_PayMethodCash
                        intPay0 = intPay0 + dt.Rows(index).Item(c_AmountColumnName)
                    Case c_PayMethodCheque
                        intPay1 = intPay1 + dt.Rows(index).Item(c_AmountColumnName)
                    Case c_PayMethodTransfer
                        intPay2 = intPay2 + dt.Rows(index).Item(c_AmountColumnName)
                    Case c_PayMethodCreditCard
                        intPay3 = intPay3 + dt.Rows(index).Item(c_AmountColumnName)
                    Case c_PayMethodKeep
                        intPay4 = intPay4 + dt.Rows(index).Item(c_AmountColumnName)
                    Case Else
                        intPay5 = intPay5 + dt.Rows(index).Item(c_AmountColumnName)
                End Select
            End If
        Next
    End Sub

    Private Sub InitClassList()
        Try
            dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
            dtSubClassList = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        chklstSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()
            RefreshClassList()
            RefreshSubClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_IDColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_NameColumnName, GetType(System.String))
        dtMaster.Columns.Add(c_StuRegCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeClrCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_FeeOweCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalBackColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_AmountColumnName, GetType(System.Int32))

    End Sub

    Private Sub LoadColumnText()

        Dim dt As DataTable = dtDetails.Clone
        dt.Columns("Receiptnum").ColumnName = "收據編號"
        dt.Columns("stuid").ColumnName = "學號"
        dt.Columns("stuname").ColumnName = "學生姓名"
        dt.Columns("subclassname").ColumnName = "班別名稱"
        dt.Columns("datetime").ColumnName = "日期時間"
        dt.Columns("paymethod").ColumnName = "繳費方式"
        dt.Columns("amount").ColumnName = "繳費金額"
        dt.Columns("backamount").ColumnName = "退費金額"
        dt.Columns("handler").ColumnName = "承辦人員"
        dt.Columns("engname").ColumnName = "英文姓名"
        dt.Columns("tel1").ColumnName = "電話一"
        dt.Columns("tel2").ColumnName = "電話二"
        dt.Columns("email").ColumnName = "電郵"
        dt.Columns("extra").ColumnName = "額外資訊"
        dt.Columns("school").ColumnName = "學校"
        dt.Columns("dadname").ColumnName = "爸爸姓名"
        dt.Columns("mumname").ColumnName = "媽媽姓名"
        dt.Columns("dadmobile").ColumnName = "爸爸手機"
        dt.Columns("mummobile").ColumnName = "媽媽手機"
        dt.Columns("introid").ColumnName = "介紹人學號"
        dt.Columns("introname").ColumnName = "介紹人姓名"
        dt.Columns("schoolgrade").ColumnName = "學校年級"
        dt.Columns("schoolclass").ColumnName = "學校班級"
        For Each row As DataRow In dtDetails.Rows
            dt.Rows.Add(row.ItemArray)
        Next

        Dim id As Integer
        Select Case intColSchType
            Case 0
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_PrimarySchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 1
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_JuniorSchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 2
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_HighSchColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 3
                For index As Integer = 0 To dt.Rows.Count - 1
                    id = GetIntValue(dt.Rows(index).Item(c_UniversityColumnName))
                    dt.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
        End Select

        dt.Columns.RemoveAt(dt.Columns.IndexOf("classid"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("classname"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("subclassid"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("mobile"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("birthday"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("graduatefrom"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("ic"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("registerdate"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("reason"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("primarysch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("juniorsch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("highsch"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("university"))
        dt.Columns.RemoveAt(dt.Columns.IndexOf("paydatetime"))

        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow.Item(index).ToString = 1 Then

            Else
                dt.Columns.RemoveAt(dt.Columns.IndexOf(lstColName.Item(index).ToString))
            End If
        Next
        dgvDetails.DataSource = dt


        'For Each col In dgvMaster.Columns
        '    col.visible = False
        'Next
        'If dgvMaster.Rows.Count > 0 Then

        '    dgvMaster.Columns.Item(c_NameColumnName).DisplayIndex = 0
        '    dgvMaster.Columns.Item(c_NameColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_NameColumnName).HeaderText = My.Resources.subClassName
        '    dgvMaster.Columns.Item(c_StuRegCntColumnName).DisplayIndex = 1
        '    dgvMaster.Columns.Item(c_StuRegCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_StuRegCntColumnName).HeaderText = My.Resources.regCnt
        '    dgvMaster.Columns.Item(c_FeeClrCntColumnName).DisplayIndex = 2
        '    dgvMaster.Columns.Item(c_FeeClrCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_FeeClrCntColumnName).HeaderText = My.Resources.clearCnt
        '    dgvMaster.Columns.Item(c_FeeOweCntColumnName).DisplayIndex = 3
        '    dgvMaster.Columns.Item(c_FeeOweCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_FeeOweCntColumnName).HeaderText = My.Resources.oweCnt
        '    dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 4
        '    dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.kbCnt
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).DisplayIndex = 5
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_TotalPayColumnName).HeaderText = My.Resources.totalPay
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).DisplayIndex = 6
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_TotalBackColumnName).HeaderText = My.Resources.kbTotal
        '    dgvMaster.Columns.Item(c_AmountColumnName).DisplayIndex = 7
        '    dgvMaster.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvMaster.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.totalAmt
        'End If
        'For Each col In dgvDetails.Columns
        '    col.visible = False
        'Next
        'If dgvDetails.Rows.Count > 0 Then

        '    dgvDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
        '    dgvDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
        '    dgvDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
        '    dgvDetails.Columns.Item(c_StuIDColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
        '    dgvDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
        '    dgvDetails.Columns.Item(c_StuNameColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
        '    dgvDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
        '    dgvDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
        '    dgvDetails.Columns.Item(c_SchoolColumnName).DisplayIndex = 4
        '    dgvDetails.Columns.Item(c_SchoolColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 6
        '    dgvDetails.Columns.Item(c_PayMethodColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
        '    dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 7
        '    dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.payAmount
        '    dgvDetails.Columns.Item(c_BackAmountColumnName).DisplayIndex = 8
        '    dgvDetails.Columns.Item(c_BackAmountColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_BackAmountColumnName).HeaderText = "退費金額"
        '    dgvDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 9
        '    dgvDetails.Columns.Item(c_HandlerColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        '    dgvDetails.Columns.Item(c_ExtraColumnName).DisplayIndex = 10
        '    dgvDetails.Columns.Item(c_ExtraColumnName).Visible = True
        '    dgvDetails.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra
        'End If
        'Dim i As Integer = 10
        'For index As Integer = 0 To lstColName.Count - 1
        '    If lstColShow(index) = 1 Then
        '        dgvDetails.Columns.Item(lstColName(index)).DisplayIndex = i
        '        dgvDetails.Columns.Item(lstColName(index)).Visible = True
        '        dgvDetails.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
        '        i = i + 1
        '    End If
        'Next
        'If dgvDetails.Columns.Item(c_SchoolColumnName).Visible = True Then
        '    Dim id As Integer
        '    Select Case intColSchType
        '        Case 0
        '            For index As Integer = 0 To dgvDetails.Rows.Count - 1
        '                id = GetIntValue(dgvDetails.Rows(index).Cells(c_PrimarySchColumnName).Value)
        '                dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 1
        '            For index As Integer = 0 To dgvDetails.Rows.Count - 1
        '                id = GetIntValue(dgvDetails.Rows(index).Cells(c_JuniorSchColumnName).Value)
        '                dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 2
        '            For index As Integer = 0 To dgvDetails.Rows.Count - 1
        '                id = GetIntValue(dgvDetails.Rows(index).Cells(c_HighSchColumnName).Value)
        '                dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 3
        '            For index As Integer = 0 To dgvDetails.Rows.Count - 1
        '                id = GetIntValue(dgvDetails.Rows(index).Cells(c_UniversityColumnName).Value)
        '                dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '    End Select
        'End If
    End Sub


    Private Sub InitColSelections()
        lstColName.Add("英文姓名")
        lstColName.Add("電話一")
        lstColName.Add("電話二")
        lstColName.Add("電郵")
        lstColName.Add("學校")
        lstColName.Add("爸爸姓名")
        lstColName.Add("媽媽姓名")
        lstColName.Add("爸爸手機")
        lstColName.Add("媽媽手機")
        lstColName.Add("介紹人學號")
        lstColName.Add("介紹人姓名")
        lstColName.Add("學校年級")
        lstColName.Add("學校班級")
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub
    'Private Sub InitColSelections()
    '    lstColName.Add(c_EngNameColumnName)
    '    lstColName.Add(c_Tel1ColumnName)
    '    lstColName.Add(c_Tel2ColumnName)
    '    lstColName.Add(c_EmailColumnName)
    '    lstColName.Add(c_SchoolColumnName)
    '    lstColName.Add(c_GraduateFromColumnName)
    '    lstColName.Add(c_DadNameColumnName)
    '    lstColName.Add(c_MumNameColumnName)
    '    lstColName.Add(c_DadMobileColumnName)
    '    lstColName.Add(c_MumMobileColumnName)
    '    lstColName.Add(c_IntroIDColumnName)
    '    lstColName.Add(c_IntroNameColumnName)
    '    lstColName.Add(c_SchoolGradeColumnName)
    '    lstColName.Add(c_SchoolClassColumnName)
    '    FillArrayList(2, lstColName.Count, lstColShow)
    '    FillArrayList(1, lstColName.Count, lstColTxt)
    'End Sub

    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            'chklstClass.SetItemChecked(0, True)
            chklstClass.SelectedItem = chklstClass.Items(0)

            If chklstSubClass.Items.Count > 0 Then
                'chklstSubClass.SetItemChecked(0, True)
                chklstSubClass.SelectedItem = chklstSubClass.Items(0)
            End If
        End If
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvDetails.Columns.Count > 9 Then
            Dim lst As New ArrayList
            For index As Integer = 10 To dgvDetails.Columns.Count - 1
                If dgvDetails.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmPayStaSubClass_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click

        RefreshSubClassList()
        RefreshData()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intClassId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intClassId = dgvMaster.SelectedRows(0).Cells(c_IDColumnName).Value
            If intClassId > -1 Then
                dtDetails.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intClassId.ToString
                dgvDetails.DataSource = dtDetails.DefaultView
            Else
                dtDetails.DefaultView.RowFilter = ""
                dgvDetails.DataSource = dtDetails.DefaultView
            End If
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        Else
            lstClass.Remove(intC)
            For index As Integer = 0 To dtSubClassList.Rows.Count - 1
                If dtSubClassList.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClassList.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    lstSubClass.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClassList.Rows(index).Item(c_NameColumnName))
                End If
            Next
        End If
    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        Dim intC As Integer
        intC = lstSubClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstSubClass.Add(intC)
        Else
            lstSubClass.Remove(intC)
        End If
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvMaster)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvDetails)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub
    'Private Function getStuOweCount() As ArrayList
    Private Function getStuOweCount() As System.Collections.Specialized.NameValueCollection
        Return getStuOweCount(Date.MinValue, Date.MaxValue)
    End Function

    '    Private Function GetStuOweCount(ByVal startDate As Date, ByVal endDate As Date) As ArrayList
    Private Function GetStuOweCount(ByVal startDate As Date, ByVal endDate As Date) As System.Collections.Specialized.NameValueCollection
        Dim intSubClass As Integer
        Dim intClass As Integer
        Dim strStuId As String
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim intTotal3 As Integer
        Dim intTotal4 As Integer
        Dim intTotalAll As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "SUM(" + c_DiscountColumnName + ")"
        Dim strCompute6 As String = "COUNT(" + c_DiscountColumnName + ")"
        Dim dtPaySta As New DataTable
        Dim dtDisc As New DataTable
        Dim dtStu As New DataTable
        Dim dtStuAll As New DataTable

        ' Dim lstSubClass As New ArrayList
        Dim dtSubClass As New DataTable
        Dim stuOweArr As New ArrayList
        Dim test As New System.Collections.Specialized.NameValueCollection
        Dim countNum As New ArrayList

        'For k As Integer = 0 To lstClass.Count - 1
        Try
            'dtSubClass = New DataTable
            'dtSubClass = objCsol.GetSubClassID(lstClass(k))
            'lstSubClass = New ArrayList
            'For j As Integer = 0 To dtSubClass.Rows.Count - 1
            '    lstSubClass.Add(dtSubClass.Rows(j).Item("SubClassID"))
            'Next
            '====================================
            'dtPaySta = objCsol.ListStuOweByClass(lstClass)
            'dtDisc = objCsol.ListStuDiscByClass(lstClass)
            'dtStu = objCsol.ListStuBySubClass(lstSubClass)

            dtPaySta = GetListStuOweByClass(lstSubClass)
            dtDisc = GetListStuDiscByClass(lstSubClass)
            dtStuAll = GetListStuByClass(lstSubClass)
            '====================================
            Dim lst As New ArrayList
            For j As Integer = 0 To lstSubClass.Count - 1
                lst.Add(lstSubClass(j).ToString)
                'lstSubClass.Add(dtSubClass.Rows(j).Item("SubClassID"))
            Next



            Dim sturows() As DataRow = dtStuAll.Select(String.Format("{0} in ({1}) ANd {2} >= '{3}' AND {2} <= '{4}'", c_SubClassIDColumnName, String.Join(",", lst.ToArray(GetType(String))), c_RegDateColumnName, startDate, endDate))
            dtStu = dtStuAll.Clone()
            For Each row As DataRow In sturows
                dtStu.Rows.Add(row.ItemArray)
            Next


            dtDetails = New DataTable()
            dtDetails = dtStu.DefaultView.ToTable(True, c_IDColumnName, _
                                c_NameColumnName, c_SubClassNameColumnName, c_ClassIDColumnName, _
                                c_SeatNumColumnName, c_SalesPersonColumnName, _
                                c_SchoolGradeColumnName, c_SubClassIDColumnName, _
                                c_EngNameColumnName, c_BirthdayColumnName, c_DadNameColumnName, _
                                c_MumNameColumnName, c_Tel1ColumnName, c_Tel2ColumnName, _
                                c_DadMobileColumnName, c_MumMobileColumnName, c_MobileColumnName, _
                                c_SchoolColumnName, c_SchoolClassColumnName, c_GraduateFromColumnName, _
                                c_IntroIDColumnName, c_IntroNameColumnName, c_ICColumnName, _
                                c_EmailColumnName, c_PrimarySchColumnName, c_JuniorSchColumnName, _
                                c_HighSchColumnName, c_UniversityColumnName, c_CancelColumnName)
            dtDetails.Columns.Add(c_AmountColumnName, GetType(System.Int32))
            dtDetails.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
            dtDetails.Columns.Add(c_DiscountColumnName, GetType(System.Int32))
            dtDetails.Columns.Add(c_FeeOweColumnName, GetType(System.Int32))
            dtDetails.Columns.Add(c_Cancel2ColumnName, GetType(System.String))

            lstClassFee = New ArrayList
            For index As Integer = 0 To lstClass.Count - 1
                lstClassFee.Add(frmMain.GetClassFee2(lstClass(index)))
            Next

            For i = 0 To dtDetails.Rows.Count - 1
                If dtDetails.Rows(i).Item(c_CancelColumnName).ToString = "0" Then
                    intSubClass = dtDetails.Rows(i).Item(c_SubClassIDColumnName)
                    intClass = dtDetails.Rows(i).Item(c_ClassIDColumnName)
                    If lstSubClass.Contains(intSubClass) Then
                        strStuId = dtDetails.Rows(i).Item(c_IDColumnName)
                        intTotal = GetClassFeeFromList(intClass)
                        strCompute2 = c_SubClassIDColumnName & "=" & intSubClass.ToString & _
                                        " AND " & c_StuIDColumnName & "='" & strStuId & "'"
                        If dtPaySta.Compute(strCompute3, strCompute2) > 0 Then
                            intTotal2 = dtPaySta.Compute(strCompute5, strCompute2) 'discount
                            intTotal3 = dtPaySta.Compute(strCompute1, strCompute2) 'pay
                            intTotal4 = dtPaySta.Compute(strCompute4, strCompute2) 'back
                        Else
                            intTotal2 = 0
                            intTotal3 = 0
                            intTotal4 = 0
                        End If

                        If dtDisc.Compute(strCompute6, strCompute2) > 0 Then
                            intTotal2 = intTotal2 + dtDisc.Compute(strCompute5, strCompute2) 'discount
                        End If

                        dtDetails.Rows(i).Item(c_AmountColumnName) = intTotal - intTotal2
                        dtDetails.Rows(i).Item(c_PayAmountColumnName) = intTotal3 - intTotal4
                        dtDetails.Rows(i).Item(c_FeeOweColumnName) = intTotal - intTotal2 - _
                                        intTotal3 + intTotal4
                        dtDetails.Rows(i).Item(c_DiscountColumnName) = intTotal2
                        intTotalAll = intTotalAll + dtDetails.Rows(i).Item(c_FeeOweColumnName)
                    End If
                End If
            Next

            Dim f As String = ""
            If lstSubClass.Count > 0 Then
                f = c_SubClassIDColumnName & "=" & lstSubClass(0).ToString
            End If
            For index As Integer = 1 To lstSubClass.Count - 1
                f = f & " OR " & c_SubClassIDColumnName & "=" & lstSubClass(index).ToString
            Next
            If f = "" Then
                dtDetails.DefaultView.RowFilter = "Cancel=0"
            Else
                dtDetails.DefaultView.RowFilter = f + " And Cancel=0"
            End If

            dtDetails.DefaultView.RowFilter = "not FeeOwe=0"

            For i As Integer = 0 To lstSubClass.Count - 1
                stuOweArr.Add(dtDetails.Compute("COUNT(ID)", "SubClassID=" + lstSubClass(i).ToString + " AND FeeOwe <= 0"))
                test.Add(lstSubClass(i).ToString, dtDetails.Compute("COUNT(ID)", "SubClassID=" + lstSubClass(i).ToString + " AND FeeOwe <= 0"))
            Next


        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try

        'Next
        'Return stuOweArr
        Return test
    End Function

    Private Shared Function GetListStuOweByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetListStuOweByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetListStuOweByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetListStuOweByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListStuDiscByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetListStuDiscByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetListStuDiscByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetListStuDiscByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Shared Function GetListStuByClass(ByVal lstClass As ArrayList) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        Dim list As New List(Of String)
        For i As Integer = 0 To lstClass.Count - 1
            list.Add(lstClass(i))
        Next
        RequestParams.Add("subclassid", String.Join(",", list.ToArray))
        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaSubClass", "GetListStuByClass", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetListStuByClass").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetListStuByClass"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Function GetClassFeeFromList(ByVal intId As Integer) As Integer
        GetClassFeeFromList = 0
        Dim index As Integer = -1
        index = lstClass.IndexOf(intId)
        If index > -1 And index < lstClassFee.Count Then
            GetClassFeeFromList = lstClassFee(index)
        End If
    End Function
End Class