﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmReceiptModRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmReceiptModRec))
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.radbutReceipt = New System.Windows.Forms.RadioButton
        Me.tboxReceipt = New System.Windows.Forms.TextBox
        Me.radbutUser = New System.Windows.Forms.RadioButton
        Me.tboxUser = New System.Windows.Forms.TextBox
        Me.chkboxDate = New System.Windows.Forms.CheckBox
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label5 = New System.Windows.Forms.Label
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.butFilter = New System.Windows.Forms.Button
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'radbutReceipt
        '
        resources.ApplyResources(Me.radbutReceipt, "radbutReceipt")
        Me.radbutReceipt.Checked = True
        Me.radbutReceipt.Name = "radbutReceipt"
        Me.radbutReceipt.TabStop = True
        Me.radbutReceipt.UseVisualStyleBackColor = True
        '
        'tboxReceipt
        '
        resources.ApplyResources(Me.tboxReceipt, "tboxReceipt")
        Me.tboxReceipt.Name = "tboxReceipt"
        '
        'radbutUser
        '
        resources.ApplyResources(Me.radbutUser, "radbutUser")
        Me.radbutUser.Name = "radbutUser"
        Me.radbutUser.UseVisualStyleBackColor = True
        '
        'tboxUser
        '
        resources.ApplyResources(Me.tboxUser, "tboxUser")
        Me.tboxUser.Name = "tboxUser"
        '
        'chkboxDate
        '
        resources.ApplyResources(Me.chkboxDate, "chkboxDate")
        Me.chkboxDate.Name = "chkboxDate"
        Me.chkboxDate.UseVisualStyleBackColor = True
        '
        'dtpickFrom
        '
        resources.ApplyResources(Me.dtpickFrom, "dtpickFrom")
        Me.dtpickFrom.Name = "dtpickFrom"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'dtpickTo
        '
        resources.ApplyResources(Me.dtpickTo, "dtpickTo")
        Me.dtpickTo.Name = "dtpickTo"
        '
        'butFilter
        '
        resources.ApplyResources(Me.butFilter, "butFilter")
        Me.butFilter.Name = "butFilter"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'frmReceiptModRec
        '
        Me.AcceptButton = Me.butFilter
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkboxDate)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.butFilter)
        Me.Controls.Add(Me.tboxUser)
        Me.Controls.Add(Me.radbutUser)
        Me.Controls.Add(Me.tboxReceipt)
        Me.Controls.Add(Me.radbutReceipt)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmReceiptModRec"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radbutReceipt As System.Windows.Forms.RadioButton
    Friend WithEvents tboxReceipt As System.Windows.Forms.TextBox
    Friend WithEvents radbutUser As System.Windows.Forms.RadioButton
    Friend WithEvents tboxUser As System.Windows.Forms.TextBox
    Friend WithEvents chkboxDate As System.Windows.Forms.CheckBox
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
