﻿Public Class frm2UpdKB
    Private intId As Integer
    Private strOldReason As String
    Private intOldAmount As Integer

    Public Sub New(ByVal ID As Integer, ByVal reason As String, _
                   ByVal amt As Integer)

        InitializeComponent()
        intId = ID
        strOldReason = reason
        tboxReason.Text = reason
        intOldAmount = amt
        tboxAmount.Text = amt.ToString
        frmMain.SetOkState(False)

    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If IsNumeric(tboxAmount.Text) Then
            Dim intAmount As Integer = CInt(tboxAmount.Text)
            If intAmount >= 0 Then
                objCsol.UpdateMB(intId, intOldAmount, intAmount, frmMain.GetUsrId, _
                                 strOldReason, tboxReason.Text, tboxReasonMod.Text)
                frmMain.SetOkState(True)
                Me.Close()
            Else
                ShowMsg(My.Resources.msgWrontAmount)
            End If
        Else
            ShowMsg(My.Resources.msgWrontAmount)
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        MsgBox(msg, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class