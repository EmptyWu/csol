﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddMK
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2AddMK))
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.tboxReason = New System.Windows.Forms.TextBox
        Me.tboxHandler = New System.Windows.Forms.TextBox
        Me.tboxSubclass = New System.Windows.Forms.TextBox
        Me.tboxClass = New System.Windows.Forms.TextBox
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxAmount = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.tboxStuId = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.SuspendLayout()
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'tboxReason
        '
        resources.ApplyResources(Me.tboxReason, "tboxReason")
        Me.tboxReason.Name = "tboxReason"
        '
        'tboxHandler
        '
        Me.tboxHandler.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.tboxHandler, "tboxHandler")
        Me.tboxHandler.Name = "tboxHandler"
        Me.tboxHandler.ReadOnly = True
        '
        'tboxSubclass
        '
        Me.tboxSubclass.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.tboxSubclass, "tboxSubclass")
        Me.tboxSubclass.Name = "tboxSubclass"
        Me.tboxSubclass.ReadOnly = True
        '
        'tboxClass
        '
        Me.tboxClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.tboxClass, "tboxClass")
        Me.tboxClass.Name = "tboxClass"
        Me.tboxClass.ReadOnly = True
        '
        'tboxName
        '
        Me.tboxName.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        Me.tboxName.ReadOnly = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxAmount
        '
        resources.ApplyResources(Me.tboxAmount, "tboxAmount")
        Me.tboxAmount.Name = "tboxAmount"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'tboxStuId
        '
        Me.tboxStuId.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.tboxStuId, "tboxStuId")
        Me.tboxStuId.Name = "tboxStuId"
        Me.tboxStuId.ReadOnly = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'frm2AddMK
        '
        Me.AcceptButton = Me.butSave
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.dtpickDate)
        Me.Controls.Add(Me.tboxReason)
        Me.Controls.Add(Me.tboxHandler)
        Me.Controls.Add(Me.tboxSubclass)
        Me.Controls.Add(Me.tboxClass)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.tboxAmount)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.tboxStuId)
        Me.Controls.Add(Me.Label2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddMK"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents tboxReason As System.Windows.Forms.TextBox
    Friend WithEvents tboxHandler As System.Windows.Forms.TextBox
    Friend WithEvents tboxSubclass As System.Windows.Forms.TextBox
    Friend WithEvents tboxClass As System.Windows.Forms.TextBox
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxAmount As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents tboxStuId As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
End Class
