﻿Imports System.Text
'20100424 autoupdate by sherry
Imports System.Configuration

Public Class CsolBL

    Inherits MarshalByRefObject
    Implements ICsol
    'Accounting
    Public Function ListPayStatistics(ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataSet Implements ICsol.ListPayStatistics
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)

        Dim dtPayStaByDate As DataTable = objAccounting.GetPayStaByDate(DateFrom, DateTo)
        dtPayStaByDate.TableName = c_PayStaByDateTableName
        ds.Tables.Add(dtPayStaByDate)

        Dim dtMkeepStaByDate As DataTable = objAccounting.GetMKeepStaByDate(DateFrom, DateTo)
        dtMkeepStaByDate.TableName = c_MKeepStaByDateTableName
        ds.Tables.Add(dtMkeepStaByDate)

        Dim dtMBackStaByDate As DataTable = objAccounting.GetMBackStaByDate(DateFrom, DateTo)
        dtMBackStaByDate.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtMBackStaByDate)

        Return ds
    End Function

    Public Overloads Function ListPayStaByClass(ByVal classId As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataSet Implements ICsol.ListPayStaByClass
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)
        Dim dtClassPaySta As DataTable
        Dim dtClassMKSta As DataTable
        Dim dtClassMBSta As DataTable

        If classId.Count > 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classId(0), DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(classId(0), DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dtClassPaySta.Merge(objAccounting.GetPayStaByClass(classId(index), DateFrom, DateTo))
                dtClassMKSta.Merge(objAccounting.GetMKeepStaByClass(classId(index), DateFrom, DateTo))
                dtClassMBSta.Merge(objAccounting.GetMBackStaByClass(classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classId(0), DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(classId(0), DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(classId(0), DateFrom, DateTo)
        Else
            dtClassPaySta = objAccounting.GetPayStaByClass(-1, DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(-1, DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(-1, DateFrom, DateTo)
        End If
        dtClassPaySta.TableName = c_PayStaByDateTableName
        dtClassMKSta.TableName = c_MKeepStaByDateTableName
        dtClassMBSta.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtClassPaySta)
        ds.Tables.Add(dtClassMKSta)
        ds.Tables.Add(dtClassMBSta)

        Return ds
    End Function

    Public Overloads Function ListPayStaByClass(ByVal classId As ArrayList) As System.Data.DataSet Implements ICsol.ListPayStaByClass
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)
        Dim dtClassPaySta As DataTable
        Dim dtClassMKSta As DataTable
        Dim dtClassMBSta As DataTable

        If classId.Count > 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classId(0))
            'dtClassPaySta = DataBaseAccess.Accounting.GetPayStaByClass(classId)

            dtClassMKSta = objAccounting.GetMKeepStaByClass(classId(0))
            dtClassMBSta = objAccounting.GetMBackStaByClass(classId(0))
            For index As Integer = 1 To classId.Count - 1
                dtClassPaySta.Merge(objAccounting.GetPayStaByClass(classId(index)))
                dtClassMKSta.Merge(objAccounting.GetMKeepStaByClass(classId(index)))
                dtClassMBSta.Merge(objAccounting.GetMBackStaByClass(classId(index)))
            Next


        ElseIf classId.Count = 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classId(0))
            dtClassMKSta = objAccounting.GetMKeepStaByClass(classId(0))
            dtClassMBSta = objAccounting.GetMBackStaByClass(classId(0))
        Else
            dtClassPaySta = objAccounting.GetPayStaByClass(-1)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(-1)
            dtClassMBSta = objAccounting.GetMBackStaByClass(-1)
        End If
        dtClassPaySta.TableName = c_PayStaByDateTableName
        dtClassMKSta.TableName = c_MKeepStaByDateTableName
        dtClassMBSta.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtClassPaySta)
        ds.Tables.Add(dtClassMKSta)
        ds.Tables.Add(dtClassMBSta)

        Return ds
    End Function
    Public Overloads Function ListPayStaBySubClass(ByVal classID As ArrayList, ByVal subClassID As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataSet Implements ICsol.ListPayStaBySubClass
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)
        Dim dtClassPaySta As DataTable
        Dim dtClassMKSta As DataTable
        Dim dtClassMBSta As DataTable

        If classID.Count > 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classID(0), DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(classID(0), DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(classID(0), DateFrom, DateTo)
            For index As Integer = 1 To classID.Count - 1
                dtClassPaySta.Merge(objAccounting.GetPayStaByClass(classID(index), DateFrom, DateTo))
                dtClassMKSta.Merge(objAccounting.GetMKeepStaByClass(classID(index), DateFrom, DateTo))
                dtClassMBSta.Merge(objAccounting.GetMBackStaByClass(classID(index), DateFrom, DateTo))
            Next
        ElseIf classID.Count = 1 Then
            dtClassPaySta = objAccounting.GetPayStaByClass(classID(0), DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(classID(0), DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(classID(0), DateFrom, DateTo)
        Else
            dtClassPaySta = objAccounting.GetPayStaByClass(-1, DateFrom, DateTo)
            dtClassMKSta = objAccounting.GetMKeepStaByClass(-1, DateFrom, DateTo)
            dtClassMBSta = objAccounting.GetMBackStaByClass(-1, DateFrom, DateTo)
        End If
        dtClassPaySta.TableName = c_PayStaByDateTableName
        dtClassMKSta.TableName = c_MKeepStaByDateTableName
        dtClassMBSta.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtClassPaySta)
        ds.Tables.Add(dtClassMKSta)
        ds.Tables.Add(dtClassMBSta)

        Return ds
    End Function

    Public Overloads Function ListPayStaBySubClass(ByVal classID As ArrayList, ByVal subClassID As ArrayList) As System.Data.DataSet Implements ICsol.ListPayStaBySubClass
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)
        Dim dtClassPaySta As New DataTable
        Dim dtClassMKSta As New DataTable
        Dim dtClassMBSta As New DataTable

        If subClassID.Count >= 1 Then
            dtClassPaySta = DataBaseAccess.Accounting.GetPayStaBySubClassNoDAte(subClassID)
        Else
            dtClassPaySta = objAccounting.GetPayStaBySubClass(-1)
        End If

        If subClassID.Count >= 1 Then
            dtClassMKSta = DataBaseAccess.Accounting.GetMKeepStaBySubClassNoDate(subClassID)
            dtClassMBSta = DataBaseAccess.Accounting.GetMBackStaBySubClassNoDate(subClassID)
        Else
            Dim ar As New ArrayList
            ar.Add(-1)
            dtClassMKSta = DataBaseAccess.Accounting.GetMKeepStaBySubClassNoDate(ar)
            dtClassMBSta = DataBaseAccess.Accounting.GetMBackStaBySubClassNoDate(ar)
        End If
        dtClassPaySta.TableName = c_PayStaByDateTableName
        dtClassMKSta.TableName = c_MKeepStaByDateTableName
        dtClassMBSta.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtClassPaySta.Copy())
        ds.Tables.Add(dtClassMKSta.Copy())
        ds.Tables.Add(dtClassMBSta.Copy())

        Return ds
    End Function

    Public Function ListPayStaForDates(ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListPayStaForDates
        Dim objAccounting As New Accounting
        Dim ds As DataSet
        Dim dtTemp As DataTable
        Dim dtTemp2 As DataTable
        Dim dtTemp3 As DataTable
        Dim dtPayStaForDates As New DataTable(c_PayStaForDatesTableName)
        Dim intTotal As Integer = 0
        Dim intTotal2 As Integer = 0
        Dim intTotal3 As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_ReceiptNumColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_PayAmountColumnName + ")"
        Dim i As Integer = 0
        Dim dt As Date

        ds = ListPayStatistics(DateFrom, DateTo)
        dtPayStaForDates = ds.Tables(c_PayStaByDateTableName).DefaultView.ToTable( _
                True, New String() {c_DateTimeColumnName})
        dtPayStaForDates.Merge(ds.Tables(c_MKeepStaByDateTableName).DefaultView.ToTable( _
                True, New String() {c_DateTimeColumnName}))
        dtPayStaForDates.Merge(ds.Tables(c_MBackStaByDateTableName).DefaultView.ToTable( _
                True, New String() {c_DateTimeColumnName}))
        dtPayStaForDates = dtPayStaForDates.DefaultView.ToTable( _
                True, New String() {c_DateTimeColumnName})

        dtPayStaForDates.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_PayAmountColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_KBCntColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_MBAmountColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_MKAmountColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_TotalDataCntColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_TotalAmountColumnName, GetType(System.Int32))
        dtPayStaForDates.Columns.Add(c_DataCntColumnName, GetType(System.Int32))

        dtTemp = ds.Tables(c_PayStaByDateTableName)
        dtTemp2 = ds.Tables(c_MKeepStaByDateTableName)
        dtTemp3 = ds.Tables(c_MBackStaByDateTableName)
        If dtPayStaForDates.Rows.Count > 0 Then
            For i = 0 To dtPayStaForDates.Rows.Count - 1
                dt = dtPayStaForDates.Rows(i).Item(c_DateTimeColumnName)
                strCompute2 = c_DateTimeColumnName + "=" + dt.ToString

                intTotal = dtTemp.Compute(strCompute3, strCompute2)
                intTotal2 = dtTemp2.Compute(strCompute3, strCompute2)
                intTotal3 = dtTemp3.Compute(strCompute3, strCompute2)
                dtPayStaForDates.Rows(i).Item(c_DataCntColumnName) = intTotal + _
                            intTotal2 + intTotal3
                dtPayStaForDates.Rows(i).Item(c_KBCntColumnName) = intTotal2 + _
                            intTotal3

                intTotal = dtTemp.Compute(strCompute1, strCompute2)
                intTotal2 = dtTemp2.Compute(strCompute4, strCompute2)
                intTotal3 = dtTemp3.Compute(strCompute4, strCompute2)
                dtPayStaForDates.Rows(i).Item(c_PayAmountColumnName) = intTotal + _
                            intTotal2 + intTotal3

                intTotal2 = dtTemp2.Compute(strCompute1, strCompute2)
                intTotal3 = dtTemp3.Compute(strCompute1, strCompute2)
                dtPayStaForDates.Rows(i).Item(c_KBAmountColumnName) = intTotal2 + _
                            intTotal3
                dtPayStaForDates.Rows(i).Item(c_MBAmountColumnName) = intTotal3
                dtPayStaForDates.Rows(i).Item(c_MKAmountColumnName) = intTotal2

                intTotal = dtPayStaForDates.Rows(i).Item(c_PayAmountColumnName)
                dtPayStaForDates.Rows(i).Item(c_TotalAmountColumnName) = intTotal - _
                            intTotal2 - intTotal3

                intTotal = dtPayStaForDates.Rows(i).Item(c_DataCntColumnName)
                intTotal2 = dtPayStaForDates.Rows(i).Item(c_KBCntColumnName)
                dtPayStaForDates.Rows(i).Item(c_TotalDataCntColumnName) = intTotal _
                            + intTotal2
            Next
        End If

        Return dtPayStaForDates
    End Function

    Public Function ListDatePaySta(ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataSet Implements ICsol.ListDatePaySta
        Dim objAccounting As New Accounting
        Dim ds As New DataSet(c_PayStaByDateDataSetName)

        Dim dtPayStaByDate As DataTable = objAccounting.GetDatePaySta(DateFrom, DateTo)
        dtPayStaByDate.TableName = c_PayStaByDateTableName
        ds.Tables.Add(dtPayStaByDate)

        Dim dtMkeepStaByDate As DataTable = objAccounting.GetDateKBSta(DateFrom, DateTo)
        dtMkeepStaByDate.TableName = c_MKeepStaByDateTableName
        ds.Tables.Add(dtMkeepStaByDate)

        Return ds
    End Function

    Public Overloads Function ListPersonalSales(ByVal usrId As String, ByVal classId As ArrayList, _
                ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListPersonalSales
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetSalesSta(usrId, classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetSalesSta(usrId, classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetSalesSta(usrId, classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetSalesSta(usrId, -1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Overloads Function ListPersonalSales(ByVal usrId As String, ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.ListPersonalSales
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetSalesSta(usrId, classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetSalesSta(usrId, classId(index)))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetSalesSta(usrId, classId(0))
        Else
            dt = objAccounting.GetSalesSta(usrId, -1)
        End If

        Return dt
    End Function

    Public Overloads Function ListPersonalDisc(ByVal usrId As String, ByVal classId As ArrayList, _
            ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListPersonalDisc
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetSalesDisc(usrId, classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetSalesDisc(usrId, classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetSalesDisc(usrId, classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetSalesDisc(usrId, -1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Overloads Function ListPersonalDisc(ByVal usrId As String, ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.ListPersonalDisc
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetSalesDisc(usrId, classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetSalesDisc(usrId, classId(index)))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetSalesDisc(usrId, classId(0))
        Else
            dt = objAccounting.GetSalesDisc(usrId, -1)
        End If

        Return dt
    End Function

    Public Function ListPersonalHandle(ByVal usrId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListPersonalHandle
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetPayStaByHandler(usrId, DateFrom, DateTo)

        Return dt
    End Function

    Public Overloads Function ListStuOweByClass(ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.ListStuOweByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable

        If classId.Count > 1 Then
            dt = DataBaseAccess.Accounting.ListStuOweByClass(classId)
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuOweByClass(classId(0))
        Else
            dt = objAccounting.GetStuOweByClass(-1)
        End If

        Return dt
    End Function

    Public Overloads Function ListStuOweByClass(ByVal classId As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListStuOweByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            'dt = objAccounting.GetStuOweByClass(classId(0), DateFrom, DateTo)
            'For index As Integer = 1 To classId.Count - 1
            '    dt.Merge(objAccounting.GetStuOweByClass(classId(index), DateFrom, DateTo))
            'Next
            dt = DataBaseAccess.Accounting.ListStuOweByClass(classId, DateFrom, DateTo)
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuOweByClass(classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetStuOweByClass(-1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Overloads Function ListStuDiscByClass(ByVal classId As ArrayList) _
            As System.Data.DataTable Implements ICsol.ListStuDiscByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetStuDiscByClass(classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetStuDiscByClass(classId(index)))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuDiscByClass(classId(0))
        Else
            dt = objAccounting.GetStuDiscByClass(-1)
        End If

        Return dt
    End Function

    Public Overloads Function ListStuDiscByClass(ByVal classId As ArrayList, _
                                                 ByVal DateFrom As Date, _
                                                 ByVal DateTo As Date) As  _
                                                 System.Data.DataTable Implements _
                                                 ICsol.ListStuDiscByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetStuDiscByClass(classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetStuDiscByClass(classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuDiscByClass(classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetStuDiscByClass(-1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Function ListStuMKByClass(ByVal classId As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListStuMKByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetStuMKByClass(classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetStuMKByClass(classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuMKByClass(classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetStuMKByClass(-1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Function ListStuMBByClass(ByVal classId As ArrayList, ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListStuMBByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        If classId.Count > 1 Then
            dt = objAccounting.GetStuMBByClass(classId(0), DateFrom, DateTo)
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetStuMBByClass(classId(index), DateFrom, DateTo))
            Next
        ElseIf classId.Count = 1 Then
            dt = objAccounting.GetStuMBByClass(classId(0), DateFrom, DateTo)
        Else
            dt = objAccounting.GetStuMBByClass(-1, DateFrom, DateTo)
        End If

        Return dt
    End Function

    Public Function GetStuNameListByClass(ByVal id As Integer) As DataTable Implements ICsol.GetStuNameListByClass
        Dim obj As New StuInfo
        Return obj.GetStuNameListByClass(id)

    End Function

    Public Function GetStuNameListBySc(ByVal id As Integer) As DataTable Implements ICsol.GetStuNameListBySc
        Dim obj As New StuInfo
        Return obj.GetStuNameListBySc(id)
    End Function

    Public Function ListJournalGrp() As System.Data.DataTable Implements ICsol.ListJournalGrp
        Dim objAccounting As New Accounting
        Dim dt As DataTable

        dt = objAccounting.GetJournalGrp()
        Return dt
    End Function

    Public Sub UpdateJournalGrp(ByVal ChangedDt As DataTable) Implements ICsol.UpdateJournalGrp
        Dim obj As New Accounting
        obj.UpdateJournalGrp(ChangedDt)
    End Sub

    Public Sub UpdateTeleInterviewType(ByVal ChangedDt As DataTable) Implements ICsol.UpdateTeleInterviewType
        Dim obj As New StuInfo
        obj.UpdateTeleInterviewType(ChangedDt)
    End Sub

    Public Overloads Sub AddTeleInterview(ByVal StuID As String, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String) Implements ICsol.AddTeleInterview
        Dim obj As New StuInfo
        obj.AddTeleInterview(StuID, IDate, TypeID, Content, HandlerID)
    End Sub

    Public Sub UpdTeleInterview(ByVal ID As Integer, _
                                ByVal TypeID As Integer, ByVal Content As String) Implements ICsol.UpdTeleInterview
        Dim obj As New StuInfo
        obj.UpdTeleInterview(ID, TypeID, Content)
    End Sub

    Public Overloads Sub AddTeleInterview(ByVal StuID As ArrayList, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String) Implements ICsol.AddTeleInterview
        Dim obj As New StuInfo
        obj.AddTeleInterview(StuID, IDate, TypeID, Content, HandlerID)
    End Sub

    Public Sub UpdateDiscDictType(ByVal ChangedDt As DataTable) Implements ICsol.UpdateDiscDictType
        Dim obj As New Accounting
        obj.UpdateDiscDictType(ChangedDt)
    End Sub

    Public Sub UpdateDiscDict(ByVal ChangedDt As DataTable) Implements ICsol.UpdateDiscDict
        Dim obj As New Accounting
        obj.UpdateDiscDict(ChangedDt)
    End Sub

    Public Sub UpdateRemarksDict(ByVal ChangedDt As DataTable) Implements ICsol.UpdateRemarksDict
        Dim obj As New Accounting
        obj.UpdateReceiptRemarks(ChangedDt)
    End Sub

    Public Sub UpdateReceiptPrint(ByVal ChangedDt As DataTable) Implements ICsol.UpdateReceiptPrint
        Dim obj As New Admin
        obj.UpdateReceiptPrint(ChangedDt)
    End Sub

    Public Sub UpdateSubjects(ByVal ChangedDt As DataTable) Implements ICsol.UpdateSubjects
        Dim obj As New ClassInfo
        obj.UpdateSubjects(ChangedDt)
    End Sub

    Public Function AddJournalRec(ByVal GroupID As Int32, _
        ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
        ByVal UserID As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
        ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String) As Int32 _
        Implements ICsol.AddJournalRec

        Dim objAccounting As New Accounting
        Return objAccounting.AddJournalRec(GroupID, Item, Amount, DateTime, _
            UserID, SerialNum, ChequeNum, Remarks3, Remarks4, Remarks5)

    End Function

    Public Sub DeleteJournalRec(ByVal ID As Integer) Implements ICsol.DeleteJournalRec
        Dim obj As New Accounting
        obj.DelJournalRec(ID)
    End Sub

    Public Sub UpdJournalRec(ByVal ID As Int32, ByVal GroupID As Int32, _
       ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
       ByVal UserID As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
       ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String) _
       Implements ICsol.UpdJournalRec

        Dim objAccounting As New Accounting
        objAccounting.UpdateJournalRec(ID, GroupID, Item, Amount, DateTime, _
            UserID, SerialNum, ChequeNum, Remarks3, Remarks4, Remarks5)

    End Sub

    Public Overloads Function ListJournalSta(ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListJournalSta
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetJournalSta(DateFrom, DateTo)
        Return dt
    End Function

    Public Overloads Function ListJournalSta() As System.Data.DataTable Implements ICsol.ListJournalSta
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetJournalSta()
        Return dt
    End Function

    Public Overloads Function ListPayStaByKeyword(ByVal method As Integer, _
                                                  ByVal keyword As String, _
                                                  ByVal dateFrom As Date, _
                                                  ByVal dateTo As Date) As System.Data.DataTable Implements ICsol.ListPayStaByKeyword
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetPayStaByKeyword(method, keyword, dateFrom, dateTo)
        Return dt
    End Function

    Public Overloads Function ListPayStaByKeyword(ByVal method As Integer, _
                                                  ByVal keyword As String) As System.Data.DataTable Implements ICsol.ListPayStaByKeyword
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetPayStaByKeyword(method, keyword)
        Return dt
    End Function

    Public Function ListDiscountStaByClass(ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.ListDiscountStaByClass
        Dim objAccounting As New Accounting
        Dim dt As DataTable

        If classId.Count > 1 Then
            dt = objAccounting.GetDiscountStaByClass(classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(objAccounting.GetDiscountStaByClass(classId(index)))
            Next
        Else
            dt = objAccounting.GetDiscountStaByClass(classId(0))
        End If

        Return dt
    End Function

    Public Function ListChequeValidSta(ByVal DateFrom As Date, ByVal DateTo As Date) As System.Data.DataTable Implements ICsol.ListChequeValidSta
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetChequeValidSta(DateFrom, DateTo)
        Return dt
    End Function

    Public Function ListPayModifyRec() As System.Data.DataTable Implements ICsol.ListPayModifyRec
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetPayModifyRec()
        Return dt
    End Function

    Public Function ListDiscModifyRec() As System.Data.DataTable Implements ICsol.ListDiscModifyRec
        Dim objAccounting As New Accounting
        Dim dt As DataTable
        dt = objAccounting.GetDiscModifyRec()
        Return dt
    End Function

    'Admin
    Public Function ListEmployee() As System.Data.DataTable Implements ICsol.ListEmployee
        Dim objAdmin As New Admin
        Dim dt As DataTable
        dt = objAdmin.GetEmployeeList()
        Return dt
    End Function

    Public Sub ModifyUsrStatus(ByVal UsrID As String, ByVal Status As Int32) _
        Implements ICsol.ModifyUsrStatus
        Dim objAdmin As New Admin
        objAdmin.ModifyUsrStatus(UsrID, Status)
    End Sub

    Public Sub DeleteUsrByID(ByVal UsrID As String) Implements ICsol.DeleteUsrByID
        Dim objAdmin As New Admin
        objAdmin.DeleteUsrByID(UsrID)

    End Sub

    Public Sub ModifyUsrCard(ByVal UsrID As String, ByVal CardNum As String) _
        Implements ICsol.ModifyUsrCard
        Dim objAdmin As New Admin
        objAdmin.ModifyUsrCard(UsrID, CardNum)

    End Sub

    '1204 updated by sherry end
    Public Function ModifyStuCard(ByVal ID As String, ByVal CardNum As String) As Integer _
        Implements ICsol.ModifyStuCard
        Dim obj As New StuInfo
        Dim intResult As Integer = 0 'success
        Dim dt As New DataTable
        dt = obj.GetPunchStuInfoByCard(CardNum.Trim)
        If dt.Rows.Count > 0 Then
            Return 1 'Cardnum exists
        End If
        obj.UpdateStuCard(ID, CardNum)

        Return intResult
    End Function
    '1204 updated by sherry end
    Public Sub ModifyStuCard2(ByVal ID As String, ByVal CardNum As String) _
        Implements ICsol.ModifyStuCard2
        Dim obj As New StuInfo
        obj.UpdateStuCard(ID, CardNum)

    End Sub

    Public Sub ModifyUsrPwd(ByVal UsrID As String, ByVal Pwd As String) _
        Implements ICsol.ModifyUsrPwd
        Dim objAdmin As New Admin
        objAdmin.ModifyUsrPwd(UsrID, Pwd)

    End Sub

    Public Sub AddUser(ByVal ID As String, ByVal Name As String, ByVal PWD As String, _
         ByVal Modify As Int32, ByVal Status As Int32, ByVal GroupID As Int32, _
         ByVal CardNum As String, ByVal Phone1 As String, ByVal Phone2 As String, _
         ByVal IC As String, ByVal Mobile As String, ByVal Guardian As String, _
         ByVal GuardMobile As String, ByVal PostalCode As String, _
         ByVal Address As String, ByVal Email As String, ByVal Remarks As String, _
         ByVal Sex As String, ByVal EnglishName As String, ByVal Birthday As Date, _
         ByVal Horoscope As String, ByVal AccntName As String, _
         ByVal OnBoardDate As Date, ByVal BloodType As Int32, _
         ByVal JuniorSchID As Int32, ByVal HighSchID As Int32, _
         ByVal UniversityID As Int32, ByVal IsSales As Int32) Implements ICsol.AddUser

        Dim objAdmin As New Admin
        objAdmin.AddUser(ID, Name, PWD, Modify, Status, GroupID, CardNum, _
                              Phone1, Phone2, IC, Mobile, Guardian, GuardMobile, _
                              PostalCode, Address, Email, Remarks, Sex, _
                              EnglishName, Birthday, Horoscope, AccntName, _
                              OnBoardDate, BloodType, JuniorSchID, HighSchID, _
                              UniversityID, IsSales)
    End Sub

    Public Function GetUsrByID(ByVal UsrID As String) As System.Data.DataTable Implements ICsol.GetUsrByID
        Dim objAdmin As New Admin
        Dim dt As DataTable
        dt = objAdmin.GetUsrByID(UsrID)
        Return dt
    End Function

    Public Sub UpdateUsrInfo(ByVal ChangedUser As DataTable) Implements ICsol.UpdateUsrInfo
        Dim objAdmin As New Admin
        objAdmin.UpdateUsrInfo(ChangedUser)
    End Sub

    Public Function ListUsrAllAuthority(ByVal UsrId As String) As System.Data.DataSet Implements ICsol.ListUsrAllAuthority
        Dim objAdmin As New Admin
        Dim ds As New DataSet(c_UsrAuthorityDSName)
        Dim dtUsrAuth As DataTable
        Dim dtClassAuth As DataTable
        Dim dtSubClassAuth As DataTable
        Dim dtAllClassAuth As DataTable
        'Dim dtAuthItem As DataTable

        dtUsrAuth = objAdmin.GetUsrAuthority(UsrId)
        dtClassAuth = objAdmin.GetClassAuthority(UsrId)
        dtSubClassAuth = objAdmin.GetSubClassAuthority(UsrId)
        dtAllClassAuth = objAdmin.GetAllClassAuthority(UsrId)
        'dtAuthItem = objAdmin.GetAuthorityItem

        dtUsrAuth.TableName = c_UsrAuthorityTableName
        dtClassAuth.TableName = c_ClassAuthorityTableName
        dtSubClassAuth.TableName = c_SubClassAuthorityTableName
        dtAllClassAuth.TableName = c_AllClassAuthorityTableName
        'dtAuthItem.TableName = c_AuthorityItemTableName
        ds.Tables.Add(dtUsrAuth)
        ds.Tables.Add(dtClassAuth)
        ds.Tables.Add(dtSubClassAuth)
        ds.Tables.Add(dtAllClassAuth)
        'ds.Tables.Add(dtAuthItem)

        Return ds
    End Function

    Public Function ListUsrAuthority(ByVal UsrId As String) As System.Data.DataTable Implements ICsol.ListUsrAuthority
        Dim objAdmin As New Admin
        Dim dtUsrAuth As DataTable

        dtUsrAuth = objAdmin.GetUsrAuthority(UsrId)

        Return dtUsrAuth
    End Function

    Public Overloads Sub UpdateUsrAuthority(ByVal id As String, ByVal items As ArrayList) Implements ICsol.UpdateUsrAuthority
        Dim obj As New Admin

        obj.DelUsrAuth(id)
        obj.AddAllClassAuth(id)
        For index As Integer = 0 To items.Count - 1
            obj.AddUsrAuth(id, items(index), 0)
        Next

    End Sub

    Public Overloads Sub UpdateUsrAuthority(ByVal id As String, ByVal items As ArrayList, _
                           ByVal ids As ArrayList, ByVal ids2 As ArrayList) Implements ICsol.UpdateUsrAuthority
        Dim obj As New Admin

        obj.DelUsrAuth(id)
        For index As Integer = 0 To ids.Count - 1
            obj.AddClassAuth(id, ids(index))
        Next
        For index As Integer = 0 To ids2.Count - 1
            obj.AddSubClassAuth(id, ids2(index))
        Next
        For index As Integer = 0 To items.Count - 1
            obj.AddUsrAuth(id, items(index), 0)
        Next
    End Sub

    Public Function GetReceiptPrint() As System.Data.DataTable Implements ICsol.GetReceiptPrint
        Dim objAdmin As New Admin
        Dim dt As DataTable
        dt = objAdmin.GetReceiptPrint()
        Return dt
    End Function

    Public Function AddClass(ByVal className As String, ByVal srtDate As Date, _
                             ByVal endDate As Date, ByVal fee As Integer, _
                             ByVal usrId As String) _
                             As Integer Implements ICsol.AddClass
        Dim objClass As New ClassInfo
        Return objClass.AddClass(className, srtDate, endDate, fee, usrId)
    End Function

    Public Sub AddClassContent(ByVal classId As Integer, ByVal content As String, _
                               ByVal srtDate As Date, ByVal endDate As Date, _
                               ByVal subClassIDs As ArrayList) Implements ICsol.AddClassContent
        Dim cId As Integer = 0
        Dim obj As New ClassInfo
        cId = obj.AddContent(content, classId, srtDate, endDate)
        For index As Integer = 0 To subClassIDs.Count - 1
            obj.AddSubClassCont(cId, subClassIDs(index))
        Next

    End Sub
    Function GetRepeatContent(ByVal subClassID As ArrayList, ByVal strDate As Date, ByVal endDate As Date) As Boolean Implements ICsol.GetRepeatContent
        Dim obj As New ClassInfo

        For i As Integer = 0 To subClassID.Count - 1
            Dim flag As Boolean = obj.GetRepeatContent(subClassID(i), strDate, endDate)
            If flag = False Then
                Return False
                Exit Function
            End If
        Next
        Return True
    End Function

    Public Sub ModifyScSequences(ByVal id As ArrayList, ByVal Sequence As ArrayList) Implements ICsol.ModifyScSequences
        Dim obj As New ClassInfo
        For index As Integer = 0 To id.Count - 1
            obj.UpdScSequence(id(index), Sequence(index))
        Next
    End Sub

    Public Sub AddContVideo(ByVal cId As Integer, ByVal lstId As ArrayList, _
                                 ByVal lstPath As ArrayList) Implements ICsol.AddContVideo
        Dim objClass As New ClassInfo
        For index As Integer = 0 To lstId.Count - 1
            objClass.AddContVideo(cId, lstId(index), lstPath(index))
        Next
    End Sub

    Public Function AddClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                 ByVal Name As String, ByVal DisStyle As Integer, _
                                 ByVal lstPw As ArrayList, ByVal lstNaCol As ArrayList, _
                                 ByVal lstNaRow As ArrayList, _
                                 ByVal lstConvex As ArrayList) As Integer Implements ICsol.AddClassRoom
        Dim objClass As New ClassInfo
        Dim intCrId As Integer = -1
        intCrId = objClass.AddClassRoom(LineCnt, ColumnCnt, Name, DisStyle)
        If intCrId > -1 Then
            For index As Integer = 0 To lstPw.Count - 1
                objClass.AddSeatNa(lstPw(index), 0, intCrId, c_SeatNaStatePw)
            Next

            For index As Integer = 0 To lstConvex.Count - 1
                objClass.AddSeatNa(0, lstConvex(index), intCrId, c_SeatNaStateConvex)
            Next

            For index As Integer = 0 To lstNaCol.Count - 1
                objClass.AddSeatNa(lstNaCol(index), lstNaRow(index), intCrId, c_SeatNaStateNotseat)
            Next
        End If

    End Function

    Public Function AddClassSbj(ByVal subject As String) As Integer Implements ICsol.AddClassSbj

    End Function

    Public Function AddClassSession(ByVal SubClassID As Integer, _
                      ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                      ByVal dtEnd As Date) As Integer Implements ICsol.AddClassSession
        Dim obj As New ClassInfo
        obj.AddSession(SubClassID, DayOfWeek, dtStart, dtEnd)
    End Function

    Public Function AddDiscountDict(ByVal amount As Integer, ByVal reason As String, ByVal typeId As Integer) As Integer Implements ICsol.AddDiscountDict

    End Function

    Public Function AddListReceiptNoteDict(ByVal reason As String) As Integer Implements ICsol.AddListReceiptNoteDict

    End Function

    Public Function AddSchool(ByVal Name As String, ByVal TypeID As Integer, _
                              ByVal Phone As String, ByVal Address As String, _
                              ByVal PersonContact As String) As Integer Implements ICsol.AddSchool
        Dim objClass As New ClassInfo
        Return objClass.AddSchool(Name, TypeID, Phone, Address, PersonContact)
    End Function

    Public Sub AddSeatNA(ByVal seatList As System.Collections.ArrayList) Implements ICsol.AddSeatNA

    End Sub

    Public Function GetStuRegInfo() As System.Data.DataSet Implements ICsol.GetStuRegInfo
        Dim objDb1 As New ClassInfo
        Dim objDb2 As New StuInfo
        Dim ds As New DataSet(c_StuRegInfoDataSetName)

        Dim dt1 As DataTable = objDb1.GetSchoolList()
        dt1.TableName = c_SchoolListTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = objDb1.GetSchoolGrpList()
        dt2.TableName = c_SchoolGrpListTableName
        ds.Tables.Add(dt2)

        Dim dt3 As DataTable = objDb2.GetSibTypeList()
        dt3.TableName = c_SibTypeListTableName
        ds.Tables.Add(dt3)

        Dim dt4 As DataTable = objDb1.GetSchoolTypes()
        dt4.TableName = c_SchoolTypesTableName
        ds.Tables.Add(dt4)

        Return ds
    End Function

    Public Function ListSchGrp() As DataTable Implements ICsol.ListSchGrp
        Dim objDb1 As New ClassInfo
        Dim dt2 As DataTable = objDb1.GetSchoolGrpList()
        Return dt2
    End Function

    Public Function ListSchType() As DataTable Implements ICsol.ListSchType
        Dim objDb1 As New ClassInfo
        Dim dt2 As DataTable = objDb1.GetSchoolTypes()
        Return dt2
    End Function

    Public Function ListSibType() As DataTable Implements ICsol.ListSibType
        Dim objDb1 As New StuInfo
        Dim dt2 As DataTable = objDb1.GetSibTypeList()
        Return dt2
    End Function

    Public Function GetStuRecByCardNum(ByVal strCard As String, ByVal ClassID As Integer) As DataTable Implements ICsol.GetStuRecByCardNum
        Dim obj As New StuInfo
        Dim result As New DataTable
        If strCard.Trim.Length > 1 Then
            Return obj.GetStuRecByCardNum(strCard, ClassID)
        Else
            Return result
        End If
    End Function
    Public Function GetStuRecByCardNum2(ByVal strCard As String, ByVal ClassID As Integer) As DataTable Implements ICsol.GetStuRecByCardNumAbsoluteEqual
        Dim obj As New StuInfo
        Dim result As New DataTable
        If strCard.Trim.Length > 1 Then
            Return obj.GetStuRecByCardNumAbsoluteEqual(strCard, ClassID)
        Else
            Return result
        End If
    End Function
    Public Function GetStuRecByID(ByVal strID As String) As DataTable Implements ICsol.GetStuRecByID
        Dim obj As New StuInfo
        Dim result As New DataTable
        If strID.Trim.Length > 1 Then
            Return obj.GetStuRecById(strID)
        Else
            Return result
        End If

    End Function
    Public Function GetStuRecByID(ByVal strID As String, ByVal intClass As Integer) As DataTable Implements ICsol.GetStuRecByID
        Dim obj As New StuInfo
        Dim result As New DataTable
        If strID.Trim.Length > 1 Then
            Return obj.GetStuRecByID2(strID, intClass)
        Else
            Return result
        End If



    End Function
    Public Function GetBookList(ByVal lstBook As ArrayList) As DataTable Implements ICsol.GetBookList
        Dim obj As New StuInfo
        Dim dtBookList As New DataTable
        For i As Integer = 0 To lstBook.Count - 1
            dtBookList.Merge(obj.GetBookList(lstBook(i)))
        Next
        Return dtBookList
    End Function

    ' ClassPunchStuinfo
    Public Function GetClassPunchBook(ByVal dayOfWeek As Integer) As DataTable Implements ICsol.GetClassPunchBook
        Dim obj As New StuInfo
        Return obj.GetClassPunchBook(dayOfWeek)
    End Function
    Public Function GetClassPunchClass(ByVal dayOfWeek As Integer) As DataTable Implements ICsol.GetClassPunchClass
        Dim obj As New StuInfo
        Return obj.GetClassPunchClass(dayOfWeek)
    End Function
    Public Function GetClassPunchSubClass(ByVal dayOfWeek As Integer) As DataTable Implements ICsol.GetClassPunchSubClass
        Dim obj As New StuInfo
        Return obj.GetClassPunchSubClass(dayOfWeek)
    End Function
    Public Function GetClassPunchContent(ByVal dayOfWeek As Integer) As DataTable Implements ICsol.GetClassPunchContent
        Dim obj As New StuInfo
        Return obj.GetClassPunchContent(dayOfWeek)
    End Function
    Public Function GetClassPunchSession(ByVal dayOfWeek As Integer) As DataTable Implements ICsol.GetClassPunchSession
        Dim obj As New StuInfo
        Return obj.GetClassPunchSession(dayOfWeek)
    End Function


    Public Function GetSubClassList() As DataTable Implements ICsol.GetSubClassList
        Dim obj As New ClassInfo
        Return obj.GetSubClassList
    End Function
    Public Function GetClassList() As DataTable Implements ICsol.GetClassList
        Dim obj As New ClassInfo
        Return obj.GetClassList
    End Function
    Public Function GetSubClassID(ByVal ClassID As Integer) As DataTable Implements ICsol.GetSubClassID
        Dim obj As New ClassInfo
        Return obj.GetSubClassID(ClassID)
    End Function

    Public Function ListUserNameList() As DataTable Implements ICsol.ListUserNameList
        Dim obj As New Admin
        Return obj.GetUserNameList
    End Function

    Public Function GetClassRegInfo() As System.Data.DataSet Implements ICsol.GetClassRegInfo
        Dim objDb1 As New ClassInfo
        Dim objDb2 As New Admin
        Dim ds As New DataSet(c_ClassRegInfoDataSetName)

        Dim dt1 As DataTable = objDb1.GetClassList()
        dt1.TableName = c_ClassListDataTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = objDb1.GetSubClassList()
        dt2.TableName = c_SubClassListDataTableName
        ds.Tables.Add(dt2)

        Dim dt3 As DataTable = objDb2.GetSalesList()
        dt3.TableName = c_SalesListDataTableName
        ds.Tables.Add(dt3)

        Dim dt4 As DataTable = objDb1.GetClassRooms()
        dt4.TableName = c_ClassRoomListTableName
        ds.Tables.Add(dt4)

        Dim dt5 As DataTable = objDb1.GetSubjects()
        dt5.TableName = c_SubjectsTableName
        ds.Tables.Add(dt5)

        Return ds
    End Function

    Public Function GetSeatMap(ByVal scId As Integer) As DataTable Implements ICsol.GetSeatMap
        Dim obj As New ClassInfo
        Return obj.GetSeatMapByScId(scId)
    End Function

    Public Function ListSubjects() As DataTable Implements ICsol.ListSubjects
        Dim obj As New ClassInfo
        Return obj.GetSubjects
    End Function

    Public Function ListStuBySch(ByVal schId As Integer, ByVal schType As Integer) As DataTable Implements ICsol.ListStuBySch
        Dim obj As New StuInfo
        Return obj.GetStuBySch(schId, schType)
    End Function

    Public Overloads Function ListStuBySubClass(ByVal id As Integer) As DataTable Implements ICsol.ListStuBySubClass
        Dim obj As New StuInfo
        Return obj.GetStuBySubClass(id)
    End Function

    Public Function GetNameRepeatStu() As DataTable Implements ICsol.GetNameRepeatStu
        Dim obj As New StuInfo
        Return obj.GetNameRepeatStu
    End Function

    Public Overloads Function ListStuBySubClass(ByVal id As ArrayList) As DataTable Implements ICsol.ListStuBySubClass
        Dim obj As New StuInfo
        Dim dt As New DataTable

        If id.Count > 1 Then
            'dt = obj.GetStuBySubClass(id(0))
            'For index As Integer = 1 To id.Count - 1
            '    dt.Merge(obj.GetStuBySubClass(id(index)))
            'Next
            dt = DataBaseAccess.StuInfo.GetStuBySubClass(id)
        ElseIf id.Count = 1 Then
            dt = obj.GetStuBySubClass(id(0))
        Else
            dt = obj.GetStuBySubClass(-1)
        End If
        Return dt
    End Function

    Public Function ListStuByClass(ByVal id As Integer) As DataTable Implements ICsol.ListStuByClass
        Dim obj As New StuInfo
        Return obj.GetStuByClass(id)
    End Function

    Public Function ListStuByClass(ByVal id As ArrayList) As DataTable Implements ICsol.ListStuByClass
        Dim obj As New StuInfo
        Return obj.GetStuByClass(id)
    End Function

    Public Function GetSeatRegInfo(ByVal scId As Integer) As System.Data.DataSet Implements ICsol.GetSeatRegInfo
        Dim objDb1 As New ClassInfo
        Dim ds As New DataSet(c_SeatRegInfoDataSetName)
        Dim intCrId As Integer = -1

        Dim dt1 As DataTable = objDb1.GetClassRoomByScId(scId)
        dt1.TableName = c_ClassRoomInfoTableName
        ds.Tables.Add(dt1)
        If dt1.Rows.Count > 0 Then
            intCrId = dt1.Rows(0).Item(c_IDColumnName)
        End If

        Dim dt2 As DataTable = objDb1.GetSeatNaListByCrId(intCrId)
        dt2.TableName = c_SeatNaListTableName
        ds.Tables.Add(dt2)

        Dim dt3 As DataTable = objDb1.GetSeatTkListByScId(scId)
        dt3.TableName = c_SeatTakenListTableName
        ds.Tables.Add(dt3)

        Return ds
    End Function
    Public Function GetSeatTakenList(ByVal scid As Integer) As DataTable Implements ICsol.GetSeatTakenList
        Dim objDb1 As New ClassInfo


        Dim dt As DataTable = objDb1.GetSeatTkListByScId(scid)

        Return dt

    End Function

    Function GetStuNameByID(ByVal id As String) As String Implements ICsol.GetStuNameByID
        Dim s As String = ""
        Dim obj As New StuInfo
        s = obj.GetStuNameById(id)
        Return s

    End Function

    Public Function GetNextStu(ByVal id As String) As String Implements ICsol.GetNextStu
        Dim id2 As String = ""
        Dim dt As New DataTable
        Dim obj As New StuInfo
        Dim c As Integer = 0
        id2 = CInt(id).ToString

        Do Until dt.Rows.Count > 0 Or c = 10
            id2 = (CInt(id2) + 1).ToString
            If id2.Length = 7 Then
                id2 = "0" & id2
            End If
            dt = obj.GetStuRecById(id2)
            c = c + 1
        Loop
        If dt.Rows.Count > 0 Then
            Return id2
        Else
            Return ""
        End If
    End Function

    Function GetTeleInterview(ByVal cid As Integer, ByVal action As Byte) As DataTable Implements ICsol.GetTeleInterview
        Dim obj As New StuInfo

        Select Case action
            Case c_SearchTelInterviewAll
                Return obj.GetTeleInterviewAll
            Case c_SearchTelInterviewClass
                Return obj.GetTeleInterviewClass(cid)
            Case c_SearchTelInterviewSc
                Return obj.GetTeleInterviewSc(cid)
            Case Else
                Return obj.GetTeleInterviewAll
        End Select

    End Function

    Function GetTeleInterviewStu(ByVal StuID As String) As DataTable Implements ICsol.GetTeleInterviewStu
        Dim obj As New StuInfo
        Return obj.GetTeleInterviewStu(StuID)

    End Function

    Sub AddTeleInterviewType(ByVal t As String) Implements ICsol.AddTeleInterviewType
        Dim obj As New StuInfo
        obj.AddTeleInterviewType(t)
    End Sub

    Public Sub DeleteTeleInterview(ByVal id As Integer) Implements ICsol.DeleteTeleInterview
        Dim obj As New StuInfo
        obj.DeleteTeleInterview(id)
    End Sub

    Function GetTeleInterviewDate(ByVal cid As Integer, ByVal datef As Date, _
                                  ByVal datet As Date, ByVal action As Byte) As DataTable Implements ICsol.GetTeleInterviewDate
        Dim obj As New StuInfo

        Select Case action
            Case c_SearchTelInterviewAll
                Return obj.GetTeleInterviewAllDate(datef, datet)
            Case c_SearchTelInterviewClass
                Return obj.GetTeleInterviewClassDate(cid, datef, datet)
            Case c_SearchTelInterviewSc
                Return obj.GetTeleInterviewScDate(cid, datef, datet)
            Case Else
                Return obj.GetTeleInterviewAllDate(datef, datet)
        End Select
    End Function

    Public Function GetPreviousStu(ByVal id As String) As String Implements ICsol.GetPreviousStu
        Dim id2 As String = ""
        Dim dt As New DataTable
        Dim obj As New StuInfo
        Dim c As Integer = 0
        id2 = CInt(id).ToString
        Do Until dt.Rows.Count > 0 Or c = 10
            id2 = (CInt(id2) - 1).ToString
            If id2.Length = 7 Then
                id2 = "0" & id2
            End If
            dt = obj.GetStuRecById(id2)
            c = c + 1
        Loop
        If dt.Rows.Count > 0 Then
            Return id2
        Else
            Return ""
        End If
    End Function

    Public Function GetSeatNaList(ByVal crId As Integer) As DataTable Implements ICsol.GetSeatNaList
        Dim objDb1 As New ClassInfo
        Return objDb1.GetSeatNaListByCrId(crId)
    End Function

    Public Function GetClassRegByStuId(ByVal stuId As String) As DataTable Implements ICsol.GetClassRegByStuId
        Dim obj As New StuInfo
        Return obj.GetClassRegByStuId(stuId)

    End Function

    Function GetClassRate(ByVal SubClassID As Integer, ByVal ContentID As Integer, _
                          ByVal dateStart As Date, ByVal dateEnd As Date) As Array Implements ICsol.GetClassRate
        Dim r As String() = {"0", "0", "0%"}
        Dim obj As New ClassInfo
        Dim dt As DataTable = obj.GetSubClassRegCnt(SubClassID)
        Dim regCnt As Integer = 0
        If dt.Rows.Count > 0 Then
            regCnt = dt.Rows(0).Item(c_StuRegCntColumnName)
        End If
        Dim lstSession As New ArrayList
        For index As Integer = 0 To dt.Rows.Count - 1
            If dt.Rows(index).Item(c_DayOfWeekColumnName) <> Nothing Then
                lstSession.Add(dt.Rows(index).Item(c_DayOfWeekColumnName))
            End If
        Next
        Dim sessionCnt As Integer = 0
        For index As Integer = 0 To lstSession.Count - 1
            sessionCnt = sessionCnt + obj.GetSessionCnt(dateStart, dateEnd, lstSession(index))
        Next
        Dim expected As Integer = 0

        Dim actual As Integer = 0
        If ContentID > -1 Then
            actual = obj.GetClassShowupCnt(SubClassID, ContentID, _
                        dateStart, dateEnd)
            expected = regCnt * sessionCnt
        Else
            actual = obj.GetClassShowupCntNoCont(SubClassID, _
                        dateStart, dateEnd)
            expected = regCnt
        End If
        If expected > 0 Then
            Dim s2 As String = CSng(actual / expected * 100).ToString
            If s2.Length > 6 Then
                s2 = s2.Substring(0, 6)
            End If
            r(0) = expected.ToString
            r(1) = actual.ToString
            r(2) = s2 & "%"
        End If
        Return r
    End Function

    Public Sub ChangeSeatNum(ByVal ID As Integer, ByVal subClassID As Integer, _
                       ByVal newSeat As String, _
                       ByVal oldSeat As String, ByVal stuID As String) Implements ICsol.ChangeSeatNum
        Dim obj As New ClassInfo
        Dim obj2 As New StuInfo
        newSeat = newSeat.Trim
        oldSeat = oldSeat.Trim
        If newSeat <> "" And oldSeat = "" Then
            obj2.AddSeatTk(stuID, subClassID, newSeat)
        ElseIf newSeat = "" And oldSeat <> "" Then
            obj2.DeleteSeatTk(stuID, subClassID, oldSeat)
        End If
        obj.ChangeSeatNum(ID, newSeat.Trim)
    End Sub

    Public Function DisplayStudentInfo(ByVal ID As String) As System.Data.DataSet Implements ICsol.DisplayStudentInfo
        Dim objDb As New StuInfo
        Dim ds As New DataSet(c_StuInfoDataSetName)

        Dim dt1 As DataTable = objDb.GetClassRegByStuId(ID)
        dt1.TableName = c_StuClassTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = objDb.GetStuRecById(ID)
        dt2.TableName = c_StuInfoTableName
        ds.Tables.Add(dt2)

        Dim dt3 As DataTable = objDb.GetPayRecByStuId(ID)
        dt3.TableName = c_StuPayRecTableName
        ds.Tables.Add(dt3)

        Dim dt4 As DataTable = objDb.GetSibByStuId(ID)
        dt4.TableName = c_StuSibTableName
        ds.Tables.Add(dt4)

        Dim dt5 As DataTable = objDb.GetDiscByStuId(ID)
        dt5.TableName = c_StuDiscTableName
        ds.Tables.Add(dt5)

        Return ds
    End Function

    Public Function ListPayRecByStuId(ByVal stuId As String) As DataTable Implements ICsol.ListPayRecByStuId
        Dim obj As New StuInfo
        Return obj.GetPayRecByStuId(stuId)
    End Function

    Public Function ListKBByStuId(ByVal stuId As String) As DataTable Implements ICsol.ListKBByStuId
        Dim obj As New StuInfo
        Return obj.GetKBByStuId(stuId)
    End Function

    Public Overloads Function AddClassReg(ByVal StuID As String, ByVal SubClassID As Integer, _
                                ByVal SeatNum As String, ByVal Cancel As Byte, _
                                ByVal FeeOwe As Integer, ByVal ReceiptRemarks As String, _
                                ByVal SalesID As String) As Integer Implements ICsol.AddClassReg
        Dim obj As New StuInfo
        obj.UpdStuOwe(StuID, FeeOwe)
        SeatNum = SeatNum.Trim
        If Not SeatNum = "" Then
            obj.AddSeatTk(StuID, SubClassID, SeatNum)
        End If
        Return obj.AddClassReg(StuID, SubClassID, _
                                SeatNum, Cancel, _
                                FeeOwe, ReceiptRemarks, _
                                Now, SalesID)
    End Function

    Public Overloads Function AddClassReg(ByVal StuID As String, ByVal SubClassID As ArrayList, _
                                ByVal SeatNum As String, ByVal Cancel As Byte, _
                                ByVal FeeOwe As ArrayList, ByVal ReceiptRemarks As String, _
                                ByVal SalesID As String) As Integer Implements ICsol.AddClassReg
        Dim obj As New StuInfo
        SeatNum = SeatNum.Trim
        For index As Integer = 0 To SubClassID.Count - 1
            obj.UpdStuOwe(StuID, FeeOwe(index))
            If Not SeatNum = "" Then
                obj.AddSeatTk(StuID, SubClassID(index), SeatNum)
            End If
            obj.AddClassReg(StuID, SubClassID(index), _
                                SeatNum, Cancel, _
                                FeeOwe(index), ReceiptRemarks, _
                                Now, SalesID)
        Next
        Return -1
    End Function

    Function CountStu(ByVal strYear As String) As Integer Implements ICsol.CountStu
        Dim objDb As New StuInfo
        Dim strStuId As String = ""
        Dim intCount As Integer
        Do While strYear.Length < 3
            strYear = "0" & strYear
        Loop
        Dim dt As DataTable = objDb.GetStuIdListByYear(strYear)
        intCount = dt.Compute("COUNT(" & c_IDColumnName & ")", "")

        Return intCount
    End Function

    Function ImportStu(ByVal dtStu As DataTable, ByVal stuType As Byte, _
                       ByVal replace As Boolean) As ArrayList Implements ICsol.ImportStu
        Dim dt As New DataTable
        Dim lstResult As New ArrayList
        Dim objDb As New StuInfo
        Dim idx As Integer = 0

        If dtStu.Rows.Count > 0 Then
            For Each row As DataRow In dtStu.Rows
                dt = objDb.GetStuRecById(row.Item(c_IDColumnName))
                If dt.Rows.Count > 0 Then
                    If replace Then
                        objDb.UpdStudent(GetstringValue(row.Item(c_IDColumnName)), GetstringValue(row.Item(c_NameColumnName)), _
                                 GetstringValue(row.Item(c_EngNameColumnName)), GetstringValue(row.Item(c_SexColumnName)), _
                                 GetDateValue(row.Item(c_BirthdayColumnName)), GetstringValue(row.Item(c_ICColumnName)), _
                                  GetstringValue(row.Item(c_AddressColumnName)), _
                                   GetstringValue(row.Item(c_PostalCodeColumnName)), "", _
                                  "", GetstringValue(row.Item(c_Tel1ColumnName)), _
                                  "", GetstringValue(row.Item(c_OfficeTelColumnName)), _
                                  GetstringValue(row.Item(c_MobileColumnName)), GetstringValue(row.Item(c_EmailColumnName)), _
                                   stuType, _
                                  "", GetstringValue(row.Item(c_SchoolClassColumnName)), _
                                   GetIntValue(row.Item(c_SchoolGradeColumnName)), GetIntValue(row.Item(c_PrimarySchColumnName)), _
                                 GetIntValue(row.Item(c_JuniorSchColumnName)), GetIntValue(row.Item(c_HighSchColumnName)), _
                                 GetIntValue(row.Item(c_UniversityColumnName)), 0, _
                                 GetIntValue(row.Item(c_SchGroupColumnName)), GetstringValue(row.Item(c_GraduateFromColumnName)), _
                                 GetstringValue(row.Item(c_DadNameColumnName)), GetstringValue(row.Item(c_MumNameColumnName)), _
                                   GetstringValue(row.Item(c_DadTitleColumnName)), GetstringValue(row.Item(c_MumTitleColumnName)), _
                                  GetstringValue(row.Item(c_DadMobileColumnName)), GetstringValue(row.Item(c_MumMobileColumnName)), _
                                   "", GetstringValue(row.Item(c_IntroNameColumnName)), _
                                   GetstringValue(row.Item(c_RemarksColumnName)), GetstringValue(row.Item(c_Cust1ColumnName)), _
                                 GetstringValue(row.Item(c_Cust2ColumnName)), "", _
                                  "", "", "", "", 0)
                    End If
                    lstResult.Add(idx)
                Else
                    objDb.AddStudent(GetstringValue(row.Item(c_IDColumnName)), GetstringValue(row.Item(c_NameColumnName)), _
                                 GetstringValue(row.Item(c_EngNameColumnName)), GetstringValue(row.Item(c_SexColumnName)), _
                                 GetDateValue(row.Item(c_BirthdayColumnName)), GetstringValue(row.Item(c_ICColumnName)), _
                                 "", GetstringValue(row.Item(c_AddressColumnName)), _
                                  GetstringValue(row.Item(c_PostalCodeColumnName)), "", _
                                  "", GetstringValue(row.Item(c_Tel1ColumnName)), _
                                  "", GetstringValue(row.Item(c_OfficeTelColumnName)), _
                                  GetstringValue(row.Item(c_MobileColumnName)), GetstringValue(row.Item(c_EmailColumnName)), _
                                  "", stuType, _
                                  "", GetstringValue(row.Item(c_SchoolClassColumnName)), _
                                   GetIntValue(row.Item(c_SchoolGradeColumnName)), GetIntValue(row.Item(c_PrimarySchColumnName)), _
                                 GetIntValue(row.Item(c_JuniorSchColumnName)), GetIntValue(row.Item(c_HighSchColumnName)), _
                                 GetIntValue(row.Item(c_UniversityColumnName)), 0, _
                                 GetIntValue(row.Item(c_SchGroupColumnName)), GetstringValue(row.Item(c_GraduateFromColumnName)), _
                                 GetstringValue(row.Item(c_DadNameColumnName)), GetstringValue(row.Item(c_MumNameColumnName)), _
                                  GetstringValue(row.Item(c_DadTitleColumnName)), GetstringValue(row.Item(c_MumTitleColumnName)), _
                                  GetstringValue(row.Item(c_DadMobileColumnName)), GetstringValue(row.Item(c_MumMobileColumnName)), _
                                  "", GetstringValue(row.Item(c_IntroNameColumnName)), _
                                  Now, 0, _
                                 GetstringValue(row.Item(c_RemarksColumnName)), GetstringValue(row.Item(c_Cust1ColumnName)), _
                                 GetstringValue(row.Item(c_Cust2ColumnName)), "", _
                                  "", "", "", "", 0)
                End If
                idx = idx + 1
            Next
        End If
        Return lstResult
    End Function

    Function GetIntValue(ByVal s As Object) As Integer Implements ICsol.GetIntValue
        Dim i As Integer = 0
        If Not TypeOf s Is DBNull Then
            If Not s.ToString.Trim = "" Then
                If IsNumeric(s.ToString.Trim) Then
                    i = CInt(s.ToString.Trim)
                End If
            End If
        End If

        Return i
    End Function

    Function GetstringValue(ByVal s As Object) As String Implements ICsol.GetstringValue
        Dim s1 As String = ""
        If Not TypeOf s Is DBNull Then
            s1 = s.ToString.Trim
        End If
        Return s1
    End Function

    Function GetByteValue(ByVal s As Object) As Byte Implements ICsol.GetByteValue
        Dim i As Byte = 0
        If Not TypeOf s Is DBNull Then
            If Not s.ToString.Trim = "" Then
                If IsNumeric(s.ToString.Trim) Then
                    i = CByte(s.ToString.Trim)
                End If
            End If
        End If

        Return i
    End Function

    Function GetDateValue(ByVal s As Object) As Date Implements ICsol.GetDateValue
        Dim i As Date = Now
        If Not TypeOf s Is DBNull Then
            If Not s.ToString.Trim = "" Then
                i = CDate(s.ToString.Trim)
            End If
        End If

        Return i
    End Function

    Public Function GenStuId(ByVal strYear As String) As String Implements ICsol.GenStuId
        Dim objDb As New StuInfo
        Dim strStuId As String = ""
        Dim strId As String
        Dim intCount As Integer
        Dim strCount As String
        Do While strYear.Length < 3
            strYear = "0" & strYear
        Loop
        Dim dt As DataTable = objDb.GetStuIdListByYear(strYear)
        intCount = dt.Compute("COUNT(" & c_IDColumnName & ")", "")
        Do While strStuId = ""
            strCount = (intCount + 1).ToString
            Do While strCount.Length < 5
                strCount = "0" & strCount
            Loop
            strId = strYear & strCount
            If dt.Compute("COUNT(" & c_IDColumnName & ")", _
                          c_IDColumnName & "='" & strId & "'") = 0 Then
                strStuId = strId
            Else
                intCount = intCount + 1
            End If
        Loop

        AddStudent(strStuId, "", "", "", _
              Now, "", "", "", _
              "", "", "", _
              "", "", "", "", _
              "", "", 0, "", _
               "", 0, 0, _
              0, 0, 0, _
              0, 0, "", _
              "", "", "", "", _
              "", "", "", _
              "", Now, 0, _
              "", "", "", _
              "", "", "", "", "", 0)

        Return strStuId
    End Function

    Public Function GenUsrId() As String Implements ICsol.GenUsrId
        Dim objDb As New Admin
        Dim strId As String = ""
        Dim intCount As Integer

        Dim dt As DataTable = GetUsrList()
        intCount = dt.Compute("COUNT(" & c_IDColumnName & ")", "")
        strId = intCount.ToString
        If strId.Length < 8 Then
            Do Until strId.Length = 8
                strId = "0" & strId
            Loop
        End If

        objDb.AddUser(strId, "", "12345", 0, _
               1, 1, "", "", _
              "", "", "", _
              "", "", "", "", _
              "", "", "", "", _
              Now, "", "", _
              Now, 0, 0, _
              0, 0, 0)
        Dim dtAuthGrp As DataTable = objDb.GetGroupAuth(1)
        Dim lst As New ArrayList
        If dtAuthGrp.Rows.Count > 0 Then

            If dtAuthGrp.Rows(0).Item(c_ViewAllClassColumnName) = 1 Then
                objDb.AddAllClassAuth(strId)
            End If
            If dtAuthGrp.Rows(0).Item(c_PartialClassColumnName) = 1 Then

            End If
            If dtAuthGrp.Rows(0).Item(c_AccntStatisColumnName) = 1 Then
                lst.Add(21)
                lst.Add(27)
                lst.Add(28)
                lst.Add(31)
            End If
            If dtAuthGrp.Rows(0).Item(c_AccntAmendColumnName) = 1 Then
                lst.Add(22)
                lst.Add(25)
            End If
            If dtAuthGrp.Rows(0).Item(c_AccntDeleteColumnName) = 1 Then
                lst.Add(23)
            End If
            If dtAuthGrp.Rows(0).Item(c_PersonalInfoColumnName) = 1 Then
                lst.Add(51)
            End If
            If dtAuthGrp.Rows(0).Item(c_DataEntrySaveColumnName) = 1 Then

            End If
            If dtAuthGrp.Rows(0).Item(c_PaymentRecColumnName) = 1 Then
                lst.Add(20)
            End If
            If dtAuthGrp.Rows(0).Item(c_ReceiptSuppleColumnName) = 1 Then
                lst.Add(19)
            End If
            If dtAuthGrp.Rows(0).Item(c_FeeOweStatisColumnName) = 1 Then
                lst.Add(24)
            End If
            If dtAuthGrp.Rows(0).Item(c_ClassPaymentColumnName) = 1 Then
                lst.Add(29)
            End If
            If dtAuthGrp.Rows(0).Item(c_ExportAbsentColumnName) = 1 Then
                lst.Add(0)
            End If
            If dtAuthGrp.Rows(0).Item(c_ExportShowupColumnName) = 1 Then
                lst.Add(1)
            End If
            If dtAuthGrp.Rows(0).Item(c_ByClassColumnName) = 1 Then
                lst.Add(75)
            End If
            If dtAuthGrp.Rows(0).Item(c_BySchoolColumnName) = 1 Then
                lst.Add(79)
            End If
            If dtAuthGrp.Rows(0).Item(c_AllStuInfoColumnName) = 1 Then
                lst.Add(57)
            End If
            If dtAuthGrp.Rows(0).Item(c_MassExportColumnName) = 1 Then
                lst.Add(16)
                lst.Add(17)
            End If
            If dtAuthGrp.Rows(0).Item(c_AccntExportColumnName) = 1 Then
                lst.Add(16)
            End If
            If dtAuthGrp.Rows(0).Item(c_AdvanceStuInfoColumnName) = 1 Then
                lst.Add(59)
                lst.Add(64)
            End If
            If dtAuthGrp.Rows(0).Item(c_ExportOthersColumnName) = 1 Then
                lst.Add(78)
                lst.Add(12)
                lst.Add(13)
                lst.Add(15)
            End If
            If dtAuthGrp.Rows(0).Item(c_DeleteBonusColumnName) = 1 Then

            End If
            If dtAuthGrp.Rows(0).Item(c_IndivSalesStatisColumnName) = 1 Then
                lst.Add(51)
            End If
            If dtAuthGrp.Rows(0).Item(c_AllSalesStatisColumnName) = 1 Then
                lst.Add(52)
            End If
            If dtAuthGrp.Rows(0).Item(c_AllHeadCntColumnName) = 1 Then
                lst.Add(54)
            End If
            If dtAuthGrp.Rows(0).Item(c_SmallExportOnlyColumnName) = 1 Then

            End If
            If dtAuthGrp.Rows(0).Item(c_RestrictClassChangeColumnName) = 1 Then

            End If
        End If
        lst.Add(2)
        lst.Add(6)
        lst.Add(54)
        lst.Add(61)
        lst.Add(67)
        lst.Add(69)
        lst.Add(70)
        lst.Add(74)
        lst.Add(80)
        lst.Add(81)
        lst.Add(82)
        For index As Integer = 0 To lst.Count - 1
            objDb.AddUsrAuth(strId, lst(index), 1)
        Next

        Return strId
    End Function

    'Public Function GenReceiptNum(ByVal dtDateFrom As Date, ByVal dtDateTo As Date) As String Implements ICsol.GenReceiptNum
    '    Dim objDb As New Accounting
    '    Dim intCount As Integer = 0
    '    Dim strBase As String
    '    Dim strM As String


    '    strBase = (dtDateFrom.Year - 1911).ToString
    '    If strBase.Length < 3 Then
    '        strBase = "0" & strBase
    '    End If
    '    strM = dtDateFrom.Month.ToString
    '    If strM.Length < 2 Then
    '        strM = "0" & strM
    '    End If
    '    strBase = strBase & strM
    '    strM = dtDateFrom.Day.ToString
    '    If strM.Length < 2 Then
    '        strM = "0" & strM
    '    End If
    '    strBase = strBase & strM

    '    Dim receiptNum As Int64 = 0
    '    Dim receiptNum1 As Int64 = 0
    '    Dim receiptNum2 As Int64 = 0
    '    Dim receiptNum3 As Int64 = 0

    '    Dim dt As DataTable = objDb.GetPayStaByDate(dtDateFrom, dtDateTo)
    '    If dt.Rows.Count > 0 Then
    '        If Not DBNull.Value.Equals(dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")) Then
    '            receiptNum = dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")
    '        End If
    '    End If

    '    dt = objDb.GetMBackStaByDate(dtDateFrom, dtDateTo)
    '    If dt.Rows.Count > 0 Then
    '        If Not DBNull.Value.Equals(dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")) Then
    '            receiptNum1 = dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")
    '        End If
    '    End If

    '    dt = objDb.GetMKeepStaByDate(dtDateFrom, dtDateTo)
    '    If dt.Rows.Count > 0 Then
    '        If Not DBNull.Value.Equals(dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")) Then
    '            receiptNum2 = dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")
    '        End If
    '    End If

    '    dt = objDb.GetPayRecModify(strBase)
    '    If dt.Rows.Count > 0 Then
    '        If Not DBNull.Value.Equals(dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")) Then
    '            receiptNum3 = dt.Compute("Max(" & c_ReceiptNumColumnName & ")", c_ReceiptNumColumnName & " LIKE '" & strBase & "%'")
    '        End If
    '    End If


    '    receiptNum = Math.Max(receiptNum, receiptNum1)
    '    receiptNum = Math.Max(receiptNum, receiptNum2)
    '    receiptNum = Math.Max(receiptNum, receiptNum3)

    '    strM = (receiptNum + 1).ToString.Insert(0, "0")

    '    If strM = "01" Then
    '        strM = strBase + "0001"
    '    End If


    'Return strM

    'End Function

    Function GenReceiptNum(ByVal receiptDate As Date) As String Implements ICsol.GenReceiptNum

        Dim result As String = ""
        Dim objDb As New Accounting
        Dim year As String = (receiptDate.Year - 1911).ToString("d3")
        Dim datestr As String = year & receiptDate.ToString("MMdd")

        Dim receiptNum As Int64 = 0
        Dim receiptNum1 As Int64 = 0
        Dim receiptNum2 As Int64 = 0
        Dim receiptNum3 As Int64 = 0
        Dim receiptStr As String = ""
        'Dim time As String = receiptDate.ToString("MM/dd/yyyy hh:mm:ss.ffff")

        Dim dt As DataTable = DataBaseAccess.Accounting.GetPayStaByDate(receiptDate, receiptDate)
        Dim rows() As DataRow = dt.Select("", String.Format("{0} DESC", c_ReceiptNumColumnName))
        If rows.Count > 0 Then
            If Not DBNull.Value.Equals(rows(0).Item(c_ReceiptNumColumnName)) Then
                receiptNum = rows(0).Item(c_ReceiptNumColumnName)
            End If
        End If


        dt = DataBaseAccess.Accounting.GetMBackStaByDate(receiptDate, receiptDate)
        rows = dt.Select("", String.Format("{0} DESC", c_ReceiptNumColumnName))
        If rows.Count > 0 Then
            If Not DBNull.Value.Equals(rows(0).Item(c_ReceiptNumColumnName)) Then
                receiptNum1 = rows(0).Item(c_ReceiptNumColumnName)
            End If
        End If

        dt = DataBaseAccess.Accounting.GetMKeepStaByDate(receiptDate, receiptDate)
        rows = dt.Select("", String.Format("{0} DESC", c_ReceiptNumColumnName))
        If rows.Count > 0 Then
            If Not DBNull.Value.Equals(rows(0).Item(c_ReceiptNumColumnName)) Then
                receiptNum2 = rows(0).Item(c_ReceiptNumColumnName)
            End If
        End If

        dt = DataBaseAccess.Accounting.GetPayRecModify(datestr)
        rows = dt.Select("", String.Format("{0} DESC", c_ReceiptNumColumnName))
        If rows.Count > 0 Then
            If Not DBNull.Value.Equals(rows(0).Item(c_ReceiptNumColumnName)) Then
                receiptNum3 = rows(0).Item(c_ReceiptNumColumnName)
            End If
        End If

        receiptNum = Math.Max(receiptNum, receiptNum1)
        receiptNum = Math.Max(receiptNum, receiptNum2)
        receiptNum = Math.Max(receiptNum, receiptNum3)

        Dim nowtime As Date = Now

        If receiptNum = 0 Then
            receiptStr = "0001"
        Else
            'receiptStr = (receiptNum + 1).ToString.Substring(receiptNum.ToString.Length - 4, 4)
            result = (receiptNum + 1).ToString
            Return result
        End If

        result = datestr & receiptStr
        Return result
    End Function


    Function GetInitData(ByVal id As String) As DataSet Implements ICsol.GetInitData
        Dim ds As New DataSet
        Dim objAdmin As New Admin
        Dim objClass As New ClassInfo
        Dim objStu As New StuInfo
        Dim dtUsrAuth As DataTable
        Dim dtClassAuth As DataTable
        Dim dtSubClassAuth As DataTable
        Dim dtAllClassAuth As DataTable

        dtUsrAuth = objAdmin.GetUsrAuthority(id)
        dtClassAuth = objAdmin.GetClassAuthority(id)
        dtSubClassAuth = objAdmin.GetSubClassAuthority(id)
        dtAllClassAuth = objAdmin.GetAllClassAuthority(id)

        dtUsrAuth.TableName = c_UsrAuthorityTableName
        dtClassAuth.TableName = c_ClassAuthorityTableName
        dtSubClassAuth.TableName = c_SubClassAuthorityTableName
        dtAllClassAuth.TableName = c_AllClassAuthorityTableName

        ds.Tables.Add(dtUsrAuth)
        ds.Tables.Add(dtClassAuth)
        ds.Tables.Add(dtSubClassAuth)
        ds.Tables.Add(dtAllClassAuth)

        Dim dtSch As DataTable = objClass.GetSchoolList()
        dtSch.TableName = c_SchoolListTableName
        ds.Tables.Add(dtSch)

        Dim dtSchGrp As DataTable = objClass.GetSchoolGrpList()
        dtSchGrp.TableName = c_SchoolGrpListTableName
        ds.Tables.Add(dtSchGrp)

        Dim dtSibType As DataTable = objStu.GetSibTypeList()
        dtSibType.TableName = c_SibTypeListTableName
        ds.Tables.Add(dtSibType)

        Dim dtSchType As DataTable = objClass.GetSchoolTypes()
        dtSchType.TableName = c_SchoolTypesTableName
        ds.Tables.Add(dtSchType)

        Dim dtClass As DataTable = objClass.GetClassList()
        dtClass.TableName = c_ClassListDataTableName
        ds.Tables.Add(dtClass)

        Dim dtSubClass As DataTable = objClass.GetSubClassList()
        dtSubClass.TableName = c_SubClassListDataTableName
        ds.Tables.Add(dtSubClass)

        Dim dtSales As DataTable = objAdmin.GetSalesList()
        dtSales.TableName = c_SalesListDataTableName
        ds.Tables.Add(dtSales)

        Dim dtClassRoom As DataTable = objClass.GetClassRooms()
        dtClassRoom.TableName = c_ClassRoomListTableName
        ds.Tables.Add(dtClassRoom)

        Dim dtSubjects As DataTable = objClass.GetSubjects()
        dtSubjects.TableName = c_SubjectsTableName
        ds.Tables.Add(dtSubjects)

        Dim objAcc As New Accounting
        Dim dtJournalGrp As DataTable
        dtJournalGrp = objAcc.GetJournalGrp()
        dtJournalGrp.TableName = c_JournalGrpTableName
        ds.Tables.Add(dtJournalGrp)

        Dim dtCont As DataTable = objClass.GetContentListByUsr(id)
        dtCont.TableName = c_ContentsTableName
        ds.Tables.Add(dtCont)
        Dim dtSession As DataTable = objClass.GetSessionListByUsr(id)
        dtSession.TableName = c_SessionListDataTableName
        ds.Tables.Add(dtSession)

        Dim dtBook As DataTable = objClass.GetBookListByUsr(id)
        dtBook.TableName = c_BookListTableName
        ds.Tables.Add(dtBook)

        Dim dtPrtOpt As DataTable = objAdmin.GetReceiptPrint
        dtPrtOpt.TableName = c_PrintOptTableName
        ds.Tables.Add(dtPrtOpt)
        Return ds
    End Function

    Function GetStuRecByName(ByVal strName As String) As DataTable Implements ICsol.GetStuRecByName
        Dim objDb As New StuInfo
        Dim dt As DataTable = objDb.GetStuRecByName(strName)
        Return dt
    End Function

    Sub UpdateStuBasic(ByVal strID As String, ByVal strName As String, _
                            ByVal strTel1 As String) Implements ICsol.UpdateStuBasic
        Dim objDb As New StuInfo
        objDb.UpdateStuBasic(strID, strName, strTel1)
    End Sub
    Public Sub AddStudent(ByVal ID As String, ByVal Name As String, ByVal EngName As String, ByVal Sex As String, _
                  ByVal Birthday As Date, ByVal IC As String, ByVal PWD As String, ByVal Address As String, _
                  ByVal PostalCode As String, ByVal Address2 As String, ByVal PostalCode2 As String, _
                  ByVal Tel1 As String, ByVal Tel2 As String, ByVal OfficeTel As String, ByVal Mobile As String, _
                  ByVal Email As String, ByVal CardNum As String, ByVal StuType As Byte, ByVal School As String, _
                  ByVal SchoolClass As String, ByVal SchoolGrade As Int32, ByVal PrimarySch As Int32, _
                  ByVal JuniorSch As Int32, ByVal HighSch As Int32, ByVal University As Int32, _
                  ByVal CurrentSch As Int32, ByVal SchGroup As Int32, ByVal GraduateFrom As String, _
                  ByVal DadName As String, ByVal MumName As String, ByVal DadJob As String, ByVal MumJob As String, _
                  ByVal DadMobile As String, ByVal MumMobile As String, ByVal IntroID As String, _
                  ByVal IntroName As String, ByVal CreateDate As Date, ByVal FeeOwe As Int32, _
                  ByVal Remarks As String, ByVal Customize1 As String, ByVal Customize2 As String, _
                  ByVal Customize3 As String, ByVal Customize4 As String, ByVal Customize5 As String, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String) Implements ICsol.AddStudent

        Dim objStu As New StuInfo
        objStu.AddStudent(ID, Name, EngName, Sex, Birthday, IC, PWD, Address, PostalCode, _
                          Address2, PostalCode2, Tel1, _
                            Tel2, OfficeTel, Mobile, Email, CardNum, StuType, School, _
                            SchoolClass, SchoolGrade, _
                            PrimarySch, JuniorSch, HighSch, University, _
                            CurrentSch, SchGroup, GraduateFrom, _
                            DadName, MumName, DadJob, MumJob, DadMobile, _
                            MumMobile, IntroID, IntroName, _
                            CreateDate, FeeOwe, Remarks, Customize1, _
                            Customize2, Customize3, Customize4, Customize5, EnrollSch, PunchCardNum, HseeMark)

    End Sub

    Public Function AddPay(ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal SalesID As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String, _
                     ByVal ChequeValid As Date, ByVal Discount As Integer, _
                         ByVal DiscountRemarks As String, ByVal oldDiscount As Integer, _
                         ByVal oldDiscountRemarks As String, ByVal oldDiscountId As Integer, ByVal DateGet As DateTime) As Int32 Implements ICsol.AddPay
        Dim objStu As New StuInfo

        If Amount >= 0 Then
            objStu.AddPay(ReceiptNum, StuID, SubClassID, Amount, DateTime, HandlerID, SalesID, PayMethod, Extra, Remarks, ChequeValid, DateGet)
        ElseIf Amount = -1 Then
            Amount = 0
        End If

        If Discount <> 0 And oldDiscount = -999999 Then 'add new disc
            objStu.AddDiscount(StuID, SubClassID, Discount, DiscountRemarks)
            objStu.UpdStuOwe(StuID, -(Discount + Amount))
            objStu.UpdStuClassOwe(StuID, SubClassID, -(Discount + Amount))
        ElseIf oldDiscount <> -999999 Then 'modify disc
            objStu.UpdateDisc(oldDiscountId, Discount, DiscountRemarks)
            objStu.AddDiscModRec(StuID, SubClassID, DateTime, HandlerID, Discount, DiscountRemarks, oldDiscount, oldDiscountRemarks)
            objStu.UpdStuOwe(StuID, -(Discount - oldDiscount + Amount))
            objStu.UpdStuClassOwe(StuID, SubClassID, -(Discount - oldDiscount + Amount))
        Else 'no disc
            objStu.UpdStuOwe(StuID, -Amount)
            objStu.UpdStuClassOwe(StuID, SubClassID, -Amount)
        End If



        Return 0

    End Function

    Public Function AddMB(ByVal pid As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                 ByVal SubClassID As Integer, ByVal Amount As Integer, _
                 ByVal PayAmount As Integer, ByVal PayDateTime As Date, _
                 ByVal DateTime As Date, ByVal HandlerID As String, _
                 ByVal SalesID As String, ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String, _
                 ByVal Discount As Integer, _
                 ByVal DiscountRemarks As String, _
                 ByVal Reason As String) As Int32 Implements ICsol.AddMB
        Dim obj As New StuInfo
        '修改 StuInfo (學生基本資料) 退費統計
        obj.UpdStuOwe(StuID, Amount)

        obj.UpdStuClassOwe(StuID, SubClassID, Amount)
        '新增 MoneyBackClassInfo (退費單筆記錄)
        Dim id As Integer = obj.AddMBClassInfo(StuID, SubClassID, Amount, DateTime, _
                                           HandlerID, SalesID, Reason, DiscountRemarks, _
                                        Remarks, Discount)
        '新增 MoneyBackPayRec()
        obj.AddMBPayRec(id, ReceiptNum, PayAmount, PayDateTime, HandlerID, _
                        SalesID, PayMethod, Extra, Remarks)
        obj.DeletePay(pid)
    End Function

    Public Function AddMK(ByVal pid As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                 ByVal SubClassID As Integer, ByVal Amount As Integer, _
                 ByVal PayAmount As Integer, ByVal PayDateTime As Date, _
                 ByVal DateTime As Date, ByVal HandlerID As String, _
                 ByVal SalesID As String, ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String, _
                 ByVal Discount As Integer, _
                 ByVal DiscountRemarks As String, _
                 ByVal Reason As String, ByVal KeepUntil As DateTime) As Int32 Implements ICsol.AddMK
        Dim obj As New StuInfo
        Dim id As Integer = obj.AddMKClassInfo(StuID, SubClassID, Amount, DateTime, _
                                           HandlerID, SalesID, Reason, DiscountRemarks, _
                                        Remarks, Discount, KeepUntil)
        obj.AddMKPayRec(id, ReceiptNum, PayAmount, PayDateTime, HandlerID, _
                        SalesID, PayMethod, Extra, Remarks)
        obj.DeletePay(pid)


    End Function

    Public Overloads Sub AddPunchRec(ByVal stuid As String, ByVal ptime As DateTime, ByVal SubClassID As Integer, ByVal SubClassContID As Integer, ByVal MakeUpClassID As Integer, ByVal SessionID As Integer, ByVal bIn As Byte) Implements ICsol.AddPunchRec
        Dim obj As New ClassInfo

        obj.AddPunchRec(stuid, ptime, SubClassID, SubClassContID, MakeUpClassID, bIn, SessionID)                                                    '100321

    End Sub

    Public Overloads Sub AddPunchRec(ByVal dtRec As ArrayList, _
           ByVal SubClassID As Integer, _
           ByVal SubClassContID As Integer, _
           ByVal MakeUpClassID As Integer, _
           ByVal SessionID As Integer, _
           ByVal DateTime As Date) Implements ICsol.AddPunchRec
        Dim obj As New ClassInfo
        For index As Integer = 0 To dtRec.Count - 1
            obj.AddPunchRec(dtRec(index), DateTime, _
                             SubClassID, SubClassContID, MakeUpClassID, _
                             0, SessionID)                                                  '100321
            obj.AddPunchRec(dtRec(index), DateTime, _
                             SubClassID, SubClassContID, MakeUpClassID, _
                             1, SessionID)                                                  '100321
        Next
    End Sub

    Public Sub AddStuNote(ByVal StuID As String _
           , ByVal DisplayDate As Date _
           , ByVal RepeatBfDate As Byte _
           , ByVal DisplayFor As Integer _
           , ByVal Frequency As Byte _
           , ByVal Content As String _
           , ByVal Style As Integer _
           , ByVal CreateBy As String) Implements ICsol.AddStuNote
        Dim obj As New StuInfo
        obj.AddStuNote(StuID, DisplayDate, RepeatBfDate, DisplayFor, Frequency, _
                        Content, Style, CreateBy)
    End Sub

    Public Sub AddBadgeReissueRec(ByVal StuID As String, _
           ByVal DateTime As Date, _
           ByVal HandlerID As String) Implements ICsol.AddBadgeReissueRec
        Dim obj As New StuInfo
        obj.AddBadgeReissueRec(StuID, DateTime, HandlerID)
    End Sub

    Public Function GetStuNote(ByVal StuId As String, ByVal StuName As String) As DataSet Implements ICsol.GetStuNote
        Dim obj As New StuInfo
        Dim ds As New DataSet

        Dim dt1 As DataTable = obj.GetStuNote(StuId, StuName)

        Dim dt2 As DataTable
        If dt1.Rows.Count = 0 Then
            dt2 = obj.GetStuNoteReceive(-1)
        ElseIf dt1.Rows.Count = 1 Then
            dt2 = obj.GetStuNoteReceive(dt1.Rows(0).Item(c_IDColumnName))
        Else
            dt2 = obj.GetStuNoteReceive(dt1.Rows(0).Item(c_IDColumnName))
            For index As Integer = 1 To dt1.Rows.Count - 1
                dt2.Merge(obj.GetStuNoteReceive(dt1.Rows(index).Item(c_IDColumnName)))
            Next
        End If
        dt1.TableName = c_StuNoteDataTableName
        dt2.TableName = c_StuNoteReceiveDataTableName
        ds.Tables.Add(dt1)
        ds.Tables.Add(dt2)
        Return ds
    End Function

    Public Sub DeleteStuNote(ByVal id As Integer) Implements ICsol.DeleteStuNote
        Dim obj As New StuInfo
        obj.DeleteStuNote(id)
    End Sub

    Public Sub DeleteBookIssueRec(ByVal id As Integer) Implements ICsol.DeleteBookIssueRec
        Dim obj As New Book
        obj.DeleteBookIssueRec(id)

    End Sub

    Public Sub DeletePunchRec(ByVal id As Integer) Implements ICsol.DeletePunchRec
        Dim obj As New StuInfo
        obj.DeletePunchRec(id)
    End Sub

    Public Function GetSubClassPunchRec(ByVal scId As Integer, ByVal cId As Integer, _
                                       ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable Implements ICsol.GetSubClassPunchRec
        Dim obj As New ClassInfo
        Return obj.GetSubClassPunchRec(scId, cId, dateFrom, dateTo)
    End Function

    Public Function GetIdvClassPunchRec(ByVal scId As Integer, _
                                      ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable Implements ICsol.GetIdvClassPunchRec
        Dim obj As New ClassInfo
        Return obj.GetIdvClassPunchRec(scId, dateFrom, dateTo)
    End Function

    Public Function GetSubClassPunchRecNoCont(ByVal scId As Integer, _
                                       ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable Implements ICsol.GetSubClassPunchRecNoCont
        Dim obj As New ClassInfo
        Return obj.GetSubClassPunchRecNoCont(scId, dateFrom, dateTo)
    End Function

    Public Function GetUsrList() As DataTable Implements ICsol.GetUsrList
        Dim obj As New Admin
        Return obj.GetUsrList
    End Function

    Public Function GetPunchRecById(ByVal strId As String) As DataTable Implements ICsol.GetPunchRecById
        Dim obj As New StuInfo
        Return obj.GetPunchRecById(strId)
    End Function

    Public Function CheckStuPunch(ByVal stuId As String, ByVal scId As Integer, ByVal sStart As Date, ByVal sEnd As Date, _
                                   ByVal sessionId As Integer) As Boolean Implements ICsol.CheckStuPunch
        Dim obj As New StuInfo
        Dim dt As DataTable
        dt = obj.GetRepeatPunchRec(stuId, scId, sStart, sEnd, sessionId, c_ClassIn)
        If dt.Rows.Count > 0 Then
            Return True
        Else
            Return False
        End If

    End Function


    'Public Function StuPunchWithIDHelper(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByRef LS As ArrayList) As DataTable Implements ICsol.StuPunchWithID

    '    Dim dtResult As New DataTable
    '    Dim strCardNum As String
    '    Dim ds As New DataSet
    '    Dim objStu As New StuInfo
    '    Dim objBook As New Book
    '    Dim dateTemp As Date
    '    Dim dateTemp2 As Date

    '    Dim timetestA As Date = Now


    '    dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
    '    dtResult.Columns.Add(c_ClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SeatNumColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_ContentColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionStartColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionEndColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BooksColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BookResultColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_NoteColumnName, GetType(System.String))
    '    '12/02 updated by Sherry Start
    '    dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
    '    dtResult.Rows.Add("", -1, "", "", "", "", "", "", "", "", "", stuID)
    '    '12/02 updated by Sherry End

    '    stuinfo = objStu.GetPunchStuInfoByID(stuID)
    '    If stuinfo.Rows.Count = 0 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
    '        Return dtResult
    '    End If

    '    strCardNum = stuinfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim.ToUpper
    '    'If strCardNum.Length = 0 Then
    '    '    dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoCard
    '    '    Return dtResult
    '    'End If

    '    dtResult.Rows(0).Item(c_StuNameColumnName) = stuinfo.Rows(0).Item(c_StuNameColumnName).ToString.Trim



    '    Dim idx As Integer = -1
    '    Dim str As String
    '    Dim dateStart As Date
    '    Dim dateEnd As Date
    '    Dim intWday As Integer = punch.DayOfWeek
    '    If intWday = 0 Then
    '        intWday = 7
    '    End If
    '    LS.Clear()
    '    Dim timetestB As Date = Now

    '    For i As Integer = 0 To stuinfo.Rows.Count - 1
    '        If intWday = GetIntValue(stuinfo.Rows(i).Item(c_DayOfWeekColumnName)) Then '檢察星期
    '            dateTemp = stuinfo.Rows(i).Item(c_ClassEndColumnName)
    '            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then '檢察是否過時
    '                If IsDBNull(stuinfo.Rows(i).Item(c_ContentStartColumnName)) Then
    '                    dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
    '                    'dtResult.Rows(0).Item(c_ClassNameColumnName) = stuinfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
    '                    'dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuinfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim

    '                    Return dtResult
    '                Else
    '                    dateTemp = stuinfo.Rows(i).Item(c_ContentStartColumnName)
    '                    dateTemp2 = stuinfo.Rows(i).Item(c_StartColumnName)
    '                    dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
    '                    dateTemp = stuinfo.Rows(i).Item(c_ContentEndColumnName)
    '                    dateTemp2 = stuinfo.Rows(i).Item(c_EndColumnName)
    '                    dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

    '                    If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then '檢查上課時間

    '                        idx = i
    '                        str = i
    '                        LS.AddRange(str.Split(","))
    '                    End If

    '                End If
    '            End If
    '        End If
    '    Next


    '    'For i As Integer = 0 To stuinfo.Rows.Count - 1
    '    '    If intWday = GetIntValue(stuinfo.Rows(i).Item(c_DayOfWeekColumnName)) Then '檢察星期
    '    '        dateTemp = stuinfo.Rows(i).Item(c_ClassEndColumnName)
    '    '        If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then '檢察是否過時

    '    '            If IsDBNull(stuinfo.Rows(i).Item(c_ContentStartColumnName)) Then
    '    '                dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
    '    '                dtResult.Rows(0).Item(c_ClassNameColumnName) = stuinfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
    '    '                dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuinfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim
    '    '                Return dtResult
    '    '            End If

    '    '            dateTemp = stuinfo.Rows(i).Item(c_ContentStartColumnName)
    '    '            dateTemp2 = stuinfo.Rows(i).Item(c_StartColumnName)
    '    '            dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
    '    '            dateTemp = stuinfo.Rows(i).Item(c_ContentEndColumnName)
    '    '            dateTemp2 = stuinfo.Rows(i).Item(c_EndColumnName)
    '    '            dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

    '    '            If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then '檢查上課時間

    '    '                idx = i
    '    '                str = i
    '    '                LS.AddRange(str.Split(","))
    '    '                'If i = stuinfo.Rows.Count - 1 Then
    '    '                '    Exit For
    '    '                'End If
    '    '            End If
    '    '        End If
    '    '    End If
    '    'Next
    '    If idx = -1 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoClass
    '        Return dtResult
    '    End If
    '    Return dtResult
    'End Function

    Public Function StuPunchWithIDstep2Helper(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal dtResult As DataTable, _
                                        ByVal idx As Integer) As DataTable Implements ICsol.StuPunchWithIDstep2Helper


        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        If intWday = 0 Then
            intWday = 7
        End If

        Dim retTB As DataTable = New DataTable()

        'CSOL.Logger.LogMsg("4.1: " & Now.ToString())
        Dim stuinfo As DataTable = objStu.GetPunchStuInfoByID(stuID)
        'For i As Integer = 0 To stuinfo.Rows.Count - 1
        If intWday = GetIntValue(stuinfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = stuinfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = stuinfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = stuinfo.Rows(idx).Item(c_ClassEndColumnName)
        End If
        'Next
        'CSOL.Logger.LogMsg("4.2: " & Now.ToString())

        Dim intSession As Integer = GetIntValue(stuinfo.Rows(idx).Item(c_SessionIDColumnName))
        Dim intSubClass As Integer = GetIntValue(stuinfo.Rows(idx).Item(c_SubClassIDColumnName))
        Dim dtRepeat As New DataTable
        'CSOL.Logger.LogMsg("4.3: " & Now.ToString())

        dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
                                            DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
        'CSOL.Logger.LogMsg("4.4: " & Now.ToString())

        If dtRepeat.Rows.Count > 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
            retTB.Clear()
            retTB.Columns.Clear()
            For Each column As DataColumn In dtResult.Columns
                retTB.Columns.Add(column.ColumnName)
            Next

            retTB.ImportRow(dtResult.Rows(0))
            'Return dtResult

            Return retTB
        End If

        'CSOL.Logger.LogMsg("4.5: " & Now.ToString())

        Dim intContent As Integer


        Dim timetestB As Date = Now

        'For i As Integer = 0 To stuinfo.Rows.Count - 1
        If intWday = GetIntValue(stuinfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = stuinfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = stuinfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = stuinfo.Rows(idx).Item(c_ClassEndColumnName)
            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
                    If Not DBNull.Value.Equals(stuinfo.Rows(idx).Item("ContentStart")) And Not DBNull.Value.Equals(stuinfo.Rows(idx).Item("ContentEnd")) Then
                        Dim contentStart As Date = CDate(stuinfo.Rows(idx).Item("ContentStart"))
                        Dim contentEnd As Date = CDate(stuinfo.Rows(idx).Item("ContentEnd"))
                        If contentStart <= Now And contentEnd >= Now Then
                            intContent = GetIntValue(stuinfo.Rows(idx).Item(c_ContentIDColumnName))
                            dtResult.Rows(0).Item(c_ContentColumnName) = stuinfo.Rows(idx).Item(c_ContentColumnName).ToString.Trim
                            'Exit For
                        End If
                    End If
                End If
            End If
        End If
        'CSOL.Logger.LogMsg("4.6: " & Now.ToString())

        'Next
        AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)
        'CSOL.Logger.LogMsg("4.7: " & Now.ToString())

        dtResult.Rows(0).Item(c_ClassNameColumnName) = stuinfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuinfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SeatNumColumnName) = stuinfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
        dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd
        Dim dtNote As New DataTable
        dtNote = objStu.GetStuNoteByID(stuID)
        'CSOL.Logger.LogMsg("4.8: " & Now.ToString())

        Dim flag As Boolean = True
        If dtNote.Rows.Count > 0 Then
            Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag Then
                objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
                'CSOL.Logger.LogMsg("4.9: " & Now.ToString())

                dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
            End If
        End If
        'CSOL.Logger.LogMsg("4.10: " & Now.ToString())

        'If bookIssue Then
        '    Dim dtBook As New DataTable
        '    dtBook = objStu.GetClassBook(intSubClass)
        '    Dim lstBook As New ArrayList
        '    Dim lstBookResult As New ArrayList
        '    Dim r As Integer = 0
        '    Dim book As Integer
        '    Dim FeeClear As Byte
        '    Dim FeeClearDate As Date
        '    Dim MultiClass As Byte
        '    Dim CreateDate As Date
        '    Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        '    Dim c2 As String = c_StuIDColumnName & "='"
        '    Dim c3 As String = c_SubClassIDColumnName & "="
        '    Dim c4 As String = c_DateTimeColumnName & "> '"
        '    Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
        '    Dim c6 As String = c_ClassIDColumnName & "="
        '    Dim dateReg As Date = stuinfo.Rows(idx).Item(c_RegDateColumnName)
        '    Dim dtPay As New DataTable
        '    dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
        '    Dim intFeeOwe As Integer = stuinfo.Rows(idx).Item(c_FeeOweColumnName)
        '    Dim dtBookIssue As New DataTable
        '    Dim intClassID As Integer = GetIntValue(stuinfo.Rows(idx).Item(c_ClassIDColumnName))

        '    For i As Integer = 0 To dtBook.Rows.Count - 1
        '        FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
        '        FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
        '        MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
        '        CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
        '        lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
        '        book = dtBook.Rows(i).Item(c_IDColumnName)

        '        If CreateDate.Year <> 1911 Then
        '            If dateReg > CreateDate Then
        '                lstBookResult.Add(1)
        '                Continue For
        '            End If
        '        End If

        '        If FeeClear = 0 Then
        '            If FeeClearDate.Year <> 1911 Then
        '                If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
        '                                 intSubClass.ToString & " AND " & c4 & _
        '                                    FeeClearDate.ToString & "'") > 0 Then
        '                    lstBookResult.Add(3)
        '                    Continue For
        '                End If
        '            End If
        '            If intFeeOwe > 0 Then
        '                lstBookResult.Add(2)
        '                Continue For
        '            End If
        '        End If

        '        dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

        '        If MultiClass = 0 Then
        '            If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        Else
        '            If dtBookIssue.Rows.Count > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        End If

        '        objBook.AddBookIssueRec(stuID, punch, intClassID, book)
        '        lstBookResult.Add(0)
        '    Next

        '    Dim strBooks As String = ""
        '    Dim strBookResults As String = ""

        '    For i As Integer = 0 To lstBook.Count - 1
        '        If i = 0 Then
        '            strBooks = lstBook(i)
        '            strBookResults = lstBookResult(i)
        '        Else
        '            strBooks = strBooks & "^" & lstBook(i)
        '            strBookResults = strBookResults & "^" & lstBookResult(i)
        '        End If
        '    Next

        '    dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
        '    dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
        'End If

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        'CSOL.Logger.LogMsg("4.11: " & Now.ToString())
        'Return dtResult
        retTB.Clear()
        retTB.Columns.Clear()
        For Each column As DataColumn In dtResult.Columns
            retTB.Columns.Add(column.ColumnName)
        Next

        retTB.ImportRow(dtResult.Rows(0))
        'Return dtResult

        Return retTB
    End Function

    Public Function StuPunchWithIDstep2(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal dtResult As DataTable, ByRef stuinfo As DataTable, _
                                        ByVal idx As Integer) As DataTable Implements ICsol.StuPunchWithIDstep2


        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        If intWday = 0 Then
            intWday = 7
        End If

        CSOL.Logger.LogMsg("4.1: " & Now.ToString())
        'For i As Integer = 0 To stuinfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)
        End If
        'Next
        CSOL.Logger.LogMsg("4.2: " & Now.ToString())

        Dim intSession As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SessionIDColumnName))
        Dim intSubClass As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SubClassIDColumnName))
        Dim dtRepeat As New DataTable
        CSOL.Logger.LogMsg("4.3: " & Now.ToString())

        dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
                                            DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
        CSOL.Logger.LogMsg("4.4: " & Now.ToString())

        If dtRepeat.Rows.Count > 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
            Return dtResult
        End If

        CSOL.Logger.LogMsg("4.5: " & Now.ToString())

        Dim intContent As Integer


        Dim timetestB As Date = Now

        'For i As Integer = 0 To stuinfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)
            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
                    If Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentStart")) And Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentEnd")) Then
                        Dim contentStart As Date = CDate(StuInfo.Rows(idx).Item("ContentStart"))
                        Dim contentEnd As Date = CDate(StuInfo.Rows(idx).Item("ContentEnd"))
                        If contentStart <= Now And contentEnd >= Now Then
                            intContent = GetIntValue(StuInfo.Rows(idx).Item(c_ContentIDColumnName))
                            dtResult.Rows(0).Item(c_ContentColumnName) = StuInfo.Rows(idx).Item(c_ContentColumnName).ToString.Trim
                            'Exit For
                        End If
                    End If
                End If
            End If
        End If
        CSOL.Logger.LogMsg("4.6: " & Now.ToString())

        'Next
        AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)
        CSOL.Logger.LogMsg("4.7: " & Now.ToString())

        dtResult.Rows(0).Item(c_ClassNameColumnName) = StuInfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SubClassNameColumnName) = StuInfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SeatNumColumnName) = StuInfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
        dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd
        Dim dtNote As New DataTable
        dtNote = objStu.GetStuNoteByID(stuID)
        CSOL.Logger.LogMsg("4.8: " & Now.ToString())

        Dim flag As Boolean = True
        If dtNote.Rows.Count > 0 Then
            Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag Then
                objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
                CSOL.Logger.LogMsg("4.9: " & Now.ToString())

                dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
            End If
        End If
        CSOL.Logger.LogMsg("4.10: " & Now.ToString())

        'If bookIssue Then
        '    Dim dtBook As New DataTable
        '    dtBook = objStu.GetClassBook(intSubClass)
        '    Dim lstBook As New ArrayList
        '    Dim lstBookResult As New ArrayList
        '    Dim r As Integer = 0
        '    Dim book As Integer
        '    Dim FeeClear As Byte
        '    Dim FeeClearDate As Date
        '    Dim MultiClass As Byte
        '    Dim CreateDate As Date
        '    Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        '    Dim c2 As String = c_StuIDColumnName & "='"
        '    Dim c3 As String = c_SubClassIDColumnName & "="
        '    Dim c4 As String = c_DateTimeColumnName & "> '"
        '    Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
        '    Dim c6 As String = c_ClassIDColumnName & "="
        '    Dim dateReg As Date = stuinfo.Rows(idx).Item(c_RegDateColumnName)
        '    Dim dtPay As New DataTable
        '    dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
        '    Dim intFeeOwe As Integer = stuinfo.Rows(idx).Item(c_FeeOweColumnName)
        '    Dim dtBookIssue As New DataTable
        '    Dim intClassID As Integer = GetIntValue(stuinfo.Rows(idx).Item(c_ClassIDColumnName))

        '    For i As Integer = 0 To dtBook.Rows.Count - 1
        '        FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
        '        FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
        '        MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
        '        CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
        '        lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
        '        book = dtBook.Rows(i).Item(c_IDColumnName)

        '        If CreateDate.Year <> 1911 Then
        '            If dateReg > CreateDate Then
        '                lstBookResult.Add(1)
        '                Continue For
        '            End If
        '        End If

        '        If FeeClear = 0 Then
        '            If FeeClearDate.Year <> 1911 Then
        '                If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
        '                                 intSubClass.ToString & " AND " & c4 & _
        '                                    FeeClearDate.ToString & "'") > 0 Then
        '                    lstBookResult.Add(3)
        '                    Continue For
        '                End If
        '            End If
        '            If intFeeOwe > 0 Then
        '                lstBookResult.Add(2)
        '                Continue For
        '            End If
        '        End If

        '        dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

        '        If MultiClass = 0 Then
        '            If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        Else
        '            If dtBookIssue.Rows.Count > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        End If

        '        objBook.AddBookIssueRec(stuID, punch, intClassID, book)
        '        lstBookResult.Add(0)
        '    Next

        '    Dim strBooks As String = ""
        '    Dim strBookResults As String = ""

        '    For i As Integer = 0 To lstBook.Count - 1
        '        If i = 0 Then
        '            strBooks = lstBook(i)
        '            strBookResults = lstBookResult(i)
        '        Else
        '            strBooks = strBooks & "^" & lstBook(i)
        '            strBookResults = strBookResults & "^" & lstBookResult(i)
        '        End If
        '    Next

        '    dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
        '    dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
        'End If

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        CSOL.Logger.LogMsg("4.11: " & Now.ToString())
        Return dtResult
    End Function


    Public Function StuPunchWithID(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByRef LS As ArrayList, ByRef stuinfo As DataTable) As DataTable Implements ICsol.StuPunchWithID


        Dim dtResult As New DataTable
        Dim strCardNum As String
        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateTemp2 As Date

        Dim timetestA As Date = Now

        dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
        dtResult.Columns.Add(c_ClassNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SeatNumColumnName, GetType(System.String))
        dtResult.Columns.Add(c_ContentColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SessionStartColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SessionEndColumnName, GetType(System.String))
        dtResult.Columns.Add(c_BooksColumnName, GetType(System.String))
        dtResult.Columns.Add(c_BookResultColumnName, GetType(System.String))
        dtResult.Columns.Add(c_NoteColumnName, GetType(System.String))
        '12/02 updated by Sherry Start
        dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
        dtResult.Rows.Add("", -1, "", "", "", "", "", "", "", "", "", stuID)
        '12/02 updated by Sherry End

        stuinfo = objStu.GetPunchStuInfoByID(stuID)
        If stuinfo.Rows.Count = 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
            Return dtResult
        End If

        strCardNum = stuinfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim.ToUpper
        'If strCardNum.Length = 0 Then
        '    dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoCard
        '    Return dtResult
        'End If

        dtResult.Rows(0).Item(c_StuNameColumnName) = stuinfo.Rows(0).Item(c_StuNameColumnName).ToString.Trim



        Dim idx As Integer = -1
        Dim str As String
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        If intWday = 0 Then
            intWday = 7
        End If
        LS.Clear()
        Dim timetestB As Date = Now

        For i As Integer = 0 To stuinfo.Rows.Count - 1
            If intWday = GetIntValue(stuinfo.Rows(i).Item(c_DayOfWeekColumnName)) Then '檢察星期
                dateTemp = stuinfo.Rows(i).Item(c_ClassEndColumnName)
                If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then '檢察是否過時
                    If IsDBNull(stuinfo.Rows(i).Item(c_ContentStartColumnName)) Then
                        dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
                        'dtResult.Rows(0).Item(c_ClassNameColumnName) = stuinfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
                        'dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuinfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim

                        Return dtResult
                    Else
                        dateTemp = stuinfo.Rows(i).Item(c_ContentStartColumnName)
                        dateTemp2 = stuinfo.Rows(i).Item(c_StartColumnName)
                        dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
                        dateTemp = stuinfo.Rows(i).Item(c_ContentEndColumnName)
                        dateTemp2 = stuinfo.Rows(i).Item(c_EndColumnName)
                        dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

                        If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then '檢查上課時間

                            idx = i
                            str = i
                            LS.AddRange(str.Split(","))
                        End If

                    End If
                End If
            End If
        Next


        'For i As Integer = 0 To stuinfo.Rows.Count - 1
        '    If intWday = GetIntValue(stuinfo.Rows(i).Item(c_DayOfWeekColumnName)) Then '檢察星期
        '        dateTemp = stuinfo.Rows(i).Item(c_ClassEndColumnName)
        '        If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then '檢察是否過時

        '            If IsDBNull(stuinfo.Rows(i).Item(c_ContentStartColumnName)) Then
        '                dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
        '                dtResult.Rows(0).Item(c_ClassNameColumnName) = stuinfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
        '                dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuinfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim
        '                Return dtResult
        '            End If

        '            dateTemp = stuinfo.Rows(i).Item(c_ContentStartColumnName)
        '            dateTemp2 = stuinfo.Rows(i).Item(c_StartColumnName)
        '            dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
        '            dateTemp = stuinfo.Rows(i).Item(c_ContentEndColumnName)
        '            dateTemp2 = stuinfo.Rows(i).Item(c_EndColumnName)
        '            dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

        '            If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then '檢查上課時間

        '                idx = i
        '                str = i
        '                LS.AddRange(str.Split(","))
        '                'If i = stuinfo.Rows.Count - 1 Then
        '                '    Exit For
        '                'End If
        '            End If
        '        End If
        '    End If
        'Next
        If idx = -1 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoClass
            Return dtResult
        End If
        Return dtResult
    End Function
    '----↓↓↓↓↓↓刷卡原始版本(沒有選擇班別視窗)
    'Public Function StuPunchWithID(ByVal stuID As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean) As DataTable Implements ICsol.StuPunchWithID
    '    Dim dtResult As New DataTable
    '    Dim strCardNum As String
    '    Dim ds As New DataSet
    '    Dim objStu As New StuInfo
    '    Dim objBook As New Book
    '    Dim dateTemp As Date

    '    dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
    '    dtResult.Columns.Add(c_ClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SeatNumColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_ContentColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionStartColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionEndColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BooksColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BookResultColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_NoteColumnName, GetType(System.String))
    '    '12/02 updated by Sherry Start
    '    dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
    '    dtResult.Rows.Add("", -1, "", "", "", "", "", "", "", "", "", stuID)
    '    '12/02 updated by Sherry End

    '    Dim stuInfo As DataTable = objStu.GetPunchStuInfoByID(stuID)
    '    If stuInfo.Rows.Count = 0 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
    '        Return dtResult
    '    End If

    '    strCardNum = stuInfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim.ToUpper
    '    'If strCardNum.Length = 0 Then
    '    '    dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoCard
    '    '    Return dtResult
    '    'End If

    '    dtResult.Rows(0).Item(c_StuNameColumnName) = stuInfo.Rows(0).Item(c_StuNameColumnName).ToString.Trim

    '    Dim idx As Integer = -1
    '    Dim dateStart As Date
    '    Dim dateEnd As Date
    '    Dim intWday As Integer = punch.DayOfWeek
    '    If intWday = 0 Then
    '        intWday = 7
    '    End If

    '    For i As Integer = 0 To stuInfo.Rows.Count - 1
    '        If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
    '            dateTemp = stuInfo.Rows(i).Item(c_StartColumnName)
    '            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_EndColumnName)
    '            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)
    '            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
    '                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
    '                    idx = i
    '                    Exit For
    '                End If
    '            End If
    '        End If
    '    Next

    '    If idx = -1 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoClass
    '        Return dtResult
    '    End If

    '    Dim intSession As Integer = GetIntValue(stuInfo.Rows(idx).Item(c_SessionIDColumnName))
    '    Dim intSubClass As Integer = GetIntValue(stuInfo.Rows(idx).Item(c_SubClassIDColumnName))

    '    Dim dtRepeat As New DataTable
    '    dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
    '                                        DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
    '    If dtRepeat.Rows.Count > 0 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
    '        Return dtResult
    '    End If

    '    Dim intContent As Integer
    '    For i As Integer = 0 To stuInfo.Rows.Count - 1
    '        If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
    '            dateTemp = stuInfo.Rows(i).Item(c_StartColumnName)
    '            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_EndColumnName)
    '            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)
    '            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
    '                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
    '                    If Not DBNull.Value.Equals(stuInfo.Rows(i).Item("ContentStart")) And Not DBNull.Value.Equals(stuInfo.Rows(i).Item("ContentEnd")) Then
    '                        Dim contentStart As Date = CDate(stuInfo.Rows(i).Item("ContentStart"))
    '                        Dim contentEnd As Date = CDate(stuInfo.Rows(i).Item("ContentEnd"))
    '                        If contentStart <= Now And contentEnd >= Now Then
    '                            intContent = GetIntValue(stuInfo.Rows(i).Item(c_ContentIDColumnName))
    '                            dtResult.Rows(0).Item(c_ContentColumnName) = stuInfo.Rows(i).Item(c_ContentColumnName).ToString.Trim
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Next






    '    AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)

    '    dtResult.Rows(0).Item(c_ClassNameColumnName) = stuInfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuInfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SeatNumColumnName) = stuInfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
    '    dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd

    '    'Get Note
    '    Dim dtNote As New DataTable
    '    dtNote = objStu.GetStuNoteByID(stuID)

    '    Dim flag As Boolean = True
    '    If dtNote.Rows.Count > 0 Then
    '        Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
    '        Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
    '        If repeat = 1 Then
    '            If New Date(Now.Year, Now.Month, Now.Day) > _
    '                New Date(dt.Year, dt.Month, dt.Day) Then
    '                flag = False
    '            End If
    '        Else
    '            If New Date(Now.Year, Now.Month, Now.Day) <> _
    '                New Date(dt.Year, dt.Month, dt.Day) Then
    '                flag = False
    '            End If
    '        End If
    '        Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
    '        If f = 0 Then
    '            Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
    '            If cnt > 0 Then
    '                flag = False
    '            End If
    '        End If
    '        If flag Then
    '            objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
    '            dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
    '        End If
    '    End If

    '    'Get book 
    '    If bookIssue Then
    '        Dim dtBook As New DataTable
    '        dtBook = objStu.GetClassBook(intSubClass)
    '        Dim lstBook As New ArrayList
    '        Dim lstBookResult As New ArrayList
    '        Dim r As Integer = 0
    '        Dim book As Integer
    '        Dim FeeClear As Byte
    '        Dim FeeClearDate As Date
    '        Dim MultiClass As Byte
    '        Dim CreateDate As Date
    '        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
    '        Dim c2 As String = c_StuIDColumnName & "='"
    '        Dim c3 As String = c_SubClassIDColumnName & "="
    '        Dim c4 As String = c_DateTimeColumnName & "> '"
    '        Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
    '        Dim c6 As String = c_ClassIDColumnName & "="
    '        Dim dateReg As Date = stuInfo.Rows(idx).Item(c_RegDateColumnName)
    '        Dim dtPay As New DataTable
    '        dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
    '        Dim intFeeOwe As Integer = stuInfo.Rows(idx).Item(c_FeeOweColumnName)
    '        Dim dtBookIssue As New DataTable
    '        Dim intClassID As Integer = GetIntValue(stuInfo.Rows(idx).Item(c_ClassIDColumnName))

    '        For i As Integer = 0 To dtBook.Rows.Count - 1
    '            FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
    '            FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
    '            MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
    '            CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
    '            lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
    '            book = dtBook.Rows(i).Item(c_IDColumnName)

    '            If CreateDate.Year <> 1911 Then
    '                If dateReg > CreateDate Then
    '                    lstBookResult.Add(1)
    '                    Continue For
    '                End If
    '            End If

    '            If FeeClear = 0 Then
    '                If FeeClearDate.Year <> 1911 Then
    '                    If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
    '                                     intSubClass.ToString & " AND " & c4 & _
    '                                        FeeClearDate.ToString & "'") > 0 Then
    '                        lstBookResult.Add(3)
    '                        Continue For
    '                    End If
    '                End If
    '                If intFeeOwe > 0 Then
    '                    lstBookResult.Add(2)
    '                    Continue For
    '                End If
    '            End If

    '            dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

    '            If MultiClass = 0 Then
    '                If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
    '                    lstBookResult.Add(4)
    '                    Continue For
    '                End If
    '            Else
    '                If dtBookIssue.Rows.Count > 0 Then
    '                    lstBookResult.Add(4)
    '                    Continue For
    '                End If
    '            End If

    '            objBook.AddBookIssueRec(stuID, punch, intClassID, book)
    '            lstBookResult.Add(0)
    '        Next

    '        Dim strBooks As String = ""
    '        Dim strBookResults As String = ""

    '        For i As Integer = 0 To lstBook.Count - 1
    '            If i = 0 Then
    '                strBooks = lstBook(i)
    '                strBookResults = lstBookResult(i)
    '            Else
    '                strBooks = strBooks & "^" & lstBook(i)
    '                strBookResults = strBookResults & "^" & lstBookResult(i)
    '            End If
    '        Next

    '        dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
    '        dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
    '    End If

    '    dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
    '    Return dtResult
    'End Function
    '----↑↑↑↑↑↑↑刷卡原始版本(沒有選擇班別視窗)
    Public Function StuPunchWithIDForIdvClass(ByVal stuID As String, ByVal punch As Date, ByVal classMode As Integer) As DataTable Implements ICsol.StuPunchWithIDForIdvClass
        Dim dtResult As New DataTable
        Dim strCardNum As String
        Dim objStu As New StuInfo

        dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
        dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
        dtResult.Rows.Add("", -1, stuID)

        Dim stuInfo As DataTable = objStu.GetStuRecById(stuID)
        If stuInfo.Rows.Count = 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
            Return dtResult
        End If

        strCardNum = stuInfo.Rows(0).Item(c_CardNumColumnName).ToString.Trim.ToUpper

        dtResult.Rows(0).Item(c_StuNameColumnName) = stuInfo.Rows(0).Item(c_NameColumnName).ToString.Trim

        AddPunchRec(stuID, punch, c_IndividualClassID, 0, 1, 0, classMode)                                  '100321

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        Return dtResult
    End Function

    Public Function StuPunchWithCardForIdvClass(ByVal card As String, ByVal punch As Date, ByVal classMode As Integer) As DataTable Implements ICsol.StuPunchWithCardForIdvClass
        Dim dtResult As New DataTable
        Dim objStu As New StuInfo

        dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
        dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
        dtResult.Rows.Add("", -1, "")

        Dim intStuIdx As Integer = -1

        Dim stuInfo As DataTable = objStu.GetStuRecByCardNum2(card)
        If stuInfo.Rows.Count = 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
            Return dtResult
        Else
            For i As Integer = 0 To stuInfo.Rows.Count - 1
                If stuInfo.Rows(i).Item(c_CardNumColumnName).ToString.Trim.ToUpper = card.ToUpper Then
                    intStuIdx = i
                    Exit For
                End If
            Next
            If intStuIdx = -1 Then
                dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
                Return dtResult
            End If
        End If

        dtResult.Rows(0).Item(c_StuIDColumnName) = stuInfo.Rows(intStuIdx).Item(c_IDColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_StuNameColumnName) = stuInfo.Rows(intStuIdx).Item(c_NameColumnName).ToString.Trim

        AddPunchRec(dtResult.Rows(0).Item(c_StuIDColumnName), punch, c_IndividualClassID, 0, 1, 0, classMode)                   '100321

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        Return dtResult
    End Function

    Public Function StuPunchWithCardstep2Helper(ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal StuInfo As DataTable, ByVal dtResult As DataTable, ByVal idx As Integer) As DataTable Implements ICsol.StuPunchWithCardstep2Helper


        Dim stuID As String = StuInfo.Rows(idx).Item(c_StuIDColumnName).ToString.Trim
        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        Dim retTB As DataTable = New DataTable

        If intWday = 0 Then
            intWday = 7
        End If

        'For i As Integer = 0 To StuInfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)

        End If
        'Next

        Dim intSession As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SessionIDColumnName))
        Dim intSubClass As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SubClassIDColumnName))

        Dim dtRepeat As New DataTable
        dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
                                            DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
        If dtRepeat.Rows.Count > 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
            'Return dtResult
            retTB.Clear()
            retTB.Columns.Clear()
            For Each column As DataColumn In dtResult.Columns
                retTB.Columns.Add(column.ColumnName)
            Next

            retTB.ImportRow(dtResult.Rows(0))

            Return retTB
        End If



        Dim intContent As Integer
        'For i As Integer = 0 To StuInfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)
            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
                    If Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentStart")) And Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentEnd")) Then
                        Dim contentStart As Date = CDate(StuInfo.Rows(idx).Item("ContentStart"))
                        Dim contentEnd As Date = CDate(StuInfo.Rows(idx).Item("ContentEnd"))
                        If contentStart <= Now And contentEnd >= Now Then
                            intContent = GetIntValue(StuInfo.Rows(idx).Item(c_ContentIDColumnName))
                            dtResult.Rows(0).Item(c_ContentColumnName) = StuInfo.Rows(idx).Item(c_ContentColumnName).ToString.Trim
                            'Exit For
                        End If
                    End If
                End If
            End If
        End If
        'Next

        AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)

        dtResult.Rows(0).Item(c_ClassNameColumnName) = StuInfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SubClassNameColumnName) = StuInfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SeatNumColumnName) = StuInfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
        dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd



        'Get Note
        Dim dtNote As New DataTable
        dtNote = objStu.GetStuNoteByID(stuID)

        Dim flag As Boolean = True
        If dtNote.Rows.Count > 0 Then
            Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag Then
                objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
                dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
            End If
        End If

        'Get book 
        'If bookIssue Then
        '    Dim dtBook As New DataTable
        '    dtBook = objStu.GetClassBook(intSubClass)
        '    Dim lstBook As New ArrayList
        '    Dim lstBookResult As New ArrayList
        '    Dim r As Integer = 0
        '    Dim book As Integer
        '    Dim FeeClear As Byte
        '    Dim FeeClearDate As Date
        '    Dim MultiClass As Byte
        '    Dim CreateDate As Date
        '    Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        '    Dim c2 As String = c_StuIDColumnName & "='"
        '    Dim c3 As String = c_SubClassIDColumnName & "="
        '    Dim c4 As String = c_DateTimeColumnName & "> '"
        '    Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
        '    Dim c6 As String = c_ClassIDColumnName & "="
        '    Dim dateReg As Date = StuInfo.Rows(idx).Item(c_RegDateColumnName)
        '    Dim dtPay As New DataTable
        '    dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
        '    Dim intFeeOwe As Integer = StuInfo.Rows(idx).Item(c_FeeOweColumnName)
        '    Dim dtBookIssue As New DataTable
        '    Dim intClassID As Integer = StuInfo.Rows(idx).Item(c_ClassIDColumnName)

        '    For i As Integer = 0 To dtBook.Rows.Count - 1
        '        FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
        '        FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
        '        MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
        '        CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
        '        lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
        '        book = dtBook.Rows(i).Item(c_IDColumnName)

        '        If CreateDate.Year <> 1911 Then
        '            If dateReg > CreateDate Then
        '                lstBookResult.Add(1)
        '                Continue For
        '            End If
        '        End If

        '        If FeeClear = 0 Then
        '            If FeeClearDate.Year <> 1911 Then
        '                If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
        '                                 intSubClass.ToString & " AND " & c4 & _
        '                                    FeeClearDate.ToString & "'") > 0 Then
        '                    lstBookResult.Add(3)
        '                    Continue For
        '                End If
        '            End If
        '            If intFeeOwe > 0 Then
        '                lstBookResult.Add(2)
        '                Continue For
        '            End If
        '        End If

        '        dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

        '        If MultiClass = 0 Then
        '            If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        Else
        '            If dtBookIssue.Rows.Count > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        End If

        '        objBook.AddBookIssueRec(stuID, punch, intClassID, book)
        '        lstBookResult.Add(0)
        '    Next

        '    Dim strBooks As String = ""
        '    Dim strBookResults As String = ""

        '    For i As Integer = 0 To lstBook.Count - 1
        '        If i = 0 Then
        '            strBooks = lstBook(i)
        '            strBookResults = lstBookResult(i)
        '        Else
        '            strBooks = strBooks & "^" & lstBook(i)
        '            strBookResults = strBookResults & "^" & lstBookResult(i)
        '        End If
        '    Next

        '    dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
        '    dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
        'End If

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        'Return dtResult
        retTB.Clear()
        retTB.Columns.Clear()
        For Each column As DataColumn In dtResult.Columns
            retTB.Columns.Add(column.ColumnName)
        Next

        retTB.ImportRow(dtResult.Rows(0))

        Return retTB
    End Function

    Public Function StuPunchWithCardstep2(ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean, ByVal StuInfo As DataTable, ByVal dtResult As DataTable, ByVal idx As Integer) As DataTable Implements ICsol.StuPunchWithCardstep2


        Dim stuID As String = StuInfo.Rows(idx).Item(c_StuIDColumnName).ToString.Trim
        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        If intWday = 0 Then
            intWday = 7
        End If

        'For i As Integer = 0 To StuInfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)

        End If
        'Next

        Dim intSession As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SessionIDColumnName))
        Dim intSubClass As Integer = GetIntValue(StuInfo.Rows(idx).Item(c_SubClassIDColumnName))

        Dim dtRepeat As New DataTable
        dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
                                            DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
        If dtRepeat.Rows.Count > 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
            Return dtResult
        End If



        Dim intContent As Integer
        'For i As Integer = 0 To StuInfo.Rows.Count - 1
        If intWday = GetIntValue(StuInfo.Rows(idx).Item(c_DayOfWeekColumnName)) Then
            dateTemp = StuInfo.Rows(idx).Item(c_StartColumnName)
            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_EndColumnName)
            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
            dateTemp = StuInfo.Rows(idx).Item(c_ClassEndColumnName)
            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
                    If Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentStart")) And Not DBNull.Value.Equals(StuInfo.Rows(idx).Item("ContentEnd")) Then
                        Dim contentStart As Date = CDate(StuInfo.Rows(idx).Item("ContentStart"))
                        Dim contentEnd As Date = CDate(StuInfo.Rows(idx).Item("ContentEnd"))
                        If contentStart <= Now And contentEnd >= Now Then
                            intContent = GetIntValue(StuInfo.Rows(idx).Item(c_ContentIDColumnName))
                            dtResult.Rows(0).Item(c_ContentColumnName) = StuInfo.Rows(idx).Item(c_ContentColumnName).ToString.Trim
                            'Exit For
                        End If
                    End If
                End If
            End If
        End If
        'Next

        AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)

        dtResult.Rows(0).Item(c_ClassNameColumnName) = StuInfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SubClassNameColumnName) = StuInfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SeatNumColumnName) = StuInfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
        dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd



        'Get Note
        Dim dtNote As New DataTable
        dtNote = objStu.GetStuNoteByID(stuID)

        Dim flag As Boolean = True
        If dtNote.Rows.Count > 0 Then
            Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag Then
                objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
                dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
            End If
        End If

        'Get book 
        'If bookIssue Then
        '    Dim dtBook As New DataTable
        '    dtBook = objStu.GetClassBook(intSubClass)
        '    Dim lstBook As New ArrayList
        '    Dim lstBookResult As New ArrayList
        '    Dim r As Integer = 0
        '    Dim book As Integer
        '    Dim FeeClear As Byte
        '    Dim FeeClearDate As Date
        '    Dim MultiClass As Byte
        '    Dim CreateDate As Date
        '    Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
        '    Dim c2 As String = c_StuIDColumnName & "='"
        '    Dim c3 As String = c_SubClassIDColumnName & "="
        '    Dim c4 As String = c_DateTimeColumnName & "> '"
        '    Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
        '    Dim c6 As String = c_ClassIDColumnName & "="
        '    Dim dateReg As Date = StuInfo.Rows(idx).Item(c_RegDateColumnName)
        '    Dim dtPay As New DataTable
        '    dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
        '    Dim intFeeOwe As Integer = StuInfo.Rows(idx).Item(c_FeeOweColumnName)
        '    Dim dtBookIssue As New DataTable
        '    Dim intClassID As Integer = StuInfo.Rows(idx).Item(c_ClassIDColumnName)

        '    For i As Integer = 0 To dtBook.Rows.Count - 1
        '        FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
        '        FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
        '        MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
        '        CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
        '        lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
        '        book = dtBook.Rows(i).Item(c_IDColumnName)

        '        If CreateDate.Year <> 1911 Then
        '            If dateReg > CreateDate Then
        '                lstBookResult.Add(1)
        '                Continue For
        '            End If
        '        End If

        '        If FeeClear = 0 Then
        '            If FeeClearDate.Year <> 1911 Then
        '                If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
        '                                 intSubClass.ToString & " AND " & c4 & _
        '                                    FeeClearDate.ToString & "'") > 0 Then
        '                    lstBookResult.Add(3)
        '                    Continue For
        '                End If
        '            End If
        '            If intFeeOwe > 0 Then
        '                lstBookResult.Add(2)
        '                Continue For
        '            End If
        '        End If

        '        dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

        '        If MultiClass = 0 Then
        '            If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        Else
        '            If dtBookIssue.Rows.Count > 0 Then
        '                lstBookResult.Add(4)
        '                Continue For
        '            End If
        '        End If

        '        objBook.AddBookIssueRec(stuID, punch, intClassID, book)
        '        lstBookResult.Add(0)
        '    Next

        '    Dim strBooks As String = ""
        '    Dim strBookResults As String = ""

        '    For i As Integer = 0 To lstBook.Count - 1
        '        If i = 0 Then
        '            strBooks = lstBook(i)
        '            strBookResults = lstBookResult(i)
        '        Else
        '            strBooks = strBooks & "^" & lstBook(i)
        '            strBookResults = strBookResults & "^" & lstBookResult(i)
        '        End If
        '    Next

        '    dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
        '    dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
        'End If

        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
        Return dtResult
    End Function

    'Public Function StuPunchWithCard(ByVal card As String, ByVal punch As Date, ByVal buffer As Integer, ByVal classMode As Integer, ByVal bookIssue As Boolean) As DataTable Implements ICsol.StuPunchWithCard
    '    Dim dtResult As New DataTable
    '    Dim stuID As String
    '    Dim ds As New DataSet
    '    Dim objStu As New StuInfo
    '    Dim objBook As New Book
    '    Dim dateTemp As Date

    '    dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
    '    dtResult.Columns.Add(c_ClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SeatNumColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_ContentColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionStartColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_SessionEndColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BooksColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_BookResultColumnName, GetType(System.String))
    '    dtResult.Columns.Add(c_NoteColumnName, GetType(System.String))
    '    '12/02 updated by Sherry Start
    '    dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
    '    dtResult.Rows.Add("", -1, "", "", "", "", "", "", "", "", "", "")
    '    '12/02 updated by Sherry End

    '    Dim stuInfo As DataTable = objStu.GetPunchStuInfoByCard(card)
    '    Dim intStuIdx As Integer = -1
    '    If stuInfo.Rows.Count = 0 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
    '        Return dtResult
    '    Else
    '        For i As Integer = 0 To stuInfo.Rows.Count - 1
    '            If stuInfo.Rows(i).Item(c_CardNumColumnName).ToString.Trim.ToUpper = card.ToUpper Then
    '                intStuIdx = i
    '                Exit For
    '            End If
    '        Next
    '        If intStuIdx = -1 Then
    '            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
    '            Return dtResult
    '        End If
    '    End If

    '    stuID = stuInfo.Rows(intStuIdx).Item(c_StuIDColumnName).ToString.Trim
    '    '12/02 updated by Sherry Start
    '    dtResult.Rows(0).Item(c_StuIDColumnName) = stuID
    '    'End
    '    dtResult.Rows(0).Item(c_StuNameColumnName) = stuInfo.Rows(intStuIdx).Item(c_StuNameColumnName).ToString.Trim

    '    Dim idx As Integer = -1
    '    Dim dateStart As Date
    '    Dim dateEnd As Date
    '    Dim intWday As Integer = punch.DayOfWeek
    '    If intWday = 0 Then
    '        intWday = 7
    '    End If

    '    For i As Integer = 0 To stuInfo.Rows.Count - 1
    '        If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
    '            dateTemp = stuInfo.Rows(i).Item(c_StartColumnName)
    '            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_EndColumnName)
    '            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)
    '            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
    '                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
    '                    idx = i
    '                    Exit For
    '                End If
    '            End If
    '        End If
    '    Next

    '    If idx = -1 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoClass
    '        Return dtResult
    '    End If

    '    Dim intSession As Integer = GetIntValue(stuInfo.Rows(idx).Item(c_SessionIDColumnName))
    '    Dim intSubClass As Integer = GetIntValue(stuInfo.Rows(idx).Item(c_SubClassIDColumnName))

    '    Dim dtRepeat As New DataTable
    '    dtRepeat = objStu.GetRepeatPunchRec(stuID, intSubClass, DateAdd(DateInterval.Minute, -buffer, dateStart), _
    '                                        DateAdd(DateInterval.Minute, buffer, dateEnd), intSession, classMode)
    '    If dtRepeat.Rows.Count > 0 Then
    '        dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultRepeat
    '        Return dtResult
    '    End If

    '    Dim intContent As Integer
    '    For i As Integer = 0 To stuInfo.Rows.Count - 1
    '        If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
    '            dateTemp = stuInfo.Rows(i).Item(c_StartColumnName)
    '            dateStart = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_EndColumnName)
    '            dateEnd = New Date(punch.Year, punch.Month, punch.Day, dateTemp.Hour, dateTemp.Minute, dateTemp.Second)
    '            dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)
    '            If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
    '                If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
    '                    If Not DBNull.Value.Equals(stuInfo.Rows(i).Item("ContentStart")) And Not DBNull.Value.Equals(stuInfo.Rows(i).Item("ContentEnd")) Then
    '                        Dim contentStart As Date = CDate(stuInfo.Rows(i).Item("ContentStart"))
    '                        Dim contentEnd As Date = CDate(stuInfo.Rows(i).Item("ContentEnd"))
    '                        If contentStart <= Now And contentEnd >= Now Then
    '                            intContent = GetIntValue(stuInfo.Rows(i).Item(c_ContentIDColumnName))
    '                            dtResult.Rows(0).Item(c_ContentColumnName) = stuInfo.Rows(i).Item(c_ContentColumnName).ToString.Trim
    '                            Exit For
    '                        End If
    '                    End If
    '                End If
    '            End If
    '        End If
    '    Next

    '    AddPunchRec(stuID, punch, intSubClass, intContent, 0, intSession, classMode)

    '    dtResult.Rows(0).Item(c_ClassNameColumnName) = stuInfo.Rows(idx).Item(c_ClassNameColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuInfo.Rows(idx).Item(c_SubClassNameColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SeatNumColumnName) = stuInfo.Rows(idx).Item(c_SeatNumColumnName).ToString.Trim
    '    dtResult.Rows(0).Item(c_SessionStartColumnName) = dateStart
    '    dtResult.Rows(0).Item(c_SessionEndColumnName) = dateEnd



    '    'Get Note
    '    Dim dtNote As New DataTable
    '    dtNote = objStu.GetStuNoteByID(stuID)

    '    Dim flag As Boolean = True
    '    If dtNote.Rows.Count > 0 Then
    '        Dim repeat As Byte = dtNote.Rows(0).Item(c_RepeatBfDateColumnName)
    '        Dim dt As Date = dtNote.Rows(0).Item(c_DisplayDateColumnName)
    '        If repeat = 1 Then
    '            If New Date(Now.Year, Now.Month, Now.Day) > _
    '                New Date(dt.Year, dt.Month, dt.Day) Then
    '                flag = False
    '            End If
    '        Else
    '            If New Date(Now.Year, Now.Month, Now.Day) <> _
    '                New Date(dt.Year, dt.Month, dt.Day) Then
    '                flag = False
    '            End If
    '        End If
    '        Dim f As Byte = dtNote.Rows(0).Item(c_FrequencyColumnName)
    '        If f = 0 Then
    '            Dim cnt As Integer = dtNote.Rows(0).Item(c_NoteRecCntColumnName)
    '            If cnt > 0 Then
    '                flag = False
    '            End If
    '        End If
    '        If flag Then
    '            objStu.AddStuNoteReceive(dtNote.Rows(0).Item(c_IDColumnName), Now, 0)
    '            dtResult.Rows(0).Item(c_NoteColumnName) = dtNote.Rows(0).Item(c_ContentColumnName).ToString.Trim
    '        End If
    '    End If

    '    'Get book 
    '    If bookIssue Then
    '        Dim dtBook As New DataTable
    '        dtBook = objStu.GetClassBook(intSubClass)
    '        Dim lstBook As New ArrayList
    '        Dim lstBookResult As New ArrayList
    '        Dim r As Integer = 0
    '        Dim book As Integer
    '        Dim FeeClear As Byte
    '        Dim FeeClearDate As Date
    '        Dim MultiClass As Byte
    '        Dim CreateDate As Date
    '        Dim c1 As String = "COUNT(" + c_StuIDColumnName + ")"
    '        Dim c2 As String = c_StuIDColumnName & "='"
    '        Dim c3 As String = c_SubClassIDColumnName & "="
    '        Dim c4 As String = c_DateTimeColumnName & "> '"
    '        Dim c5 As String = "COUNT(" + c_ClassIDColumnName + ")"
    '        Dim c6 As String = c_ClassIDColumnName & "="
    '        Dim dateReg As Date = stuInfo.Rows(idx).Item(c_RegDateColumnName)
    '        Dim dtPay As New DataTable
    '        dtPay = objStu.GetStuPayRec2(stuID, intSubClass)
    '        Dim intFeeOwe As Integer = stuInfo.Rows(idx).Item(c_FeeOweColumnName)
    '        Dim dtBookIssue As New DataTable
    '        Dim intClassID As Integer = stuInfo.Rows(idx).Item(c_ClassIDColumnName)

    '        For i As Integer = 0 To dtBook.Rows.Count - 1
    '            FeeClear = dtBook.Rows(i).Item(c_FeeClearColumnName)
    '            FeeClearDate = dtBook.Rows(i).Item(c_FeeClearDateColumnName)
    '            MultiClass = dtBook.Rows(i).Item(c_MultiClassColumnName)
    '            CreateDate = dtBook.Rows(i).Item(c_CreateDateColumnName)
    '            lstBook.Add(dtBook.Rows(i).Item(c_NameColumnName))
    '            book = dtBook.Rows(i).Item(c_IDColumnName)

    '            If CreateDate.Year <> 1911 Then
    '                If dateReg > CreateDate Then
    '                    lstBookResult.Add(1)
    '                    Continue For
    '                End If
    '            End If

    '            If FeeClear = 0 Then
    '                If FeeClearDate.Year <> 1911 Then
    '                    If dtPay.Compute(c1, c2 & stuID & "' AND " & c3 & _
    '                                     intSubClass.ToString & " AND " & c4 & _
    '                                        FeeClearDate.ToString & "'") > 0 Then
    '                        lstBookResult.Add(3)
    '                        Continue For
    '                    End If
    '                End If
    '                If intFeeOwe > 0 Then
    '                    lstBookResult.Add(2)
    '                    Continue For
    '                End If
    '            End If

    '            dtBookIssue = objBook.GetBookIssueRecByStuBook(stuID, book)

    '            If MultiClass = 0 Then
    '                If dtBookIssue.Compute(c5, c6 & intClassID.ToString) > 0 Then
    '                    lstBookResult.Add(4)
    '                    Continue For
    '                End If
    '            Else
    '                If dtBookIssue.Rows.Count > 0 Then
    '                    lstBookResult.Add(4)
    '                    Continue For
    '                End If
    '            End If

    '            objBook.AddBookIssueRec(stuID, punch, intClassID, book)
    '            lstBookResult.Add(0)
    '        Next

    '        Dim strBooks As String = ""
    '        Dim strBookResults As String = ""

    '        For i As Integer = 0 To lstBook.Count - 1
    '            If i = 0 Then
    '                strBooks = lstBook(i)
    '                strBookResults = lstBookResult(i)
    '            Else
    '                strBooks = strBooks & "^" & lstBook(i)
    '                strBookResults = strBookResults & "^" & lstBookResult(i)
    '            End If
    '        Next

    '        dtResult.Rows(0).Item(c_BooksColumnName) = strBooks
    '        dtResult.Rows(0).Item(c_BookResultColumnName) = strBookResults
    '    End If

    '    dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultSuccess
    '    Return dtResult
    'End Function

    Public Function StuPunchWithCard(ByVal card As String, ByVal punch As Date, ByVal buffer As Integer, _
                                     ByVal classMode As Integer, ByVal bookIssue As Boolean, _
                                     ByRef LS As ArrayList, ByRef stuInfo As DataTable) As DataTable Implements ICsol.StuPunchWithCard


        Dim dtResult As New DataTable
        Dim stuID As String
        Dim ds As New DataSet
        Dim objStu As New StuInfo
        Dim objBook As New Book
        Dim dateTemp As Date
        Dim dateTemp2 As Date

        dtResult.Columns.Add(c_StuNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_PunchResultColumnName, GetType(System.Int32))
        dtResult.Columns.Add(c_ClassNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SubClassNameColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SeatNumColumnName, GetType(System.String))
        dtResult.Columns.Add(c_ContentColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SessionStartColumnName, GetType(System.String))
        dtResult.Columns.Add(c_SessionEndColumnName, GetType(System.String))
        dtResult.Columns.Add(c_BooksColumnName, GetType(System.String))
        dtResult.Columns.Add(c_BookResultColumnName, GetType(System.String))
        dtResult.Columns.Add(c_NoteColumnName, GetType(System.String))
        dtResult.Columns.Add(c_StuIDColumnName, GetType(System.String))
        dtResult.Rows.Add("", -1, "", "", "", "", "", "", "", "", "", "")

        stuInfo = objStu.GetPunchStuInfoByCard(card)
        If stuInfo.Rows.Count = 0 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoStu
            Return dtResult
        End If

        stuID = stuInfo.Rows(0).Item(c_StuIDColumnName).ToString.Trim
        dtResult.Rows(0).Item(c_StuIDColumnName) = stuID
        dtResult.Rows(0).Item(c_StuNameColumnName) = stuInfo.Rows(0).Item(c_StuNameColumnName).ToString.Trim
        Dim idx As Integer = -1
        Dim str As String
        Dim dateStart As Date
        Dim dateEnd As Date
        Dim intWday As Integer = punch.DayOfWeek
        If intWday = 0 Then
            intWday = 7
        End If
        LS.Clear()
        For i As Integer = 0 To stuInfo.Rows.Count - 1
            If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
                dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)
                If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
                    If IsDBNull(stuInfo.Rows(i).Item(c_ContentStartColumnName)) Then
                        dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
                        'dtResult.Rows(0).Item(c_ClassNameColumnName) = stuInfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
                        'dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuInfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim
                        Return dtResult
                    Else
                        dateTemp = stuInfo.Rows(i).Item(c_ContentStartColumnName)
                        dateTemp2 = stuInfo.Rows(i).Item(c_StartColumnName)
                        dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
                        dateTemp = stuInfo.Rows(i).Item(c_ContentEndColumnName)
                        dateTemp2 = stuInfo.Rows(i).Item(c_EndColumnName)
                        dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

                        If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
                            idx = i
                            str = i
                            LS.AddRange(str.Split(","))
                        End If
                    End If
                End If
            End If
        Next

        'For i As Integer = 0 To stuInfo.Rows.Count - 1
        '    If intWday = GetIntValue(stuInfo.Rows(i).Item(c_DayOfWeekColumnName)) Then
        '        dateTemp = stuInfo.Rows(i).Item(c_ClassEndColumnName)


        '        If New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, 23, 59, 59) >= Now Then
        '            If IsDBNull(stuInfo.Rows(i).Item(c_ContentStartColumnName)) Then
        '                dtResult.Rows(0).Item(c_PunchResultColumnName) = 6
        '                dtResult.Rows(0).Item(c_ClassNameColumnName) = stuInfo.Rows(i).Item(c_ClassNameColumnName).ToString.Trim
        '                dtResult.Rows(0).Item(c_SubClassNameColumnName) = stuInfo.Rows(i).Item(c_SubClassNameColumnName).ToString.Trim
        '                Return dtResult
        '            End If

        '            dateTemp = stuInfo.Rows(i).Item(c_ContentStartColumnName)
        '            dateTemp2 = stuInfo.Rows(i).Item(c_StartColumnName)
        '            dateStart = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)
        '            dateTemp = stuInfo.Rows(i).Item(c_ContentEndColumnName)
        '            dateTemp2 = stuInfo.Rows(i).Item(c_EndColumnName)
        '            dateEnd = New Date(dateTemp.Year, dateTemp.Month, dateTemp.Day, dateTemp2.Hour, dateTemp2.Minute, dateTemp2.Second)

        '            If DateAdd(DateInterval.Minute, -buffer, dateStart) <= punch And DateAdd(DateInterval.Minute, buffer, dateEnd) >= punch Then
        '                idx = i
        '                str = i
        '                LS.AddRange(str.Split(","))

        '                'If i = stuInfo.Rows.Count - 1 Then
        '                '    Exit For
        '                'End If
        '            End If
        '        End If
        '    End If
        'Next

        If idx = -1 Then
            dtResult.Rows(0).Item(c_PunchResultColumnName) = c_PunchResultNoClass
            Return dtResult
        End If
        Return dtResult
    End Function



    Public Sub UpdPunchRec(ByVal ID As Integer, _
                 ByVal DateTime1 As DateTime, ByVal InType As Integer) Implements ICsol.UpdPunchRec
        Dim obj As New StuInfo
        obj.UpdPunchRec(ID, DateTime1, InType)
    End Sub


    Public Function GetPunchRecByName(ByVal strName As String) As DataTable Implements ICsol.GetPunchRecByName
        Dim obj As New StuInfo
        Return obj.GetPunchRecByName(strName)
    End Function

    Public Function GetPunchRecByContent(ByVal subclass As String, ByVal content As String) As List(Of String) Implements ICsol.GetPunchRecByContent
        Dim obj As New StuInfo
        Return obj.GetPunchRecByContent(subclass, content)
    End Function


    Public Overloads Sub AddBookIssueRec(ByVal dtRec As DataTable, ByVal ClassID As Integer, ByVal BookID As ArrayList) Implements ICsol.AddBookIssueRec
        Dim obj As New Book
        Dim bid As Integer
        For index As Integer = 0 To BookID.Count - 1
            bid = BookID(index)
            For Each row In dtRec.Rows
                obj.AddBookIssueRec(row.item(c_StuIDColumnName), row.item(c_DateTimeColumnName), ClassID, bid)
            Next
        Next

    End Sub
    'Public Overloads Sub AddBookIssueRec(ByVal stuid As String, ByVal ClassID As Integer, ByVal BookID As ArrayList, ByVal DateTime As Date) Implements ICsol.AddBookIssueRec
    '    Dim obj As New Book
    '    Dim bid As Integer
    '    For index As Integer = 0 To bookid.Count - 1
    '        bid = bookid(index)
    '        obj.AddBookIssueRec(stuid, DateTime, ClassID, bid)
    '    Next

    'End Sub


    Public Overloads Sub AddBookIssueRec(ByVal stuid As String, ByVal ClassID As Integer, ByVal BookID As ArrayList, ByVal DateTime As Date) Implements ICsol.AddBookIssueRec
        Dim obj As New Book
        Dim bid As Integer
        For index As Integer = 0 To BookID.Count - 1
            bid = BookID(index)
            obj.AddBookIssueRec(stuid, DateTime, ClassID, bid)
        Next


    End Sub


    Public Overloads Function AddBookIssueRec(ByVal stuID As String, _
       ByVal ClassID As Integer, _
       ByVal BookID As ArrayList, _
           ByVal DateTime As Date, ByVal MultiClass As ArrayList) As Integer Implements ICsol.AddBookIssueRec
        Dim obj As New Book
        Dim bid As Integer
        Dim dt As DataTable
        Dim r As Integer = 0


        For index As Integer = 0 To BookID.Count - 1
            bid = BookID(index)
            dt = obj.GetBookIssueRecByStuBook(stuID, bid)
            If MultiClass(index) = 0 Then
                For j As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).Item(c_ClassIDColumnName) = ClassID Then
                        r = 4

                        Return r
                    End If
                Next
            Else
                If dt.Rows.Count > 0 Then
                    r = 4

                    Return r
                End If
            End If
        Next

        For index As Integer = 0 To BookID.Count - 1
            bid = BookID(index)
            obj.AddBookIssueRec(stuID, DateTime, ClassID, bid)
        Next

        Return r
    End Function
    Public Overloads Function AddBookIssueRec2(ByVal stuID As String, ByVal ClassID As Integer, ByVal BookID As Integer, _
       ByVal DateTime As Date, ByVal MultiClass As ArrayList) As Integer Implements ICsol.AddBookIssueRec2
        Dim obj As New Book
        Dim bid As Integer
        Dim dt As DataTable
        Dim r As Integer = 0


        For index As Integer = 0 To MultiClass.Count - 1
            bid = BookID
            dt = obj.GetBookIssueRecByStuBook(stuID, bid)
            If MultiClass(index) = 0 Then
                For j As Integer = 0 To dt.Rows.Count - 1
                    If dt.Rows(j).Item(c_ClassIDColumnName) = ClassID Then

                        r = 4
                        Return r
                    End If
                Next
            Else
                If dt.Rows.Count > 0 Then
                    r = 4
                    Return r
                End If
            End If
        Next


        bid = BookID
        obj.AddBookIssueRec(stuID, DateTime, ClassID, bid)


        Return r
    End Function

    Public Function AddSubClass(ByVal Name As String, ByVal ClassID As Integer, _
                         ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                         ByVal ClassRoomID As Integer, ByVal usrId As String) As Integer Implements ICsol.AddSubClass
        Dim objClass As New ClassInfo
        Return objClass.AddSubClass(Name, ClassID, Sequence, SubjectID, ClassRoomID, usrId)
    End Function

    Public Function AddBook(ByVal Name As String, ByVal Stock As Integer, _
                          ByVal Price As Integer, ByVal BeforeDate As Date, _
                          ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                          ByVal MultiClass As Byte, ByVal CreateDate As Date, _
                          ByVal lstSubClass As ArrayList) As Integer Implements ICsol.AddBook
        Dim objBook As New Book
        Dim i As Integer
        i = objBook.AddBook(Name, Stock, Price, BeforeDate, FeeClear, FeeClearDate, _
                               MultiClass, CreateDate)
        For index As Integer = 0 To lstSubClass.Count - 1
            objBook.AddClassBook(i, lstSubClass(index))
        Next
        Return i
    End Function

    Public Sub UpdateBook(ByVal ID As Integer, _
                          ByVal Name As String, ByVal Stock As Integer, _
                          ByVal Price As Integer, ByVal BeforeDate As Date, _
                          ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                          ByVal MultiClass As Byte, ByVal CreateDate As Date, _
                          ByVal lstSubClass As ArrayList) Implements ICsol.UpdateBook
        Dim objBook As New Book

        objBook.UpdateBook(ID, Name, Stock, Price, BeforeDate, FeeClear, FeeClearDate, _
                               MultiClass, CreateDate)
        objBook.DeleteClassBook(ID)
        For index As Integer = 0 To lstSubClass.Count - 1
            objBook.AddClassBook(ID, lstSubClass(index))
        Next

    End Sub

    Public Sub CancelStuClass(ByVal subClassID As Integer, ByVal stuID As String, _
                              ByVal cancel As Byte) Implements ICsol.CancelStuClass
        Dim obj As New StuInfo
        obj.CancelStuClass(subClassID, stuID, cancel)
    End Sub

    Public Sub ChangeStuClass(ByVal newClassID As Integer, _
                       ByVal oldClassID As Integer, ByVal newSeat As String, _
                       ByVal oldSeat As String, ByVal stuID As String) Implements ICsol.ChangeStuClass
        Dim obj As New StuInfo
        newSeat = newSeat.Trim
        oldSeat = oldSeat.Trim
        obj.ChangeStuClass(newClassID, oldClassID, newSeat, oldSeat, stuID)
        If oldSeat = "" And Not newSeat = "" Then
            obj.AddSeatTk(stuID, newClassID, newSeat)
        ElseIf Not oldSeat = "" And newSeat = "" Then
            obj.DeleteSeatTk(stuID, oldClassID, oldSeat)
        ElseIf Not oldSeat = "" And Not newSeat = "" Then
            obj.ChangeSeatTk(newClassID, oldClassID, newSeat, oldSeat, stuID)
        End If
    End Sub

    Public Sub ChangeStuSales(ByVal ID As Integer, ByVal sales As String) Implements ICsol.ChangeStuSales
        Dim obj As New StuInfo
        obj.ChangeStuSales(ID, sales)
    End Sub

    Public Sub DeleteStuClass(ByVal Id As Integer) Implements ICsol.DeleteStuClass
        Dim obj As New StuInfo
        obj.DelStuClass(Id)
    End Sub

    Public Function ChangeStuID(ByVal oldID As String, ByVal newID As String, _
                    ByVal user As String) As Integer Implements ICsol.ChangeStuID
        Dim obj As New StuInfo
        Dim dt As New DataTable
        Dim dt2 As New DataTable
        Dim result As Integer = -1 'id taken
        dt = obj.GetStuRecById(newID)
        dt2 = obj.GetStuRecById(oldID)
        If dt.Rows.Count = 0 Then
            If dt2.Rows.Count > 0 Then
                obj.ChangeStuID(oldID, newID, user)
                result = 1 'success
            Else
                result = 0 'no old id exist
            End If
        End If
        Return result
    End Function

    Public Function ChgUsrPwd(ByVal UsrId As String, ByVal OldPwd As String, ByVal NewPwd As String) As Integer Implements ICsol.ChgUsrPwd
        Dim result As Integer = 0
        Dim obj As New Admin
        Dim dt As DataTable = obj.GetUsrPWD(UsrId)
        If dt.Rows.Count > 0 Then
            Dim s As String = dt.Rows(0).Item(c_PWDColumnName).trim
            If s = OldPwd.Trim Then
                obj.ModifyUsrPwd(UsrId, NewPwd.Trim)
                result = 1
            End If
        End If
        Return result
    End Function

    Public Sub DeleteClass(ByVal classId As Integer) Implements ICsol.DeleteClass
        Dim obj As New ClassInfo
        obj.DeleteClass(classId)
    End Sub

    Public Sub DeleteBook(ByVal id As Integer) Implements ICsol.DeleteBook
        Dim obj As New Book
        obj.DeleteBook(id)
    End Sub

    Public Sub UpdateBookQ(ByVal ID As Integer, ByVal Stock As Integer) Implements ICsol.UpdateBookQ
        Dim obj As New Book
        obj.UpdateBookQ(ID, Stock)
    End Sub

    Public Sub DeleteClassContent(ByVal contId As Integer) Implements ICsol.DeleteClassContent
        Dim obj As New ClassInfo
        obj.DeleteContent(contId)
        obj.DeleteContentVideo(contId)
        obj.DeleteScContent(contId)
    End Sub

    Public Sub DeleteClassRoom(ByVal rmId As Integer) Implements ICsol.DeleteClassRoom
        Dim obj As New ClassInfo
        obj.DeleteClassRoom(rmId)
    End Sub

    Public Sub DeleteClassSbj(ByVal classSbjId As Integer) Implements ICsol.DeleteClassSbj

    End Sub

    Public Overloads Sub DeletePayRec(ByVal ID As Integer) Implements ICsol.DeletePayRec
        Dim obj As New StuInfo

        obj.DeletePay(ID)
    End Sub

    Public Overloads Sub DeletePayRec(ByVal ID As Integer, ByVal stuid As String, ByVal scid As Integer, ByVal amount As Integer, _
                                      ByVal backAmount As Integer, ByVal keepAmount As Integer) Implements ICsol.DeletePayRec
        Dim obj As New StuInfo
        Dim updAmount As Integer = amount - backAmount
        If Not backAmount = 0 Then
            obj.UpdStuOwe(stuid, updAmount)
            obj.UpdStuClassOwe(stuid, scid, updAmount)
        Else
            obj.UpdStuOwe(stuid, amount)
            obj.UpdStuClassOwe(stuid, scid, amount)
        End If
        If backAmount = 0 And keepAmount = 0 Then
            obj.DeletePay(ID)
        ElseIf Not backAmount = 0 Then
            obj.DeleteMoneyBackRec(ID)
        ElseIf Not keepAmount = 0 Then
            obj.DeleteMoneyKeepRec(ID)
        End If

    End Sub
    Public Overloads Sub DeletePayRec(ByVal ID As Integer, ByVal stuid As String, ByVal scid As Integer, ByVal amount As Integer) Implements ICsol.DeletePayRec
        Dim obj As New StuInfo
        obj.UpdStuOwe(stuid, amount)
        obj.UpdStuClassOwe(stuid, scid, amount)
        obj.DeletePay(ID)
    End Sub

    Public Sub DeleteDisc(ByVal ID As Integer, ByVal stuid As String, _
                     ByVal scid As Integer, ByVal Discount As Integer) Implements ICsol.DeleteDisc
        Dim obj As New StuInfo
        obj.UpdStuOwe(stuid, Discount)
        obj.UpdStuClassOwe(stuid, scid, Discount)
        obj.DeleteDisc(ID)
    End Sub
    Public Sub DeleteDisc(ByVal ID As Integer) Implements ICsol.DeleteDisc                      '100304
        Dim obj As New StuInfo
        obj.DeleteDisc(ID)
    End Sub                                                                                     '100304

    Public Sub DeleteMB(ByVal ID As Integer) Implements ICsol.DeleteMB
        Dim obj As New StuInfo
        Dim dt As New DataTable
        Dim dtMBDetails As New DataTable
        dt = obj.GetMBRecById(ID)
        If dt.Rows.Count > 0 Then
            Dim stuid1 As String = dt.Rows(0).Item(c_StuIDColumnName)
            Dim scid As Integer = dt.Rows(0).Item(c_SubClassIDColumnName)
            Dim diff As Integer = dt.Rows(0).Item(c_AmountColumnName)

            obj.UpdStuOwe(stuid1, -diff)
            obj.UpdStuClassOwe(stuid1, scid, -diff)
        End If
        dtMBDetails = obj.GetMBDetails(ID)
        Dim ReceiptNum As String = dtMBDetails.Rows(0).Item("ReceiptNum").ToString
        Dim StuID As String = dtMBDetails.Rows(0).Item("StuID").ToString
        Dim SubClassID As Integer = CInt(dtMBDetails.Rows(0).Item("SubClassID"))
        Dim Amount As Integer = CInt(dtMBDetails.Rows(0).Item("PayAmount"))
        Dim DateTime As Date = dtMBDetails.Rows(0).Item("DateTime")
        Dim HandlerID As String = dtMBDetails.Rows(0).Item("HandlerID").ToString
        Dim SalesID As String = dtMBDetails.Rows(0).Item("SalesID").ToString
        Dim PayMethod As Integer = CInt(dtMBDetails.Rows(0).Item("PayMethod"))
        Dim Extra As String = dtMBDetails.Rows(0).Item("Extra").ToString
        Dim Remarks As String = dtMBDetails.Rows(0).Item("Remarks").ToString
        Dim ChequeValid As Date = dtMBDetails.Rows(0).Item("DateTime")
        obj.AddPay(ReceiptNum, StuID, SubClassID, Amount, DateTime, HandlerID, SalesID, PayMethod, Extra, Remarks, ChequeValid, DateTime)
        obj.DeleteMoneyBackRec(ID)
    End Sub

    Public Sub DeleteClassSession(ByVal classSessionId As Integer) Implements ICsol.DeleteClassSession
        Dim obj As New ClassInfo
        obj.DeleteSession(classSessionId)
    End Sub

    Public Sub DeleteDiscountDict(ByVal dictId As Integer) Implements ICsol.DeleteDiscountDict

    End Sub

    Public Sub DeleteListReceiptNoteDict(ByVal dictId As Integer) Implements ICsol.DeleteListReceiptNoteDict

    End Sub

    Public Sub DeleteSchool(ByVal schoolId As Integer) Implements ICsol.DeleteSchool
        Dim obj As New ClassInfo
        obj.DeleteSchool(schoolId)
    End Sub

    Public Sub DeleteStudent(ByVal ID As String) Implements ICsol.DeleteStudent
        Dim obj As New StuInfo
        obj.DelStuByID(ID)
    End Sub

    Public Sub DeleteSubClass(ByVal subClassId As Integer) Implements ICsol.DeleteSubClass
        Dim obj As New ClassInfo
        obj.DeleteSubClass(subClassId)
    End Sub

    Public Sub ImportSchools(ByVal schoolDt As System.Data.DataTable) Implements ICsol.ImportSchools

    End Sub

    Public Sub ImportStuInfo(ByVal stus As System.Collections.ArrayList) Implements ICsol.ImportStuInfo

    End Sub

    Public Function ListBookIssueRec(ByVal stuId As String) As DataTable Implements ICsol.ListBookIssueRec
        Dim obj As New StuInfo
        Return obj.GetBookRecByStuId(stuId)
    End Function
    Public Function ListBookIssueRec2(ByVal stuId As String) As DataTable Implements ICsol.ListBookIssueRec2                '100224
        Dim obj As New StuInfo
        Return obj.GetBookRecByStuId2(stuId)
    End Function                                                                                                            '100224
    Public Function GetStuNoBookRec(ByVal stuId As String) As DataTable Implements ICsol.GetStuNoBookRec                '100224
        Dim obj As New StuInfo
        Return obj.GetStuNoBookRec(stuId)
    End Function

    '20100205 updated by sherry start
    Public Function GetBookRecByUsr(ByVal ClassId As Integer, ByVal BookId As Integer) As DataTable Implements ICsol.GetBookRecByUsr
        Dim obj As New Book
        Return obj.GetBookRecByUsr(ClassId, BookId)
    End Function
    '20100205 updated by sherry end

    Public Function ListChangeClassRec(ByVal stuId As String) As DataTable Implements ICsol.ListChangeClassRec
        Dim obj As New StuInfo
        Return obj.GetStuClassChangeRec(stuId)
    End Function

    Public Function SearchStu(ByVal method As Integer, ByVal kw As String, _
                                 ByVal type As Byte) As DataTable Implements ICsol.SearchStu
        Dim obj As New StuInfo
        Return obj.GetStuSearch(method, kw, type)
    End Function

    Public Function SearchStu(ByVal method As Integer, ByVal kw As String, _
                             ByVal type As Byte, ByVal int As Integer) As DataTable Implements ICsol.SearchStu
        Dim obj As New StuInfo
        Return obj.GetStuSearch(method, kw, type, -1)
    End Function

    Public Function SearchStu_Short(ByVal method As Integer, ByVal kw As String, _
                             ByVal type As Byte) As DataTable Implements ICsol.SearchStu_Short
        Dim obj As New StuInfo
        Return obj.GetStuSearch_Short(method, kw, type)
    End Function

    Public Function ListClassContents(ByVal usrId As String) As System.Data.DataTable Implements ICsol.ListClassContents
        Dim obj As New ClassInfo
        Return obj.GetContentListByUsr(usrId)

    End Function

    Public Function ListClassContents() As System.Data.DataTable Implements ICsol.ListClassContents
        Dim obj As New ClassInfo
        Return obj.GetContentList()

    End Function

    Public Function ListClassBooks() As System.Data.DataTable Implements ICsol.ListClassBooks
        'Dim obj As New ClassInfo
        'Return obj.GetBookListByUsr(usrId)
        Return DataBaseAccess.ClassInfo.GetClassBookListByUsr()
    End Function

    Public Function ListBooks(ByVal usrId As String) As System.Data.DataTable Implements ICsol.ListBooks
        'Dim obj As New ClassInfo
        'Return obj.GetBookListByUsr(usrId)
        Return DataBaseAccess.ClassInfo.GetBookListByUsr(usrId)
    End Function

    Public Function ListBooksWhithoutUsrID() As System.Data.DataTable Implements ICsol.ListBooksWhithoutUsrID
        Dim obj As New ClassInfo
        Return obj.GetBookListWithoutUsrID

    End Function

    Public Function ListBooksByClassID(ByVal userId As String, ByVal ClassId As Integer) As System.Data.DataTable Implements ICsol.ListBookksByClassId
        Dim obj As New ClassInfo
        Return obj.GetBookByClassID(userId, ClassId)

    End Function

    Public Function GetClassPaper(ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.GetClassPaper
        Dim obj As New ExamPaper
        Dim dt As DataTable

        If classId.Count > 1 Then
            dt = obj.GetClassPaper(classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(obj.GetClassPaper(classId(index)))
            Next
        Else
            dt = obj.GetClassPaper(classId(0))
        End If

        Return dt

    End Function

    Public Function GetClassPaperList(ByVal classId As Integer) As System.Data.DataTable Implements ICsol.GetClassPaperList
        Dim obj As New ExamPaper

        Return obj.GetClassPaperList(classId)

    End Function

    Public Function ListClassSta(ByVal sc As ArrayList) As System.Data.DataTable Implements ICsol.ListClassSta
        Dim obj As New ClassInfo
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetClassStaList(sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetClassStaList(sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetClassStaList(sc(0))
        Else
            dt = obj.GetClassStaList(-1)
        End If

        Return dt
    End Function

    Public Function ListClassSta(ByVal sc As ArrayList, ByVal startDate As Date, ByVal endDate As Date) As DataTable Implements ICsol.ListClassStaByDate
        Dim obj As New ClassInfo
        Dim dt As New DataTable

        If sc.Count > 1 Then
            dt = obj.GetClassStaListByData(sc(0), startDate, endDate)
            For i As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetClassStaListByData(sc(i), startDate, endDate))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetClassStaListByData(sc(0), startDate, endDate)
        Else
            dt = obj.GetClassStaListByData(-1, startDate, endDate)
        End If

        Return dt
    End Function

    Public Function ListSubClassSta(ByVal sc As ArrayList) As System.Data.DataTable Implements ICsol.ListSubClassSta
        Dim obj As New ClassInfo
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetSubClassStaList(sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetSubClassStaList(sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetSubClassStaList(sc(0))
        Else
            dt = obj.GetSubClassStaList(-1)
        End If

        Return dt
    End Function

    Public Function ListClasseSbjs() As System.Data.DataTable Implements ICsol.ListClasseSbjs
        Return Nothing
    End Function

    Public Function ListClassRooms() As System.Data.DataSet Implements ICsol.ListClassRooms
        Dim objDb As New ClassInfo
        Dim ds As New DataSet()

        Dim dt1 As DataTable = objDb.GetClassRoomList()
        dt1.TableName = c_ClassRoomListTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = objDb.GetClassRoomUsageList
        dt2.TableName = c_ClassRoomUsageListTableName
        ds.Tables.Add(dt2)

        Return ds
    End Function

    Public Function ListClassSessions(ByVal usrId As String) As System.Data.DataTable Implements ICsol.ListClassSessions
        Dim obj As New ClassInfo
        Return obj.GetSessionListByUsr(usrId)
    End Function

    Public Function ListContentVideo(ByVal contId As Integer) As System.Data.DataTable Implements ICsol.ListContentVideo
        Dim obj As New ClassInfo
        Return obj.GetContentVideo(contId)
    End Function

    Public Function ListDiscountDict() As DataTable Implements ICsol.ListDiscountDict
        Dim obj As New Accounting
        Return obj.GetReceiptDict
    End Function

    Public Function ListTeleInterviewType() As DataTable Implements ICsol.ListTeleInterviewType
        Dim obj As New StuInfo
        Return obj.GetTeleInterviewType
    End Function

    Public Function ListDiscountDictType() As DataTable Implements ICsol.ListDiscountDictType
        Dim obj As New Accounting
        Return obj.GetReceiptDictType
    End Function

    Public Function ListNameRepeat() As System.Data.DataTable Implements ICsol.ListNameRepeat
        Return Nothing
    End Function

    Public Function ListPayBackRec(ByVal stuId As String) As System.Data.DataTable Implements ICsol.ListPayBackRec
        Return Nothing
    End Function

    Public Function ListPayRec() As System.Data.DataTable Implements ICsol.ListPayRec
        Dim obj As New StuInfo
        Return obj.GetPayRec
    End Function

    Public Function ListPunchRec(ByVal stuId As String) As DataSet Implements ICsol.ListPunchRec
        Dim obj As New StuInfo
        Dim ds As New DataSet
        Dim dt1 As DataTable = obj.GetAttendanceById(stuId)
        dt1.TableName = c_AttRecTableName
        ds.Tables.Add(dt1)
        Dim dt2 As DataTable = obj.GetAttClassById(stuId)
        dt2.TableName = c_AttClassTableName
        ds.Tables.Add(dt2)
        Return ds
    End Function

    Public Function ListReceiptNoteDict() As DataTable Implements ICsol.ListReceiptNoteDict
        Dim obj As New Accounting
        Return obj.GetRemarksDict
    End Function

    Public Function ListSampleTimes() As System.Data.DataTable Implements ICsol.ListSampleTimes
        Dim obj As New ClassInfo
        Return obj.GetSampleTime

    End Function

    Public Function ListSchools() As System.Data.DataTable Implements ICsol.ListSchools
        Dim obj As New ClassInfo
        Return obj.GetSchoolList()

    End Function

    Public Function ListSeatNA(ByVal rmId As Integer) As System.Data.DataTable Implements ICsol.ListSeatNA
        Return Nothing
    End Function

    Public Function ListStuBadgeReIssue() As DataTable Implements ICsol.ListStuBadgeReIssue
        Dim obj As New StuInfo
        Return obj.GetBadgeReissueRec
    End Function

    Public Function ListStudents(ByVal classId As Integer) As System.Collections.ArrayList Implements ICsol.ListStudents
        Return Nothing
    End Function

    Public Function ListStudentSiblings() As DataTable Implements ICsol.ListStudentSiblings
        Dim obj As New StuInfo
        Return obj.GetStuSiblings
    End Function
    Public Function ListStudentSiblings(ByVal lstfilter As List(Of String)) As DataTable Implements ICsol.ListStudentSiblings
        Dim obj As New StuInfo
        Return obj.GetStuSiblings(lstfilter)
    End Function

    Public Function GetStuChangeClassList() As DataTable Implements ICsol.GetStuChangeClassList
        Dim obj As New StuInfo
        Return obj.GetStuChangeClassList
    End Function

    Public Function ListClasses(ByVal usrId As String) As System.Data.DataSet Implements ICsol.ListClasses
        Dim objDb As New ClassInfo
        Dim ds As New DataSet()

        Dim dt1 As DataTable = objDb.GetClassListByUsr(usrId)
        dt1.TableName = c_ClassListDataTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = objDb.GetSubClassListByUsr(usrId)
        dt2.TableName = c_SubClassListDataTableName
        ds.Tables.Add(dt2)

        Dim dt3 As DataTable = objDb.GetSessionListByUsr(usrId)
        dt3.TableName = c_SessionListDataTableName
        ds.Tables.Add(dt3)

        Return ds
    End Function

    Public Function ListClassInfoSet(ByVal usrId As String) As System.Data.DataSet Implements ICsol.ListClassInfoSet
        Dim obj As New ClassInfo
        Dim ds As New DataSet()

        Dim dt1 As DataTable = obj.GetClassListByUsr2(usrId)
        dt1.TableName = c_ClassListDataTableName
        ds.Tables.Add(dt1)

        Dim dt2 As DataTable = obj.GetSubClassListByUsr(usrId)
        dt2.TableName = c_SubClassListDataTableName
        ds.Tables.Add(dt2)

        Return ds
    End Function


    Public Function GetClassListByUsr(ByVal usrId As String) As DataTable Implements ICsol.GetClassListByUsr
        Dim obj As New ClassInfo
        Return obj.GetClassListByUsr2(usrId)
    End Function

    Public Function GetSubClassListByUsr(ByVal usrId As String) As DataTable Implements ICsol.GetSubClassListByUsr
        Dim obj As New ClassInfo
        Return obj.GetSubClassListByUsr2(usrId)
    End Function

    Public Function GetPunchData(ByVal strCard As String, _
                                       ByVal ClassID As Integer) As DataSet Implements ICsol.GetPunchData
        Dim ds As New DataSet
        Dim obj As New StuInfo

        Dim dt1 As DataTable
        If strCard.Trim.Length >= 1 Then
            dt1 = obj.GetStuRecByCardNum(strCard, ClassID)
        Else
            dt1 = New DataTable
        End If

        dt1.TableName = c_StuInfoTableName
        ds.Tables.Add(dt1)
        Dim dt2 As DataTable
        If strCard.Trim.Length >= 1 Then
            dt2 = obj.GetStuNoteByCard(strCard)
        Else
            dt2 = New DataTable
        End If
        Dim flag As Boolean = True
        If dt2.Rows.Count > 0 Then
            Dim repeat As Byte = dt2.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dt2.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dt2.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dt2.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag = False Then
                dt2.Clear()
            Else
                Dim id As Integer = dt2.Rows(0).Item(c_IDColumnName)
                obj.AddStuNoteReceive(id, Now, 0)
            End If
        End If
        dt2.TableName = c_StuNoteDataTableName
        ds.Tables.Add(dt2)

        Return ds
    End Function

    Public Function GetPunchDataByID2(ByVal stuID As String, _
                                       ByVal ClassID As Integer) As DataSet Implements ICsol.GetPunchDataByID2
        Dim ds As New DataSet
        Dim obj As New StuInfo

        Dim dt1 As DataTable
        If stuID.Trim.Length = 8 Then
            dt1 = obj.GetStuRecByID2(stuID, ClassID)
        Else
            dt1 = New DataTable
        End If

        dt1.TableName = c_StuInfoTableName
        ds.Tables.Add(dt1)
        Dim dt2 As DataTable
        If stuID.Trim.Length >= 1 Then
            dt2 = obj.GetStuNoteByID(stuID)
        Else
            dt2 = New DataTable
        End If
        Dim flag As Boolean = True
        If dt2.Rows.Count > 0 Then
            Dim repeat As Byte = dt2.Rows(0).Item(c_RepeatBfDateColumnName)
            Dim dt As Date = dt2.Rows(0).Item(c_DisplayDateColumnName)
            If repeat = 1 Then
                If New Date(Now.Year, Now.Month, Now.Day) > _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            Else
                If New Date(Now.Year, Now.Month, Now.Day) <> _
                    New Date(dt.Year, dt.Month, dt.Day) Then
                    flag = False
                End If
            End If
            Dim f As Byte = dt2.Rows(0).Item(c_FrequencyColumnName)
            If f = 0 Then
                Dim cnt As Integer = dt2.Rows(0).Item(c_NoteRecCntColumnName)
                If cnt > 0 Then
                    flag = False
                End If
            End If
            If flag = False Then
                dt2.Clear()
            Else
                Dim id As Integer = dt2.Rows(0).Item(c_IDColumnName)
                obj.AddStuNoteReceive(id, Now, 0)
            End If
        End If
        dt2.TableName = c_StuNoteDataTableName
        ds.Tables.Add(dt2)

        Return ds
    End Function

    Public Sub ModifyClass(ByVal ID As Integer, ByVal Name As String, _
                           ByVal dateStart As Date, _
                         ByVal dateEnd As Date, ByVal Fee As Integer) Implements ICsol.ModifyClass
        Dim obj As New ClassInfo
        obj.UpdateClass(ID, Name, dateStart, dateEnd, Fee)
    End Sub

    Public Sub ModifyClassContent(ByVal Id As Integer, _
                                  ByVal content As String, _
                               ByVal srtDate As Date, ByVal endDate As Date, _
                               ByVal subClassIDs As ArrayList) Implements ICsol.ModifyClassContent
        Dim obj As New ClassInfo
        obj.DeleteScContent(Id)
        obj.UpdContent(Id, content, srtDate, endDate)
        For index As Integer = 0 To subClassIDs.Count - 1
            obj.AddSubClassCont(Id, subClassIDs(index))
        Next
    End Sub

    Function GetClassToday(ByVal d As Date) As DataTable Implements ICsol.GetClassToday
        Dim obj As New ClassInfo
        Return obj.GetClassToday(d)
    End Function

    Public Sub ModifyClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                 ByVal Name As String, ByVal DisStyle As Integer, _
                                 ByVal lstPw As ArrayList, ByVal lstNaCol As ArrayList, _
                                 ByVal lstNaRow As ArrayList, _
                                 ByVal lstConvex As ArrayList, _
                                 ByVal id As Integer) Implements ICsol.ModifyClassRoom
        Dim objClass As New ClassInfo

        objClass.UpdateClassRoom(LineCnt, ColumnCnt, Name, DisStyle, id)

        For index As Integer = 0 To lstPw.Count - 1
            objClass.AddSeatNa(lstPw(index), 0, id, c_SeatNaStatePw)
        Next

        For index As Integer = 0 To lstConvex.Count - 1
            objClass.AddSeatNa(0, lstConvex(index), id, c_SeatNaStateConvex)
        Next

        For index As Integer = 0 To lstNaCol.Count - 1
            objClass.AddSeatNa(lstNaCol(index), lstNaRow(index), id, c_SeatNaStateNotseat)
        Next

    End Sub

    Public Sub ModifyClassSbj(ByVal classSbjId As Integer, ByVal subject As String) Implements ICsol.ModifyClassSbj

    End Sub

    Public Sub ModifyClassSession(ByVal id As Integer, ByVal SubClassID As Integer, _
                  ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                  ByVal dtEnd As Date) Implements ICsol.ModifyClassSession
        Dim obj As New ClassInfo
        obj.UpdSession(id, SubClassID, DayOfWeek, dtStart, dtEnd)
    End Sub

    Public Sub ModifyContentVideo(ByVal cId As Integer, ByVal lstId As ArrayList, _
                                 ByVal lstPath As ArrayList) Implements ICsol.ModifyContentVideo
        Dim objClass As New ClassInfo
        objClass.DeleteContentVideo(cId)
        For index As Integer = 0 To lstId.Count - 1
            objClass.AddContVideo(cId, lstId(index), lstPath(index))
        Next
    End Sub

    Public Sub ModifyDiscountDict(ByVal dictId As Integer, ByVal amount As Integer, ByVal reason As String, ByVal typeId As Integer) Implements ICsol.ModifyDiscountDict

    End Sub

    Public Sub ModifyListReceiptNoteDict(ByVal dictId As Integer, ByVal reason As String) Implements ICsol.ModifyListReceiptNoteDict

    End Sub

    Public Sub ModifySchool(ByVal schoolId As Integer, ByVal name As String, ByVal typeId As Integer, ByVal phone As String, ByVal address As String, ByVal personContact As String) Implements ICsol.ModifySchool
        Dim obj As New ClassInfo
        obj.UpdSchool(schoolId, name, typeId, phone, address, personContact)
    End Sub

    Sub ModifyStudent(ByVal ID As String, ByVal Name As String, _
                     ByVal EngName As String, ByVal Sex As String, _
                     ByVal Birthday As Date, ByVal IC As String, _
                     ByVal Address As String, _
                     ByVal PostalCode As String, ByVal Address2 As String, _
                     ByVal PostalCode2 As String, ByVal Tel1 As String, _
                     ByVal Tel2 As String, ByVal OfficeTel As String, _
                     ByVal Mobile As String, ByVal Email As String, _
                     ByVal StuType As Byte, ByVal School As String, _
                     ByVal SchoolClass As String, ByVal SchoolGrade As Integer, _
                     ByVal PrimarySch As Integer, ByVal JuniorSch As Integer, _
                     ByVal HighSch As Integer, ByVal University As Integer, _
                     ByVal CurrentSch As Integer, ByVal SchGroup As Integer, _
                     ByVal GraduateFrom As String, ByVal DadName As String, _
                     ByVal MumName As String, ByVal DadJob As String, _
                     ByVal MumJob As String, ByVal DadMobile As String, _
                     ByVal MumMobile As String, ByVal IntroID As String, _
                     ByVal IntroName As String, _
                     ByVal Remarks As String, _
                     ByVal Customize1 As String, ByVal Customize2 As String, _
                     ByVal Customize3 As String, ByVal Customize4 As String, _
                     ByVal Customize5 As String, ByVal lstSibRank As ArrayList, _
                    ByVal lstSibTypeID As ArrayList, _
                    ByVal lstSibName As ArrayList, _
                    ByVal lstSibBirth As ArrayList, _
                    ByVal lstSibSchool As ArrayList, _
                    ByVal lstSibRemarks As ArrayList, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String) Implements ICsol.ModifyStudent
        Dim objStu As New StuInfo
        objStu.UpdStudent(ID, Name, EngName, Sex, Birthday, IC, Address, PostalCode, _
                          Address2, PostalCode2, Tel1, _
                            Tel2, OfficeTel, Mobile, Email, StuType, School, _
                            SchoolClass, SchoolGrade, _
                            PrimarySch, JuniorSch, HighSch, University, _
                            CurrentSch, SchGroup, GraduateFrom, _
                            DadName, MumName, DadJob, MumJob, DadMobile, _
                            MumMobile, IntroID, IntroName, _
                            Remarks, Customize1, _
                            Customize2, Customize3, Customize4, Customize5, EnrollSch, PunchCardNum, HseeMark)

        objStu.DelSibByID(ID)
        For index = 0 To lstSibName.Count - 1
            objStu.AddStuSib(ID, lstSibRank(index), lstSibTypeID(index), _
                             lstSibName(index), lstSibBirth(index), _
                            lstSibSchool(index), lstSibRemarks(index))
        Next
    End Sub

    Public Sub ModifySubClass(ByVal id As Integer, ByVal Name As String, _
                                ByVal ClassID As Integer, _
                     ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                     ByVal ClassRoomID As Integer) Implements ICsol.ModifySubClass
        Dim obj As New ClassInfo
        obj.UpdSubClass(id, Name, ClassID, Sequence, SubjectID, ClassRoomID)
    End Sub

    Public Sub UpdateKeepPay(ByVal keepPay As System.Data.DataTable) Implements ICsol.UpdateKeepPay

    End Sub

    Public Sub UpdateMB(ByVal ID As Integer, _
                  ByVal OldAmount As Integer, ByVal Amount As Integer, _
                  ByVal UsrID As String, _
                  ByVal OldReason As String, ByVal Reason As String, _
                  ByVal ReasonMod As String) Implements ICsol.UpdateMB
        Dim obj As New StuInfo
        Dim dt As New DataTable
        dt = obj.GetMBRecById(ID)
        If dt.Rows.Count > 0 Then
            Dim stuid As String = dt.Rows(0).Item(c_StuIDColumnName)
            Dim scid As Integer = dt.Rows(0).Item(c_SubClassIDColumnName)
            Dim diff As Integer = Amount - OldAmount

            obj.UpdStuOwe(stuid, diff)
            obj.UpdStuClassOwe(stuid, scid, diff)
        End If

        obj.UpdMB(ID, OldAmount, Amount, Now, UsrID, OldReason, Reason, ReasonMod)

    End Sub

    Public Sub AddMBRemarks(ByVal ID As Integer, ByVal Remarks As String) Implements ICsol.AddMBRemarks
        Dim obj As New StuInfo
        obj.UpdMBRemarks(ID, Remarks)
    End Sub

    Public Sub UpdatePayOther(ByVal payOtherDt As System.Data.DataTable) Implements ICsol.UpdatePayOther

    End Sub

    Public Sub UpdatePayRec(ByVal id As Integer, _
                          ByVal ReceiptNum As String, ByVal StuID As String, _
                 ByVal SubClassID As Integer, ByVal Amount As Integer, _
                 ByVal DateTime As Date, ByVal HandlerID As String, _
                 ByVal UsrName As String, ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String, _
                 ByVal Reason As String, ByVal OldAmount As Integer) Implements ICsol.UpdatePayRec
        Dim obj As New StuInfo
        Dim diff As Integer = Amount - OldAmount
        obj.UpdStuOwe(StuID, -diff)
        obj.UpdStuClassOwe(StuID, SubClassID, -diff)

        obj.UpdatePay(id, ReceiptNum, StuID, SubClassID, Amount, DateTime, _
                      HandlerID, UsrName, PayMethod, Extra, Remarks, Reason)
    End Sub

    Sub UpdateDisc(ByVal id As Integer, _
                             ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Discount As Integer, _
                     ByVal Remarks As String, ByVal OldDiscount As Integer) Implements ICsol.UpdateDisc
        Dim obj As New StuInfo
        Dim diff As Integer = Discount - OldDiscount
        obj.UpdStuOwe(StuID, -diff)
        obj.UpdStuClassOwe(StuID, SubClassID, -diff)

        obj.UpdateDisc(id, Discount, Remarks)
    End Sub

    Public Sub AddPayModRec(ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal UsrName As String, _
                     ByVal Remarks As String, _
                     ByVal Reason As String) Implements ICsol.AddPayModRec
        Dim obj As New StuInfo
        obj.AddPayModRec(ReceiptNum, StuID, SubClassID, DateTime, HandlerID, UsrName, Remarks, Reason)

    End Sub


    Public Sub UpdateSampleTimes(ByVal dt As ArrayList) Implements ICsol.UpdateSampleTimes
        Dim obj As New ClassInfo
        obj.DeleteSampleTime()

        For index As Integer = 0 To dt.Count - 1
            obj.AddSampleTime(dt(index))
        Next
    End Sub

    Public Sub UpdateSeatNA(ByVal seatDt As System.Data.DataTable) Implements ICsol.UpdateSeatNA

    End Sub

    Public Sub UpdateStuClass(ByVal StuClass As System.Data.DataTable) Implements ICsol.UpdateStuClass

    End Sub

    Public Sub UpdateStuSibling(ByVal StuSibling As System.Data.DataTable) Implements ICsol.UpdateStuSibling

    End Sub

    Public Sub UpdateSubClasses(ByVal subClassDt As System.Data.DataTable) Implements ICsol.UpdateSubClasses

    End Sub

    'Public Function UsrLogin(ByVal UsrId As String, ByVal UsrPwd As String, _
    '                         ByVal pcname As String) As ArrayList Implements ICsol.UsrLogin
    '    Dim obj As New Admin
    '    Dim dt As DataTable = obj.GetUsrPWD(UsrId)
    '    Dim s As String = ""
    '    Dim lst As New ArrayList

    '    If dt.Rows.Count > 0 Then
    '        Dim p As String = dt.Rows(0).Item(c_PWDColumnName).trim
    '        If p = UsrPwd Then
    '            Dim dt2 As DataTable = obj.GetUsrLoginFail(UsrId)
    '            If dt2.Rows.Count > 0 Then
    '                s = "Datetime: " & dt2.Rows(0).Item(c_DateTimeColumnName).ToString & vbNewLine & _
    '                    "PC: " & dt2.Rows(0).Item(c_PCNameColumnName) & vbNewLine & _
    '                    "Wrong password: " & dt2.Rows(0).Item(c_IncorrectPWDColumnName).trim
    '                obj.DeleteUsrLoginFail(UsrId)
    '            Else
    '                s = "OK"
    '            End If
    '            lst.Add(s)

    '            Dim dt3 As DataTable = GetUsrByID(UsrId)
    '            If dt3.Rows.Count > 0 Then
    '                lst.Add(dt3.Rows(0).Item(c_NameColumnName))
    '            Else
    '                lst.Add("")
    '            End If

    '            Dim dt4 As DataTable = GetUsrByID(UsrId)
    '            If dt4.Rows.Count > 0 Then
    '                lst.Add(dt4.Rows(0).Item(c_StatusColumnName))
    '            Else
    '                lst.Add("")
    '            End If
    '        Else
    '            obj.InsertUsrLoginFail(UsrId, pcname, UsrPwd, 2)
    '            lst.Add(s)
    '        End If
    '    End If

    '    Return lst
    'End Function
    Public Function UsrLogin(ByVal UsrId As String, ByVal UsrPwd As String, _
                                ByVal pcname As String) As ArrayList Implements ICsol.UsrLogin
        Dim lst As New ArrayList

        Try
            Dim obj As New Admin
            Dim dt As DataTable = obj.GetUsrPWD(UsrId)
            Dim s As String = ""


            If dt.Rows.Count > 0 Then

                Dim p As String = dt.Rows(0).Item(c_PWDColumnName).trim
                If p = UsrPwd Then
                    Dim dt2 As DataTable = obj.GetUsrLoginFail(UsrId)
                    If dt2.Rows.Count > 0 Then
                        s = "Datetime: " & dt2.Rows(0).Item(c_DateTimeColumnName).ToString & vbNewLine & _
                            "PC: " & dt2.Rows(0).Item(c_PCNameColumnName) & vbNewLine & _
                            "Wrong password: " & dt2.Rows(0).Item(c_IncorrectPWDColumnName).trim
                        obj.DeleteUsrLoginFail(UsrId)
                    Else
                        s = "OK"
                    End If
                    lst.Add(s)

                    Dim dt3 As DataTable = GetUsrByID(UsrId)
                    If dt3.Rows.Count > 0 Then
                        lst.Add(dt3.Rows(0).Item(c_NameColumnName))
                    Else
                        lst.Add("")
                    End If

                    Dim dt4 As DataTable = GetUsrByID(UsrId)
                    If dt4.Rows.Count > 0 Then
                        lst.Add(dt4.Rows(0).Item(c_StatusColumnName))
                    Else
                        lst.Add("")
                    End If
                Else
                    obj.InsertUsrLoginFail(UsrId, pcname, UsrPwd, 2)
                    lst.Add(s)
                End If
                'Else
                '    lst.Add("1")
            End If

        Catch ex As Exception

            CSOL.Logger.Path = My.Application.Info.DirectoryPath
            CSOL.Logger.Log("LoginError", ex.ToString)

        End Try
        Return lst
    End Function

    Public Function AddPaper(ByVal Name As String, ByVal Abbr As String, _
                   ByVal PaperDate As Date, ByVal content As Integer, _
                     ByVal lstSc As ArrayList) As Int32 Implements ICsol.AddPaper
        Dim obj As New ExamPaper
        Return obj.AddPaper(Name, Abbr, PaperDate, content, lstSc)
    End Function

    Public Sub UpdatePaper(ByVal ID As Integer, ByVal Name As String, ByVal Abbr As String, _
                      ByVal PaperDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList) Implements ICsol.UpdatePaper
        Dim obj As New ExamPaper
        obj.UpdatePaper(ID, Name, Abbr, PaperDate, content, lstSc)
    End Sub

    Public Sub DeletePaper(ByVal id As Integer) Implements ICsol.DeletePaper
        Dim obj As New ExamPaper
        obj.DeletePaper(id)
    End Sub

    Public Function GetStuGrades(ByVal paperId As Integer, ByVal sc As ArrayList) As DataTable Implements ICsol.GetStuGrades
        Dim obj As New ExamPaper
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetStuGrades(paperId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetStuGrades(paperId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetStuGrades(paperId, sc(0))
        Else
            dt = obj.GetStuGrades(paperId, -1)
        End If

        Return dt

    End Function

    Public Function GetStuGradesMul(ByVal paperId As ArrayList, ByVal sc As ArrayList, ByVal c As Integer) As DataTable Implements ICsol.GetStuGradesMul
        Dim obj As New ExamPaper
        Dim dt As DataTable
        Dim dtPaper As DataTable = GetClassPaperList(c)
        Dim lst As New ArrayList
        Dim s As Integer = 0
        Dim paper As Integer = 0

        If paperId.Count > 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
            For index As Integer = 1 To paperId.Count - 1
                paper = paperId(index)
                lst = New ArrayList
                For i As Integer = 0 To sc.Count - 1
                    s = sc(i)
                    If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                               c_IDColumnName & "=" & paper.ToString) > 0 Then
                        lst.Add(s)
                    End If
                Next
                dt.Merge(GetStuGrades(paper, lst))
            Next
        ElseIf paperId.Count = 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
        Else
            dt = GetStuGrades(-1, lst)
        End If

        Return dt

    End Function

    Public Function GetStuGradesMulByMulClass(ByVal paperId As ArrayList, ByVal sc As ArrayList, ByVal c As ArrayList) As DataTable Implements ICsol.GetStuGradesMulByMulClass
        Dim obj As New ExamPaper
        Dim dt As DataTable
        Dim dtPaper As DataTable
        If c.Count = 1 Then
            dtPaper = GetClassPaperList(c(0))
        ElseIf c.Count > 1 Then
            dtPaper = GetClassPaperList(c(0))
            For i As Integer = 1 To c.Count - 1
                dtPaper.Merge(GetClassPaperList(c(i)))
            Next
        Else
            dtPaper = GetClassPaperList(-1)
        End If

        Dim lst As New ArrayList
        Dim s As Integer = 0
        Dim paper As Integer = 0

        If paperId.Count > 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
            For index As Integer = 1 To paperId.Count - 1
                paper = paperId(index)
                lst = New ArrayList
                For i As Integer = 0 To sc.Count - 1
                    s = sc(i)
                    If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                               c_IDColumnName & "=" & paper.ToString) > 0 Then
                        lst.Add(s)
                    End If
                Next
                dt.Merge(GetStuGrades(paper, lst))
            Next
        ElseIf paperId.Count = 1 Then
            paper = paperId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuGrades(paper, lst)
        Else
            dt = GetStuGrades(-1, lst)
        End If

        Return dt

    End Function

    Public Function GetClassAssignment(ByVal classId As ArrayList) As System.Data.DataTable Implements ICsol.GetClassAssignment
        Dim obj As New Assignment
        Dim dt As DataTable

        If classId.Count > 1 Then
            dt = obj.GetClassAssignment(classId(0))
            For index As Integer = 1 To classId.Count - 1
                dt.Merge(obj.GetClassAssignment(classId(index)))
            Next
        Else
            dt = obj.GetClassAssignment(classId(0))
        End If

        Return dt

    End Function

    Public Function AddAssignment(ByVal Name As String, ByVal StartDate As Date, _
                       ByVal EndDate As Date, ByVal content As Integer, _
                         ByVal lstSc As ArrayList) As Int32 Implements ICsol.AddAssignment
        Dim obj As New Assignment
        Return obj.AddAssignment(Name, StartDate, EndDate, content, lstSc)
    End Function

    Public Sub UpdateAssignment(ByVal ID As Integer, ByVal Name As String, ByVal StartDate As Date, _
                      ByVal EndDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList) Implements ICsol.UpdateAssignment
        Dim obj As New Assignment
        obj.UpdateAssignment(ID, Name, StartDate, EndDate, content, lstSc)
    End Sub

    Public Sub DeleteAssignment(ByVal id As Integer) Implements ICsol.DeleteAssignment
        Dim obj As New Assignment
        obj.DeleteAssignment(id)
    End Sub

    Public Sub DeleteStuGrade(ByVal stuId As String, ByVal paperId As Integer) Implements ICsol.DeleteStuGrade
        Dim obj As New ExamPaper
        obj.DeleteStuGrade(stuId, paperId)
    End Sub

    Public Sub UpdateStuGrade(ByVal stuId As String, ByVal paperId As Integer, ByVal mark As Single, _
                              ByVal dt As Date, ByVal remarks As String, ByVal inputby As String) Implements ICsol.UpdateStuGrade
        Dim obj As New ExamPaper
        obj.DeleteStuGrade(stuId, paperId)
        obj.InsertStuGrade(stuId, paperId, mark, dt, remarks, inputby)
    End Sub

    Public Function GetClassAssignmentList(ByVal classId As Integer) As System.Data.DataTable Implements ICsol.GetClassAssignmentList
        Dim obj As New Assignment

        Return obj.GetClassAssignmentList(classId)

    End Function

    Sub UpdateStuAssignment(ByVal stuId As String, ByVal AssignmentId As Integer, ByVal dt As Date, ByVal mark As Integer) Implements ICsol.UpdateStuAssignment
        Dim obj As New Assignment
        obj.DeleteStuAssignment(stuId, AssignmentId)
        If mark = c_Pass Then
            obj.InsertStuAssignment(stuId, AssignmentId, dt)
        End If
    End Sub

    Function GetStuAssignments(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable Implements ICsol.GetStuAssignments
        Dim obj As New Assignment
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetStuAssignments(assignmentId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetStuAssignments(assignmentId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetStuAssignments(assignmentId, sc(0))
        Else
            dt = obj.GetStuAssignments(assignmentId, -1)
        End If

        Return dt
    End Function

    Function GetStuAssignmentsMul(ByVal assignmentId As ArrayList, ByVal sc As ArrayList, ByVal c As Integer) As DataTable Implements ICsol.GetStuAssignmentsMul
        Dim obj As New Assignment
        Dim dt As DataTable
        Dim dtAssignment As DataTable = GetClassAssignmentList(c)
        Dim lst As New ArrayList
        Dim s As Integer = 0
        Dim Assignment As Integer = 0

        If assignmentId.Count > 1 Then
            Assignment = assignmentId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                    lst.Add(s)
                End If
            Next
            dt = GetStuAssignments(Assignment, lst)
            For index As Integer = 1 To assignmentId.Count - 1
                Assignment = assignmentId(index)
                lst = New ArrayList
                For i As Integer = 0 To sc.Count - 1
                    s = sc(i)
                    If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                               c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                        lst.Add(s)
                    End If
                Next
                dt.Merge(GetStuAssignments(Assignment, lst))
            Next
        ElseIf assignmentId.Count = 1 Then
            Assignment = assignmentId(0)
            For i As Integer = 0 To sc.Count - 1
                s = sc(i)
                If dtAssignment.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & s.ToString & " AND " & _
                                           c_IDColumnName & "=" & Assignment.ToString) > 0 Then
                    lst.Add(s)
                End If
                'If dtAssignment.Compute(String.Format("COUNT('{0}')", "ID"), String.Format("'{1}'" = s.ToString And "'{0}'" = Assignment.ToString, "ID", "SubClass")) > 0 Then

                '    lst.Add(s)
                'End If
            Next
            dt = GetStuAssignments(Assignment, lst)
        Else
            dt = GetStuAssignments(-1, lst)
        End If

        Return dt

    End Function

    Function GetStuDoneAssignmentList(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable Implements ICsol.GetStuDoneAssignmentList
        Dim obj As New Assignment
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetStuDoneAssignmentList(assignmentId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetStuDoneAssignmentList(assignmentId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetStuDoneAssignmentList(assignmentId, sc(0))
        Else
            dt = obj.GetStuDoneAssignmentList(assignmentId, -1)
        End If

        Return dt
    End Function

    Function GetStuNoAssignmentList(ByVal assignmentId As Integer, ByVal sc As ArrayList) As DataTable Implements ICsol.GetStuNoAssignmentList
        Dim obj As New Assignment
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetStuNoAssignmentList(assignmentId, sc(0))
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetStuNoAssignmentList(assignmentId, sc(index)))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetStuNoAssignmentList(assignmentId, sc(0))
        Else
            dt = obj.GetStuNoAssignmentList(assignmentId, -1)
        End If

        Return dt
    End Function

    Public Function GetSubClassPunchRecList(ByVal sc As ArrayList, _
                                    ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable Implements ICsol.GetSubClassPunchRecList
        Dim obj As New ClassInfo
        Dim dt As DataTable
        If sc.Count > 1 Then
            dt = obj.GetSubClassPunchRecNoCont(sc(0), dateFrom, dateTo)
            For index As Integer = 1 To sc.Count - 1
                dt.Merge(obj.GetSubClassPunchRecNoCont(sc(index), dateFrom, dateTo))
            Next
        ElseIf sc.Count = 1 Then
            dt = obj.GetSubClassPunchRecNoCont(sc(0), dateFrom, dateTo)
        Else
            dt = obj.GetSubClassPunchRecNoCont(-1, dateFrom, dateTo)
        End If

        Return dt
    End Function

    Sub ImportStuGrades(ByVal dt As DataTable, ByVal nameType As Integer, ByVal replace As Integer, _
                        ByVal datetime As Date, ByVal paper As Integer, ByVal sc As ArrayList, ByVal handler As String) Implements ICsol.ImportStuGrades
        Dim dt2 As DataTable
        dt2 = GetStuGrades(paper, sc)
        Dim id As String = ""

        If nameType = 0 Then 'id
            If replace = 0 Then '0-keep, 1-replace
                For Each row As DataRow In dt.Rows

                    id = GetStuIDFromGradeTable(dt2, row.Item(c_IDColumnName), 1)
                    If id.Length = 8 Then
                        If GetStuGradeFromTable(dt2, id) = 0 Then
                            UpdateStuGrade(id, paper, row.Item(c_MarkColumnName), datetime, "", handler)
                        End If
                    End If
                Next
            Else
                For Each row As DataRow In dt.Rows
                    id = GetStuIDFromGradeTable(dt2, row.Item(c_IDColumnName), 1)
                    If id.Length = 8 Then
                        UpdateStuGrade(id, paper, row.Item(c_MarkColumnName), datetime, "", handler)
                    End If
                Next
            End If
        Else  'name

            If replace = 0 Then '0-keep, 1-replace

                For Each row As DataRow In dt.Rows
                    id = GetStuIDFromGradeTable(dt2, row.Item(c_NameColumnName), 0)
                    If id.Length = 8 Then
                        If GetStuGradeFromTable(dt2, id) = 0 Then
                            UpdateStuGrade(id, paper, row.Item(c_MarkColumnName), datetime, "", handler)
                        End If
                    End If
                Next
            Else
                For Each row As DataRow In dt.Rows
                    id = GetStuIDFromGradeTable(dt2, row.Item(c_NameColumnName), 0)
                    If id.Length = 8 Then
                        UpdateStuGrade(id, paper, row.Item(c_MarkColumnName), datetime, "", handler)
                    End If
                Next
            End If
        End If
    End Sub

    Function GetStuGradeFromTable(ByRef dt As DataTable, ByVal id As String) As Integer Implements ICsol.GetStuGradeFromTable
        Dim r As Integer = 0
        If dt.Compute("COUNT (" & c_IDColumnName & ")", c_IDColumnName & "=" & id) > 0 Then
            r = 1
        End If

        Return r
    End Function

    Function GetStuIDFromGradeTable(ByRef dt As DataTable, ByVal name As String, ByVal type As Integer) As String Implements ICsol.GetStuIDFromGradeTable
        Dim r As String = ""
        If type = 0 Then 'get id from name
            For Each row As DataRow In dt.Rows
                If row.Item(c_NameColumnName).ToString.ToUpper.Trim = name.ToUpper.Trim Then
                    r = row.Item(c_IDColumnName)
                End If
            Next
        ElseIf type = 1 Then 'verify id exists in this subclass
            For Each row As DataRow In dt.Rows
                If row.Item(c_IDColumnName).ToString.ToUpper.Trim = name.ToUpper.Trim Then
                    r = row.Item(c_IDColumnName)
                End If
            Next
        End If
        Return r
    End Function

    Public Function getTransSubClassInfo(ByVal intOldSubClassId As Integer, ByVal strStuId As String) As DataSet Implements ICsol.getTransSubClassInfo
        Dim objClassInfo As New ClassInfo
        Dim ds As New DataSet("getTransSubClassInfo")

        Dim GetTransPayRec As DataTable = objClassInfo.GetTransPayRec(intOldSubClassId, strStuId)
        GetTransPayRec.TableName = "TransPayRec"
        ds.Tables.Add(GetTransPayRec)

        Dim GetTransFeeDiscount As DataTable = objClassInfo.GetTransFeeDiscount(intOldSubClassId, strStuId)
        GetTransFeeDiscount.TableName = "TransFeeDiscount"
        ds.Tables.Add(GetTransFeeDiscount)

        Dim GetTransKeepPay As DataTable = objClassInfo.GetTransKeepPay(intOldSubClassId, strStuId)
        GetTransKeepPay.TableName = "TransKeepPay"
        ds.Tables.Add(GetTransKeepPay)

        Dim GetTransMoneyBack As DataTable = objClassInfo.GetTransMoneyBack(intOldSubClassId, strStuId)
        GetTransMoneyBack.TableName = "TransMoneyBack"
        ds.Tables.Add(GetTransMoneyBack)

        Return ds
    End Function
    Public Sub UpdateTransPayRec(ByVal ID As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                                        ByVal IntSubClassId As Integer) Implements ICsol.UpdateTransPayRec
        Dim obj As New ClassInfo
        obj.UpdTransPayRec(ID, ReceiptNum, StuID, IntSubClassId)
    End Sub
    Public Sub UpdateTransMoneyBack(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer) Implements ICsol.UpdateTransMoneyBack
        Dim obj As New ClassInfo
        obj.UpdTransMoneyBack(ID, StuID, IntSubClassId)
    End Sub
    Public Sub UpdateTransFeeDiscount(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer) Implements ICsol.UpdateTransFeeDiscount
        Dim obj As New ClassInfo
        obj.UpdTransFeeDiscount(ID, StuID, IntSubClassId)
    End Sub
    Public Sub UpdateTransKeepPay(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer) Implements ICsol.UpdateTransKeepPay
        Dim obj As New ClassInfo
        obj.UpdTransKeepPay(ID, StuID, IntSubClassId)
    End Sub

    '20100424 autoupdate by sherry

    Public Function GetUpdateList(ByVal last As Date) As String() Implements ICsol.GetUpdateList
        Dim updatePath As String = String.Format("{0}\update", My.Application.Info.DirectoryPath)
        Dim result As New List(Of String)

        Dim list() As String = System.IO.Directory.GetFiles(updatePath)
        For Each file As String In list
            Dim d As Date = System.IO.File.GetLastWriteTime(file)
            If last < d Then
                result.Add(System.IO.Path.GetFileName(file))
            End If
        Next

        Return result.ToArray()
    End Function

    Public Function GetUpdateFile(ByVal filename As String) As Byte() Implements ICsol.GetUpdateFile
        Dim path As String = String.Format("{0}\update\{1}", My.Application.Info.DirectoryPath, filename)
        If System.IO.File.Exists(path) Then
            Return System.IO.File.ReadAllBytes(path)
        Else
            Return Nothing
        End If
    End Function
    '20100424 imgdb by sherry start
    Public Sub UploadImg(ByVal StuID As String, ByVal img As Byte()) Implements ICsol.UploadImg

        Dim obj As New Picture
        Dim dt As DataTable = obj.GetImgList
        Dim id As Integer = -1
        For i As Integer = 0 To dt.Rows.Count - 1
            If GetstringValue(dt.Rows(i).Item(c_StuIDColumnName)).Equals(StuID.Trim) Then
                id = dt.Rows(i).Item(c_IDColumnName)
                Exit For
            End If
        Next
        If id = -1 Then
            obj.UploadImg(StuID, img)
        Else
            obj.ModImg(id, img)
        End If
    End Sub

    Function GetStuIdList() As DataTable Implements ICsol.GetStuIdList
        Dim obj As New StuInfo
        Dim dt As DataTable = obj.GetStuIdList
        Return dt

    End Function

    Public Function DownloadImg(ByVal StuID As String) As Byte() Implements ICsol.DownloadImg
        Dim result As Byte() = Nothing
        Dim obj As New Picture
        Dim dt As DataTable = obj.GetImgList
        Dim id As Integer = -1

        For i As Integer = 0 To dt.Rows.Count - 1
            If GetstringValue(dt.Rows(i).Item(c_StuIDColumnName)).Equals(StuID.Trim) Then
                id = dt.Rows(i).Item(c_IDColumnName)
                Exit For
            End If
        Next

        If id >= 0 Then
            dt = obj.DownloadImg(id)
            If dt.Rows.Count > 0 Then
                result = CType(dt.Rows(0).Item(c_PictureColumnName), Byte())
            End If
        End If

        Return result
    End Function

    Public Sub DelImg(ByVal id As Integer) Implements ICsol.DelImg
        Dim obj As New Picture
        obj.DelImg(id)
    End Sub

    Public Sub DelImgByStuID(ByVal ID As String) Implements ICsol.DelImgByStuID
        Dim obj As New Picture
        Dim dt As DataTable = obj.GetImgList
        Dim idx As Integer = -1

        For i As Integer = 0 To dt.Rows.Count - 1
            If GetstringValue(dt.Rows(i).Item(c_StuIDColumnName)).Equals(ID.Trim) Then
                idx = dt.Rows(i).Item(c_IDColumnName)
                Exit For
            End If
        Next

        If idx >= 0 Then
            obj.DelImg(idx)
        End If
    End Sub

    Public Function GetAutoUpdatePath() As String Implements ICsol.GetAutoUpdatePath
        'Return CType(ConfigurationManager.AppSettings("autoupdpath"), String)
        Return CSOL.DataBase.AutoUpdPath
    End Function
    '20100424 imgdb by sherry end
End Class
