﻿Imports System.Net

Public Interface IHttpHandler
    ReadOnly Property VirtualPath() As String
    Sub Handle(ByVal Context As HttpListenerContext)
End Interface
