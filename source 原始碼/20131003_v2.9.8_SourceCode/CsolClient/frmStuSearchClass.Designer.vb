﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuSearchClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuSearchClass))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip
        Me.mnuDetails = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExportData = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPayRemind = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNote = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.chkboxFormal = New System.Windows.Forms.CheckBox
        Me.chkboxPotential = New System.Windows.Forms.CheckBox
        Me.chkboxShowCancel = New System.Windows.Forms.CheckBox
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.tboxCount = New System.Windows.Forms.TextBox
        Me.MenuStrip1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.AllowMerge = False
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDetails, Me.mnuSearch, Me.mnuSelectCol, Me.mnuDelete, Me.mnuPrint, Me.mnuExportData, Me.mnuPayRemind, Me.mnuNote, Me.mnuClose})
        resources.ApplyResources(Me.MenuStrip1, "MenuStrip1")
        Me.MenuStrip1.Name = "MenuStrip1"
        '
        'mnuDetails
        '
        Me.mnuDetails.Name = "mnuDetails"
        resources.ApplyResources(Me.mnuDetails, "mnuDetails")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        resources.ApplyResources(Me.mnuSelectCol, "mnuSelectCol")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExportData
        '
        Me.mnuExportData.Name = "mnuExportData"
        resources.ApplyResources(Me.mnuExportData, "mnuExportData")
        '
        'mnuPayRemind
        '
        Me.mnuPayRemind.Name = "mnuPayRemind"
        resources.ApplyResources(Me.mnuPayRemind, "mnuPayRemind")
        '
        'mnuNote
        '
        Me.mnuNote.Name = "mnuNote"
        resources.ApplyResources(Me.mnuNote, "mnuNote")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'chkboxFormal
        '
        resources.ApplyResources(Me.chkboxFormal, "chkboxFormal")
        Me.chkboxFormal.Checked = True
        Me.chkboxFormal.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkboxFormal.Name = "chkboxFormal"
        Me.chkboxFormal.UseVisualStyleBackColor = True
        '
        'chkboxPotential
        '
        resources.ApplyResources(Me.chkboxPotential, "chkboxPotential")
        Me.chkboxPotential.Name = "chkboxPotential"
        Me.chkboxPotential.UseVisualStyleBackColor = True
        '
        'chkboxShowCancel
        '
        resources.ApplyResources(Me.chkboxShowCancel, "chkboxShowCancel")
        Me.chkboxShowCancel.Name = "chkboxShowCancel"
        Me.chkboxShowCancel.UseVisualStyleBackColor = True
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.Name = "cboxClass"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'PrintDocument1
        '
        '
        'tboxCount
        '
        resources.ApplyResources(Me.tboxCount, "tboxCount")
        Me.tboxCount.ForeColor = System.Drawing.SystemColors.WindowText
        Me.tboxCount.Name = "tboxCount"
        Me.tboxCount.ReadOnly = True
        '
        'frmStuSearchClass
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Controls.Add(Me.tboxCount)
        Me.Controls.Add(Me.chkboxPotential)
        Me.Controls.Add(Me.chkboxShowCancel)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.chkboxFormal)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Controls.Add(Me.dgv)
        Me.Name = "frmStuSearchClass"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuDetails As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExportData As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkboxFormal As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxPotential As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowCancel As System.Windows.Forms.CheckBox
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents mnuPayRemind As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNote As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents tboxCount As System.Windows.Forms.TextBox
End Class
