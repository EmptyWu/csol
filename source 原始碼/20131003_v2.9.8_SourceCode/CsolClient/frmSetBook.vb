﻿Imports System.Net
Public Class frmSetBook
    Private dtBook As New DataTable
    Private dt As New DataTable
    Private dt2 As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private rowNum As Integer

    Public Sub New()
        InitializeComponent()
        Me.Text = My.Resources.frmSetBook
        'RefreshData()
        Dim jobtime As DateTime = Now
        RefreshData2()
        System.Diagnostics.Debug.WriteLine((Now - jobtime).ToString)
        jobtime = Now
        dtClass = frmMain.GetClassList_Fast
        System.Diagnostics.Debug.WriteLine((Now - jobtime).ToString)
        jobtime = Now
        InitList()
        System.Diagnostics.Debug.WriteLine((Now - jobtime).ToString)
        jobtime = Now
    End Sub

    Private Sub frmSetBook_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmSetBook_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()
        cboxClass.Items.Add(My.Resources.all)
        lstClass.Add(-1)
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        cboxClass.SelectedIndex = -1
        cboxClass.Text = ""
    End Sub

    Friend Sub RefreshData2()
        Dim ClassSelected As String = ""
        If Not cboxClass.SelectedItem Is Nothing Then
            ClassSelected = cboxClass.SelectedItem
        End If

        If cboxClass.SelectedIndex > -1 Then

            If lstClass(cboxClass.SelectedIndex) = -1 Then
                'dtBook = frmMain.GetRefreshBooks()
                Dim n As DateTime = Now
                dtBook = GetBookTable(ClassSelected)
                System.Diagnostics.Debug.WriteLine((Now - n).TotalMilliseconds)
            Else
                Dim n As DateTime = Now
                dtBook = GetBookTable(ClassSelected)
                System.Diagnostics.Debug.WriteLine((Now - n).TotalMilliseconds)
                'dtBook = frmMain.GetRefreshBooks(lstClass(cboxClass.SelectedIndex))
            End If

            dt.Clear()
            dt2.Clear()
            dt = dtBook.Clone
            dt = dtBook

            dt.Columns.Add(c_AmountColumnName, GetType(System.Int32))
            dt.Columns.Add(c_LeftCntColumnName, GetType(System.Int32))

            Dim i1 As Integer
            Dim i2 As Integer
            Dim i3 As Integer
            For index As Integer = 0 To dt.Rows.Count - 1
                i1 = dt.Rows(index).Item(c_PriceColumnName)
                i2 = dt.Rows(index).Item(c_StockColumnName)
                i3 = i1 * i2
                dt.Rows(index).Item(c_AmountColumnName) = i3

                i1 = dt.Rows(index).Item(c_IssuedCntColumnName)
                i3 = i2 - i1
                dt.Rows(index).Item(c_LeftCntColumnName) = i3

            Next

            If Not ClassSelected Is Nothing Then
                If cboxClass.Items.IndexOf(ClassSelected) > -1 Then
                    RefreshTable()
                    cboxClass.SelectedItem = ClassSelected
                End If
            End If
        End If


    End Sub



    Friend Sub RefreshData()
        Dim ClassSelected As String = ""
        If Not cboxClass.SelectedItem Is Nothing Then
            ClassSelected = cboxClass.SelectedItem
        End If

        'Dim RequestParams As New System.Collections.Specialized.NameValueCollection
        'Dim ResponseData() As Byte = CSOL.HTTPClient.Post("ClassInfo", "GetBookListByUsr", RequestParams)
        'Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        'Dim xmlstring As String = ""
        'Try
        '    Using ms As New System.IO.MemoryStream
        '        zip("BookList").Extract(ms)
        '        ms.Flush()
        '        ms.Position = 0
        '        Dim sr As New System.IO.StreamReader(ms)
        '        xmlstring = sr.ReadToEnd()

        '    End Using
        '    dtBook = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("BookList"))
        'Catch ex As Exception
        '    System.Diagnostics.Debug.WriteLine(ex)
        'Finally

        'End Try

        dtBook = GetBookTable(ClassSelected)

        dt.Clear()
        dt2.Clear()
        dt = dtBook.Clone
        dt = dtBook

        dt.Columns.Add(c_AmountColumnName, GetType(System.Int32))
        dt.Columns.Add(c_LeftCntColumnName, GetType(System.Int32))

        Dim i1 As Integer
        Dim i2 As Integer
        Dim i3 As Integer
        For index As Integer = 0 To dt.Rows.Count - 1
            i1 = dt.Rows(index).Item(c_PriceColumnName)
            i2 = dt.Rows(index).Item(c_StockColumnName)
            i3 = i1 * i2
            dt.Rows(index).Item(c_AmountColumnName) = i3

            i1 = dt.Rows(index).Item(c_IssuedCntColumnName)
            i3 = i2 - i1
            dt.Rows(index).Item(c_LeftCntColumnName) = i3

        Next

        'If Not ClassSelected Is Nothing Then
        '    If cboxClass.Items.IndexOf(ClassSelected) > -1 Then
        '        cboxClass.SelectedItem = ClassSelected
        '    End If
        'End If

    End Sub

    Private Sub RefreshTable()

        Dim f As String = ""
        If dt.Rows.Count = 0 Then
            dgv.DataSource = Nothing
            Exit Sub
        End If
        If cboxClass.SelectedIndex > 0 Then
            f = c_ClassIDColumnName & "=" & lstClass(cboxClass.SelectedIndex).ToString
            '20100511 sherry 12
        ElseIf cboxClass.SelectedIndex = -1 Then
            dgv.DataSource = Nothing
            Exit Sub
        End If
        If Not chkboxShowPastBook.Checked Then
            If f = "" Then
                f = c_BeforeDateColumnName & ">='" & Now.Date.ToString & "' OR " & _
                    c_BeforeDateColumnName & "=NULL"
            Else
                f = f & " AND (" & c_BeforeDateColumnName & ">='" & Now.Date.ToString & _
                    "' OR " & _
                    c_BeforeDateColumnName & "=NULL)"
            End If
        End If

        dt.DefaultView.RowFilter = f
        dt2.Clear()
        dt2 = dt.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, _
                                          c_StockColumnName, c_PriceColumnName, _
                                          c_BeforeDateColumnName, _
                                          c_FeeClearColumnName, c_FeeClearDateColumnName, _
                                          c_MultiClassColumnName, c_CreateDateColumnName, _
                                          c_AmountColumnName, c_LeftCntColumnName, _
                                            c_IssuedCntColumnName)



        dgv.DataSource = dt2
        rowNum = dgv.RowCount
        tboxCount.Text = "1/" + rowNum.ToString

        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
            dgv.Columns.Item(c_BeforeDateColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_BeforeDateColumnName).Visible = True
            dgv.Columns.Item(c_BeforeDateColumnName).HeaderText = My.Resources.bookBeforeDate
            dgv.Columns.Item(c_PriceColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_PriceColumnName).Visible = True
            dgv.Columns.Item(c_PriceColumnName).HeaderText = My.Resources.unitPrice
            dgv.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_AmountColumnName).Visible = True
            dgv.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.totalPrice
            dgv.Columns.Item(c_StockColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_StockColumnName).Visible = True
            dgv.Columns.Item(c_StockColumnName).HeaderText = My.Resources.stock
            dgv.Columns.Item(c_IssuedCntColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_IssuedCntColumnName).Visible = True
            dgv.Columns.Item(c_IssuedCntColumnName).HeaderText = My.Resources.issuedCnt
            dgv.Columns.Item(c_LeftCntColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_LeftCntColumnName).Visible = True
            dgv.Columns.Item(c_LeftCntColumnName).HeaderText = My.Resources.leftCnt
            dgv.Columns.Item(c_FeeClearColumnName).DisplayIndex = 7
            dgv.Columns.Item(c_FeeClearColumnName).Visible = True
            dgv.Columns.Item(c_FeeClearColumnName).HeaderText = My.Resources.bookMustClearFee
            dgv.Columns.Item(c_FeeClearDateColumnName).DisplayIndex = 8
            dgv.Columns.Item(c_FeeClearDateColumnName).Visible = True
            dgv.Columns.Item(c_FeeClearDateColumnName).HeaderText = My.Resources.bookMustClearFeeDate
            dgv.Columns.Item(c_MultiClassColumnName).DisplayIndex = 9
            dgv.Columns.Item(c_MultiClassColumnName).Visible = True
            dgv.Columns.Item(c_MultiClassColumnName).HeaderText = My.Resources.bookMultiClass
        End If
    End Sub

    Private Sub frmSetBook_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        '20100511 sherry issue 12
        If cboxClass.SelectedIndex > -1 Then
            RefreshData2()
            'RefreshTable()
        Else
            dgv.DataSource = Nothing
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub mnuNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim lst As New ArrayList

        Dim frm As New frm2AddBook(-1, "", 0, 0, Now, 0, Now, 0, Now, lst)

        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim start As DateTime = Now()
            'cboxClass.SelectedIndex = -1
            frmMain.SetOkState(False)
            'frmMain.RefreshBookTb()
            Dim jobtime As DateTime = Now
            RefreshData()
            System.Diagnostics.Debug.WriteLine("1" & (Now - jobtime).ToString)
            jobtime = Now

            RefreshTable()
            System.Diagnostics.Debug.WriteLine("2" & (Now - jobtime).ToString)
            jobtime = Now

        End If

    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            If frmMain.BackgroundWorker8.IsBusy Then
                MsgBox("很抱歉，背景作業執行中，請稍後重新執行刪除動作，謝謝！！")
            Else
                Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                objCsol.DeleteBook(id)
                frmMain.DelBook(id)
                RefreshData()
                RefreshTable()
                frmMain.BackgroundWorker8.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim lst As New ArrayList
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value

            For index As Integer = 0 To dtBook.Rows.Count - 1
                If dtBook.Rows(index).Item(c_IDColumnName) = id Then
                    lst.Add(dtBook.Rows(index).Item(c_SubClassIDColumnName))
                End If
            Next

            Dim frm As New frm2AddBook(id, dgv.SelectedRows(0).Cells(c_NameColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_StockColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_PriceColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_BeforeDateColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_FeeClearColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_FeeClearDateColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_MultiClassColumnName).Value, _
                                    dgv.SelectedRows(0).Cells(c_CreateDateColumnName).Value, _
                                    lst)
            frm.ShowDialog()
            Dim jobtime As DateTime = Now

            jobtime = Now
            If frmMain.GetOkState Then
                frmMain.SetOkState(False)
                'frmMain.RefreshBookTb()
                RefreshData()
                RefreshTable()
            End If

        End If
    End Sub

    Private Sub mnuQuantity_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuQuantity.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim s As Integer = dgv.SelectedRows(0).Cells(c_StockColumnName).Value
            Dim frm As New frm2SetBookQ(s)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                objCsol.UpdateBookQ(id, frmMain.GetCurrentValue)
                frmMain.RefreshBookStock(id, frmMain.GetCurrentValue)
                'frmMain.RefreshBookTb()
                RefreshData()
                RefreshTable()
                frmMain.BackgroundWorker8.RunWorkerAsync()
            End If
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        Dim frm As New frm2BookSearch
        frm.ShowDialog()
        If frmMain.GetOkState Then
            Dim s As String = frmMain.GetCurrentString
            Dim t As String = ""
            For Each row As DataGridViewRow In dgv.Rows
                t = row.Cells(c_NameColumnName).Value.ToString.Trim
                If t.Contains(s) Then
                    row.Selected = True
                    dgv.CurrentCell = row.Cells(c_NameColumnName)
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub chkboxShowPastBook_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPastBook.CheckedChanged
        RefreshTable()
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        'frmMain.RefreshBookTb()
        If cboxClass.SelectedIndex > -1 Then
            RefreshData2()
            'RefreshTable()
        Else
            dgv.DataSource = Nothing
        End If
        'RefreshData()

    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim currentRow As Integer = dgv.CurrentRow.Index + 1
        tboxCount.Text = currentRow.ToString + "/" + rowNum.ToString
    End Sub

    Private Shared Function GetBookTable(ByVal ClassName As String) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("ClassName", ClassName)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("ClassInfo", "GetBookListByUsr2", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("BookList").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()

            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("BookList"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally

        End Try
        Return dt
    End Function

    'Private Shared Function GetPayStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
    '    Dim dt As New DataTable
    '    Dim RequestParams As New System.Collections.Specialized.NameValueCollection


    '    RequestParams.Add("dateFrom", dateFrom)
    '    RequestParams.Add("dateTo", dateTo)

    '    Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaToday", "GetPayStaByDate", RequestParams)
    '    Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
    '    Dim xmlstring As String = ""
    '    Try
    '        Using ms As New System.IO.MemoryStream
    '            zip("GetPayStaByDate").Extract(ms)
    '            ms.Flush()
    '            ms.Position = 0
    '            Dim sr As New System.IO.StreamReader(ms)
    '            xmlstring = sr.ReadToEnd()
    '        End Using
    '        dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetPayStaByDate"))
    '    Catch ex As Exception
    '        System.Diagnostics.Debug.WriteLine(ex)
    '    Finally
    '    End Try
    '    Return dt
    'End Function

   
   
End Class