﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetShowPhoto
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SetShowPhoto))
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.radbutCheck = New System.Windows.Forms.RadioButton
        Me.radbutUncheck = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Label1.Name = "Label1"
        '
        'radbutCheck
        '
        resources.ApplyResources(Me.radbutCheck, "radbutCheck")
        Me.radbutCheck.Checked = True
        Me.radbutCheck.Name = "radbutCheck"
        Me.radbutCheck.TabStop = True
        Me.radbutCheck.UseVisualStyleBackColor = True
        '
        'radbutUncheck
        '
        resources.ApplyResources(Me.radbutUncheck, "radbutUncheck")
        Me.radbutUncheck.Name = "radbutUncheck"
        Me.radbutUncheck.UseVisualStyleBackColor = True
        '
        'frm2SetShowPhoto
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.radbutCheck)
        Me.Controls.Add(Me.radbutUncheck)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SetShowPhoto"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radbutCheck As System.Windows.Forms.RadioButton
    Friend WithEvents radbutUncheck As System.Windows.Forms.RadioButton
End Class
