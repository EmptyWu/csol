﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmDiscModRec
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.chkboxDate = New System.Windows.Forms.CheckBox
        Me.dtpickFrom = New System.Windows.Forms.DateTimePicker
        Me.Label5 = New System.Windows.Forms.Label
        Me.dtpickTo = New System.Windows.Forms.DateTimePicker
        Me.butFilter = New System.Windows.Forms.Button
        Me.tboxUser = New System.Windows.Forms.TextBox
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.radbutUser = New System.Windows.Forms.RadioButton
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.radbutReceipt = New System.Windows.Forms.RadioButton
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.tboxReceipt = New System.Windows.Forms.TextBox
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToResizeRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(12, 66)
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 15
        Me.dgv.RowTemplate.ReadOnly = True
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.ShowCellToolTips = False
        Me.dgv.ShowEditingIcon = False
        Me.dgv.Size = New System.Drawing.Size(1004, 513)
        Me.dgv.TabIndex = 117
        '
        'chkboxDate
        '
        Me.chkboxDate.AutoSize = True
        Me.chkboxDate.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxDate.Location = New System.Drawing.Point(462, 36)
        Me.chkboxDate.Name = "chkboxDate"
        Me.chkboxDate.Size = New System.Drawing.Size(72, 16)
        Me.chkboxDate.TabIndex = 116
        Me.chkboxDate.Text = "日期篩選"
        Me.chkboxDate.UseVisualStyleBackColor = True
        '
        'dtpickFrom
        '
        Me.dtpickFrom.Location = New System.Drawing.Point(540, 34)
        Me.dtpickFrom.Name = "dtpickFrom"
        Me.dtpickFrom.Size = New System.Drawing.Size(119, 22)
        Me.dtpickFrom.TabIndex = 114
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(665, 39)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(11, 12)
        Me.Label5.TabIndex = 113
        Me.Label5.Text = "~"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'dtpickTo
        '
        Me.dtpickTo.Location = New System.Drawing.Point(682, 34)
        Me.dtpickTo.Name = "dtpickTo"
        Me.dtpickTo.Size = New System.Drawing.Size(119, 22)
        Me.dtpickTo.TabIndex = 115
        '
        'butFilter
        '
        Me.butFilter.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butFilter.Location = New System.Drawing.Point(817, 35)
        Me.butFilter.Name = "butFilter"
        Me.butFilter.Size = New System.Drawing.Size(82, 25)
        Me.butFilter.TabIndex = 112
        Me.butFilter.Text = "執行篩選"
        Me.butFilter.UseVisualStyleBackColor = True
        '
        'tboxUser
        '
        Me.tboxUser.Location = New System.Drawing.Point(319, 34)
        Me.tboxUser.Name = "tboxUser"
        Me.tboxUser.Size = New System.Drawing.Size(97, 22)
        Me.tboxUser.TabIndex = 111
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(68, 20)
        Me.mnuPrint.Text = "列印資料"
        '
        'radbutUser
        '
        Me.radbutUser.AutoSize = True
        Me.radbutUser.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutUser.Location = New System.Drawing.Point(220, 37)
        Me.radbutUser.Name = "radbutUser"
        Me.radbutUser.Size = New System.Drawing.Size(83, 16)
        Me.radbutUser.TabIndex = 110
        Me.radbutUser.Text = "使用者帳號"
        Me.radbutUser.UseVisualStyleBackColor = True
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出資料"
        '
        'PrintDocument1
        '
        '
        'radbutReceipt
        '
        Me.radbutReceipt.AutoSize = True
        Me.radbutReceipt.Checked = True
        Me.radbutReceipt.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.radbutReceipt.Location = New System.Drawing.Point(12, 37)
        Me.radbutReceipt.Name = "radbutReceipt"
        Me.radbutReceipt.Size = New System.Drawing.Size(47, 16)
        Me.radbutReceipt.TabIndex = 108
        Me.radbutReceipt.TabStop = True
        Me.radbutReceipt.Text = "學號"
        Me.radbutReceipt.UseVisualStyleBackColor = True
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'tboxReceipt
        '
        Me.tboxReceipt.Location = New System.Drawing.Point(79, 34)
        Me.tboxReceipt.Name = "tboxReceipt"
        Me.tboxReceipt.Size = New System.Drawing.Size(105, 22)
        Me.tboxReceipt.TabIndex = 109
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 107
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'frmDiscModRec
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.chkboxDate)
        Me.Controls.Add(Me.dtpickFrom)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.dtpickTo)
        Me.Controls.Add(Me.butFilter)
        Me.Controls.Add(Me.tboxUser)
        Me.Controls.Add(Me.radbutUser)
        Me.Controls.Add(Me.radbutReceipt)
        Me.Controls.Add(Me.tboxReceipt)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmDiscModRec"
        Me.Text = "frmDiscModRec"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents chkboxDate As System.Windows.Forms.CheckBox
    Friend WithEvents dtpickFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents dtpickTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents butFilter As System.Windows.Forms.Button
    Friend WithEvents tboxUser As System.Windows.Forms.TextBox
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radbutUser As System.Windows.Forms.RadioButton
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents radbutReceipt As System.Windows.Forms.RadioButton
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tboxReceipt As System.Windows.Forms.TextBox
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
End Class
