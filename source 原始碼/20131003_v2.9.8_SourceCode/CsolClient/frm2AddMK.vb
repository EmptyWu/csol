﻿Public Class frm2AddMK
    Dim strReceiptNum As String
    Dim strStuID As String
    Dim intSubClassID As Integer
    Dim intPayAmount As Integer
    Dim dtPayDateTime As Date
    Dim strHandlerID As String
    Dim strSalesID As String
    Dim intPayMethod As Integer
    Dim strExtra As String
    Dim strRemarks As String
    Dim intDiscount As Integer
    Dim strDiscountRemarks As String
    Dim intId As Integer

    Public Sub New(ByVal id As Integer, ByVal ReceiptNum As String, ByVal StuID As String, _
                 ByVal SubClassID As Integer, _
                 ByVal PayAmount As Integer, ByVal PayDateTime As Date, _
                 ByVal HandlerID As String, _
                 ByVal SalesID As String, ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String, _
                 ByVal Discount As Integer, _
                 ByVal DiscountRemarks As String, _
                 ByVal ClassName As String, _
                 ByVal SubClassName As String, ByVal StuName As String, _
                 ByVal HandlerName As String)

        InitializeComponent()
        intId = id
        tboxStuId.Text = StuID
        tboxName.Text = StuName
        tboxClass.Text = ClassName
        tboxSubclass.Text = SubClassName
        tboxHandler.Text = HandlerName

        strReceiptNum = ReceiptNum
        strStuID = StuID
        intSubClassID = SubClassID
        intPayAmount = PayAmount
        dtPayDateTime = PayDateTime
        strHandlerID = HandlerID
        strSalesID = SalesID
        intPayMethod = PayMethod
        strExtra = Extra
        strRemarks = Remarks
        intDiscount = Discount
        strDiscountRemarks = DiscountRemarks
        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If IsNumeric(tboxAmount.Text) Then
            If CInt(tboxAmount.Text) >= 0 Then
                objCsol.AddMK(intId, strReceiptNum, strStuID, intSubClassID, _
                              CInt(tboxAmount.Text), intPayAmount, _
                              dtPayDateTime, Now, strHandlerID, _
                              strSalesID, intPayMethod, strExtra, strRemarks, _
                              intDiscount, strDiscountRemarks, tboxReason.Text, GetDateEnd(dtpickDate.Value))
                frmMain.SetOkState(True)
                Me.Close()

            Else
                ShowMsg(My.Resources.msgWrontAmount)
            End If
        Else
            ShowMsg(My.Resources.msgWrontAmount)
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        MsgBox(msg, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class