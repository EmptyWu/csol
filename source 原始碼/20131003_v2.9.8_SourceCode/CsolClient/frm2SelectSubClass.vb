﻿Public Class frm2SelectSubClass
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private lstSubClass As New ArrayList

    Public Sub New()

        InitializeComponent()

        InitClassList

        frmMain.SetOkState(False)
        radbutAll.Checked = True
    End Sub

    Private Sub InitClassList()
        Try
            dtClass = frmMain.GetClassList
            dtSubClass = frmMain.GetSubClassList

            If Not chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                        lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            End If
            If lstboxClass.Items.Count > 0 Then
                lstboxClass.SelectedIndex = -1
            End If

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            If dtClass.Rows.Count = 0 Then
                Exit Sub
            End If

            lstboxClass.Items.Clear()
            chklstSubClass.Items.Clear()
            lstClass.Clear()
            lstSubClass.Clear()
            lstSubClassShown.Clear()

            If Not chkboxShowPast.Checked Then
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                        lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If
                Next
            Else
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    lstboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                Next
            End If
            If lstboxClass.Items.Count > 0 Then
                lstboxClass.SelectedIndex = -1
            End If

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        'If lstboxClass.SelectedIndex > -1 Then
        '    Dim i As Integer = lstClass(lstboxClass.SelectedIndex)
        '    If radbutAll.Checked Then
        '        frmMain.SetCurrentValue(i)
        '        frmMain.SetCurrentString(lstboxClass.SelectedItem)
        '        frmMain.SetCurrentList(New ArrayList)
        '        frmMain.SetOkState(True)
        '        Me.Close()
        '    ElseIf radbutSelected.Checked Then
        '        RefreshSubClassList()
        '        If lstSubClass.Count > 0 Then
        '            frmMain.SetCurrentValue(i)
        '            frmMain.SetCurrentList(lstSubClass)
        '            frmMain.SetCurrentString(lstboxClass.SelectedItem)
        '            frmMain.SetOkState(True)
        '            Me.Close()
        '        Else
        '            MsgBox(My.Resources.msgNoSubClassSelected, _
        '                    MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        '        End If
        '    End If
        'End If

        If lstboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(lstboxClass.SelectedIndex)
            RefreshSubClassList()
            If lstSubClass.Count > 0 Then
                frmMain.SetCurrentValue(i)
                frmMain.SetCurrentList(lstSubClass)
                frmMain.SetCurrentString(lstboxClass.SelectedItem)
                frmMain.SetOkState(True)
                Me.Close()
            Else
                MsgBox(My.Resources.msgNoSubClassSelected, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
    End Sub

    Private Sub lstboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstboxClass.SelectedIndexChanged
        If lstboxClass.SelectedIndex > -1 Then
            lstSubClassShown.Clear()
            chklstSubClass.Items.Clear()
            Dim i As Integer = lstClass(lstboxClass.SelectedIndex)
            For Index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(Index).Item(c_ClassIDColumnName) = i Then
                    chklstSubClass.Items.Add(dtSubClass.Rows(Index).Item(c_NameColumnName).trim, True)
                    lstSubClassShown.Add(dtSubClass.Rows(Index).Item(c_IDColumnName))
                End If
            Next
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshClassList()
    End Sub
End Class