﻿Public Class frm2AddContent
    Dim intId As Integer = -1
    Dim intClass As Integer
    Dim lstSc As New ArrayList
    Dim lstScId As New ArrayList
    Dim lstscont As New ArrayList

    Public Sub New(ByVal Id As Integer, _
                    ByVal content As String, ByVal ClassID As Integer, _
                    ByVal srtDate As Date, ByVal endDate As Date, _
                    ByVal scontIds As ArrayList, ByVal scNames As ArrayList, _
                    ByVal scIds As ArrayList)
        InitializeComponent()

        lstSc = scNames
        lstScId = scIds
        intClass = ClassID

        If Id = -1 Then 'new
            lstscont = New ArrayList
        Else 'update
            intId = Id
            lstscont = scontIds
            tboxCont.Text = content
            dtpickFrom.Value = srtDate
            dtpickTo.Value = endDate
        End If

        InitList()

    End Sub

    Private Sub InitList()
        chklstSubClass.Items.Clear()

        For index = 0 To lstSc.Count - 1
            chklstSubClass.Items.Add(lstSc(index), False)
        Next
        If intId = -1 Then
            If lstSc.Count > 0 Then
                chklstSubClass.SelectedIndex = 0
                chklstSubClass.SetItemChecked(0, True)
                lstscont.Add(lstScId(0))
            End If
        Else
            If lstSc.Count > 0 Then
                chklstSubClass.SelectedIndex = 0
                For index As Integer = 0 To lstScId.Count - 1
                    If lstscont.IndexOf(lstScId(index)) > -1 Then
                        chklstSubClass.SetItemChecked(index, True)
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub radbutAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutAll.Click
        If radbutAll.Checked Then
            For index = 0 To lstSc.Count - 1
                chklstSubClass.SetItemChecked(index, True)
            Next
        End If
    End Sub


    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        Dim flag As Boolean
        lstscont = New ArrayList
        'CSOL.Logger.LogMessage("butSave1")
        For index As Integer = 0 To lstSc.Count - 1
            If chklstSubClass.GetItemChecked(index) Then
                lstscont.Add(lstScId(index))
            End If
        Next
        'CSOL.Logger.LogMessage("butSave2")
        If intId = -1 Then
            flag = objCsol.GetRepeatContent(lstscont, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            If flag = False Then
                MsgBox("請勿輸入與其他上課內容重覆時段 !!", MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
                Exit Sub
            End If
        End If

        If tboxCont.Text.Trim = "" Then
            ShowMsg()
            Exit Sub
        End If

        If intId = -1 Then
            objCsol.AddClassContent(intClass, tboxCont.Text, GetDateStart(dtpickFrom.Value), _
                        GetDateEnd(dtpickTo.Value), lstscont)
            'CSOL.Logger.LogMessage("butSave3")
        Else
            objCsol.ModifyClassContent(intId, tboxCont.Text, GetDateStart(dtpickFrom.Value), _
                GetDateEnd(dtpickTo.Value), lstscont)
            'CSOL.Logger.LogMessage("butSave4")
        End If

        Dim isBusy As Boolean = False

        If frmMain.BackgroundWorker8.IsBusy Then
            Do Until Not frmMain.BackgroundWorker8.IsBusy
                isBusy = False
            Loop
        End If

        If Not isBusy Then
            frmMain.BackgroundWorker8.RunWorkerAsync()
        End If

        frmMain.SetOkState(True)
        'CSOL.Logger.LogMessage("butSave5")
        Me.Close()
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgNoName, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class