﻿Public Class frm2SetCardReader
    Dim strReader As String
    Dim intDefault As Integer

    Private Sub frm2SetCardReader_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        strReader = GetConfigCardReader()
        Select Case strReader
            Case c_CardReaderComPort
                cboxReader.SelectedIndex = 0
                intDefault = 0
            Case c_CardReaderUsb
                cboxReader.SelectedIndex = 1
                intDefault = 1
        End Select
    End Sub

    Private Sub butConfirm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butConfirm.Click
        If cboxReader.SelectedIndex >= 0 And cboxReader.SelectedIndex <= 1 Then
            If intDefault <> cboxReader.SelectedIndex Then
                Select Case cboxReader.SelectedIndex
                    Case 0
                        SetConfigCardReader(c_CardReaderComPort)
                    Case 1
                        SetConfigCardReader(c_CardReaderUsb)
                End Select
            End If
        End If
        Me.Close()
    End Sub

    Private Sub butCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class