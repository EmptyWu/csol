﻿Public Class frmPayRecSearch
    Private dtPayRec As New DataTable
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPayRecSearch
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmPayRecSearch
        cboxFilter1.SelectedIndex = 0
        RefreshData()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(8) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(17) Then
            ExportDgvToExcel(dgvRec)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvRec.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvRec.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvRec.Rows.Count

            Dim oRow As DataGridViewRow = dgvRec.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvRec.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvRec.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvRec.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvRec.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvRec.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvRec.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvRec.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvRec.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmPayRecSearch_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPayRecSearch_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intMethod As Integer = 0
        Dim strKw As String = ""
        Dim intTotal As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"

        dtPayRec.Clear()

        If cboxFilter1.SelectedIndex > -1 Then
            intMethod = cboxFilter1.SelectedIndex
        Else
            intMethod = 0
        End If

        strKw = tboxFilter.Text

        If radbutNoDate.Checked Then
            dtPayRec = objCsol.ListPayStaByKeyword(intMethod, strKw)
        Else
            dtPayRec = objCsol.ListPayStaByKeyword(intMethod, strKw, _
                                                   GetDateStart(dtpickFrom.Value), _
                                                   GetDateEnd(dtpickTo.Value))
        End If

        dgvRec.DataSource = dtPayRec
        If dtPayRec.Rows.Count > 0 Then
            intTotal = dtPayRec.Compute(strCompute1, "")
        Else
            intTotal = 0
        End If

        tboxTotal.Text = Format(intTotal, "$#,##0")
        dgvRec.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
        LoadColumnText()
    End Sub

    Private Sub InitTable()

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvRec.Columns
            col.visible = False
        Next
        If dgvRec.Rows.Count > 0 Then

            dgvRec.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
            dgvRec.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvRec.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgvRec.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
            dgvRec.Columns.Item(c_StuIDColumnName).Visible = True
            dgvRec.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvRec.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
            dgvRec.Columns.Item(c_StuNameColumnName).Visible = True
            dgvRec.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvRec.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
            dgvRec.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvRec.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvRec.Columns.Item(c_AmountColumnName).DisplayIndex = 4
            dgvRec.Columns.Item(c_AmountColumnName).Visible = True
            dgvRec.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
            dgvRec.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
            dgvRec.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvRec.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvRec.Columns.Item(c_ExtraColumnName).DisplayIndex = 6
            dgvRec.Columns.Item(c_ExtraColumnName).Visible = True
            dgvRec.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra
        End If
    End Sub


    Private Sub InitSelections()

    End Sub

    Private Sub frmPayRecSearch_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshData()
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgvRec.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            frmMain.SetOkState(False)
            Dim id As Integer = dgvRec.SelectedRows(0).Cells(c_IDColumnName).Value
            Dim receipt As String = dgvRec.SelectedRows(0).Cells(c_ReceiptNumColumnName).Value
            Dim subclass As Integer = dgvRec.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            Dim amount As Integer = dgvRec.SelectedRows(0).Cells(c_AmountColumnName).Value
            Dim handler As String = dgvRec.SelectedRows(0).Cells(c_HandlerIDColumnName).Value
            Dim method As Integer = dgvRec.SelectedRows(0).Cells(c_PayMethodColumnName).Value
            Dim extra As String = dgvRec.SelectedRows(0).Cells(c_ExtraColumnName).Value
            Dim remarks As String = dgvRec.SelectedRows(0).Cells(c_RemarksColumnName).Value
            Dim strStuId As String = dgvRec.SelectedRows(0).Cells(c_StuIDColumnName).Value
            Dim frm As New frm2UpdateReceipt(id, receipt, strStuId, subclass, amount, _
                                            handler, method, extra, remarks)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                RefreshData()
            End If
        End If
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(23) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgvRec.SelectedRows.Count = 0 Then
            Exit Sub
        Else
            Dim result As MsgBoxResult = _
                        MsgBox(My.Resources.msgConfirmDeletePay, _
                                MsgBoxStyle.YesNo, My.Resources.msgTitle)
            If result = MsgBoxResult.Yes Then
                Dim id As Integer = dgvRec.SelectedRows(0).Cells(c_IDColumnName).Value
                Dim subclass As Integer = dgvRec.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
                Dim amount As Integer = dgvRec.SelectedRows(0).Cells(c_AmountColumnName).Value
                Dim strStuId As String = dgvRec.SelectedRows(0).Cells(c_StuIDColumnName).Value

                objCsol.DeletePayRec(id, strStuId, subclass, amount)
                RefreshData()
            End If
        End If
    End Sub
End Class