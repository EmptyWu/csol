﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmModifyClassRoom
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmModifyClassRoom))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.mnuCancelConvex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetDisStyle = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetConvex = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxRowCnt = New System.Windows.Forms.TextBox
        Me.lblThisMonth = New System.Windows.Forms.Label
        Me.mnuCancelNoSeat = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSetToNoSeat = New System.Windows.Forms.ToolStripMenuItem
        Me.Label3 = New System.Windows.Forms.Label
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.mnuRemovePw = New System.Windows.Forms.ToolStripMenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.tboxColCnt = New System.Windows.Forms.TextBox
        Me.mnuSave = New System.Windows.Forms.ToolStripMenuItem
        Me.Label9 = New System.Windows.Forms.Label
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuInsertPw = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.dgvSeat = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuCancelConvex
        '
        Me.mnuCancelConvex.Name = "mnuCancelConvex"
        resources.ApplyResources(Me.mnuCancelConvex, "mnuCancelConvex")
        '
        'mnuSetDisStyle
        '
        Me.mnuSetDisStyle.Name = "mnuSetDisStyle"
        resources.ApplyResources(Me.mnuSetDisStyle, "mnuSetDisStyle")
        '
        'mnuSetConvex
        '
        Me.mnuSetConvex.Name = "mnuSetConvex"
        resources.ApplyResources(Me.mnuSetConvex, "mnuSetConvex")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'tboxRowCnt
        '
        resources.ApplyResources(Me.tboxRowCnt, "tboxRowCnt")
        Me.tboxRowCnt.Name = "tboxRowCnt"
        '
        'lblThisMonth
        '
        resources.ApplyResources(Me.lblThisMonth, "lblThisMonth")
        Me.lblThisMonth.Name = "lblThisMonth"
        '
        'mnuCancelNoSeat
        '
        Me.mnuCancelNoSeat.Name = "mnuCancelNoSeat"
        resources.ApplyResources(Me.mnuCancelNoSeat, "mnuCancelNoSeat")
        '
        'mnuSetToNoSeat
        '
        Me.mnuSetToNoSeat.Name = "mnuSetToNoSeat"
        resources.ApplyResources(Me.mnuSetToNoSeat, "mnuSetToNoSeat")
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'tboxName
        '
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        '
        'mnuRemovePw
        '
        Me.mnuRemovePw.Name = "mnuRemovePw"
        resources.ApplyResources(Me.mnuRemovePw, "mnuRemovePw")
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'tboxColCnt
        '
        resources.ApplyResources(Me.tboxColCnt, "tboxColCnt")
        Me.tboxColCnt.Name = "tboxColCnt"
        '
        'mnuSave
        '
        Me.mnuSave.Name = "mnuSave"
        resources.ApplyResources(Me.mnuSave, "mnuSave")
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuSave, Me.mnuInsertPw, Me.mnuRemovePw, Me.mnuSetToNoSeat, Me.mnuCancelNoSeat, Me.mnuSetConvex, Me.mnuCancelConvex, Me.mnuSetDisStyle, Me.mnuPrint, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuInsertPw
        '
        Me.mnuInsertPw.Name = "mnuInsertPw"
        resources.ApplyResources(Me.mnuInsertPw, "mnuInsertPw")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'dgvSeat
        '
        Me.dgvSeat.AllowUserToAddRows = False
        Me.dgvSeat.AllowUserToDeleteRows = False
        Me.dgvSeat.AllowUserToResizeColumns = False
        Me.dgvSeat.AllowUserToResizeRows = False
        Me.dgvSeat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSeat.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvSeat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvSeat.DefaultCellStyle = DataGridViewCellStyle2
        resources.ApplyResources(Me.dgvSeat, "dgvSeat")
        Me.dgvSeat.MultiSelect = False
        Me.dgvSeat.Name = "dgvSeat"
        Me.dgvSeat.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvSeat.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvSeat.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing
        Me.dgvSeat.RowTemplate.Height = 24
        Me.dgvSeat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSeat.ShowCellErrors = False
        Me.dgvSeat.ShowCellToolTips = False
        Me.dgvSeat.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'frmModifyClassRoom
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.Controls.Add(Me.dgvSeat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxRowCnt)
        Me.Controls.Add(Me.lblThisMonth)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.tboxColCnt)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmModifyClassRoom"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuCancelConvex As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetDisStyle As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetConvex As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxRowCnt As System.Windows.Forms.TextBox
    Friend WithEvents lblThisMonth As System.Windows.Forms.Label
    Friend WithEvents mnuCancelNoSeat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSetToNoSeat As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents mnuRemovePw As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tboxColCnt As System.Windows.Forms.TextBox
    Friend WithEvents mnuSave As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuInsertPw As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvSeat As System.Windows.Forms.DataGridView
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
