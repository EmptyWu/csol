﻿Public Class frm2AddJournalRec
    Private dtGrp As New DataTable
    Private lstId As New ArrayList

    Public Sub New()

        InitializeComponent()

        RefreshData()

    End Sub

    Private Sub RefreshData()
        dtGrp = frmMain.GetJournalGrp
        For index As Integer = 0 To dtGrp.Rows.Count - 1
            cboxGrp.Items.Add(dtGrp.Rows(index).Item(c_NameColumnName).trim)
            lstId.Add(dtGrp.Rows(index).Item(c_IDColumnName))
        Next

        If cboxGrp.Items.Count > 0 Then
            cboxGrp.SelectedIndex = 0
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If cboxGrp.SelectedIndex > -1 Then
            If IsNumeric(tboxAmount.Text) Then
                If CInt(tboxAmount.Text) > 0 Then
                    objCsol.AddJournalRec(lstId(cboxGrp.SelectedIndex), _
                                          tboxItem.Text, CInt(tboxAmount.Text), _
                                          dtpickDate.Value, frmMain.GetUsrId, _
                                          tboxNum.Text, tboxCheque.Text, _
                                      tboxRemark3.Text, tboxRemark4.Text, _
                                      tboxRemark5.Text)
                    Me.Close()
                Else
                    ShowMsg()
                End If
            Else
                ShowMsg()
            End If
        Else
            ShowMsg()
        End If
    End Sub

    Private Sub ShowMsg()
        MsgBox(My.Resources.msgWrongJournalFormat, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class