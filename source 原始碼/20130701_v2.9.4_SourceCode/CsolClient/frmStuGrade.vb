﻿Public Class frmStuGrade
    Private dtPaper As New DataTable
    Private dtSubClass As New DataTable
    Private dtClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstPaper As New ArrayList

    Private dtData As New DataTable
    Private ds As New DataSet
    Private intClass As Integer

    Private dtSchoolType As New DataTable
    Private dtSchool As New DataTable
    Private lstSch As New ArrayList
    Private dtPaper2 As New DataTable
    Private strOldMark As String = ""
    Private strOldRemarks As String = ""

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Try
            Me.Text = My.Resources.frmStuGrade          

            ds = frmMain.GetClassInfoSet
            dtClass = ds.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                                                                                  c_NameColumnName, c_EndColumnName)
            dtSubClass = ds.Tables(c_SubClassListDataTableName)

            InitList()
            InitSchList()
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub InitList()
        cboxClass.Items.Clear()
        lstClass.Clear()

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If GetDateEnd(dtClass.Rows(index).Item(c_EndColumnName)) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        If lstClass.Count > 0 Then
            cboxClass.SelectedIndex = 0
        Else
            cboxClass.SelectedIndex = -1
        End If


    End Sub

    Private Sub InitSchList()
        dtSchoolType = frmMain.GetSchTypes
        dtSchool = frmMain.GetSchList
        cboxSchool.Items.Clear()
        For index As Integer = 0 To dtSchoolType.Rows.Count - 1
            cboxSchool.Items.Add(dtSchoolType.Rows(index).Item(c_NameColumnName))
            lstSch.Add(dtSchoolType.Rows(index).Item(c_IDColumnName))
        Next
        If dtSchoolType.Rows.Count > 1 Then
            cboxSchool.SelectedIndex = 2
        End If
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        RefreshPaperList()
    End Sub

    Private Sub RefreshPaperList()
        Try
            cboxSubClass.Items.Clear()
            lstSubClass.Clear()
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            If lstClass.Count = 0 Then
                Exit Sub
            End If
            intClass = lstClass(cboxClass.SelectedIndex)
            If intClass > -1 Then
                For Each row As DataRow In dtSubClass.Rows
                    If row.Item(c_ClassIDColumnName) = intClass Then
                        lstSubClass.Add(row.Item(c_IDColumnName))
                        cboxSubClass.Items.Add(row.Item(c_NameColumnName))
                    End If
                Next

                dtPaper = objCsol.GetClassPaperList(intClass)
                dtPaper2 = dtPaper.DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName)
                If dtPaper.Rows.Count = 0 Then
                    cboxPaper.Items.Clear()
                    lstPaper.Clear()
                    cboxPaper.Text = ""
                    dgv.DataSource = Nothing
                End If
            End If

            cboxSubClass.SelectedIndex = 0
            RefreshPaperShownList()

        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        RefreshPaperShownList()
    End Sub

    Private Sub RefreshPaperShownList()
        Try
            cboxPaper.Items.Clear()
            lstPaper.Clear()
            cboxPaper.Text = ""
            dgv.DataSource = Nothing
            If cboxSubClass.SelectedIndex > -1 Then

                If cboxSubClass.SelectedIndex = 0 Then
                    For Each row As DataRow In dtPaper2.Rows
                        cboxPaper.Items.Add(row.Item(c_NameColumnName))
                        lstPaper.Add(row.Item(c_IDColumnName))
                    Next
                Else
                    Dim id As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                    For Each row As DataRow In dtPaper.Rows
                        If row.Item(c_SubClassIDColumnName) = id Then
                            cboxPaper.Items.Add(row.Item(c_NameColumnName))
                            lstPaper.Add(row.Item(c_IDColumnName))
                        End If
                    Next
                End If
                If lstPaper.Count > 0 Then
                    cboxPaper.SelectedIndex = 0
                Else
                    cboxPaper.SelectedIndex = -1
                End If
            Else
                dgv.DataSource = Nothing
            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub


    Private Sub cboxPaper_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxPaper.SelectedIndexChanged
        RefreshData()
    End Sub

    Private Sub RefreshData()
        Try
            If cboxPaper.SelectedIndex > -1 Then
                Dim paper As Integer = lstPaper(cboxPaper.SelectedIndex)
                Dim lst As New ArrayList
                Dim sc As Integer = 0

                If cboxSubClass.SelectedIndex = 0 Then
                    For i As Integer = 1 To lstSubClass.Count - 1
                        sc = lstSubClass(i)
                        If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                           c_IDColumnName & "=" & paper.ToString) > 0 Then
                            lst.Add(sc)
                        End If
                    Next

                ElseIf cboxSubClass.SelectedIndex > 0 Then
                    sc = lstSubClass(cboxSubClass.SelectedIndex)
                    If dtPaper.Compute("COUNT (" & c_IDColumnName & ")", c_SubClassIDColumnName & "=" & sc.ToString & " AND " & _
                                       c_IDColumnName & "=" & paper.ToString) > 0 Then
                        lst.Add(sc)
                    End If
                End If
                dtData = objCsol.GetStuGrades(paper, lst).DefaultView.ToTable(True, c_SubClassIDColumnName, _
                                                                                    c_SchoolColumnName, _
                                                                                    c_PrimarySchColumnName, _
                                                                                    c_JuniorSchColumnName, _
                                                                                    c_HighSchColumnName, _
                                                                                    c_UniversityColumnName, _
                                                                                    c_IDColumnName, _
                                                                                    c_NameColumnName, _
                                                                                    c_EngNameColumnName, _
                                                                                    c_SeatNumColumnName, _
                                                                                    c_Cust2ColumnName, _
                                                                                    c_MarkColumnName, _
                                                                                    c_RemarksColumnName, _
                                                                                    c_DateTimeColumnName, _
                                                                                    c_HandlerColumnName, _
                                                                                    c_SubClassNameColumnName, _
                                                                                    c_CancelColumnName)                     '100318


                If cboxSubClass.SelectedIndex > 0 Then
                    dtData.DefaultView.RowFilter = c_SubClassIDColumnName & "=" & lstSubClass(cboxSubClass.SelectedIndex).ToString
                End If
                dtData.DefaultView.RowFilter = "Cancel=0"                                                                   '100318
                dgv.DataSource = dtData.DefaultView
                LoadColumnText()

            End If
        Catch ex As ApplicationException
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub RefreshSchDisplay()
        Dim sch As Integer = 0
        If cboxSchool.SelectedIndex > -1 Then
            sch = lstSch(cboxSchool.SelectedIndex)
        End If
        Select Case sch
            Case 1
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_PrimarySchColumnName).Value)
                Next
            Case 2
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_JuniorSchColumnName).Value)
                Next
            Case 3
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_HighSchColumnName).Value)
                Next
            Case 4
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_UniversityColumnName).Value)
                Next
            Case Else
                For Each row As DataGridViewRow In dgv.Rows
                    row.Cells(c_SchoolColumnName).Value = GetSchoolName(row.Cells(c_PrimarySchColumnName).Value)
                Next
        End Select
    End Sub

    Private Sub LoadColumnText()
        For Each col As DataGridViewColumn In dgv.Columns
            col.Visible = False
            col.ReadOnly = True
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            If radbutChineseName.Checked Then
                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.name
            ElseIf radbutEngName.Checked Then
                dgv.Columns.Item(c_EngNameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_EngNameColumnName).Visible = True
                dgv.Columns.Item(c_EngNameColumnName).HeaderText = My.Resources.engName
            End If
            dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_SeatNumColumnName).Visible = True
            dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgv.Columns.Item(c_Cust2ColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_Cust2ColumnName).Visible = True
            dgv.Columns.Item(c_Cust2ColumnName).HeaderText = My.Resources.classNum

            RefreshSchDisplay
            dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_SchoolColumnName).Visible = True
            dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgv.Columns.Item(c_MarkColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_MarkColumnName).Visible = True
            dgv.Columns.Item(c_MarkColumnName).HeaderText = My.Resources.mark
            dgv.Columns.Item(c_MarkColumnName).ReadOnly = False
            dgv.Columns.Item(c_RemarksColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_RemarksColumnName).Visible = True
            dgv.Columns.Item(c_RemarksColumnName).HeaderText = My.Resources.remarks
            dgv.Columns.Item(c_RemarksColumnName).ReadOnly = False
            dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 7
            dgv.Columns.Item(c_DateTimeColumnName).Visible = True
            dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.inputDate
            dgv.Columns.Item(c_DateTimeColumnName).DefaultCellStyle.Format = "d"
            dgv.Columns.Item(c_HandlerColumnName).DisplayIndex = 8
            dgv.Columns.Item(c_HandlerColumnName).Visible = True
            dgv.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.inputBy
            If lstSubClass.Count > 0 And cboxSubClass.SelectedIndex = 0 Then
                dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 9
                dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            End If
        End If
    End Sub

    Private Function GetSchoolName(ByVal i As Integer) As String
        Dim r As String = ""
        If dtSchool.Rows.Count > 0 Then
            For index As Integer = 0 To dtSchool.Rows.Count - 1
                If dtSchool.Rows(index).Item(c_IDColumnName) = i Then
                    r = dtSchool.Rows(index).Item(c_NameColumnName)
                    Exit For
                End If
            Next
        End If
        Return r
    End Function

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        InitList()
    End Sub

    Private Sub cboxSchool_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboxSchool.SelectedIndexChanged
        If cboxSchool.SelectedIndex > -1 Then
            RefreshSchDisplay()
        End If
    End Sub

    Private Sub radbutChineseName_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles radbutChineseName.CheckedChanged
        LoadColumnText()
    End Sub

    Private Sub butNewPaper_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butNewPaper.Click
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2ExamPaper(-1, True, "", "", Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshPaperList()
            dgv.DataSource = Nothing
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuNew_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNew.Click
        Dim lst As New ArrayList
        frmMain.SetOkState(False)
        Dim frm As New frm2ExamPaper(-1, True, "", "", Now, 0, lst)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshPaperList()
            dgv.DataSource = Nothing
            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuImport.Click
        frmMain.SetOkState(False)
        Dim frm As New frm2ImportGrades
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshData()

            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmStuGrade_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmStuGrade_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuGrade_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        dgv.BeginEdit(True)
    End Sub

    Private Sub dgv_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        Dim intC As Integer
        Dim intR As Integer
        Dim stuid As String = ""
        Dim paper As Integer
        Dim mark As Single
        Dim remarks As String = ""
        Try
            intC = e.ColumnIndex
            intR = e.RowIndex
            If intC = 4 Or 5 Then
                stuid = dgv.Rows(intR).Cells(c_IDColumnName).Value
                paper = lstPaper(cboxPaper.SelectedIndex)
                If DBNull.Value.Equals(dgv.Rows(intR).Cells(c_MarkColumnName).Value) Then
                    If DBNull.Value.Equals(dgv.Rows(intR).Cells(c_RemarksColumnName).Value) Then
                        objCsol.DeleteStuGrade(stuid, paper)
                        dgv.Rows(intR).Cells(c_RemarksColumnName).Value = DBNull.Value
                        dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = DBNull.Value
                        dgv.Rows(intR).Cells(c_HandlerColumnName).Value = DBNull.Value
                        Exit Sub
                    ElseIf dgv.Rows(intR).Cells(c_RemarksColumnName).Value.ToString.Trim = "" Then
                        objCsol.DeleteStuGrade(stuid, paper)
                        dgv.Rows(intR).Cells(c_RemarksColumnName).Value = DBNull.Value
                        dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = DBNull.Value
                        dgv.Rows(intR).Cells(c_HandlerColumnName).Value = DBNull.Value
                        Exit Sub
                    Else
                        mark = c_NullMark
                        remarks = dgv.Rows(intR).Cells(c_RemarksColumnName).Value.ToString.Trim
                    End If
                Else
                    mark = CSng(dgv.Rows(intR).Cells(c_MarkColumnName).Value)
                    remarks = GetDbString(dgv.Rows(intR).Cells(c_RemarksColumnName).Value)
                End If
            End If

        Catch ex As Exception
            AddErrorLog(ex.ToString)
            Exit Sub
        End Try

        Try
            If stuid.Length = 8 Then
                If radbutNow.Checked Then
                    objCsol.UpdateStuGrade(stuid, paper, mark, Now, remarks, frmMain.GetUsrId)
                    dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = Now
                Else
                    objCsol.UpdateStuGrade(stuid, paper, mark, dtPickInput.Value, remarks, frmMain.GetUsrId)
                    dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = dtPickInput.Value
                End If

                dgv.Rows(intR).Cells(c_HandlerColumnName).Value = frmMain.GetUsrName
            End If

        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try


    End Sub

    Private Sub tboxID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged
        If tboxID.Text.Trim.Length = 8 Then
            Dim stuid As String = tboxID.Text.Trim
            tboxName.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_IDColumnName) = stuid Then
                    tboxIDName.Text = row.Item(c_NameColumnName)
                    If Not DBNull.Value.Equals(row.Item(c_MarkColumnName)) Then
                        tboxIDMark.Text = row.Item(c_MarkColumnName).ToString
                        strOldMark = tboxIDMark.Text
                    End If
                    If Not DBNull.Value.Equals(row.Item(c_RemarksColumnName)) Then
                        tboxIDRemarks.Text = row.Item(c_RemarksColumnName).ToString
                        strOldRemarks = tboxIDRemarks.Text
                    End If
                    flag = True
                    Exit For
                End If
            Next
            tboxIDMark.Focus()
            If flag = False Then
                ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
            End If

        End If
    End Sub

    Private Sub tboxName_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxName.TextChanged
        If tboxName.Text.Trim.Length > 1 Then
            Dim stuname As String = tboxName.Text.Trim
            tboxNameID.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_NameColumnName) = stuname Then
                    tboxNameID.Text = row.Item(c_IDColumnName)
                    tboxNameMark.Focus()
                    If Not DBNull.Value.Equals(row.Item(c_MarkColumnName)) Then
                        tboxNameMark.Text = row.Item(c_MarkColumnName).ToString
                        strOldMark = tboxNameMark.Text
                    End If
                    If Not DBNull.Value.Equals(row.Item(c_RemarksColumnName)) Then
                        tboxNameRemarks.Text = row.Item(c_RemarksColumnName).ToString
                        strOldRemarks = tboxNameRemarks.Text
                    End If
                    flag = True
                    Exit For
                End If
            Next
        End If
    End Sub

    Private Sub tboxSeatNum_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxSeatNum.TextChanged
        If tboxSeatNum.Text.Trim.Length > 1 Then
            Dim seat As String = tboxSeatNum.Text.Trim.ToUpper
            tboxSeatName.Text = ""
            Dim flag As Boolean = False
            For Each row As DataRow In dtData.Rows
                If row.Item(c_SeatNumColumnName).ToString.Trim.ToUpper.Equals(seat) Then
                    tboxSeatName.Text = row.Item(c_NameColumnName)
                    tboxSeatMark.Focus()
                    If Not DBNull.Value.Equals(row.Item(c_MarkColumnName)) Then
                        tboxSeatMark.Text = row.Item(c_MarkColumnName).ToString
                        strOldMark = tboxSeatMark.Text
                    End If
                    If Not DBNull.Value.Equals(row.Item(c_RemarksColumnName)) Then
                        tboxSeatRemarks.Text = row.Item(c_RemarksColumnName).ToString
                        strOldRemarks = tboxSeatRemarks.Text
                    End If
                    flag = True
                    Exit For
                End If
            Next

        End If
    End Sub

    Private Sub tboxIDMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxIDMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeID()
            tboxID.Clear()
            tboxIDMark.Clear()
            tboxIDName.Clear()
            tboxIDRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
            tboxID.Focus()
        End If

    End Sub

    '20100511 sherry 13
    Private Sub RefreshStuGrade(ByVal stuid As String, ByVal mark As Single, _
                                ByVal remarks As String)
        Dim intR As Integer = -1
        For i As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(i).Cells(c_IDColumnName).Value.ToString.Trim.Equals(stuid) Then
                intR = i
                Exit For
            End If
        Next

        If intR > -1 Then
            Dim d As Date
            If radbutNow.Checked Then
                d = Now
            Else
                d = dtPickInput.Value
            End If

            dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = d
            dgv.Rows(intR).Cells(c_MarkColumnName).Value = mark
            dgv.Rows(intR).Cells(c_RemarksColumnName).Value = remarks
            dgv.Rows(intR).Cells(c_HandlerColumnName).Value = frmMain.GetUsrName
        End If

    End Sub

    '20100511 sherry 13
    Private Sub DelStuGrade(ByVal stuid As String)
        Dim intR As Integer = -1
        For i As Integer = 0 To dgv.Rows.Count - 1
            If dgv.Rows(i).Cells(c_IDColumnName).Value.ToString.Trim.Equals(stuid) Then
                intR = i
                Exit For
            End If
        Next

        If intR > -1 Then
            dgv.Rows(intR).Cells(c_DateTimeColumnName).Value = DBNull.Value
            dgv.Rows(intR).Cells(c_MarkColumnName).Value = DBNull.Value
            dgv.Rows(intR).Cells(c_RemarksColumnName).Value = DBNull.Value
            dgv.Rows(intR).Cells(c_HandlerColumnName).Value = DBNull.Value
        End If

    End Sub

    Private Sub UpdGradeID()
        Try
            Dim flag As Boolean = False
            Dim paper As Integer = lstPaper(cboxPaper.SelectedIndex)
            Dim mark As Single
            If tboxIDMark.Text.Trim.Length = 0 Then
                mark = c_NullMark
            Else
                mark = CSng(tboxIDMark.Text.Trim)
            End If

            Dim remarks As String = tboxIDRemarks.Text.Trim
            Dim stuid As String = tboxID.Text.Trim

            If stuid.Length = 8 Then
                For Each row As DataRow In dtData.Rows
                    If row.Item(c_IDColumnName) = stuid Then
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Then
                    ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
                    Exit Sub
                Else
                    If strOldMark.Length <> 0 Or strOldRemarks.Length <> 0 Then
                        Dim result As MsgBoxResult = MsgBox(My.Resources.msgConfirmModGrade, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
                        If result = MsgBoxResult.No Then
                            Exit Sub
                        End If

                        If tboxIDMark.Text.Trim.Length = 0 And tboxIDRemarks.Text.Trim.Length = 0 Then
                            objCsol.DeleteStuGrade(stuid, paper)
                            '20100511 sherry 13
                            DelStuGrade(stuid)
                            Exit Sub
                        End If
                    End If
                End If

                If radbutNow.Checked Then
                    objCsol.UpdateStuGrade(stuid, paper, mark, Now, remarks, frmMain.GetUsrId)
                Else
                    objCsol.UpdateStuGrade(stuid, paper, mark, dtPickInput.Value, remarks, frmMain.GetUsrId)
                End If
                '20100511 sherry 13
                RefreshStuGrade(stuid, mark, remarks)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub UpdGradeName()
        Try
            Dim flag As Boolean = False
            Dim paper As Integer = lstPaper(cboxPaper.SelectedIndex)
            Dim mark As Single
            If tboxNameMark.Text.Trim.Length = 0 Then
                mark = c_NullMark
            Else
                mark = CSng(tboxNameMark.Text.Trim)
            End If
            Dim remarks As String = tboxNameRemarks.Text.Trim
            Dim stuid As String = tboxNameID.Text.Trim

            If stuid.Length = 8 Then
                If strOldMark.Length <> 0 Or strOldRemarks.Length <> 0 Then
                    Dim result As MsgBoxResult = MsgBox(My.Resources.msgConfirmModGrade, _
                        MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
                    If result = MsgBoxResult.No Then
                        Exit Sub
                    End If

                    If tboxNameMark.Text.Trim.Length = 0 And tboxNameRemarks.Text.Trim.Length = 0 Then
                        objCsol.DeleteStuGrade(stuid, paper)
                        DelStuGrade(stuid)
                        Exit Sub
                    End If
                End If

                If radbutNow.Checked Then
                    objCsol.UpdateStuGrade(stuid, paper, mark, Now, remarks, frmMain.GetUsrId)
                Else
                    objCsol.UpdateStuGrade(stuid, paper, mark, dtPickInput.Value, remarks, frmMain.GetUsrId)
                End If
                '20100511 sherry 13
                RefreshStuGrade(stuid, mark, remarks)
            Else
                ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub UpdGradeSeat()
        Try
            Dim flag As Boolean = False
            Dim paper As Integer = lstPaper(cboxPaper.SelectedIndex)
            Dim mark As Single
            If tboxSeatMark.Text.Trim.Length = 0 Then
                mark = c_NullMark
            Else
                mark = CSng(tboxSeatMark.Text.Trim)
            End If
            Dim remarks As String = tboxSeatRemarks.Text.Trim
            Dim seat As String = tboxSeatNum.Text.Trim.ToUpper
            Dim stuid As String = ""

            If seat.Length > 1 Then
                For Each row As DataRow In dtData.Rows
                    If row.Item(c_SeatNumColumnName).ToString.ToUpper = seat Then
                        stuid = row.Item(c_IDColumnName)
                        flag = True
                        Exit For
                    End If
                Next
                If flag = False Or stuid.Length <> 8 Then
                    ShowInfo(My.Resources.msgNoSuchStuIDOrClass)
                    Exit Sub
                Else
                    If strOldMark.Length <> 0 Or strOldRemarks.Length <> 0 Then
                        Dim result As MsgBoxResult = MsgBox(My.Resources.msgConfirmModGrade, _
                            MsgBoxStyle.YesNo, My.Resources.msgYesNoTitle)
                        If result = MsgBoxResult.No Then
                            Exit Sub
                        End If

                        If tboxSeatMark.Text.Trim.Length = 0 And tboxSeatRemarks.Text.Trim.Length = 0 Then
                            objCsol.DeleteStuGrade(stuid, paper)
                            DelStuGrade(stuid)
                            Exit Sub
                        End If
                    End If
                End If

                If radbutNow.Checked Then
                    objCsol.UpdateStuGrade(stuid, paper, mark, Now, remarks, frmMain.GetUsrId)
                Else
                    objCsol.UpdateStuGrade(stuid, paper, mark, dtPickInput.Value, remarks, frmMain.GetUsrId)
                End If
                '20100511 sherry 13
                RefreshStuGrade(stuid, mark, remarks)
            End If
        Catch ex As Exception
            ShowInfo(My.Resources.msgInvalidData)
            AddErrorLog(ex.ToString)
        End Try
    End Sub

    Private Sub tboxIDRemarks_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxIDRemarks.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeID()
            tboxID.Clear()
            tboxIDMark.Clear()
            tboxIDName.Clear()
            tboxIDRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
        End If
    End Sub


    Private Sub tboxNameMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxNameMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeName()
            tboxName.Clear()
            tboxNameMark.Clear()
            tboxNameID.Clear()
            tboxNameRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
            tboxName.Clear()
            tboxName.Focus()
        End If
    End Sub

    Private Sub tboxNameRemarks_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxNameRemarks.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeName()
            tboxName.Clear()
            tboxNameMark.Clear()
            tboxNameID.Clear()
            tboxNameRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
        End If
    End Sub

    Private Sub tboxSeatMark_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxSeatMark.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeSeat()
            tboxSeatMark.Clear()
            tboxSeatName.Clear()
            tboxSeatRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
            tboxSeatNum.Clear()
            tboxSeatNum.Focus()
        End If
    End Sub

    Private Sub tboxSeatRemarks_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tboxSeatRemarks.KeyDown
        If e.KeyCode = Keys.Enter Then
            UpdGradeSeat()
            tboxSeatMark.Clear()
            tboxSeatName.Clear()
            tboxSeatRemarks.Clear()
            strOldMark = ""
            strOldRemarks = ""
        End If
    End Sub

End Class