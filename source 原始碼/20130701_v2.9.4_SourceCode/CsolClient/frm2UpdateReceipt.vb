﻿Public Class frm2UpdateReceipt
    Dim dtHandler As New DataTable
    Dim lstHId As New ArrayList
    Dim intId As Integer
    Dim strStuId As String
    Dim intSubclass As Integer
    Dim intAmount As Integer
    Dim strHandler As String
    Dim strRemarks As String
    Dim intMethod As Integer
    Dim intOldAmount As Integer

    Public Sub New(ByVal id As Integer, _
                ByVal ReceiptNum As String, ByVal StuID As String, _
                 ByVal SubClassID As Integer, ByVal Amount As Integer, _
                 ByVal HandlerID As String, _
                 ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String)

        InitializeComponent()
        tboxReceipt.Text = ReceiptNum
        intMethod = PayMethod
        strHandler = HandlerID
        strRemarks = Remarks
        strStuId = StuID
        intSubclass = SubClassID
        intId = id
        cboxPayMethod.Items.Add(My.Resources.cash)
        cboxPayMethod.Items.Add(My.Resources.cheque)
        cboxPayMethod.Items.Add(My.Resources.transfer)
        cboxPayMethod.Items.Add("其他")

        If PayMethod > 0 And PayMethod < 6 Then
            cboxPayMethod.SelectedIndex = PayMethod - 1
        End If

        dtHandler = frmMain.GetSalesList
        For index As Integer = 0 To dtHandler.Rows.Count - 1
            cboxHandler.Items.Add(dtHandler.Rows(index).Item(c_NameColumnName))
            lstHId.Add(dtHandler.Rows(index).Item(c_IDColumnName))
        Next
        If lstHId.IndexOf(HandlerID) > -1 Then
            cboxHandler.SelectedIndex = lstHId.IndexOf(HandlerID)
        End If
        tboxAmount.Text = Amount.ToString
        intOldAmount = Amount
        tboxExtra.Text = Extra
        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not frmMain.CheckAuth(22) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If IsNumeric(tboxAmount.Text) Then
            If CInt(tboxAmount.Text) >= 0 Then
                If cboxPayMethod.SelectedIndex > -1 Then
                    intMethod = cboxPayMethod.SelectedIndex + 1
                End If
                If cboxHandler.SelectedIndex > -1 Then
                    strHandler = lstHId(cboxHandler.SelectedIndex)
                End If
                strRemarks = My.Resources.payRecMod & ", " & My.Resources.oldAmount _
                    & "=" & intOldAmount & ", " & My.Resources.newAmount _
                    & "=" & tboxAmount.Text.Trim
                objCsol.UpdatePayRec(intId, tboxReceipt.Text, strStuId, _
                                    intSubclass, CInt(tboxAmount.Text), _
                                    Now, strHandler.Trim, frmMain.GetUsrName, _
                                    intMethod, tboxExtra.Text.Trim, _
                                    strRemarks, tboxReason.Text.Trim, intOldAmount)
                frmMain.SetOkState(True)
                Me.Close()

            Else
                ShowMsg(My.Resources.msgWrontAmount)
            End If
        Else
            ShowMsg(My.Resources.msgWrontAmount)
        End If
    End Sub

    Private Sub ShowMsg(ByVal msg As String)
        MsgBox(msg, MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub
End Class