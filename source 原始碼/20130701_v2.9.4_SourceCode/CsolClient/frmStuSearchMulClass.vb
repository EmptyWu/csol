﻿Public Class frmStuSearchMulClass
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private lstClassShown As New ArrayList
    Private lstSubClassShown As New ArrayList
    Private dateNow As Date = Now
    Private dtStu2 As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuSearchMulClass
    Private sUserName As String = frmMain.GetUsrName
    Private rowNum As Integer

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmStuSearchMulClass
        InitClassList()
        ShowClassList()
        RefreshData()
        InitSelections()
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub frmStuSearchMulClass_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuSearchMulClass_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()

        Try
            dtStu = New DataTable
            If lstSubClass.Count > 0 Then
                For i As Integer = 0 To lstSubClass.Count - 1
                    Dim RequestParams As New System.Collections.Specialized.NameValueCollection
                    RequestParams.Add("SubClassID", lstSubClass(i).ToString)
                    Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", "GetStuMulClass", RequestParams, System.Text.Encoding.UTF8)
                    Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                    Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("StuMulClassInfo"))
                    dtStu.Merge(dt)
                Next
            End If
            'dgv.DataSource = dtStu
            'dtStu = objCsol.ListStuBySubClass(lstSubClass)

            'dgv.DataSource = dtStu

            For Each row As DataRow In dtStu.Rows()
                'Select Case row.Item("Schgroup").ToString.Trim()
                '    Case "-1"
                '        row.Item("Schgroup") = ""
                '        Continue For
                '    Case "0"
                '        row.Item("Schgroup") = 1
                '        Continue For
                '    Case "1"
                '        row.Item("Schgroup") = 2
                '        Continue For
                '    Case "2"
                '        row.Item("Schgroup") = 3
                '        Continue For
                '    Case "3"
                '        row.Item("Schgroup") = 4
                '        Continue For
                '    Case Else

                'End Select
                Dim key_Group As String = row.Item("Schgroup").ToString.Trim()
                Dim key_Current As String = row.Item("CurrentSch").ToString.Trim()
                Dim key_Grade As String = row.Item("SchoolGrade").ToString.Trim()
                Select Case key_Group
                    Case "-1"
                        row.Item("Schgroup") = ""
                    Case "0"
                        row.Item("Schgroup") = 1
                    Case "1"
                        row.Item("Schgroup") = 2
                    Case "2"
                        row.Item("Schgroup") = 3
                    Case "3"
                        row.Item("Schgroup") = 4
                    Case Else
                End Select

                Select Case key_Current
                    Case "0"
                        'row.Item("SchoolGrade") = 1
                        Continue For
                    Case "1"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case "3"
                                row.Item("SchoolGrade") = 4
                                Continue For
                            Case "4"
                                row.Item("SchoolGrade") = 5
                                Continue For
                            Case "5"
                                row.Item("SchoolGrade") = 6
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "2"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "3"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "4"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case "3"
                                row.Item("SchoolGrade") = 4
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case Else
                        Continue For
                End Select
            Next
        Catch ex As Exception
            CSOL.Logger.LogError(ex.ToString())
        End Try

        MakeTable()
    End Sub

    Private Sub MakeTable()

        Try

            If dtStu.Rows.Count = 0 Then
                dgv.DataSource = Nothing
                dgv.Rows.Clear()
                Exit Sub
            End If
            dtStu2.Clear()
            If radbutShowAll.Checked Then
                dtStu2 = dtStu.Clone
                dtStu2.Merge(dtStu)
                dtStu2.DefaultView.RowFilter = ""

            ElseIf radbutShowOne.Checked Or radbutShow1Sbj.Checked Then

                dtStu2 = dtStu.Clone
                dtStu2.Merge(dtStu)
                dtStu2 = dtStu.DefaultView.ToTable(True)
                dtStu2.Columns.Add(c_SubjectColumnName, GetType(System.Int32))
                Dim id As String = ""
                Dim sc As String = ""
                Dim c As Integer = 0
                For index As Integer = 0 To dtStu2.Rows.Count - 1
                    id = dtStu2.Rows(index).Item(c_IDColumnName)
                    sc = ""
                    c = 0
                    For j As Integer = 0 To dtStu.Rows.Count - 1
                        If dtStu.Rows(j).Item(c_IDColumnName) = id Then
                            If sc.Length > 0 Then
                                sc = sc & ", " & dtStu.Rows(j).Item(c_SubClassNameColumnName)
                            Else
                                sc = dtStu.Rows(j).Item(c_SubClassNameColumnName)
                            End If
                            c = c + 1
                        End If
                    Next
                    dtStu2.Rows(index).Item(c_SubjectColumnName) = c
                    dtStu2.Rows(index).Item(c_SubClassNameColumnName) = sc
                Next
                If radbutShow1Sbj.Checked Then
                    dtStu2.DefaultView.RowFilter = c_SubjectColumnName & "=1 AND " & _
                        c_SubClassNameColumnName & "='" & cboxSubject.SelectedItem & "'"
                Else
                    dtStu2.DefaultView.RowFilter = ""
                End If

            End If
            If Not chkboxShowCancel.Checked And Not dtStu2.Rows.Count = 0 Then
                If dtStu2.DefaultView.RowFilter = "" Then
                    dtStu2.DefaultView.RowFilter = c_CancelColumnName & "=0"
                Else
                    dtStu2.DefaultView.RowFilter = dtStu2.DefaultView.RowFilter & _
                            " AND " & c_CancelColumnName & "=0"
                End If
            End If

            If radbutShowOne.Checked And dtStu2.Rows.Count > 0 Then
                Dim dtShownOne As New DataTable
                Dim arrShownOne As New ArrayList
                dtShownOne = dtStu2.Clone()
                dtShownOne.Clear()
                For i As Integer = 0 To dtStu2.Rows.Count - 1
                    Dim strID As String = dtStu2.Rows(i).Item(c_IDColumnName)
                    If Not arrShownOne.Contains(strID) Then
                        arrShownOne.Add(strID)
                        dtShownOne.Rows.Add(dtStu2.Rows(i).ItemArray)
                    End If
                Next
                dgv.DataSource = dtShownOne
            ElseIf radbutShow1Sbj.Checked And dtStu2.Rows.Count > 0 Then
                Dim dtShow1Sbj As New DataTable
                dtShow1Sbj = dtStu2.Clone
                dtShow1Sbj.Clear()
                Dim strArr() As String
                For i As Integer = 0 To dtStu2.Rows.Count - 1
                    strArr = dtStu2.Rows(i).Item(c_SubClassNameColumnName).ToString.Split(",")
                    If strArr.Length = 1 Then
                        dtShow1Sbj.Rows.Add(dtStu2.Rows(i).ItemArray)
                    End If
                Next
                dgv.DataSource = dtShow1Sbj
            Else
                dgv.DataSource = dtStu2
                'dgv.DataSource = dtStu
            End If
            rowNum = dgv.RowCount
            tboxCount.Text = "1/" + rowNum.ToString

        Catch ex As Exception
            CSOL.Logger.LogError(ex.ToString)
        End Try
        LoadColumnText()
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgv.Columns.Item(c_SeatNumColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_SeatNumColumnName).Visible = True
            dgv.Columns.Item(c_SeatNumColumnName).HeaderText = My.Resources.seatNum
            dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_Tel1ColumnName).Visible = True
            dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgv.Columns.Item(c_Tel2ColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_Tel2ColumnName).Visible = True
            dgv.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            Try
                Dim i As Integer = 6
                For index As Integer = 0 To lstColName.Count - 1
                    If lstColShow(index) = 1 Then
                        dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                        dgv.Columns.Item(lstColName(index)).Visible = True
                        dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                        i = i + 1
                    End If
                Next
            Catch ex As Exception
                AddErrorLog(ex.ToString)
            End Try
            If dgv.Columns("CardNum").Visible = True Then
                For j As Integer = 0 To dgv.Rows.Count - 1
                    Dim str As String = dgv.Rows(j).Cells("CardNum").Value.ToString.Trim
                    If str = "" Then
                        dgv.Rows(j).Cells("CardNum").Value = "否"
                    ElseIf str = "否" Then
                        dgv.Rows(j).Cells("CardNum").Value = "否"
                    Else
                        dgv.Rows(j).Cells("CardNum").Value = "是"
                    End If
                Next                
            End If
            If dgv.Columns("SchGroup").Visible = True Then
                'For k As Integer = 0 To dgv.Rows.Count - 1
                '    If dgv.Rows(k).Cells("SchGroup").Value.ToString = -1 Then
                '        dgv.Rows(k).Cells("SchGroup").Value = ""
                '    ElseIf dgv.Rows(k).Cells("SchGroup").Value.ToString = 0 Then
                '        dgv.Rows(k).Cells("SchGroup").Value = 1
                '    ElseIf dgv.Rows(k).Cells("SchGroup").Value.ToString = 1 Then
                '        dgv.Rows(k).Cells("SchGroup").Value = 2
                '    ElseIf dgv.Rows(k).Cells("SchGroup").Value.ToString = 2 Then
                '        dgv.Rows(k).Cells("SchGroup").Value = 3
                '    ElseIf dgv.Rows(k).Cells("SchGroup").Value.ToString = 3 Then
                '        dgv.Rows(k).Cells("SchGroup").Value = 4
                '    End If
                'Next
            End If
            If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
            If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
                For index As Integer = 0 To dgv.Rows.Count - 1
                    If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                        If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                            dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                        End If
                    End If
                Next
            End If
        End If
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_ICColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CardNumColumnName)
        'lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)

        lstColName.Add("ComeInfo")
        lstColName.Add("ClassNum")
        lstColName.Add("Accommodation")
        lstColName.Add("EnrollSch")
        lstColName.Add("PunchCardNum")
        lstColName.Add("HseeMark")
        lstColName.Add("SalesName")

        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 5 Then
            Dim lst As New ArrayList
            For index As Integer = 6 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub frmStuSearchMulClass_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub InitClassList()
        Try
            dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
            dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        chklstSubClass.Items.Clear()
        lstClassShown = New ArrayList
        lstSubClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClass.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClass.Rows.Count)
                    FillArrayList(2, dtClass.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClass.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClass.Rows.Count - 1
                    dateClass = dtClass.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            InitClassSelection()
            RefreshClassList()
            RefreshSubClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(3)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(64) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim Answer As MsgBoxResult = MsgBox("請問真的確定要刪除該學生資料??", MsgBoxStyle.YesNo, "警告!!")
        Select Case Answer
            Case MsgBoxResult.Yes
                If dgv.SelectedRows.Count > 0 Then
                    Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                    If id.Length = 8 Then
                        objCsol.DeleteStudent(id)
                        DeleteStu(id)
                    End If
                End If
            Case MsgBoxResult.No
                Exit Sub
            Case Else
                Exit Sub
        End Select
    End Sub

    Private Sub DeleteStu(ByVal id As String)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(index).Item(c_IDColumnName) = id Then
                dtStu.Rows.RemoveAt(index)
            End If
        Next
        MakeTable()
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshSubClassList()
        Try
            lstSubClass = New ArrayList()
            cboxSubject.Items.Clear()
            For index As Integer = 0 To chklstSubClass.Items.Count - 1
                If chklstSubClass.GetItemChecked(index) = True Then
                    lstSubClass.Add(lstSubClassShown(index))
                    cboxSubject.Items.Add(chklstSubClass.Items(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitClassSelection()
        If chklstClass.Items.Count > 0 Then
            chklstClass.SelectedItem = chklstClass.Items(0)
            If chklstSubClass.Items.Count > 0 Then
                chklstSubClass.SelectedItem = chklstSubClass.Items(0)
            End If
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstSubClass.Items.Count - 1
            chklstSubClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub

    Private Sub chklstClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstClass.ItemCheck
        Dim intC As Integer
        Dim intSc As Integer
        intC = lstClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstClass.Add(intC)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClass.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Add(intSc)
                    chklstSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                End If
            Next
        Else
            lstClass.Remove(intC)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = intC Then
                    intSc = dtSubClass.Rows(index).Item(c_IDColumnName)
                    lstSubClassShown.Remove(intSc)
                    lstSubClass.Remove(intSc)
                    chklstSubClass.Items.Remove(dtSubClass.Rows(index).Item(c_NameColumnName))
                End If
            Next
        End If
    End Sub

 
 

    Private Sub cboxSubject_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubject.SelectedIndexChanged
        MakeTable()
    End Sub

    Private Sub chkboxShowCancel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowCancel.CheckedChanged
        MakeTable()
    End Sub

    Private Sub radbutShow1Sbj_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShow1Sbj.CheckedChanged
        If radbutShow1Sbj.Checked Then
            MakeTable()
        End If
    End Sub

    Private Sub radbutShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowAll.CheckedChanged
        If radbutShowAll.Checked Then
            MakeTable()
        End If
    End Sub

    Private Sub radbutShowOne_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowOne.CheckedChanged
        If radbutShowOne.Checked Then
            MakeTable()
        End If
    End Sub

    Private Sub mnuExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        If frmMain.CheckAuth(12) Then
            ExportDgvToExcel(dgv)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNote.Click
        Dim frm As New frm2AddNote
        frm.ShowDialog()
    End Sub

    Private Sub mnuPayRemind_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPayRemind.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
        dgv.DataSource = Nothing
        dgv.Rows.Clear()
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim currentRow As Integer = dgv.CurrentRow.Index + 1
        tboxCount.Text = currentRow.ToString + "/" + rowNum.ToString
    End Sub

    Private Sub btnExc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExc.Click
       RefreshData()
    End Sub

    Private Sub chklstSubClass_ItemCheck(ByVal sender As Object, ByVal e As System.Windows.Forms.ItemCheckEventArgs) Handles chklstSubClass.ItemCheck
        Dim intC As Integer
        intC = lstSubClassShown(e.Index)
        If e.CurrentValue = 0 And e.NewValue = 1 Then
            lstSubClass.Add(intC)
        Else
            lstSubClass.Remove(intC)
        End If
    End Sub


   
End Class