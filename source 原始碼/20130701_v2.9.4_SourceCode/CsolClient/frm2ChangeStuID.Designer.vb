﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2ChangeStuID
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.tboxYear = New System.Windows.Forms.TextBox
        Me.radbutNewYear = New System.Windows.Forms.RadioButton
        Me.radbutID = New System.Windows.Forms.RadioButton
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(225, 39)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(75, 29)
        Me.butCancel.TabIndex = 134
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(225, 6)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(75, 29)
        Me.butSave.TabIndex = 133
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'tboxYear
        '
        Me.tboxYear.Location = New System.Drawing.Point(91, 11)
        Me.tboxYear.Name = "tboxYear"
        Me.tboxYear.Size = New System.Drawing.Size(97, 22)
        Me.tboxYear.TabIndex = 132
        '
        'radbutNewYear
        '
        Me.radbutNewYear.AutoSize = True
        Me.radbutNewYear.Checked = True
        Me.radbutNewYear.Location = New System.Drawing.Point(14, 12)
        Me.radbutNewYear.Name = "radbutNewYear"
        Me.radbutNewYear.Size = New System.Drawing.Size(71, 16)
        Me.radbutNewYear.TabIndex = 135
        Me.radbutNewYear.TabStop = True
        Me.radbutNewYear.Text = "新的年籍"
        Me.radbutNewYear.UseVisualStyleBackColor = True
        '
        'radbutID
        '
        Me.radbutID.AutoSize = True
        Me.radbutID.Location = New System.Drawing.Point(14, 45)
        Me.radbutID.Name = "radbutID"
        Me.radbutID.Size = New System.Drawing.Size(71, 16)
        Me.radbutID.TabIndex = 137
        Me.radbutID.Text = "指定學號"
        Me.radbutID.UseVisualStyleBackColor = True
        '
        'tboxID
        '
        Me.tboxID.Location = New System.Drawing.Point(91, 44)
        Me.tboxID.Name = "tboxID"
        Me.tboxID.Size = New System.Drawing.Size(97, 22)
        Me.tboxID.TabIndex = 136
        '
        'frm2ChangeStuID
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(317, 84)
        Me.Controls.Add(Me.radbutID)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.radbutNewYear)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.tboxYear)
        Me.Name = "frm2ChangeStuID"
        Me.Text = "換學號"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents tboxYear As System.Windows.Forms.TextBox
    Friend WithEvents radbutNewYear As System.Windows.Forms.RadioButton
    Friend WithEvents radbutID As System.Windows.Forms.RadioButton
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
End Class
