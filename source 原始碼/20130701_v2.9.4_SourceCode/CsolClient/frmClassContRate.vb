﻿Public Class frmClassContRate
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstClass2 As New ArrayList
    Private lstSubClass As New ArrayList
    Private lstSubClass2 As New ArrayList
    Private dtStu As New DataTable
    Private dtStu2 As New DataTable
    Private dtNot As New DataTable
    Private dtCont As New DataTable
    Private dtNew As New DataTable
    Private d As Date
    Private d2 As Date
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header1 As String = My.Resources.frmClassContRate & " : " & My.Resources.stuClassCont
    Private Header2 As String = My.Resources.frmClassContRate & " : " & My.Resources.stuClassNew
    Private Header3 As String = My.Resources.frmClassContRate & " : " & My.Resources.stuClassContNo
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmClassContRate
        d = Now.AddMonths(-6)
        d2 = Now.AddYears(-1)

        'If Now.Month > 6 Then
        '    d = New Date(Now.Year + 1, Now.Month - 6, Now.Day, Now.Hour, Now.Minute, Now.Second)
        'Else
        '    If Now.Day = 31 Then
        '        If Now.Month + 6 = 7 Or Now.Month + 6 = 8 Or Now.Month + 6 = 10 Or Now.Month + 6 = 12 Then
        '            d = New Date(Now.Year, Now.Month + 6, Now.Day, Now.Hour, Now.Minute, Now.Second)
        '        Else
        '            d = New Date(Now.Year, Now.Month + 6, Now.Day - 1, Now.Hour, Now.Minute, Now.Second)
        '        End If

        '    End If
        'End If
        'd2 = New Date(Now.Year + 1, Now.Month, Now.Day, Now.Hour, Now.Minute, Now.Second)

        InitLst()
        RefreshData()
        LoadColumnText()
        InitSelections()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvCont.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvCont.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvCont.Rows.Count

            Dim oRow As DataGridViewRow = dgvCont.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter1(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header1, New Font(dgvCont.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header1, New Font(dgvCont.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvCont.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter1(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvNew.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvNew.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvNew.Rows.Count

            Dim oRow As DataGridViewRow = dgvNew.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header2, New Font(dgvNew.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header2, New Font(dgvNew.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvNew.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub PrintDocument3_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument3.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvContNo.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument3_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument3.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvContNo.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvContNo.Rows.Count

            Dim oRow As DataGridViewRow = dgvContNo.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter3(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header3, New Font(dgvContNo.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header3, New Font(dgvContNo.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvContNo.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter3(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter3(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvContNo.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        sUserName = sUserName & " " & My.Resources.classContRate & ": " & tboxContRate.Text.Trim & _
                              " " & My.Resources.classGrowRate & ": " & tboxGrowRate.Text.Trim
        e.Graphics.DrawString(sUserName, _
                              dgvContNo.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvContNo.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvContNo.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvContNo.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvContNo.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvNew.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        sUserName = sUserName & " " & My.Resources.classContRate & ": " & tboxContRate.Text.Trim & _
                              " " & My.Resources.classGrowRate & ": " & tboxGrowRate.Text.Trim
        e.Graphics.DrawString(sUserName, _
                              dgvNew.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvNew.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvNew.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvNew.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvNew.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub DrawFooter1(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvCont.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        sUserName = sUserName & " " & My.Resources.classContRate & ": " & tboxContRate.Text.Trim & _
                              " " & My.Resources.classGrowRate & ": " & tboxGrowRate.Text.Trim
        e.Graphics.DrawString(sUserName, _
                              dgvCont.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvCont.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvCont.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvCont.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvCont.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmClassContRate_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmClassContRate_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim com1 As String = "COUNT(" + c_IDColumnName + ")"
        Dim com2 As String = c_IDColumnName + "='"
        Dim id As String = ""
        Dim c As Integer = 0
        Dim f As String = c_CancelColumnName & "=" & c_StuClassNotCancel
        Dim s1 As String
        Dim s2 As String

        If dtStu.Rows.Count > 0 And dtStu2.Rows.Count > 0 Then
            For Each row In dtStu.Rows
                id = row.Item(c_IDColumnName)
                c = dtStu2.Compute(com1, com2 & id & "'")
                If c = 0 Then
                    dtNot.ImportRow(row)
                ElseIf c > 0 Then
                    dtCont.ImportRow(row)
                End If
            Next

            For Each row In dtStu2.Rows
                id = row.Item(c_IDColumnName)
                c = dtStu.Compute(com1, com2 & id & "'")
                If c = 0 Then
                    dtNew.ImportRow(row)
                End If
            Next
            s1 = CSng((dtCont.Rows.Count / dtStu.Rows.Count) * 100).ToString
            s2 = CSng((dtStu2.Rows.Count / dtStu.Rows.Count) * 100).ToString

            If s1.Length > 6 Then
                s1 = s1.Substring(0, 6)
            End If

            If s2.Length > 6 Then
                s2 = s2.Substring(0, 6)
            End If

            s1 = s1 & "%"
            s2 = s2 & "%"
        Else
            s1 = "0.00%"
            s2 = "0.00%"
        End If

        If chkboxShowCancel.Checked Then
            dtCont.DefaultView.RowFilter = ""
            dtNot.DefaultView.RowFilter = ""
            dtNew.DefaultView.RowFilter = ""
        Else
            If dtCont.Rows.Count > 0 Then
                dtCont.DefaultView.RowFilter = f
            End If
            If dtNot.Rows.Count > 0 Then
                dtNot.DefaultView.RowFilter = f
            End If
            If dtNew.Rows.Count > 0 Then
                dtNew.DefaultView.RowFilter = f
            End If
        End If

        dgvCont.DataSource = dtCont.DefaultView
        dgvContNo.DataSource = dtNot.DefaultView
        dgvNew.DataSource = dtNew.DefaultView
        tboxContRate.Text = s1
        tboxGrowRate.Text = s2
        LoadColumnText()
    End Sub

    Private Sub InitLst()

        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        cboxClass.Items.Clear()
        cboxClass2.Items.Clear()
        lstClass.Clear()
        lstClass2.Clear()

        If radbutShowAll.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass2.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        ElseIf radbutShowValid.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                    cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass2.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        ElseIf radbutShowValidInHalfYear.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) <= Now.Date And dtClass.Rows(index).Item(c_EndColumnName) >= d Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
                If dtClass.Rows(index).Item(c_EndColumnName) >= d Then
                    cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass2.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        ElseIf radbutShowValidInOneYear.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) <= Now.Date And dtClass.Rows(index).Item(c_EndColumnName) >= d2 Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
                If dtClass.Rows(index).Item(c_EndColumnName) >= d2 Then
                    cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass2.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If
        'If radbutShowAll.Checked Then
        '    For index As Integer = 0 To dtClass.Rows.Count - 1
        '        cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '        cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '        lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        '    Next
        'ElseIf radbutShowValid.Checked Then
        '    For index As Integer = 0 To dtClass.Rows.Count - 1
        '        If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
        '            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        '        End If
        '    Next
        'ElseIf radbutShowValidInHalfYear.Checked Then
        '    For index As Integer = 0 To dtClass.Rows.Count - 1
        '        If dtClass.Rows(index).Item(c_EndColumnName) <= Now.Date And dtClass.Rows(index).Item(c_EndColumnName) >= d Then
        '            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        '        End If
        '    Next
        'ElseIf radbutShowValidInOneYear.Checked Then
        '    For index As Integer = 0 To dtClass.Rows.Count - 1
        '        If dtClass.Rows(index).Item(c_EndColumnName) <= Now.Date And dtClass.Rows(index).Item(c_EndColumnName) >= d2 Then
        '            cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            cboxClass2.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
        '            lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
        '        End If
        '    Next
        'End If



        cboxClass.SelectedIndex = -1
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            cboxSubClass.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvNew.Columns
            col.visible = False
        Next
        If dgvNew.Rows.Count > 0 Then

            dgvNew.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgvNew.Columns.Item(c_IDColumnName).Visible = True
            dgvNew.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgvNew.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvNew.Columns.Item(c_NameColumnName).Visible = True
            dgvNew.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgvNew.Columns.Item(c_BirthdayColumnName).DisplayIndex = 2
            dgvNew.Columns.Item(c_BirthdayColumnName).Visible = True
            dgvNew.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
            dgvNew.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
            dgvNew.Columns.Item(c_SchoolColumnName).Visible = True
            dgvNew.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgvNew.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgvNew.Columns.Item(c_Tel1ColumnName).Visible = True
            dgvNew.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgvNew.Columns.Item(c_Tel2ColumnName).DisplayIndex = 5
            dgvNew.Columns.Item(c_Tel2ColumnName).Visible = True
            dgvNew.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            dgvNew.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 6
            dgvNew.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvNew.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgvNew.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgvNew.Columns.Item(lstColName(index)).Visible = True
                    dgvNew.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
            If dgvNew.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgvNew.Rows.Count - 1
                            id = dgvNew.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgvNew.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgvNew.Rows.Count - 1
                            id = dgvNew.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgvNew.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgvNew.Rows.Count - 1
                            id = dgvNew.Rows(index).Cells(c_HighSchColumnName).Value
                            dgvNew.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgvNew.Rows.Count - 1
                            id = dgvNew.Rows(index).Cells(c_UniversityColumnName).Value
                            dgvNew.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
        End If
        For Each col In dgvCont.Columns
            col.visible = False
        Next
        If dgvCont.Rows.Count > 0 Then
            
            dgvCont.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgvCont.Columns.Item(c_IDColumnName).Visible = True
            dgvCont.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgvCont.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvCont.Columns.Item(c_NameColumnName).Visible = True
            dgvCont.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgvCont.Columns.Item(c_BirthdayColumnName).DisplayIndex = 2
            dgvCont.Columns.Item(c_BirthdayColumnName).Visible = True
            dgvCont.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
            dgvCont.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
            dgvCont.Columns.Item(c_SchoolColumnName).Visible = True
            dgvCont.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgvCont.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgvCont.Columns.Item(c_Tel1ColumnName).Visible = True
            dgvCont.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgvCont.Columns.Item(c_Tel2ColumnName).DisplayIndex = 5
            dgvCont.Columns.Item(c_Tel2ColumnName).Visible = True
            dgvCont.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            dgvCont.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 6
            dgvCont.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvCont.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgvCont.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgvCont.Columns.Item(lstColName(index)).Visible = True
                    dgvCont.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
            If dgvCont.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgvCont.Rows.Count - 1
                            id = dgvCont.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgvCont.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgvCont.Rows.Count - 1
                            id = dgvCont.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgvCont.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgvCont.Rows.Count - 1
                            id = dgvCont.Rows(index).Cells(c_HighSchColumnName).Value
                            dgvCont.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgvCont.Rows.Count - 1
                            id = dgvCont.Rows(index).Cells(c_UniversityColumnName).Value
                            dgvCont.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
        End If
        For Each col In dgvContNo.Columns
            col.visible = False
        Next
        If dgvContNo.Rows.Count > 0 Then
            
            dgvContNo.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgvContNo.Columns.Item(c_IDColumnName).Visible = True
            dgvContNo.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgvContNo.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgvContNo.Columns.Item(c_NameColumnName).Visible = True
            dgvContNo.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgvContNo.Columns.Item(c_BirthdayColumnName).DisplayIndex = 2
            dgvContNo.Columns.Item(c_BirthdayColumnName).Visible = True
            dgvContNo.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
            dgvContNo.Columns.Item(c_SchoolColumnName).DisplayIndex = 3
            dgvContNo.Columns.Item(c_SchoolColumnName).Visible = True
            dgvContNo.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
            dgvContNo.Columns.Item(c_Tel1ColumnName).DisplayIndex = 4
            dgvContNo.Columns.Item(c_Tel1ColumnName).Visible = True
            dgvContNo.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgvContNo.Columns.Item(c_Tel2ColumnName).DisplayIndex = 5
            dgvContNo.Columns.Item(c_Tel2ColumnName).Visible = True
            dgvContNo.Columns.Item(c_Tel2ColumnName).HeaderText = My.Resources.tel2
            dgvContNo.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 6
            dgvContNo.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvContNo.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgvContNo.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgvContNo.Columns.Item(lstColName(index)).Visible = True
                    dgvContNo.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
            If dgvContNo.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgvContNo.Rows.Count - 1
                            id = dgvContNo.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgvContNo.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgvContNo.Rows.Count - 1
                            id = dgvContNo.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgvContNo.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgvContNo.Rows.Count - 1
                            id = dgvContNo.Rows(index).Cells(c_HighSchColumnName).Value
                            dgvContNo.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgvContNo.Rows.Count - 1
                            id = dgvContNo.Rows(index).Cells(c_UniversityColumnName).Value
                            dgvContNo.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If
        End If

    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub frmClassContRate_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub radbutShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowAll.CheckedChanged
        If radbutShowAll.Checked Then
            InitLst()
        End If
    End Sub

    Private Sub radbutShowValid_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowValid.CheckedChanged
        If radbutShowValid.Checked Then
            InitLst()
        End If
    End Sub

    Private Sub radbutShowValidInHalfYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowValidInHalfYear.CheckedChanged
        If radbutShowValidInHalfYear.Checked Then
            InitLst()
        End If
    End Sub

    Private Sub radbutShowValidInOneYear_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radbutShowValidInOneYear.CheckedChanged
        If radbutShowValidInOneYear.Checked Then
            InitLst()
        End If
    End Sub

    Private Sub chkboxShowCancel_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowCancel.CheckedChanged
        Dim f As String = c_CancelColumnName & "=" & c_StuClassNotCancel
        If chkboxShowCancel.Checked Then
            dtCont.DefaultView.RowFilter = ""
            dtNot.DefaultView.RowFilter = ""
            dtNew.DefaultView.RowFilter = ""
        Else
            dtCont.DefaultView.RowFilter = f
            dtNot.DefaultView.RowFilter = f
            dtNew.DefaultView.RowFilter = f
        End If

        dgvCont.DataSource = dtCont.DefaultView
        dgvContNo.DataSource = dtNot.DefaultView
        dgvNew.DataSource = dtNew.DefaultView
        LoadColumnText()
    End Sub

    Private Sub cboxClass2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass2.SelectedIndexChanged
        cboxSubClass2.Items.Clear()
        cboxSubClass2.Text = ""
        lstSubClass2.Clear()
        If cboxClass2.SelectedIndex > -1 Then
            Dim i As Integer = lstClass2(cboxClass2.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass2.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass2.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            'cboxSubClass2.SelectedIndex = 0
        End If
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvNew.Columns.Count > 6 Then
            Dim lst As New ArrayList
            For index As Integer = 7 To dgvNew.Columns.Count - 1
                If dgvNew.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub mnuExportData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        If frmMain.CheckAuth(14) Then
            ExportDgvToExcel(dgvContNo)
            ExportDgvToExcel(dgvCont)
            ExportDgvToExcel(dgvNew)
        Else
            frmMain.ShowNoAuthMsg()
        End If
        
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(4) Then
            PrintDocument3.DefaultPageSettings.Landscape = True
            PrintDocument3.Print()
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
            PrintDocument2.DefaultPageSettings.Landscape = True
            PrintDocument2.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuClose_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub mnuAnalyze_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAnalyze.Click
        If cboxSubClass.SelectedIndex > -1 And cboxSubClass2.SelectedIndex > -1 Then
            Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            Dim i2 As Integer = lstSubClass2(cboxSubClass2.SelectedIndex)
            dtStu = objCsol.ListStuBySubClass(i)
            dtStu2 = objCsol.ListStuBySubClass(i2)
            dtCont.Clear()
            dtNew.Clear()
            dtNot.Clear()
            dtCont = dtStu.Clone
            dtNew = dtStu.Clone
            dtNot = dtStu.Clone
            RefreshData()
        End If
    End Sub

    Private Sub mnuNote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuNote.Click
        Dim frm As New frm2AddNote
        frm.ShowDialog()
    End Sub
End Class