﻿Public Class frm2AddTeleInterviewStu
    Dim strId As String = ""
    Dim strName As String = ""
    Dim dtType As New DataTable
    Dim lstTypeId As New ArrayList

    Private Sub frm2AddTeleInterview_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        RefreshData()
    End Sub

    Private Sub RefreshData()
        dtType = frmMain.GetTeleInterviewType
        lstTypeId.Clear()
        cboxType.Items.Clear()
        For index As Integer = 0 To dtType.Rows.Count - 1
            cboxType.Items.Add(dtType.Rows(index).Item(c_TypeColumnName).ToString.Trim)
            lstTypeId.Add(dtType.Rows(index).Item(c_IDColumnName))
        Next

    End Sub

    Private Sub butAddType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddType.Click
        Dim frm As New frm2AddTeleInterviewType
        frm.ShowDialog()
        If frmMain.GetOkState Then
            frmMain.SetOkState(False)
            frmMain.RefreshTeleInterviewType()
            RefreshData()
        End If

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If strId.Trim.Length > 7 And _
            cboxType.SelectedIndex > -1 And tboxContent.Text.Trim.Length > 0 And lstTypeId.Count > 0 Then
            objCsol.AddTeleInterview(strId.Trim, dtpickDate.Value, lstTypeId(cboxType.SelectedIndex), _
                                    tboxContent.Text.Trim, frmMain.GetUsrId)
            frmMain.SetOkState(True)
            Me.Close()
        Else
            MsgBox(My.Resources.msgInvalidTeleInterviewInput, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Public Sub New(ByVal id As String)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。

        strId = id
    End Sub
End Class