﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SearchStu
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2SearchStu))
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.tboxName = New System.Windows.Forms.TextBox
        Me.tboxTel = New System.Windows.Forms.TextBox
        Me.tboxParent = New System.Windows.Forms.TextBox
        Me.butSearch = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.radbut5 = New System.Windows.Forms.RadioButton
        Me.radbut4 = New System.Windows.Forms.RadioButton
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.butDetails = New System.Windows.Forms.Button
        Me.tboxCardNum = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tboxSch = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.lbMes = New System.Windows.Forms.Label
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'tboxID
        '
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        '
        'tboxName
        '
        resources.ApplyResources(Me.tboxName, "tboxName")
        Me.tboxName.Name = "tboxName"
        '
        'tboxTel
        '
        resources.ApplyResources(Me.tboxTel, "tboxTel")
        Me.tboxTel.Name = "tboxTel"
        '
        'tboxParent
        '
        resources.ApplyResources(Me.tboxParent, "tboxParent")
        Me.tboxParent.Name = "tboxParent"
        '
        'butSearch
        '
        resources.ApplyResources(Me.butSearch, "butSearch")
        Me.butSearch.Name = "butSearch"
        Me.butSearch.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'radbut5
        '
        resources.ApplyResources(Me.radbut5, "radbut5")
        Me.radbut5.Checked = True
        Me.radbut5.Name = "radbut5"
        Me.radbut5.TabStop = True
        Me.radbut5.UseVisualStyleBackColor = True
        '
        'radbut4
        '
        resources.ApplyResources(Me.radbut4, "radbut4")
        Me.radbut4.Name = "radbut4"
        Me.radbut4.UseVisualStyleBackColor = True
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        resources.ApplyResources(Me.dgv, "dgv")
        Me.dgv.MultiSelect = False
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowHeadersVisible = False
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'butDetails
        '
        resources.ApplyResources(Me.butDetails, "butDetails")
        Me.butDetails.Name = "butDetails"
        Me.butDetails.UseVisualStyleBackColor = True
        '
        'tboxCardNum
        '
        resources.ApplyResources(Me.tboxCardNum, "tboxCardNum")
        Me.tboxCardNum.Name = "tboxCardNum"
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'tboxSch
        '
        resources.ApplyResources(Me.tboxSch, "tboxSch")
        Me.tboxSch.Name = "tboxSch"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'lbMes
        '
        resources.ApplyResources(Me.lbMes, "lbMes")
        Me.lbMes.ForeColor = System.Drawing.Color.Red
        Me.lbMes.Name = "lbMes"
        '
        'frm2SearchStu
        '
        Me.AcceptButton = Me.butDetails
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(219, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.lbMes)
        Me.Controls.Add(Me.tboxSch)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.tboxCardNum)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.butDetails)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.radbut4)
        Me.Controls.Add(Me.radbut5)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSearch)
        Me.Controls.Add(Me.tboxParent)
        Me.Controls.Add(Me.tboxTel)
        Me.Controls.Add(Me.tboxName)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SearchStu"
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents tboxName As System.Windows.Forms.TextBox
    Friend WithEvents tboxTel As System.Windows.Forms.TextBox
    Friend WithEvents tboxParent As System.Windows.Forms.TextBox
    Friend WithEvents butSearch As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents radbut5 As System.Windows.Forms.RadioButton
    Friend WithEvents radbut4 As System.Windows.Forms.RadioButton
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents butDetails As System.Windows.Forms.Button
    Friend WithEvents tboxCardNum As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tboxSch As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents lbMes As System.Windows.Forms.Label
End Class
