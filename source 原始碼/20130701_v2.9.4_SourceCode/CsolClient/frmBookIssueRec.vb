﻿Public Class frmBookIssueRec
    Private dtBook As New DataTable
    Private lstBook As New ArrayList
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Dim intType As Integer = 0
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmBookIssueRec
    Private sUserName As String = frmMain.GetUsrName
    Private rowNum As Integer

    Public Sub New()
        InitializeComponent()

        RefreshData()
        Me.Text = My.Resources.frmBookIssueRec
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(5) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0
    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuSearchSchool_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuSearchSchool_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_StuIDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Friend Sub RefreshData()
        dtBook = frmMain.GetBooks
        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                     c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        cboxClass.Items.Clear()
        lstClass.Clear()
        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
        End If
    End Sub

    Private Sub RefreshTable()
        Dim bid As Integer = 0
        '20100205 updated by sherry start
        If cboxBook.SelectedIndex > -1 Then
            bid = lstBook(cboxBook.SelectedIndex)
        End If
        Dim classId As Integer = 0
        If cboxClass.SelectedIndex > -1 Then
            classId = lstClass(cboxClass.SelectedIndex)
        End If

        dtStu = objCsol.GetBookRecByUsr(classId, bid)


        Dim f2 As String = ""
        Dim cid As Integer = 0
        Dim sid As Integer = 0
        If cboxSubClass.SelectedIndex = 0 Then
            If cboxClass.SelectedIndex > -1 Then
                cid = lstClass(cboxClass.SelectedIndex)
                f2 = c_ClassIDColumnName & "=" & cid.ToString
            End If
        ElseIf cboxSubClass.SelectedIndex > 0 Then
            sid = lstSubClass(cboxSubClass.SelectedIndex)
            f2 = c_SubClassIDColumnName & "=" & sid.ToString
        End If




        Dim dt As New DataTable

        If cboxSubClass.SelectedIndex = 0 Then
            dtStu.DefaultView.RowFilter = f2
            dt = dtStu.DefaultView.ToTable(True, c_RecIDColumnName, c_StuIDColumnName, c_StuNameColumnName, c_NameColumnName, c_StockColumnName, c_DateTimeColumnName, c_ClassNameColumnName) ', c_SubClassNameColumnName,  c_StockColumnName)

            'dt.Columns.Add(c_NameColumnName, GetType(System.String))
            'dt.Columns.Add(c_StockColumnName, GetType(System.String))
            'Dim i1 As String = dtStu.Rows(0).Item().ToString
            'Dim i2 As String = dtStu.Rows(0).Item().ToString
            'For index As Integer = 0 To dt.Rows.Count - 1
            'dt.Rows(index).Item(c_NameColumnName) = i1
            'dt.Rows(index).Item(c_StockColumnName) = i2
            'Next
        Else
            dt = dtStu.Copy
            dt.DefaultView.RowFilter = f2
        End If

        '20100205 updated by sherry end
        dgv.DataSource = dt.DefaultView

        rowNum = dgv.RowCount
        tboxCount.Text = "1/" + rowNum.ToString
        LoadColumnText()
    End Sub


    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next

        If cboxSubClass.SelectedIndex = 0 Then
            If dgv.Columns.Count > 0 Then
                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
                dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_StuIDColumnName).Visible = True
                dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
                dgv.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_StuNameColumnName).Visible = True
                dgv.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
                dgv.Columns.Item(c_ClassNameColumnName).DisplayIndex = 3
                dgv.Columns.Item(c_ClassNameColumnName).Visible = True
                dgv.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
                dgv.Columns.Item(c_StockColumnName).DisplayIndex = 4
                dgv.Columns.Item(c_StockColumnName).Visible = True
                dgv.Columns.Item(c_StockColumnName).HeaderText = My.Resources.stock
                dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
                dgv.Columns.Item(c_DateTimeColumnName).Visible = True
                dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            End If
        Else
            If dgv.Columns.Count > 0 Then
                dgv.Columns.Item(c_NameColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_NameColumnName).Visible = True
                dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.bookName
                dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_StuIDColumnName).Visible = True
                dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
                dgv.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
                dgv.Columns.Item(c_StuNameColumnName).Visible = True
                dgv.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
                dgv.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
                dgv.Columns.Item(c_SubClassNameColumnName).Visible = True
                dgv.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
                dgv.Columns.Item(c_StockColumnName).DisplayIndex = 4
                dgv.Columns.Item(c_StockColumnName).Visible = True
                dgv.Columns.Item(c_StockColumnName).HeaderText = My.Resources.stock
                dgv.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
                dgv.Columns.Item(c_DateTimeColumnName).Visible = True
                dgv.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            End If
        End If


    End Sub

    Private Sub frmStuSearchSchool_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex > -1 Then
            cboxSubClass.Items.Add(My.Resources.all)
            lstSubClass.Add(-1)
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName).trim)
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            If cboxSubClass.Items.Count > 0 Then
                cboxSubClass.SelectedIndex = 0
            End If
        End If
        RefreshTable()
    End Sub

    

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_StuIDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(9)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As Integer = dgv.SelectedRows(0).Cells(c_RecIDColumnName).Value
            objCsol.DeleteBookIssueRec(id)
            DeleteRec(id)
        End If
    End Sub

    Private Sub DeleteRec(ByVal id As Integer)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If GetIntValue(dtStu.Rows(index).Item(c_RecIDColumnName)) = id Then
                dtStu.Rows.RemoveAt(index)
                Exit For
            End If
        Next
        RefreshTable()
    End Sub

    Private Sub cboxBook_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxBook.SelectedIndexChanged
        If cboxBook.SelectedIndex > -1 Then
            RefreshTable()
        End If
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        cboxClass.Items.Clear()
        lstClass.Clear()

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
            Next
        Else
            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName).trim)
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
        End If

        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
            cboxSubClass.SelectedIndex = -1
            cboxBook.SelectedIndex = -1
            cboxClass.Text = ""
            cboxSubClass.Text = ""
            cboxBook.Text = ""
        End If
        RefreshTable()
    End Sub

    Private Sub mnuExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(15) Then
            ExportDgvToExcel(dgv)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        lstBook.Clear()
        cboxBook.Items.Clear()
        If cboxSubClass.SelectedIndex > -1 Then
            If cboxSubClass.SelectedIndex = 0 Then
                If cboxClass.SelectedIndex > -1 Then
                    Dim i As Integer = lstClass(cboxClass.SelectedIndex)
                    For index As Integer = 0 To dtBook.Rows.Count - 1
                        If dtBook.Rows(index).Item(c_ClassIDColumnName) = i Then
                            If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                                If lstBook.Contains(dtBook.Rows(index).Item(c_IDColumnName)) = False Then
                                    lstBook.Add(dtBook.Rows(index).Item(c_IDColumnName))
                                    cboxBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).ToString.Trim)
                                End If
                            End If
                        End If
                    Next
                End If
            Else
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtBook.Rows.Count - 1
                    If dtBook.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        If dtBook.Rows(index).Item(c_BeforeDateColumnName) >= GetDate(Now) Then
                            lstBook.Add(dtBook.Rows(index).Item(c_IDColumnName))
                            cboxBook.Items.Add(dtBook.Rows(index).Item(c_NameColumnName).ToString.Trim)
                        End If
                    End If
                Next
            End If
            If cboxBook.Items.Count > 0 Then
                cboxBook.SelectedIndex = 0
            End If
        End If
        RefreshTable()
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim currentRow As Integer = dgv.CurrentRow.Index + 1
        tboxCount.Text = currentRow.ToString + "/" + rowNum.ToString
    End Sub

 
End Class