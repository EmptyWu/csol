﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetSchool
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuModify = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuRemove = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuImport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.radbutNoFilter = New System.Windows.Forms.RadioButton
        Me.radbutFilter1 = New System.Windows.Forms.RadioButton
        Me.radbutFilter2 = New System.Windows.Forms.RadioButton
        Me.radbutFilter3 = New System.Windows.Forms.RadioButton
        Me.radbutFilter4 = New System.Windows.Forms.RadioButton
        Me.dgvSta = New System.Windows.Forms.DataGridView
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgvSta, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuModify, Me.mnuRemove, Me.mnuSearch, Me.mnuImport, Me.mnuExport, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 19
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        Me.mnuNew.Size = New System.Drawing.Size(68, 20)
        Me.mnuNew.Text = "新增學校"
        '
        'mnuModify
        '
        Me.mnuModify.Name = "mnuModify"
        Me.mnuModify.Size = New System.Drawing.Size(68, 20)
        Me.mnuModify.Text = "修改學校"
        '
        'mnuRemove
        '
        Me.mnuRemove.Name = "mnuRemove"
        Me.mnuRemove.Size = New System.Drawing.Size(68, 20)
        Me.mnuRemove.Text = "刪除學校"
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        Me.mnuSearch.Size = New System.Drawing.Size(68, 20)
        Me.mnuSearch.Text = "搜尋學校"
        '
        'mnuImport
        '
        Me.mnuImport.Name = "mnuImport"
        Me.mnuImport.Size = New System.Drawing.Size(68, 20)
        Me.mnuImport.Text = "匯入學校"
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出學校"
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'radbutNoFilter
        '
        Me.radbutNoFilter.AutoSize = True
        Me.radbutNoFilter.Location = New System.Drawing.Point(28, 38)
        Me.radbutNoFilter.Name = "radbutNoFilter"
        Me.radbutNoFilter.Size = New System.Drawing.Size(59, 16)
        Me.radbutNoFilter.TabIndex = 20
        Me.radbutNoFilter.Text = "未分類"
        Me.radbutNoFilter.UseVisualStyleBackColor = True
        '
        'radbutFilter1
        '
        Me.radbutFilter1.AutoSize = True
        Me.radbutFilter1.Location = New System.Drawing.Point(113, 38)
        Me.radbutFilter1.Name = "radbutFilter1"
        Me.radbutFilter1.Size = New System.Drawing.Size(47, 16)
        Me.radbutFilter1.TabIndex = 21
        Me.radbutFilter1.Text = "國小"
        Me.radbutFilter1.UseVisualStyleBackColor = True
        '
        'radbutFilter2
        '
        Me.radbutFilter2.AutoSize = True
        Me.radbutFilter2.Checked = True
        Me.radbutFilter2.Location = New System.Drawing.Point(186, 38)
        Me.radbutFilter2.Name = "radbutFilter2"
        Me.radbutFilter2.Size = New System.Drawing.Size(47, 16)
        Me.radbutFilter2.TabIndex = 22
        Me.radbutFilter2.TabStop = True
        Me.radbutFilter2.Text = "國中"
        Me.radbutFilter2.UseVisualStyleBackColor = True
        '
        'radbutFilter3
        '
        Me.radbutFilter3.AutoSize = True
        Me.radbutFilter3.Location = New System.Drawing.Point(259, 38)
        Me.radbutFilter3.Name = "radbutFilter3"
        Me.radbutFilter3.Size = New System.Drawing.Size(47, 16)
        Me.radbutFilter3.TabIndex = 23
        Me.radbutFilter3.Text = "高中"
        Me.radbutFilter3.UseVisualStyleBackColor = True
        '
        'radbutFilter4
        '
        Me.radbutFilter4.AutoSize = True
        Me.radbutFilter4.Location = New System.Drawing.Point(332, 38)
        Me.radbutFilter4.Name = "radbutFilter4"
        Me.radbutFilter4.Size = New System.Drawing.Size(47, 16)
        Me.radbutFilter4.TabIndex = 24
        Me.radbutFilter4.Text = "大學"
        Me.radbutFilter4.UseVisualStyleBackColor = True
        '
        'dgvSta
        '
        Me.dgvSta.AllowUserToAddRows = False
        Me.dgvSta.AllowUserToDeleteRows = False
        Me.dgvSta.AllowUserToResizeRows = False
        Me.dgvSta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvSta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSta.Location = New System.Drawing.Point(14, 60)
        Me.dgvSta.MultiSelect = False
        Me.dgvSta.Name = "dgvSta"
        Me.dgvSta.RowHeadersVisible = False
        Me.dgvSta.RowTemplate.Height = 15
        Me.dgvSta.RowTemplate.ReadOnly = True
        Me.dgvSta.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSta.ShowCellToolTips = False
        Me.dgvSta.ShowEditingIcon = False
        Me.dgvSta.Size = New System.Drawing.Size(1002, 571)
        Me.dgvSta.TabIndex = 0
        '
        'frmSetSchool
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.dgvSta)
        Me.Controls.Add(Me.radbutFilter4)
        Me.Controls.Add(Me.radbutFilter3)
        Me.Controls.Add(Me.radbutFilter2)
        Me.Controls.Add(Me.radbutFilter1)
        Me.Controls.Add(Me.radbutNoFilter)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmSetSchool"
        Me.Text = "frmSetSchool"
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgvSta, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuModify As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuRemove As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuImport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents radbutNoFilter As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFilter1 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFilter2 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFilter3 As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFilter4 As System.Windows.Forms.RadioButton
    Friend WithEvents dgvSta As System.Windows.Forms.DataGridView
End Class
