﻿Public Class frmStuHereToday
    Private dtContent As New DataTable
    Private lstContent As New ArrayList
    Private dtClass As New DataTable
    Private dtSubClass As New DataTable
    Private lstClass As New ArrayList
    Private lstSubClass As New ArrayList
    Private dtStu As New DataTable
    Private dtStu2 As New DataTable
    Dim intType As Integer = 0
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuHereToday
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        RefreshData()
        InitSelections()
        Me.Text = My.Resources.frmStuHereToday
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        If frmMain.CheckAuth(1) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub frmStuHereToday_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuHereToday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_StuIDColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value.ToString.Trim = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Friend Sub RefreshData()
        cboxClass.Items.Clear()
        cboxSubClass.Items.Clear()
        lstClass.Clear()
        dtContent = frmMain.GetContents
        dtClass = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
             c_NameColumnName, c_EndColumnName)
        dtSubClass = frmMain.GetClassInfoSet.Tables(c_SubClassListDataTableName)

        If chkboxShowPast.Checked Then
            For index As Integer = 0 To dtClass.Rows.Count - 1
                cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))

            Next
        Else

            For index As Integer = 0 To dtClass.Rows.Count - 1
                If dtClass.Rows(index).Item(c_EndColumnName) >= Now.Date Then
                    cboxClass.Items.Add(dtClass.Rows(index).Item(c_NameColumnName))
                    lstClass.Add(dtClass.Rows(index).Item(c_IDColumnName))

                End If
            Next
        End If
        If cboxClass.Items.Count > 0 Then
            cboxClass.SelectedIndex = -1
            cboxSubClass.SelectedIndex = -1
            cboxSubClass.Text = ""
            cboxContent.SelectedIndex = -1
            cboxContent.Text = ""
        End If
    End Sub

    Private Sub LoadColumnText()
        Try
            For Each col In dgv.Columns
                col.visible = False
            Next
            If dgv.Rows.Count > 0 Then

                dgv.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
                dgv.Columns.Item(c_StuIDColumnName).Visible = True
                dgv.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
                dgv.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
                dgv.Columns.Item(c_StuNameColumnName).Visible = True
                dgv.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName

                dgv.Columns.Item("Content").DisplayIndex = 2
                dgv.Columns.Item("Content").Visible = True
                dgv.Columns.Item("Content").HeaderText = "上課內容"

                dgv.Columns.Item("PunchDate").DisplayIndex = 3
                dgv.Columns.Item("PunchDate").Visible = True
                dgv.Columns.Item("PunchDate").HeaderText = "刷卡日期"

                dgv.Columns.Item("StartTime").DisplayIndex = 4
                dgv.Columns.Item("StartTime").Visible = True
                dgv.Columns.Item("StartTime").HeaderText = "上課時間"

                dgv.Columns.Item("EndTime").DisplayIndex = 5
                dgv.Columns.Item("EndTime").Visible = True
                dgv.Columns.Item("EndTime").HeaderText = "下課時間"

                dgv.Columns.Item("MakeUpClassID2").DisplayIndex = 6
                dgv.Columns.Item("MakeUpClassID2").Visible = True
                dgv.Columns.Item("MakeUpClassID2").HeaderText = "補課於"
   
                'dgv.Columns.Item(c_BirthdayColumnName).DisplayIndex = 7
                'dgv.Columns.Item(c_BirthdayColumnName).Visible = True
                'dgv.Columns.Item(c_BirthdayColumnName).HeaderText = My.Resources.birthday
                'dgv.Columns.Item(c_SchoolColumnName).DisplayIndex = 8
                'dgv.Columns.Item(c_SchoolColumnName).Visible = True
                'dgv.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
                'dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 9
                'dgv.Columns.Item(c_Tel1ColumnName).Visible = True
                'dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            End If
            Dim i As Integer = 7
            For index As Integer = 0 To lstColName.Count - 1
                If lstColShow(index) = 1 Then
                    dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                    dgv.Columns.Item(lstColName(index)).Visible = True
                    dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                    i = i + 1
                End If
            Next
            If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
                Dim id As Integer
                Select Case intColSchType
                    Case 0
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 1
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 2
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                    Case 3
                        For index As Integer = 0 To dgv.Rows.Count - 1
                            id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                            dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                        Next
                End Select
            End If

            If dgv.Columns.Item(c_BirthdayColumnName).Visible = True Then
                For index As Integer = 0 To dgv.Rows.Count - 1
                    If Not DBNull.Value.Equals(dgv.Rows(index).Cells(c_BirthdayColumnName).Value) Then
                        If CDate(dgv.Rows(index).Cells(c_BirthdayColumnName).Value).Year = GetMinDate().Year Then
                            dgv.Rows(index).Cells(c_BirthdayColumnName).Value = ""
                        End If
                    End If
                Next
            End If
        Catch ex As Exception
            AddErrorLog(ex.ToString)
        End Try

    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_SexColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_MobileColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_Tel1ColumnName)

        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 6 Then
            Dim lst As New ArrayList
            For index As Integer = 7 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub frmStuHereToday_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub butFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butFilter.Click
        If cboxSubClass.SelectedIndex > -1 Then
            Dim sc As Integer = lstSubClass(cboxSubClass.SelectedIndex)
            If sc = c_IndividualClassID Then
                dtStu = objCsol.GetIdvClassPunchRec(sc, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            Else
                If chkboxShowAll.Checked = False Then
                    Dim c As Integer = 0
                    If cboxContent.SelectedIndex > -1 Then
                        c = lstContent(cboxContent.SelectedIndex)
                    End If
                    dtStu = objCsol.GetSubClassPunchRec(sc, c, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                Else
                    dtStu = objCsol.GetSubClassPunchRecNoCont(sc, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
                End If
            End If
            If Not dtStu.Columns.Contains("MakeUpClassID2") Then
                dtStu.Columns.Add("MakeUpClassID2")
            End If
            If Not dtStu.Columns.Contains("PunchDate") Then
                dtStu.Columns.Add("PunchDate")
            End If
            If Not dtStu.Columns.Contains("StratTime") Then
                dtStu.Columns.Add("StartTime")
            End If
            If Not dtStu.Columns.Contains("EndTime") Then
                dtStu.Columns.Add("EndTime")
            End If
            For i As Integer = 0 To dtStu.Rows.Count - 1
                Dim punchDate As String = CDate(dtStu.Rows(i).Item("DateTime1")).ToString("yyyy-MM-dd")
                Dim punchTime As String = CDate(dtStu.Rows(i).Item("DateTime1")).ToString("HH:mm:ss")
                Dim punchType As String = dtStu.Rows(i).Item("MakeUpClassID").ToString.Trim
                Dim classIn As String = dtStu.Rows(i).Item("in").ToString.Trim
                If classIn = "0" Then
                    If punchType = "-1" Then
                        dtStu.Rows(i).Item("MakeUpClassID2") = "補課刷卡"
                    End If
                    dtStu.Rows(i).Item("PunchDate") = punchDate
                    dtStu.Rows(i).Item("StartTime") = punchTime
                ElseIf classIn = "1" Then
                    If punchType = "-1" Then
                        dtStu.Rows(i).Item("MakeUpClassID2") = "補課刷卡"
                    End If
                    dtStu.Rows(i).Item("PunchDate") = punchDate
                    dtStu.Rows(i).Item("EndTime") = punchTime
                End If
            Next
            dgv.DataSource = dtStu.DefaultView
            LoadColumnText()
            dtStu2 = objCsol.ListStuBySubClass(sc)
            txtboxStu.Text = dtStu2.Rows.Count.ToString
            Dim PunchCount As Integer = 0
            For index As Integer = 0 To dtStu.Rows.Count - 1
                If dtStu.Rows(index).Item(c_InColumnName) = 0 And dtStu.Rows(index).Item("MakeUpClassID") = 0 Then
                    PunchCount += 1
                End If
            Next
            txtboxAct.Text = PunchCount.ToString
        End If
    End Sub

    Private Sub cboxClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxClass.SelectedIndexChanged
        cboxSubClass.Items.Clear()
        lstSubClass.Clear()
        If cboxClass.SelectedIndex >= 0 Then
            Dim i As Integer = lstClass(cboxClass.SelectedIndex)
            For index As Integer = 0 To dtSubClass.Rows.Count - 1
                If dtSubClass.Rows(index).Item(c_ClassIDColumnName) = i Then
                    cboxSubClass.Items.Add(dtSubClass.Rows(index).Item(c_NameColumnName))
                    lstSubClass.Add(dtSubClass.Rows(index).Item(c_IDColumnName))
                End If
            Next
            If cboxSubClass.Items.Count > 0 Then
                cboxSubClass.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_StuIDColumnName).Value.ToString.Trim
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(6)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            objCsol.DeletePunchRec(id)
            DeleteStu(id)
            MessageBox.Show(My.Resources.msgDeleteSuccess, My.Resources.msgRemindTitle, MessageBoxButtons.OK)

        End If

    End Sub

    Private Sub DeleteStu(ByVal id As String)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(index).Item(c_IDColumnName) = id Then
                dtStu.Rows.RemoveAt(index)
                Exit For
            End If
        Next

    End Sub

    Private Sub cboxSubClass_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxSubClass.SelectedIndexChanged
        If cboxSubClass.SelectedIndex > -1 Then
            If Not chkboxShowAll.Checked Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxContent.Items.Count > 0 Then
                    cboxContent.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Private Sub chkboxShowAll_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowAll.CheckedChanged
        If Not chkboxShowAll.Checked Then
            If cboxSubClass.SelectedIndex > -1 Then
                cboxContent.Items.Clear()
                lstContent.Clear()
                Dim i As Integer = lstSubClass(cboxSubClass.SelectedIndex)
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_SubClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxContent.Items.Count > 0 Then
                    cboxContent.SelectedIndex = 0
                End If
            End If
        Else
            If cboxClass.SelectedIndex > 0 Then
                Dim i As Integer = lstClass(cboxClass.SelectedIndex)
                cboxContent.Items.Clear()
                lstContent.Clear()
                For index As Integer = 0 To dtContent.Rows.Count - 1
                    If dtContent.Rows(index).Item(c_ClassIDColumnName) = i Then
                        cboxContent.Items.Add(dtContent.Rows(index).Item(c_ContentColumnName))
                        lstContent.Add(dtContent.Rows(index).Item(c_IDColumnName))
                    End If
                Next
                If cboxContent.Items.Count > 0 Then
                    cboxContent.SelectedIndex = 0
                End If
            End If
        End If
    End Sub

    Private Sub mnuExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExport.Click
        If frmMain.CheckAuth(11) Then
            ExportDgvToExcel(dgv)
        Else
            frmMain.ShowNoAuthMsg()
        End If

    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        RefreshData()
    End Sub
End Class