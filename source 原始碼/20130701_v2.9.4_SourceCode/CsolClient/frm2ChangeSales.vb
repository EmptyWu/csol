﻿Public Class frm2ChangeSales
    Private lstId As New ArrayList
    Private intId As Integer
    Private strSales As String

    Public Sub New(ByVal id As Integer, ByVal sales As String)
        InitializeComponent()

        intId = id
        strSales = sales

        ShowList()
    End Sub

    Private Sub ShowList()
        Dim dt As DataTable = frmMain.GetSalesList

        For index As Integer = 0 To dt.Rows.Count - 1
            lstId.Add(dt.Rows(index).Item(c_IDColumnName))
            lstboxSales.Items.Add(dt.Rows(index).Item(c_NameColumnName).trim)
        Next
        Dim i As Integer = lstId.IndexOf(strSales)
        If i > -1 Then
            lstboxSales.SelectedIndex = i
        Else
            If lstId.Count > 0 Then
                lstboxSales.SelectedIndex = 0
            End If
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butConfirm.Click

        If lstboxSales.Items.Count > 0 Then
            If lstboxSales.SelectedIndex > -1 Then
                strSales = lstId(lstboxSales.SelectedIndex)
                objCsol.ChangeStuSales(intId, strSales)
                Me.Close()
            End If
        End If

    End Sub
End Class