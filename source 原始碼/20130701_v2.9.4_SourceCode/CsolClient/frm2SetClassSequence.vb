﻿Public Class frm2SetClassSequence
    Dim lstId As New ArrayList
    Dim lstName As New ArrayList
    Dim lstSeq As New ArrayList
    Dim blFlag As Boolean = False

    Public Sub New(ByVal strName As String, _
                   ByVal Ids As ArrayList, _
                   ByVal Names As ArrayList, _
                   ByVal Sequences As ArrayList)

        InitializeComponent()

        lblClassName.Text = strName
        lstId = Ids
        lstName = Names
        lstSeq = Sequences

        dgvS.Rows.Clear()

        For index As Integer = 0 To lstId.Count - 1
            dgvS.Rows.Add(lstName(index), lstSeq(index))
        Next

        blFlag = True
    End Sub

    Private Sub RefreshTable()
        For index As Integer = 0 To lstId.Count - 1
            dgvS.Rows(index).Cells(1).Value = lstSeq(index)
        Next
    End Sub

    Private Sub dgvS_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvS.CellClick
        If dgvS.CurrentCell.ColumnIndex = 1 Then
            dgvS.CurrentCell.ReadOnly = False
            blFlag = True
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        objCsol.ModifyScSequences(lstId, lstSeq)
        Me.Close()
    End Sub

    Private Sub dgvS_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvS.CellValueChanged
        If Not dgvS.CurrentCell Is Nothing Then
            If dgvS.CurrentCell.ColumnIndex = 1 Then
                If blFlag Then
                    If IsNumeric(dgvS.CurrentCell.Value) Then
                        Dim s As Integer = CInt(dgvS.CurrentCell.Value)
                        If s > 0 Then
                            Dim s1 As Integer = lstSeq(dgvS.CurrentCell.RowIndex)
                            If s > s1 Then
                                For index As Integer = 0 To lstSeq.Count - 1
                                    If lstSeq(index) > s1 And lstSeq(index) <= s Then
                                        lstSeq(index) = lstSeq(index) - 1
                                    End If
                                Next
                                lstSeq(dgvS.CurrentCell.RowIndex) = s
                                blFlag = False
                                RefreshTable()
                            ElseIf s < s1 Then
                                For index As Integer = 0 To lstSeq.Count - 1
                                    If lstSeq(index) >= s And lstSeq(index) < s1 Then
                                        lstSeq(index) = lstSeq(index) + 1
                                    End If
                                Next
                                lstSeq(dgvS.CurrentCell.RowIndex) = s
                                blFlag = False
                                RefreshTable()
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub
End Class