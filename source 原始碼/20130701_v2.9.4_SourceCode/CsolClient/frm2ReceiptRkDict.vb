﻿Public Class frm2ReceiptRkDict
    Private dt As New DataTable

    Public Sub New()

        InitializeComponent()

        frmMain.SetOkState(False)
        frmMain.SetCurrentString("")
        RefreshData()

    End Sub

    Private Sub RefreshData()
        dt = objCsol.ListReceiptNoteDict
        dgv.DataSource = dt

    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        objCsol.UpdateRemarksDict(CType(dgv.DataSource, DataTable).GetChanges)
        frmMain.RefreshDictInfo()
        MsgBox(My.Resources.msgSaveComplete, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If dgv.SelectedRows.Count > 0 Then
            Try
                dgv.Rows.Remove(dgv.SelectedRows(0))
            Catch ex As Exception
            End Try
        End If
    End Sub

    Private Sub butSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelect.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim r As String = dgv.SelectedRows(0).Cells(c_ReasonColumnName).Value
            If Not r = "" Then
                frmMain.SetCurrentString(r)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        End If
    End Sub
End Class