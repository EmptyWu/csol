﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPunchMakeup
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPunchMakeup))
        Me.timerClock = New System.Windows.Forms.Timer(Me.components)
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.butManualPunch = New System.Windows.Forms.Button
        Me.timerUsb = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxCardNum = New System.Windows.Forms.TextBox
        Me.butStart = New System.Windows.Forms.Button
        Me.butEnd = New System.Windows.Forms.Button
        Me.cboxSession = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.chklstBook = New System.Windows.Forms.CheckedListBox
        Me.tboxStatus = New System.Windows.Forms.TextBox
        Me.lblSeatNum = New System.Windows.Forms.Label
        Me.lblMsg = New System.Windows.Forms.Label
        Me.lnklabelKeyPad = New System.Windows.Forms.LinkLabel
        Me.tboxSeatNum = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.cboxBook = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboxContent = New System.Windows.Forms.ComboBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.radbutEndClass = New System.Windows.Forms.RadioButton
        Me.radbutBeginClass = New System.Windows.Forms.RadioButton
        Me.lblTime = New System.Windows.Forms.Label
        Me.lblName = New System.Windows.Forms.Label
        Me.picbox = New System.Windows.Forms.PictureBox
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'timerClock
        '
        Me.timerClock.Enabled = True
        Me.timerClock.Interval = 1000
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'butManualPunch
        '
        Me.butManualPunch.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.butManualPunch, "butManualPunch")
        Me.butManualPunch.ForeColor = System.Drawing.Color.White
        Me.butManualPunch.Name = "butManualPunch"
        Me.butManualPunch.UseVisualStyleBackColor = False
        '
        'timerUsb
        '
        Me.timerUsb.Enabled = True
        Me.timerUsb.Interval = 250
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Name = "Label2"
        '
        'tboxCardNum
        '
        Me.tboxCardNum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxCardNum.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxCardNum, "tboxCardNum")
        Me.tboxCardNum.Name = "tboxCardNum"
        Me.tboxCardNum.ReadOnly = True
        '
        'butStart
        '
        Me.butStart.BackColor = System.Drawing.Color.Black
        resources.ApplyResources(Me.butStart, "butStart")
        Me.butStart.ForeColor = System.Drawing.Color.White
        Me.butStart.Name = "butStart"
        Me.butStart.UseVisualStyleBackColor = False
        '
        'butEnd
        '
        Me.butEnd.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        resources.ApplyResources(Me.butEnd, "butEnd")
        Me.butEnd.ForeColor = System.Drawing.Color.White
        Me.butEnd.Name = "butEnd"
        Me.butEnd.UseVisualStyleBackColor = False
        '
        'cboxSession
        '
        Me.cboxSession.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxSession, "cboxSession")
        Me.cboxSession.FormattingEnabled = True
        Me.cboxSession.Name = "cboxSession"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.ForeColor = System.Drawing.Color.White
        Me.Label1.Name = "Label1"
        '
        'chklstBook
        '
        Me.chklstBook.BackColor = System.Drawing.Color.White
        Me.chklstBook.CheckOnClick = True
        Me.chklstBook.FormattingEnabled = True
        resources.ApplyResources(Me.chklstBook, "chklstBook")
        Me.chklstBook.Name = "chklstBook"
        '
        'tboxStatus
        '
        Me.tboxStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxStatus.BorderStyle = System.Windows.Forms.BorderStyle.None
        resources.ApplyResources(Me.tboxStatus, "tboxStatus")
        Me.tboxStatus.ForeColor = System.Drawing.Color.White
        Me.tboxStatus.Name = "tboxStatus"
        Me.tboxStatus.ReadOnly = True
        '
        'lblSeatNum
        '
        resources.ApplyResources(Me.lblSeatNum, "lblSeatNum")
        Me.lblSeatNum.ForeColor = System.Drawing.Color.White
        Me.lblSeatNum.Name = "lblSeatNum"
        '
        'lblMsg
        '
        resources.ApplyResources(Me.lblMsg, "lblMsg")
        Me.lblMsg.ForeColor = System.Drawing.Color.White
        Me.lblMsg.Name = "lblMsg"
        '
        'lnklabelKeyPad
        '
        resources.ApplyResources(Me.lnklabelKeyPad, "lnklabelKeyPad")
        Me.lnklabelKeyPad.ForeColor = System.Drawing.Color.White
        Me.lnklabelKeyPad.LinkColor = System.Drawing.Color.White
        Me.lnklabelKeyPad.Name = "lnklabelKeyPad"
        Me.lnklabelKeyPad.TabStop = True
        '
        'tboxSeatNum
        '
        Me.tboxSeatNum.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxSeatNum.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxSeatNum, "tboxSeatNum")
        Me.tboxSeatNum.Name = "tboxSeatNum"
        Me.tboxSeatNum.ReadOnly = True
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Name = "Label10"
        '
        'tboxID
        '
        Me.tboxID.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.tboxID.ForeColor = System.Drawing.Color.White
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        Me.tboxID.ReadOnly = True
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Name = "Label9"
        '
        'cboxBook
        '
        Me.cboxBook.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxBook, "cboxBook")
        Me.cboxBook.FormattingEnabled = True
        Me.cboxBook.Items.AddRange(New Object() {resources.GetString("cboxBook.Items"), resources.GetString("cboxBook.Items1")})
        Me.cboxBook.Name = "cboxBook"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Name = "Label7"
        '
        'cboxContent
        '
        Me.cboxContent.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxContent, "cboxContent")
        Me.cboxContent.FormattingEnabled = True
        Me.cboxContent.Name = "cboxContent"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Name = "Label3"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Name = "Label4"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxSubClass, "cboxSubClass")
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Name = "cboxSubClass"
        '
        'cboxClass
        '
        Me.cboxClass.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.cboxClass, "cboxClass")
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Name = "cboxClass"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Name = "Label5"
        '
        'radbutEndClass
        '
        resources.ApplyResources(Me.radbutEndClass, "radbutEndClass")
        Me.radbutEndClass.ForeColor = System.Drawing.Color.White
        Me.radbutEndClass.Name = "radbutEndClass"
        Me.radbutEndClass.TabStop = True
        Me.radbutEndClass.UseVisualStyleBackColor = True
        '
        'radbutBeginClass
        '
        resources.ApplyResources(Me.radbutBeginClass, "radbutBeginClass")
        Me.radbutBeginClass.Checked = True
        Me.radbutBeginClass.ForeColor = System.Drawing.Color.White
        Me.radbutBeginClass.Name = "radbutBeginClass"
        Me.radbutBeginClass.TabStop = True
        Me.radbutBeginClass.UseVisualStyleBackColor = True
        '
        'lblTime
        '
        resources.ApplyResources(Me.lblTime, "lblTime")
        Me.lblTime.ForeColor = System.Drawing.Color.White
        Me.lblTime.Name = "lblTime"
        '
        'lblName
        '
        resources.ApplyResources(Me.lblName, "lblName")
        Me.lblName.ForeColor = System.Drawing.Color.White
        Me.lblName.Name = "lblName"
        '
        'picbox
        '
        resources.ApplyResources(Me.picbox, "picbox")
        Me.picbox.MaximumSize = New System.Drawing.Size(300, 300)
        Me.picbox.Name = "picbox"
        Me.picbox.TabStop = False
        '
        'frmPunchMakeup
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.butManualPunch)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.tboxCardNum)
        Me.Controls.Add(Me.butStart)
        Me.Controls.Add(Me.butEnd)
        Me.Controls.Add(Me.cboxSession)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.chklstBook)
        Me.Controls.Add(Me.tboxStatus)
        Me.Controls.Add(Me.lblSeatNum)
        Me.Controls.Add(Me.lblMsg)
        Me.Controls.Add(Me.lnklabelKeyPad)
        Me.Controls.Add(Me.tboxSeatNum)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.tboxID)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.cboxBook)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cboxContent)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.radbutEndClass)
        Me.Controls.Add(Me.radbutBeginClass)
        Me.Controls.Add(Me.lblTime)
        Me.Controls.Add(Me.lblName)
        Me.Controls.Add(Me.picbox)
        Me.ForeColor = System.Drawing.Color.White
        Me.Name = "frmPunchMakeup"
        CType(Me.picbox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents timerClock As System.Windows.Forms.Timer
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents butManualPunch As System.Windows.Forms.Button
    Friend WithEvents timerUsb As System.Windows.Forms.Timer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxCardNum As System.Windows.Forms.TextBox
    Friend WithEvents butStart As System.Windows.Forms.Button
    Friend WithEvents butEnd As System.Windows.Forms.Button
    Friend WithEvents cboxSession As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chklstBook As System.Windows.Forms.CheckedListBox
    Friend WithEvents tboxStatus As System.Windows.Forms.TextBox
    Friend WithEvents lblSeatNum As System.Windows.Forms.Label
    Friend WithEvents lblMsg As System.Windows.Forms.Label
    Friend WithEvents lnklabelKeyPad As System.Windows.Forms.LinkLabel
    Friend WithEvents tboxSeatNum As System.Windows.Forms.TextBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents cboxBook As System.Windows.Forms.ComboBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboxContent As System.Windows.Forms.ComboBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents radbutEndClass As System.Windows.Forms.RadioButton
    Friend WithEvents radbutBeginClass As System.Windows.Forms.RadioButton
    Friend WithEvents lblTime As System.Windows.Forms.Label
    Friend WithEvents lblName As System.Windows.Forms.Label
    Friend WithEvents picbox As System.Windows.Forms.PictureBox
End Class
