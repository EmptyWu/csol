﻿Public Class frmPotentialStu
    Private intSearchIndex As Integer = 0
    Private strKw As String = ""
    Private dtStu As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPotentialStu
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()
        InitSelections()
        Me.Text = My.Resources.frmPotentialStu
    End Sub

    Private Sub mnuPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrint.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgv.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgv.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgv.Rows.Count

            Dim oRow As DataGridViewRow = dgv.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgv.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgv.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgv.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgv.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgv.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgv.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgv.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgv.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Public Sub SearchStu(ByVal action As Integer, ByVal kw As String, ByVal type As Integer)
        If action = 0 Then 'search first match
            intSearchIndex = 0
            strKw = kw
        End If
        Select Case type
            Case 1 'ID
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_IDColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 2 'Name
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_NameColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
            Case 3 'Tel
                For index As Integer = intSearchIndex To dgv.Rows.Count - 1
                    If dgv.Rows(index).Cells(c_Tel1ColumnName).Value = strKw Then
                        intSearchIndex = index
                        dgv.Rows(index).Selected = True
                        Exit For
                    End If
                Next
        End Select
    End Sub

    Private Sub frmPotentialStu_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPotentialStu_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Private Sub LoadColumnText()
        For Each col In dgv.Columns
            col.visible = False
        Next
        If dgv.Rows.Count > 0 Then

            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_IDColumnName).Visible = True
            dgv.Columns.Item(c_IDColumnName).HeaderText = My.Resources.stuID
            dgv.Columns.Item(c_NameColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_NameColumnName).Visible = True
            dgv.Columns.Item(c_NameColumnName).HeaderText = My.Resources.humanName
            dgv.Columns.Item(c_EngNameColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_EngNameColumnName).Visible = True
            dgv.Columns.Item(c_EngNameColumnName).HeaderText = My.Resources.engName
            dgv.Columns.Item(c_SexColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_SexColumnName).Visible = True
            dgv.Columns.Item(c_SexColumnName).HeaderText = My.Resources.sex
            dgv.Columns.Item(c_MumNameColumnName).DisplayIndex = 4
            dgv.Columns.Item(c_MumNameColumnName).Visible = True
            dgv.Columns.Item(c_MumNameColumnName).HeaderText = My.Resources.mumName
            dgv.Columns.Item(c_Tel1ColumnName).DisplayIndex = 5
            dgv.Columns.Item(c_Tel1ColumnName).Visible = True
            dgv.Columns.Item(c_Tel1ColumnName).HeaderText = My.Resources.tel1
            dgv.Columns.Item(c_MobileColumnName).DisplayIndex = 6
            dgv.Columns.Item(c_MobileColumnName).Visible = True
            dgv.Columns.Item(c_MobileColumnName).HeaderText = My.Resources.stuMobile
        End If
        Dim i As Integer = 7
        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow(index) = 1 Then
                dgv.Columns.Item(lstColName(index)).DisplayIndex = i
                dgv.Columns.Item(lstColName(index)).Visible = True
                dgv.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                i = i + 1
            End If
        Next
        If dgv.Columns.Item(c_SchoolColumnName).Visible = True Then
            Dim id As Integer
            Select Case intColSchType
                Case 0
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_PrimarySchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 1
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_JuniorSchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 2
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_HighSchColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 3
                    For index As Integer = 0 To dgv.Rows.Count - 1
                        id = dgv.Rows(index).Cells(c_UniversityColumnName).Value
                        dgv.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
            End Select
        End If
    End Sub

    Private Sub InitSelections()
        lstColName.Add(c_BirthdayColumnName)
        lstColName.Add(c_AddressColumnName)
        lstColName.Add(c_PostalCodeColumnName)
        lstColName.Add(c_Address2ColumnName)
        lstColName.Add(c_PostalCode2ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_OfficeTelColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_CurrentSchColumnName)
        lstColName.Add(c_SchGroupColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_DadTitleColumnName)
        lstColName.Add(c_MumTitleColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_CreateDateColumnName)
        lstColName.Add(c_RemarksColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgv.Columns.Count > 6 Then
            Dim lst As New ArrayList
            For index As Integer = 7 To dgv.Columns.Count - 1
                If dgv.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub frmPotentialStu_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub mnuDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDetails.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
            If id.Length = 8 Then
                frmMain.DisplayStuInfo(id, 0)
            End If
        End If
    End Sub

    Private Sub mnuSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSearch.Click
        frmMain.ShowSearchStu2(4)
    End Sub

    Private Sub mnuDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuDelete.Click
        If Not frmMain.CheckAuth(64) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Dim Answer As MsgBoxResult = MsgBox("請問真的確定要刪除該學生資料??", MsgBoxStyle.YesNo, "警告!!")
        Select Case Answer
            Case MsgBoxResult.Yes
                If dgv.SelectedRows.Count > 0 Then
                    Dim id As String = dgv.SelectedRows(0).Cells(c_IDColumnName).Value
                    If id.Length = 8 Then
                        objCsol.DeleteStudent(id)
                        DeleteStu(id)
                    End If
                End If
            Case MsgBoxResult.No
                Exit Sub
            Case Else
                Exit Sub
        End Select
    End Sub

    Private Sub DeleteStu(ByVal id As String)
        For index As Integer = 0 To dtStu.Rows.Count - 1
            If dtStu.Rows(index).Item(c_IDColumnName) = id Then
                dtStu.Rows.RemoveAt(index)
            End If
        Next
    End Sub

    Private Sub butFilter_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butFilter.Click
        If cboxItem.SelectedIndex > -1 And cboxFilter.SelectedIndex > -1 And _
            Not tboxFilter.Text.Trim = "" Then
            Dim t As Integer = 0
            Select Case cboxItem.SelectedIndex
                Case 0
                    t = 7
                Case 1
                    t = 2
                Case 2
                    t = 3
                Case 3
                    t = 5
            End Select
            dtStu = objCsol.SearchStu(t, tboxFilter.Text.Trim, c_StuTypePotential, -1)
            'dtStu = SearchStu(t, tboxFilter.Text.Trim, c_StuTypePotential, -1)
            For Each row As DataRow In dtStu.Rows
                Dim key_Group As String = row.Item("Schgroup").ToString.Trim()
                Dim key_Current As String = row.Item("CurrentSch").ToString.Trim()
                Dim key_Grade As String = row.Item("SchoolGrade").ToString.Trim()
                Select Case key_Group
                    Case "-1"
                        row.Item("Schgroup") = ""
                    Case "0"
                        row.Item("Schgroup") = 1
                    Case "1"
                        row.Item("Schgroup") = 2
                    Case "2"
                        row.Item("Schgroup") = 3
                    Case "3"
                        row.Item("Schgroup") = 4
                    Case Else
                End Select

                Select Case key_Current
                    Case "0"
                        'row.Item("SchoolGrade") = 1
                        Continue For
                    Case "1"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case "3"
                                row.Item("SchoolGrade") = 4
                                Continue For
                            Case "4"
                                row.Item("SchoolGrade") = 5
                                Continue For
                            Case "5"
                                row.Item("SchoolGrade") = 6
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "2"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "3"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case "4"
                        Select Case key_Grade
                            Case "0"
                                row.Item("SchoolGrade") = 1
                                Continue For
                            Case "1"
                                row.Item("SchoolGrade") = 2
                                Continue For
                            Case "2"
                                row.Item("SchoolGrade") = 3
                                Continue For
                            Case "3"
                                row.Item("SchoolGrade") = 4
                                Continue For
                            Case Else
                                row.Item("SchoolGrade") = -1
                                Continue For
                        End Select
                    Case Else
                        Continue For
                End Select
            Next
            

            dgv.DataSource = dtStu.DefaultView
            LoadColumnText()
        End If
    End Sub

    'Private Shared Function SearchStu(ByVal method As Integer, ByVal kw As String, ByVal type As Byte, ByVal int As Integer) As DataTable


    '    Dim dt As New DataTable
    '    Dim RequestParams As New System.Collections.Specialized.NameValueCollection
    '    Dim list As New List(Of String)
    '    'For i As Integer = 0 To lstClass.Count - 1
    '    '    list.Add(lstClass(i))
    '    'Next
    '    'RequestParams.Add("classid", String.Join(",", list.ToArray))
    '    'RequestParams.Add("DateFrom", DateFrom)
    '    'RequestParams.Add("DateTo", DateTo)
    '    RequestParams.Add("method", method)
    '    RequestParams.Add("kw", kw)
    '    RequestParams.Add("type", type)


    '    Dim ResponseData() As Byte = CSOL.HTTPClient.Post("Accounting", "GetPayStaClass", RequestParams)
    '    Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
    '    Dim xmlstring As String = ""
    '    Try
    '        Using ms As New System.IO.MemoryStream
    '            zip("PayStaClass").Extract(ms)
    '            ms.Flush()
    '            ms.Position = 0
    '            Dim sr As New System.IO.StreamReader(ms)
    '            xmlstring = sr.ReadToEnd()
    '        End Using
    '        dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("PayStaClass"))
    '    Catch ex As Exception
    '        System.Diagnostics.Debug.WriteLine(ex)
    '    Finally
    '    End Try
    '    Return dt
    'End Function


    Private Sub mnuExportData_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuExportData.Click
        ExportDgvToExcel(dgv)
    End Sub

    Private Sub mnuExportPhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportPhoto.Click

    End Sub
End Class