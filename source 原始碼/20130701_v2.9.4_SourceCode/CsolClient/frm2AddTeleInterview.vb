﻿Public Class frm2AddTeleInterview
    Dim strId As String = ""
    Dim strName As String = ""
    Dim dtType As New DataTable
    Dim lstTypeId As New ArrayList

    Private Sub tboxID_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tboxID.TextChanged
        If tboxID.Text.Trim.Length > 7 Then
            strId = tboxID.Text.Trim
            strName = objCsol.GetStuNameByID(strId)
            If strName.Length > 0 Then
                tboxName.Text = strName
            Else
                tboxName.Text = ""
                MsgBox(My.Resources.msgNoSuchStuID, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            End If
        End If
    End Sub

    Private Sub frm2AddTeleInterview_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        frmMain.SetOkState(False)
        RefreshData()
    End Sub

    Private Sub RefreshData()
        dtType = frmMain.GetTeleInterviewType
        lstTypeId.Clear()
        cboxType.Items.Clear()
        For index As Integer = 0 To dtType.Rows.Count - 1
            cboxType.Items.Add(dtType.Rows(index).Item(c_TypeColumnName).ToString.Trim)
            lstTypeId.Add(dtType.Rows(index).Item(c_IDColumnName))
        Next

    End Sub

    Private Sub butAddType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butAddType.Click
        Dim frm As New frm2AddTeleInterviewType
        frm.ShowDialog()
        If frmMain.GetOkState Then
            frmMain.SetOkState(False)
            frmMain.RefreshTeleInterviewType()
            RefreshData()
        End If

    End Sub

    Private Sub butSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSave.Click
        If tboxID.Text.Trim.Length > 7 And tboxName.Text.Trim.Length > 0 And _
            cboxType.SelectedIndex > -1 And tboxContent.Text.Trim.Length > 0 And lstTypeId.Count > 0 Then
            objCsol.AddTeleInterview(tboxID.Text.Trim, dtpickDate.Value, lstTypeId(cboxType.SelectedIndex), _
                                    tboxContent.Text.Trim, frmMain.GetUsrId)
            Me.Close()
        Else
            MsgBox(My.Resources.msgInvalidTeleInterviewInput, _
                            MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
        End If
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub
End Class