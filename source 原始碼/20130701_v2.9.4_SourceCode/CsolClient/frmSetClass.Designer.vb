﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetClass))
        Me.dgvSession = New System.Windows.Forms.DataGridView
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.butDelSession = New System.Windows.Forms.Button
        Me.butModSession = New System.Windows.Forms.Button
        Me.butAddSession = New System.Windows.Forms.Button
        Me.mnuContent = New System.Windows.Forms.ToolStripMenuItem
        Me.dgvSubClass = New System.Windows.Forms.DataGridView
        Me.mnuRemove = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuNew = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvClass = New System.Windows.Forms.DataGridView
        Me.mnuModify = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.butDelSc = New System.Windows.Forms.Button
        Me.butModSc = New System.Windows.Forms.Button
        Me.butAddSc = New System.Windows.Forms.Button
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuSequence = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuTodayClass = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSubject = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        CType(Me.dgvSession, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvSubClass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.mnustrTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvSession
        '
        Me.dgvSession.AllowUserToAddRows = False
        Me.dgvSession.AllowUserToDeleteRows = False
        Me.dgvSession.AllowUserToResizeRows = False
        Me.dgvSession.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvSession.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvSession, "dgvSession")
        Me.dgvSession.MultiSelect = False
        Me.dgvSession.Name = "dgvSession"
        Me.dgvSession.RowHeadersVisible = False
        Me.dgvSession.RowTemplate.Height = 15
        Me.dgvSession.RowTemplate.ReadOnly = True
        Me.dgvSession.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSession.ShowCellToolTips = False
        Me.dgvSession.ShowEditingIcon = False
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.butDelSession)
        Me.GroupBox4.Controls.Add(Me.butModSession)
        Me.GroupBox4.Controls.Add(Me.butAddSession)
        Me.GroupBox4.Controls.Add(Me.dgvSession)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'butDelSession
        '
        resources.ApplyResources(Me.butDelSession, "butDelSession")
        Me.butDelSession.Name = "butDelSession"
        Me.butDelSession.UseVisualStyleBackColor = True
        '
        'butModSession
        '
        resources.ApplyResources(Me.butModSession, "butModSession")
        Me.butModSession.Name = "butModSession"
        Me.butModSession.UseVisualStyleBackColor = True
        '
        'butAddSession
        '
        resources.ApplyResources(Me.butAddSession, "butAddSession")
        Me.butAddSession.Name = "butAddSession"
        Me.butAddSession.UseVisualStyleBackColor = True
        '
        'mnuContent
        '
        Me.mnuContent.Name = "mnuContent"
        resources.ApplyResources(Me.mnuContent, "mnuContent")
        '
        'dgvSubClass
        '
        Me.dgvSubClass.AllowUserToAddRows = False
        Me.dgvSubClass.AllowUserToDeleteRows = False
        Me.dgvSubClass.AllowUserToResizeRows = False
        Me.dgvSubClass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvSubClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvSubClass, "dgvSubClass")
        Me.dgvSubClass.MultiSelect = False
        Me.dgvSubClass.Name = "dgvSubClass"
        Me.dgvSubClass.RowHeadersVisible = False
        Me.dgvSubClass.RowTemplate.Height = 15
        Me.dgvSubClass.RowTemplate.ReadOnly = True
        Me.dgvSubClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvSubClass.ShowCellToolTips = False
        Me.dgvSubClass.ShowEditingIcon = False
        '
        'mnuRemove
        '
        Me.mnuRemove.Name = "mnuRemove"
        resources.ApplyResources(Me.mnuRemove, "mnuRemove")
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuNew
        '
        Me.mnuNew.Name = "mnuNew"
        resources.ApplyResources(Me.mnuNew, "mnuNew")
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        resources.ApplyResources(Me.mnuSearch, "mnuSearch")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvClass)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'dgvClass
        '
        Me.dgvClass.AllowUserToAddRows = False
        Me.dgvClass.AllowUserToDeleteRows = False
        Me.dgvClass.AllowUserToResizeRows = False
        Me.dgvClass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClass, "dgvClass")
        Me.dgvClass.MultiSelect = False
        Me.dgvClass.Name = "dgvClass"
        Me.dgvClass.RowHeadersVisible = False
        Me.dgvClass.RowTemplate.Height = 15
        Me.dgvClass.RowTemplate.ReadOnly = True
        Me.dgvClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClass.ShowCellToolTips = False
        Me.dgvClass.ShowEditingIcon = False
        '
        'mnuModify
        '
        Me.mnuModify.Name = "mnuModify"
        resources.ApplyResources(Me.mnuModify, "mnuModify")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.butDelSc)
        Me.GroupBox2.Controls.Add(Me.butModSc)
        Me.GroupBox2.Controls.Add(Me.butAddSc)
        Me.GroupBox2.Controls.Add(Me.dgvSubClass)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'butDelSc
        '
        resources.ApplyResources(Me.butDelSc, "butDelSc")
        Me.butDelSc.Name = "butDelSc"
        Me.butDelSc.UseVisualStyleBackColor = True
        '
        'butModSc
        '
        resources.ApplyResources(Me.butModSc, "butModSc")
        Me.butModSc.Name = "butModSc"
        Me.butModSc.UseVisualStyleBackColor = True
        '
        'butAddSc
        '
        resources.ApplyResources(Me.butAddSc, "butAddSc")
        Me.butAddSc.Name = "butAddSc"
        Me.butAddSc.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuNew, Me.mnuModify, Me.mnuSearch, Me.mnuRemove, Me.mnuContent, Me.mnuSequence, Me.mnuTodayClass, Me.mnuSubject, Me.mnuPrint, Me.mnuExport, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuSequence
        '
        Me.mnuSequence.Name = "mnuSequence"
        resources.ApplyResources(Me.mnuSequence, "mnuSequence")
        '
        'mnuTodayClass
        '
        Me.mnuTodayClass.Name = "mnuTodayClass"
        resources.ApplyResources(Me.mnuTodayClass, "mnuTodayClass")
        '
        'mnuSubject
        '
        Me.mnuSubject.Name = "mnuSubject"
        resources.ApplyResources(Me.mnuSubject, "mnuSubject")
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        resources.ApplyResources(Me.mnuExport, "mnuExport")
        '
        'PrintDocument1
        '
        '
        'frmSetClass
        '
        Me.AllowDrop = True
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.mnustrTop)
        Me.Name = "frmSetClass"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.dgvSession, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        CType(Me.dgvSubClass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvSession As System.Windows.Forms.DataGridView
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents mnuContent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvSubClass As System.Windows.Forms.DataGridView
    Friend WithEvents mnuRemove As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuNew As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvClass As System.Windows.Forms.DataGridView
    Friend WithEvents mnuModify As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents butAddSc As System.Windows.Forms.Button
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuSequence As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuTodayClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSubject As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents butDelSession As System.Windows.Forms.Button
    Friend WithEvents butModSession As System.Windows.Forms.Button
    Friend WithEvents butAddSession As System.Windows.Forms.Button
    Friend WithEvents butDelSc As System.Windows.Forms.Button
    Friend WithEvents butModSc As System.Windows.Forms.Button
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
End Class
