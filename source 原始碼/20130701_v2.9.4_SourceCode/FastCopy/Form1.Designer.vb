﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.tboxMsg = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'Timer1
        '
        '
        'tboxMsg
        '
        Me.tboxMsg.BackColor = System.Drawing.SystemColors.Control
        Me.tboxMsg.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.tboxMsg.Enabled = False
        Me.tboxMsg.Font = New System.Drawing.Font("標楷體", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.tboxMsg.Location = New System.Drawing.Point(12, 12)
        Me.tboxMsg.Multiline = True
        Me.tboxMsg.Name = "tboxMsg"
        Me.tboxMsg.ReadOnly = True
        Me.tboxMsg.Size = New System.Drawing.Size(377, 42)
        Me.tboxMsg.TabIndex = 1
        Me.tboxMsg.Text = "各位同仁好，我是MIS，管理系統用戶端有新版程式，目前正在進行更新，請稍後片刻，謝謝。"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(398, 65)
        Me.ControlBox = False
        Me.Controls.Add(Me.tboxMsg)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Form1"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "更新清單"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents tboxMsg As System.Windows.Forms.TextBox

End Class
