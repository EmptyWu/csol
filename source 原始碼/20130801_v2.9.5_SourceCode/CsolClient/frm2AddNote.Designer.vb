﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddNote
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frm2AddNote))
        Me.Label4 = New System.Windows.Forms.Label
        Me.butConfirm = New System.Windows.Forms.Button
        Me.radbutInfoBox = New System.Windows.Forms.RadioButton
        Me.radbutFullScreen = New System.Windows.Forms.RadioButton
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.tboxNote = New System.Windows.Forms.TextBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.radbutEverytime = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.tboxID = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutOnce = New System.Windows.Forms.RadioButton
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxSeconds = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.radbutRepeat = New System.Windows.Forms.RadioButton
        Me.radbutOnlyTod = New System.Windows.Forms.RadioButton
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.butCancel = New System.Windows.Forms.Button
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'butConfirm
        '
        resources.ApplyResources(Me.butConfirm, "butConfirm")
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'radbutInfoBox
        '
        resources.ApplyResources(Me.radbutInfoBox, "radbutInfoBox")
        Me.radbutInfoBox.Name = "radbutInfoBox"
        Me.radbutInfoBox.UseVisualStyleBackColor = True
        '
        'radbutFullScreen
        '
        resources.ApplyResources(Me.radbutFullScreen, "radbutFullScreen")
        Me.radbutFullScreen.Checked = True
        Me.radbutFullScreen.Name = "radbutFullScreen"
        Me.radbutFullScreen.TabStop = True
        Me.radbutFullScreen.UseVisualStyleBackColor = True
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.radbutInfoBox)
        Me.GroupBox4.Controls.Add(Me.radbutFullScreen)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.tboxNote)
        Me.GroupBox5.Controls.Add(Me.Label5)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'tboxNote
        '
        resources.ApplyResources(Me.tboxNote, "tboxNote")
        Me.tboxNote.Name = "tboxNote"
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'radbutEverytime
        '
        resources.ApplyResources(Me.radbutEverytime, "radbutEverytime")
        Me.radbutEverytime.Name = "radbutEverytime"
        Me.radbutEverytime.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tboxID)
        Me.GroupBox1.Controls.Add(Me.Label3)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'tboxID
        '
        resources.ApplyResources(Me.tboxID, "tboxID")
        Me.tboxID.Name = "tboxID"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutEverytime)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.radbutOnce)
        Me.GroupBox3.Controls.Add(Me.Label2)
        Me.GroupBox3.Controls.Add(Me.tboxSeconds)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'radbutOnce
        '
        resources.ApplyResources(Me.radbutOnce, "radbutOnce")
        Me.radbutOnce.Checked = True
        Me.radbutOnce.Name = "radbutOnce"
        Me.radbutOnce.TabStop = True
        Me.radbutOnce.UseVisualStyleBackColor = True
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'tboxSeconds
        '
        resources.ApplyResources(Me.tboxSeconds, "tboxSeconds")
        Me.tboxSeconds.Name = "tboxSeconds"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.radbutRepeat)
        Me.GroupBox2.Controls.Add(Me.radbutOnlyTod)
        Me.GroupBox2.Controls.Add(Me.dtpickDate)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'radbutRepeat
        '
        resources.ApplyResources(Me.radbutRepeat, "radbutRepeat")
        Me.radbutRepeat.Name = "radbutRepeat"
        Me.radbutRepeat.UseVisualStyleBackColor = True
        '
        'radbutOnlyTod
        '
        resources.ApplyResources(Me.radbutOnlyTod, "radbutOnlyTod")
        Me.radbutOnlyTod.Checked = True
        Me.radbutOnlyTod.Name = "radbutOnlyTod"
        Me.radbutOnlyTod.TabStop = True
        Me.radbutOnlyTod.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        resources.ApplyResources(Me.dtpickDate, "dtpickDate")
        Me.dtpickDate.Name = "dtpickDate"
        '
        'butCancel
        '
        resources.ApplyResources(Me.butCancel, "butCancel")
        Me.butCancel.Name = "butCancel"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2AddNote
        '
        Me.AcceptButton = Me.butConfirm
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.butCancel)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2AddNote"
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents radbutInfoBox As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFullScreen As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents radbutEverytime As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxID As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutOnce As System.Windows.Forms.RadioButton
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxSeconds As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents radbutRepeat As System.Windows.Forms.RadioButton
    Friend WithEvents radbutOnlyTod As System.Windows.Forms.RadioButton
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents tboxNote As System.Windows.Forms.TextBox
End Class
