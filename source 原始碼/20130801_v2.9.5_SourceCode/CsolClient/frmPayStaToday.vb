﻿Public Class frmPayStaToday
    Private dsPaySta As New DataSet
    Private dtPaySta As New DataTable
    Private dtPaySta1 As New DataTable
    Private dtPay As New DataTable
    Private dtMBackSta As New DataTable
    Private dtMKeepSta As New DataTable
    Private dtKBSta As New DataTable
    Private dtKBS As New DataTable
    Private dtPayClass As New DataTable()
    Private dtKBClass As New DataTable()
    Private dateFrom As Date
    Private dateTo As Date
    Private intPayMethod As Integer
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmPaySta
    Private sUserName As String = frmMain.GetUsrName
    Dim intSize As Integer = GetPrintSize()

    Dim flag As Integer = 1
    Dim strPerPages As String = ""

    Public Sub New(ByVal dtFrom As Date, ByVal dtTo As Date)
        InitializeComponent()

        Me.Text = My.Resources.frmPaySta
        SetDate(dtFrom, dtTo)
        InitColSelections()
        'RefreshData()
    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvClassPay.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvClassPay.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvClassPay.Rows.Count

            Dim oRow As DataGridViewRow = dgvClassPay.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvClassPay.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvClassPay.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvClassPay.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvClassPay.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvClassPay.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvClassPay.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvClassPay.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvClassPay.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvClassPay.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Friend Sub SetDate(ByVal dtFrom As Date, ByVal dtTo As Date)
        dtpickFrom.Value = dtFrom
        dtpickTo.Value = dtTo
    End Sub

    Private Sub frmPayStaToday_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub frmPayStaToday_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmPayStaToday_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim i As Integer = 0
        Dim intClass As Integer = -1
        Dim intRow As Integer = -1
        Dim intTotal As Integer = 0
        Dim intTotal2 As Integer = 0
        Dim intTotal3 As Integer = 0
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_ReceiptNumColumnName + ")"
        Dim strCompute4 As String
        Dim strCompute5 As String

        dtKBSta = New DataTable()

        dateFrom = GetDateStart(dtpickFrom.Value)
        dateTo = GetDateEnd(dtpickTo.Value)

        Try

            'CSOL.Logger.LogMessage("frmPayStaToday1: " & Now.ToString())
            Dim dateTimeFilter As String = c_DateTimeColumnName & "<='" & dateTo.Date & "' AND " & c_DateTimeColumnName & ">='" & dateFrom.Date & "'"
            Dim dateTimeFilter1 As String = c_DateTimeColumnName & "<='" & dateTo & "' AND " & c_DateTimeColumnName & ">='" & dateFrom & "'"

            Dim payDateTimeFilter As String = c_PayDateTimeColumnName & "<='" & dateTo & "' AND " & c_PayDateTimeColumnName & ">='" & dateFrom & "'"
            Dim payDateTimeFilter1 As String = c_PayDateTimeColumnName & "<='" & dateTo.Date & "' AND " & c_PayDateTimeColumnName & ">='" & dateFrom.Date & "'"

            'dsPaySta = objCsol.ListPayStatistics(dateFrom, dateTo)
            'CSOL.Logger.LogMessage("A1.1.1: " & Now.ToString())

            'dsPaySta = ListPayStatistics(dateFrom, dateTo)
            dsPaySta = ListPayStatisticsHelper(dateFrom, dateTo)

            'CSOL.Logger.LogMessage("frmPayStaToday1.1: " & Now.ToString())


            dtPaySta = dsPaySta.Tables.Item(c_PayStaByDateTableName)
            dtPaySta.Columns("Amount").ColumnName = "PayAmount"
            dtMBackSta = dsPaySta.Tables.Item(c_MBackStaByDateTableName)
            dtMKeepSta = dsPaySta.Tables.Item(c_MKeepStaByDateTableName)
            AddIsNewReg()

            'CSOL.Logger.LogMessage("frmPayStaToday1.2: " & Now.ToString())

            dtPayClass = New DataTable()
            dtKBClass = New DataTable()

            dtKBSta = dtMBackSta.Clone()
            dtKBSta.Merge(dtMBackSta)
            dtKBSta.Merge(dtMKeepSta)
            dtKBSta.DefaultView.RowFilter = dateTimeFilter + " OR " + dateTimeFilter1
            dtKBSta = dtKBSta.DefaultView.ToTable

            'CSOL.Logger.LogMessage("frmPayStaToday1.3: " & Now.ToString())

            dtMBackSta.DefaultView.RowFilter = payDateTimeFilter + " OR " + payDateTimeFilter1
            dtMBackSta = dtMBackSta.DefaultView.ToTable

            dtPaySta1 = dsPaySta.Tables.Item(c_PayStaByDateTableName).Clone
            dtPaySta1.Merge(dtMBackSta)
            dtPaySta1.Merge(dtPaySta)
            dtPaySta1.DefaultView.RowFilter = payDateTimeFilter + " OR " + payDateTimeFilter1
            dtPaySta1 = dtPaySta1.DefaultView.ToTable

            'CSOL.Logger.LogMessage("frmPayStaToday1.4: " & Now.ToString())

            dtPayClass = dtPaySta1.DefaultView.ToTable(True, c_ClassIDColumnName, c_ClassNameColumnName)
            dtKBClass = dtKBSta.DefaultView.ToTable(True, c_ClassIDColumnName, c_ClassNameColumnName)
            InitTable()

            'CSOL.Logger.LogMessage("frmPayStaToday2: " & Now.ToString())

            If dtPayClass.Rows.Count > 0 And dtPaySta1.Rows.Count > 0 Then

                'CSOL.Logger.LogMessage("frmPayStaToday3: " & Now.ToString())

                For i = 0 To dtPayClass.Rows.Count - 1
                    intClass = dtPayClass.Rows(i).Item(c_ClassIDColumnName)
                    strCompute2 = c_ClassIDColumnName + "=" + intClass.ToString
                    'intTotal = dtPaySta.Compute(strCompute1, strCompute2)
                    intTotal = dtPaySta1.Compute("SUM(PayAmount)", strCompute2)
                    dtPayClass.Rows(i).Item(c_TotalPayColumnName) = intTotal
                    'intTotal = dtPaySta.Compute(strCompute3, strCompute2)
                    intTotal = dtPaySta1.Compute(strCompute3, strCompute2)
                    dtPayClass.Rows(i).Item(c_DataCntColumnName) = intTotal
                    If cbTodayStuSta.Checked = True Then

                        strCompute2 = c_ClassIDColumnName + "=" + intClass.ToString + _
                                " AND " + c_IsNewRegColumnName + "=True"
                        'intTotal = dtPaySta.Compute(strCompute3, strCompute2)
                        'intTotal = dtPaySta1.Compute(strCompute3, strCompute2)

                        intTotal = 0
                        Dim list As New ArrayList

                        For Each row As DataRow In dtPaySta1.Select(String.Format("{0} = '{1}' AND {2} = '{3}' And {4} <= '{5} 23:59:59.999' AND {4} >= '00:00:00.000'", _
                                                                                  c_ClassIDColumnName, intClass.ToString, c_IsNewRegColumnName, "True", c_RegDateColumnName, Now.ToString("yyyy/MM/dd")))
                            'For Each row As DataRow In dtPaySta1.Select(String.Format("{0} = '{1}' AND {2} = '{3}' And {5} <= '{4}' AND {4} >= '{6}'", _
                            '                                                      c_ClassIDColumnName, intClass.ToString, c_IsNewRegColumnName, "True", c_RegDateColumnName, dateFrom.ToString("yyyy/MM/dd")))


                            'For Each row As DataRow In dtPaySta1.Select(c_ClassIDColumnName & " = '" & intClass.ToString & "' AND " & c_IsNewRegColumnName & " = '" & "True" & String.Format("' And {1} <= '{0}' AND {0} >= '{2}'", c_RegDateColumnName, dateFrom, dateTo))
                            'For Each row As DataRow In dtPaySta1.Select(String.Format("' And {1} <= '{0}' AND {0} >= '{2}'", c_RegDateColumnName, dateFrom, dateTo))
                            Dim stuid As String = row.Item("StuId")
                            If list.IndexOf(stuid) = -1 Then
                                list.Add(stuid)
                                intTotal += 1
                            End If
                        Next
                        dtPayClass.Rows(i).Item(c_NewCntColumnName) = intTotal                              '100301

                        strCompute5 = c_ClassIDColumnName + "=" + intClass.ToString
                        'intTotal = dtPaySta.Compute(strCompute3, strCompute5)
                        intTotal = dtPaySta1.Compute(strCompute3, strCompute5)
                        dtPayClass.Rows(i).Item(c_DataCntColumnName) = intTotal                             '100301
                    End If
                Next
            End If

            'CSOL.Logger.LogMessage("frmPayStaToday4: " & Now.ToString())

            If dtKBClass.Rows.Count > 0 And dtKBSta.Rows.Count > 0 Then
                strCompute1 = "SUM(" + c_PayAmountColumnName + ")"
                strCompute4 = "SUM(" + c_AmountColumnName + ")"

                'CSOL.Logger.LogMessage("frmPayStaToday5: " & Now.ToString())

                For i = 0 To dtKBClass.Rows.Count - 1
                    intClass = dtKBClass.Rows(i).Item(c_ClassIDColumnName)
                    strCompute2 = c_ClassIDColumnName + "=" + intClass.ToString
                    intTotal = dtKBSta.Compute(strCompute1, strCompute2)
                    dtKBClass.Rows(i).Item(c_TotalPayColumnName) = intTotal
                    intTotal = dtKBSta.Compute(strCompute3, strCompute2)
                    dtKBClass.Rows(i).Item(c_DataCntColumnName) = intTotal
                    'If dtMBackSta.Rows.Count > 0 Then
                    '    intTotal = dtMBackSta.Compute(strCompute4, strCompute2)
                    '    dtKBClass.Rows(i).Item(c_TotalBackColumnName) = intTotal
                    'End If
                    If dtKBSta.Rows.Count > 0 Then
                        intTotal = dtKBSta.Compute(strCompute4, strCompute2)
                        dtKBClass.Rows(i).Item(c_TotalBackColumnName) = intTotal
                    End If
                    If dtMKeepSta.Rows.Count > 0 Then
                        intTotal = dtMKeepSta.Compute(strCompute4, strCompute2)
                        dtKBClass.Rows(i).Item(c_TotalKeepColumnName) = intTotal
                    End If
                Next
            End If

            'CSOL.Logger.LogMessage("frmPayStaToday6: " & Now.ToString())

            strCompute1 = "SUM(" + c_DataCntColumnName + ")"
            strCompute2 = "SUM(" + c_NewCntColumnName + ")"
            strCompute3 = "SUM(" + c_TotalPayColumnName + ")"
            If dtPayClass.Rows.Count > 0 Then
                dtPayClass.Rows.Add(-1, My.Resources.totalAmt, dtPayClass.Compute(strCompute1, ""), _
                                    dtPayClass.Compute(strCompute2, ""), dtPayClass.Compute(strCompute3, ""))
            Else
                dtPayClass.Rows.Add(-1, My.Resources.totalAmt, 0, 0, 0)
            End If

            If dtKBClass.Rows.Count > 0 Then
                strCompute2 = "SUM(" + c_TotalBackColumnName + ")"
                strCompute4 = "SUM(" + c_TotalKeepColumnName + ")"
                dtKBClass.Rows.Add(-1, My.Resources.totalAmt, dtKBClass.Compute(strCompute1, ""), _
                                    dtKBClass.Compute(strCompute3, ""), dtKBClass.Compute(strCompute2, ""), _
                                    dtKBClass.Compute(strCompute4, ""))
            Else
                dtKBClass.Rows.Add(-1, My.Resources.totalAmt, 0, 0, 0, 0)
            End If

            'dgvClassPay.DataSource = dtPayClass
            'dgvClassKB.DataSource = dtKBClass

            'If dgvClassPay.RowCount > 0 Then
            '    dgvClassPay.CurrentCell = dgvClassPay.Rows.Item(dgvClassPay.RowCount - 1).Cells(c_ClassNameColumnName)
            'End If
            'If dgvClassKB.RowCount > 0 Then
            '    dgvClassKB.CurrentCell = dgvClassKB.Rows.Item(dgvClassKB.RowCount - 1).Cells(c_ClassNameColumnName)
            'End If

            'If radbutAllStu.Checked = True Then

            '    dtPaySta1.DefaultView.RowFilter = ""
            '    dgvClassPayDetails.DataSource = dtPaySta1.DefaultView
            'Else
            '    dtPaySta1.DefaultView.RowFilter = c_IsNewRegColumnName + "=" + True.ToString
            '    dgvClassPayDetails.DataSource = dtPaySta1.DefaultView

            'End If



            'dgvClassKBDetails.DataSource = dtKBSta.DefaultView

            'If dtPayClass.Rows.Count > 1 Then
            '    intTotal = dtPayClass.Rows(dtPayClass.Rows.Count - 1).Item(c_TotalPayColumnName)
            'Else
            '    intTotal = 0
            'End If
            'If dtKBClass.Rows.Count > 1 Then
            '    intTotal2 = dtKBClass.Rows(dtKBClass.Rows.Count - 1).Item(c_TotalPayColumnName)
            '    intTotal3 = dtKBClass.Rows(dtKBClass.Rows.Count - 1).Item(c_TotalBackColumnName)
            'Else
            '    intTotal2 = 0
            '    intTotal3 = 0
            'End If
            ''txtboxTotalAmt.Text = Format(intTotal + intTotal2, "$#,##0")
            ''txtboxTotalWithMBack.Text = Format(intTotal + intTotal2 - _
            ''                                        intTotal3, "$#,##0")
            'txtboxTotalAmt.Text = Format(intTotal, "$#,##0")
            'txtboxTotalWithMBack.Text = Format(intTotal - intTotal3, "$#,##0")
            'txtboxTotal1.Text = Format(intTotal, "$#,##0")
            'txtboxTotal2.Text = Format(intTotal3, "$#,##0")

            'dgvClassPay.Columns(c_TotalPayColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvClassPayDetails.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            ''dgvClassPayDetails.Columns(c_DiscountColumnName).DefaultCellStyle.Format = "c"

            'dgvClassKB.Columns(c_TotalPayColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvClassKB.Columns(c_TotalBackColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvClassKB.Columns(c_TotalKeepColumnName).DefaultCellStyle.Format = "$#,##0"

            'dgvClassKBDetails.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            'dgvClassKBDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"

            'LoadColumnText()



            Dim dtP As DataTable = dtPayClass.Clone
            dtP.Columns("ClassName").ColumnName = "班級名稱"
            dtP.Columns("DataCnt").ColumnName = "總筆數"
            dtP.Columns("NewCnt").ColumnName = "新報名人數"
            dtP.Columns("TotalPay").ColumnName = "總計"

            'CSOL.Logger.LogMessage("frmPayStaToday7: " & Now.ToString())

            For Each row As DataRow In dtPayClass.Rows
                dtP.Rows.Add(row.ItemArray)
            Next

            'CSOL.Logger.LogMessage("frmPayStaToday8: " & Now.ToString())

            Dim dtKB As DataTable = dtKBClass.Clone
            dtKB.Columns("ClassName").ColumnName = "班級名稱"
            dtKB.Columns("DataCnt").ColumnName = "總筆數"
            dtKB.Columns("TotalBack").ColumnName = "退回總金額"
            dtKB.Columns("Totalpay").ColumnName = "總計"
            dtKB.Columns("TotalKeep").ColumnName = "保留總金額"

            'CSOL.Logger.LogMessage("frmPayStaToday9: " & Now.ToString())

            For Each row As DataRow In dtKBClass.Rows
                dtKB.Rows.Add(row.ItemArray)
            Next

            'CSOL.Logger.LogMessage("frmPayStaToday10: " & Now.ToString())

            dgvClassPay.DataSource = dtP
            dgvClassKB.DataSource = dtKB

            If dgvClassPay.RowCount > 0 Then
                dgvClassPay.CurrentCell = dgvClassPay.Rows.Item(dgvClassPay.RowCount - 1).Cells("班級名稱")
            End If
            If dgvClassKB.RowCount > 0 Then
                dgvClassKB.CurrentCell = dgvClassKB.Rows.Item(dgvClassKB.RowCount - 1).Cells("班級名稱")
            End If

            dgvClassPay.Columns.Item("ClassID").Visible = False
            dgvClassKB.Columns.Item("ClassID").Visible = False
            dgvClassKB.Columns.Item("總計").Visible = False
            If dtPayClass.Rows.Count > 1 Then
                intTotal = dtP.Rows(dtP.Rows.Count - 1).Item("總計")
            Else
                intTotal = 0
            End If
            dgvClassPay.Columns("總計").DefaultCellStyle.Format = "$#,##0"
            dgvClassKB.Columns("退回總金額").DefaultCellStyle.Format = "$#,##0"
            dgvClassKB.Columns("保留總金額").DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()

            'CSOL.Logger.LogMessage("frmPayStaToday10.1: " & Now.ToString())

            LoadColumnText2()

            'CSOL.Logger.LogMessage("frmPayStaToday10.2: " & Now.ToString())

            dgvClassPayDetails.DataSource = dtPay.DefaultView
            dgvClassPayDetails.Columns("IsNewReg").Visible = False
            dgvClassPayDetails.Columns("ClassID").Visible = False

            dgvClassKBDetails.DataSource = dtKBS.DefaultView
            dgvClassKBDetails.Columns("ClassID").Visible = False


            If dtKBClass.Rows.Count > 1 Then
                intTotal2 = dtKB.Rows(dtKB.Rows.Count - 1).Item("總計")
                intTotal3 = dtKB.Rows(dtKB.Rows.Count - 1).Item("退回總金額")
            Else
                intTotal2 = 0
                intTotal3 = 0
            End If

            txtboxTotalAmt.Text = Format(intTotal, "$#,##0")
            txtboxTotalWithMBack.Text = Format(intTotal - intTotal3, "$#,##0")
            txtboxTotal1.Text = Format(intTotal, "$#,##0")
            txtboxTotal2.Text = Format(intTotal3, "$#,##0")


            dgvClassPayDetails.Columns("繳費金額").DefaultCellStyle.Format = "$#,##0"

            'dgvClassKBDetails.Columns(c_PayAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvClassKBDetails.Columns("退費金額").DefaultCellStyle.Format = "$#,##0"

            'CSOL.Logger.LogMessage("frmPayStaToday11: " & Now.ToString())

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Public Sub ResetGridview()
        dgvClassPay.DataSource = Nothing
        dgvClassPayDetails.DataSource = Nothing
        dgvClassKB.DataSource = Nothing
        dgvClassKBDetails.DataSource = Nothing
        txtboxTotal1.Text = ""
        txtboxTotalAmt.Text = ""
        txtboxTotalWithMBack.Text = ""
        txtboxTotal2.Text = ""
    End Sub

    Private Shared Function ListPayStatisticsHelper(ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        'CSOL.Logger.LogMessage("1.1.2: " & Now.ToString())

        Dim ds As DataSet = GetPayStaByHelper(dateFrom, dateTo)

        'Dim dtPayStaByDate As DataTable = GetPayStaByDate(dateFrom, dateTo) '第一個Table

        'CSOL.Logger.LogMessage("1.1.3: " & Now.ToString())

        'dtPayStaByDate.TableName = c_PayStaByDateTableName
        'ds.Tables.Add(dtPayStaByDate)

        'Dim dtMkeepStaByDate As DataTable = GetMKeepStaByDate(dateFrom, dateTo) '第二個Table

        'CSOL.Logger.LogMessage("1.1.4: " & Now.ToString())

        'dtMkeepStaByDate.TableName = c_MKeepStaByDateTableName
        'ds.Tables.Add(dtMkeepStaByDate)

        'Dim dtMBackStaByDate As DataTable = GetMBackStaByDate(dateFrom, dateTo) '第三個Table

        'CSOL.Logger.LogMessage("1.1.5: " & Now.ToString())

        'dtMBackStaByDate.TableName = c_MBackStaByDateTableName
        'ds.Tables.Add(dtMBackStaByDate)
        Return ds
    End Function


    Private Shared Function ListPayStatistics(ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet

        'CSOL.Logger.LogMessage("1.1.2: " & Now.ToString())

        Dim ds As New DataSet
        Dim dtPayStaByDate As DataTable = GetPayStaByDate(dateFrom, dateTo) '第一個Table

        'CSOL.Logger.LogMessage("1.1.3: " & Now.ToString())

        dtPayStaByDate.TableName = c_PayStaByDateTableName
        ds.Tables.Add(dtPayStaByDate)

        Dim dtMkeepStaByDate As DataTable = GetMKeepStaByDate(dateFrom, dateTo) '第二個Table

        'CSOL.Logger.LogMessage("1.1.4: " & Now.ToString())

        dtMkeepStaByDate.TableName = c_MKeepStaByDateTableName
        ds.Tables.Add(dtMkeepStaByDate)

        Dim dtMBackStaByDate As DataTable = GetMBackStaByDate(dateFrom, dateTo) '第三個Table

        'CSOL.Logger.LogMessage("1.1.5: " & Now.ToString())

        dtMBackStaByDate.TableName = c_MBackStaByDateTableName
        ds.Tables.Add(dtMBackStaByDate)
        Return ds
    End Function

    Private Shared Function GetPayStaByHelper(ByVal dateFrom As Date, ByVal dateTo As Date) As DataSet
        Dim dt As New DataTable
        Dim ds As New DataSet
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection

        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)

        'Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaToday", "GetPayStaByHelper", RequestParams)
        Dim ResponseData() As Byte = objCsol.GetPayStaByHelper(dateFrom, dateTo)

        'CSOL.Logger.LogMessage("1.1.3: " & Now.ToString())

        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetPayStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetPayStaByDate"))
            dt.TableName = c_PayStaByDateTableName
            ds.Tables.Add(dt)

            Using ms As New System.IO.MemoryStream
                zip("GetMKeepStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetMKeepStaByDate"))
            dt.TableName = c_MKeepStaByDateTableName
            ds.Tables.Add(dt)

            Using ms As New System.IO.MemoryStream
                zip("GetMBackStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetMBackStaByDate"))
            dt.TableName = c_MBackStaByDateTableName
            ds.Tables.Add(dt)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return ds
    End Function


    'GetPayStaByDate
    Private Shared Function GetPayStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection


        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaToday", "GetPayStaByDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetPayStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetPayStaByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function
    'GetMKeepStaByDate
    Private Shared Function GetMKeepStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection


        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaToday", "GetMKeepStaByDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetMKeepStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetMKeepStaByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    'GetMKeepStaByDate
    Private Shared Function GetMBackStaByDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim dt As New DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection


        RequestParams.Add("dateFrom", dateFrom)
        RequestParams.Add("dateTo", dateTo)

        Dim ResponseData() As Byte = CSOL.HTTPClient.Post("PayStaToday", "GetMBackStaByDate", RequestParams)
        Dim zip As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(ResponseData)
        Dim xmlstring As String = ""
        Try
            Using ms As New System.IO.MemoryStream
                zip("GetMBackStaByDate").Extract(ms)
                ms.Flush()
                ms.Position = 0
                Dim sr As New System.IO.StreamReader(ms)
                xmlstring = sr.ReadToEnd()
            End Using
            dt = CSOL.Convert.XmlStringToDataTable(CSOL.HTTPClient.ParseQuery(xmlstring).Item("GetMBackStaByDate"))
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex)
        Finally
        End Try
        Return dt
    End Function

    Private Sub InitTable()
        dtPayClass.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtPayClass.Columns.Add(c_NewCntColumnName, GetType(System.Int32))
        dtPayClass.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))

        dtKBClass.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtKBClass.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))
        dtKBClass.Columns.Add(c_TotalBackColumnName, GetType(System.Int32))
        dtKBClass.Columns.Add(c_TotalKeepColumnName, GetType(System.Int32))
    End Sub

    Private Sub AddIsNewReg()
        Dim i As Integer = 0
        Dim blIsNew As Boolean

        dtPaySta.Columns.Add(c_IsNewRegColumnName, GetType(System.Boolean))
        For i = 0 To dtPaySta.Rows.Count - 1
            blIsNew = IsNew(dtPaySta.Rows(i).Item(c_RegDateColumnName))
            dtPaySta.Rows(i).Item(c_IsNewRegColumnName) = blIsNew
        Next
    End Sub

    Private Function IsNew(ByVal regDate As Date) As Boolean
        If regDate >= dateFrom Then
            IsNew = True
        Else
            IsNew = False
        End If
    End Function
    Private Sub LoadColumnText2()
        dtKBS = dtKBSta.Clone
        dtKBS.Columns("Receiptnum").ColumnName = "收據編號"
        dtKBS.Columns("stuid").ColumnName = "學號"
        dtKBS.Columns("stuname").ColumnName = "學生姓名"
        dtKBS.Columns("subclassname").ColumnName = "班別名稱"
        dtKBS.Columns("datetime").ColumnName = "日期時間"
        dtKBS.Columns("paymethod").ColumnName = "繳費方式"
        dtKBS.Columns("handler").ColumnName = "承辦人員"
        dtKBS.Columns("remarks").ColumnName = "備註"
        dtKBS.Columns("amount").ColumnName = "退費金額"
        dtKBS.Columns("engname").ColumnName = "英文姓名"
        dtKBS.Columns("tel1").ColumnName = "電話一"
        dtKBS.Columns("tel2").ColumnName = "電話二"
        dtKBS.Columns("email").ColumnName = "電郵"
        dtKBS.Columns("extra").ColumnName = "額外資訊"
        dtKBS.Columns("school").ColumnName = "學校"
        dtKBS.Columns("dadname").ColumnName = "爸爸姓名"
        dtKBS.Columns("mumname").ColumnName = "媽媽姓名"
        dtKBS.Columns("dadmobile").ColumnName = "爸爸手機"
        dtKBS.Columns("mummobile").ColumnName = "媽媽手機"
        dtKBS.Columns("introid").ColumnName = "介紹人學號"
        dtKBS.Columns("introname").ColumnName = "介紹人姓名"
        dtKBS.Columns("schoolgrade").ColumnName = "學校年級"
        dtKBS.Columns("schoolclass").ColumnName = "學校班級"
        For Each row As DataRow In dtKBSta.Rows
            dtKBS.Rows.Add(row.ItemArray)
        Next

        Dim id As Integer
        Select Case intColSchType
            Case 0
                For index As Integer = 0 To dtKBS.Rows.Count - 1
                    id = GetIntValue(dtKBS.Rows(index).Item(c_PrimarySchColumnName))
                    dtKBS.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 1
                For index As Integer = 0 To dtKBS.Rows.Count - 1
                    id = GetIntValue(dtKBS.Rows(index).Item(c_JuniorSchColumnName))
                    dtKBS.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 2
                For index As Integer = 0 To dtKBS.Rows.Count - 1
                    id = GetIntValue(dtKBS.Rows(index).Item(c_HighSchColumnName))
                    dtKBS.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 3
                For index As Integer = 0 To dtKBS.Rows.Count - 1
                    id = GetIntValue(dtKBS.Rows(index).Item(c_UniversityColumnName))
                    dtKBS.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
        End Select
        'dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("classid"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("classname"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("subclassid"))
        'dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("mobile"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("birthday"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("graduatefrom"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("ic"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("registerdate"))
        'dt.Columns.RemoveAt(dt.Columns.IndexOf("schoolclass"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("primarysch"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("juniorsch"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("highsch"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("university"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("payamount"))
        dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf("paydatetime"))

        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow.Item(index).ToString = 1 Then

            Else
                dtKBS.Columns.RemoveAt(dtKBS.Columns.IndexOf(lstColName.Item(index).ToString))
            End If
        Next

       
    End Sub
    Private Sub LoadColumnText()

        'For Each col In dgvClassPay.Columns
        '    col.visible = False
        'Next
        'If dgvClassPay.Rows.Count > 0 Then

        '    dgvClassPay.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
        '    dgvClassPay.Columns.Item(c_ClassNameColumnName).Visible = True
        '    dgvClassPay.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
        '    dgvClassPay.Columns.Item(c_DataCntColumnName).DisplayIndex = 1
        '    dgvClassPay.Columns.Item(c_DataCntColumnName).Visible = True
        '    dgvClassPay.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
        '    dgvClassPay.Columns.Item(c_NewCntColumnName).DisplayIndex = 2
        '    dgvClassPay.Columns.Item(c_NewCntColumnName).Visible = True
        '    dgvClassPay.Columns.Item(c_NewCntColumnName).HeaderText = My.Resources.newCnt
        '    dgvClassPay.Columns.Item(c_TotalPayColumnName).DisplayIndex = 3
        '    dgvClassPay.Columns.Item(c_TotalPayColumnName).Visible = True
        '    dgvClassPay.Columns.Item(c_TotalPayColumnName).HeaderText = My.Resources.totalAmt
        'End If
        'For Each col In dgvClassPayDetails.Columns
        '    col.visible = False
        'Next
        'If dgvClassPayDetails.Rows.Count > 0 Then

        '    dgvClassPayDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
        '    dgvClassPayDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
        '    dgvClassPayDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
        '    dgvClassPayDetails.Columns.Item(c_StuIDColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
        '    dgvClassPayDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
        '    dgvClassPayDetails.Columns.Item(c_StuNameColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
        '    dgvClassPayDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
        '    dgvClassPayDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
        '    dgvClassPayDetails.Columns.Item(c_SchoolGradeColumnName).DisplayIndex = 4
        '    dgvClassPayDetails.Columns.Item(c_SchoolGradeColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_SchoolGradeColumnName).HeaderText = My.Resources.schoolGrade
        '    dgvClassPayDetails.Columns.Item(c_PayDateTimeColumnName).DisplayIndex = 5
        '    dgvClassPayDetails.Columns.Item(c_PayDateTimeColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_PayDateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvClassPayDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 6
        '    dgvClassPayDetails.Columns.Item(c_PayMethodColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
        '    dgvClassPayDetails.Columns.Item(c_PayAmountColumnName).DisplayIndex = 7
        '    dgvClassPayDetails.Columns.Item(c_PayAmountColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_PayAmountColumnName).HeaderText = My.Resources.payAmount
        '    dgvClassPayDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 8
        '    dgvClassPayDetails.Columns.Item(c_HandlerColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        '    dgvClassPayDetails.Columns.Item(c_SalesPersonColumnName).DisplayIndex = 9
        '    dgvClassPayDetails.Columns.Item(c_SalesPersonColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_SalesPersonColumnName).HeaderText = My.Resources.salesPerson
        '    dgvClassPayDetails.Columns.Item(c_ExtraColumnName).DisplayIndex = 10
        '    dgvClassPayDetails.Columns.Item(c_ExtraColumnName).Visible = True
        '    dgvClassPayDetails.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra
        '    'dgvClassPayDetails.Columns.Item(c_DiscountColumnName).DisplayIndex = 11
        '    'dgvClassPayDetails.Columns.Item(c_DiscountColumnName).Visible = True
        '    'dgvClassPayDetails.Columns.Item(c_DiscountColumnName).HeaderText = My.Resources.discount
        '    'dgvClassPayDetails.Columns.Item(c_DisRemarksColumnName).DisplayIndex = 12
        '    'dgvClassPayDetails.Columns.Item(c_DisRemarksColumnName).Visible = True
        '    'dgvClassPayDetails.Columns.Item(c_DisRemarksColumnName).HeaderText = My.Resources.discRemarks
        'End If

        'Dim i As Integer = 13
        'For index As Integer = 0 To lstColName.Count - 1
        '    If lstColShow(index) = 1 Then
        '        dgvClassPayDetails.Columns.Item(lstColName(index)).DisplayIndex = i
        '        dgvClassPayDetails.Columns.Item(lstColName(index)).Visible = True
        '        dgvClassPayDetails.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
        '        i = i + 1
        '    End If
        'Next
        'If dgvClassPayDetails.Columns.Item(c_SchoolColumnName).Visible = True Then
        '    Dim id As Integer
        '    Select Case intColSchType
        '        Case 0
        '            For index As Integer = 0 To dgvClassPayDetails.Rows.Count - 1
        '                id = dgvClassPayDetails.Rows(index).Cells(c_PrimarySchColumnName).Value
        '                dgvClassPayDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 1
        '            For index As Integer = 0 To dgvClassPayDetails.Rows.Count - 1
        '                id = dgvClassPayDetails.Rows(index).Cells(c_JuniorSchColumnName).Value
        '                dgvClassPayDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 2
        '            For index As Integer = 0 To dgvClassPayDetails.Rows.Count - 1
        '                id = dgvClassPayDetails.Rows(index).Cells(c_HighSchColumnName).Value
        '                dgvClassPayDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 3
        '            For index As Integer = 0 To dgvClassPayDetails.Rows.Count - 1
        '                id = dgvClassPayDetails.Rows(index).Cells(c_UniversityColumnName).Value
        '                dgvClassPayDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '    End Select
        'End If
        'For Each col In dgvClassKB.Columns
        '    col.visible = False
        'Next
        'If dgvClassKB.Rows.Count > 0 Then

        '    dgvClassKB.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
        '    dgvClassKB.Columns.Item(c_ClassNameColumnName).Visible = True
        '    dgvClassKB.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
        '    dgvClassKB.Columns.Item(c_DataCntColumnName).DisplayIndex = 1
        '    dgvClassKB.Columns.Item(c_DataCntColumnName).Visible = True
        '    dgvClassKB.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
        '    'dgvClassKB.Columns.Item(c_TotalPayColumnName).DisplayIndex = 2
        '    'dgvClassKB.Columns.Item(c_TotalPayColumnName).Visible = True
        '    'dgvClassKB.Columns.Item(c_TotalPayColumnName).HeaderText = My.Resources.totalPay
        '    dgvClassKB.Columns.Item(c_TotalBackColumnName).DisplayIndex = 3
        '    dgvClassKB.Columns.Item(c_TotalBackColumnName).Visible = True
        '    dgvClassKB.Columns.Item(c_TotalBackColumnName).HeaderText = My.Resources.totalBack
        '    dgvClassKB.Columns.Item(c_TotalKeepColumnName).DisplayIndex = 4
        '    dgvClassKB.Columns.Item(c_TotalKeepColumnName).Visible = True
        '    dgvClassKB.Columns.Item(c_TotalKeepColumnName).HeaderText = My.Resources.totalKeep
        'End If
        'For Each col In dgvClassKBDetails.Columns
        '    col.visible = False
        'Next
        'If dgvClassKBDetails.Rows.Count > 0 Then

        '    dgvClassKBDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 0
        '    dgvClassKBDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
        '    dgvClassKBDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 1
        '    dgvClassKBDetails.Columns.Item(c_StuIDColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
        '    dgvClassKBDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 2
        '    dgvClassKBDetails.Columns.Item(c_StuNameColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
        '    dgvClassKBDetails.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 3
        '    dgvClassKBDetails.Columns.Item(c_SubClassNameColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
        '    dgvClassKBDetails.Columns.Item(c_SchoolColumnName).DisplayIndex = 4
        '    dgvClassKBDetails.Columns.Item(c_SchoolColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_SchoolColumnName).HeaderText = My.Resources.school
        '    dgvClassKBDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 5
        '    dgvClassKBDetails.Columns.Item(c_DateTimeColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
        '    dgvClassKBDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 6
        '    dgvClassKBDetails.Columns.Item(c_PayMethodColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
        '    dgvClassKBDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 7
        '    dgvClassKBDetails.Columns.Item(c_AmountColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_AmountColumnName).HeaderText = "退費金額"
        '    dgvClassKBDetails.Columns.Item(c_ExtraColumnName).DisplayIndex = 8
        '    dgvClassKBDetails.Columns.Item(c_ExtraColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra
        '    dgvClassKBDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 9
        '    dgvClassKBDetails.Columns.Item(c_HandlerColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.dealer
        '    dgvClassKBDetails.Columns.Item(c_RemarksColumnName).DisplayIndex = 10
        '    dgvClassKBDetails.Columns.Item(c_RemarksColumnName).Visible = True
        '    dgvClassKBDetails.Columns.Item(c_RemarksColumnName).HeaderText = My.Resources.remarks
        'End If
        'i = 11
        'For index As Integer = 0 To lstColName.Count - 1
        '    If lstColShow(index) = 1 Then
        '        dgvClassKBDetails.Columns.Item(lstColName(index)).DisplayIndex = i
        '        dgvClassKBDetails.Columns.Item(lstColName(index)).Visible = True
        '        dgvClassKBDetails.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
        '        i = i + 1
        '    End If
        'Next
        'If dgvClassKBDetails.Columns.Item(c_SchoolColumnName).Visible = True Then
        '    Dim id As Integer
        '    Select Case intColSchType
        '        Case 0
        '            For index As Integer = 0 To dgvClassKBDetails.Rows.Count - 1
        '                id = dgvClassKBDetails.Rows(index).Cells(c_PrimarySchColumnName).Value
        '                dgvClassKBDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 1
        '            For index As Integer = 0 To dgvClassKBDetails.Rows.Count - 1
        '                id = dgvClassKBDetails.Rows(index).Cells(c_JuniorSchColumnName).Value
        '                dgvClassKBDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 2
        '            For index As Integer = 0 To dgvClassKBDetails.Rows.Count - 1
        '                id = dgvClassKBDetails.Rows(index).Cells(c_HighSchColumnName).Value
        '                dgvClassKBDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '        Case 3
        '            For index As Integer = 0 To dgvClassKBDetails.Rows.Count - 1
        '                id = dgvClassKBDetails.Rows(index).Cells(c_UniversityColumnName).Value
        '                dgvClassKBDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
        '            Next
        '    End Select
        'End If
        dtPay = dtPaySta1.Clone
        dtPay.Columns("Receiptnum").ColumnName = "收據編號"
        dtPay.Columns("stuid").ColumnName = "學號"
        dtPay.Columns("stuname").ColumnName = "學生姓名"
        dtPay.Columns("subclassname").ColumnName = "班別名稱"
        dtPay.Columns("paydatetime").ColumnName = "日期時間"
        dtPay.Columns("paymethod").ColumnName = "繳費方式"
        dtPay.Columns("payamount").ColumnName = "繳費金額"
        dtPay.Columns("handler").ColumnName = "承辦人員"
        dtPay.Columns("salesperson").ColumnName = "業務人員"
        dtPay.Columns("engname").ColumnName = "英文姓名"
        dtPay.Columns("tel1").ColumnName = "電話一"
        dtPay.Columns("tel2").ColumnName = "電話二"
        dtPay.Columns("email").ColumnName = "電郵"
        dtPay.Columns("extra").ColumnName = "額外資訊"
        dtPay.Columns("school").ColumnName = "學校"
        dtPay.Columns("dadname").ColumnName = "爸爸姓名"
        dtPay.Columns("mumname").ColumnName = "媽媽姓名"
        dtPay.Columns("dadmobile").ColumnName = "爸爸手機"
        dtPay.Columns("mummobile").ColumnName = "媽媽手機"
        dtPay.Columns("introid").ColumnName = "介紹人學號"
        dtPay.Columns("introname").ColumnName = "介紹人姓名"
        dtPay.Columns("schoolgrade").ColumnName = "學校年級"
        dtPay.Columns("schoolclass").ColumnName = "學校班級"
        For Each row As DataRow In dtPaySta1.Rows
            dtPay.Rows.Add(row.ItemArray)
        Next
        'CSOL.Logger.LogMessage("10.1.1: " & Now.ToString())

        Dim id As Integer
        Select Case intColSchType
            Case 0
                For index As Integer = 0 To dtPay.Rows.Count - 1
                    id = GetIntValue(dtPay.Rows(index).Item(c_PrimarySchColumnName))
                    dtPay.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 1
                For index As Integer = 0 To dtPay.Rows.Count - 1
                    id = GetIntValue(dtPay.Rows(index).Item(c_JuniorSchColumnName))
                    dtPay.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 2
                For index As Integer = 0 To dtPay.Rows.Count - 1
                    id = GetIntValue(dtPay.Rows(index).Item(c_HighSchColumnName))
                    dtPay.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
            Case 3
                For index As Integer = 0 To dtPay.Rows.Count - 1
                    id = GetIntValue(dtPay.Rows(index).Item(c_UniversityColumnName))
                    dtPay.Rows(index).Item("學校") = frmMain.GetSchName(id)
                Next
        End Select

        'CSOL.Logger.LogMessage("10.1.2: " & Now.ToString())

        'dtP.Columns.RemoveAt(dtP.Columns.IndexOf("classid"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("classname"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("subclassid"))
        'dt.Columns.RemoveAt(dt.Columns.IndexOf("seatnum"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("mobile"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("birthday"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("graduatefrom"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("ic"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("registerdate"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("remarks"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("primarysch"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("juniorsch"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("highsch"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("university"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("amount"))
        dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf("datetime"))

        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow.Item(index).ToString = 1 Then

            Else
                dtPay.Columns.RemoveAt(dtPay.Columns.IndexOf(lstColName.Item(index).ToString))
            End If
        Next

        'CSOL.Logger.LogMessage("10.1.3: " & Now.ToString())

        If radbutAllStu.Checked <> True Then
            dtPay = dtPay.Select(String.Format("{0} = {1}", c_IsNewRegColumnName, True.ToString)).CopyToDataTable
            'dtPay.Rows.Clear()

            'Dim dt As DataTable = dtPay.Clone
            'For Each row As DataRow In rows
            '    dtPay.Rows.Add(row.ItemArray)
            'Next
            'dtPay.Rows.Clear()

        End If

        'CSOL.Logger.LogMessage("10.1.4: " & Now.ToString())


    End Sub

    Private Sub InitColSelections()
        'lstColName.Add(c_EngNameColumnName)
        'lstColName.Add(c_Tel1ColumnName)
        'lstColName.Add(c_Tel2ColumnName)
        'lstColName.Add(c_EmailColumnName)
        'lstColName.Add(c_SchoolColumnName)
        'lstColName.Add(c_GraduateFromColumnName)
        'lstColName.Add(c_DadNameColumnName)
        'lstColName.Add(c_MumNameColumnName)
        'lstColName.Add(c_DadMobileColumnName)
        'lstColName.Add(c_MumMobileColumnName)
        'lstColName.Add(c_IntroIDColumnName)
        'lstColName.Add(c_IntroNameColumnName)
        'lstColName.Add(c_SchoolGradeColumnName)
        'lstColName.Add(c_SchoolClassColumnName)
        'FillArrayList(2, lstColName.Count, lstColShow)
        'FillArrayList(1, lstColName.Count, lstColTxt)
        lstColName.Add("英文姓名")
        lstColName.Add("電話一")
        lstColName.Add("電話二")
        lstColName.Add("電郵")
        lstColName.Add("學校")
        'lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add("爸爸姓名")
        lstColName.Add("媽媽姓名")
        lstColName.Add("爸爸手機")
        lstColName.Add("媽媽手機")
        lstColName.Add("介紹人學號")
        lstColName.Add("介紹人姓名")
        lstColName.Add("學校年級")
        lstColName.Add("學校班級")
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub dgvClassPay_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClassPay.SelectionChanged
        'Dim intClassId As Integer
        'If dgvClassPay.SelectedRows.Count > 0 Then
        '    intClassId = dgvClassPay.SelectedRows(0).Cells(c_ClassIDColumnName).Value
        '    If radbutAllStu.Checked = True Then
        '        If intClassId > -1 Then
        '            dtPaySta1.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString
        '            dgvClassPayDetails.DataSource = dtPaySta1.DefaultView
        '        Else
        '            dtPaySta1.DefaultView.RowFilter = ""
        '            dgvClassPayDetails.DataSource = dtPaySta1.DefaultView
        '        End If
        '    Else
        '        If intClassId > -1 Then
        '            dtPaySta1.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString + _
        '                   " AND " + c_IsNewRegColumnName + "=" + True.ToString
        '            dgvClassPayDetails.DataSource = dtPaySta1.DefaultView
        '        Else
        '            dtPaySta1.DefaultView.RowFilter = c_IsNewRegColumnName + "=" + True.ToString
        '            dgvClassPayDetails.DataSource = dtPaySta1.DefaultView
        '        End If
        '    End If
        'End If
        Dim intClassId As Integer
        If dgvClassPay.SelectedRows.Count > 0 And dtPay.Rows.Count > 0 Then
            intClassId = dgvClassPay.SelectedRows(0).Cells(c_ClassIDColumnName).Value
            If radbutAllStu.Checked = True Then
                If intClassId > -1 Then
                    'LoadColumnText()
                    dtPay.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString
                Else
                    dtPay.DefaultView.RowFilter = ""
                End If
            Else
                If intClassId > -1 Then
                    'LoadColumnText()
                    dtPay.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString + " AND " + c_IsNewRegColumnName + "=" + True.ToString
                Else
                    'LoadColumnText()
                    dtPay.DefaultView.RowFilter = c_IsNewRegColumnName + "=" + True.ToString
                End If
            End If
            dgvClassPayDetails.DataSource = dtPay.DefaultView
            'dgvClassPayDetails.Columns("IsNewReg").Visible = False
            dgvClassPayDetails.Columns("ClassID").Visible = False
        End If
    End Sub

    Private Sub dgvClassKB_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvClassKB.SelectionChanged
        Dim intClassId As Integer
        If dgvClassKB.SelectedRows.Count > 0 And dtKBS.Rows.Count > 0 Then
            intClassId = dgvClassKB.SelectedRows(0).Cells(c_ClassIDColumnName).Value
            If intClassId > -1 Then
                dtKBS.DefaultView.RowFilter = c_ClassIDColumnName + "=" + intClassId.ToString
            Else
                dtKBS.DefaultView.RowFilter = ""
            End If
            dgvClassKBDetails.DataSource = dtKBS.DefaultView
            dgvClassKBDetails.Columns("ClassID").Visible = False
        End If
    End Sub

    Private Sub butListSta_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butListSta.Click
        InitSelections()
        RefreshData()
    End Sub

    Private Sub InitSelections()
        'radbutAllStu.Checked = True
        SetPayMethodFilter(0)
    End Sub

    Friend Sub SetPayMethodFilter(ByVal payMethod As Integer)
        intPayMethod = payMethod
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmPayStaToday_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

    End Sub

    Private Sub butSelectCol1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelectCol1.Click
        Dim lst As New ArrayList
        For index As Integer = 13 To dgvClassPayDetails.Columns.Count - 1
            If dgvClassPayDetails.Columns(index).Visible = True Then
                lst.Add(1)
            Else
                lst.Add(0)
            End If
        Next
        Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            lstColShow = frmMain.GetCurrentList
            lstColTxt = frmMain.GetCurrentList2
            intColSchType = frmMain.GetCurrentValue
            LoadColumnText()
            dgvClassPayDetails.DataSource = dtPay.DefaultView
            dgvClassPayDetails.Columns("IsNewReg").Visible = False
            dgvClassPayDetails.Columns("ClassID").Visible = False

        End If
    End Sub

    Private Sub butSelectCol2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelectCol2.Click
        Dim lst As New ArrayList
        For index As Integer = 12 To dgvClassKBDetails.Columns.Count - 1
            If dgvClassKBDetails.Columns(index).Visible = True Then
                lst.Add(1)
            Else
                lst.Add(0)
            End If
        Next
        Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
        frm.ShowDialog()
        If frmMain.GetOkState Then
            lstColShow = frmMain.GetCurrentList
            lstColTxt = frmMain.GetCurrentList2
            intColSchType = frmMain.GetCurrentValue
            LoadColumnText2()
            dgvClassKBDetails.DataSource = dtKBS.DefaultView
            dgvClassKBDetails.Columns("ClassID").Visible = False
        End If
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument1.DefaultPageSettings.Landscape = True
            PrintDocument1.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvClassPayDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        '  If nPageNo = 1 Then

        For Each oColumn As DataGridViewColumn In dgvClassPayDetails.Columns
            If oColumn.Visible = True Then
                nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                oColumnLefts.Add(nLeft)
                oColumnWidths.Add(nWidth)
                oColumnTypes.Add(oColumn.GetType)
                nLeft += nWidth
            End If
        Next

        ' End If

        Do While nRowPos < dgvClassPayDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvClassPayDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvClassPayDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvClassPayDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvClassPayDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop


        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        If flag = 1 Then
            strPerPages = Math.Ceiling(dgvClassPayDetails.Rows.Count / RowsPerPage).ToString()
            flag = 2
        End If
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + strPerPages
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvClassPayDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvClassPayDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvClassPayDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvClassPayDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvClassPayDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        If frmMain.CheckAuth(7) Then
            PrintDocument2.DefaultPageSettings.Landscape = True
            PrintDocument2.Print()
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvClassPay)
            ExportDgvToExcel(dgvClassKB)
        Else
            frmMain.ShowNoAuthMsg()
        End If
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
    

        If frmMain.CheckAuth(16) Then
            ExportDgvToExcel(dgvClassPayDetails)
            If dgvClassKBDetails.RowCount > 0 Then
                ExportDgvToExcel(dgvClassKBDetails)
            End If
        Else
            frmMain.ShowNoAuthMsg()
        End If
        
    End Sub

    Private Sub mnuSetPrintSize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuSetPrintSize.Click
        Dim frm As New frm2SetPrintSize
        frm.ShowDialog()
        If frmMain.GetOkState Then
            intSize = GetPrintSize()
            dgvClassPay.ColumnHeadersDefaultCellStyle.Font = _
                New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassPay.DefaultCellStyle.Font = New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassPayDetails.ColumnHeadersDefaultCellStyle.Font = _
                New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassPayDetails.DefaultCellStyle.Font = New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassKB.ColumnHeadersDefaultCellStyle.Font = _
                New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassKB.DefaultCellStyle.Font = New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassKBDetails.ColumnHeadersDefaultCellStyle.Font = _
                New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)
            dgvClassKBDetails.DefaultCellStyle.Font = New Font("新細明體", intSize, FontStyle.Regular, GraphicsUnit.Pixel)

            frmMain.SetOkState(False)
        End If
    End Sub

    Private Sub butByMethod1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butByMethod1.Click

    End Sub

    Private Sub butByMethod2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butByMethod2.Click

    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

End Class