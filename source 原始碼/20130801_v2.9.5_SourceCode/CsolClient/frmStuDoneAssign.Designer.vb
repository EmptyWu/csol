﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuDoneAssign
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuExport = New System.Windows.Forms.ToolStripMenuItem
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.mnuSearch = New System.Windows.Forms.ToolStripMenuItem
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuStuInfo = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDetele = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuSelectCol = New System.Windows.Forms.ToolStripMenuItem
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxAssignment = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.chkboxShowPastAssign = New System.Windows.Forms.CheckBox
        Me.tboxIndex = New System.Windows.Forms.TextBox
        Me.dgv = New System.Windows.Forms.DataGridView
        Me.mnustrTop.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        Me.mnuClose.Size = New System.Drawing.Size(44, 20)
        Me.mnuClose.Text = "關閉"
        '
        'mnuExport
        '
        Me.mnuExport.Name = "mnuExport"
        Me.mnuExport.Size = New System.Drawing.Size(68, 20)
        Me.mnuExport.Text = "匯出資料"
        '
        'PrintDocument1
        '
        '
        'mnuSearch
        '
        Me.mnuSearch.Name = "mnuSearch"
        Me.mnuSearch.Size = New System.Drawing.Size(68, 20)
        Me.mnuSearch.Text = "搜尋學生"
        '
        'chkboxShowPast
        '
        Me.chkboxShowPast.AutoSize = True
        Me.chkboxShowPast.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPast.Location = New System.Drawing.Point(262, 34)
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPast.TabIndex = 139
        Me.chkboxShowPast.Text = "顯示過時班級"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuStuInfo, Me.mnuSearch, Me.mnuDetele, Me.mnuPrint, Me.mnuSelectCol, Me.mnuExport, Me.mnuClose})
        Me.mnustrTop.Location = New System.Drawing.Point(0, 0)
        Me.mnustrTop.Name = "mnustrTop"
        Me.mnustrTop.Size = New System.Drawing.Size(1028, 24)
        Me.mnustrTop.TabIndex = 131
        Me.mnustrTop.Text = "MenuStrip1"
        '
        'mnuStuInfo
        '
        Me.mnuStuInfo.Name = "mnuStuInfo"
        Me.mnuStuInfo.Size = New System.Drawing.Size(68, 20)
        Me.mnuStuInfo.Text = "學生資料"
        '
        'mnuDetele
        '
        Me.mnuDetele.Name = "mnuDetele"
        Me.mnuDetele.Size = New System.Drawing.Size(68, 20)
        Me.mnuDetele.Text = "刪除紀錄"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        Me.mnuPrint.Size = New System.Drawing.Size(68, 20)
        Me.mnuPrint.Text = "列印資料"
        '
        'mnuSelectCol
        '
        Me.mnuSelectCol.Name = "mnuSelectCol"
        Me.mnuSelectCol.Size = New System.Drawing.Size(68, 20)
        Me.mnuSelectCol.Text = "選擇欄位"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(9, 36)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(35, 12)
        Me.Label1.TabIndex = 132
        Me.Label1.Text = "班級: "
        Me.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(355, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(35, 12)
        Me.Label2.TabIndex = 134
        Me.Label2.Text = "班別: "
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(605, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(35, 12)
        Me.Label3.TabIndex = 136
        Me.Label3.Text = "作業: "
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Location = New System.Drawing.Point(392, 31)
        Me.cboxSubClass.Name = "cboxSubClass"
        Me.cboxSubClass.Size = New System.Drawing.Size(212, 20)
        Me.cboxSubClass.TabIndex = 135
        '
        'cboxAssignment
        '
        Me.cboxAssignment.FormattingEnabled = True
        Me.cboxAssignment.Location = New System.Drawing.Point(640, 31)
        Me.cboxAssignment.Name = "cboxAssignment"
        Me.cboxAssignment.Size = New System.Drawing.Size(206, 20)
        Me.cboxAssignment.TabIndex = 137
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(44, 32)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(213, 20)
        Me.cboxClass.TabIndex = 133
        '
        'chkboxShowPastAssign
        '
        Me.chkboxShowPastAssign.AutoSize = True
        Me.chkboxShowPastAssign.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.chkboxShowPastAssign.Location = New System.Drawing.Point(849, 35)
        Me.chkboxShowPastAssign.Name = "chkboxShowPastAssign"
        Me.chkboxShowPastAssign.Size = New System.Drawing.Size(96, 16)
        Me.chkboxShowPastAssign.TabIndex = 149
        Me.chkboxShowPastAssign.Text = "顯示過時作業"
        Me.chkboxShowPastAssign.UseVisualStyleBackColor = True
        '
        'tboxIndex
        '
        Me.tboxIndex.BackColor = System.Drawing.Color.White
        Me.tboxIndex.Font = New System.Drawing.Font("Times New Roman", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tboxIndex.ForeColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(128, Byte), Integer))
        Me.tboxIndex.Location = New System.Drawing.Point(941, 30)
        Me.tboxIndex.Name = "tboxIndex"
        Me.tboxIndex.ReadOnly = True
        Me.tboxIndex.Size = New System.Drawing.Size(76, 22)
        Me.tboxIndex.TabIndex = 150
        Me.tboxIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Location = New System.Drawing.Point(12, 59)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        Me.dgv.RowTemplate.Height = 24
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(1004, 552)
        Me.dgv.TabIndex = 151
        '
        'frmStuDoneAssign
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1028, 623)
        Me.Controls.Add(Me.dgv)
        Me.Controls.Add(Me.tboxIndex)
        Me.Controls.Add(Me.chkboxShowPastAssign)
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxAssignment)
        Me.Controls.Add(Me.cboxClass)
        Me.Name = "frmStuDoneAssign"
        Me.Text = "通過作業的學生"
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuExport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents mnuSearch As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuStuInfo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDetele As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSelectCol As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxAssignment As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents chkboxShowPastAssign As System.Windows.Forms.CheckBox
    Friend WithEvents tboxIndex As System.Windows.Forms.TextBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
End Class
