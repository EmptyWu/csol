﻿Public Class frmStuMBSta
    Private dtClassList As New DataTable
    Private dateNow As Date = Now
    Private dtPaySta As New DataTable
    Private lstClass As New ArrayList
    Private lstClassShown As New ArrayList
    Private dtMaster As New DataTable
    Private dtDetails As New DataTable
    Private lstColName As New ArrayList
    Private lstColShow As New ArrayList
    Private lstColTxt As New ArrayList
    Private intColSchType As Integer = 1
    Private oStringFormat As StringFormat
    Private oStringFormatComboBox As StringFormat
    Private oButton As Button
    Private oCheckbox As CheckBox
    Private oComboBox As ComboBox
    Private nTotalWidth As Int16
    Private nRowPos As Int16
    Private NewPage As Boolean
    Private nPageNo As Int16
    Private Header As String = My.Resources.frmStuMBSta
    Private sUserName As String = frmMain.GetUsrName

    Public Sub New()
        InitializeComponent()

        Me.Text = My.Resources.frmStuMBSta
        SetDate()
        InitClassList()
        ShowClassList()
        'RefreshData()
        InitColSelections()
    End Sub

    Private Sub mnuExportAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportAll.Click
        ExportDgvToExcel(dgvMaster)
    End Sub

    Private Sub mnuExportDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuExportDetails.Click
        ExportDgvToExcel(dgvDetails)
    End Sub

    Private Sub mnuPrintAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintAll.Click
        PrintDocument1.DefaultPageSettings.Landscape = True
        PrintDocument1.Print()
    End Sub

    Private Sub PrintDocument2_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument2.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvDetails.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument2_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument2.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvDetails.Rows.Count

            Dim oRow As DataGridViewRow = dgvDetails.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter2(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvDetails.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvDetails.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvDetails.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter2(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter2(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvDetails.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvDetails.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvDetails.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvDetails.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub PrintDocument1_BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs) Handles PrintDocument1.BeginPrint
        oStringFormat = New StringFormat
        oStringFormat.Alignment = StringAlignment.Center
        oStringFormat.LineAlignment = StringAlignment.Center
        oStringFormat.Trimming = StringTrimming.EllipsisCharacter

        oStringFormatComboBox = New StringFormat
        oStringFormatComboBox.LineAlignment = StringAlignment.Center
        oStringFormatComboBox.FormatFlags = StringFormatFlags.NoWrap
        oStringFormatComboBox.Trimming = StringTrimming.EllipsisCharacter

        oButton = New Button
        oCheckbox = New CheckBox
        oComboBox = New ComboBox

        nTotalWidth = 0
        For Each oColumn As DataGridViewColumn In dgvMaster.Columns
            If oColumn.Visible = True Then
                nTotalWidth += oColumn.Width
            End If
        Next
        nPageNo = 1
        NewPage = True
        nRowPos = 0


    End Sub

    Private Sub PrintDocument1_PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs) Handles PrintDocument1.PrintPage
        Dim oColumnLefts As New ArrayList
        Dim oColumnWidths As New ArrayList
        Dim oColumnTypes As New ArrayList
        Dim nHeight As Int16

        Dim nWidth, i, nRowsPerPage As Int16
        Dim nTop As Int16 = e.MarginBounds.Top
        Dim nLeft As Int16 = e.MarginBounds.Left

        If nPageNo = 1 Then

            For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                If oColumn.Visible = True Then
                    nWidth = CType(Math.Floor(oColumn.Width / nTotalWidth * nTotalWidth * (e.MarginBounds.Width / nTotalWidth)), Int16)

                    nHeight = e.Graphics.MeasureString(oColumn.HeaderText, oColumn.InheritedStyle.Font, nWidth).Height + 11

                    oColumnLefts.Add(nLeft)
                    oColumnWidths.Add(nWidth)
                    oColumnTypes.Add(oColumn.GetType)
                    nLeft += nWidth
                End If
            Next

        End If

        Do While nRowPos < dgvMaster.Rows.Count

            Dim oRow As DataGridViewRow = dgvMaster.Rows(nRowPos)

            If nTop + nHeight >= e.MarginBounds.Height + e.MarginBounds.Top Then

                DrawFooter(e, nRowsPerPage)

                NewPage = True
                nPageNo += 1
                e.HasMorePages = True
                Exit Sub

            Else

                If NewPage Then

                    ' Draw Header
                    e.Graphics.DrawString(Header, New Font(dgvMaster.Font, FontStyle.Bold), Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top - e.Graphics.MeasureString(Header, New Font(dgvMaster.Font, FontStyle.Bold), e.MarginBounds.Width).Height - 13)

                    ' Draw Columns
                    nTop = e.MarginBounds.Top
                    i = 0
                    For Each oColumn As DataGridViewColumn In dgvMaster.Columns
                        If oColumn.Visible = True Then
                            e.Graphics.FillRectangle(New SolidBrush(Drawing.Color.LightGray), New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))
                            e.Graphics.DrawString(oColumn.HeaderText, oColumn.InheritedStyle.Font, New SolidBrush(oColumn.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)
                            i += 1
                        End If
                    Next
                    NewPage = False

                End If

                nTop += nHeight
                i = 0
                For Each oCell As DataGridViewCell In oRow.Cells
                    If oCell.Visible = True Then
                        If oColumnTypes(i) Is GetType(DataGridViewTextBoxColumn) OrElse oColumnTypes(i) Is GetType(DataGridViewLinkColumn) Then

                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i), nTop, oColumnWidths(i), nHeight), oStringFormat)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewButtonColumn) Then

                            oButton.Text = oCell.Value.ToString
                            oButton.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oButton.Width, oButton.Height)
                            oButton.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewCheckBoxColumn) Then

                            oCheckbox.Size = New Size(14, 14)
                            oCheckbox.Checked = CType(oCell.Value, Boolean)
                            Dim oBitmap As New Bitmap(oColumnWidths(i), nHeight)
                            Dim oTempGraphics As Graphics = Graphics.FromImage(oBitmap)
                            oTempGraphics.FillRectangle(Brushes.White, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            oCheckbox.DrawToBitmap(oBitmap, New Rectangle(CType((oBitmap.Width - oCheckbox.Width) / 2, Int32), CType((oBitmap.Height - oCheckbox.Height) / 2, Int32), oCheckbox.Width, oCheckbox.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewComboBoxColumn) Then

                            oComboBox.Size = New Size(oColumnWidths(i), nHeight)
                            Dim oBitmap As New Bitmap(oComboBox.Width, oComboBox.Height)
                            oComboBox.DrawToBitmap(oBitmap, New Rectangle(0, 0, oBitmap.Width, oBitmap.Height))
                            e.Graphics.DrawImage(oBitmap, New Point(oColumnLefts(i), nTop))
                            e.Graphics.DrawString(oCell.Value.ToString, oCell.InheritedStyle.Font, New SolidBrush(oCell.InheritedStyle.ForeColor), New RectangleF(oColumnLefts(i) + 1, nTop, oColumnWidths(i) - 16, nHeight), oStringFormatComboBox)

                        ElseIf oColumnTypes(i) Is GetType(DataGridViewImageColumn) Then

                            Dim oCellSize As Rectangle = New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight)
                            Dim oImageSize As Size = CType(oCell.Value, Image).Size
                            e.Graphics.DrawImage(oCell.Value, New Rectangle(oColumnLefts(i) + CType(((oCellSize.Width - oImageSize.Width) / 2), Int32), nTop + CType(((oCellSize.Height - oImageSize.Height) / 2), Int32), CType(oCell.Value, Image).Width, CType(oCell.Value, Image).Height))

                        End If

                        e.Graphics.DrawRectangle(Pens.Black, New Rectangle(oColumnLefts(i), nTop, oColumnWidths(i), nHeight))

                        i += 1
                    End If
                Next

            End If

            nRowPos += 1
            nRowsPerPage += 1

        Loop

        DrawFooter(e, nRowsPerPage)

        e.HasMorePages = False


    End Sub

    Private Sub DrawFooter(ByVal e As System.Drawing.Printing.PrintPageEventArgs, ByVal RowsPerPage As Int32)
        Dim sPageNo As String = ""
        If RowsPerPage = 0 Then
            sPageNo = nPageNo.ToString + " of 1"
        Else
            sPageNo = nPageNo.ToString + " of " + Math.Ceiling(dgvMaster.Rows.Count / RowsPerPage).ToString
        End If

        ' Right Align - User Name
        e.Graphics.DrawString(sUserName, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sUserName, dgvMaster.Font, e.MarginBounds.Width).Width), e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Left Align - Date/Time
        e.Graphics.DrawString(Now.ToLongDateString + " " + Now.ToShortTimeString, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left, e.MarginBounds.Top + e.MarginBounds.Height + 31)

        ' Center  - Page No. Info
        e.Graphics.DrawString(sPageNo, dgvMaster.Font, Brushes.Black, e.MarginBounds.Left + (e.MarginBounds.Width - e.Graphics.MeasureString(sPageNo, dgvMaster.Font, e.MarginBounds.Width).Width) / 2, e.MarginBounds.Top + e.MarginBounds.Height + 31)

    End Sub

    Private Sub mnuPrintDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuPrintDetails.Click
        PrintDocument2.DefaultPageSettings.Landscape = True
        PrintDocument2.Print()
    End Sub

    Friend Sub SetDate()
        dtpickFrom.Value = dateNow
        dtpickTo.Value = dateNow
    End Sub

    Private Sub frmStuMBSta_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmMain.CloseTab(Me.Tag)
    End Sub

    Private Sub frmStuMBSta_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
    End Sub

    Friend Sub RefreshData()
        Dim intSubClass As Integer
        Dim intTotal As Integer
        Dim intTotal2 As Integer
        Dim strCompute1 As String = "SUM(" + c_AmountColumnName + ")"
        Dim strCompute2 As String
        Dim strCompute3 As String = "COUNT(" + c_AmountColumnName + ")"
        Dim strCompute4 As String = "SUM(" + c_BackAmountColumnName + ")"
        Dim strCompute5 As String = "COUNT(" + c_BackAmountColumnName + ")"
     
        Try
            'CSOL.Logger.LogMessage("frmStuMBSta1: " & Now.ToString())
            dtPaySta = objCsol.ListStuMBByClass(lstClass, GetDateStart(dtpickFrom.Value), GetDateEnd(dtpickTo.Value))
            'CSOL.Logger.LogMessage("frmStuMBSta2: " & Now.ToString())

            dtMaster = New DataTable()
            dtDetails = New DataTable()
            dtDetails = dtPaySta.Clone()
            dtDetails.Merge(dtPaySta)
            
            tbox0.Text = "0"
            tbox1.Text = "0"
            tbox2.Text = "0"
            tbox5.Text = "0"
            dtMaster = dtPaySta.DefaultView.ToTable(True, c_SubClassIDColumnName, _
                                                    c_ClassNameColumnName, _
                                                    c_SubClassNameColumnName)
            'CSOL.Logger.LogMessage("frmStuMBSta3: " & Now.ToString())
            InitTable()
            'CSOL.Logger.LogMessage("frmStuMBSta4: " & Now.ToString())

            For i = 0 To dtMaster.Rows.Count - 1
                intSubClass = dtMaster.Rows(i).Item(c_SubClassIDColumnName)
                strCompute2 = c_SubClassIDColumnName + "=" + intSubClass.ToString
                intTotal = dtPaySta.Compute(strCompute3, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtPaySta.Compute(strCompute1, strCompute2)
                Else
                    intTotal2 = 0
                End If
                dtMaster.Rows(i).Item(c_DataCntColumnName) = intTotal
                dtMaster.Rows(i).Item(c_TotalPayColumnName) = intTotal2
                intTotal = dtPaySta.Compute(strCompute5, strCompute2)
                If intTotal > 0 Then
                    intTotal2 = dtPaySta.Compute(strCompute4, strCompute2)
                Else
                    intTotal2 = 0
                End If
                dtMaster.Rows(i).Item(c_TotalBackColumnName) = intTotal2
            Next
            'CSOL.Logger.LogMessage("frmStuMBSta5: " & Now.ToString())

            strCompute1 = "SUM(" + c_DataCntColumnName + ")"
            strCompute2 = "SUM(" + c_TotalPayColumnName + ")"
            strCompute3 = "SUM(" + c_TotalBackColumnName + ")"

            If dtMaster.Rows.Count > 0 Then
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, "", dtMaster.Compute(strCompute1, ""), _
                                    dtMaster.Compute(strCompute2, ""), dtMaster.Compute(strCompute3, ""))
            Else
                dtMaster.Rows.Add(-1, My.Resources.totalAmt, "", 0, 0, 0)
            End If

            dgvMaster.DataSource = dtMaster
            If dgvMaster.RowCount > 0 Then
                dgvMaster.CurrentCell = dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_ClassNameColumnName)
            End If
            dtDetails.DefaultView.RowFilter = ""
            dgvDetails.DataSource = dtDetails

            RefreshPayMethodSta(dtPaySta, c_BackAmountColumnName)
            'CSOL.Logger.LogMessage("frmStuMBSta6: " & Now.ToString())

            tbox6.Text = (dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_TotalPayColumnName).Value).ToString
            tboxTotalBack.Text = Format((dgvMaster.Rows(dgvMaster.RowCount - 1).Cells(c_TotalBackColumnName).Value), "$#,##0")

            tbox0.Text = Format(CInt(tbox0.Text), "$#,##0")
            tbox1.Text = Format(CInt(tbox1.Text), "$#,##0")
            tbox2.Text = Format(CInt(tbox2.Text), "$#,##0")
            tbox5.Text = Format(CInt(tbox5.Text), "$#,##0")
            tbox6.Text = Format(CInt(tbox6.Text), "$#,##0")
            tboxTotal.Text = tbox6.Text

            dgvMaster.Columns(c_TotalPayColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvMaster.Columns(c_TotalBackColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_AmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_GrantAmountColumnName).DefaultCellStyle.Format = "$#,##0"
            dgvDetails.Columns(c_BackAmountColumnName).DefaultCellStyle.Format = "$#,##0"

            LoadColumnText()
            'CSOL.Logger.LogMessage("frmStuMBSta7: " & Now.ToString())

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshPayMethodSta(ByRef dt As DataTable, ByVal colName As String)
        Dim strCompute1 = "SUM(" + colName + ")"
        Dim strCompute2 As String
        Dim strCompute3 = "COUNT(" + colName + ")"

        If dt.Rows.Count > 0 Then
            strCompute2 = c_PayMethodColumnName + "=1"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox0.Text = (CInt(tbox0.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=2"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox1.Text = (CInt(tbox1.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=3"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox2.Text = (CInt(tbox2.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
            strCompute2 = c_PayMethodColumnName + "=6"
            If dt.Compute(strCompute3, strCompute2) > 0 Then
                tbox5.Text = (CInt(tbox5.Text) + dt.Compute(strCompute1, strCompute2)).ToString
            End If
        End If
    End Sub

    Private Sub InitClassList()
        Try
            dtClassList = frmMain.GetClassInfoSet.Tables(c_ClassListDataTableName).DefaultView.ToTable(True, c_IDColumnName, _
                     c_NameColumnName, c_EndColumnName)

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ShowClassList()
        chklstClass.Items.Clear()
        lstClassShown = New ArrayList

        Try
            If chkboxShowPast.Checked = True Then
                If dtClassList.Rows.Count > 0 Then
                    lstClassShown = New ArrayList(dtClassList.Rows.Count)
                    FillArrayList(2, dtClassList.Rows.Count, lstClassShown)
                End If
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                    lstClassShown(index) = dtClassList.Rows(index).Item(c_IDColumnName)
                Next
            Else
                Dim dateClass As Date
                lstClassShown = New ArrayList
                For index As Integer = 0 To dtClassList.Rows.Count - 1
                    dateClass = dtClassList.Rows(index).Item(c_EndColumnName)
                    If dateClass >= Now.Date Then
                        chklstClass.Items.Add(dtClassList.Rows(index).Item(c_NameColumnName), False)
                        lstClassShown.Add(dtClassList.Rows(index).Item(c_IDColumnName))
                    End If

                Next
            End If

            If chklstClass.Items.Count > 0 Then
                'chklstClass.SetItemChecked(0, True)
                chklstClass.SelectedItem = chklstClass.Items(0)
            End If
            RefreshClassList()

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub RefreshClassList()
        Try
            lstClass = New ArrayList()
            For index As Integer = 0 To chklstClass.Items.Count - 1
                If chklstClass.GetItemChecked(index) = True Then
                    lstClass.Add(lstClassShown(index))
                End If
            Next

        Catch ex As ApplicationException
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub InitTable()
        dtMaster.Columns.Add(c_DataCntColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalPayColumnName, GetType(System.Int32))
        dtMaster.Columns.Add(c_TotalBackColumnName, GetType(System.Int32))

    End Sub

    Private Sub LoadColumnText()
        For Each col In dgvMaster.Columns
            col.visible = False
        Next
        If dgvMaster.Rows.Count > 0 Then

            dgvMaster.Columns.Item(c_ClassNameColumnName).DisplayIndex = 0
            dgvMaster.Columns.Item(c_ClassNameColumnName).Visible = True
            dgvMaster.Columns.Item(c_ClassNameColumnName).HeaderText = My.Resources.className
            dgvMaster.Columns.Item(c_SubClassNameColumnName).DisplayIndex = 1
            dgvMaster.Columns.Item(c_SubClassNameColumnName).Visible = True
            dgvMaster.Columns.Item(c_SubClassNameColumnName).HeaderText = My.Resources.subClassName
            dgvMaster.Columns.Item(c_DataCntColumnName).DisplayIndex = 2
            dgvMaster.Columns.Item(c_DataCntColumnName).Visible = True
            dgvMaster.Columns.Item(c_DataCntColumnName).HeaderText = My.Resources.totalCnt
            'dgvMaster.Columns.Item(c_TotalPayColumnName).DisplayIndex = 3
            'dgvMaster.Columns.Item(c_TotalPayColumnName).Visible = True
            'dgvMaster.Columns.Item(c_TotalPayColumnName).HeaderText = My.Resources.totalPay
            dgvMaster.Columns.Item(c_TotalBackColumnName).DisplayIndex = 4
            dgvMaster.Columns.Item(c_TotalBackColumnName).Visible = True
            dgvMaster.Columns.Item(c_TotalBackColumnName).HeaderText = My.Resources.totalBack
        End If
        For Each col In dgvDetails.Columns
            col.visible = False
        Next
        If dgvDetails.Rows.Count > 0 Then
            
            dgvDetails.Columns.Item(c_StuIDColumnName).DisplayIndex = 0
            dgvDetails.Columns.Item(c_StuIDColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuIDColumnName).HeaderText = My.Resources.stuID
            dgvDetails.Columns.Item(c_StuNameColumnName).DisplayIndex = 1
            dgvDetails.Columns.Item(c_StuNameColumnName).Visible = True
            dgvDetails.Columns.Item(c_StuNameColumnName).HeaderText = My.Resources.stuName
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).DisplayIndex = 2
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).Visible = True
            dgvDetails.Columns.Item(c_ReceiptNumColumnName).HeaderText = My.Resources.receiptNum
            dgvDetails.Columns.Item(c_BackAmountColumnName).DisplayIndex = 3
            dgvDetails.Columns.Item(c_BackAmountColumnName).Visible = True
            dgvDetails.Columns.Item(c_BackAmountColumnName).HeaderText = "退費金額"
            'dgvDetails.Columns.Item(c_AmountColumnName).DisplayIndex = 3
            'dgvDetails.Columns.Item(c_AmountColumnName).Visible = True
            'dgvDetails.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
            dgvDetails.Columns.Item(c_DateTimeColumnName).DisplayIndex = 4
            dgvDetails.Columns.Item(c_DateTimeColumnName).Visible = True
            dgvDetails.Columns.Item(c_DateTimeColumnName).HeaderText = My.Resources.datetime
            dgvDetails.Columns.Item(c_PayMethodColumnName).DisplayIndex = 5
            dgvDetails.Columns.Item(c_PayMethodColumnName).Visible = True
            dgvDetails.Columns.Item(c_PayMethodColumnName).HeaderText = My.Resources.payMethod
            'dgvDetails.Columns.Item(c_GrantAmountColumnName).DisplayIndex = 6
            'dgvDetails.Columns.Item(c_GrantAmountColumnName).Visible = True
            'dgvDetails.Columns.Item(c_GrantAmountColumnName).HeaderText = My.Resources.grantAmt
            dgvDetails.Columns.Item(c_HandlerColumnName).DisplayIndex = 7
            dgvDetails.Columns.Item(c_HandlerColumnName).Visible = True
            dgvDetails.Columns.Item(c_HandlerColumnName).HeaderText = My.Resources.handler
            dgvDetails.Columns.Item(c_ReasonColumnName).DisplayIndex = 8
            dgvDetails.Columns.Item(c_ReasonColumnName).Visible = True
            dgvDetails.Columns.Item(c_ReasonColumnName).HeaderText = My.Resources.reason
            dgvDetails.Columns.Item(c_ExtraColumnName).DisplayIndex = 9
            dgvDetails.Columns.Item(c_ExtraColumnName).Visible = True
            dgvDetails.Columns.Item(c_ExtraColumnName).HeaderText = My.Resources.extra
        End If
        Dim i As Integer = 10
        For index As Integer = 0 To lstColName.Count - 1
            If lstColShow(index) = 1 Then
                dgvDetails.Columns.Item(lstColName(index)).DisplayIndex = i
                dgvDetails.Columns.Item(lstColName(index)).Visible = True
                dgvDetails.Columns.Item(lstColName(index)).HeaderText = lstColTxt(index)
                i = i + 1
            End If
        Next
        If dgvDetails.Columns.Item(c_SchoolColumnName).Visible = True Then
            Dim id As Integer
            Select Case intColSchType
                Case 0
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_PrimarySchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 1
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_JuniorSchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 2
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_HighSchColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
                Case 3
                    For index As Integer = 0 To dgvDetails.Rows.Count - 1
                        id = dgvDetails.Rows(index).Cells(c_UniversityColumnName).Value
                        dgvDetails.Rows(index).Cells(c_SchoolColumnName).Value = frmMain.GetSchName(id)
                    Next
            End Select
        End If
    End Sub

    Private Sub InitColSelections()
        lstColName.Add(c_EngNameColumnName)
        lstColName.Add(c_Tel1ColumnName)
        lstColName.Add(c_Tel2ColumnName)
        lstColName.Add(c_EmailColumnName)
        lstColName.Add(c_SchoolColumnName)
        lstColName.Add(c_GraduateFromColumnName)
        lstColName.Add(c_DadNameColumnName)
        lstColName.Add(c_MumNameColumnName)
        lstColName.Add(c_DadMobileColumnName)
        lstColName.Add(c_MumMobileColumnName)
        lstColName.Add(c_IntroIDColumnName)
        lstColName.Add(c_IntroNameColumnName)
        lstColName.Add(c_SchoolGradeColumnName)
        lstColName.Add(c_SchoolClassColumnName)
        FillArrayList(2, lstColName.Count, lstColShow)
        FillArrayList(1, lstColName.Count, lstColTxt)
    End Sub

    Private Sub mnuSelectCol_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuSelectCol.Click
        If dgvDetails.Columns.Count > 9 Then
            Dim lst As New ArrayList
            For index As Integer = 10 To dgvDetails.Columns.Count - 1
                If dgvDetails.Columns(index).Visible = True Then
                    lst.Add(1)
                Else
                    lst.Add(0)
                End If
            Next
            Dim frm As New frm2SelectCol(lstColName, lstColShow, intColSchType)
            frm.ShowDialog()
            If frmMain.GetOkState Then
                lstColShow = frmMain.GetCurrentList
                lstColTxt = frmMain.GetCurrentList2
                intColSchType = frmMain.GetCurrentValue
                LoadColumnText()
            End If
        End If
    End Sub

    Private Sub mnuClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles mnuClose.Click
        Me.Close()
    End Sub

    Private Sub frmStuMBSta_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        frmMain.SelectTab(Me.Text)
    End Sub

    Private Sub chkboxShowPast_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkboxShowPast.CheckedChanged
        ShowClassList()
    End Sub

    Private Sub butListSta_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butListSta.Click
        RefreshClassList()
        RefreshData()
    End Sub

    Private Sub dgvMaster_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvMaster.SelectionChanged
        Dim intSubClassId As Integer
        If dgvMaster.SelectedRows.Count > 0 Then
            intSubClassId = dgvMaster.SelectedRows(0).Cells(c_SubClassIDColumnName).Value
            If intSubClassId > -1 Then
                dtDetails.DefaultView.RowFilter = c_SubClassIDColumnName + "=" + intSubClassId.ToString
                dgvDetails.DataSource = dtDetails.DefaultView
            Else
                dtDetails.DefaultView.RowFilter = ""
                dgvDetails.DataSource = dtDetails.DefaultView
            End If
        End If
    End Sub

    Private Sub butSelAll_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelAll.Click
        For index As Integer = 0 To chklstClass.Items.Count - 1
            chklstClass.SetItemChecked(index, True)
        Next
        RefreshClassList()

    End Sub

    Private Sub butSelNone_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSelNone.Click
        For index As Integer = 0 To chklstClass.Items.Count - 1
            chklstClass.SetItemChecked(index, False)
        Next
        RefreshClassList()
    End Sub
End Class