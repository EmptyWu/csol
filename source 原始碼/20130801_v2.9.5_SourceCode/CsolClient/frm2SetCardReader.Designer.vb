﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2SetCardReader
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboxReader = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.butConfirm = New System.Windows.Forms.Button
        Me.butCancel = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboxReader
        '
        Me.cboxReader.FormattingEnabled = True
        Me.cboxReader.Items.AddRange(New Object() {"ComPort (舊式)", "USB"})
        Me.cboxReader.Location = New System.Drawing.Point(96, 9)
        Me.cboxReader.Name = "cboxReader"
        Me.cboxReader.Size = New System.Drawing.Size(121, 20)
        Me.cboxReader.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(13, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "選擇讀卡裝置: "
        '
        'butConfirm
        '
        Me.butConfirm.Location = New System.Drawing.Point(239, 4)
        Me.butConfirm.Name = "butConfirm"
        Me.butConfirm.Size = New System.Drawing.Size(82, 28)
        Me.butConfirm.TabIndex = 2
        Me.butConfirm.Text = "確定"
        Me.butConfirm.UseVisualStyleBackColor = True
        '
        'butCancel
        '
        Me.butCancel.Location = New System.Drawing.Point(239, 48)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 28)
        Me.butCancel.TabIndex = 4
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'frm2SetCardReader
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(335, 90)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butConfirm)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cboxReader)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frm2SetCardReader"
        Me.ShowInTaskbar = False
        Me.Text = "設定讀卡裝置"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboxReader As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents butConfirm As System.Windows.Forms.Button
    Friend WithEvents butCancel As System.Windows.Forms.Button
End Class
