﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm2AddTeleInterviewClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Label2 = New System.Windows.Forms.Label
        Me.cboxSubClass = New System.Windows.Forms.ComboBox
        Me.cboxClass = New System.Windows.Forms.ComboBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.butSelNone = New System.Windows.Forms.Button
        Me.butSelAll = New System.Windows.Forms.Button
        Me.chklstStu = New System.Windows.Forms.CheckedListBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.butAddType = New System.Windows.Forms.Button
        Me.tboxContent = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.cboxType = New System.Windows.Forms.ComboBox
        Me.butCancel = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.dtpickDate = New System.Windows.Forms.DateTimePicker
        Me.Label7 = New System.Windows.Forms.Label
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label2.Location = New System.Drawing.Point(274, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 12)
        Me.Label2.TabIndex = 148
        Me.Label2.Text = "班別"
        '
        'cboxSubClass
        '
        Me.cboxSubClass.FormattingEnabled = True
        Me.cboxSubClass.Location = New System.Drawing.Point(309, 12)
        Me.cboxSubClass.Name = "cboxSubClass"
        Me.cboxSubClass.Size = New System.Drawing.Size(211, 20)
        Me.cboxSubClass.TabIndex = 147
        '
        'cboxClass
        '
        Me.cboxClass.FormattingEnabled = True
        Me.cboxClass.Location = New System.Drawing.Point(45, 12)
        Me.cboxClass.Name = "cboxClass"
        Me.cboxClass.Size = New System.Drawing.Size(211, 20)
        Me.cboxClass.TabIndex = 146
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label1.Location = New System.Drawing.Point(10, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(29, 12)
        Me.Label1.TabIndex = 145
        Me.Label1.Text = "班級"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.Label3)
        Me.GroupBox3.Controls.Add(Me.butSelNone)
        Me.GroupBox3.Controls.Add(Me.butSelAll)
        Me.GroupBox3.Controls.Add(Me.chklstStu)
        Me.GroupBox3.Location = New System.Drawing.Point(12, 38)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(262, 427)
        Me.GroupBox3.TabIndex = 149
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "學生"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Red
        Me.Label3.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label3.Location = New System.Drawing.Point(31, 368)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(173, 12)
        Me.Label3.TabIndex = 149
        Me.Label3.Text = "請勾選要儲存電訪記錄的學生。"
        '
        'butSelNone
        '
        Me.butSelNone.AutoSize = True
        Me.butSelNone.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSelNone.Location = New System.Drawing.Point(170, 399)
        Me.butSelNone.Name = "butSelNone"
        Me.butSelNone.Size = New System.Drawing.Size(76, 22)
        Me.butSelNone.TabIndex = 20
        Me.butSelNone.Text = "全不選"
        Me.butSelNone.UseVisualStyleBackColor = True
        '
        'butSelAll
        '
        Me.butSelAll.AutoSize = True
        Me.butSelAll.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSelAll.Location = New System.Drawing.Point(88, 399)
        Me.butSelAll.Name = "butSelAll"
        Me.butSelAll.Size = New System.Drawing.Size(76, 22)
        Me.butSelAll.TabIndex = 19
        Me.butSelAll.Text = "全選"
        Me.butSelAll.UseVisualStyleBackColor = True
        '
        'chklstStu
        '
        Me.chklstStu.CheckOnClick = True
        Me.chklstStu.FormattingEnabled = True
        Me.chklstStu.Location = New System.Drawing.Point(22, 21)
        Me.chklstStu.Name = "chklstStu"
        Me.chklstStu.Size = New System.Drawing.Size(222, 344)
        Me.chklstStu.TabIndex = 17
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dtpickDate)
        Me.GroupBox1.Controls.Add(Me.Label7)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.butAddType)
        Me.GroupBox1.Controls.Add(Me.tboxContent)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.cboxType)
        Me.GroupBox1.Location = New System.Drawing.Point(290, 38)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(368, 365)
        Me.GroupBox1.TabIndex = 150
        Me.GroupBox1.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label5.Location = New System.Drawing.Point(17, 320)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(192, 12)
        Me.Label5.TabIndex = 140
        Me.Label5.Text = "最長255個中文字，Enter算兩個字。"
        Me.Label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'butAddType
        '
        Me.butAddType.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butAddType.Location = New System.Drawing.Point(199, 12)
        Me.butAddType.Name = "butAddType"
        Me.butAddType.Size = New System.Drawing.Size(82, 25)
        Me.butAddType.TabIndex = 139
        Me.butAddType.Text = "新增類別"
        Me.butAddType.UseVisualStyleBackColor = True
        '
        'tboxContent
        '
        Me.tboxContent.Location = New System.Drawing.Point(19, 105)
        Me.tboxContent.Multiline = True
        Me.tboxContent.Name = "tboxContent"
        Me.tboxContent.Size = New System.Drawing.Size(332, 200)
        Me.tboxContent.TabIndex = 138
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label4.Location = New System.Drawing.Point(17, 79)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 12)
        Me.Label4.TabIndex = 137
        Me.Label4.Text = "電訪內容: "
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label6.Location = New System.Drawing.Point(17, 18)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(59, 12)
        Me.Label6.TabIndex = 136
        Me.Label6.Text = "電訪類別: "
        Me.Label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'cboxType
        '
        Me.cboxType.FormattingEnabled = True
        Me.cboxType.Location = New System.Drawing.Point(76, 15)
        Me.cboxType.Name = "cboxType"
        Me.cboxType.Size = New System.Drawing.Size(117, 20)
        Me.cboxType.TabIndex = 135
        '
        'butCancel
        '
        Me.butCancel.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butCancel.Location = New System.Drawing.Point(501, 436)
        Me.butCancel.Name = "butCancel"
        Me.butCancel.Size = New System.Drawing.Size(82, 25)
        Me.butCancel.TabIndex = 152
        Me.butCancel.Text = "取消"
        Me.butCancel.UseVisualStyleBackColor = True
        '
        'butSave
        '
        Me.butSave.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.butSave.Location = New System.Drawing.Point(394, 436)
        Me.butSave.Name = "butSave"
        Me.butSave.Size = New System.Drawing.Size(82, 25)
        Me.butSave.TabIndex = 151
        Me.butSave.Text = "儲存"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'dtpickDate
        '
        Me.dtpickDate.Location = New System.Drawing.Point(76, 43)
        Me.dtpickDate.Name = "dtpickDate"
        Me.dtpickDate.Size = New System.Drawing.Size(165, 22)
        Me.dtpickDate.TabIndex = 142
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.Label7.Location = New System.Drawing.Point(35, 48)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(35, 12)
        Me.Label7.TabIndex = 141
        Me.Label7.Text = "日期: "
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'frm2AddTeleInterviewClass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(690, 479)
        Me.Controls.Add(Me.butCancel)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.cboxSubClass)
        Me.Controls.Add(Me.cboxClass)
        Me.Controls.Add(Me.Label1)
        Me.Name = "frm2AddTeleInterviewClass"
        Me.Text = "電訪記錄整批輸入"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents cboxSubClass As System.Windows.Forms.ComboBox
    Friend WithEvents cboxClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents chklstStu As System.Windows.Forms.CheckedListBox
    Friend WithEvents butSelNone As System.Windows.Forms.Button
    Friend WithEvents butSelAll As System.Windows.Forms.Button
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents butAddType As System.Windows.Forms.Button
    Friend WithEvents tboxContent As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents cboxType As System.Windows.Forms.ComboBox
    Friend WithEvents butCancel As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents dtpickDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label7 As System.Windows.Forms.Label
End Class
