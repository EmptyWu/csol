﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAddStu
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAddStu))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.NumUDStuYear = New System.Windows.Forms.NumericUpDown
        Me.lblStuYear = New System.Windows.Forms.Label
        Me.radbutPotStu = New System.Windows.Forms.RadioButton
        Me.radbutFormalStu = New System.Windows.Forms.RadioButton
        Me.Label5 = New System.Windows.Forms.Label
        Me.tboxStuName = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.lblStuId = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.cboxSex = New System.Windows.Forms.ComboBox
        Me.tboxBirthD = New System.Windows.Forms.TextBox
        Me.tboxBirthM = New System.Windows.Forms.TextBox
        Me.tboxBirthY = New System.Windows.Forms.TextBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tboxEngName = New System.Windows.Forms.TextBox
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label10 = New System.Windows.Forms.Label
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.radbutNowUni = New System.Windows.Forms.RadioButton
        Me.radbutNowHig = New System.Windows.Forms.RadioButton
        Me.tboxClass = New System.Windows.Forms.TextBox
        Me.Label16 = New System.Windows.Forms.Label
        Me.cboxGroup = New System.Windows.Forms.ComboBox
        Me.Label15 = New System.Windows.Forms.Label
        Me.cboxGrade = New System.Windows.Forms.ComboBox
        Me.Label14 = New System.Windows.Forms.Label
        Me.butUni = New System.Windows.Forms.Button
        Me.cboxUniversity = New System.Windows.Forms.ComboBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.butHig = New System.Windows.Forms.Button
        Me.cboxHighSch = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.butJun = New System.Windows.Forms.Button
        Me.cboxJuniorSch = New System.Windows.Forms.ComboBox
        Me.Label9 = New System.Windows.Forms.Label
        Me.butPri = New System.Windows.Forms.Button
        Me.cboxPrimarySch = New System.Windows.Forms.ComboBox
        Me.radbutNowJun = New System.Windows.Forms.RadioButton
        Me.radbutNowPri = New System.Windows.Forms.RadioButton
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label13 = New System.Windows.Forms.Label
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.tboxPunchCardNum = New System.Windows.Forms.TextBox
        Me.Label43 = New System.Windows.Forms.Label
        Me.tboxEnrollSch = New System.Windows.Forms.TextBox
        Me.Label42 = New System.Windows.Forms.Label
        Me.tboxHseeMark = New System.Windows.Forms.TextBox
        Me.Label41 = New System.Windows.Forms.Label
        Me.tboxAdd1b = New System.Windows.Forms.TextBox
        Me.tboxAdd2b = New System.Windows.Forms.TextBox
        Me.tboxClassId = New System.Windows.Forms.TextBox
        Me.tboxStay = New System.Windows.Forms.TextBox
        Me.Label32 = New System.Windows.Forms.Label
        Me.Label33 = New System.Windows.Forms.Label
        Me.tboxDadTitle = New System.Windows.Forms.TextBox
        Me.tboxRegInfo = New System.Windows.Forms.TextBox
        Me.Label34 = New System.Windows.Forms.Label
        Me.tboxMumTitle = New System.Windows.Forms.TextBox
        Me.Label35 = New System.Windows.Forms.Label
        Me.Label36 = New System.Windows.Forms.Label
        Me.tboxMumMobile = New System.Windows.Forms.TextBox
        Me.tboxIntroName = New System.Windows.Forms.TextBox
        Me.Label27 = New System.Windows.Forms.Label
        Me.Label28 = New System.Windows.Forms.Label
        Me.tboxGradJunior = New System.Windows.Forms.TextBox
        Me.tboxMumName = New System.Windows.Forms.TextBox
        Me.Label29 = New System.Windows.Forms.Label
        Me.tboxIc = New System.Windows.Forms.TextBox
        Me.Label30 = New System.Windows.Forms.Label
        Me.Label31 = New System.Windows.Forms.Label
        Me.tboxAdd1a = New System.Windows.Forms.TextBox
        Me.tboxEmail = New System.Windows.Forms.TextBox
        Me.Label23 = New System.Windows.Forms.Label
        Me.tboxAdd2a = New System.Windows.Forms.TextBox
        Me.Label24 = New System.Windows.Forms.Label
        Me.Label25 = New System.Windows.Forms.Label
        Me.tboxIntroId = New System.Windows.Forms.TextBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.tboxMobile = New System.Windows.Forms.TextBox
        Me.tboxDadMobile = New System.Windows.Forms.TextBox
        Me.Label17 = New System.Windows.Forms.Label
        Me.tboxDadName = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.Label22 = New System.Windows.Forms.Label
        Me.tboxTel1 = New System.Windows.Forms.TextBox
        Me.tboxOfficeTel = New System.Windows.Forms.TextBox
        Me.Label18 = New System.Windows.Forms.Label
        Me.tboxTel2 = New System.Windows.Forms.TextBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.Label21 = New System.Windows.Forms.Label
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.butSales = New System.Windows.Forms.Button
        Me.butSeatNum = New System.Windows.Forms.Button
        Me.butDelClass = New System.Windows.Forms.Button
        Me.butReg = New System.Windows.Forms.Button
        Me.dgvClass = New System.Windows.Forms.DataGridView
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.tboxRemark4 = New System.Windows.Forms.TextBox
        Me.tboxRemark3 = New System.Windows.Forms.TextBox
        Me.tboxRemark2 = New System.Windows.Forms.TextBox
        Me.tboxRemark1 = New System.Windows.Forms.TextBox
        Me.tboxRank4 = New System.Windows.Forms.TextBox
        Me.tboxRank3 = New System.Windows.Forms.TextBox
        Me.tboxRank2 = New System.Windows.Forms.TextBox
        Me.tboxRank1 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.tboxSchool4 = New System.Windows.Forms.TextBox
        Me.cboxFamily4 = New System.Windows.Forms.ComboBox
        Me.tboxBirth4d = New System.Windows.Forms.TextBox
        Me.tboxBirth4m = New System.Windows.Forms.TextBox
        Me.tboxBirth4y = New System.Windows.Forms.TextBox
        Me.tboxName4 = New System.Windows.Forms.TextBox
        Me.tboxSchool3 = New System.Windows.Forms.TextBox
        Me.cboxFamily3 = New System.Windows.Forms.ComboBox
        Me.tboxBirth3d = New System.Windows.Forms.TextBox
        Me.tboxBirth3m = New System.Windows.Forms.TextBox
        Me.tboxBirth3y = New System.Windows.Forms.TextBox
        Me.tboxName3 = New System.Windows.Forms.TextBox
        Me.tboxSchool2 = New System.Windows.Forms.TextBox
        Me.cboxFamily2 = New System.Windows.Forms.ComboBox
        Me.tboxBirth2d = New System.Windows.Forms.TextBox
        Me.tboxBirth2m = New System.Windows.Forms.TextBox
        Me.tboxBirth2y = New System.Windows.Forms.TextBox
        Me.tboxName2 = New System.Windows.Forms.TextBox
        Me.tboxSchool1 = New System.Windows.Forms.TextBox
        Me.Label40 = New System.Windows.Forms.Label
        Me.cboxFamily1 = New System.Windows.Forms.ComboBox
        Me.tboxBirth1d = New System.Windows.Forms.TextBox
        Me.tboxBirth1m = New System.Windows.Forms.TextBox
        Me.tboxBirth1y = New System.Windows.Forms.TextBox
        Me.Label37 = New System.Windows.Forms.Label
        Me.tboxName1 = New System.Windows.Forms.TextBox
        Me.Label38 = New System.Windows.Forms.Label
        Me.Label39 = New System.Windows.Forms.Label
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.rtboxRemarks = New System.Windows.Forms.RichTextBox
        Me.butClose = New System.Windows.Forms.Button
        Me.butSave = New System.Windows.Forms.Button
        Me.GroupBox8 = New System.Windows.Forms.GroupBox
        Me.dgvOldRec = New System.Windows.Forms.DataGridView
        Me.GroupBox1.SuspendLayout()
        CType(Me.NumUDStuYear, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.GroupBox7.SuspendLayout()
        Me.GroupBox8.SuspendLayout()
        CType(Me.dgvOldRec, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.NumUDStuYear)
        Me.GroupBox1.Controls.Add(Me.lblStuYear)
        Me.GroupBox1.Controls.Add(Me.radbutPotStu)
        Me.GroupBox1.Controls.Add(Me.radbutFormalStu)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.tboxStuName)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.lblStuId)
        Me.GroupBox1.Controls.Add(Me.Label1)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'NumUDStuYear
        '
        resources.ApplyResources(Me.NumUDStuYear, "NumUDStuYear")
        Me.NumUDStuYear.Name = "NumUDStuYear"
        Me.NumUDStuYear.Value = New Decimal(New Integer() {99, 0, 0, 0})
        '
        'lblStuYear
        '
        resources.ApplyResources(Me.lblStuYear, "lblStuYear")
        Me.lblStuYear.Name = "lblStuYear"
        '
        'radbutPotStu
        '
        resources.ApplyResources(Me.radbutPotStu, "radbutPotStu")
        Me.radbutPotStu.Name = "radbutPotStu"
        Me.radbutPotStu.TabStop = True
        Me.radbutPotStu.UseVisualStyleBackColor = True
        '
        'radbutFormalStu
        '
        resources.ApplyResources(Me.radbutFormalStu, "radbutFormalStu")
        Me.radbutFormalStu.Checked = True
        Me.radbutFormalStu.Name = "radbutFormalStu"
        Me.radbutFormalStu.TabStop = True
        Me.radbutFormalStu.UseVisualStyleBackColor = True
        '
        'Label5
        '
        resources.ApplyResources(Me.Label5, "Label5")
        Me.Label5.Name = "Label5"
        '
        'tboxStuName
        '
        resources.ApplyResources(Me.tboxStuName, "tboxStuName")
        Me.tboxStuName.Name = "tboxStuName"
        '
        'Label3
        '
        resources.ApplyResources(Me.Label3, "Label3")
        Me.Label3.Name = "Label3"
        '
        'lblStuId
        '
        Me.lblStuId.BackColor = System.Drawing.Color.White
        resources.ApplyResources(Me.lblStuId, "lblStuId")
        Me.lblStuId.Name = "lblStuId"
        '
        'Label1
        '
        resources.ApplyResources(Me.Label1, "Label1")
        Me.Label1.Name = "Label1"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.cboxSex)
        Me.GroupBox2.Controls.Add(Me.tboxBirthD)
        Me.GroupBox2.Controls.Add(Me.tboxBirthM)
        Me.GroupBox2.Controls.Add(Me.tboxBirthY)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Controls.Add(Me.tboxEngName)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label10)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'cboxSex
        '
        Me.cboxSex.FormattingEnabled = True
        Me.cboxSex.Items.AddRange(New Object() {resources.GetString("cboxSex.Items"), resources.GetString("cboxSex.Items1")})
        resources.ApplyResources(Me.cboxSex, "cboxSex")
        Me.cboxSex.Name = "cboxSex"
        '
        'tboxBirthD
        '
        resources.ApplyResources(Me.tboxBirthD, "tboxBirthD")
        Me.tboxBirthD.Name = "tboxBirthD"
        '
        'tboxBirthM
        '
        resources.ApplyResources(Me.tboxBirthM, "tboxBirthM")
        Me.tboxBirthM.Name = "tboxBirthM"
        '
        'tboxBirthY
        '
        resources.ApplyResources(Me.tboxBirthY, "tboxBirthY")
        Me.tboxBirthY.Name = "tboxBirthY"
        '
        'Label7
        '
        resources.ApplyResources(Me.Label7, "Label7")
        Me.Label7.Name = "Label7"
        '
        'tboxEngName
        '
        resources.ApplyResources(Me.tboxEngName, "tboxEngName")
        Me.tboxEngName.Name = "tboxEngName"
        '
        'Label8
        '
        resources.ApplyResources(Me.Label8, "Label8")
        Me.Label8.Name = "Label8"
        '
        'Label10
        '
        resources.ApplyResources(Me.Label10, "Label10")
        Me.Label10.Name = "Label10"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.radbutNowUni)
        Me.GroupBox3.Controls.Add(Me.radbutNowHig)
        Me.GroupBox3.Controls.Add(Me.tboxClass)
        Me.GroupBox3.Controls.Add(Me.Label16)
        Me.GroupBox3.Controls.Add(Me.cboxGroup)
        Me.GroupBox3.Controls.Add(Me.Label15)
        Me.GroupBox3.Controls.Add(Me.cboxGrade)
        Me.GroupBox3.Controls.Add(Me.Label14)
        Me.GroupBox3.Controls.Add(Me.butUni)
        Me.GroupBox3.Controls.Add(Me.cboxUniversity)
        Me.GroupBox3.Controls.Add(Me.Label12)
        Me.GroupBox3.Controls.Add(Me.butHig)
        Me.GroupBox3.Controls.Add(Me.cboxHighSch)
        Me.GroupBox3.Controls.Add(Me.Label11)
        Me.GroupBox3.Controls.Add(Me.butJun)
        Me.GroupBox3.Controls.Add(Me.cboxJuniorSch)
        Me.GroupBox3.Controls.Add(Me.Label9)
        Me.GroupBox3.Controls.Add(Me.butPri)
        Me.GroupBox3.Controls.Add(Me.cboxPrimarySch)
        Me.GroupBox3.Controls.Add(Me.radbutNowJun)
        Me.GroupBox3.Controls.Add(Me.radbutNowPri)
        Me.GroupBox3.Controls.Add(Me.Label6)
        Me.GroupBox3.Controls.Add(Me.Label13)
        resources.ApplyResources(Me.GroupBox3, "GroupBox3")
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.TabStop = False
        '
        'radbutNowUni
        '
        resources.ApplyResources(Me.radbutNowUni, "radbutNowUni")
        Me.radbutNowUni.Name = "radbutNowUni"
        Me.radbutNowUni.UseVisualStyleBackColor = True
        '
        'radbutNowHig
        '
        resources.ApplyResources(Me.radbutNowHig, "radbutNowHig")
        Me.radbutNowHig.Name = "radbutNowHig"
        Me.radbutNowHig.UseVisualStyleBackColor = True
        '
        'tboxClass
        '
        resources.ApplyResources(Me.tboxClass, "tboxClass")
        Me.tboxClass.Name = "tboxClass"
        '
        'Label16
        '
        resources.ApplyResources(Me.Label16, "Label16")
        Me.Label16.Name = "Label16"
        '
        'cboxGroup
        '
        Me.cboxGroup.FormattingEnabled = True
        resources.ApplyResources(Me.cboxGroup, "cboxGroup")
        Me.cboxGroup.Name = "cboxGroup"
        '
        'Label15
        '
        resources.ApplyResources(Me.Label15, "Label15")
        Me.Label15.Name = "Label15"
        '
        'cboxGrade
        '
        Me.cboxGrade.FormattingEnabled = True
        Me.cboxGrade.Items.AddRange(New Object() {resources.GetString("cboxGrade.Items"), resources.GetString("cboxGrade.Items1"), resources.GetString("cboxGrade.Items2"), resources.GetString("cboxGrade.Items3"), resources.GetString("cboxGrade.Items4"), resources.GetString("cboxGrade.Items5"), resources.GetString("cboxGrade.Items6"), resources.GetString("cboxGrade.Items7"), resources.GetString("cboxGrade.Items8"), resources.GetString("cboxGrade.Items9"), resources.GetString("cboxGrade.Items10"), resources.GetString("cboxGrade.Items11"), resources.GetString("cboxGrade.Items12")})
        resources.ApplyResources(Me.cboxGrade, "cboxGrade")
        Me.cboxGrade.Name = "cboxGrade"
        '
        'Label14
        '
        resources.ApplyResources(Me.Label14, "Label14")
        Me.Label14.Name = "Label14"
        '
        'butUni
        '
        resources.ApplyResources(Me.butUni, "butUni")
        Me.butUni.Name = "butUni"
        Me.butUni.UseVisualStyleBackColor = True
        '
        'cboxUniversity
        '
        Me.cboxUniversity.FormattingEnabled = True
        resources.ApplyResources(Me.cboxUniversity, "cboxUniversity")
        Me.cboxUniversity.Name = "cboxUniversity"
        '
        'Label12
        '
        resources.ApplyResources(Me.Label12, "Label12")
        Me.Label12.Name = "Label12"
        '
        'butHig
        '
        resources.ApplyResources(Me.butHig, "butHig")
        Me.butHig.Name = "butHig"
        Me.butHig.UseVisualStyleBackColor = True
        '
        'cboxHighSch
        '
        Me.cboxHighSch.FormattingEnabled = True
        resources.ApplyResources(Me.cboxHighSch, "cboxHighSch")
        Me.cboxHighSch.Name = "cboxHighSch"
        '
        'Label11
        '
        resources.ApplyResources(Me.Label11, "Label11")
        Me.Label11.Name = "Label11"
        '
        'butJun
        '
        resources.ApplyResources(Me.butJun, "butJun")
        Me.butJun.Name = "butJun"
        Me.butJun.UseVisualStyleBackColor = True
        '
        'cboxJuniorSch
        '
        Me.cboxJuniorSch.FormattingEnabled = True
        resources.ApplyResources(Me.cboxJuniorSch, "cboxJuniorSch")
        Me.cboxJuniorSch.Name = "cboxJuniorSch"
        '
        'Label9
        '
        resources.ApplyResources(Me.Label9, "Label9")
        Me.Label9.Name = "Label9"
        '
        'butPri
        '
        resources.ApplyResources(Me.butPri, "butPri")
        Me.butPri.Name = "butPri"
        Me.butPri.UseVisualStyleBackColor = True
        '
        'cboxPrimarySch
        '
        Me.cboxPrimarySch.FormattingEnabled = True
        resources.ApplyResources(Me.cboxPrimarySch, "cboxPrimarySch")
        Me.cboxPrimarySch.Name = "cboxPrimarySch"
        '
        'radbutNowJun
        '
        resources.ApplyResources(Me.radbutNowJun, "radbutNowJun")
        Me.radbutNowJun.Name = "radbutNowJun"
        Me.radbutNowJun.UseVisualStyleBackColor = True
        '
        'radbutNowPri
        '
        resources.ApplyResources(Me.radbutNowPri, "radbutNowPri")
        Me.radbutNowPri.Name = "radbutNowPri"
        Me.radbutNowPri.UseVisualStyleBackColor = True
        '
        'Label6
        '
        resources.ApplyResources(Me.Label6, "Label6")
        Me.Label6.Name = "Label6"
        '
        'Label13
        '
        resources.ApplyResources(Me.Label13, "Label13")
        Me.Label13.Name = "Label13"
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.tboxPunchCardNum)
        Me.GroupBox4.Controls.Add(Me.Label43)
        Me.GroupBox4.Controls.Add(Me.tboxEnrollSch)
        Me.GroupBox4.Controls.Add(Me.Label42)
        Me.GroupBox4.Controls.Add(Me.tboxHseeMark)
        Me.GroupBox4.Controls.Add(Me.Label41)
        Me.GroupBox4.Controls.Add(Me.tboxAdd1b)
        Me.GroupBox4.Controls.Add(Me.tboxAdd2b)
        Me.GroupBox4.Controls.Add(Me.tboxClassId)
        Me.GroupBox4.Controls.Add(Me.tboxStay)
        Me.GroupBox4.Controls.Add(Me.Label32)
        Me.GroupBox4.Controls.Add(Me.Label33)
        Me.GroupBox4.Controls.Add(Me.tboxDadTitle)
        Me.GroupBox4.Controls.Add(Me.tboxRegInfo)
        Me.GroupBox4.Controls.Add(Me.Label34)
        Me.GroupBox4.Controls.Add(Me.tboxMumTitle)
        Me.GroupBox4.Controls.Add(Me.Label35)
        Me.GroupBox4.Controls.Add(Me.Label36)
        Me.GroupBox4.Controls.Add(Me.tboxMumMobile)
        Me.GroupBox4.Controls.Add(Me.tboxIntroName)
        Me.GroupBox4.Controls.Add(Me.Label27)
        Me.GroupBox4.Controls.Add(Me.Label28)
        Me.GroupBox4.Controls.Add(Me.tboxGradJunior)
        Me.GroupBox4.Controls.Add(Me.tboxMumName)
        Me.GroupBox4.Controls.Add(Me.Label29)
        Me.GroupBox4.Controls.Add(Me.tboxIc)
        Me.GroupBox4.Controls.Add(Me.Label30)
        Me.GroupBox4.Controls.Add(Me.Label31)
        Me.GroupBox4.Controls.Add(Me.tboxAdd1a)
        Me.GroupBox4.Controls.Add(Me.tboxEmail)
        Me.GroupBox4.Controls.Add(Me.Label23)
        Me.GroupBox4.Controls.Add(Me.tboxAdd2a)
        Me.GroupBox4.Controls.Add(Me.Label24)
        Me.GroupBox4.Controls.Add(Me.Label25)
        Me.GroupBox4.Controls.Add(Me.tboxIntroId)
        Me.GroupBox4.Controls.Add(Me.Label26)
        Me.GroupBox4.Controls.Add(Me.tboxMobile)
        Me.GroupBox4.Controls.Add(Me.tboxDadMobile)
        Me.GroupBox4.Controls.Add(Me.Label17)
        Me.GroupBox4.Controls.Add(Me.tboxDadName)
        Me.GroupBox4.Controls.Add(Me.Label20)
        Me.GroupBox4.Controls.Add(Me.Label22)
        Me.GroupBox4.Controls.Add(Me.tboxTel1)
        Me.GroupBox4.Controls.Add(Me.tboxOfficeTel)
        Me.GroupBox4.Controls.Add(Me.Label18)
        Me.GroupBox4.Controls.Add(Me.tboxTel2)
        Me.GroupBox4.Controls.Add(Me.Label19)
        Me.GroupBox4.Controls.Add(Me.Label21)
        resources.ApplyResources(Me.GroupBox4, "GroupBox4")
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.TabStop = False
        '
        'tboxPunchCardNum
        '
        resources.ApplyResources(Me.tboxPunchCardNum, "tboxPunchCardNum")
        Me.tboxPunchCardNum.Name = "tboxPunchCardNum"
        '
        'Label43
        '
        resources.ApplyResources(Me.Label43, "Label43")
        Me.Label43.Name = "Label43"
        '
        'tboxEnrollSch
        '
        resources.ApplyResources(Me.tboxEnrollSch, "tboxEnrollSch")
        Me.tboxEnrollSch.Name = "tboxEnrollSch"
        '
        'Label42
        '
        resources.ApplyResources(Me.Label42, "Label42")
        Me.Label42.Name = "Label42"
        '
        'tboxHseeMark
        '
        resources.ApplyResources(Me.tboxHseeMark, "tboxHseeMark")
        Me.tboxHseeMark.Name = "tboxHseeMark"
        '
        'Label41
        '
        resources.ApplyResources(Me.Label41, "Label41")
        Me.Label41.Name = "Label41"
        '
        'tboxAdd1b
        '
        resources.ApplyResources(Me.tboxAdd1b, "tboxAdd1b")
        Me.tboxAdd1b.Name = "tboxAdd1b"
        '
        'tboxAdd2b
        '
        resources.ApplyResources(Me.tboxAdd2b, "tboxAdd2b")
        Me.tboxAdd2b.Name = "tboxAdd2b"
        '
        'tboxClassId
        '
        resources.ApplyResources(Me.tboxClassId, "tboxClassId")
        Me.tboxClassId.Name = "tboxClassId"
        '
        'tboxStay
        '
        resources.ApplyResources(Me.tboxStay, "tboxStay")
        Me.tboxStay.Name = "tboxStay"
        '
        'Label32
        '
        resources.ApplyResources(Me.Label32, "Label32")
        Me.Label32.Name = "Label32"
        '
        'Label33
        '
        resources.ApplyResources(Me.Label33, "Label33")
        Me.Label33.Name = "Label33"
        '
        'tboxDadTitle
        '
        resources.ApplyResources(Me.tboxDadTitle, "tboxDadTitle")
        Me.tboxDadTitle.Name = "tboxDadTitle"
        '
        'tboxRegInfo
        '
        resources.ApplyResources(Me.tboxRegInfo, "tboxRegInfo")
        Me.tboxRegInfo.Name = "tboxRegInfo"
        '
        'Label34
        '
        resources.ApplyResources(Me.Label34, "Label34")
        Me.Label34.Name = "Label34"
        '
        'tboxMumTitle
        '
        resources.ApplyResources(Me.tboxMumTitle, "tboxMumTitle")
        Me.tboxMumTitle.Name = "tboxMumTitle"
        '
        'Label35
        '
        resources.ApplyResources(Me.Label35, "Label35")
        Me.Label35.Name = "Label35"
        '
        'Label36
        '
        resources.ApplyResources(Me.Label36, "Label36")
        Me.Label36.Name = "Label36"
        '
        'tboxMumMobile
        '
        resources.ApplyResources(Me.tboxMumMobile, "tboxMumMobile")
        Me.tboxMumMobile.Name = "tboxMumMobile"
        '
        'tboxIntroName
        '
        resources.ApplyResources(Me.tboxIntroName, "tboxIntroName")
        Me.tboxIntroName.Name = "tboxIntroName"
        '
        'Label27
        '
        resources.ApplyResources(Me.Label27, "Label27")
        Me.Label27.Name = "Label27"
        '
        'Label28
        '
        resources.ApplyResources(Me.Label28, "Label28")
        Me.Label28.Name = "Label28"
        '
        'tboxGradJunior
        '
        resources.ApplyResources(Me.tboxGradJunior, "tboxGradJunior")
        Me.tboxGradJunior.Name = "tboxGradJunior"
        '
        'tboxMumName
        '
        resources.ApplyResources(Me.tboxMumName, "tboxMumName")
        Me.tboxMumName.Name = "tboxMumName"
        '
        'Label29
        '
        resources.ApplyResources(Me.Label29, "Label29")
        Me.Label29.Name = "Label29"
        '
        'tboxIc
        '
        resources.ApplyResources(Me.tboxIc, "tboxIc")
        Me.tboxIc.Name = "tboxIc"
        '
        'Label30
        '
        resources.ApplyResources(Me.Label30, "Label30")
        Me.Label30.Name = "Label30"
        '
        'Label31
        '
        resources.ApplyResources(Me.Label31, "Label31")
        Me.Label31.Name = "Label31"
        '
        'tboxAdd1a
        '
        resources.ApplyResources(Me.tboxAdd1a, "tboxAdd1a")
        Me.tboxAdd1a.Name = "tboxAdd1a"
        '
        'tboxEmail
        '
        resources.ApplyResources(Me.tboxEmail, "tboxEmail")
        Me.tboxEmail.Name = "tboxEmail"
        '
        'Label23
        '
        resources.ApplyResources(Me.Label23, "Label23")
        Me.Label23.Name = "Label23"
        '
        'tboxAdd2a
        '
        resources.ApplyResources(Me.tboxAdd2a, "tboxAdd2a")
        Me.tboxAdd2a.Name = "tboxAdd2a"
        '
        'Label24
        '
        resources.ApplyResources(Me.Label24, "Label24")
        Me.Label24.Name = "Label24"
        '
        'Label25
        '
        resources.ApplyResources(Me.Label25, "Label25")
        Me.Label25.Name = "Label25"
        '
        'tboxIntroId
        '
        resources.ApplyResources(Me.tboxIntroId, "tboxIntroId")
        Me.tboxIntroId.Name = "tboxIntroId"
        '
        'Label26
        '
        resources.ApplyResources(Me.Label26, "Label26")
        Me.Label26.Name = "Label26"
        '
        'tboxMobile
        '
        resources.ApplyResources(Me.tboxMobile, "tboxMobile")
        Me.tboxMobile.Name = "tboxMobile"
        '
        'tboxDadMobile
        '
        resources.ApplyResources(Me.tboxDadMobile, "tboxDadMobile")
        Me.tboxDadMobile.Name = "tboxDadMobile"
        '
        'Label17
        '
        resources.ApplyResources(Me.Label17, "Label17")
        Me.Label17.Name = "Label17"
        '
        'tboxDadName
        '
        resources.ApplyResources(Me.tboxDadName, "tboxDadName")
        Me.tboxDadName.Name = "tboxDadName"
        '
        'Label20
        '
        resources.ApplyResources(Me.Label20, "Label20")
        Me.Label20.Name = "Label20"
        '
        'Label22
        '
        resources.ApplyResources(Me.Label22, "Label22")
        Me.Label22.Name = "Label22"
        '
        'tboxTel1
        '
        resources.ApplyResources(Me.tboxTel1, "tboxTel1")
        Me.tboxTel1.Name = "tboxTel1"
        '
        'tboxOfficeTel
        '
        resources.ApplyResources(Me.tboxOfficeTel, "tboxOfficeTel")
        Me.tboxOfficeTel.Name = "tboxOfficeTel"
        '
        'Label18
        '
        resources.ApplyResources(Me.Label18, "Label18")
        Me.Label18.Name = "Label18"
        '
        'tboxTel2
        '
        resources.ApplyResources(Me.tboxTel2, "tboxTel2")
        Me.tboxTel2.Name = "tboxTel2"
        '
        'Label19
        '
        resources.ApplyResources(Me.Label19, "Label19")
        Me.Label19.Name = "Label19"
        '
        'Label21
        '
        resources.ApplyResources(Me.Label21, "Label21")
        Me.Label21.Name = "Label21"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.butSales)
        Me.GroupBox5.Controls.Add(Me.butSeatNum)
        Me.GroupBox5.Controls.Add(Me.butDelClass)
        Me.GroupBox5.Controls.Add(Me.butReg)
        Me.GroupBox5.Controls.Add(Me.dgvClass)
        resources.ApplyResources(Me.GroupBox5, "GroupBox5")
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.TabStop = False
        '
        'butSales
        '
        resources.ApplyResources(Me.butSales, "butSales")
        Me.butSales.Name = "butSales"
        Me.butSales.UseVisualStyleBackColor = True
        '
        'butSeatNum
        '
        resources.ApplyResources(Me.butSeatNum, "butSeatNum")
        Me.butSeatNum.Name = "butSeatNum"
        Me.butSeatNum.UseVisualStyleBackColor = True
        '
        'butDelClass
        '
        resources.ApplyResources(Me.butDelClass, "butDelClass")
        Me.butDelClass.Name = "butDelClass"
        Me.butDelClass.UseVisualStyleBackColor = True
        '
        'butReg
        '
        resources.ApplyResources(Me.butReg, "butReg")
        Me.butReg.Name = "butReg"
        Me.butReg.UseVisualStyleBackColor = True
        '
        'dgvClass
        '
        Me.dgvClass.AllowUserToAddRows = False
        Me.dgvClass.AllowUserToDeleteRows = False
        Me.dgvClass.AllowUserToResizeRows = False
        Me.dgvClass.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvClass.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvClass, "dgvClass")
        Me.dgvClass.MultiSelect = False
        Me.dgvClass.Name = "dgvClass"
        Me.dgvClass.ReadOnly = True
        Me.dgvClass.RowHeadersVisible = False
        Me.dgvClass.RowTemplate.Height = 24
        Me.dgvClass.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Panel1)
        resources.ApplyResources(Me.GroupBox6, "GroupBox6")
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.TabStop = False
        '
        'Panel1
        '
        resources.ApplyResources(Me.Panel1, "Panel1")
        Me.Panel1.Controls.Add(Me.tboxRemark4)
        Me.Panel1.Controls.Add(Me.tboxRemark3)
        Me.Panel1.Controls.Add(Me.tboxRemark2)
        Me.Panel1.Controls.Add(Me.tboxRemark1)
        Me.Panel1.Controls.Add(Me.tboxRank4)
        Me.Panel1.Controls.Add(Me.tboxRank3)
        Me.Panel1.Controls.Add(Me.tboxRank2)
        Me.Panel1.Controls.Add(Me.tboxRank1)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.tboxSchool4)
        Me.Panel1.Controls.Add(Me.cboxFamily4)
        Me.Panel1.Controls.Add(Me.tboxBirth4d)
        Me.Panel1.Controls.Add(Me.tboxBirth4m)
        Me.Panel1.Controls.Add(Me.tboxBirth4y)
        Me.Panel1.Controls.Add(Me.tboxName4)
        Me.Panel1.Controls.Add(Me.tboxSchool3)
        Me.Panel1.Controls.Add(Me.cboxFamily3)
        Me.Panel1.Controls.Add(Me.tboxBirth3d)
        Me.Panel1.Controls.Add(Me.tboxBirth3m)
        Me.Panel1.Controls.Add(Me.tboxBirth3y)
        Me.Panel1.Controls.Add(Me.tboxName3)
        Me.Panel1.Controls.Add(Me.tboxSchool2)
        Me.Panel1.Controls.Add(Me.cboxFamily2)
        Me.Panel1.Controls.Add(Me.tboxBirth2d)
        Me.Panel1.Controls.Add(Me.tboxBirth2m)
        Me.Panel1.Controls.Add(Me.tboxBirth2y)
        Me.Panel1.Controls.Add(Me.tboxName2)
        Me.Panel1.Controls.Add(Me.tboxSchool1)
        Me.Panel1.Controls.Add(Me.Label40)
        Me.Panel1.Controls.Add(Me.cboxFamily1)
        Me.Panel1.Controls.Add(Me.tboxBirth1d)
        Me.Panel1.Controls.Add(Me.tboxBirth1m)
        Me.Panel1.Controls.Add(Me.tboxBirth1y)
        Me.Panel1.Controls.Add(Me.Label37)
        Me.Panel1.Controls.Add(Me.tboxName1)
        Me.Panel1.Controls.Add(Me.Label38)
        Me.Panel1.Controls.Add(Me.Label39)
        Me.Panel1.Name = "Panel1"
        '
        'tboxRemark4
        '
        resources.ApplyResources(Me.tboxRemark4, "tboxRemark4")
        Me.tboxRemark4.Name = "tboxRemark4"
        '
        'tboxRemark3
        '
        resources.ApplyResources(Me.tboxRemark3, "tboxRemark3")
        Me.tboxRemark3.Name = "tboxRemark3"
        '
        'tboxRemark2
        '
        resources.ApplyResources(Me.tboxRemark2, "tboxRemark2")
        Me.tboxRemark2.Name = "tboxRemark2"
        '
        'tboxRemark1
        '
        resources.ApplyResources(Me.tboxRemark1, "tboxRemark1")
        Me.tboxRemark1.Name = "tboxRemark1"
        '
        'tboxRank4
        '
        resources.ApplyResources(Me.tboxRank4, "tboxRank4")
        Me.tboxRank4.Name = "tboxRank4"
        '
        'tboxRank3
        '
        resources.ApplyResources(Me.tboxRank3, "tboxRank3")
        Me.tboxRank3.Name = "tboxRank3"
        '
        'tboxRank2
        '
        resources.ApplyResources(Me.tboxRank2, "tboxRank2")
        Me.tboxRank2.Name = "tboxRank2"
        '
        'tboxRank1
        '
        resources.ApplyResources(Me.tboxRank1, "tboxRank1")
        Me.tboxRank1.Name = "tboxRank1"
        '
        'Label4
        '
        resources.ApplyResources(Me.Label4, "Label4")
        Me.Label4.Name = "Label4"
        '
        'Label2
        '
        resources.ApplyResources(Me.Label2, "Label2")
        Me.Label2.Name = "Label2"
        '
        'tboxSchool4
        '
        resources.ApplyResources(Me.tboxSchool4, "tboxSchool4")
        Me.tboxSchool4.Name = "tboxSchool4"
        '
        'cboxFamily4
        '
        Me.cboxFamily4.FormattingEnabled = True
        resources.ApplyResources(Me.cboxFamily4, "cboxFamily4")
        Me.cboxFamily4.Name = "cboxFamily4"
        '
        'tboxBirth4d
        '
        resources.ApplyResources(Me.tboxBirth4d, "tboxBirth4d")
        Me.tboxBirth4d.Name = "tboxBirth4d"
        '
        'tboxBirth4m
        '
        resources.ApplyResources(Me.tboxBirth4m, "tboxBirth4m")
        Me.tboxBirth4m.Name = "tboxBirth4m"
        '
        'tboxBirth4y
        '
        resources.ApplyResources(Me.tboxBirth4y, "tboxBirth4y")
        Me.tboxBirth4y.Name = "tboxBirth4y"
        '
        'tboxName4
        '
        resources.ApplyResources(Me.tboxName4, "tboxName4")
        Me.tboxName4.Name = "tboxName4"
        '
        'tboxSchool3
        '
        resources.ApplyResources(Me.tboxSchool3, "tboxSchool3")
        Me.tboxSchool3.Name = "tboxSchool3"
        '
        'cboxFamily3
        '
        Me.cboxFamily3.FormattingEnabled = True
        resources.ApplyResources(Me.cboxFamily3, "cboxFamily3")
        Me.cboxFamily3.Name = "cboxFamily3"
        '
        'tboxBirth3d
        '
        resources.ApplyResources(Me.tboxBirth3d, "tboxBirth3d")
        Me.tboxBirth3d.Name = "tboxBirth3d"
        '
        'tboxBirth3m
        '
        resources.ApplyResources(Me.tboxBirth3m, "tboxBirth3m")
        Me.tboxBirth3m.Name = "tboxBirth3m"
        '
        'tboxBirth3y
        '
        resources.ApplyResources(Me.tboxBirth3y, "tboxBirth3y")
        Me.tboxBirth3y.Name = "tboxBirth3y"
        '
        'tboxName3
        '
        resources.ApplyResources(Me.tboxName3, "tboxName3")
        Me.tboxName3.Name = "tboxName3"
        '
        'tboxSchool2
        '
        resources.ApplyResources(Me.tboxSchool2, "tboxSchool2")
        Me.tboxSchool2.Name = "tboxSchool2"
        '
        'cboxFamily2
        '
        Me.cboxFamily2.FormattingEnabled = True
        resources.ApplyResources(Me.cboxFamily2, "cboxFamily2")
        Me.cboxFamily2.Name = "cboxFamily2"
        '
        'tboxBirth2d
        '
        resources.ApplyResources(Me.tboxBirth2d, "tboxBirth2d")
        Me.tboxBirth2d.Name = "tboxBirth2d"
        '
        'tboxBirth2m
        '
        resources.ApplyResources(Me.tboxBirth2m, "tboxBirth2m")
        Me.tboxBirth2m.Name = "tboxBirth2m"
        '
        'tboxBirth2y
        '
        resources.ApplyResources(Me.tboxBirth2y, "tboxBirth2y")
        Me.tboxBirth2y.Name = "tboxBirth2y"
        '
        'tboxName2
        '
        resources.ApplyResources(Me.tboxName2, "tboxName2")
        Me.tboxName2.Name = "tboxName2"
        '
        'tboxSchool1
        '
        resources.ApplyResources(Me.tboxSchool1, "tboxSchool1")
        Me.tboxSchool1.Name = "tboxSchool1"
        '
        'Label40
        '
        resources.ApplyResources(Me.Label40, "Label40")
        Me.Label40.Name = "Label40"
        '
        'cboxFamily1
        '
        Me.cboxFamily1.FormattingEnabled = True
        resources.ApplyResources(Me.cboxFamily1, "cboxFamily1")
        Me.cboxFamily1.Name = "cboxFamily1"
        '
        'tboxBirth1d
        '
        resources.ApplyResources(Me.tboxBirth1d, "tboxBirth1d")
        Me.tboxBirth1d.Name = "tboxBirth1d"
        '
        'tboxBirth1m
        '
        resources.ApplyResources(Me.tboxBirth1m, "tboxBirth1m")
        Me.tboxBirth1m.Name = "tboxBirth1m"
        '
        'tboxBirth1y
        '
        resources.ApplyResources(Me.tboxBirth1y, "tboxBirth1y")
        Me.tboxBirth1y.Name = "tboxBirth1y"
        '
        'Label37
        '
        resources.ApplyResources(Me.Label37, "Label37")
        Me.Label37.Name = "Label37"
        '
        'tboxName1
        '
        resources.ApplyResources(Me.tboxName1, "tboxName1")
        Me.tboxName1.Name = "tboxName1"
        '
        'Label38
        '
        resources.ApplyResources(Me.Label38, "Label38")
        Me.Label38.Name = "Label38"
        '
        'Label39
        '
        resources.ApplyResources(Me.Label39, "Label39")
        Me.Label39.Name = "Label39"
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.rtboxRemarks)
        resources.ApplyResources(Me.GroupBox7, "GroupBox7")
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.TabStop = False
        '
        'rtboxRemarks
        '
        resources.ApplyResources(Me.rtboxRemarks, "rtboxRemarks")
        Me.rtboxRemarks.Name = "rtboxRemarks"
        '
        'butClose
        '
        resources.ApplyResources(Me.butClose, "butClose")
        Me.butClose.Name = "butClose"
        Me.butClose.UseVisualStyleBackColor = True
        '
        'butSave
        '
        resources.ApplyResources(Me.butSave, "butSave")
        Me.butSave.Name = "butSave"
        Me.butSave.UseVisualStyleBackColor = True
        '
        'GroupBox8
        '
        Me.GroupBox8.Controls.Add(Me.dgvOldRec)
        resources.ApplyResources(Me.GroupBox8, "GroupBox8")
        Me.GroupBox8.Name = "GroupBox8"
        Me.GroupBox8.TabStop = False
        '
        'dgvOldRec
        '
        Me.dgvOldRec.AllowUserToAddRows = False
        Me.dgvOldRec.AllowUserToDeleteRows = False
        Me.dgvOldRec.AllowUserToResizeRows = False
        Me.dgvOldRec.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvOldRec, "dgvOldRec")
        Me.dgvOldRec.Name = "dgvOldRec"
        Me.dgvOldRec.ReadOnly = True
        Me.dgvOldRec.RowHeadersVisible = False
        Me.dgvOldRec.RowTemplate.Height = 24
        Me.dgvOldRec.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        '
        'frmAddStu
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.Controls.Add(Me.GroupBox8)
        Me.Controls.Add(Me.butClose)
        Me.Controls.Add(Me.butSave)
        Me.Controls.Add(Me.GroupBox7)
        Me.Controls.Add(Me.GroupBox6)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmAddStu"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.NumUDStuYear, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvClass, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox7.ResumeLayout(False)
        Me.GroupBox8.ResumeLayout(False)
        CType(Me.dgvOldRec, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents radbutPotStu As System.Windows.Forms.RadioButton
    Friend WithEvents radbutFormalStu As System.Windows.Forms.RadioButton
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents tboxStuName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblStuId As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxBirthY As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tboxEngName As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tboxBirthD As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirthM As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents butPri As System.Windows.Forms.Button
    Friend WithEvents cboxPrimarySch As System.Windows.Forms.ComboBox
    Friend WithEvents radbutNowJun As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNowPri As System.Windows.Forms.RadioButton
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents radbutNowUni As System.Windows.Forms.RadioButton
    Friend WithEvents radbutNowHig As System.Windows.Forms.RadioButton
    Friend WithEvents tboxClass As System.Windows.Forms.TextBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents cboxGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents cboxGrade As System.Windows.Forms.ComboBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents butUni As System.Windows.Forms.Button
    Friend WithEvents cboxUniversity As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents butHig As System.Windows.Forms.Button
    Friend WithEvents cboxHighSch As System.Windows.Forms.ComboBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents butJun As System.Windows.Forms.Button
    Friend WithEvents cboxJuniorSch As System.Windows.Forms.ComboBox
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tboxOfficeTel As System.Windows.Forms.TextBox
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents tboxTel2 As System.Windows.Forms.TextBox
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tboxAdd1a As System.Windows.Forms.TextBox
    Friend WithEvents tboxEmail As System.Windows.Forms.TextBox
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents tboxAdd2a As System.Windows.Forms.TextBox
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents tboxIntroId As System.Windows.Forms.TextBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents tboxMobile As System.Windows.Forms.TextBox
    Friend WithEvents tboxDadMobile As System.Windows.Forms.TextBox
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents tboxDadName As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents tboxTel1 As System.Windows.Forms.TextBox
    Friend WithEvents tboxAdd1b As System.Windows.Forms.TextBox
    Friend WithEvents tboxAdd2b As System.Windows.Forms.TextBox
    Friend WithEvents tboxClassId As System.Windows.Forms.TextBox
    Friend WithEvents tboxStay As System.Windows.Forms.TextBox
    Friend WithEvents Label32 As System.Windows.Forms.Label
    Friend WithEvents Label33 As System.Windows.Forms.Label
    Friend WithEvents tboxDadTitle As System.Windows.Forms.TextBox
    Friend WithEvents tboxRegInfo As System.Windows.Forms.TextBox
    Friend WithEvents Label34 As System.Windows.Forms.Label
    Friend WithEvents tboxMumTitle As System.Windows.Forms.TextBox
    Friend WithEvents Label35 As System.Windows.Forms.Label
    Friend WithEvents Label36 As System.Windows.Forms.Label
    Friend WithEvents tboxMumMobile As System.Windows.Forms.TextBox
    Friend WithEvents tboxIntroName As System.Windows.Forms.TextBox
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents Label28 As System.Windows.Forms.Label
    Friend WithEvents tboxGradJunior As System.Windows.Forms.TextBox
    Friend WithEvents tboxMumName As System.Windows.Forms.TextBox
    Friend WithEvents Label29 As System.Windows.Forms.Label
    Friend WithEvents tboxIc As System.Windows.Forms.TextBox
    Friend WithEvents Label30 As System.Windows.Forms.Label
    Friend WithEvents Label31 As System.Windows.Forms.Label
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvClass As System.Windows.Forms.DataGridView
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents rtboxRemarks As System.Windows.Forms.RichTextBox
    Friend WithEvents butSales As System.Windows.Forms.Button
    Friend WithEvents butSeatNum As System.Windows.Forms.Button
    Friend WithEvents butDelClass As System.Windows.Forms.Button
    Friend WithEvents butReg As System.Windows.Forms.Button
    Friend WithEvents butClose As System.Windows.Forms.Button
    Friend WithEvents butSave As System.Windows.Forms.Button
    Friend WithEvents lblStuYear As System.Windows.Forms.Label
    Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvOldRec As System.Windows.Forms.DataGridView
    Friend WithEvents cboxSex As System.Windows.Forms.ComboBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents tboxRemark4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRemark1 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxRank1 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tboxSchool4 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily4 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth4d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth4m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth4y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName4 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool3 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily3 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth3d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth3m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth3y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName3 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool2 As System.Windows.Forms.TextBox
    Friend WithEvents cboxFamily2 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth2d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth2m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth2y As System.Windows.Forms.TextBox
    Friend WithEvents tboxName2 As System.Windows.Forms.TextBox
    Friend WithEvents tboxSchool1 As System.Windows.Forms.TextBox
    Friend WithEvents Label40 As System.Windows.Forms.Label
    Friend WithEvents cboxFamily1 As System.Windows.Forms.ComboBox
    Friend WithEvents tboxBirth1d As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth1m As System.Windows.Forms.TextBox
    Friend WithEvents tboxBirth1y As System.Windows.Forms.TextBox
    Friend WithEvents Label37 As System.Windows.Forms.Label
    Friend WithEvents tboxName1 As System.Windows.Forms.TextBox
    Friend WithEvents Label38 As System.Windows.Forms.Label
    Friend WithEvents Label39 As System.Windows.Forms.Label
    Friend WithEvents tboxPunchCardNum As System.Windows.Forms.TextBox
    Friend WithEvents Label43 As System.Windows.Forms.Label
    Friend WithEvents tboxEnrollSch As System.Windows.Forms.TextBox
    Friend WithEvents Label42 As System.Windows.Forms.Label
    Friend WithEvents tboxHseeMark As System.Windows.Forms.TextBox
    Friend WithEvents Label41 As System.Windows.Forms.Label
    Friend WithEvents NumUDStuYear As System.Windows.Forms.NumericUpDown
End Class
