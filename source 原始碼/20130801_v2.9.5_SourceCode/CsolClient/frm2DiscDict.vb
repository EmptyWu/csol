﻿Public Class frm2DiscDict
    Private dtType As New DataTable
    Private dtDict As New DataTable

    Public Sub New()

        InitializeComponent()

        RefreshData()
        RefreshLst()
        frmMain.SetOkState(False)
        frmMain.SetCurrentString("")
        frmMain.SetCurrentValue(0)

    End Sub

    Private Sub RefreshData()
        dtDict = frmMain.GetDiscountDict
    End Sub

    Private Sub RefreshLst()
        dtType = frmMain.GetDiscountDictType
        cboxType.Items.Clear()
        cboxType.Items.Add(My.Resources.all)

        For index As Integer = 0 To dtType.Rows.Count - 1
            '20100408 sherry start
            cboxType.Items.Add(dtType.Rows(index).Item(c_IDColumnName).ToString & _
                               " " & dtType.Rows(index).Item(c_TypeColumnName).ToString.Trim)
            '20100408 sherry end
        Next

        cboxType.SelectedIndex = 0
        dtDict.DefaultView.RowFilter = ""
        dgv.DataSource = dtDict
        LoadColumnText()
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub


    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
        If Not frmMain.CheckAuth(32) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If Not frmMain.CheckAuth(33) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        Try
            dtDict.DefaultView.RowFilter = ""
            dgv.DataSource = dtDict

            objCsol.UpdateDiscDict((CType(dgv.DataSource, DataTable)).GetChanges)
            frmMain.RefreshDictInfo()
            MsgBox(My.Resources.msgSaveComplete, _
                                MsgBoxStyle.OkOnly, My.Resources.msgRemindTitle)
            Me.Close()

        Catch ex As Exception
            MessageBox.Show(My.Resources.msgSaveIncomplete & " " & ex.Message, My.Resources.msgRemindTitle, MessageBoxButtons.OK)
        End Try

    End Sub

    Private Sub butDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butDelete.Click
        If Not frmMain.CheckAuth(34) Then
            frmMain.ShowNoAuthMsg()
            Exit Sub
        End If
        If dgv.SelectedRows.Count > 0 Then
            Try
                dgv.Rows.Remove(dgv.SelectedRows(0))
            Catch ex As Exception
            End Try

        End If
    End Sub

    Private Sub butSelect_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSelect.Click
        If dgv.SelectedRows.Count > 0 Then
            Dim i As Integer = dgv.SelectedRows(0).Cells(c_AmountColumnName).Value
            Dim r As String = dgv.SelectedRows(0).Cells(c_ReasonColumnName).Value
            If i > 0 Or Not r = "" Then
                frmMain.SetCurrentString(r)
                frmMain.SetCurrentValue(i)
                frmMain.SetOkState(True)
                Me.Close()
            End If
        End If
    End Sub

    Private Sub cboxType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboxType.SelectedIndexChanged
        If dtDict.Rows.Count > 0 And dtType.Rows.Count > 0 And cboxType.SelectedIndex <= dtType.Rows.Count Then
            If cboxType.SelectedIndex > 0 Then
                dtDict.DefaultView.RowFilter = c_TypeIdColumnName & "=" & _
                        dtType.Rows(cboxType.SelectedIndex - 1).Item(c_IDColumnName).ToString
            ElseIf cboxType.SelectedIndex = 0 Then
                dtDict.DefaultView.RowFilter = ""
            End If

            dgv.DataSource = dtDict.DefaultView
            LoadColumnText()
        End If
    End Sub

    Private Sub LoadColumnText()
        If dgv.Columns.Count > 0 Then
            dgv.Columns.Item(c_IDColumnName).DisplayIndex = 0
            dgv.Columns.Item(c_AmountColumnName).DisplayIndex = 1
            dgv.Columns.Item(c_AmountColumnName).HeaderText = My.Resources.amount
            dgv.Columns.Item(c_ReasonColumnName).DisplayIndex = 2
            dgv.Columns.Item(c_ReasonColumnName).HeaderText = My.Resources.reason
            dgv.Columns.Item(c_TypeIdColumnName).DisplayIndex = 3
            dgv.Columns.Item(c_TypeIdColumnName).HeaderText = My.Resources.typeID
        End If
    End Sub

    Private Sub butSetType_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles butSetType.Click
        Dim frm As New frm2DiscDictType
        frm.ShowDialog()
        If frmMain.GetOkState Then
            RefreshLst()
            frmMain.SetOkState(False)
        End If
    End Sub
End Class