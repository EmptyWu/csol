﻿Public Class frm2AddMBRemarks
    Private intId As Integer
    Private strRemarks As String

    Public Sub New(ByVal ID As Integer)

        InitializeComponent()
        intId = ID
        frmMain.SetOkState(False)
    End Sub

    Private Sub butCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butCancel.Click
        Me.Close()
    End Sub

    Private Sub butSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles butSave.Click
       
        objCsol.AddMBRemarks(intId, tboxRemarks.Text)
        frmMain.SetOkState(True)
        Me.Close()
    End Sub

End Class