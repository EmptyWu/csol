﻿Option Strict Off

Public Class clsExcelReport

    Private Function CreateRecordsetFromDataGrid(ByVal DGV As DataGridView) As ADODB.Recordset

        Dim rs As New ADODB.Recordset
        Dim strP As String = " "

        'Create columns in ADODB.Recordset

        Dim FieldAttr As ADODB.FieldAttributeEnum

        FieldAttr = ADODB.FieldAttributeEnum.adFldIsNullable Or ADODB.FieldAttributeEnum.adFldIsNullable Or ADODB.FieldAttributeEnum.adFldUpdatable

        For Each iColumn As DataGridViewColumn In DGV.Columns

            'only add Visible columns

            If iColumn.Visible = True Then

                Dim FieldType As ADODB.DataTypeEnum

                'select dataType

                If iColumn.ValueType Is GetType(Boolean) Then

                    FieldType = ADODB.DataTypeEnum.adBoolean

                ElseIf iColumn.ValueType Is GetType(Byte) Then

                    FieldType = ADODB.DataTypeEnum.adTinyInt

                ElseIf iColumn.ValueType Is GetType(Int16) Then

                    FieldType = ADODB.DataTypeEnum.adSmallInt

                ElseIf iColumn.ValueType Is GetType(Int32) Then

                    FieldType = ADODB.DataTypeEnum.adInteger

                ElseIf iColumn.ValueType Is GetType(Int64) Then

                    FieldType = ADODB.DataTypeEnum.adBigInt

                ElseIf iColumn.ValueType Is GetType(Single) Then

                    FieldType = ADODB.DataTypeEnum.adSingle

                ElseIf iColumn.ValueType Is GetType(Double) Then

                    FieldType = ADODB.DataTypeEnum.adDouble

                ElseIf iColumn.ValueType Is GetType(Decimal) Then

                    FieldType = ADODB.DataTypeEnum.adCurrency

                ElseIf iColumn.ValueType Is GetType(DateTime) Then

                    FieldType = ADODB.DataTypeEnum.adDBDate

                ElseIf iColumn.ValueType Is GetType(Char) Then

                    FieldType = ADODB.DataTypeEnum.adChar

                ElseIf iColumn.ValueType Is GetType(String) Then

                    FieldType = ADODB.DataTypeEnum.adVarWChar
                Else
                    FieldType = ADODB.DataTypeEnum.adVarWChar

                End If

                If FieldType = ADODB.DataTypeEnum.adVarWChar Then
                    If iColumn.Name = Nothing Then
                        rs.Fields.Append(strP, FieldType, 200)  'modified by owen 0114
                        strP = strP & " " 'modified by owen 0114
                    Else
                        rs.Fields.Append(iColumn.Name, FieldType, 200)
                    End If


                Else
                    If iColumn.Name = Nothing Then
                        rs.Fields.Append(" ", FieldType)
                    Else
                        rs.Fields.Append(iColumn.Name, FieldType)
                    End If

                End If

                If iColumn.Name = Nothing Then
                    'do nothing here
                Else
                    rs.Fields(iColumn.Name).Attributes = FieldAttr
                End If
            End If

        Next

        'Opens the ADODB.Recordset

        rs.Open()

        'Inserts rows into the recordset

        For Each iRow As DataGridViewRow In DGV.Rows

            rs.AddNew()

            For Each iColumn As DataGridViewColumn In DGV.Columns

                'only add values for Visible columns

                If iColumn.Visible = True Then
                    If Not DBNull.Value.Equals(iRow.Cells(iColumn.Name).Value) Then
                        If Not iRow.Cells(iColumn.Name).Value = Nothing Then
                            If iRow.Cells(iColumn.Name).Value.ToString = "" Then
                                If (rs(iColumn.Name).Attributes And ADODB.FieldAttributeEnum.adFldIsNullable) <> 0 Then
                                    rs(iColumn.Name).Value = DBNull.Value
                                End If
                            Else
                                rs(iColumn.Name).Value = iRow.Cells(iColumn.Name).Value
                            End If
                        End If
                        'Else
                        '    'If iColumn.ValueType Is GetType(Int32) Or iColumn.ValueType Is GetType(Int16) Or _
                        '    '    iColumn.ValueType Is GetType(Int64) Or iColumn.ValueType Is GetType(Byte) Or _
                        '    '    iColumn.ValueType Is GetType(Single) Or iColumn.ValueType Is GetType(Double) Or _
                        '    '    iColumn.ValueType Is GetType(Decimal) Then
                        '    rs(iColumn.Name).Value = 0
                        'End If
                    End If
                End If
            Next
        Next

        'Moves to the first record in recordset

        If Not rs.BOF Then rs.MoveFirst()

        Return rs

    End Function

    Public Sub openExcelReport(ByRef DGV As DataGridView, _
            Optional ByVal bolSave As Boolean = False, _
            Optional ByVal bolOpen As Boolean = True)
        Try

            Dim xlApp As New Microsoft.Office.Interop.Excel.Application

            Dim xlBook As Microsoft.Office.Interop.Excel.Workbook

            Dim xlSheet As Microsoft.Office.Interop.Excel.Worksheet

            Dim rs As New ADODB.Recordset

            Dim xlrow As Integer

            Dim strColType() As String

            'Try

            'opening connections to excel

            xlBook = xlApp.Workbooks.Add

            xlSheet = xlBook.Worksheets.Add()

            If bolOpen = True Then

                xlApp.Application.Visible = True

            End If

            xlrow = 1

            'Try

            xlSheet.Columns.HorizontalAlignment = 2

            'formating the output of the report

            xlSheet.Columns.Font.Name = "Times New Roman"

            xlSheet.Rows.Item(xlrow).Font.Bold = 1

            xlSheet.Rows.Item(xlrow).Interior.ColorIndex = 15

            rs = CreateRecordsetFromDataGrid(DGV)

            If rs.State = 0 Then rs.Open()

            'Catch

            '    GoTo PROC_EXIT

            'End Try

            ReDim strColType(rs.Fields.Count)

            For j As Integer = 0 To rs.Fields.Count - 1

                xlSheet.Cells.Item(xlrow, j + 1) = rs.Fields.Item(j).Name

                xlSheet.Cells(xlrow, j + 1).BorderAround(1, ColorIndex:=16, Weight:=2)

            Next j

            'This does a simple test to see if the of excel held by the user is 2000 or later

            'This is needed because "CopyFromRecordset" only works with excel 2000 or later

            xlrow = 2

            If Val(Mid(xlApp.Version, 1, InStr(1, xlApp.Version, ".") - 1)) > 8 Then

                xlSheet.Range("A" & xlrow).CopyFromRecordset(rs)

            Else

                MessageBox.Show("You must use excel 2000 or above, opperation can not continue", "Excel Report", _
    MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)

                'GoTo PROC_EXIT

            End If

            Dim strSearch As String = ""

            Dim strTemp As String = ""

            For j As Integer = 0 To rs.Fields.Count - 1

                'debug.Print(rs.Fields.Item(j).Name)

                'debug.Print(rs.Fields.Item(j).Type.ToString) 'finds out the format of the field

                If rs.Fields.Item(j).Type.ToString = "adBigInt" Then 'these if statements are used to select which format to use

                    strSearch = "0;[Red]0"

                ElseIf rs.Fields.Item(j).Type.ToString = "adVarChar" Then

                    strSearch = "@"

                ElseIf rs.Fields.Item(j).Type.ToString = "adDouble" Then 'type float/double

                    strSearch = "0.00_ ;[Red]-0.00"

                ElseIf rs.Fields.Item(j).Type.ToString = "adCurrency" Then 'money

                    strTemp = j & " " & strTemp 'used to keep track of currency for sum

                    strSearch = "€#,##0.00;[Red]-€#,##0.00"

                ElseIf rs.Fields.Item(j).Type.ToString = "11" Then

                    strSearch = "@"

                ElseIf rs.Fields.Item(j).Type.ToString = "adDBTimeStamp" Then 'type date/time

                    If rs.Fields.Item(j).Name.Contains("Date") = True Then

                        strSearch = "[$-1809]ddd, dd-mmm-yyyy;@"

                    Else

                        strSearch = "h:mm AM/PM"

                    End If

                Else

                    strSearch = "@"

                End If

                xlSheet.Columns(j + 1).NumberFormat = strSearch

            Next j

            If strTemp.Length > 0 Then

                Dim arrayStr() As String = Split(strTemp, " ")

                Dim X As Integer

                Dim lngLenght As Integer

                For i As Integer = UBound(arrayStr) To 0 Step -1

                    'Asc (UCase$(Chr(KeyAscii)))

                    lngLenght = InStr(1, strTemp, " ")

                    If lngLenght > 0 Then

                        X = CInt(Left(strTemp, lngLenght - 1))

                        lngLenght = Len(strTemp) - lngLenght

                        strTemp = Right(strTemp, lngLenght)

                    End If

                    'debug.Print X

                    If X >= 26 Then

                        X = X - 26

                        arrayStr(i) = "A" & Chr(X + 65)

                    Else

                        arrayStr(i) = Chr(X + 65)

                    End If

                    'debug.Print arrayStr(i)

                    xlSheet.Cells.Item(CInt(rs.RecordCount + 6), arrayStr(i)).Font.Bold = 1

                    xlSheet.Cells.Item(CInt(rs.RecordCount + 6), arrayStr(i)) = "=SUM(" & _
                    arrayStr(i) & xlrow & _
                    ":" & arrayStr(i) & CInt(rs.RecordCount + 4) & ")"

                Next i

            End If

            'some more formatting of the excel sheet. Thsi formats the way the

            'sheet looks in print preview

            xlSheet.PageSetup.LeftHeader = "&[Page]" & " of " & "&[Pages]"

            xlSheet.PageSetup.RightHeader = "&[Date]" & " &[Time]"

            xlSheet.PageSetup.HeaderMargin = 5

            xlSheet.PageSetup.BottomMargin = 5

            xlSheet.PageSetup.LeftMargin = 5

            xlSheet.PageSetup.RightMargin = 5

            xlSheet.PageSetup.TopMargin = 25

            xlSheet.PageSetup.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4
            xlSheet.PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape

            xlSheet.Columns.AutoFit()

            xlSheet.Rows.AutoFit()

            xlApp.UserControl = True

            If bolOpen = False Then

                xlApp.DisplayAlerts = False
                Dim saveFileDialog1 As New SaveFileDialog()

                saveFileDialog1.Filter = "excel files (*.xls)|*.xls|All files (*.*)|*.*"
                saveFileDialog1.FilterIndex = 1
                saveFileDialog1.RestoreDirectory = True

                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    xlBook.Close(True, saveFileDialog1.FileName)
                Else
                    xlBook.Close(False)
                End If
                xlApp.Application.Quit()

            ElseIf bolSave = True Then

                xlApp.DisplayAlerts = False
                Dim saveFileDialog1 As New SaveFileDialog()

                saveFileDialog1.Filter = "excel files (*.xls)|*.xls|All files (*.*)|*.*"
                saveFileDialog1.FilterIndex = 1
                saveFileDialog1.RestoreDirectory = True

                If saveFileDialog1.ShowDialog() = DialogResult.OK Then
                    xlBook.SaveAs(saveFileDialog1.FileName)
                End If
                xlBook.Close()

                xlApp.DisplayAlerts = True

            End If

            '        Catch ex As Exception

            '        End Try

            'PROC_EXIT:

            '        xlSheet = Nothing

            '        xlBook = Nothing

            '        xlApp = Nothing
        Catch ex As Exception

        End Try

    End Sub

    

End Class

