﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSetClassRoom
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSetClassRoom))
        Me.mnuClose = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuModify = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvMaster = New System.Windows.Forms.DataGridView
        Me.mnuAdd = New System.Windows.Forms.ToolStripMenuItem
        Me.mnuDelete = New System.Windows.Forms.ToolStripMenuItem
        Me.mnustrTop = New System.Windows.Forms.MenuStrip
        Me.mnuPrint = New System.Windows.Forms.ToolStripMenuItem
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvDetails = New System.Windows.Forms.DataGridView
        Me.PrintDocument1 = New System.Drawing.Printing.PrintDocument
        Me.chkboxShowPast = New System.Windows.Forms.CheckBox
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnustrTop.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'mnuClose
        '
        Me.mnuClose.Name = "mnuClose"
        resources.ApplyResources(Me.mnuClose, "mnuClose")
        '
        'mnuModify
        '
        Me.mnuModify.Name = "mnuModify"
        resources.ApplyResources(Me.mnuModify, "mnuModify")
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvMaster)
        resources.ApplyResources(Me.GroupBox1, "GroupBox1")
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.TabStop = False
        '
        'dgvMaster
        '
        Me.dgvMaster.AllowUserToAddRows = False
        Me.dgvMaster.AllowUserToDeleteRows = False
        Me.dgvMaster.AllowUserToResizeRows = False
        Me.dgvMaster.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvMaster.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvMaster, "dgvMaster")
        Me.dgvMaster.MultiSelect = False
        Me.dgvMaster.Name = "dgvMaster"
        Me.dgvMaster.RowHeadersVisible = False
        Me.dgvMaster.RowTemplate.Height = 15
        Me.dgvMaster.RowTemplate.ReadOnly = True
        Me.dgvMaster.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvMaster.ShowCellToolTips = False
        Me.dgvMaster.ShowEditingIcon = False
        '
        'mnuAdd
        '
        Me.mnuAdd.Name = "mnuAdd"
        resources.ApplyResources(Me.mnuAdd, "mnuAdd")
        '
        'mnuDelete
        '
        Me.mnuDelete.Name = "mnuDelete"
        resources.ApplyResources(Me.mnuDelete, "mnuDelete")
        '
        'mnustrTop
        '
        Me.mnustrTop.AllowMerge = False
        Me.mnustrTop.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuAdd, Me.mnuModify, Me.mnuDelete, Me.mnuPrint, Me.mnuClose})
        resources.ApplyResources(Me.mnustrTop, "mnustrTop")
        Me.mnustrTop.Name = "mnustrTop"
        '
        'mnuPrint
        '
        Me.mnuPrint.Name = "mnuPrint"
        resources.ApplyResources(Me.mnuPrint, "mnuPrint")
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvDetails)
        resources.ApplyResources(Me.GroupBox2, "GroupBox2")
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.TabStop = False
        '
        'dgvDetails
        '
        Me.dgvDetails.AllowUserToAddRows = False
        Me.dgvDetails.AllowUserToDeleteRows = False
        Me.dgvDetails.AllowUserToResizeRows = False
        Me.dgvDetails.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells
        Me.dgvDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        resources.ApplyResources(Me.dgvDetails, "dgvDetails")
        Me.dgvDetails.MultiSelect = False
        Me.dgvDetails.Name = "dgvDetails"
        Me.dgvDetails.RowHeadersVisible = False
        Me.dgvDetails.RowTemplate.Height = 15
        Me.dgvDetails.RowTemplate.ReadOnly = True
        Me.dgvDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvDetails.ShowCellToolTips = False
        Me.dgvDetails.ShowEditingIcon = False
        '
        'PrintDocument1
        '
        '
        'chkboxShowPast
        '
        resources.ApplyResources(Me.chkboxShowPast, "chkboxShowPast")
        Me.chkboxShowPast.Name = "chkboxShowPast"
        Me.chkboxShowPast.UseVisualStyleBackColor = True
        '
        'frmSetClassRoom
        '
        resources.ApplyResources(Me, "$this")
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(219, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Controls.Add(Me.chkboxShowPast)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.mnustrTop)
        Me.Controls.Add(Me.GroupBox2)
        Me.Name = "frmSetClassRoom"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvMaster, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnustrTop.ResumeLayout(False)
        Me.mnustrTop.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvDetails, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuClose As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuModify As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents mnuAdd As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDelete As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnustrTop As System.Windows.Forms.MenuStrip
    Friend WithEvents mnuPrint As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvDetails As System.Windows.Forms.DataGridView
    Friend WithEvents dgvMaster As System.Windows.Forms.DataGridView
    Friend WithEvents PrintDocument1 As System.Drawing.Printing.PrintDocument
    Friend WithEvents chkboxShowPast As System.Windows.Forms.CheckBox
End Class
