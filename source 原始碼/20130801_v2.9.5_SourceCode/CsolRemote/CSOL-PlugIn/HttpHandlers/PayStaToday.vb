﻿Namespace HTTPHandlers
    Public Class PayStaToday
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection
        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)
            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetPayStaByDate"
                    GetPayStaByDate()
                Case "GetMKeepStaByDate"
                    GetMKeepStaByDate()
                Case "GetMBackStaByDate"
                    GetMBackStaByDate()

                Case "GetPayStaByHelper"
                    GetPayStaByHelper()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/PayStaToday"
            End Get
        End Property

        Private Sub GetPayStaByHelper()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""


            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))


            Dim GetPayStaByDate As DataTable = DataBaseAccess.PayStaToday.GetPayStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetPayStaByDate", CSOL.Convert.DataTableToXmlString(GetPayStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetPayStaByDate", ResponseBody)

            RequestParams.Clear()
            RequestBody = Nothing

            Dim GetMKeepStaByDate As DataTable = DataBaseAccess.PayStaToday.GetMKeepStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetMKeepStaByDate", CSOL.Convert.DataTableToXmlString(GetMKeepStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            zip.AddEntry("GetMKeepStaByDate", ResponseBody)

            RequestParams.Clear()
            RequestBody = Nothing

            Dim GetMBackStaByDate As DataTable = DataBaseAccess.PayStaToday.GetMBackStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetMBackStaByDate", CSOL.Convert.DataTableToXmlString(GetMBackStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            zip.AddEntry("GetMBackStaByDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)
        End Sub


        Private Sub GetPayStaByDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""


            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))

            Dim GetPayStaByDate As DataTable = DataBaseAccess.PayStaToday.GetPayStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetPayStaByDate", CSOL.Convert.DataTableToXmlString(GetPayStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetPayStaByDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)

        End Sub

        Private Sub GetMKeepStaByDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""


            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))

            Dim GetMKeepStaByDate As DataTable = DataBaseAccess.PayStaToday.GetMKeepStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetMKeepStaByDate", CSOL.Convert.DataTableToXmlString(GetMKeepStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetMKeepStaByDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)

        End Sub


        Private Sub GetMBackStaByDate()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""


            Dim DateFrom As Date = Date.Parse(RequestParams("DateFrom"))
            Dim DateTo As Date = Date.Parse(RequestParams("DateTo"))

            Dim GetMBackStaByDate As DataTable = DataBaseAccess.PayStaToday.GetMBackStaByDate(DateFrom, DateTo)
            ResponseParams.Add("Status", "OK")
            ResponseParams.Add("GetMBackStaByDate", CSOL.Convert.DataTableToXmlString(GetMBackStaByDate))
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Me.m_Context.Response.StatusCode = StatusCode
            Dim zip As New Ionic.Zip.ZipFile()
            zip.AddEntry("GetMBackStaByDate", ResponseBody)
            zip.Save(Me.m_Context.Response.OutputStream)

        End Sub
    End Class
End Namespace
