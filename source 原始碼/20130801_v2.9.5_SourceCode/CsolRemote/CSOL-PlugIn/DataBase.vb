﻿Namespace CSOL
    Public Class DataBase
        Public Shared ReadOnly Property ConnectionString() As String
            Get
                Dim cn As String = SearchMySettings(String.Format("userSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "ConnectionString")
                If cn <> "" Then
                    Return cn
                End If

                cn = SearchMySettings(String.Format("applicationSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "ConnectionString")
                If cn <> "" Then
                    Return cn
                End If

                Dim cns As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString")
                If cns IsNot Nothing Then
                    Return cns.ConnectionString
                Else
                    Return System.Configuration.ConfigurationManager.AppSettings("ConnectionString")
                End If
            End Get
        End Property

        Public Shared ReadOnly Property ConnectionStringImage() As String
            Get
                Dim cn As String = SearchMySettings(String.Format("userSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "ConnectionStringImg")
                If cn <> "" Then
                    Return cn
                End If

                cn = SearchMySettings(String.Format("applicationSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "ConnectionStringImg")
                If cn <> "" Then
                    Return cn
                End If

                Dim cns As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString")
                If cns IsNot Nothing Then
                    Return cns.ConnectionString
                Else
                    Return System.Configuration.ConfigurationManager.AppSettings("ConnectionStringImg")
                End If
            End Get
        End Property

        Public Shared ReadOnly Property AutoUpdPath() As String
            Get
                Dim cn As String = SearchMySettings(String.Format("userSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "autoupdpath")
                If cn <> "" Then
                    Return cn
                End If

                cn = SearchMySettings(String.Format("applicationSettings/{0}.My.MySettings", My.Application.Info.AssemblyName), "autoupdpath")
                If cn <> "" Then
                    Return cn
                End If

                Dim cns As System.Configuration.ConnectionStringSettings = System.Configuration.ConfigurationManager.ConnectionStrings("autoupdpath")
                If cns IsNot Nothing Then
                    Return cns.ConnectionString
                Else
                    Return System.Configuration.ConfigurationManager.AppSettings("autoupdpath")
                End If
            End Get
        End Property

        Private Shared Function SearchMySettings(ByVal xmlPath As String, ByVal key As String) As String
            Dim result As String = ""
            Dim section As Object = System.Configuration.ConfigurationManager.GetSection(xmlPath)
            If section IsNot Nothing Then
                Dim settings As System.Configuration.SettingElementCollection = CType(section, System.Configuration.ClientSettingsSection).Settings
                For Each element As System.Configuration.SettingElement In settings
                    If element.Name = key Then
                        result = element.Value.ValueXml.InnerText
                    End If
                Next
            End If
            Return result
        End Function

        Public Shared Sub ExecSql(ByVal sql As String)
            Using conn As New System.Data.SqlClient.SqlConnection(DataBase.ConnectionString)
                conn.Open()
                Dim cmd As System.Data.SqlClient.SqlCommand = conn.CreateCommand()
                cmd.Connection = conn
                cmd.CommandTimeout = 300

                Try
                    cmd.CommandText = sql
                    cmd.ExecuteNonQuery()
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                    Throw ex
                End Try
            End Using
        End Sub

        Public Shared Sub ExecSql(ByVal sql() As String)
            Using conn As New System.Data.SqlClient.SqlConnection(DataBase.ConnectionString)
                conn.Open()
                Dim cmd As System.Data.SqlClient.SqlCommand = conn.CreateCommand()
                Dim trans As System.Data.SqlClient.SqlTransaction = conn.BeginTransaction()
                cmd.Connection = conn
                cmd.Transaction = trans
                cmd.CommandTimeout = 300

                Try
                    For Each commandtext As String In sql
                        cmd.CommandText = commandtext
                        cmd.ExecuteNonQuery()
                    Next

                    trans.Commit()
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                    Try
                        trans.Rollback()
                    Catch ex2 As Exception
                        System.Diagnostics.Debug.WriteLine(ex2.ToString())
                        Throw ex2
                    End Try
                    Throw ex
                End Try
            End Using
        End Sub

        Public Shared Sub ExecSql(ByVal sql As List(Of String))
            ExecSql(sql.ToArray())
        End Sub

        Public Shared Sub ExecSql(ByVal sql As ArrayList)
            ExecSql(sql.ToArray(GetType(String)))
        End Sub

        Public Shared Function LoadToDataTable(ByVal sql As String) As System.Data.DataTable
            Dim ds As DataSet = DataBase.LoadToDataSet(sql)
            If ds.Tables.Count > 0 Then
                Return ds.Tables(0)
            Else
                Return Nothing
            End If
        End Function

        Public Shared Function LoadToDataSet(ByVal sql As String) As System.Data.DataSet
            Dim ds As New System.Data.DataSet()
            'sql = "SELECT * FROM StuInfo"

            Try
                Dim da As New System.Data.SqlClient.SqlDataAdapter(sql, DataBase.ConnectionString)
                da.SelectCommand.CommandTimeout = 300
                da.Fill(ds)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Throw ex
            End Try

            Return ds
        End Function
    End Class
End Namespace