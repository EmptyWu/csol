﻿Imports System.Data.SqlClient
Imports System.Configuration

Friend Class StuInfo
    Private Function GetConnectionString() As String
        '        Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    Public Sub AddStudent(ByVal ID As String, ByVal Name As String, ByVal EngName As String, ByVal Sex As String, _
                          ByVal Birthday As Date, ByVal IC As String, ByVal PWD As String, ByVal Address As String, _
                          ByVal PostalCode As String, ByVal Address2 As String, ByVal PostalCode2 As String, _
                          ByVal Tel1 As String, ByVal Tel2 As String, ByVal OfficeTel As String, ByVal Mobile As String, _
                          ByVal Email As String, ByVal CardNum As String, ByVal StuType As Byte, ByVal School As String, _
                          ByVal SchoolClass As String, ByVal SchoolGrade As Int32, ByVal PrimarySch As Int32, _
                          ByVal JuniorSch As Int32, ByVal HighSch As Int32, ByVal University As Int32, _
                          ByVal CurrentSch As Int32, ByVal SchGroup As Int32, ByVal GraduateFrom As String, _
                          ByVal DadName As String, ByVal MumName As String, ByVal DadJob As String, ByVal MumJob As String, _
                          ByVal DadMobile As String, ByVal MumMobile As String, ByVal IntroID As String, _
                          ByVal IntroName As String, ByVal CreateDate As Date, ByVal FeeOwe As Int32, _
                          ByVal Remarks As String, ByVal Customize1 As String, ByVal Customize2 As String, _
                          ByVal Customize3 As String, ByVal Customize4 As String, ByVal Customize5 As String, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String)

        Dim strSQL As String = "InsertStu"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char)).Value = ID
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@EngName", SqlDbType.NVarChar)).Value = EngName
            .Add(New SqlParameter("@Sex", SqlDbType.NVarChar)).Value = Sex
            .Add(New SqlParameter("@Birthday", SqlDbType.DateTime)).Value = Birthday
            .Add(New SqlParameter("@IC", SqlDbType.NVarChar)).Value = IC
            .Add(New SqlParameter("@PWD", SqlDbType.VarChar)).Value = PWD
            .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = Address
            .Add(New SqlParameter("@PostalCode", SqlDbType.NVarChar)).Value = PostalCode
            .Add(New SqlParameter("@Address2", SqlDbType.NVarChar)).Value = Address2
            .Add(New SqlParameter("@PostalCode2", SqlDbType.NVarChar)).Value = PostalCode2
            .Add(New SqlParameter("@Tel1", SqlDbType.NVarChar)).Value = Tel1
            .Add(New SqlParameter("@Tel2", SqlDbType.NVarChar)).Value = Tel2
            .Add(New SqlParameter("@OfficeTel", SqlDbType.NVarChar)).Value = OfficeTel
            .Add(New SqlParameter("@Mobile", SqlDbType.NVarChar)).Value = Mobile
            .Add(New SqlParameter("@Email", SqlDbType.NVarChar)).Value = Email
            .Add(New SqlParameter("@CardNum", SqlDbType.Char)).Value = CardNum
            .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = StuType
            .Add(New SqlParameter("@School", SqlDbType.NVarChar)).Value = School
            .Add(New SqlParameter("@SchoolClass", SqlDbType.NVarChar)).Value = SchoolClass
            .Add(New SqlParameter("@SchoolGrade", SqlDbType.SmallInt)).Value = SchoolGrade
            .Add(New SqlParameter("@PrimarySch", SqlDbType.Int)).Value = PrimarySch
            .Add(New SqlParameter("@JuniorSch", SqlDbType.Int)).Value = JuniorSch
            .Add(New SqlParameter("@HighSch", SqlDbType.Int)).Value = HighSch
            .Add(New SqlParameter("@University", SqlDbType.Int)).Value = University
            .Add(New SqlParameter("@CurrentSch", SqlDbType.SmallInt)).Value = CurrentSch
            .Add(New SqlParameter("@SchGroup", SqlDbType.NVarChar)).Value = SchGroup
            .Add(New SqlParameter("@GraduateFrom", SqlDbType.NVarChar)).Value = GraduateFrom
            .Add(New SqlParameter("@DadName", SqlDbType.NVarChar)).Value = DadName
            .Add(New SqlParameter("@MumName", SqlDbType.NVarChar)).Value = MumName
            .Add(New SqlParameter("@DadJob", SqlDbType.NVarChar)).Value = DadJob
            .Add(New SqlParameter("@MumJob", SqlDbType.NVarChar)).Value = MumJob
            .Add(New SqlParameter("@DadMobile", SqlDbType.NVarChar)).Value = DadMobile
            .Add(New SqlParameter("@MumMobile", SqlDbType.NVarChar)).Value = MumMobile
            .Add(New SqlParameter("@IntroID", SqlDbType.Char)).Value = IntroID
            .Add(New SqlParameter("@IntroName", SqlDbType.NVarChar)).Value = IntroName
            .Add(New SqlParameter("@CreateDate", SqlDbType.DateTime)).Value = CreateDate
            .Add(New SqlParameter("@FeeOwe", SqlDbType.Int)).Value = FeeOwe
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Customize1", SqlDbType.NVarChar)).Value = Customize1
            .Add(New SqlParameter("@Customize2", SqlDbType.NVarChar)).Value = Customize2
            .Add(New SqlParameter("@Customize3", SqlDbType.NVarChar)).Value = Customize3
            .Add(New SqlParameter("@Customize4", SqlDbType.NVarChar)).Value = Customize4
            .Add(New SqlParameter("@Customize5", SqlDbType.NVarChar)).Value = Customize5
            .Add(New SqlParameter("@EnrollSch", SqlDbType.NVarChar)).Value = EnrollSch
            .Add(New SqlParameter("@PunchCardNum", SqlDbType.NVarChar)).Value = PunchCardNum
            .Add(New SqlParameter("@HseeMark", SqlDbType.NVarChar)).Value = HseeMark
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub UpdStudent(ByVal ID As String, ByVal Name As String, ByVal EngName As String, ByVal Sex As String, _
                      ByVal Birthday As Date, ByVal IC As String, ByVal Address As String, _
                      ByVal PostalCode As String, ByVal Address2 As String, ByVal PostalCode2 As String, _
                      ByVal Tel1 As String, ByVal Tel2 As String, ByVal OfficeTel As String, ByVal Mobile As String, _
                      ByVal Email As String, ByVal StuType As Byte, ByVal School As String, _
                      ByVal SchoolClass As String, ByVal SchoolGrade As Int32, ByVal PrimarySch As Int32, _
                      ByVal JuniorSch As Int32, ByVal HighSch As Int32, ByVal University As Int32, _
                      ByVal CurrentSch As Int32, ByVal SchGroup As Int32, ByVal GraduateFrom As String, _
                      ByVal DadName As String, ByVal MumName As String, ByVal DadJob As String, ByVal MumJob As String, _
                      ByVal DadMobile As String, ByVal MumMobile As String, ByVal IntroID As String, _
                      ByVal IntroName As String, _
                      ByVal Remarks As String, ByVal Customize1 As String, ByVal Customize2 As String, _
                      ByVal Customize3 As String, ByVal Customize4 As String, ByVal Customize5 As String, _
                          ByVal EnrollSch As String, ByVal PunchCardNum As String, ByVal HseeMark As String)

        Dim strSQL As String = "UpdStuInfo"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char)).Value = ID
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@EngName", SqlDbType.NVarChar)).Value = EngName
            .Add(New SqlParameter("@Sex", SqlDbType.NVarChar)).Value = Sex
            .Add(New SqlParameter("@Birthday", SqlDbType.DateTime)).Value = Birthday
            .Add(New SqlParameter("@IC", SqlDbType.NVarChar)).Value = IC
            .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = Address
            .Add(New SqlParameter("@PostalCode", SqlDbType.NVarChar)).Value = PostalCode
            .Add(New SqlParameter("@Address2", SqlDbType.NVarChar)).Value = Address2
            .Add(New SqlParameter("@PostalCode2", SqlDbType.NVarChar)).Value = PostalCode2
            .Add(New SqlParameter("@Tel1", SqlDbType.NVarChar)).Value = Tel1
            .Add(New SqlParameter("@Tel2", SqlDbType.NVarChar)).Value = Tel2
            .Add(New SqlParameter("@OfficeTel", SqlDbType.NVarChar)).Value = OfficeTel
            .Add(New SqlParameter("@Mobile", SqlDbType.NVarChar)).Value = Mobile
            .Add(New SqlParameter("@Email", SqlDbType.NVarChar)).Value = Email
            .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = StuType
            .Add(New SqlParameter("@School", SqlDbType.NVarChar)).Value = School
            .Add(New SqlParameter("@SchoolClass", SqlDbType.NVarChar)).Value = SchoolClass
            .Add(New SqlParameter("@SchoolGrade", SqlDbType.SmallInt)).Value = SchoolGrade
            .Add(New SqlParameter("@PrimarySch", SqlDbType.Int)).Value = PrimarySch
            .Add(New SqlParameter("@JuniorSch", SqlDbType.Int)).Value = JuniorSch
            .Add(New SqlParameter("@HighSch", SqlDbType.Int)).Value = HighSch
            .Add(New SqlParameter("@University", SqlDbType.Int)).Value = University
            .Add(New SqlParameter("@CurrentSch", SqlDbType.SmallInt)).Value = CurrentSch
            .Add(New SqlParameter("@SchGroup", SqlDbType.NVarChar)).Value = SchGroup
            .Add(New SqlParameter("@GraduateFrom", SqlDbType.NVarChar)).Value = GraduateFrom
            .Add(New SqlParameter("@DadName", SqlDbType.NVarChar)).Value = DadName
            .Add(New SqlParameter("@MumName", SqlDbType.NVarChar)).Value = MumName
            .Add(New SqlParameter("@DadJob", SqlDbType.NVarChar)).Value = DadJob
            .Add(New SqlParameter("@MumJob", SqlDbType.NVarChar)).Value = MumJob
            .Add(New SqlParameter("@DadMobile", SqlDbType.NVarChar)).Value = DadMobile
            .Add(New SqlParameter("@MumMobile", SqlDbType.NVarChar)).Value = MumMobile
            .Add(New SqlParameter("@IntroID", SqlDbType.Char)).Value = IntroID
            .Add(New SqlParameter("@IntroName", SqlDbType.NVarChar)).Value = IntroName
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Customize1", SqlDbType.NVarChar)).Value = Customize1
            .Add(New SqlParameter("@Customize2", SqlDbType.NVarChar)).Value = Customize2
            .Add(New SqlParameter("@Customize3", SqlDbType.NVarChar)).Value = Customize3
            .Add(New SqlParameter("@Customize4", SqlDbType.NVarChar)).Value = Customize4
            .Add(New SqlParameter("@Customize5", SqlDbType.NVarChar)).Value = Customize5
            .Add(New SqlParameter("@EnrollSch", SqlDbType.NVarChar)).Value = EnrollSch
            .Add(New SqlParameter("@PunchCardNum", SqlDbType.NVarChar)).Value = PunchCardNum
            .Add(New SqlParameter("@HseeMark", SqlDbType.NVarChar)).Value = HseeMark
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Function AddClassReg(ByVal StuID As String, ByVal SubClassID As Integer, _
                                ByVal SeatNum As String, ByVal Cancel As Byte, _
                                ByVal FeeOwe As Integer, ByVal ReceiptRemarks As String, _
                                ByVal RegisterDate As Date, ByVal SalesID As String) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertClassReg"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 4)).Value = SeatNum
            .Add(New SqlParameter("@Cancel", SqlDbType.Bit)).Value = Cancel
            .Add(New SqlParameter("@FeeOwe", SqlDbType.Int)).Value = FeeOwe
            .Add(New SqlParameter("@ReceiptRemarks", SqlDbType.NVarChar)).Value = ReceiptRemarks
            .Add(New SqlParameter("@RegisterDate", SqlDbType.DateTime)).Value = RegisterDate
            .Add(New SqlParameter("@SalesID", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Sub AddSeatTk(ByVal StuID As String, ByVal SubClassID As Integer, _
                               ByVal SeatNum As String)

        Dim strSQL As String = "InsertSeatTk"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 4)).Value = SeatNum
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeleteSeatTk(ByVal StuID As String, ByVal SubClassID As Integer, _
                              ByVal SeatNum As String)

        Dim strSQL As String = "DelSeatTk"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 4)).Value = SeatNum
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub ChangeSeatTk(ByVal newClassID As Integer, _
                           ByVal oldClassID As Integer, ByVal newSeat As String, _
                           ByVal oldSeat As String, ByVal stuID As String)
        Dim strSQL As String = "UpdSeatTk"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = newClassID
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuID
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = oldClassID
            .Add(New SqlParameter("@OldSeatNum", SqlDbType.Char, 3)).Value = oldSeat
            .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 4)).Value = newSeat
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub AddBadgeReissueRec(ByVal StuID As String, _
           ByVal DateTime As Date, _
           ByVal HandlerID As String)

        Dim strSQL As String = "InsertBadgeReissueRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID

        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub AddTeleInterview(ByVal StuID As String, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String)

        Dim strSQL As String = "InsertTeleInterview"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = IDate
            .Add(New SqlParameter("@TypeID", SqlDbType.Int)).Value = TypeID
            .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdTeleInterview(ByVal ID As Integer, _
                               ByVal TypeID As Integer, ByVal Content As String)

        Dim strSQL As String = "UpdTeleInterview"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@TypeID", SqlDbType.Int)).Value = TypeID
            .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddTeleInterviewType(ByVal t As String)

        Dim strSQL As String = "InsertTeleInterviewType"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@Type", SqlDbType.NVarChar)).Value = t
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddTeleInterview(ByVal StuID As ArrayList, ByVal IDate As Date, _
                                ByVal TypeID As Integer, ByVal Content As String, _
                                ByVal HandlerID As String)

        Dim strSQL As String = "InsertTeleInterview"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim id As String = ""

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure

        If StuID.Count > 0 Then
            Try
                For index As Integer = 0 To StuID.Count - 1
                    id = StuID(index)
                    With dc.Parameters
                        .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = id
                        .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = IDate
                        .Add(New SqlParameter("@TypeID", SqlDbType.Int)).Value = TypeID
                        .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
                        .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
                    End With
                    dc.ExecuteNonQuery()
                    dc.Parameters.Clear()
                Next
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End If

    End Sub

    Public Sub UpdStuOwe(ByVal StuID As String, ByVal Amount As Integer)

        Dim strSQL As String = "UpdStuOwe"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub UpdStuClassOwe(ByVal StuID As String, ByVal SubClassID As Integer, _
                              ByVal Amount As Integer)

        Dim strSQL As String = "UpdStuClassOwe"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub UpdateStuCard(ByVal id As String, ByVal card As String)
        Dim strSQL As String = "UpdStuCard"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = id
            .Add(New SqlParameter("@CardNum", SqlDbType.Char, 10)).Value = card
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub AddStuNote(ByVal StuID As String _
           , ByVal DisplayDate As Date _
           , ByVal RepeatBfDate As Byte _
           , ByVal DisplayFor As Integer _
           , ByVal Frequency As Byte _
           , ByVal Content As String _
           , ByVal Style As Integer _
           , ByVal CreateBy As String)

        Dim strSQL As String = "InsertStuNote"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@DisplayDate", SqlDbType.DateTime)).Value = DisplayDate
            .Add(New SqlParameter("@RepeatBfDate", SqlDbType.Bit)).Value = RepeatBfDate
            .Add(New SqlParameter("@DisplayFor", SqlDbType.SmallInt)).Value = DisplayFor
            .Add(New SqlParameter("@Frequency", SqlDbType.Bit)).Value = Frequency
            .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
            .Add(New SqlParameter("@Style", SqlDbType.SmallInt)).Value = Style
            .Add(New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)).Value = Now
            .Add(New SqlParameter("@CreateBy", SqlDbType.Char, 8)).Value = CreateBy
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddStuNoteReceive(ByVal NoteID As Integer _
       , ByVal TimeReceive As Date _
       , ByVal WayReceive As Integer)

        Dim strSQL As String = "InsertNoteReceive"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@NoteID", SqlDbType.Int)).Value = NoteID
            .Add(New SqlParameter("@TimeReceive", SqlDbType.DateTime)).Value = TimeReceive
            .Add(New SqlParameter("@WayReceive", SqlDbType.SmallInt)).Value = WayReceive
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Function GetTeleInterviewType() As DataTable
        Dim strSQL As String = "GetTeleInterviewType"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Sub UpdateTeleInterviewType(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM TeleInterviewType"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Function AddPay(ByVal ReceiptNum As String, ByVal StuID As String, _
                         ByVal SubClassID As Integer, ByVal Amount As Integer, _
                         ByVal DateTime As Date, ByVal HandlerID As String, _
                         ByVal SalesID As String, ByVal PayMethod As Integer, _
                         ByVal Extra As String, ByVal Remarks As String, _
                         ByVal ChequeValid As Date, ByVal DateGet As DateTime) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertPayRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
            .Add(New SqlParameter("@tb1", SqlDbType.NVarChar)).Value = ""
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@SalesID", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@PayMethod", SqlDbType.Int)).Value = PayMethod
            .Add(New SqlParameter("@Extra", SqlDbType.NVarChar)).Value = Extra
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@ChequeValid", SqlDbType.SmallDateTime)).Value = ChequeValid
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
            .Add(New SqlParameter("@DateGet", SqlDbType.DateTime)).Value = DateGet
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Function AddMBClassInfo(ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal SalesID As String, ByVal Reason As String, _
                     ByVal DRemarks As String, ByVal Remarks As String, _
                     ByVal Discount As Integer) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertMBClassInfo"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@Reason", SqlDbType.NVarChar)).Value = Reason
            .Add(New SqlParameter("@Discount", SqlDbType.Int)).Value = Discount
            .Add(New SqlParameter("@DiscountRemarks", SqlDbType.NVarChar)).Value = DRemarks
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@SalesID", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Sub UpdMB(ByVal ID As Integer, _
                  ByVal OldAmount As Integer, ByVal Amount As Integer, _
                  ByVal DateTime As Date, ByVal UsrID As String, _
                  ByVal OldReason As String, ByVal Reason As String, _
                  ByVal ReasonMod As String)

        Dim strSQL As String = "UpdMBRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@OldAmount", SqlDbType.Int)).Value = OldAmount
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@ModifyBy", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@Reason", SqlDbType.NVarChar)).Value = Reason
            .Add(New SqlParameter("@OldReason", SqlDbType.NVarChar)).Value = OldReason
            .Add(New SqlParameter("@ReasonModify", SqlDbType.NVarChar)).Value = ReasonMod
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub UpdPunchRec(ByVal ID As Integer, _
                 ByVal DateTime1 As DateTime, ByVal InType As Integer)

        Dim strSQL As String = "UpdPunchRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@Datetime1", SqlDbType.DateTime)).Value = DateTime1
            .Add(New SqlParameter("@In", SqlDbType.SmallInt)).Value = InType
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub UpdMBRemarks(ByVal ID As Integer, ByVal Remarks As String)

        Dim strSQL As String = "UpdMBRemarks"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Function AddMKClassInfo(ByVal StuID As String, _
                 ByVal SubClassID As Integer, ByVal Amount As Integer, _
                 ByVal DateTime As Date, ByVal HandlerID As String, _
                 ByVal SalesID As String, ByVal Reason As String, _
                 ByVal DRemarks As String, ByVal Remarks As String, _
                 ByVal Discount As Integer, ByVal KeepUntil As Date) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertMKClassInfo"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@Reason", SqlDbType.NVarChar)).Value = Reason
            .Add(New SqlParameter("@Discount", SqlDbType.Int)).Value = Discount
            .Add(New SqlParameter("@DiscountRemarks", SqlDbType.NVarChar)).Value = DRemarks
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@SalesID", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@KeepUntil", SqlDbType.SmallDateTime)).Value = KeepUntil
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Function AddMBPayRec(ByVal MBClassInfoID As Integer, _
                                ByVal ReceiptNum As String, _
                     ByVal Amount As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal SalesID As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertMBPayRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@MBClassInfoID", SqlDbType.Int)).Value = MBClassInfoID
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@ArrangeBy", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@PayMethod", SqlDbType.Int)).Value = PayMethod
            .Add(New SqlParameter("@Extra", SqlDbType.NVarChar)).Value = Extra
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Function AddMKPayRec(ByVal KPClassRecID As Integer, _
                            ByVal ReceiptNum As String, _
                 ByVal Amount As Integer, _
                 ByVal DateTime As Date, ByVal HandlerID As String, _
                 ByVal SalesID As String, ByVal PayMethod As Integer, _
                 ByVal Extra As String, ByVal Remarks As String) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertMKPayRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@KPClassRecID", SqlDbType.Int)).Value = KPClassRecID
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@ArrangeBy", SqlDbType.Char, 8)).Value = SalesID
            .Add(New SqlParameter("@PayMethod", SqlDbType.Int)).Value = PayMethod
            .Add(New SqlParameter("@Extra", SqlDbType.NVarChar)).Value = Extra
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Sub UpdatePay(ByVal id As Integer, _
                              ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Amount As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal UsrName As String, ByVal PayMethod As Integer, _
                     ByVal Extra As String, ByVal Remarks As String, _
                     ByVal Reason As String)

        Dim strSQL As String = "UpdPayRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@UsrName", SqlDbType.NVarChar)).Value = UsrName
            .Add(New SqlParameter("@PayMethod", SqlDbType.Int)).Value = PayMethod
            .Add(New SqlParameter("@Extra", SqlDbType.NVarChar)).Value = Extra
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Reason", SqlDbType.NVarChar)).Value = Reason
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub



    Public Sub UpdateDisc(ByVal id As Integer, _
                    ByVal Discount As Integer, _
                     ByVal Remarks As String)

        Dim strSQL As String = "UpdDisc"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            .Add(New SqlParameter("@Discount", SqlDbType.Int)).Value = Discount
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub DeleteDisc(ByVal id As Integer)

        Dim strSQL As String = "DelDisc"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub AddPayModRec(ByVal ReceiptNum As String, ByVal StuID As String, _
                     ByVal SubClassID As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal UsrName As String, _
                     ByVal Remarks As String, _
                     ByVal Reason As String)

        Dim strSQL As String = "InsertPayModRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@HandlerID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@UsrName", SqlDbType.NVarChar, 12)).Value = UsrName
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Reason", SqlDbType.NVarChar)).Value = Reason
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub AddDiscModRec(ByVal StuID As String, _
                     ByVal SubClassID As Integer, _
                     ByVal DateTime As Date, ByVal HandlerID As String, _
                     ByVal Discount As Integer, ByVal Remarks As String, _
                     ByVal OldDiscount As Integer, ByVal OldRemarks As String)

        Dim strSQL As String = "InsertDiscModRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@EmployeeID", SqlDbType.Char, 8)).Value = HandlerID
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Discount", SqlDbType.Int)).Value = Discount
            .Add(New SqlParameter("@OldRemarks", SqlDbType.NVarChar)).Value = OldRemarks
            .Add(New SqlParameter("@OldDiscount", SqlDbType.Int)).Value = OldDiscount
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub DeletePay(ByVal id As Integer)

        Dim strSQL As String = "DelPayRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub
    Public Sub DeleteMoneyKeepRec(ByVal id As Integer)

        Dim strSQL As String = "DelMoneyKeepRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub
    Public Sub DeleteMoneyBackRec(ByVal id As Integer)

        Dim strSQL As String = "DelMoneyBackRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub


    Public Sub DeleteTeleInterview(ByVal id As Integer)

        Dim strSQL As String = "DelTeleInterview"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub DeleteStuNote(ByVal id As Integer)

        Dim strSQL As String = "DelStuNote"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub DeletePunchRec(ByVal id As Integer)

        Dim strSQL As String = "DelPunchRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub DeleteMB(ByVal id As Integer)

        Dim strSQL As String = "DelMBRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Function AddDiscount(ByVal StuID As String, _
                     ByVal SubClassID As Integer, ByVal Discount As Integer, _
                     ByVal Remarks As String) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertDiscountRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters

            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@Discount", SqlDbType.Int)).Value = Discount
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@ID").Value, Int32)
        Return lResult
    End Function

    Public Function GetSibTypeList() As DataTable
        Dim strSQL As String = "GetStuSibTypeList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuIdListByYear(ByVal strYear As String) As DataTable
        Dim strSQL As String = "GetStuIdListByYear"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@Year", SqlDbType.Char, 3)).Value = strYear
        End With
        da.Fill(dt)
        Return dt
    End Function

    '20100516 sherry upload old student photos
    Public Function GetStuIdList() As DataTable
        Dim strSQL As String = "GetStuIdList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuByClass(ByVal id As Integer) As DataTable
        Dim strSQL As String = "GetStuSearchByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = id
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuByClass(ByVal id As ArrayList) As DataTable

        Dim dt As New DataTable

        dt = DataBaseAccess.StuInfo.GetStuBySubClass(id)

        Return dt
    End Function

    Public Function GetStuNameListByClass(ByVal id As Integer) As DataTable
        Dim strSQL As String = "GetStuNameListByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = id
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuNameListBySc(ByVal id As Integer) As DataTable
        Dim strSQL As String = "GetStuNameListBySc"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ScId", SqlDbType.Int)).Value = id
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuBySubClass(ByVal id As Integer) As DataTable
        Dim strSQL As String = "GetStuSearchBySubClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = id
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuBySch(ByVal schId As Integer, ByVal schType As Integer) As DataTable
        Dim strSQL As String = ""
        Select Case schType
            Case 1
                strSQL = "GetStuSearchByPrimarySch"
            Case 2
                strSQL = "GetStuSearchByJuniorySch"
            Case 3
                strSQL = "GetStuSearchByHighSch"
            Case 4
                strSQL = "GetStuSearchByUniversity"
        End Select

        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SchoolId", SqlDbType.Int)).Value = schId
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuRecByName(ByVal strName As String) As DataTable
        Dim strSQL As String = "GetStuRecByName"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = strName
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuRecById(ByVal strId As String) As DataTable
        Dim strSQL As String = "GetStuRecById"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@Id", SqlDbType.Char, 8)).Value = strId
        End With
        da.Fill(dt)
        Return dt
    End Function


    Public Function GetStuNameById(ByVal strId As String) As String
        Dim strSQL As String = "GetStuNameById"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        Dim n As String = ""

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@Id", SqlDbType.Char, 8)).Value = strId
        End With
        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            n = dt.Rows(0).Item(c_NameColumnName)
        End If
        Return n
    End Function

    Public Function GetStuRecByCardNum(ByVal strCard As String, _
                                       ByVal ClassID As Integer) As DataTable
        Dim strSQL As String = "GetStuRecByCardNum"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@CardNum", SqlDbType.VarChar)).Value = strCard
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetStuRecByCardNumAbsoluteEqual(ByVal strCard As String, ByVal ClassID As Integer) As DataTable
        Dim strSQL As String = "GetStuRecByCardNumAbsoluteEqual"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@CardNum", SqlDbType.VarChar)).Value = strCard
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuRecByCardNum2(ByVal strCard As String) As DataTable
        Dim strSQL As String = "GetStuRecByCardNum2"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@CardNum", SqlDbType.VarChar)).Value = strCard
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuRecByID2(ByVal stuID As String, _
                                       ByVal ClassID As Integer) As DataTable
        Dim strSQL As String = "GetStuRecByID2"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuID
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetBookList(ByVal lstBook As Integer) As DataTable
        Dim strSQL As String = "GetBookList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = lstBook
        End With
        da.Fill(dt)
        Return dt
    End Function
    'ClassPunchStuinfo

    Public Function GetClassPunchBook(ByVal dayOfWeek As Integer) As DataTable
        Dim strSQL As String = "GetClassPunchBook"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DayOfWeek", SqlDbType.Int)).Value = dayOfWeek
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetClassPunchClass(ByVal dayOfWeek As Integer) As DataTable
        Dim strSQL As String = "GetClassPunchClass"
        Dim dt As New DataTable
        Dim dt1 As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DayOfWeek", SqlDbType.Int)).Value = dayOfWeek
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetClassPunchSubClass(ByVal dayOfWeek As Integer) As DataTable
        Dim strSQL As String = "GetClassPunchSubClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DayOfWeek", SqlDbType.Int)).Value = dayOfWeek
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetClassPunchContent(ByVal dayOfWeek As Integer) As DataTable
        Dim strSQL As String = "GetClassPunchContent"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DayOfWeek", SqlDbType.Int)).Value = dayOfWeek
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetClassPunchSession(ByVal dayOfWeek As Integer) As DataTable
        Dim strSQL As String = "GetClassPunchSession"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DayOfWeek", SqlDbType.Int)).Value = dayOfWeek
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetMBRecById(ByVal Id As Integer) As DataTable
        Dim strSQL As String = "GetMBRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = Id
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetMBDetails(ByVal Id As Integer) As DataTable
        Dim strSQL As String = "GetMBDetails"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = Id
        End With
        da.Fill(dt)
        Return dt
    End Function
    Public Function GetStuSiblings() As DataTable
        'Dim strSQL As String = "GetStuSibList"
        'Dim dt As New DataTable
        'Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'da.Fill(dt)
        'Return dt
        Dim dt As New DataTable
        dt = DataBaseAccess.StuInfo.GetStuSibList
        Return dt
    End Function

    Public Function GetStuSiblings(ByVal lstfilter As List(Of String)) As DataTable
        'Dim strSQL As String = "GetStuSibList"
        'Dim dt As New DataTable
        'Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'da.Fill(dt)
        'Return dt
        Dim dt As New DataTable
        dt = DataBaseAccess.StuInfo.GetStuSibList(lstfilter)
        Return dt
    End Function

    Public Function GetStuChangeClassList() As DataTable
        Dim strSQL As String = "GetStuClassChangeList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetBadgeReissueRec() As DataTable
        Dim strSQL As String = "GetBadgeReissueRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetNameRepeatStu() As DataTable
        Dim strSQL As String = "GetNameRepeatStu"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuSearch(ByVal method As Integer, ByVal kw As String, _
                                 ByVal type As Byte) As DataTable
        Select Case method
            Case 0
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.Char, 5)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 1
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.Char, 4)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 2
                Dim strSQL As String = "GetStuSearchByName"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 3
                Dim strSQL As String = "GetStuSearchByTel"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 4
                Dim strSQL As String = "GetStuSearchByParent"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case 5
                Dim strSQL As String = "GetStuSearchByAddress"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 6
                Dim strSQL As String = "GetStuSearchByIDFast"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case 7
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)

                Return dt

            Case 8
                Dim strSQL As String = "GetStuSearchByIDIn"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 9                                                                                  '100226
                Dim strSQL As String = "GetStuSearchByCardNum"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt                                                                           '100226
            Case 10
                Dim strSQL As String = "GetStuSearchBySchName"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case Else
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
        End Select


    End Function



    Public Function GetStuSearch(ByVal method As Integer, ByVal kw As String, _
                                 ByVal type As Byte, ByVal int As Integer) As DataTable
        Select Case method
            Case 0
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.Char, 5)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 1
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.Char, 4)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 2
                Dim strSQL As String = "GetStuSearchByNameEasy"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 3
                Dim strSQL As String = "GetStuSearchByTelEasy"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 4
                Dim strSQL As String = "GetStuSearchByParent"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case 5
                Dim strSQL As String = "GetStuSearchByAddressEasy"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 6
                Dim strSQL As String = "GetStuSearchByIDFast"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case 7
                Dim strSQL As String = "GetStuSearchByIDEasy"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)

                Return dt

            Case 8
                Dim strSQL As String = "GetStuSearchByIDIn"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case 9                                                                                  '100226
                Dim strSQL As String = "GetStuSearchByCardNum"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt                                                                           '100226
            Case 10
                Dim strSQL As String = "GetStuSearchBySchName"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                End With
                da.Fill(dt)
                Return dt
            Case Else
                Dim strSQL As String = "GetStuSearchByIDEasy"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
        End Select


    End Function



    Public Function GetStuSearch_Short(ByVal method As Integer, ByVal kw As String, ByVal type As Byte) As DataTable
        Select Case method
            Case 1
                Dim strSQL As String = "GetStuSearchByID"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.Char, 5)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt

            Case 3
                Dim strSQL As String = "GetStuSearchByTel_Short"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = kw
                    .Add(New SqlParameter("@StuType", SqlDbType.SmallInt)).Value = type
                End With
                da.Fill(dt)
                Return dt
            Case Else
                Dim dt As New DataTable
                Return dt
        End Select
    End Function

    Public Function GetAttendanceById(ByVal strId As String) As DataTable
        Dim strSQL As String = "GetStuClassPunchRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = strId
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetAttClassById(ByVal strId As String) As DataTable
        Dim strSQL As String = "GetStuAtendanceClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = strId
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetTeleInterviewClassDate(ByVal cId As Integer, _
                                    ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim strSQL As String = "GetTeleInterviewClassDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = cId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = dateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = dateTo
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewScDate(ByVal scId As Integer, _
                                        ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim strSQL As String = "GetTeleInterviewScDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = scId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = dateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = dateTo
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewAllDate(ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim strSQL As String = "GetTeleInterviewAllDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = dateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = dateTo
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewClass(ByVal cId As Integer) As DataTable
        Dim strSQL As String = "GetTeleInterviewClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = cId
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewStu(ByVal StuID As String) As DataTable
        Dim strSQL As String = "GetTeleInterviewStu"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewSc(ByVal scId As Integer) As DataTable
        Dim strSQL As String = "GetTeleInterviewSc"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = scId
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetTeleInterviewAll() As DataTable
        Dim strSQL As String = "GetTeleInterviewAll"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPunchRecById(ByVal strId As String) As DataTable
        Dim strSQL As String = "GetStuPunchByID"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = strId
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetPunchRecByName(ByVal strName As String) As DataTable
        Dim strSQL As String = "GetStuPunchByName"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuName", SqlDbType.NVarChar)).Value = strName
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetPunchRecByContent(ByVal SubClassID As String, ByVal ContentID As String) As List(Of String)
        Dim al As New List(Of String)
        Dim strSQL As String = "GetPunchRecByContent"


        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@SubClassContId", SqlDbType.Int)).Value = ContentID
        End With
        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            For Each row As DataRow In dt.Rows
                al.Add(row.Item("StuID"))
            Next
        End If

        Return al
    End Function

    Public Sub ChangeStuClass(ByVal newClassID As Integer, _
                       ByVal oldClassID As Integer, ByVal newSeat As String, _
                       ByVal oldSeat As String, ByVal stuID As String)
        Dim strSQL As String = "UpdStuSubClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = newClassID
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuID
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = oldClassID
            .Add(New SqlParameter("@ChgdOnDate", SqlDbType.SmallDateTime)).Value = Now
            .Add(New SqlParameter("@OldSeatNum", SqlDbType.Char, 3)).Value = oldSeat
            .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 3)).Value = newSeat
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub CancelStuClass(ByVal subClassID As Integer, ByVal stuID As String, _
                              ByVal cancel As Byte)
        Dim strSQL As String = "UpdStuCancelClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = subClassID
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuID
            .Add(New SqlParameter("@Cancel", SqlDbType.Bit)).Value = cancel
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub ChangeStuSales(ByVal ID As Integer, ByVal sales As String)
        Dim strSQL As String = "UpdStuSales"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@SalesID", SqlDbType.Char, 8)).Value = sales
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub ChangeStuID(ByVal ID As String, ByVal ID2 As String, _
                           ByVal UsrID As String)
        Dim strSQL As String = "UpdStuId"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char, 8)).Value = ID
            .Add(New SqlParameter("@ID2", SqlDbType.Char, 8)).Value = ID2
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Function GetClassRegByStuId(ByVal stuId As String) As DataTable
        Dim strSQL As String = "GetClassRegByStuId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = stuId
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPayRecByStuId(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuPayRecByStuId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetRepeatPunchRec(ByVal stuId As String, ByVal scId As Integer, ByVal sStart As Date, ByVal sEnd As Date, _
                                   ByVal sessionId As Integer, ByVal classMode As Integer) As DataTable
        Dim strSQL As String = "GetRepeatPunchRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
            .Add(New SqlParameter("@SessionID", SqlDbType.Int)).Value = sessionId
            .Add(New SqlParameter("@ClassMode", SqlDbType.SmallInt)).Value = classMode
            .Add(New SqlParameter("@SessionStart", SqlDbType.SmallDateTime)).Value = sStart
            .Add(New SqlParameter("@SessionEnd", SqlDbType.SmallDateTime)).Value = sEnd
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetClassBook(ByVal scId As Integer) As DataTable
        Dim strSQL As String = "GetClassBook"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPunchStuInfoByID(ByVal stuId As String) As DataTable
        Dim strSQL As String = "GetPunchStuInfoByID"
        Dim dt As New DataTable
        'Dim DateTime As Date = Now()

        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuId
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = Now
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetDiscByStuId(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuDiscByStuId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPayRec() As DataTable
        Dim strSQL As String = "GetStuPayRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuPayRec2(ByVal StuId As String, ByVal SubClassID As Integer) As DataTable
        Dim strSQL As String = "GetStuPayRec2"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
        End With

        da.Fill(dt)
        Return dt

    End Function


    Public Function GetBookRecByStuId(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuBookRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function
    Public Function GetBookRecByStuId2(ByVal StuId As String) As DataTable                                  '100224
        Dim strSQL As String = "GetStuBookRec2"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function                                                                                            '100224
    Public Function GetStuNoBookRec(ByVal StuId As String) As DataTable                                  '100224
        Dim strSQL As String = "GetStuNoBookRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuClassChangeRec(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuChangeClassRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetKBByStuId(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuKBById"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetSibByStuId(ByVal StuId As String) As DataTable
        Dim strSQL As String = "GetStuSibByStuId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.Char, 8)).Value = StuId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuNote(ByVal StuId As String, ByVal StuName As String) As DataTable
        Dim strSQL As String = "GetStuNote"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuId", SqlDbType.VarChar)).Value = StuId
            .Add(New SqlParameter("@StuName", SqlDbType.NVarChar)).Value = StuName
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPunchStuInfoByCard(ByVal card As String) As DataTable
        Dim strSQL As String = "GetPunchStuInfoByCard"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@CardNum", SqlDbType.VarChar)).Value = card
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = Now
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuNoteByCard(ByVal card As String) As DataTable
        Dim strSQL As String = "GetStuNoteByCard"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@CardNum", SqlDbType.VarChar)).Value = card
        End With

        da.Fill(dt)
        Return dt

    End Function


    Public Function GetStuNoteByID(ByVal card As String) As DataTable
        Dim strSQL As String = "GetStuNoteByID"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = card
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuNoteReceive(ByVal Id As Integer) As DataTable
        Dim strSQL As String = "GetStuNoteReceive"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = Id
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Sub DelStuByID(ByVal StuID As String)
        Dim strSQL As String = "DelStuByID"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char, 8)).Value = StuID.ToString()
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DelSibByID(ByVal StuID As String)
        Dim strSQL As String = "DelStuSib"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char, 8)).Value = StuID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DelStuClass(ByVal ID As Integer)
        Dim strSQL As String = "DelStuClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub AddStuSib(ByVal StuID As String, _
                         ByVal Rank As Integer, _
                         ByVal SibType As Integer, _
                         ByVal Name As String, _
                            ByVal Birthday As Date, _
                            ByVal School As String, _
                            ByVal Remarks As String)

        Dim strSQL As String = "InsertStuSib"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@Rank", SqlDbType.SmallInt)).Value = Rank
            .Add(New SqlParameter("@SibType", SqlDbType.SmallInt)).Value = SibType
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@Birthday", SqlDbType.SmallDateTime)).Value = Birthday
            .Add(New SqlParameter("@School", SqlDbType.NVarChar)).Value = School
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub UpdateStuBasic(ByVal ID As String, ByVal Name As String, _
                              ByVal Tel1 As String)

        Dim strSQL As String = "UpdStuBasic"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char)).Value = ID
            .Add(New SqlParameter("@StuName", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@Tel1", SqlDbType.NVarChar)).Value = Tel1
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

End Class

Friend Class ClassInfo
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function
    '' Transfer Strat
    Public Function GetTransPayRec(ByVal intOldSubClassId As Integer, ByVal strStuId As String) As DataTable
        Dim strSQL As String = "GetTransPayRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = intOldSubClassId
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = strStuId
        End With
        da.Fill(dt)
        Return dt

    End Function
    Public Function GetTransFeeDiscount(ByVal intOldSubClassId As String, ByVal strStuId As String) As DataTable
        Dim strSQL As String = "GetTransFeeDiscount"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = intOldSubClassId
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = strStuId
        End With
        da.Fill(dt)
        Return dt

    End Function
    Public Function GetTransKeepPay(ByVal intOldSubClassId As String, ByVal strStuId As String) As DataTable
        Dim strSQL As String = "GetTransKeepPay"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = intOldSubClassId
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = strStuId
        End With
        da.Fill(dt)
        Return dt

    End Function
    Public Function GetTransMoneyBack(ByVal intOldSubClassId As String, ByVal strStuId As String) As DataTable
        Dim strSQL As String = "GetTransMoneyBack"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@OldSubClassID", SqlDbType.Int)).Value = intOldSubClassId
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = strStuId
        End With
        da.Fill(dt)
        Return dt

    End Function


    'Transfer End

    Public Function GetSchoolList() As DataTable
        Dim strSQL As String = "GetSchoolList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetSchoolGrpList() As DataTable
        Dim strSQL As String = "GetSchoolGrpList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetSchoolTypes() As DataTable
        Dim strSQL As String = "GetSchoolType"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetClassStaList(ByVal id As Integer) As DataTable
        Dim strSQL As String = "GetClassStaList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With
        da.Fill(dt)
        dt.TableName = c_ClassInfoDataTableName
        Return dt

    End Function

    Public Function GetClassStaListByData(ByVal id As Integer, ByVal startDate As Date, ByVal endDate As Date) As DataTable
        Dim strSQL As String = "GetClassStaListByData"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            .Add(New SqlParameter("@startDate", SqlDbType.DateTime)).Value = startDate
            .Add(New SqlParameter("@endDate", SqlDbType.DateTime)).Value = endDate
        End With
        da.Fill(dt)
        dt.TableName = c_ClassInfoDataTableName

        Return dt
    End Function

    Public Function GetSubClassStaListHelper(ByVal scId As String) As DataTable
        Dim strSQL As String = String.Format("SELECT CL.Name As ClassName, CL.[End], SU.ID, SU.Name, SU.ClassID, CL.Fee, " & _
                    "(SELECT COUNT(ID) AS SNID " & _
                    "FROM StuClass " & _
                    "WHERE (SubClassID = SU.ID)) AS StuRegCnt, " & _
                    "(SELECT COUNT(ID) AS SNID " & _
                    "   FROM StuClass " & _
                    "   WHERE(SubClassID = SU.ID) And " & _
                    "   (FeeOwe = 0)) as  FeeClrCnt " & _
                    "FROM SubClass AS SU INNER JOIN " & _
                    "Class AS CL ON SU.ClassID = CL.ID " & _
                    "WHERE SU.ID in ({0}) " & _
                    "ORDER BY ClassName", scId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        da.SelectCommand.CommandType = CommandType.Text

        da.Fill(dt)
        Return dt

    End Function

        Public Function GetSubClassStaList(ByVal scId As Integer) As DataTable
            Dim strSQL As String = "GetSubClassStaList"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = scId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassList() As DataTable
            Dim strSQL As String = "GetClassList"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function
        Public Function GetSubClassID(ByVal ClassID As Integer) As DataTable
            Dim strSQL As String = "GetSubClassID"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassToday(ByVal d As Date) As DataTable
            Dim strSQL As String = "GetClassToday"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@Date", SqlDbType.SmallDateTime)).Value = d
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSubClassListByUsr(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetSubClassListByUsr"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)
            dt.TableName = c_ClassInfoDataTableName
            Return dt

        End Function

        Public Function GetClassListByUsr(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetClassListByUsr"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)
            dt.TableName = c_ClassInfoDataTableName
            Return dt

        End Function

    Public Function GetClassListByUsrAll(ByVal usrId As String) As DataSet
        Dim strSQL As String = "SELECT DISTINCT CL.ID, CL.Name, CL.Start, CL.[End], CL.Fee, " & _
            "(SELECT COUNT(ID) AS SNID " & _
            "FROM StuClass " & _
            "WHERE (SubClassID = SU.ID)) AS StuRegCnt, " & _
            "(SELECT COUNT(ID) AS SNID " & _
            "FROM StuClass " & _
            "WHERE (SubClassID = SU.ID)) AS CancelCnt " & _
            "FROM Class AS CL LEFT OUTER JOIN " & _
            "SubClass AS SU ON CL.ID = SU.ClassID " & _
            "WHERE (CL.ID IN " & _
            "(SELECT DISTINCT ClassID " & _
            "FROM SubClass AS SC " & _
            "WHERE (ID IN " & _
            "(SELECT DISTINCT SubClassID " & _
            "FROM ClassAuthority AS CA " & _
            "WHERE (UsrID = '{0}'))))) OR " & _
            "(CL.ID IN " & _
            "(SELECT DISTINCT ClassID " & _
            "FROM NewClassAuthority AS NC " & _
            "WHERE (UsrID = '{0}'))) " & _
            "OR '{0}' = 'admin' " & _
            "OR ('{0}' IN (SELECT UsrID " & _
            "FROM AllClassAuthority)) " & _
            "ORDER BY CL.Name;"
        '"SELECT DISTINCT CL.ID, CL.Name,SU.Name AS SubClassName, CL.Start, CL.[End], CL.Fee, " & _
        '"(SELECT COUNT(ID) AS SNID " & _
        '"FROM StuClass " & _
        '"WHERE (SubClassID = SU.ID)) AS StuRegCnt, " & _
        '"(SELECT COUNT(ID) AS SNID " & _
        '"FROM StuClass " & _
        '"WHERE (SubClassID = SU.ID) AND (Cancel = 1)) AS CancelCnt " & _
        '"FROM Class AS CL LEFT JOIN " & _
        '"SubClass AS SU ON CL.ID = SU.ClassID " & _
        '"WHERE (CL.ID IN " & _
        '"(SELECT DISTINCT ClassID " & _
        '"FROM SubClass AS SC " & _
        '"WHERE (ID IN " & _
        '"(SELECT DISTINCT SubClassID " & _
        '"FROM ClassAuthority AS CA " & _
        '"WHERE (UsrID = '{0}'))))) OR " & _
        '"(CL.ID IN " & _
        '"(SELECT DISTINCT ClassID " & _
        '"FROM NewClassAuthority AS NC " & _
        '"WHERE (UsrID = '{0}'))) " & _
        '"OR '{0}' = 'admin' " & _
        '"OR ('{0}' IN (SELECT UsrID " & _
        '"FROM AllClassAuthority)); "
        strSQL = String.Format(strSQL, usrId)
        Dim ds As New DataSet
        Dim cmd As New SqlCommand(strSQL, New SqlConnection(GetConnectionString()))
        Dim da As New SqlDataAdapter()

        cmd.CommandText = CommandType.Text
        da.SelectCommand = cmd
        'With da.SelectCommand.Parameters
        '    .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
        'End With
        da.Fill(ds)

        Return ds

    End Function


    Public Function GetClassListByUsr2(ByVal usrId As String) As DataTable
        Dim strSQL As String = "GetClassListByUsr2"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
        End With
        da.Fill(dt)

        Return dt

    End Function

        Public Function GetSubClassListByUsr2(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetSubClassListByUsr2"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)

            Return dt

        End Function

        Public Function GetContentListByUsr(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetContentByUsrID"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetBookListByUsr(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetBookByUsrID"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetContentList() As DataTable
            Dim strSQL As String = "GetContent"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters

            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetBookListWithoutUsrID() As DataTable
            Dim strSQL As String = "GetBookListWithoutUsrID"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetBookByClassID(ByVal userId As String, ByVal ClassId As Integer) As DataTable
            Dim strSQL As String = "GEtBookListByClassID"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = userId
                .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetContentVideo(ByVal cId As Integer) As DataTable
            Dim strSQL As String = "GetContVideo"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = cId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSessionListByUsr(ByVal usrId As String) As DataTable
            Dim strSQL As String = "GetSessionListByUsr"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = usrId
            End With
            da.Fill(dt)
            dt.TableName = c_ClassInfoDataTableName
            Return dt

        End Function

        Public Function GetClassRoomByScId(ByVal scId As Integer) As DataTable
            Dim strSQL As String = "GetClassRoomByScId"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = scId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSeatMapByScId(ByVal scId As Integer) As DataTable
            Dim strSQL As String = "GetClassSeatMap"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassRoomList() As DataTable
            Dim strSQL As String = "GetClassRoomList"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassRooms() As DataTable
            Dim strSQL As String = "GetClassRooms"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSampleTime() As DataTable
            Dim strSQL As String = "GetSampleTime"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Sub AddSampleTime(ByVal t As Date)
            Dim strSQL As String = "InsertSampleTime"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@DateTime", SqlDbType.SmallDateTime)).Value = t
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try


        End Sub

        Public Function GetSubjects() As DataTable
            Dim strSQL As String = "GetSubjects"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassRoomUsageList() As DataTable
            Dim strSQL As String = "GetClassRoomUsageList"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSeatTkListByScId(ByVal scId As Integer) As DataTable
            Dim strSQL As String = "GetSeatTkByScId"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassId", SqlDbType.Int)).Value = scId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSeatNaListByCrId(ByVal crId As Integer) As DataTable
            Dim strSQL As String = "GetSeatNaByCrId"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@ClassRoomId", SqlDbType.Int)).Value = crId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSubClassPunchRec(ByVal scId As Integer, ByVal cId As Integer, _
                                            ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim strSQL As String = "GetClassPunchRec"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
                .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = cId
                .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetIdvClassPunchRec(ByVal scId As Integer, _
                                         ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim strSQL As String = "GetIdvClassPunchRec"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
                .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSubClassPunchRecNoCont(ByVal scId As Integer, _
                                            ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
            Dim strSQL As String = "GetClassPunchRecNoCont"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
                .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassShowupCnt(ByVal scId As Integer, ByVal cId As Integer, _
                                            ByVal dateFrom As Date, ByVal dateTo As Date) As Integer
            Dim strSQL As String = "GetClassShowupCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
                .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = cId
                .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
            End With
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function GetClassShowupCntNoCont(ByVal scId As Integer, _
                                            ByVal dateFrom As Date, ByVal dateTo As Date) As Integer
            Dim strSQL As String = "GetClassShowupCntNoCont"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = scId
                .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
            End With
            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function GetSubClassRegCnt(ByVal scId As Integer) As DataTable
            Dim strSQL As String = "GetSubClassRegCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = scId
            End With
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetSessionCnt(ByVal dateFrom As Date, ByVal dateTo As Date, _
                                      ByVal dayOfWeek As Integer) As Integer
            Dim c As Integer = 0
            Do Until dateFrom.Year = dateTo.Year And dateFrom.Month = dateTo.Month And _
                dateFrom.Day > dateTo.Day
                If dateFrom.DayOfWeek = dayOfWeek Then
                    c = c + 1
                End If
                dateFrom = DateSerial(dateFrom.Year, dateFrom.Month, dateFrom.Day + 1).Date
            Loop
            Return c
        End Function

        Public Function GetSubClassList() As DataTable
            Dim strSQL As String = "GetSubClassList"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure
            da.Fill(dt)
            Return dt

        End Function

        Public Function GetClassCnt() As Integer
            Dim strSQL As String = "GetClassCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function AddClass(ByVal Name As String, ByVal dateStart As Date, _
                                 ByVal dateEnd As Date, ByVal Fee As Integer, _
                                 ByVal usrID As String) As Int32

            Dim lResult As Int32

            Dim strSQL As String = "InsertClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetClassCnt() + 1001
            Dim dt As DataTable = GetClassList()
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dateStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dateEnd
                .Add(New SqlParameter("@Fee", SqlDbType.Int)).Value = Fee
                .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = usrID
                .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            lResult = id
            Return lResult
        End Function

        Public Sub AddPunchRec(ByVal StuID As String, _
               ByVal DateTime1 As Date, _
               ByVal SubClassID As Integer, _
               ByVal SubClassContID As Integer, _
               ByVal MakeUpClassID As Integer, _
               ByVal bIn As Byte, _
               ByVal SessionID As Integer)

            Dim strSQL As String = "InsertPunchRec"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
                .Add(New SqlParameter("@DateTime1", SqlDbType.DateTime)).Value = DateTime1
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
                .Add(New SqlParameter("@SubClassContID", SqlDbType.Int)).Value = SubClassContID
                .Add(New SqlParameter("@MakeUpClassID", SqlDbType.Int)).Value = MakeUpClassID
                .Add(New SqlParameter("@In", SqlDbType.Bit)).Value = bIn
                .Add(New SqlParameter("@SessionID", SqlDbType.Int)).Value = SessionID

            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
                Dim t1 As Date = Now

                'CSOL.Logger.LogMsg(String.Format("{0}", t1.ToString))
                'Throw New System.Exception(c_DataAccessErrMessage)
            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
                CSOL.Logger.LogError(String.Format("{0}", ex))
            End Try
        End Sub

        Public Function GetContentCnt() As Integer
            Dim strSQL As String = "GetContentCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function
        Public Function GetRepeatContent(ByVal subClassID As Integer, ByVal strDate As Date, ByVal endDate As Date) As Boolean
            Dim strSQL As String = "GetRepeatContent"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
            da.SelectCommand.CommandType = CommandType.StoredProcedure
            With da.SelectCommand.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = subClassID
                .Add(New SqlParameter("@Start", SqlDbType.DateTime)).Value = strDate
                .Add(New SqlParameter("@End", SqlDbType.DateTime)).Value = endDate
            End With
            da.Fill(dt)

            If dt.Rows.Count > 0 Then
                Return False
            Else
                Return True
            End If

        End Function
        Public Function AddContent(ByVal Content As String, ByVal ClassID As Integer, _
                                   ByVal dateStart As Date, _
                              ByVal dateEnd As Date) As Int32

            Dim lResult As Int32

            Dim strSQL As String = "InsertContent"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetContentCnt() + 1001
            Dim dt As DataTable = GetContentListByUsr("admin")
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dateStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dateEnd
                .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            lResult = id
            Return lResult
        End Function

        Public Sub ChangeSeatNum(ByVal ID As Integer, ByVal SeatNum As String)
            Dim strSQL As String = "UpdSeatNum"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@SeatNum", SqlDbType.Char, 4)).Value = SeatNum
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub UpdContent(ByVal ID As Integer, ByVal Content As String, _
                               ByVal dateStart As Date, _
                          ByVal dateEnd As Date)

            Dim strSQL As String = "UpdContent"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Content", SqlDbType.NVarChar)).Value = Content
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dateStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dateEnd
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Sub AddSubClassCont(ByVal ContentID As Integer, ByVal SubClassID As Integer)

            Dim strSQL As String = "InsertScContent"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = ContentID
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Sub AddContVideo(ByVal ContentID As Integer, ByVal ID As Integer, _
                                ByVal Path As String)

            Dim strSQL As String = "InsertContVideo"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = ContentID
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@Path", SqlDbType.NVarChar)).Value = Path
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub
        ''Transfer Start
        Public Sub UpdTransPayRec(ByVal ID As Integer, ByVal ReceiptNum As String, ByVal StuID As String, ByVal IntSubClassId As Integer)

            Dim strSQL As String = "UpdTransPayRec"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@ReceiptNum", SqlDbType.NVarChar)).Value = ReceiptNum
                .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
                .Add(New SqlParameter("@IntSubClassId", SqlDbType.Int)).Value = IntSubClassId
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub

        Public Sub UpdTransMoneyBack(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)

            Dim strSQL As String = "UpdTransMoneyBack"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
                .Add(New SqlParameter("@IntSubClassId", SqlDbType.Int)).Value = IntSubClassId
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub

        Public Sub UpdTransFeeDiscount(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)

            Dim strSQL As String = "UpdTransFeeDiscount"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
                .Add(New SqlParameter("@IntSubClassId", SqlDbType.Int)).Value = IntSubClassId
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub

        Public Sub UpdTransKeepPay(ByVal ID As Integer, ByVal StuID As String, ByVal IntSubClassId As Integer)

            Dim strSQL As String = "UpdTransKeepPay"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
                .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
                .Add(New SqlParameter("@IntSubClassId", SqlDbType.Int)).Value = IntSubClassId
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub



        ''Transfer End


        Public Sub UpdateClass(ByVal ID As Integer, ByVal Name As String, _
                               ByVal dateStart As Date, _
                             ByVal dateEnd As Date, ByVal Fee As Integer)

            Dim strSQL As String = "UpdClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.NVarChar)).Value = ID
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dateStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dateEnd
                .Add(New SqlParameter("@Fee", SqlDbType.Int)).Value = Fee
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub

        Public Sub DeleteClassRoom(ByVal id As Integer)
            Dim strSQL As String = "DelClassRoom"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ClassRoomId", SqlDbType.Int)).Value = id
            End With



            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try


        End Sub

        Public Sub DeleteClass(ByVal id As Integer)
            Dim strSQL As String = "DelClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteSchool(ByVal id As Integer)
            Dim strSQL As String = "DelSchool"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteContent(ByVal id As Integer)
            Dim strSQL As String = "DelContent"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteContentVideo(ByVal id As Integer)
            Dim strSQL As String = "DelContVideo"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                ' Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteScContent(ByVal id As Integer)
            Dim strSQL As String = "DelScContent"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteSubClass(ByVal id As Integer)
            Dim strSQL As String = "DelSubClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteSession(ByVal id As Integer)
            Dim strSQL As String = "DelSession"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Sub DeleteSampleTime()
            Dim strSQL As String = "DelSampleTime"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            cn.Open()
            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                'Throw New System.Exception(c_DataAccessErrMessage, ex)
            Finally
                cn.Close()
            End Try

        End Sub

        Public Function GetSubClassCnt() As Integer
            Dim strSQL As String = "GetSubClassCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function AddSubClass(ByVal Name As String, ByVal ClassID As Integer, _
                             ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                             ByVal ClassRoomID As Integer, ByVal UsrID As String) As Int32

            Dim lResult As Int32

            Dim strSQL As String = "InsertSubClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetSubClassCnt() + 1001
            Dim dt As DataTable = GetSubClassList()
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
                .Add(New SqlParameter("@Sequence", SqlDbType.SmallInt)).Value = Sequence
                .Add(New SqlParameter("@SubjectID", SqlDbType.SmallInt)).Value = SubjectID
                .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
                .Add(New SqlParameter("@ClassRoomID", SqlDbType.Int)).Value = ClassRoomID
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            lResult = id
            Return lResult
        End Function

        Public Sub UpdSubClass(ByVal id As Integer, ByVal Name As String, _
                                    ByVal ClassID As Integer, _
                         ByVal Sequence As Integer, ByVal SubjectID As Integer, _
                         ByVal ClassRoomID As Integer)

            Dim strSQL As String = "UpdSubClass"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
                .Add(New SqlParameter("@Sequence", SqlDbType.SmallInt)).Value = Sequence
                .Add(New SqlParameter("@SubjectID", SqlDbType.SmallInt)).Value = SubjectID
                .Add(New SqlParameter("@ClassRoomID", SqlDbType.Int)).Value = ClassRoomID
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Sub UpdScSequence(ByVal id As Integer, ByVal Sequence As Integer)

            Dim strSQL As String = "UpdScSequence"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Sequence", SqlDbType.SmallInt)).Value = Sequence
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Function GetSessionCnt() As Integer
            Dim strSQL As String = "GetSessionCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function AddSession(ByVal SubClassID As Integer, _
                          ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                          ByVal dtEnd As Date) As Int32

            Dim lResult As Int32

            Dim strSQL As String = "InsertSession"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetSessionCnt() + 1001
            Dim dt As DataTable = GetSessionListByUsr("admin")
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
                .Add(New SqlParameter("@DayOfWeek", SqlDbType.SmallInt)).Value = DayOfWeek
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dtStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dtEnd
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            lResult = id
            Return lResult
        End Function

        Public Sub UpdSession(ByVal id As Integer, ByVal SubClassID As Integer, _
                      ByVal DayOfWeek As Integer, ByVal dtStart As Date, _
                      ByVal dtEnd As Date)

            Dim strSQL As String = "UpdSession"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
                .Add(New SqlParameter("@DayOfWeek", SqlDbType.SmallInt)).Value = DayOfWeek
                .Add(New SqlParameter("@Start", SqlDbType.SmallDateTime)).Value = dtStart
                .Add(New SqlParameter("@End", SqlDbType.SmallDateTime)).Value = dtEnd
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Sub UpdateSubjects(ByVal ChangeTb As DataTable)

            If IsNothing(ChangeTb) Then
                Exit Sub
            End If

            Dim strSQL As String = "SELECT * FROM ClassSubject"
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

            Dim cb As New SqlCommandBuilder(da)
            da.UpdateCommand = cb.GetUpdateCommand

            Try
                da.Update(ChangeTb)
            Catch ex As Exception
                Throw New Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Function GetClassRoomCnt() As Integer
            Dim strSQL As String = "GetClassRoomCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function AddClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                     ByVal Name As String, ByVal DisStyle As Integer) As Int32

            Dim lResult As Int32 = 0

            Dim strSQL As String = "InsertClassRoom"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetClassRoomCnt() + 1001
            Dim dt As DataTable = GetClassRoomList()
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@LineCnt", SqlDbType.SmallInt)).Value = LineCnt
                .Add(New SqlParameter("@ColumnCnt", SqlDbType.SmallInt)).Value = ColumnCnt
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@DisStyle", SqlDbType.SmallInt)).Value = DisStyle
                .Add(New SqlParameter("@RoomID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()
            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            Return id
        End Function

        Public Sub UpdateClassRoom(ByVal LineCnt As Integer, ByVal ColumnCnt As Integer, _
                                 ByVal Name As String, ByVal DisStyle As Integer, _
                                 ByVal id As Integer)

            Dim strSQL As String = "UpdClassRoom"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@LineCnt", SqlDbType.SmallInt)).Value = LineCnt
                .Add(New SqlParameter("@ColumnCnt", SqlDbType.SmallInt)).Value = ColumnCnt
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@DisStyle", SqlDbType.SmallInt)).Value = DisStyle
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub

        Public Sub AddSeatNa(ByVal ColNum As Integer, ByVal RowNum As Integer, _
                             ByVal CrNum As Integer, ByVal State As Integer)

            Dim strSQL As String = "InsertSeatNa"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@ColumnNum", SqlDbType.SmallInt)).Value = ColNum
                .Add(New SqlParameter("@RowNum", SqlDbType.SmallInt)).Value = RowNum
                .Add(New SqlParameter("@ClassRoomNum", SqlDbType.Int)).Value = CrNum
                .Add(New SqlParameter("@State", SqlDbType.SmallInt)).Value = State
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

        End Sub


        Public Function GetSchoolCnt() As Integer
            Dim strSQL As String = "GetSchoolCnt"
            Dim dt As New DataTable
            Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

            da.SelectCommand.CommandType = CommandType.StoredProcedure

            da.Fill(dt)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item(c_StuRegCntColumnName)
            Else
                Return 0
            End If

        End Function

        Public Function AddSchool(ByVal Name As String, ByVal TypeID As Integer, _
                                  ByVal Phone As String, ByVal Address As String, _
                                  ByVal PersonContact As String) As Int32

            Dim lResult As Int32

            Dim strSQL As String = "InsertSchool"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand
            Dim id As Integer = GetSchoolCnt() + 1001
            Dim dt As DataTable = GetSchoolList()
            Dim flag As Boolean = False
            Dim strC1 As String = "COUNT(ID)"
            Dim strC2 As String = ""
            Do Until flag
                strC2 = "ID=" & id.ToString
                If dt.Compute(strC1, strC2) = 0 Then
                    flag = True
                Else
                    id = id + 1
                End If
            Loop

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@TypeID", SqlDbType.SmallInt)).Value = TypeID
                .Add(New SqlParameter("@Phone", SqlDbType.NVarChar)).Value = Phone
                .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = Address
                .Add(New SqlParameter("@PersonContact", SqlDbType.NVarChar)).Value = PersonContact
                .Add(New SqlParameter("@SchoolID", SqlDbType.Int)).Value = id
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            lResult = id
            Return lResult
        End Function

        Public Sub UpdSchool(ByVal ID As Integer, _
                                  ByVal Name As String, ByVal TypeID As Integer, _
                               ByVal Phone As String, ByVal Address As String, _
                               ByVal PersonContact As String)

            Dim strSQL As String = "UpdSchool"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@TypeID", SqlDbType.SmallInt)).Value = TypeID
                .Add(New SqlParameter("@Phone", SqlDbType.NVarChar)).Value = Phone
                .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = Address
                .Add(New SqlParameter("@PersonContact", SqlDbType.NVarChar)).Value = PersonContact
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try
        End Sub

    End Class

Friend Class Book
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    Public Function GetBookCnt() As Integer
        Dim strSQL As String = "GetBookCnt"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(c_StuRegCntColumnName)
        Else
            Return 0
        End If

    End Function

    Public Function GetAllBookIDs() As ArrayList
        Dim strSQL As String = "GetAllBookIDs"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        Dim lst As New ArrayList

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        For Each row As DataRow In dt.Rows
            lst.Add(row.Item(c_IDColumnName))
        Next
        Return lst

    End Function

    Public Function AddBook(ByVal Name As String, ByVal Stock As Integer, _
                          ByVal Price As Integer, ByVal BeforeDate As Date, _
                          ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                          ByVal MultiClass As Byte, ByVal CreateDate As Date) As Int32

        Dim strSQL As String = "InsertBook"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim id As Integer = GetBookCnt() + 1001
        Dim lst As ArrayList = GetAllBookIDs()
        Dim flag As Boolean = False

        Do Until flag
            If lst.Contains(id) = False Then
                flag = True
            Else
                id = id + 1
            End If
        Loop

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@Stock", SqlDbType.Int)).Value = Stock
            .Add(New SqlParameter("@Price", SqlDbType.Int)).Value = Price
            .Add(New SqlParameter("@BeforeDate", SqlDbType.SmallDateTime)).Value = New Date(BeforeDate.Year, _
                                                                                            BeforeDate.Month, _
                                                                                            BeforeDate.Day)
            .Add(New SqlParameter("@FeeClear", SqlDbType.Bit)).Value = FeeClear
            .Add(New SqlParameter("@FeeClearDate", SqlDbType.SmallDateTime)).Value = New Date(FeeClearDate.Year, _
                                                                                            FeeClearDate.Month, _
                                                                                            FeeClearDate.Day)
            .Add(New SqlParameter("@MultiClass", SqlDbType.Bit)).Value = MultiClass
            .Add(New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)).Value = New Date(CreateDate.Year, _
                                                                                            CreateDate.Month, _
                                                                                            CreateDate.Day)
            .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
        Return id
    End Function



    '20100205 updated by sherry start
    Public Function GetBookRecByUsr(ByVal ClassId As Integer, ByVal BookId As Integer) As DataTable
        'Dim strSQL As String = "GetBookIssueRecByClassBookID"
        'Dim dt As New DataTable
        'Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'With da.SelectCommand.Parameters
        '    .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassId
        '    .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = BookId
        'End With

        'da.Fill(dt)
        'Return dt
        Return DataBaseAccess.Book.GetBookRecByUsr(ClassId, BookId)

    End Function
    '20100205 updated by sherry end

    Public Function GetBookIssueRecByStuBook(ByVal stuid As String, _
                                         ByVal bookid As Integer) As DataTable
        Dim strSQL As String = "GetBookIssueRecByStuBook"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = stuid
            .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = bookid
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Sub UpdateBook(ByVal ID As Integer, _
                               ByVal Name As String, ByVal Stock As Integer, _
                      ByVal Price As Integer, ByVal BeforeDate As Date, _
                      ByVal FeeClear As Byte, ByVal FeeClearDate As Date, _
                      ByVal MultiClass As Byte, ByVal CreateDate As Date)

        Dim strSQL As String = "UpdBook"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@Stock", SqlDbType.Int)).Value = Stock
            .Add(New SqlParameter("@Price", SqlDbType.Int)).Value = Price
            .Add(New SqlParameter("@BeforeDate", SqlDbType.SmallDateTime)).Value = BeforeDate
            .Add(New SqlParameter("@FeeClear", SqlDbType.Bit)).Value = FeeClear
            .Add(New SqlParameter("@FeeClearDate", SqlDbType.SmallDateTime)).Value = FeeClearDate
            .Add(New SqlParameter("@MultiClass", SqlDbType.Bit)).Value = MultiClass
            .Add(New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)).Value = CreateDate
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateBookQ(ByVal ID As Integer, ByVal Stock As Integer)

        Dim strSQL As String = "UpdBookQ"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@Stock", SqlDbType.Int)).Value = Stock
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateBookQByValue(ByVal ID As Integer, ByVal Stock As Integer)

        Dim strSQL As String = "UpdBookQByValue"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@Stock", SqlDbType.Int)).Value = Stock
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddClassBook(ByVal BookID As Integer, ByVal SubClassID As Integer)

        Dim strSQL As String = "InsertClassBook"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = BookID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub DeleteBook(ByVal id As Integer)
        Dim strSQL As String = "DelBook"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeleteBookIssueRec(ByVal id As Integer)
        Dim strSQL As String = "DelBookIssueRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeleteClassBook(ByVal id As Integer)
        Dim strSQL As String = "DelClassBook"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try

            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub AddBookIssueRec(ByVal StuID As String, _
       ByVal DateTime As Date, _
       ByVal ClassID As Integer, _
       ByVal BookID As Integer)

        Dim strSQL As String = "InsertBookIssueRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
            .Add(New SqlParameter("@BookID", SqlDbType.Int)).Value = BookID

        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub
End Class

Friend Class Accounting
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    'Pay, money keep, money back statistics
    Public Function GetPayStaByDate(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetPayStaByDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Function GetMKeepStaByDate(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetMKeepStaByDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Function GetMBackStaByDate(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetMBackStaByDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Function GetPayRecModify(ByVal ReceiptNum As String) As DataTable
        Dim strSQL As String = "GetPayRecModify"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ReceiptNum", SqlDbType.Char, 7)).Value = ReceiptNum
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetPayStaByClass(ByVal ClassId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetPayStaByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetPayStaByClassHelper(ByVal ClassId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim daf As String = DateFrom.ToString("yyyy/MM/dd")
        Dim dat As String = DateTo.ToString("yyyy/MM/dd")
        Dim strSQL As String = "SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, " & _
                "SI.School, PA.DateTime, PA.PayMethod, PA.Amount, US.Name As Handler, " & _
                "PA.Extra, SC.ClassID, PA.SubClassID, TC.SeatNum, SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
                "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
                "SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
                "SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University " & _
                "FROM PayRec AS PA INNER JOIN " & _
                "SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN " & _
                "StuInfo AS SI ON PA.StuID = SI.ID left outer join " & _
                "OrgUser As US ON PA.HandlerID = US.ID INNER JOIN " & _
                "StuClass As TC ON (PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID) " & _
                "WHERE (PA.DateTime BETWEEN '{0}' AND {1}) AND " & _
                "(SC.ClassID in ({2}))"
        strSQL = String.Format(strSQL, daf, dat, ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'With da.SelectCommand.Parameters
        '    .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        '    .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
        '    .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        'End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetPayStaBySubClass(ByVal SubClassID As Integer, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetPayStaBySubClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetSalesSta(ByVal UsrId As String, ByVal ClassId As Integer, _
                                          ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetSalesPaySta"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char)).Value = UsrId
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetSalesSta(ByVal UsrId As String, ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetSalesPayStaNoDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char)).Value = UsrId
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetSalesDisc(ByVal UsrId As String, ByVal ClassId As Integer, _
                                      ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetUsrDiscByUsrId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char)).Value = UsrId
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetSalesDisc(ByVal UsrId As String, ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetUsrDiscNoDateByUsrId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char)).Value = UsrId
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetPayStaByClassHelper(ByVal ClassId As String) As DataTable
        Dim strSQL As String = String.Format("SELECT PA.ReceiptNum, PA.StuID, SI.Name AS StuName, SC.Name AS SubClassName, " & _
                "SI.School, PA.DateTime, PA.PayMethod, PA.Amount, US.Name As Handler, " & _
                "PA.Extra, SC.ClassID, PA.SubClassID, TC.SeatNum, " & _
                "SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
                "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
                "SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
                "SI.Email, TC.RegisterDate, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University " & _
                "FROM PayRec AS PA INNER JOIN " & _
                "SubClass AS SC ON PA.SubClassID = SC.ID INNER JOIN " & _
                "StuInfo AS SI ON PA.StuID = SI.ID left outer join " & _
                "OrgUser As US ON PA.HandlerID = US.ID INNER JOIN " & _
                "StuClass As TC ON (PA.StuID = TC.StuID AND PA.SubClassID = TC.SubClassID) " & _
                "where SC.ClassID in ({0})", ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.Text

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetPayStaByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetPayStaByClassNoDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetPayStaBySubClass(ByVal SubClassID As Integer) As DataTable
        Dim strSQL As String = "GetPayStaBySubClassNoDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMKeepStaByClass(ByVal ClassId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetMKeepStaByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMKeepStaByClassHelper(ByVal ClassId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim daf As String = DateFrom.ToString("yyyy/MM/dd")
        Dim dat As String = DateTo.ToString("yyyy/MM/dd")
        Dim strSQL As String = "SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, " & _
                "BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount, BP.Extra, " & _
                "US.Name As Handler, SC.Name AS SubClassName, MB.Amount As BackAmount, " & _
                "SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, " & _
                "SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
                "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
                "SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
                "SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch," & _
                "SI.HighSch, SI.University, MB.DateTime " & _
                "FROM KeepPaySubClassRec AS MB INNER JOIN " & _
                "KeepPayRec AS BP ON MB.ID = BP.KPClassRecID INNER JOIN " & _
                "SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN " & _
                "Class AS CL ON SC.ClassID = CL.ID INNER JOIN " & _
                "StuInfo AS SI ON MB.StuID = SI.ID left outer join " & _
                "OrgUser As US ON BP.HandlerID = US.ID INNER JOIN " & _
                "StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) " & _
                "WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND " & _
                "(SC.ClassID in ({2}))"
        strSQL = String.Format(strSQL, daf, dat, ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'With da.SelectCommand.Parameters
        '    .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        '    .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
        '    .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        'End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMKeepStaByClassHelper(ByVal ClassId As String) As DataTable
        Dim strSQL As String = String.Format("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, " & _
                "BP.DateTime, BP.PayMethod, BP.Amount, BP.Extra, " & _
                "US.Name As Handler, SC.Name AS SubClassName, MB.Amount As BackAmount, " & _
                "SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, " & _
                "SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
                "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
                "SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
                "SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University " & _
                "FROM KeepPaySubClassRec AS MB INNER JOIN " & _
                "KeepPayRec AS BP ON MB.ID = BP.KPClassRecID INNER JOIN " & _
                "SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN " & _
                "Class AS CL ON SC.ClassID = CL.ID INNER JOIN " & _
                "StuInfo AS SI ON MB.StuID = SI.ID left outer join " & _
                "OrgUser As US ON BP.HandlerID = US.ID INNER JOIN " & _
                "  StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) " & _
                "WHERE SC.ClassID in ({0})", ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.Text

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMKeepStaByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetMKeepStaByClassNoDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMBackStaByClass(ByVal ClassId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetMBackStaByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMBackStaByClassHelper(ByVal ClassId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim daf As String = DateFrom.ToString("yyyy/MM/dd")
        Dim dat As String = DateTo.ToString("yyyy/MM/dd")
        Dim strSQL As String = "SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, " & _
            "BP.DateTime as PayDateTime, BP.PayMethod, BP.Amount, BP.Extra, " & _
            "US.Name AS Handler, SC.Name AS SubClassName, MB.Amount AS BackAmount, " & _
            "SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, " & _
            "SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
            "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
            "SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
            "SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University, MB.DateTime " & _
            "FROM MoneyBackClassInfo AS MB INNER JOIN " & _
            "MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID INNER JOIN " & _
            "SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN " & _
            "Class AS CL ON SC.ClassID = CL.ID INNER JOIN " & _
            "StuInfo AS SI ON MB.StuID = SI.ID left outer join " & _
            "OrgUser AS US ON BP.HandlerID = US.ID INNER JOIN " & _
            "StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) " & _
            "WHERE (MB.DateTime BETWEEN '{0}' AND '{1}') AND " & _
            "(SC.ClassID in ({2}))"
        strSQL = String.Format(strSQL, daf, dat, ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        'da.SelectCommand.CommandType = CommandType.StoredProcedure
        'With da.SelectCommand.Parameters
        '    .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        '    .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
        '    .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        'End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMBackStaByClassHelper(ByVal ClassId As String) As DataTable
        Dim strSQL As String = String.Format("SELECT BP.ReceiptNum, MB.StuID, SI.Name AS StuName, SI.School, " & _
            "BP.DateTime, BP.PayMethod, BP.Amount, BP.Extra, " & _
            "US.Name AS Handler, SC.Name AS SubClassName, MB.Amount AS BackAmount, " & _
            "SC.ClassID, CL.Name AS ClassName, MB.SubClassID, TC.SeatNum, " & _
            "SI.Mobile, SI.EngName, SI.Birthday, SI.DadName, " & _
            "SI.MumName, SI.Tel1, SI.Tel2, SI.DadMobile, SI.MumMobile, SI.SchoolGrade, " & _
            "SI.SchoolClass, SI.GraduateFrom, SI.IntroID, SI.IntroName, SI.IC, " & _
            "SI.Email, TC.RegisterDate, MB.Reason, SI.PrimarySch, SI.JuniorSch, SI.HighSch, SI.University " & _
            "FROM MoneyBackClassInfo AS MB INNER JOIN " & _
            "MoneyBackPayRec AS BP ON MB.ID = BP.MBClassInfoID INNER JOIN " & _
            "SubClass AS SC ON MB.SubClassID = SC.ID INNER JOIN " & _
            "Class AS CL ON SC.ClassID = CL.ID INNER JOIN " & _
            "StuInfo AS SI ON MB.StuID = SI.ID left outer join " & _
            "OrgUser AS US ON BP.HandlerID = US.ID INNER JOIN " & _
            "StuClass As TC ON (MB.StuID = TC.StuID AND MB.SubClassID = TC.SubClassID) " & _
            "WHERE SC.ClassID in ({0})", ClassId)

        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.Text

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Overloads Function GetMBackStaByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetMBackStaByClassNoDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        dt.TableName = c_PayStaByDateTableName
        Return dt

    End Function

    Public Function GetDatePaySta(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetDatePaySta"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetDateKBSta(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetDateKBSta"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPayStaByHandler(ByVal UsrId As String, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetHandlerPaySta"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetStuOweByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetStuOweByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetStuOweByClassLike(ByVal ClassId As String) As DataTable
        Dim strSQL As String = "GetStuOweByClassLike"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.VarChar)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetStuOweByClass(ByVal ClassId As Integer, _
                                               ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetStuOweByClassDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetStuDiscByClassHelper(ByVal ClassId As String) As DataTable
        Dim strSQL As String = String.Format("SELECT distinct FD.Discount, FD.Remarks, FD.SubClassID, FD.StuID " & _
            "FROM FeeDiscount AS FD inner join  " & _
            "SubClass AS SC on FD.SubClassID = SC.ID " & _
            "WHERE SC.ClassID in ({0})", ClassId)
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.Text

        da.Fill(dt)
        Return dt
    End Function

    Public Overloads Function GetStuDiscByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetStuDiscByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetStuDiscByClass(ByVal ClassId As Integer, _
                                               ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetStuDiscByClassDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuMKByClass(ByVal ClassId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetStuMKByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetStuMBByClass(ByVal ClassId As Integer, ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetStuMBByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetJournalGrp() As DataTable
        Dim strSQL As String = "GetJournalGrp"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetReceiptDictType() As DataTable
        Dim strSQL As String = "GetFeeDiscType"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetReceiptDict() As DataTable
        Dim strSQL As String = "GetFeeDiscountDict"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetRemarksDict() As DataTable
        Dim strSQL As String = "GetRemarksDict"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Sub UpdateDiscDictType(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM FeeDiscountDictType"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateDiscDict(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM FeeDiscountDict"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateReceiptRemarks(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM ReceiptDict"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateJournalGrp(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM JournalGroup"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub


    Public Function AddJournalRec(ByVal GroupID As Int32, _
       ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
       ByVal UserID As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
       ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String) As Int32

        Dim lResult As Int32

        Dim strSQL As String = "InsertJournalRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@GroupID", SqlDbType.Int)).Value = GroupID
            .Add(New SqlParameter("@Item", SqlDbType.NVarChar)).Value = Item
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@UserID", SqlDbType.Char)).Value = UserID
            .Add(New SqlParameter("@SerialNum", SqlDbType.NVarChar)).Value = SerialNum
            .Add(New SqlParameter("@ChequeNum", SqlDbType.NVarChar)).Value = ChequeNum
            .Add(New SqlParameter("@Remarks3", SqlDbType.NVarChar)).Value = Remarks3
            .Add(New SqlParameter("@Remarks4", SqlDbType.NVarChar)).Value = Remarks4
            .Add(New SqlParameter("@Remarks5", SqlDbType.NVarChar)).Value = Remarks5
            .Add(New SqlParameter("@JournalID", SqlDbType.Int)).Direction = ParameterDirection.Output
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        lResult = CType(dc.Parameters("@JournalID").Value, Int32)
        Return lResult
    End Function

    Public Sub UpdateJournalRec(ByVal ID As Int32, ByVal GroupID As Int32, _
   ByVal Item As String, ByVal Amount As Int32, ByVal DateTime As Date, _
   ByVal UserID As String, ByVal SerialNum As String, ByVal ChequeNum As String, _
   ByVal Remarks3 As String, ByVal Remarks4 As String, ByVal Remarks5 As String)

        Dim strSQL As String = "UpdJournalRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            .Add(New SqlParameter("@GroupID", SqlDbType.Int)).Value = GroupID
            .Add(New SqlParameter("@Item", SqlDbType.NVarChar)).Value = Item
            .Add(New SqlParameter("@Amount", SqlDbType.Int)).Value = Amount
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
            .Add(New SqlParameter("@UserID", SqlDbType.Char)).Value = UserID
            .Add(New SqlParameter("@SerialNum", SqlDbType.NVarChar)).Value = SerialNum
            .Add(New SqlParameter("@ChequeNum", SqlDbType.NVarChar)).Value = ChequeNum
            .Add(New SqlParameter("@Remarks3", SqlDbType.NVarChar)).Value = Remarks3
            .Add(New SqlParameter("@Remarks4", SqlDbType.NVarChar)).Value = Remarks4
            .Add(New SqlParameter("@Remarks5", SqlDbType.NVarChar)).Value = Remarks5
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub DelJournalRec(ByVal ID As Integer)
        Dim strSQL As String = "DelJournalRec"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Overloads Function GetJournalSta(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetJournalStaByDate"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Overloads Function GetJournalSta() As DataTable
        Dim dt As New DataTable
        dt = DataBaseAccess.JounalRec.GetJournalStaNoDate()
        Return dt
    End Function

    'method: 0-receiptnum, 1-student id, 2-student name
    Public Overloads Function GetPayStaByKeyword(ByVal method As Integer, ByVal keyword As String, ByVal dateFrom As Date, ByVal dateTo As Date) As DataTable
        Dim strSQL As String
        Select Case method
            Case c_KeywordReceiptNum
                strSQL = "GetPayStaByKwDate"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = keyword
                    .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                    .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
                End With
                da.Fill(dt)
                Return dt
            Case c_KeywordStuId
                strSQL = "GetPayStaByKw2Date"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = keyword
                    .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                    .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
                End With
                da.Fill(dt)
                Return dt
            Case Else
                strSQL = "GetPayStaByKw3Date"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = keyword
                    .Add(New SqlParameter("@DateFrom", SqlDbType.DateTime)).Value = dateFrom
                    .Add(New SqlParameter("@DateTo", SqlDbType.DateTime)).Value = dateTo
                End With
                da.Fill(dt)
                Return dt
        End Select

    End Function

    Public Overloads Function GetPayStaByKeyword(ByVal method As Integer, _
                                                  ByVal keyword As String) As DataTable
        Dim strSQL As String
        Select Case method
            Case c_KeywordReceiptNum
                strSQL = "GetPayStaByKwNoDate"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = keyword
                End With
                da.Fill(dt)
                Return dt
            Case c_KeywordStuId
                strSQL = "GetPayStaByKw2NoDate"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.VarChar)).Value = keyword
                End With
                da.Fill(dt)
                Return dt
            Case Else
                strSQL = "GetPayStaByKw3NoDate"
                Dim dt As New DataTable
                Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

                da.SelectCommand.CommandType = CommandType.StoredProcedure
                With da.SelectCommand.Parameters
                    .Add(New SqlParameter("@Keyword", SqlDbType.NVarChar)).Value = keyword
                End With
                da.Fill(dt)
                Return dt
        End Select
    End Function

    Public Function GetDiscountStaByClass(ByVal ClassId As Integer) As DataTable
        Dim strSQL As String = "GetDiscountStaByClass"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassId", SqlDbType.Int)).Value = ClassId
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetChequeValidSta(ByVal DateFrom As Date, ByVal DateTo As Date) As DataTable
        Dim strSQL As String = "GetChequeValidSta"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@DateFrom", SqlDbType.SmallDateTime)).Value = DateFrom
            .Add(New SqlParameter("@DateTo", SqlDbType.SmallDateTime)).Value = DateTo
        End With
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetPayModifyRec() As DataTable
        Dim strSQL As String = "GetPayModifyRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function


    Public Function GetDiscModifyRec() As DataTable
        Dim strSQL As String = "GetDiscModRec"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function
End Class

Friend Class Admin
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    Public Function GetEmployeeList() As DataTable
        Dim strSQL As String = "GetEmployeeList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetSalesList() As DataTable
        Dim strSQL As String = "GetSalesList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Function GetUserNameList() As DataTable
        Dim strSQL As String = "GetUserNameList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt

    End Function

    Public Sub ModifyUsrStatus(ByVal UsrID As String, ByVal Status As Int32)
        Dim strSQL As String = "UpdUsrAccnt"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char)).Value = UsrID
            .Add(New SqlParameter("@Status", SqlDbType.Int)).Value = Status
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

    Public Sub DeleteUsrByID(ByVal UsrID As String)
        Dim strSQL As String = "DelUsrByID"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char)).Value = UsrID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeleteUsrLoginFail(ByVal UsrID As String)
        Dim strSQL As String = "DelUsrLoginFail"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char)).Value = UsrID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DelUsrAuth(ByVal ID As String)
        Dim strSQL As String = "DelUsrAuth"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub ModifyUsrCard(ByVal UsrID As String, ByVal CardNum As String)
        Dim strSQL As String = "UpdUsrCard"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@CardNum", SqlDbType.Char, 10)).Value = CardNum
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub ModifyUsrPwd(ByVal UsrID As String, ByVal PWD As String)
        Dim strSQL As String = "UpdUsrPwd"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@PWD", SqlDbType.VarChar)).Value = PWD
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub AddUser(ByVal ID As String, ByVal Name As String, ByVal PWD As String, _
         ByVal Modify As Int32, ByVal Status As Int32, ByVal GroupID As Int32, _
         ByVal CardNum As String, ByVal Phone1 As String, ByVal Phone2 As String, _
         ByVal IC As String, ByVal Mobile As String, ByVal Guardian As String, _
         ByVal GuardMobile As String, ByVal PostalCode As String, _
         ByVal Address As String, ByVal Email As String, ByVal Remarks As String, _
         ByVal Sex As String, ByVal EnglishName As String, ByVal Birthday As Date, _
         ByVal Horoscope As String, ByVal AccntName As String, _
         ByVal OnBoardDate As Date, ByVal BloodType As Int32, _
         ByVal JuniorSchID As Int32, ByVal HighSchID As Int32, _
         ByVal UniversityID As Int32, ByVal IsSales As Int32)

        Dim strSQL As String = "InsertUsr"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char)).Value = ID
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@PWD", SqlDbType.VarChar)).Value = PWD
            .Add(New SqlParameter("@Modify", SqlDbType.Bit)).Value = Modify
            .Add(New SqlParameter("@Status", SqlDbType.SmallInt)).Value = Status
            .Add(New SqlParameter("@GroupID", SqlDbType.Int)).Value = GroupID
            .Add(New SqlParameter("@CardNum", SqlDbType.Char)).Value = CardNum
            .Add(New SqlParameter("@Phone1", SqlDbType.VarChar)).Value = Phone1
            .Add(New SqlParameter("@Phone2", SqlDbType.VarChar)).Value = Phone2
            .Add(New SqlParameter("@IC", SqlDbType.VarChar)).Value = IC
            .Add(New SqlParameter("@Mobile", SqlDbType.VarChar)).Value = Mobile
            .Add(New SqlParameter("@Guardian", SqlDbType.NVarChar)).Value = Guardian
            .Add(New SqlParameter("@GuardMobile", SqlDbType.VarChar)).Value = GuardMobile
            .Add(New SqlParameter("@PostalCode", SqlDbType.VarChar)).Value = PostalCode
            .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = Address
            .Add(New SqlParameter("@Email", SqlDbType.VarChar)).Value = Email
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = Remarks
            .Add(New SqlParameter("@Sex", SqlDbType.NVarChar)).Value = Sex
            .Add(New SqlParameter("@EnglishName", SqlDbType.VarChar)).Value = EnglishName
            .Add(New SqlParameter("@Birthday", SqlDbType.SmallDateTime)).Value = Birthday
            .Add(New SqlParameter("@Horoscope", SqlDbType.NVarChar)).Value = Horoscope
            .Add(New SqlParameter("@AccntName", SqlDbType.VarChar)).Value = AccntName
            .Add(New SqlParameter("@OnBoardDate", SqlDbType.SmallDateTime)).Value = OnBoardDate
            .Add(New SqlParameter("@BloodType", SqlDbType.SmallInt)).Value = BloodType
            .Add(New SqlParameter("@JuniorSchID", SqlDbType.SmallInt)).Value = JuniorSchID
            .Add(New SqlParameter("@HighSchID", SqlDbType.SmallInt)).Value = HighSchID
            .Add(New SqlParameter("@UniversityID", SqlDbType.SmallInt)).Value = UniversityID
            .Add(New SqlParameter("@IsSales", SqlDbType.Bit)).Value = IsSales
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Sub AddUsrAuth(ByVal UsrID As String, ByVal ItemID As Integer, _
                          ByVal ViewAllClass As Byte)
        Dim strSQL As String = "InsertUsrAuth"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@ItemID", SqlDbType.Int)).Value = ItemID
            .Add(New SqlParameter("@ViewAllClass", SqlDbType.Bit)).Value = ViewAllClass
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddSubClassAuth(ByVal UsrID As String, ByVal SubClassID As Integer)
        Dim strSQL As String = "InsertSubClassAuth"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = SubClassID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddClassAuth(ByVal UsrID As String, ByVal ClassID As Integer)
        Dim strSQL As String = "InsertClassAuth"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub AddAllClassAuth(ByVal UsrID As String)
        Dim strSQL As String = "InsertAllClassAuth"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = UsrID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Function GetUsrList() As DataTable
        Dim strSQL As String = "GetUsrList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        Return dt

    End Function

    Public Function GetUsrByID(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetUsrByID"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetUsrPWD(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetUsrPWD"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetUsrLoginFail(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetUsrLoginFail"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Sub InsertUsrLoginFail(ByVal User As String, ByVal PCName As String, _
                                 ByVal IncorrectPwd As String, ByVal state As Byte)
        Dim strSQL As String = "InsertUsrLoginFail"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@User", SqlDbType.Char, 8)).Value = User
            .Add(New SqlParameter("@PCName", SqlDbType.NVarChar)).Value = PCName
            .Add(New SqlParameter("@IncorrectPwd", SqlDbType.VarChar)).Value = IncorrectPwd
            .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = Now
            .Add(New SqlParameter("@State", SqlDbType.Bit)).Value = 1
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub UpdateUsrInfo(ByVal dt As DataTable)
        If dt.Rows.Count = 0 Then
            Exit Sub
        End If
        Dim strSQL As String = "UpdUsrInfo"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Char)).Value = dt.Rows(0).Item(c_IDColumnName)
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_NameColumnName)
            .Add(New SqlParameter("@Phone1", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_Phone1ColumnName)
            .Add(New SqlParameter("@Phone2", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_Phone2ColumnName)
            .Add(New SqlParameter("@IC", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_ICColumnName)
            .Add(New SqlParameter("@Mobile", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_MobileColumnName)
            .Add(New SqlParameter("@Guardian", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_GuardianColumnName)
            .Add(New SqlParameter("@GuardMobile", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_GuardMobileColumnName)
            .Add(New SqlParameter("@PostalCode", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_PostalCodeColumnName)
            .Add(New SqlParameter("@Address", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_AddressColumnName)
            .Add(New SqlParameter("@Email", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_EmailColumnName)
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_RemarksColumnName)
            .Add(New SqlParameter("@Sex", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_SexColumnName)
            .Add(New SqlParameter("@EnglishName", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_EnglishNameColumnName)
            .Add(New SqlParameter("@Birthday", SqlDbType.SmallDateTime)).Value = dt.Rows(0).Item(c_BirthdayColumnName)
            .Add(New SqlParameter("@Horoscope", SqlDbType.NVarChar)).Value = dt.Rows(0).Item(c_HoroscopeColumnName)
            .Add(New SqlParameter("@AccntName", SqlDbType.VarChar)).Value = dt.Rows(0).Item(c_AccntNameColumnName)
            .Add(New SqlParameter("@OnBoardDate", SqlDbType.SmallDateTime)).Value = dt.Rows(0).Item(c_OnBoardDateColumnName)
            .Add(New SqlParameter("@BloodType", SqlDbType.SmallInt)).Value = dt.Rows(0).Item(c_BloodTypeColumnName)
            .Add(New SqlParameter("@JuniorSchID", SqlDbType.SmallInt)).Value = dt.Rows(0).Item(c_JuniorSchIDColumnName)
            .Add(New SqlParameter("@HighSchID", SqlDbType.SmallInt)).Value = dt.Rows(0).Item(c_HighSchIDColumnName)
            .Add(New SqlParameter("@UniversityID", SqlDbType.SmallInt)).Value = dt.Rows(0).Item(c_UniversityIDColumnName)
            .Add(New SqlParameter("@IsSales", SqlDbType.Bit)).Value = dt.Rows(0).Item(c_IsSalesColumnName)
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub UpdateUsrCard(ByVal id As String, ByVal card As String)
        Dim strSQL As String = "UpdUsrCard"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@UsrID", SqlDbType.Char, 8)).Value = id
            .Add(New SqlParameter("@CardNum", SqlDbType.Char, 10)).Value = card
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Function GetAuthorityItem() As DataTable
        Dim strSQL As String = "GetAuthorityItem"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetUsrAuthority(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetUsrAuthority"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetClassAuthority(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetClassAuthority"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetAllClassAuthority(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetAllClassAuthority"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetGroupAuth(ByVal ID As Integer) As DataTable
        Dim strSQL As String = "GetGroupAuth"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetSubClassAuthority(ByVal UsrID As String) As DataTable
        Dim strSQL As String = "GetSubClassAuthority"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@UsrId", SqlDbType.Char, 8)).Value = UsrID
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Sub UpdateUsrAuthority(ByVal ChangedUser As DataTable)
        Dim strId As String

        If IsNothing(ChangedUser) Then
            Exit Sub
        End If
        strId = ChangedUser.Rows(0).Item(c_UsrIDColumnName)
        Dim strSQL As String = "SELECT * FROM UserAuthority WHERE UsrID = " + strId
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangedUser)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub UpdateClassAuthority(ByVal ChangedUser As DataTable)
        Dim strId As String

        If IsNothing(ChangedUser) Then
            Exit Sub
        End If
        strId = ChangedUser.Rows(0).Item(c_UsrIDColumnName)
        Dim strSQL As String = "SELECT * FROM ClassAuthority WHERE UsrID = " + strId
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangedUser)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Function GetReceiptPrint() As DataTable
        Dim strSQL As String = "GetReceiptPrint"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        da.Fill(dt)
        Return dt
    End Function

    Public Sub UpdateReceiptPrint(ByVal ChangeTb As DataTable)

        If IsNothing(ChangeTb) Then
            Exit Sub
        End If

        Dim strSQL As String = "SELECT * FROM ReceiptPrint"
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString)

        Dim cb As New SqlCommandBuilder(da)
        da.UpdateCommand = cb.GetUpdateCommand

        Try
            da.Update(ChangeTb)
        Catch ex As Exception
            Throw New Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

End Class

Friend Class ExamPaper
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    Public Function GetClassPaper(ByVal classid As Integer) As DataTable
        Dim strSQL As String = "GetClassPaper"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = classid
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetClassPaperList(ByVal classid As Integer) As DataTable
        Dim strSQL As String = "GetClassPaperList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = classid
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuGrades(ByVal paperId As Integer, ByVal sc As Integer) As DataTable
        Dim strSQL As String = "GetStuGradesByPaperId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@PaperID", SqlDbType.Int)).Value = paperId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = sc
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetPaperCnt() As Integer
        Dim strSQL As String = "GetPaperCnt"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(c_StuRegCntColumnName)
        Else
            Return 0
        End If

    End Function

    Public Function GetAllPaperIDs() As ArrayList
        Dim strSQL As String = "GetAllExamPaperIDs"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        Dim lst As New ArrayList

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        For Each row As DataRow In dt.Rows
            lst.Add(row.Item(c_IDColumnName))
        Next
        Return lst

    End Function

    Public Function AddPaper(ByVal Name As String, ByVal Abbr As String, _
                          ByVal PaperDate As Date, ByVal content As Integer, _
                            ByVal lstSc As ArrayList) As Int32

        Dim strSQL As String = "InsertExamPaper"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim id As Integer = GetPaperCnt() + 1001
        Dim lst As ArrayList = GetAllPaperIDs()
        Dim flag As Boolean = False

        Do Until flag
            If lst.Contains(id) = False Then
                flag = True
            Else
                id = id + 1
            End If
        Loop

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@Abbr", SqlDbType.NVarChar)).Value = Abbr
            .Add(New SqlParameter("@DateTime", SqlDbType.SmallDateTime)).Value = PaperDate
            .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = content
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        For i As Integer = 0 To lstSc.Count - 1
            If lstSc(i) > 0 Then
                AddPaperOfClass(id, lstSc(i))
            End If
        Next

        Return id
    End Function

    Public Sub UpdatePaper(ByVal ID As Integer, ByVal Name As String, ByVal Abbr As String, _
                      ByVal PaperDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList)
        Try
            Dim strSQL As String = "UpdExamPaper"
            Dim cn As New SqlConnection(GetConnectionString)
            Dim dc As New SqlCommand

            cn.Open()

            dc.Connection = cn
            dc.CommandText = strSQL
            dc.CommandType = CommandType.StoredProcedure
            With dc.Parameters
                .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
                .Add(New SqlParameter("@Abbr", SqlDbType.NVarChar)).Value = Abbr
                '.Add(New SqlParameter("@DateTime", SqlDbType.SmallDateTime)).Value = PaperDate
                .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = PaperDate '--------2011/2/18
                .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = content
                .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
            End With

            Try
                dc.ExecuteNonQuery()
                cn.Close()

            Catch ex As Exception
                CSOL.Logger.LogError(ex.ToString())
                Throw New System.Exception(c_DataAccessErrMessage, ex)
            End Try

            DeletePaperOfClass(ID)

            For i As Integer = 0 To lstSc.Count - 1
                If lstSc(i) > 0 Then
                    AddPaperOfClass(ID, lstSc(i))
                End If
            Next
        Catch ex As Exception
            CSOL.Logger.LogError(ex.ToString())
            Throw ex
        End Try
    End Sub

    Public Sub AddPaperOfClass(ByVal PaperID As Integer, ByVal ScID As Integer)

        Dim strSQL As String = "InsertPaperOfClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@PaperID", SqlDbType.Int)).Value = PaperID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = ScID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeletePaperOfClass(ByVal ID As Integer)

        Dim strSQL As String = "DelPaperOfClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeletePaper(ByVal id As Integer)
        Dim strSQL As String = "DelExamPaper"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub InsertStuGrade(ByVal stuId As String, ByVal paperId As Integer, ByVal mark As Single, _
                              ByVal dt As Date, ByVal remarks As String, ByVal inputby As String)
        Dim strSQL As String = "InsertStuGrade"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char)).Value = stuId
            .Add(New SqlParameter("@PaperID", SqlDbType.Int)).Value = paperId
            If mark = c_NullMark Then
                .Add(New SqlParameter("@Mark", SqlDbType.Float)).Value = DBNull.Value
            Else
                .Add(New SqlParameter("@Mark", SqlDbType.Float)).Value = mark
            End If
            .Add(New SqlParameter("@DateTime", SqlDbType.SmallDateTime)).Value = dt
            .Add(New SqlParameter("@Remarks", SqlDbType.NVarChar)).Value = remarks
            .Add(New SqlParameter("@InputBy", SqlDbType.Char)).Value = inputby
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try



    End Sub

    Public Sub DeleteStuGrade(ByVal stuid As String, ByVal paperid As Integer)
        Dim strSQL As String = "DelStuGrade"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char)).Value = stuid
            .Add(New SqlParameter("@PaperID", SqlDbType.Int)).Value = paperid
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    'Public Sub AddPaperIssueRec(ByVal StuID As String, _
    '   ByVal DateTime As Date, _
    '   ByVal ClassID As Integer, _
    '   ByVal PaperID As Integer)

    '    Dim strSQL As String = "InsertPaperIssueRec"
    '    Dim cn As New SqlConnection(GetConnectionString)
    '    Dim dc As New SqlCommand

    '    cn.Open()

    '    dc.Connection = cn
    '    dc.CommandText = strSQL
    '    dc.CommandType = CommandType.StoredProcedure
    '    With dc.Parameters
    '        .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
    '        .Add(New SqlParameter("@DateTime", SqlDbType.DateTime)).Value = DateTime
    '        .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = ClassID
    '        .Add(New SqlParameter("@PaperID", SqlDbType.Int)).Value = PaperID

    '    End With

    '    Try
    '        dc.ExecuteNonQuery()
    '        cn.Close()

    '    Catch ex As Exception
    '        Throw New System.Exception(c_DataAccessErrMessage, ex)
    '    End Try
    'End Sub
End Class

Friend Class Assignment
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringKeyName), String)
        Return CSOL.DataBase.ConnectionString

    End Function

    Public Function GetClassAssignment(ByVal classid As Integer) As DataTable
        Dim strSQL As String = "GetClassAssignment"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = classid
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetClassAssignmentList(ByVal classid As Integer) As DataTable
        Dim strSQL As String = "GetClassAssignmentList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ClassID", SqlDbType.Int)).Value = classid
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuAssignments(ByVal AssignmentId As Integer, ByVal sc As Integer) As DataTable
        Dim strSQL As String = "GetStuAssignByAssignId"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = AssignmentId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = sc
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuDoneAssignmentList(ByVal AssignmentId As Integer, ByVal sc As Integer) As DataTable
        Dim strSQL As String = "GetStuDoneAssignList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = AssignmentId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = sc
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetStuNoAssignmentList(ByVal AssignmentId As Integer, ByVal sc As Integer) As DataTable
        Dim strSQL As String = "GetStuNoAssign"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = AssignmentId
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = sc
        End With
        da.Fill(dt)
        Return dt
    End Function

    Public Function GetAssignmentCnt() As Integer
        Dim strSQL As String = "GetAssignmentCnt"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        If dt.Rows.Count > 0 Then
            Return dt.Rows(0).Item(c_StuRegCntColumnName)
        Else
            Return 0
        End If

    End Function

    Public Function GetAllAssignmentIDs() As ArrayList
        Dim strSQL As String = "GetAllAssignIDs"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        Dim lst As New ArrayList

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)
        For Each row As DataRow In dt.Rows
            lst.Add(row.Item(c_IDColumnName))
        Next
        Return lst

    End Function

    Public Function AddAssignment(ByVal Name As String, ByVal StartDate As Date, _
                          ByVal EndDate As Date, ByVal content As Integer, _
                            ByVal lstSc As ArrayList) As Int32

        Dim strSQL As String = "InsertAssignment"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim id As Integer = GetAssignmentCnt() + 1001
        Dim lst As ArrayList = GetAllAssignmentIDs()
        Dim flag As Boolean = False

        Do Until flag
            If lst.Contains(id) = False Then
                flag = True
            Else
                id = id + 1
            End If
        Loop

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@StartDate", SqlDbType.SmallDateTime)).Value = StartDate
            .Add(New SqlParameter("@EndDate", SqlDbType.SmallDateTime)).Value = EndDate
            .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = content
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        For i As Integer = 0 To lstSc.Count - 1
            If lstSc(i) > 0 Then
                AddAssignmentOfClass(id, lstSc(i))
            End If
        Next

        Return id
    End Function

    Public Sub UpdateAssignment(ByVal ID As Integer, ByVal Name As String, ByVal StartDate As Date, _
                      ByVal EndDate As Date, ByVal content As Integer, _
                        ByVal lstSc As ArrayList)

        Dim strSQL As String = "UpdAssignment"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@Name", SqlDbType.NVarChar)).Value = Name
            .Add(New SqlParameter("@StartDate", SqlDbType.SmallDateTime)).Value = StartDate
            .Add(New SqlParameter("@EndDate", SqlDbType.SmallDateTime)).Value = EndDate
            .Add(New SqlParameter("@ContentID", SqlDbType.Int)).Value = content
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

        DeleteAssignmentOfClass(ID)

        For i As Integer = 0 To lstSc.Count - 1
            If lstSc(i) > 0 Then
                AddAssignmentOfClass(ID, lstSc(i))
            End If
        Next

    End Sub

    Public Sub AddAssignmentOfClass(ByVal AssignmentID As Integer, ByVal ScID As Integer)

        Dim strSQL As String = "InsertAssignOfClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = AssignmentID
            .Add(New SqlParameter("@SubClassID", SqlDbType.Int)).Value = ScID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try

    End Sub

    Public Sub DeleteAssignmentOfClass(ByVal ID As Integer)

        Dim strSQL As String = "DelAssignOfClass"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = ID
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub DeleteAssignment(ByVal id As Integer)
        Dim strSQL As String = "DelAssignment"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            ' Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub

    Public Sub InsertStuAssignment(ByVal stuId As String, ByVal assignmentId As Integer, ByVal dt As Date)
        Dim strSQL As String = "InsertStuAssignmentDone"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char)).Value = stuId
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = assignmentId
            .Add(New SqlParameter("@DateTime", SqlDbType.SmallDateTime)).Value = dt
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try



    End Sub

    Public Sub DeleteStuAssignment(ByVal stuid As String, ByVal assignmentid As Integer)
        Dim strSQL As String = "DelStuAssignmentDone"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char)).Value = stuid
            .Add(New SqlParameter("@AssignmentID", SqlDbType.Int)).Value = assignmentid
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try

    End Sub
End Class
'20100424 imgdb by sherry start
Friend Class Picture
    Private Function GetConnectionString() As String
        'Return CType(ConfigurationManager.AppSettings(c_ConnectionStringImgKeyName), String)
        Return CSOL.DataBase.ConnectionStringImage

    End Function

    Public Function GetImgList() As DataTable
        Dim strSQL As String = "GetImgList"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())

        da.SelectCommand.CommandType = CommandType.StoredProcedure

        da.Fill(dt)

        Return dt

    End Function

    Public Sub UploadImg(ByVal StuID As String, ByVal img As Byte())

        Dim strSQL As String = "UploadImg"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim pic As SqlParameter = Nothing

        pic = New SqlParameter("@Picture", SqlDbType.Image)
        pic.Value = img

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure

        With dc.Parameters
            .Add(New SqlParameter("@StuID", SqlDbType.Char, 8)).Value = StuID
            .Add(pic)
        End With

        'Try
        dc.ExecuteNonQuery()
        cn.Close()

        'Catch ex As Exception
        '    Throw New System.Exception(c_DataAccessErrMessage, ex)
        'End Try
    End Sub

    Public Sub ModImg(ByVal id As Integer, ByVal img As Byte())

        Dim strSQL As String = "ModImg"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        Dim pic As SqlParameter = Nothing

        pic = New SqlParameter("@Picture", SqlDbType.Image)
        pic.Value = img

        cn.Open()

        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure

        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
            .Add(pic)
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()

        Catch ex As Exception
            Throw New System.Exception(c_DataAccessErrMessage, ex)
        End Try
    End Sub

    Public Function DownloadImg(ByVal id As Integer) As DataTable
        Dim strSQL As String = "DownloadImg"
        Dim dt As New DataTable
        Dim da As New SqlDataAdapter(strSQL, GetConnectionString())
        Dim result As Byte() = Nothing

        da.SelectCommand.CommandType = CommandType.StoredProcedure
        With da.SelectCommand.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        da.Fill(dt)
        Return dt

    End Function

    Public Sub DelImg(ByVal id As Integer)
        Dim strSQL As String = "DelImg"
        Dim cn As New SqlConnection(GetConnectionString)
        Dim dc As New SqlCommand
        cn.Open()
        dc.Connection = cn
        dc.CommandText = strSQL
        dc.CommandType = CommandType.StoredProcedure
        With dc.Parameters
            .Add(New SqlParameter("@ID", SqlDbType.Int)).Value = id
        End With

        Try
            dc.ExecuteNonQuery()
            cn.Close()
        Catch ex As Exception
            'Throw New System.Exception(c_DataAccessErrMessage, ex)
        Finally
            cn.Close()
        End Try
    End Sub

End Class
'20100424 imgdb by sherry end