﻿Namespace CSOL.Report.Accounting
    Public Class MonthlyReport
        Private m_ReportName As String = "班內收支月報表"
        Public Property ReportName() As String
            Get
                Return Me.m_ReportName
            End Get
            Set(ByVal value As String)
                Me.m_ReportName = value
            End Set
        End Property

        Private m_Printer As String = My.Settings.Printer
        Private m_Report As New CSOL.Report.Advanced()
        Private m_ReportData As DataTable
        Private m_ReportDate As Date
        Private m_User As String

        Private m_ColumnHeaders() As String
        Private m_Departments() As String
        Private m_SumTypes() As String
        Private m_PayTypes() As String
        Private m_Summary As New DataTable()

        Private m_Title As String = "班內收支月報表"

        Private m_TitleFont As New Font("arial", ShortCut.MT.ci2mm(26), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_HeaderFont As New Font("arial", ShortCut.MT.ci2mm(12), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_ContentFont As New Font("arial", ShortCut.MT.ci2mm(10), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_CommentFont As New Font("arial", ShortCut.MT.ci2mm(9), FontStyle.Regular, GraphicsUnit.Millimeter)

        Public Sub New(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal ReportDate As Date, ByVal User As String)
            Me.m_Departments = Departments
            Me.m_ReportData = ReportData
            Me.m_ReportDate = ReportDate
            Me.m_User = User
        End Sub

        Public Sub Print()
            BeginPrint()
            For Each department As String In Me.m_Departments
                DrawTitle(department)
                Dim ColumnHeaderWidths() As Double = GetColumnHeaderWidths()
                Dim ColumnHeaderHeight As Double = GetColumnHeaderHeight()
                Dim Height As Double = GetHeight()

                Dim AmountWidths As New List(Of Double)
                AmountWidths.AddRange(ColumnHeaderWidths)
                AmountWidths.RemoveAt(0)

                Me.m_Report.PrintHeader(Me.m_ColumnHeaders, Me.m_HeaderFont, ColumnHeaderWidths, ColumnHeaderHeight)

                Dim Days As Integer = Date.Parse(Me.m_ReportDate.ToString("yyyy-MM-01")).AddMonths(1).AddDays(-1).Day
                For day As Integer = 1 To Days
                    Dim rows() As DataRow = Me.m_Summary.Select(String.Format("日 = {0}", day))
                    Me.m_Report.PrintCell(String.Format("{0:d2}", day), Me.m_ContentFont, ShortCut.SF.c("cm"), New SizeF(ColumnHeaderWidths(0), Height * rows.Length))
                    Me.m_Report.MoveCursorX(-1 * ColumnHeaderWidths(0))
                    For Each row As DataRow In rows
                        Me.m_Report.MoveCursorX(ColumnHeaderWidths(0))
                        Dim cells As New List(Of String)
                        Dim alignments As New List(Of String)
                        cells.Add(row("支付方式"))
                        alignments.Add("cm")
                        For Each sumType As String In Me.m_SumTypes
                            If row(sumType) = 0 Then
                                cells.Add("--")
                                alignments.Add("cm")
                            Else
                                cells.Add(String.Format("{0:$#,0}", row(sumType)))
                                alignments.Add("rm")
                            End If
                        Next
                        cells.Add(String.Format("{0:$#,0}", row("支付總計")))
                        alignments.Add("rm")

                        Me.m_Report.PrintRow(cells.ToArray(), Me.m_ContentFont, AmountWidths.ToArray(), Height, String.Join(",", alignments.ToArray()))
                    Next
                Next

                AmountWidths(0) += ColumnHeaderWidths(0)
                Dim rowSum As DataRow = Me.m_Summary.Rows(Me.m_Summary.Rows.Count - 1)
                Dim sumCells As New List(Of String)
                Dim sumAlignments As New List(Of String)
                sumCells.Add("金額總計：")
                sumAlignments.Add("rm")
                For Each sumType As String In Me.m_SumTypes
                    sumCells.Add(String.Format("{0:$#,0}", rowSum(sumType)))
                    sumAlignments.Add("rm")
                Next
                sumCells.Add(String.Format("{0:$#,0}", rowSum("支付總計")))
                sumAlignments.Add("rm")
                Me.m_Report.PrintRow(sumCells.ToArray(), Me.m_ContentFont, AmountWidths.ToArray(), Height, String.Join(",", sumAlignments.ToArray()))
            Next
            'Departments
            EndPrint()
        End Sub

        Private Sub BeginPrint()
            Me.m_Report.Name = Me.m_ReportName
            Me.m_Report.Printer = Me.m_Printer
            Me.m_SumTypes = GetSumTypes()
            Me.m_PayTypes = GetPayTypes()

            GetSummary()
            Me.m_ColumnHeaders = GetColumnHeaders()
        End Sub

        Private Sub EndPrint()
            Me.m_Report.DrawPageNumber()
            Me.m_Report.Print()
        End Sub

        Private Sub GetSummary()
            CreateSummary()
            CalcSummary()
        End Sub

        Private Sub CreateSummary()
            Me.m_Summary.Columns.Add("日", System.Type.GetType("System.Int32"))
            Me.m_Summary.Columns.Add("支付方式", System.Type.GetType("System.String"))
            For Each sumType As String In Me.m_SumTypes
                Me.m_Summary.Columns.Add(sumType, System.Type.GetType("System.Int32"))
            Next
            Me.m_Summary.Columns.Add("支付總計", System.Type.GetType("System.Int32"))
        End Sub

        Private Sub CalcSummary()
            Dim rowSum As DataRow = Me.m_Summary.NewRow()
            rowSum("日") = 999
            rowSum("支付方式") = ""
            For Each sumType As String In Me.m_SumTypes
                rowSum(sumType) = 0
            Next
            rowSum("支付總計") = 0

            Dim Days As Integer = Date.Parse(Me.m_ReportDate.ToString("yyyy-MM-01")).AddMonths(1).AddDays(-1).Day
            For day As Integer = 1 To Days
                Dim CurrentDate As Date = String.Format("{0:yyyy-MM}-{1:d2}", Me.m_ReportDate, day)
                Dim rowsReportData() As DataRow = Me.m_ReportData.Select(String.Format("Date = '{0:yyyy-MM-dd}'", CurrentDate))
                If rowsReportData.Length = 0 Then
                    Dim row As DataRow = Me.m_Summary.NewRow()
                    row("日") = day
                    row("支付方式") = "--"
                    For Each sumType As String In Me.m_SumTypes
                        row(sumType) = 0
                    Next
                    row("支付總計") = 0
                    Me.m_Summary.Rows.Add(row)
                Else
                    For Each rowReportData As DataRow In rowsReportData
                        Dim sumType As String = rowReportData("SumType")
                        Dim payType As String = rowReportData("PayType")
                        Dim payMethod As String = rowReportData("PayMethod")
                        Dim Amount As Integer = rowReportData("Amount")

                        Dim rowsSummary() As DataRow = Me.m_Summary.Select(String.Format("日 = {0} AND 支付方式 = '{1}'", day, payMethod))
                        If rowsSummary.Length = 0 Then
                            Dim row As DataRow = Me.m_Summary.NewRow()
                            row("日") = day
                            row("支付方式") = payMethod
                            For Each st As String In Me.m_SumTypes
                                If st = sumType Then
                                    row(sumType) = Amount
                                    rowSum(sumType) += Amount
                                    If st = "收入" Then
                                        row("支付總計") = Amount
                                        rowSum("支付總計") += Amount
                                    ElseIf st = "支出" Then
                                        row("支付總計") = -1 * Amount
                                        rowSum("支付總計") -= Amount
                                    Else
                                        row("支付總計") = 0
                                    End If
                                Else
                                    row(st) = 0
                                End If
                            Next
                            Me.m_Summary.Rows.Add(row)
                        Else
                            Dim row As DataRow = rowsSummary(0)
                            row(sumType) += Amount
                            rowSum(sumType) += Amount
                            If sumType = "收入" Then
                                row("支付總計") += Amount
                                rowSum("支付總計") += Amount
                            ElseIf sumType = "支出" Then
                                row("支付總計") -= Amount
                                rowSum("支付總計") -= Amount
                            End If
                        End If
                    Next
                End If
            Next
            Me.m_Summary.Rows.Add(rowSum)
        End Sub

        Private Sub DrawTitle(ByVal department As String)
            Me.m_Report.AddPage()
            Dim sz As SizeF = Me.m_Report.Drawer.MeasureString(Me.m_Title, Me.m_TitleFont)
            Me.m_Report.Drawer.DrawString(String.Format("列印時間：{0:d3}{1:/MM/dd HH:mm:ss}{2}列印人員：{3}", Now.Year - 1911, Now, vbCrLf, Me.m_User), Me.m_CommentFont, Brushes.Black, Me.m_Report.PrintArea, ShortCut.SF.c("lt"))
            Me.m_Report.PrintString(String.Format("{0}({1}年{2:M月})", Me.m_Title, Me.m_ReportDate.Year - 1911, Me.m_ReportDate), Me.m_TitleFont, Brushes.Black, New SizeF(Me.m_Report.PrintArea.Width, sz.Height), ShortCut.SF.c("ct"))
            Me.m_Report.Drawer.DrawString(String.Format("分部：{0}", department), Me.m_CommentFont, Brushes.Black, Me.m_Report.PrintArea, ShortCut.SF.c("rt"))
            Me.m_Report.LineBreak()

            DrawSeperatorLine()
            Me.m_Report.MoveCursorY(4.0F)
        End Sub

        Private Function GetColumnHeaders() As String()
            Return Me.m_Report.GetTableHeaderArray(Me.m_Summary)
        End Function

        Private Function GetColumnHeaderWidths() As Double()
            Dim ColumnHeaderWidths() As Double = Me.m_Report.GetColumnWidths(Me.m_ColumnHeaders, Me.m_HeaderFont)
            ColumnHeaderWidths = Me.m_Report.AllocateWidthEqually(Me.m_Report.PrintArea.Width, ColumnHeaderWidths)
            Return ColumnHeaderWidths
        End Function

        Private Function GetColumnHeaderHeight() As Double
            Dim ColumnHeaderHeight As Double = Me.m_Report.GetColumnHeight(Me.m_ColumnHeaders, Me.m_HeaderFont)
            Return ColumnHeaderHeight
        End Function

        Private Function GetHeight() As Double
            Dim Height As Double = Me.m_Report.GetColumnHeight(Me.m_ColumnHeaders, Me.m_ContentFont)
            Return Height
        End Function

        Private Function GetSumTypes() As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs("FeeSumType")
            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Function GetPayTypes() As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs("FeePayType")
            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Function GetPayMethods(ByVal PayType As String) As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs(String.Format("FeePayType-{0}", PayType))
            If dt.Rows.Count = 0 Then
                dt = CSOL.Data.GetNameValuePairs("FeePayMethod")
            End If

            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Sub DrawSeperatorLine()
            Dim x As Single = Me.m_Report.PrintArea.X
            Dim x2 As Single = x + Me.m_Report.PrintArea.Width
            Dim y As Single = Me.m_Report.Cursor.Y

            Me.m_Report.Drawer.DrawLine(New Pen(Color.Gray, 0.3), x, y, x2, y)
            Me.m_Report.MoveCursorY(0.5)
        End Sub

        'Fast Method
        Public Shared Sub DirectPrint(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal ReportDate As Date, ByVal User As String)
            Dim report As New MonthlyReport(Departments, ReportData, ReportDate, User)
            report.Print()
        End Sub
        Public Shared Sub DirectPrint(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal User As String, ByVal ReportDate As Date, ByVal ReportName As String)
            Dim report As New MonthlyReport(Departments, ReportData, ReportDate, User)
            report.ReportName = ReportName
            report.Print()
        End Sub
    End Class
End Namespace