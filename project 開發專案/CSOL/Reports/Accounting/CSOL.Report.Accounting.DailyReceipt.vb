﻿Namespace CSOL.Report.Accounting
    Public Class DailyReport
        Private m_ReportName As String = "班內收支日報表"
        Public Property ReportName() As String
            Get
                Return Me.m_ReportName
            End Get
            Set(ByVal value As String)
                Me.m_ReportName = value
            End Set
        End Property

        Private m_Printer As String = My.Settings.Printer
        Private m_Report As New CSOL.Report.Advanced()
        Private m_ReportData As DataTable
        Private m_ReportDate As Date
        Private m_User As String

        Private m_Departments() As String
        Private m_SumTypes() As String
        Private m_PayTypes() As String
        Private m_Summary As New DataTable()

        Private m_Title As String = "班內收支日報表"

        Private m_TitleFont As New Font("arial", ShortCut.MT.ci2mm(26), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_HeaderFont As New Font("arial", ShortCut.MT.ci2mm(12), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_ContentFont As New Font("arial", ShortCut.MT.ci2mm(10), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_CommentFont As New Font("arial", ShortCut.MT.ci2mm(9), FontStyle.Regular, GraphicsUnit.Millimeter)

        Public Sub New(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal ReportDate As Date, ByVal User As String)
            Me.m_Departments = Departments
            Me.m_ReportData = ReportData
            Me.m_ReportDate = ReportDate
            Me.m_User = User
        End Sub

        Public Sub Print()
            BeginPrint()
            For Each department As String In Me.m_Departments
                DrawTitle(department)
                Dim ColumnHeaderWidths() As Double = GetColumnHeaderWidths()
                Dim ColumnHeaderHeight As Double = GetColumnHeaderHeight()
                Dim Height As Double = GetHeight()

                Me.m_Report.BorderStyle = New Pen(Color.White)
                For Each sumType As String In Me.m_SumTypes
                    Dim SumTotal As Double = 0.0F

                    Dim SumTypeSectionHeight As Double = ColumnHeaderHeight * 2 + Height * 2 + 4.0F
                    If PageBreakDetect(SumTypeSectionHeight) Then
                        DrawTitle(department)
                    End If

                    DrawSumType(sumType)
                    If Me.m_ReportData.Select(String.Format("Department = '{0}' AND SumType = '{1}'", department, sumType)).Length = 0 Then
                        DrawNoData()
                    Else
                        For Each payType As String In Me.m_PayTypes
                            Dim PayMethods() As String = GetPayMethods(payType)
                            For Each payMethod As String In PayMethods
                                Dim rows() As DataRow = Me.m_ReportData.Select(String.Format("Department = '{0}' AND SumType = '{1}' AND PayType = '{2}' AND PayMethod = '{3}'", department, sumType, payType, payMethod))
                                If rows.Length > 0 Then
                                    Dim amount As Integer = 0.0F

                                    Dim SubTotalSectionHeight As Double = ColumnHeaderHeight + Height * 2 + 2.0F
                                    If PageBreakDetect(SubTotalSectionHeight) Then
                                        DrawTitle(department)
                                    End If

                                    DrawColumnHeaders(ColumnHeaderWidths, ColumnHeaderHeight)
                                    For Each row As DataRow In rows
                                        Dim RowAndSubTotalHeight As Double = Height * 2 + 1.0F
                                        If PageBreakDetect(RowAndSubTotalHeight) Then
                                            DrawTitle(department)
                                            DrawColumnHeaders(ColumnHeaderWidths, ColumnHeaderHeight)
                                        End If

                                        DrawCells(row, ColumnHeaderWidths, Height)
                                        amount += row("Amount")
                                    Next
                                    DrawSubTotal(ColumnHeaderWidths, Height, String.Format("{0}小計：", payMethod), amount)
                                    SumTotal += amount
                                    AddSummary(sumType, payMethod, amount)
                                End If
                            Next
                            'PayMethod
                        Next
                        'PayType
                    End If
                Next
                'SumType

                CalcSummary()
                DrawFooter(department)
            Next
            'Departments
            EndPrint()
        End Sub

        Private Sub BeginPrint()
            Me.m_Report.Name = Me.m_ReportName
            Me.m_Report.Printer = Me.m_Printer
            Me.m_SumTypes = GetSumTypes()
            Me.m_PayTypes = GetPayTypes()
            CreateSummary()
        End Sub

        Private Sub EndPrint()
            Me.m_Report.DrawPageNumber()
            Me.m_Report.Print()
        End Sub

        Private Sub CreateSummary()
            Me.m_Summary.Columns.Add("支付方式", System.Type.GetType("System.String"))
            For Each sumType As String In Me.m_SumTypes
                Me.m_Summary.Columns.Add(sumType, System.Type.GetType("System.Int32"))
            Next
        End Sub

        Private Sub AddSummary(ByVal SumType As String, ByVal PayMethod As String, ByVal SumTotal As Integer)
            Dim rowsSum() As DataRow = Me.m_Summary.Select(String.Format("支付方式 = '{0}'", PayMethod))
            If rowsSum.Length = 0 Then
                Dim row As DataRow = Me.m_Summary.NewRow()
                row("支付方式") = PayMethod
                For Each st As String In Me.m_SumTypes
                    If st = SumType Then
                        row(st) = SumTotal
                    Else
                        row(st) = 0
                    End If
                Next
                Me.m_Summary.Rows.Add(row)
            Else
                rowsSum(0)(sumType) += SumTotal
            End If
        End Sub

        Private Sub CalcSummary()
            Me.m_Summary.Columns.Add("支付總計", System.Type.GetType("System.Int32"))
            For Each r As DataRow In Me.m_Summary.Rows
                Dim amount As Integer = 0
                For i As Integer = 1 To Me.m_Summary.Columns.Count - 2
                    If Me.m_Summary.Columns(i).ColumnName = "收入" Then
                        amount += r(i)
                    ElseIf Me.m_Summary.Columns(i).ColumnName = "支出" Then
                        amount -= r(i)
                    End If
                Next
                r("支付總計") = amount
            Next

            Dim row As DataRow = Me.m_Summary.NewRow()
            row("支付方式") = "金額總計"
            For Each r As DataRow In Me.m_Summary.Rows
                For Each c As DataColumn In Me.m_Summary.Columns
                    If c.ColumnName = "支付方式" Then
                        Continue For
                    End If

                    If IsDBNull(row(c.ColumnName)) Then
                        row(c.ColumnName) = 0
                    End If

                    row(c.ColumnName) += r(c.ColumnName)
                Next
            Next

            Me.m_Summary.Rows.Add(row)
        End Sub

        Private Sub DrawTitle(ByVal department As String)
            Me.m_Report.AddPage()
            Dim sz As SizeF = Me.m_Report.Drawer.MeasureString(Me.m_Title, Me.m_TitleFont)
            Me.m_Report.Drawer.DrawString(String.Format("列印時間：{0:d3}{1:/MM/dd HH:mm:ss}{2}列印人員：{3}", Now.Year - 1911, Now, vbCrLf, Me.m_User), Me.m_CommentFont, Brushes.Black, Me.m_Report.PrintArea, ShortCut.SF.c("lt"))
            Me.m_Report.PrintString(String.Format("{0}({1}年{2:M月d日})", Me.m_Title, Me.m_ReportDate.Year - 1911, Me.m_ReportDate), Me.m_TitleFont, Brushes.Black, New SizeF(Me.m_Report.PrintArea.Width, sz.Height), ShortCut.SF.c("ct"))
            Me.m_Report.Drawer.DrawString(String.Format("分部：{0}", department), Me.m_CommentFont, Brushes.Black, Me.m_Report.PrintArea, ShortCut.SF.c("rt"))
            Me.m_Report.LineBreak()

            DrawSeperatorLine()
            Me.m_Report.MoveCursorY(4.0F)
        End Sub

        Private Sub DrawSumType(ByVal sumType As String)
            Me.m_Report.PrintString(sumType, Me.m_HeaderFont, Brushes.SlateBlue)
            Me.m_Report.LineBreak()
            DrawSeperatorLine()
            DrawSeperatorLine()
        End Sub

        Private Sub DrawNoData()
            Me.m_Report.PrintString("(無資料)", Me.m_ContentFont, Brushes.Gray)
            Me.m_Report.LineBreak()
            Me.m_Report.MoveCursorY(1.0F)
        End Sub

        Private m_ColumnHeaders() As String = "班級名稱,座位,姓名,收據編號,金額,尚欠,繳費方式,備註".Split(",")
        Private Function GetColumnHeaderWidths() As Double()
            Dim ColumnHeaderWidths() As Double = Me.m_Report.GetColumnWidths(Me.m_ColumnHeaders, Me.m_HeaderFont)
            Dim ColumnHeaderWidthsFixed() As Boolean = New Boolean() {True, False, False, False, True, True, False, True}
            ColumnHeaderWidths = Me.m_Report.MergeWidthsWithMax(ColumnHeaderWidths, New Double() {30.0F, 0.0F, 0.0F, 0.0F, 18.0F, 18.0F, 0.0F, 60.0F})
            ColumnHeaderWidths = Me.m_Report.AllocateWidthEqually(Me.m_Report.PrintArea.Width, ColumnHeaderWidths, ColumnHeaderWidthsFixed)
            Return ColumnHeaderWidths
        End Function

        Private Function GetColumnHeaderHeight() As Double
            Dim ColumnHeaderHeight As Double = Me.m_Report.GetColumnHeight(Me.m_ColumnHeaders, Me.m_HeaderFont)
            Return ColumnHeaderHeight
        End Function

        Private Function GetHeight() As Double
            Dim Height As Double = Me.m_Report.GetColumnHeight(Me.m_ColumnHeaders, Me.m_ContentFont)
            Return Height
        End Function

        Private Sub DrawColumnHeaders(ByVal widths() As Double, ByVal height As Double)
            Me.m_Report.PrintHeader(Me.m_ColumnHeaders, widths, height)
            DrawSeperatorLine()
        End Sub

        Private Sub DrawCells(ByVal row As DataRow, ByVal widths() As Double, ByVal height As Double)
            Dim cells As New List(Of String)
            cells.Add(row("ClassName"))
            cells.Add(String.Format("{0}{1:d2}", GetAlphabet(row("X")), row("Y")))
            cells.Add(row("Name"))
            cells.Add(row("Receipt"))
            cells.Add(String.Format("{0:$#,0}", row("Amount")))
            cells.Add(String.Format("{0:$#,0}", row("FeeOwe")))
            cells.Add(String.Format("{0}/{1}", row("PayType"), row("PayMethod")))
            cells.Add(row("Comment"))

            Me.m_Report.PrintRow(cells.ToArray(), Me.m_ContentFont, widths, height, "lm,cm,lm,cm,rm,rm,cm,lm")
        End Sub

        Private Sub DrawSubTotal(ByVal widths() As Double, ByVal height As Double, ByVal header As String, ByVal amount As Integer)
            Dim width As Double = 0.0F
            For i As Integer = 0 To 3
                width += widths(i)
            Next

            Dim widths2() As Double = New Double() {width, widths(4)}
            DrawSeperatorLine()
            Me.m_Report.MoveCursorY(0.2F)
            Me.m_Report.PrintRow(New String() {header, amount.ToString("$#,0")}, Me.m_ContentFont, widths2, height, "rm,rm")
            Me.m_Report.MoveCursorY(3.0F)
        End Sub

        Private Sub DrawFooter(ByVal department As String)
            Me.m_Report.MoveCursorY(3.0F)
            Me.m_Report.BorderStyle = New Pen(Color.Black, 0.3F)

            Dim Headers() As String = Me.m_Report.GetTableHeaderArray(Me.m_Summary)
            Dim ColumnHeaderWidths() As Double = Me.m_Report.GetColumnWidths(Headers, Me.m_HeaderFont)
            ColumnHeaderWidths = Me.m_Report.AllocateWidthEqually(Me.m_Report.PrintArea.Width, ColumnHeaderWidths)
            Dim ColumnHeaderHeight As Double = Me.m_Report.GetColumnHeight(Headers, Me.m_HeaderFont)
            Dim height As Double = Me.m_Report.GetColumnHeight(Headers, Me.m_ContentFont)

            Dim TotalHeight As Double = ColumnHeaderHeight * 2 + 2.0F + height * Me.m_Summary.Rows.Count
            If PageBreakDetect(TotalHeight) Then
                DrawTitle(department)
            End If

            DrawSumType("總計")
            For i As Integer = 1 To Headers.Length - 1
                If Headers(i) = "收入" Then
                    Headers(i) &= " * 1"
                ElseIf Headers(i) = "支出" Then
                    Headers(i) &= " * (-1)"
                ElseIf Headers(i) = "支付總計" Then
                    'Do Nothing
                Else
                    Headers(i) &= " * 0"
                End If
            Next
            Me.m_Report.PrintHeader(Headers, ColumnHeaderWidths, ColumnHeaderHeight)

            For Each row As DataRow In Me.m_Summary.Rows
                Dim cells As New List(Of String)
                Dim cellAlignments As New List(Of String)
                cells.Add(row(0))
                cellAlignments.Add("cm")

                For i As Integer = 1 To Me.m_Summary.Columns.Count - 1
                    cells.Add(String.Format("{0:$#,0}", row(i)))
                    cellAlignments.Add("rm")
                Next

                Me.m_Report.PrintRow(cells.ToArray, Me.m_ContentFont, ColumnHeaderWidths, height, String.Join(",", cellAlignments.ToArray()))
            Next
        End Sub

        Private Function PageBreakDetect(ByVal TotalHeight As Double) As Boolean
            Dim PrintableHeight As Double = Me.m_Report.PrintArea.Bottom - Me.m_Report.Cursor.Y
            If PrintableHeight < TotalHeight Then
                Return True
            Else
                Return False
            End If
        End Function

        Private Function GetSumTypes() As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs("FeeSumType")
            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Function GetPayTypes() As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs("FeePayType")
            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Function GetPayMethods(ByVal PayType As String) As String()
            Dim list As New List(Of String)
            Dim dt As DataTable = CSOL.Data.GetNameValuePairs(String.Format("FeePayType-{0}", PayType))
            If dt.Rows.Count = 0 Then
                dt = CSOL.Data.GetNameValuePairs("FeePayMethod")
            End If

            For Each row As DataRow In dt.Rows
                list.Add(row("Text"))
            Next
            Return list.ToArray()
        End Function

        Private Sub DrawSeperatorLine()
            Dim x As Single = Me.m_Report.PrintArea.X
            Dim x2 As Single = x + Me.m_Report.PrintArea.Width
            Dim y As Single = Me.m_Report.Cursor.Y

            Me.m_Report.Drawer.DrawLine(New Pen(Color.Gray, 0.3), x, y, x2, y)
            Me.m_Report.MoveCursorY(0.5)
        End Sub

        Private Function GetAlphabet(ByVal no As Integer) As String
            Dim abs() As String = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z".Split(",")
            Dim d As Integer = no \ 26
            Dim m As Integer = no Mod 26
            If d = 0 Then
                Return abs(m)
            Else
                Return GetAlphabet(d - 1) & abs(m)
            End If
        End Function

        'Fast Method
        Public Shared Sub DirectPrint(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal ReportDate As Date, ByVal User As String)
            Dim report As New DailyReport(Departments, ReportData, ReportDate, User)
            report.Print()
        End Sub
        Public Shared Sub DirectPrint(ByVal Departments() As String, ByVal ReportData As DataTable, ByVal User As String, ByVal ReportDate As Date, ByVal ReportName As String)
            Dim report As New DailyReport(Departments, ReportData, ReportDate, User)
            report.ReportName = ReportName
            report.Print()
        End Sub
    End Class
End Namespace