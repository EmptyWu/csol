﻿Namespace CSOL.Report.Student
    Public Class Receipt
        Private m_ReportName As String = "學生繳費收據"
        Public Property ReportName() As String
            Get
                Return Me.m_ReportName
            End Get
            Set(ByVal value As String)
                Me.m_ReportName = value
            End Set
        End Property

        Private m_Printer As String = My.Settings.Printer
        Private m_Receipt As String
        Private m_StartDate As Date
        Private m_EndDate As Date
        Private m_StuName As String
        Private m_Amount As Integer
        Private m_Extra As String
        Private m_ClassAndSeat As String

        Public Sub New(ByVal Receipt As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal StuName As String, ByVal Amount As Integer, ByVal Extra As String, ByVal ClassAndSeat As String)
            Me.m_Receipt = Receipt
            Me.m_StartDate = StartDate
            Me.m_EndDate = EndDate
            Me.m_StuName = StuName
            Me.m_Amount = Amount
            Me.m_Extra = Extra.Replace(" ", vbCrLf)
            Me.m_ClassAndSeat = ClassAndSeat
        End Sub

        Public Sub Print()
            Dim report As New Report.Base()
            report.Name = Me.m_ReportName
            report.Printer = Me.m_Printer
            report.AddPage()

            Dim ContentFont As New Font("新細明體", 3.4F, FontStyle.Regular, GraphicsUnit.Millimeter)
            Dim ExtraFont As New Font("新細明體", 2.8F, FontStyle.Regular, GraphicsUnit.Millimeter)

            Dim background As New Bitmap(My.Application.Info.DirectoryPath & "\Images\StuReceipt.tif")
            report.Drawer.DrawImage(background, 0, 0)
            background.Dispose()

            Dim offset As Double = 147.7F
            For i As Integer = 0 To 1
                '收據編號
                Dim x As Double = 68.0F
                Dim y As Double = 8.0F
                report.SetCursor(x, y + offset * i)
                report.PrintString(Me.m_Receipt, ContentFont, Brushes.Black)

                '修業期限
                x = 25.0F
                y += 21.95F
                report.SetCursor(x, y + offset * i)
                Dim sy As String = (Me.m_StartDate.Year - 1911).ToString().PadLeft(3)
                Dim sm As String = Me.m_StartDate.Month.ToString().PadLeft(2)
                Dim sd As String = Me.m_StartDate.Day.ToString().PadLeft(2)
                Dim ey As String = (Me.m_EndDate.Year - 1911).ToString().PadLeft(3)
                Dim em As String = Me.m_EndDate.Month.ToString().PadLeft(2)
                Dim ed As String = Me.m_EndDate.Day.ToString().PadLeft(2)
                Dim ymd As String = String.Format("{0}　   {1}　   {2}　　　  {3}　   {4}　   {5}", sy, sm, sd, ey, em, ed)
                report.PrintString(ymd, ContentFont, Brushes.Black)

                '學生姓名 班級 座號
                x = 21.0F
                y += 7.15F
                report.SetCursor(x, y + offset * i)
                report.PrintString(String.Format("{0}        {1}", Me.m_StuName, Me.m_ClassAndSeat), ContentFont, Brushes.Black)

                '大寫金額
                x = 40.8F
                y += 7.6F
                report.SetCursor(x, y + offset * i)
                report.PrintString(TranslateDigitToComplexTraditionalChinese(Me.m_Amount.ToString().PadLeft(5, "0")), ContentFont, Brushes.Black)

                '數字金額
                x = 25.5F
                y += 12.6F
                report.SetCursor(x, y + offset * i)
                Dim cellSize As New SizeF(20.0F, 4.6F)
                Dim format As New StringFormat()
                format.Alignment = StringAlignment.Far
                format.LineAlignment = StringAlignment.Center
                report.PrintString(Me.m_Amount.ToString("C0").Replace("NT", ""), ContentFont, Brushes.Black, cellSize, format)

                y += 50.5F
                report.SetCursor(x, y + offset * i)
                report.PrintString(Me.m_Amount.ToString("C0").Replace("NT", ""), ContentFont, Brushes.Black, cellSize, format)

                '額外資訊
                x = 56.0F
                y += offset * i - 15.0F
                Dim rectExtra As New RectangleF(x, y, 50.0!, 60.0!)
                Dim sfExtra As New StringFormat(StringFormatFlags.NoWrap Or StringFormatFlags.NoClip)
                report.Drawer.DrawString(Me.m_Extra, ExtraFont, Brushes.DarkBlue, rectExtra, sfExtra)
            Next

            report.Print()
        End Sub

        Private Function TranslateDigitToComplexTraditionalChinese(ByVal digit As String) As String
            Dim digits As String = "0123456789"
            Dim cDigits As String = "零壹貳叁肆伍陸柒捌玖"
            Dim result As String = ""
            For i As Integer = 0 To digit.Length - 1
                Dim index As Integer = digits.IndexOf(digit(i))
                If index >= -1 And index < cDigits.Length Then
                    result &= cDigits(index) & "　  "
                Else
                    result &= "　"
                End If
            Next
            Return result
        End Function


        'Fast Method
        Public Shared Sub DirectPrint(ByVal Receipt As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal StuName As String, ByVal Amount As Integer, ByVal Extra As String, ByVal ClassAndSeat As String)
            Dim report As New Receipt(Receipt, StartDate, EndDate, StuName, Amount, Extra, ClassAndSeat)
            report.Print()
        End Sub
        Public Shared Sub DirectPrint(ByVal Receipt As String, ByVal StartDate As Date, ByVal EndDate As Date, ByVal StuName As String, ByVal Amount As Integer, ByVal Extra As String, ByVal ClassAndSeat As String, ByVal ReportName As String)
            Dim report As New Receipt(Receipt, StartDate, EndDate, StuName, Amount, Extra, ClassAndSeat)
            report.ReportName = ReportName
            report.Print()
        End Sub
    End Class
End Namespace