﻿Namespace CSOL.Report
    Public Class PaperSizes
        Public Shared A4 As New System.Drawing.Printing.PaperSize("A4", 210, 297)
        Public Shared A4_Portrait As New System.Drawing.Printing.PaperSize("A4_Portrait", 297, 210)
    End Class
    Public Class Resolutions
        Public Shared High As Double = 600.0F
        Public Shared Fine As Double = 300.0F
        Public Shared Screen As Double = 96.0F
    End Class

    Public Class MarginF
        Private m_Margins(3) As Double
        Public Property Left() As Double
            Get
                Return Me.m_Margins(3)
            End Get
            Set(ByVal value As Double)
                Me.m_Margins(3) = value
            End Set
        End Property

        Public Property Right() As Double
            Get
                Return Me.m_Margins(1)
            End Get
            Set(ByVal value As Double)
                Me.m_Margins(1) = value
            End Set
        End Property

        Public Property Top() As Double
            Get
                Return Me.m_Margins(0)
            End Get
            Set(ByVal value As Double)
                Me.m_Margins(0) = value
            End Set
        End Property

        Public Property Bottom() As Double
            Get
                Return Me.m_Margins(2)
            End Get
            Set(ByVal value As Double)
                Me.m_Margins(2) = value
            End Set
        End Property

        Public ReadOnly Property Width() As Double
            Get
                Return Me.m_Margins(1) + Me.m_Margins(3)
            End Get
        End Property

        Public ReadOnly Property Height() As Double
            Get
                Return Me.m_Margins(0) + Me.m_Margins(2)
            End Get
        End Property

        Public Sub New(ByVal Top As Double, ByVal Right As Double, ByVal Bottom As Double, ByVal Left As Double)
            Me.m_Margins(0) = Top
            Me.m_Margins(1) = Right
            Me.m_Margins(2) = Bottom
            Me.m_Margins(3) = Left
        End Sub

        Public Sub New(ByVal Vertial As Double, ByVal Horizontal As Double)
            Me.m_Margins(0) = Vertial
            Me.m_Margins(1) = Horizontal
            Me.m_Margins(2) = Vertial
            Me.m_Margins(3) = Horizontal
        End Sub

        Public Sub New(ByVal Width As Double)
            Me.m_Margins(0) = Width
            Me.m_Margins(1) = Width
            Me.m_Margins(2) = Width
            Me.m_Margins(3) = Width
        End Sub
    End Class
    Public Class PaddingF
        Inherits MarginF
        Public Sub New(ByVal Width As Double)
            MyBase.New(Width)
        End Sub
        Public Sub New(ByVal Vertial As Double, ByVal Horizontal As Double)
            MyBase.New(Vertial, Horizontal)
        End Sub
        Public Sub New(ByVal Top As Double, ByVal Right As Double, ByVal Bottom As Double, ByVal Left As Double)
            MyBase.New(Top, Right, Bottom, Left)
        End Sub
    End Class
    Public Class BorderF
        Inherits MarginF
        Public Sub New(ByVal Width As Double)
            MyBase.New(Width)
        End Sub
        Public Sub New(ByVal Vertial As Double, ByVal Horizontal As Double)
            MyBase.New(Vertial, Horizontal)
        End Sub
        Public Sub New(ByVal Top As Double, ByVal Right As Double, ByVal Bottom As Double, ByVal Left As Double)
            MyBase.New(Top, Right, Bottom, Left)
        End Sub
    End Class

    Public Class DataFormater
        Private m_Date As String = "yyyy-MM-dd"
        Private m_Time As String = "HH:mm:ss"
        Private m_DateTime As String = "yyyy-MM-dd HH:mm:ss"
        Public Property [Date]() As String
            Get
                Return Me.m_Date
            End Get
            Set(ByVal value As String)
                Me.m_Date = value
            End Set
        End Property
        Public Property [Time]() As String
            Get
                Return Me.m_Time
            End Get
            Set(ByVal value As String)
                Me.m_Time = value
            End Set
        End Property
        Public Property [DateTime]() As String
            Get
                Return Me.m_DateTime
            End Get
            Set(ByVal value As String)
                Me.m_DateTime = value
            End Set
        End Property

        Private m_Currency As String = "$#,0"
        Private m_FloatNumber As String = "#.00"
        Private m_NaturalNumber As String = "#"
        Public Property Currency() As String
            Get
                Return Me.m_Currency
            End Get
            Set(ByVal value As String)
                Me.m_Currency = value
            End Set
        End Property
        Public Property FloatNumber() As String
            Get
                Return Me.m_FloatNumber
            End Get
            Set(ByVal value As String)
                Me.m_FloatNumber = value
            End Set
        End Property
        Public Property NaturalNumber() As String
            Get
                Return Me.m_NaturalNumber
            End Get
            Set(ByVal value As String)
                Me.m_NaturalNumber = value
            End Set
        End Property

        Public Shared Function DefaultFormater() As DataFormater
            Return New DataFormater()
        End Function
    End Class
    Public Class MeterTranslations
        Public Shared Function FromMilliMeterToCentiInch(ByVal millimeter As Double) As Double
            Dim cm As Double = millimeter / 10
            Dim inch As Double = cm / 2.54
            Dim ci As Double = inch * 100
            Return ci
        End Function

        Public Shared Function FromCentiInchToMilliMeter(ByVal centiinch As Double) As Double
            Dim inch As Double = centiinch / 100
            Dim cm As Double = inch * 2.54
            Dim mm As Double = cm * 10
            Return mm
        End Function
    End Class
    Public Class StringFormatCreater
        Public Shared Function Create() As StringFormat
            Return Create("")
        End Function

        Public Shared Function Create(ByVal alignments As String) As StringFormat
            Return Create(alignments, StringFormatFlags.NoWrap)
        End Function

        Public Shared Function Create(ByVal alignments As String, ByVal flags As StringFormatFlags) As StringFormat
            Dim sf As New StringFormat(flags)
            sf.Alignment = StringAlignment.Near
            sf.LineAlignment = StringAlignment.Center

            If alignments Is Nothing Then
                Return sf
            End If

            'left,center,right
            Dim htags As String = "lcr"

            'top,middle,bottom
            Dim vtags As String = "tmb"
            For i As Integer = 0 To 2
                If alignments.IndexOf(htags(i)) > -1 Then
                    sf.Alignment = i
                End If
                If alignments.IndexOf(vtags(i)) > -1 Then
                    sf.LineAlignment = i
                End If
            Next
            Return sf
        End Function
    End Class
    Public Class Utility
        Public Shared Function GetFormattedString(ByVal content As Object, ByVal format As String) As String
            Dim f As String = "{0f}".Replace("f", System.Text.RegularExpressions.Regex.Replace(":" & format, ":$", ""))
            Return String.Format(f, content)
        End Function
    End Class


    Public Class ImagePrinter
        Private m_Index As Integer = 0
        Private m_PrintDocument As New System.Drawing.Printing.PrintDocument()
        Private m_Images As New List(Of System.Drawing.Bitmap)

        Public Property PrintDocument() As System.Drawing.Printing.PrintDocument
            Get
                Return m_PrintDocument
            End Get
            Set(ByVal value As System.Drawing.Printing.PrintDocument)
                m_PrintDocument = value
            End Set
        End Property

        Public Property PrintDocumentName() As String
            Get
                Return Me.m_PrintDocument.DocumentName
            End Get
            Set(ByVal value As String)
                Me.m_PrintDocument.DocumentName = value
            End Set
        End Property

        Public Property Printer() As String
            Get
                Return Me.m_PrintDocument.PrinterSettings.PrinterName
            End Get
            Set(ByVal value As String)
                For Each ptr As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
                    If ptr = value Then
                        Me.m_PrintDocument.PrinterSettings.PrinterName = ptr
                        Exit For
                    End If
                Next
            End Set
        End Property

        Public Property Images() As List(Of System.Drawing.Bitmap)
            Get
                Return m_Images
            End Get
            Set(ByVal value As List(Of System.Drawing.Bitmap))
                m_Images = value
            End Set
        End Property

        Public Sub New()
            Me.PrintDocument.PrintController = New System.Drawing.Printing.StandardPrintController()
            AddHandler Me.PrintDocument.BeginPrint, AddressOf BeginPrint
            AddHandler Me.PrintDocument.PrintPage, AddressOf PrintPage
            AddHandler Me.PrintDocument.EndPrint, AddressOf EndPrint
        End Sub

        Public Sub New(ByVal Images As List(Of System.Drawing.Bitmap))
            Me.New()
            Me.m_Images = Images
        End Sub

        Private Sub BeginPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        End Sub

        Private Sub PrintPage(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintPageEventArgs)
            If Me.m_Index < Me.m_Images.Count Then
                Dim image As Bitmap = Me.m_Images(Me.m_Index)
                Dim x As Single = -1 * Me.m_PrintDocument.DefaultPageSettings.PrintableArea.X
                Dim y As Single = -1 * Me.m_PrintDocument.DefaultPageSettings.PrintableArea.Y
                e.Graphics.DrawImageUnscaled(image, x, y)
                image.Dispose()
                Me.m_Index += 1
            End If

            If Me.m_Index < Me.m_Images.Count Then
                e.HasMorePages = True
            End If
        End Sub

        Private Sub EndPrint(ByVal sender As Object, ByVal e As System.Drawing.Printing.PrintEventArgs)

        End Sub

        Public Sub Print()
            If Me.m_Images.Count > 0 Then
                Me.m_PrintDocument.Print()
            End If
        End Sub


        'Shared Functions
        Public Shared Function CreateHighQualityPrinterPage() As Bitmap
            'A4 Portrait Size 210 * 297 (mm), 600 dpi
            Return CreatePrinterPage(PaperSizes.A4, Resolutions.High)
        End Function

        Public Shared Function CreatePrinterPage() As Bitmap
            'A4 Portrait Size 210 * 297 (mm), 300 dpi
            Return CreatePrinterPage(PaperSizes.A4, Resolutions.Fine)
        End Function

        Public Shared Function CreatePrinterPage(ByVal PaperSize As System.Drawing.Printing.PaperSize, ByVal PrinterResolution As Single) As Bitmap
            Dim ResolutionRatio As Single = PrinterResolution / Resolutions.Screen

            Dim ciWidth As Single = MeterTranslations.FromMilliMeterToCentiInch(PaperSize.Width)
            Dim ciHeight As Single = MeterTranslations.FromMilliMeterToCentiInch(PaperSize.Height)

            Dim Width As Integer = ciWidth * ResolutionRatio
            Dim Height As Integer = ciHeight * ResolutionRatio

            Dim image As New Bitmap(Width, Height)
            image.SetResolution(PrinterResolution, PrinterResolution)
            Return image
        End Function
    End Class
End Namespace

Namespace CSOL.Report.ShortCut
    Public Class MT
        Public Shared Function ci2mm(ByVal ci As Double) As Double
            Return MeterTranslations.FromCentiInchToMilliMeter(ci)
        End Function
        Public Shared Function mm2ci(ByVal mm As Double) As Double
            Return MeterTranslations.FromMilliMeterToCentiInch(mm)
        End Function
    End Class

    Public Class SF
        Public Shared Function c() As StringFormat
            Return StringFormatCreater.Create()
        End Function
        Public Shared Function c(ByVal alignments As String) As StringFormat
            Return StringFormatCreater.Create(alignments)
        End Function
        Public Shared Function c(ByVal alignments As String, ByVal flags As StringFormatFlags) As StringFormat
            Return StringFormatCreater.Create(alignments, flags)
        End Function
    End Class

    Public Class UT
        Public Shared Function FS(ByVal content As Object, ByVal format As String) As String
            Return Utility.GetFormattedString(content, format)
        End Function
    End Class
End Namespace


Namespace CSOL.Report
    Public Class Base
        Private m_Name As String = "Report"
        Private m_Images As New List(Of Bitmap)
        Private m_Drawer As Graphics
        Private m_Cursor As New PointF(0.0F, 0.0F)
        Private m_MaxLineHeight As Double = 0.0F
        Private m_Margins As New System.Drawing.Printing.Margins(8, 8, 8, 8)
        Private m_PaperSize As System.Drawing.Printing.PaperSize = PaperSizes.A4
        Private m_Printer As String = New System.Drawing.Printing.PrinterSettings().PrinterName

        Public Property Name() As String
            Get
                Return Me.m_Name
            End Get
            Set(ByVal value As String)
                Me.m_Name = value
            End Set
        End Property

        Public Property Image() As List(Of Bitmap)
            Get
                Return Me.m_Images
            End Get
            Set(ByVal value As List(Of Bitmap))
                Me.m_Images = value
            End Set
        End Property

        Public Property Drawer() As Graphics
            Get
                Return Me.m_Drawer
            End Get
            Set(ByVal value As Graphics)
                Me.m_Drawer = value
            End Set
        End Property

        Public Property Cursor() As PointF
            Get
                Return Me.m_Cursor
            End Get
            Set(ByVal value As PointF)
                Me.m_Cursor = value
            End Set
        End Property

        Public Property Margins() As System.Drawing.Printing.Margins
            Get
                Return Me.m_Margins
            End Get
            Set(ByVal value As System.Drawing.Printing.Margins)
                Me.m_Margins = value
            End Set
        End Property

        Public Property PaperSize() As System.Drawing.Printing.PaperSize
            Get
                Return Me.m_PaperSize
            End Get
            Set(ByVal value As System.Drawing.Printing.PaperSize)
                If Me.m_Images Is Nothing Then
                    Me.m_PaperSize = value
                End If
            End Set
        End Property

        Public Property PrintArea() As RectangleF
            Get
                Dim width As Double = Me.m_PaperSize.Width - Me.m_Margins.Left - Me.m_Margins.Right
                Dim height As Double = Me.m_PaperSize.Height - Me.m_Margins.Top - Me.m_Margins.Bottom
                Return New RectangleF(Me.m_Margins.Left, Me.m_Margins.Right, width, height)
            End Get
            Set(ByVal value As RectangleF)
                Me.m_Margins.Left = value.X
                Me.m_Margins.Top = value.Y
                Me.m_Margins.Right = Me.m_PaperSize.Width - value.X - value.Width
                Me.m_Margins.Bottom = Me.m_PaperSize.Height - value.Y - value.Height
            End Set
        End Property

        Public Property Printer() As String
            Get
                Return Me.m_Printer
            End Get
            Set(ByVal value As String)
                Me.m_Printer = value
            End Set
        End Property

        Public Sub AddPage()
            SetCursor(0.0F, 0.0F)
            Me.m_Images.Add(ImagePrinter.CreatePrinterPage(Me.m_PaperSize, Resolutions.Fine))
            Me.m_Drawer = Graphics.FromImage(Me.m_Images(Me.m_Images.Count - 1))
            Me.m_Drawer.PageUnit = GraphicsUnit.Millimeter
            Me.m_Drawer.Clear(Color.White)
        End Sub

        Public Sub SwitchPage(ByVal index As Integer)
            If index >= 0 And index < Me.m_Images.Count Then
                SetCursor(0, 0)
                Me.m_Drawer = Graphics.FromImage(Me.m_Images(index))
                Me.m_Drawer.PageUnit = GraphicsUnit.Millimeter
            End If
        End Sub

        Public Sub DrawPageNumber()
            DrawPageNumber("cb")
        End Sub

        Public Sub DrawPageNumber(ByVal position As String)
            DrawPageNumber(position, New Font("arial", 9), "{0}/{1}")
        End Sub

        Public Sub DrawPageNumber(ByVal position As String, ByVal font As Font, ByVal format As String)
            Dim sf As StringFormat = ShortCut.SF.c(position)
            For i As Integer = 0 To Me.m_Images.Count - 1
                SwitchPage(i)
                Me.m_Drawer.DrawString(String.Format(format, i + 1, Me.m_Images.Count), font, System.Drawing.Brushes.Black, Me.PrintArea, sf)
            Next
        End Sub

        Public Sub Print()
            Dim printer As New ImagePrinter(Me.m_Images)
            printer.PrintDocumentName = Me.m_Name
            printer.Printer = Me.m_Printer
            printer.Print()
        End Sub
        Public Sub SetCursor(ByVal x As Double, ByVal y As Double)
            Me.m_Cursor = New PointF(Me.m_Margins.Left + x, Me.m_Margins.Top + y)
        End Sub
        Public Sub SetCursorX(ByVal x As Double)
            Me.m_Cursor.X = x
        End Sub
        Public Sub SetCursorY(ByVal y As Double)
            Me.m_Cursor.Y = y
        End Sub
        Public Sub MoveCursorX(ByVal x As Double)
            Me.m_Cursor.X += x
        End Sub
        Public Sub MoveCursorY(ByVal y As Double)
            Me.m_Cursor.Y += y
        End Sub
        Public Sub PrintString(ByVal text As String, ByVal font As Font, ByVal brush As Brush)
            Dim size As SizeF = Me.m_Drawer.MeasureString(text, font)
            If size.Height > Me.m_MaxLineHeight Then
                Me.m_MaxLineHeight = size.Height
            End If

            Me.m_Drawer.DrawString(text, font, brush, Me.m_Cursor.X, Me.m_Cursor.Y)
            Me.m_Cursor.X += size.Width
        End Sub
        Public Sub PrintString(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal width As Double, ByVal format As StringFormat)
            Dim size As SizeF = Me.m_Drawer.MeasureString(text, font, width)
            If size.Height > Me.m_MaxLineHeight Then
                Me.m_MaxLineHeight = size.Height
            End If

            Me.m_Drawer.DrawString(text, font, brush, New RectangleF(Me.m_Cursor, New SizeF(width, size.Height)), format)
            Me.m_Cursor.X += width
        End Sub
        Public Sub PrintString(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal destSize As SizeF, ByVal format As StringFormat)
            If destSize.Height > Me.m_MaxLineHeight Then
                Me.m_MaxLineHeight = destSize.Height
            End If

            Me.m_Drawer.DrawString(text, font, brush, New RectangleF(Me.m_Cursor, destSize), format)
            Me.m_Cursor.X += destSize.Width
        End Sub
        Public Sub LineBreak()
            SetCursorX(Me.Margins.Left)
            MoveCursorY(Me.m_MaxLineHeight)
            Me.m_MaxLineHeight = 0
        End Sub
    End Class

    Public Class Advanced
        Inherits Base
#Region "Default Settings"
        Private m_HeaderFont As New Font("arial", ShortCut.MT.ci2mm(12), FontStyle.Bold, GraphicsUnit.Millimeter)
        Private m_Font As New Font("arial", ShortCut.MT.ci2mm(10), FontStyle.Regular, GraphicsUnit.Millimeter)
        Private m_HeaderAlignments As String = "cm"
        Private m_Alignments As String = "lm"
        Private m_CellMargins As New MarginF(0.0F)
        Private m_CellBorders As New BorderF(ShortCut.MT.ci2mm(1))
        Private m_CellPaddings As New PaddingF(ShortCut.MT.ci2mm(2), ShortCut.MT.ci2mm(5))
        Private m_BorderStyle As New Pen(Color.Black, ShortCut.MT.ci2mm(1))

        Public Property HeaderFont() As Font
            Get
                Return Me.m_HeaderFont
            End Get
            Set(ByVal value As Font)
                Me.m_HeaderFont = value
            End Set
        End Property
        Public Property Font() As Font
            Get
                Return Me.m_Font
            End Get
            Set(ByVal value As Font)
                Me.m_Font = value
            End Set
        End Property
        Public Property HeaderAlignments() As String
            Get
                Return Me.m_HeaderAlignments
            End Get
            Set(ByVal value As String)
                Me.m_HeaderAlignments = value
            End Set
        End Property
        Public Property Alignments() As String
            Get
                Return Me.m_Alignments
            End Get
            Set(ByVal value As String)
                Me.m_Alignments = value
            End Set
        End Property
        Public Property CellMargins() As MarginF
            Get
                Return Me.m_CellMargins
            End Get
            Set(ByVal value As MarginF)
                Me.m_CellMargins = value
            End Set
        End Property
        Public Property CellBorders() As BorderF
            Get
                Return Me.m_CellBorders
            End Get
            Set(ByVal value As BorderF)
                Me.m_CellBorders = value
            End Set
        End Property
        Public Property CellPaddings() As PaddingF
            Get
                Return Me.m_CellPaddings
            End Get
            Set(ByVal value As PaddingF)
                Me.m_CellPaddings = value
            End Set
        End Property
        Public Property BorderStyle() As Pen
            Get
                Return Me.m_BorderStyle
            End Get
            Set(ByVal value As Pen)
                Me.m_BorderStyle = value
            End Set
        End Property
#End Region

        Public Sub PrintTable(ByVal table As DataTable)
            Dim columns() As String = GetTableHeaderArray(table)
            Dim columnWidths() As Double = GetColumnWidths(columns, Me.m_HeaderFont)
            Dim columnHeight As Double = GetColumnHeight(columns, Me.m_HeaderFont)

            Dim array()() As String = GetTableArray(table)
            Dim widths() As Double = GetWidthsFromTableArray(array, Me.m_Font)
            Dim height As Double = GetHeightFromTableArray(array, Me.m_Font)

            widths = MergeWidthsWithMax(widths, columnWidths)
            widths = AllocateWidthEqually(Me.PrintArea.Width, widths)
            PrintHeader(GetTableHeaderArray(table), Me.m_HeaderFont, widths, columnHeight)
            For i As Integer = 0 To array.Length - 1
                PrintRow(array(i), Me.m_Font, widths, height)
                If Me.Cursor.Y + height > Me.PrintArea.Bottom Then
                    Me.AddPage()
                End If
            Next
        End Sub

#Region "PrintRow"
        Public Sub PrintHeader(ByVal columns() As String, ByVal widths() As Double, ByVal height As Double)
            PrintHeader(columns, Me.m_HeaderFont, widths, height)
        End Sub

        Public Sub PrintHeader(ByVal columns() As String, ByVal font As Font, ByVal widths() As Double, ByVal height As Double)
            Dim alignmentsContainer(columns.Length - 1) As String
            If Me.m_HeaderAlignments = "cm" Then
                For i As Integer = 0 To columns.Length - 1
                    alignmentsContainer(i) = Me.m_HeaderAlignments
                Next
            End If
            PrintHeader(columns, font, widths, height, String.Join(",", alignmentsContainer))
        End Sub

        Public Sub PrintHeader(ByVal columns() As String, ByVal font As Font, ByVal widths() As Double, ByVal height As Double, ByVal alignments As String)
            PrintRow(columns, font, widths, height, alignments)
        End Sub

        Public Sub PrintRow(ByVal cells() As String, ByVal widths() As Double, ByVal height As Double)
            PrintRow(cells, Me.m_Font, widths, height)
        End Sub

        Public Sub PrintRow(ByVal cells() As String, ByVal font As Font, ByVal widths() As Double, ByVal height As Double)
            Dim alignmentsContainer(cells.Length - 1) As String
            PrintRow(cells, font, widths, height, Me.m_Alignments)
        End Sub

        Public Sub PrintRow(ByVal cells() As String, ByVal font As Font, ByVal widths() As Double, ByVal height As Double, ByVal alignments As String)
            Dim alignmentsContainer(cells.Length - 1) As String
            alignments.Split(",").CopyTo(alignmentsContainer, 0)
            For i As Integer = 0 To cells.Length - 1
                Dim size As New SizeF(widths(i), height)
                Dim rect As New RectangleF(Me.Cursor, size)
                Dim sf As StringFormat = ShortCut.SF.c(alignmentsContainer(i))
                PrintCell(cells(i), font, Brushes.Black, sf, Me.m_CellMargins, Me.m_CellBorders, Me.m_CellPaddings, Me.m_BorderStyle, rect)
            Next
            RowBreak(height)
        End Sub

        Public Sub RowBreak(ByVal row_height As Double)
            RowBreak(Me.PrintArea, row_height)
        End Sub

        Public Sub RowBreak(ByVal rect As RectangleF, ByVal row_height As Double)
            SetCursorX(rect.Left)
            MoveCursorY(row_height)
        End Sub
#End Region

#Region "Convert DataTable To 2D Array"
        Public Function GetTableArray(ByVal table As DataTable) As String()()
            Dim formats(table.Columns.Count - 1) As String
            Return GetTableArray(table, formats)
        End Function

        Public Function GetTableArray(ByVal table As DataTable, ByVal columnFormats() As String) As String()()
            Dim formats(table.Columns.Count - 1) As String
            Array.Copy(columnFormats, formats, columnFormats.Length)
            Dim rows(table.Rows.Count - 1)() As String
            For i As Integer = 0 To table.Rows.Count - 1
                Dim items(table.Columns.Count - 1) As String
                For j As Integer = 0 To table.Columns.Count - 1
                    items(j) = ShortCut.UT.FS(table.Rows(i)(j), formats(j))
                Next
                rows(i) = items
            Next
            Return rows
        End Function

        Public Function GetTableHeaderArray(ByVal table As DataTable) As String()
            Dim columns(table.Columns.Count - 1) As String
            For i As Integer = 0 To table.Columns.Count - 1
                columns(i) = table.Columns(i).ColumnName
            Next
            Return columns
        End Function
#End Region

#Region "Width Calculation"
        Public Function GetWidthsFromTableArray(ByVal table()() As String, ByVal font As Font) As Double()
            If table.Length = 0 Then
                Return Nothing
            End If

            Dim widths(table(0).Length - 1) As Double
            For i As Integer = 0 To table.Length - 1
                Dim sizes() As SizeF = MeasureSizes(table(i), font)
                For j As Integer = 0 To widths.Length - 1
                    If widths(j) < sizes(j).Width Then
                        widths(j) = sizes(j).Width
                    End If
                Next
            Next
            For i As Integer = 0 To widths.Length - 1
                widths(i) += Me.m_CellMargins.Width + Me.m_CellBorders.Width + Me.m_CellPaddings.Width
            Next
            Return widths
        End Function

        Public Function GetColumnWidths(ByVal columns() As String, ByVal font As Font) As Double()
            Dim table()() As String = New String()() {columns}
            Return GetWidthsFromTableArray(table, font)
        End Function

        Public Function GetHeightFromTableArray(ByVal table()() As String, ByVal font As Font) As Double
            Dim height As Double = 0.0F
            For i As Integer = 0 To table.Length - 1
                Dim sizes() As SizeF = MeasureSizes(table(i), font)
                Dim h As Double = GetMaxHeightFromSizes(sizes)
                If height < h Then
                    height = h
                End If
            Next
            height += Me.m_CellMargins.Height + Me.m_CellBorders.Height + Me.m_CellPaddings.Height
            Return height
        End Function

        Public Function GetColumnHeight(ByVal columns() As String, ByVal font As Font) As Double
            Dim table()() As String = New String()() {columns}
            Return GetHeightFromTableArray(table, font)
        End Function

        Public Function MeasureSizes(ByVal cells() As String, ByVal font As Font) As SizeF()
            Dim sizes(cells.Length - 1) As SizeF
            For i As Integer = 0 To cells.Length - 1
                sizes(i) = Me.Drawer.MeasureString(cells(i), font)
            Next
            Return sizes
        End Function

        Public Function GetWidthsFromSizes(ByVal sizes() As SizeF) As Double()
            Dim widths(sizes.Length - 1) As Double
            For i As Integer = 0 To sizes.Length - 1
                widths(i) = sizes(i).Width
            Next
            Return widths
        End Function

        Public Function AllocateWidthEqually(ByVal width As Double, ByVal ratios() As Double, ByVal fixed() As Boolean) As Double()
            If ratios Is Nothing Then
                Return Nothing
            End If

            If fixed Is Nothing Then
                Return AllocateWidthEqually(width, ratios)
            End If

            If ratios.Length <> fixed.Length Then
                Return AllocateWidthEqually(width, ratios)
            End If

            Dim unfixed_width As Double = width
            Dim widths(ratios.Length - 1) As Double
            ratios.CopyTo(widths, 0)
            For i As Integer = 0 To ratios.Length - 1
                If fixed(i) = True Then
                    unfixed_width -= ratios(i)
                    widths(i) = 0.0F
                End If
            Next
            widths = AllocateWidthEqually(unfixed_width, widths)

            For i As Integer = 0 To ratios.Length - 1
                If fixed(i) = True Then
                    widths(i) = ratios(i)
                End If
            Next
            Return widths
        End Function

        Public Function AllocateWidthEqually(ByVal width As Double, ByVal ratios() As Double) As Double()
            If ratios Is Nothing Then
                Return Nothing
            End If

            Dim total As Double = 0.0F
            For Each ratio As Double In ratios
                total += ratio
            Next

            Dim widths(ratios.Length - 1) As Double
            For i As Integer = 0 To ratios.Length - 1
                widths(i) = width * ratios(i) / total
            Next
            Return widths
        End Function

        Public Function MergeWidthsWithMax(ByVal widths1() As Double, ByVal widths2() As Double) As Double()
            If widths1 Is Nothing And widths2 Is Nothing Then
                Return Nothing
            ElseIf widths1 Is Nothing Then
                Return widths2
            ElseIf widths2 Is Nothing Then
                Return widths1
            End If

            Dim widths(widths1.Length - 1) As Double
            widths1.CopyTo(widths, 0)
            For i As Integer = 0 To widths.Length - 1
                If i >= widths2.Length Then
                    Exit For
                End If
                If widths(i) < widths2(i) Then
                    widths(i) = widths2(i)
                End If
            Next
            Return widths
        End Function

        Public Function GetMaxHeightFromSizes(ByVal sizes() As SizeF) As Double
            Dim height As Double = 0.0F
            For i As Integer = 0 To sizes.Length - 1
                If sizes(i).Height > height Then
                    height = sizes(i).Height
                End If
            Next
            Return height
        End Function
#End Region

#Region "PrintCell"
        Public Sub PrintCell(ByVal text As String)
            Me.PrintCell(text, Me.m_Font, Brushes.Black)
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal rect As RectangleF)
            Me.PrintCell(text, Me.m_Font, Brushes.Black, ShortCut.SF.c(Me.m_Alignments.Split(",")(0)), Me.m_CellMargins, Me.m_CellBorders, Me.m_CellPaddings, Me.m_BorderStyle, rect)
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal font As Font, ByVal brush As Brush)
            Me.PrintCell(text, font, brush, ShortCut.SF.c(Me.m_Alignments.Split(",")(0)))
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal format As StringFormat)
            Dim rect As RectangleF = GetCellRectangleF(text, font)
            Me.PrintCell(text, font, brush, format, Me.m_CellMargins, Me.m_CellBorders, Me.m_CellPaddings, Me.m_BorderStyle, rect)
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal font As Font, ByVal format As StringFormat, ByVal size As SizeF)
            PrintCell(text, font, Brushes.Black, format, Me.m_CellMargins, Me.m_CellBorders, Me.m_CellPaddings, Me.m_BorderStyle, size)
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal format As StringFormat, _
                             ByVal margin As MarginF, ByVal border As BorderF, ByVal padding As PaddingF, ByVal borderStyle As Pen, ByVal size As SizeF)
            Dim rect As New RectangleF(Me.Cursor, size)
            PrintCell(text, font, brush, format, margin, border, padding, borderStyle, rect)
        End Sub

        Public Sub PrintCell(ByVal text As String, ByVal font As Font, ByVal brush As Brush, ByVal format As StringFormat, _
                             ByVal margin As MarginF, ByVal border As BorderF, ByVal padding As PaddingF, ByVal borderStyle As Pen, ByVal rect As RectangleF)

            MoveCursorX(rect.Width)
            rect.Offset(margin.Left, margin.Top)
            rect.Width -= margin.Width
            rect.Height -= margin.Height
            Me.Drawer.DrawRectangles(borderStyle, New RectangleF() {rect})

            rect.Offset(border.Width + padding.Left, border.Width + padding.Top)
            rect.Width -= border.Width + padding.Width
            rect.Height -= margin.Height + padding.Height
            Me.Drawer.DrawString(text, font, brush, rect, format)
        End Sub

        Public Function GetCellRectangleF(ByVal text As String, ByVal font As Font) As RectangleF
            Dim size As SizeF = Me.Drawer.MeasureString(text, font)
            size.Width += Me.m_CellMargins.Width + Me.m_CellBorders.Width + Me.m_CellPaddings.Width
            size.Height += Me.m_CellMargins.Height + Me.m_CellBorders.Height + Me.m_CellPaddings.Height
            Return New RectangleF(Me.Cursor, size)
        End Function
#End Region

    End Class
End Namespace