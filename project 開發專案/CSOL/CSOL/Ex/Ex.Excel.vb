﻿Imports NPOI.HSSF.UserModel

Namespace Ex
    Public Class Excel
        Public Shared Function HSSFWorkbookToDataSet(ByVal workbook As HSSFWorkbook) As DataSet
            Dim ds As New DataSet()
            For i As Integer = 0 To workbook.NumberOfSheets - 1
                Dim dt As New DataTable(workbook.GetSheetName(i))
                Dim sheet As HSSFSheet = workbook.GetSheetAt(i)
                If sheet.LastRowNum = 0 Then
                    Continue For
                End If

                Dim row As HSSFRow = sheet.GetRow(0)
                For col As Integer = 0 To row.LastCellNum - 1
                    Dim cell As HSSFCell = row.GetCell(col)
                    dt.Columns.Add(cell.ToString(), GetType(String))
                Next

                For j As Integer = 1 To sheet.LastRowNum
                    row = sheet.GetRow(j)
                    Dim items As New List(Of String)
                    For col As Integer = 0 To dt.Columns.Count - 1
                        Dim cell As HSSFCell = row.GetCell(col)
                        If cell Is Nothing Then
                            items.Add("")
                        Else
                            items.Add(cell.ToString())
                        End If
                    Next
                    dt.Rows.Add(items.ToArray())
                Next
                ds.Tables.Add(dt)
            Next
            Return ds
        End Function

        Public Shared Function DataSetToHSSFWorkbook(ByVal ds As DataSet) As HSSFWorkbook
            Dim workbook As New HSSFWorkbook()
            For Each dt As DataTable In ds.Tables
                Dim sheet As HSSFSheet = workbook.CreateSheet(dt.TableName)
                Dim row As HSSFRow = sheet.CreateRow(0)
                For col As Integer = 0 To dt.Columns.Count - 1
                    Dim cell As HSSFCell = row.CreateCell(col)
                    cell.SetCellValue(dt.Columns(col).ColumnName)
                Next

                For i As Integer = 0 To dt.Rows.Count - 1
                    Dim rowIndex As Integer = i + 1
                    row = sheet.CreateRow(rowIndex)
                    For col As Integer = 0 To dt.Columns.Count - 1
                        Dim cell As HSSFCell = row.CreateCell(col)
                        cell.SetCellValue(dt.Rows(i).Item(col).ToString())
                    Next
                Next
            Next
            Return workbook
        End Function
    End Class
End Namespace