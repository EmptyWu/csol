﻿Namespace CSOL
    Public Class HTTPClient
        Public Shared Function SyncSendAndGetResult(ByVal Handler As String, ByVal Action As String, ByVal Params() As Byte) As Byte()
            Dim server As String = My.Settings.Server
            Dim url As String = String.Format("{0}{1}?act={2}", server, Handler, Action)
            Dim request As System.Net.HttpWebRequest = System.Net.WebRequest.Create(url)
            request.Method = "POST"
            request.ContentType = "application/x-www-form-urlencoded"
            request.ContentLength = Params.Length
            request.Timeout = 300000
            request.CookieContainer = New System.Net.CookieContainer()

            Dim currentIP As String = GetCurrentIP()
            Dim cookie As New System.Net.Cookie("SessionID", My.Application.SessionID, "/", New Uri(server).Host)
            request.CookieContainer.Add(cookie)

            Using stream As System.IO.Stream = request.GetRequestStream()
                stream.Write(Params, 0, Params.Length)
            End Using

            Dim bs() As Byte = New Byte() {}

            Try
                Dim response As System.Net.HttpWebResponse = request.GetResponse()

                Using stream As System.IO.Stream = response.GetResponseStream()
                    Dim ms As New System.IO.MemoryStream()
                    Dim bytes As Integer = 0
                    Dim buffer(&H1000) As Byte
                    While True
                        bytes = stream.Read(buffer, 0, buffer.Length)
                        If bytes = 0 Then
                            Exit While
                        Else
                            ms.Write(buffer, 0, bytes)
                        End If
                    End While
                    bs = ms.ToArray()
                End Using
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return bs
        End Function

        Public Shared Function SyncSendAndGetResult(ByVal Handler As String, ByVal Action As String, ByVal Params As String) As String
            Dim bs() As Byte = SyncSendAndGetResult(Handler, Action, System.Text.Encoding.UTF8.GetBytes(Params))
            Return System.Text.Encoding.UTF8.GetString(bs)
        End Function

        Public Shared Function SyncSendAndGetResult(ByVal Handler As String, ByVal Action As String, ByVal Params As System.Collections.Specialized.NameValueCollection) As String
            Dim result As String = SyncSendAndGetResult(Handler, Action, GetQuery(Params))
            Return result
        End Function

        Public Shared Function GetCurrentIP() As String
            Dim currentIP As String = ""
            For Each ip As System.Net.IPAddress In System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList
                If ip.GetAddressBytes().Length = 4 Then
                    currentIP = ip.ToString()
                    Exit For
                End If
            Next
            Return currentIP
        End Function

        Public Shared Function GetQuery(ByVal nv As System.Collections.Specialized.NameValueCollection) As String
            Return CSOL.Convert.GetQuery(nv)
        End Function

        Public Shared Function ParseQuery(ByVal query As String) As System.Collections.Specialized.NameValueCollection
            Return CSOL.Convert.ParseQuery(query)
        End Function

        Public Shared Function Post(ByVal Handler As String, ByVal Action As String) As Byte()
            Dim nv As New System.Collections.Specialized.NameValueCollection()
            Return Post(Handler, Action, nv)
        End Function

        Public Shared Function Post(ByVal Handler As String, ByVal Action As String, ByVal Encoding As System.Text.Encoding) As String
            Dim bs() As Byte = Post(Handler, Action)
            Return Encoding.GetString(bs)
        End Function

        Public Shared Function Post(ByVal Handler As String, ByVal Action As String, ByVal Params As System.Collections.Specialized.NameValueCollection) As Byte()
            Dim server As String = My.Settings.Server
            Dim url As String = String.Format("{0}{1}?act={2}", server, Handler, Action)

            Dim wc As New CSOL.WebClient()
            wc.Timeout = 300000
            wc.Headers.Add(Net.HttpRequestHeader.Cookie, String.Format("SessionID={0}", My.Application.SessionID))
            Dim cookie As New System.Net.Cookie("SessionID", My.Application.SessionID, "/", New Uri(server).Host)
            wc.CookieContainer.Add(cookie)
            Dim bs() As Byte = New Byte() {}
            Try
                bs = wc.UploadValues(url, Params)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return bs
        End Function

        Public Shared Function Post(ByVal Handler As String, ByVal Action As String, ByVal Params As System.Collections.Specialized.NameValueCollection, ByVal Encoding As System.Text.Encoding) As String
            Dim bs() As Byte = Post(Handler, Action, Params)
            Return Encoding.GetString(bs)
        End Function
    End Class
End Namespace