﻿Imports System.Text
Imports System.Security.Cryptography

Namespace CSOL
    Public Class Crypto
        Public Shared Function GetMD5(ByVal input As String) As String
            Return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(input, "MD5").ToLower()
        End Function

        Public Shared Function GetSHA1(ByVal input As String) As String
            Return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(input, "SHA1").ToLower()
        End Function
    End Class
End Namespace
