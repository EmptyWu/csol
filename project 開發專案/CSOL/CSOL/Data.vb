﻿Namespace CSOL
    Public Class Data
        Private Shared c_EntryName As String = "Data"
        Private Shared c_Password As String = ""
        Private Shared c_Path As String = String.Format("{0}\{1}", My.Application.Info.DirectoryPath, c_EntryName)
        Private Shared c_OldPath As String = String.Format("{0}\{1}.old", My.Application.Info.DirectoryPath, c_EntryName)

        Public Shared Function GetNameValuePairs() As DataTable
            Dim xml As String = GetZippedString(c_Path, c_EntryName)
            If xml = "" And System.IO.File.Exists(c_OldPath) Then
                xml = GetZippedString(c_OldPath, c_EntryName)
            End If

            Dim ds As DataSet = CSOL.Convert.XmlStringToDataSet(xml)
            Dim dt As DataTable = ds.Tables("NameValuePairs")
            Return dt
        End Function

        Public Shared Function GetNameValuePairs(ByVal Name As String) As DataTable
            Return GetNameValuePairs(Name, Name)
        End Function

        Public Shared Function GetNameValuePairs(ByVal Name As String, ByVal TableName As String) As DataTable
            Dim dt As DataTable = GetNameValuePairs()
            Dim dtResult As DataTable = dt.Clone()
            dtResult.TableName = TableName
            Dim rows() As DataRow = dt.Select(String.Format("Name = '{0}'", Name))
            For Each row As DataRow In rows
                dtResult.Rows.Add(row.ItemArray)
            Next
            Return dtResult
        End Function

        Public Shared Sub DownloadData()
            Dim server As String = My.Settings.Server
            Dim url As String = String.Format("{0}Data?act=GetAll", server)

            Dim wc As New CSOL.WebClient()
            wc.Encoding = System.Text.Encoding.UTF8
            AddHandler wc.DownloadStringCompleted, AddressOf DownloadData_DownloadStringCompleted
            wc.DownloadStringAsync(New Uri(url))
        End Sub

        Private Shared Sub DownloadData_DownloadStringCompleted(ByVal sender As Object, ByVal e As System.Net.DownloadStringCompletedEventArgs)
            If e.Error Is Nothing Then
                Dim content As String = e.Result
                Dim nv As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(content)
                Try
                    SaveZippedString(c_Path, c_EntryName, nv("Data"))
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try
            Else
                Dim timer As New Timer()
                timer.Interval = 30000
                AddHandler timer.Tick, AddressOf RetryDownloadData
                timer.Start()

                System.Diagnostics.Debug.WriteLine(e.Error.ToString())
            End If
        End Sub

        Private Shared Sub RetryDownloadData(ByVal sender As Object, ByVal e As EventArgs)
            Dim timer As Timer = sender
            timer.Stop()
            timer.Dispose()

            DownloadData()
        End Sub

        'Public Shared Function GetDefinitions() As DataTable
        '    Dim xml As String = GetZippedString(c_Path, c_EntryName)
        '    If xml = "" And System.IO.File.Exists(c_OldPath) Then
        '        xml = GetZippedString(c_OldPath, c_EntryName)
        '    End If

        '    Dim ds As DataSet = CSOL.Convert.XmlStringToDataSet(xml)
        '    Dim dt As DataTable = ds.Tables("Definitions")
        '    Return dt
        'End Function

        'Public Shared Function GetDefinitions(ByVal Name As String) As DataTable
        '    Return GetDefinitions(Name, Name)
        'End Function

        'Public Shared Function GetDefinitions(ByVal Name As String, ByVal TableName As String) As DataTable
        '    Dim dt As DataTable = GetDefinitions()
        '    Dim dtResult As DataTable = dt.Clone()
        '    dtResult.TableName = TableName
        '    Dim rows() As DataRow = dt.Select(String.Format("Name = '{0}'", Name))
        '    For Each row As DataRow In rows
        '        dtResult.Rows.Add(row.ItemArray)
        '    Next
        '    Return dtResult
        'End Function

        'Public Shared Sub DownloadDefinitions()
        '    Dim server As String = My.Settings.Server
        '    Dim url As String = String.Format("{0}Data?act=GetAll", server)

        '    Dim wc As New CSOL.WebClient()
        '    wc.Encoding = System.Text.Encoding.UTF8
        '    AddHandler wc.DownloadStringCompleted, AddressOf DownloadDefinitions_DownloadStringCompleted
        '    wc.DownloadStringAsync(New Uri(url))
        'End Sub

        'Private Shared Sub DownloadDefinitions_DownloadStringCompleted(ByVal sender As Object, ByVal e As System.Net.DownloadStringCompletedEventArgs)
        '    If e.Error Is Nothing Then
        '        Dim content As String = e.Result
        '        Dim nv As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(content)
        '        Try
        '            SaveZippedString(c_Path, c_EntryName, nv("Data"))
        '        Catch ex As Exception
        '            System.Diagnostics.Debug.WriteLine(ex.ToString())
        '        End Try
        '    Else
        '        Dim timer As New Timer()
        '        timer.Interval = 5000
        '        AddHandler timer.Tick, AddressOf RetryDownloadDefinitions
        '        timer.Start()

        '        System.Diagnostics.Debug.WriteLine(e.Error.ToString())
        '    End If
        'End Sub

        'Private Shared Sub RetryDownloadDefinitions(ByVal sender As Object, ByVal e As EventArgs)
        '    Dim timer As Timer = sender
        '    timer.Stop()
        '    timer.Dispose()

        '    DownloadDefinitions()
        'End Sub

        Public Shared Sub SaveZippedString(ByVal path As String, ByVal entryname As String, ByVal content As String)
            SaveZippedString(path, entryname, content, c_Password)
        End Sub

        Public Shared Sub SaveZippedString(ByVal path As String, ByVal entryname As String, ByVal content As String, ByVal password As String)
            If System.IO.File.Exists(c_Path) Then
                System.IO.File.Copy(c_Path, c_OldPath, True)
            End If

            Dim zip As New Ionic.Zip.ZipFile()
            zip.Password = password
            zip.AddEntry(entryname, content, System.Text.Encoding.UTF8)
            zip.Save(path)
        End Sub

        Public Shared Function GetZippedString(ByVal path As String, ByVal entryname As String) As String
            Return GetZippedString(path, entryname, c_Password)
        End Function

        Public Shared Function GetZippedString(ByVal path As String, ByVal entryname As String, ByVal password As String) As String
            Dim result As String = ""
            If System.IO.File.Exists(path) Then
                If Ionic.Zip.ZipFile.IsZipFile(path) Then
                    Using ms As New System.IO.MemoryStream()
                        Dim z As Ionic.Zip.ZipFile = Ionic.Zip.ZipFile.Read(path)
                        z.Password = password
                        z.Item(entryname).Extract(ms)
                        ms.Position = 0

                        Using sr As New System.IO.StreamReader(ms, System.Text.Encoding.UTF8)
                            result = sr.ReadToEnd()
                        End Using
                    End Using
                End If
            End If

            Return result
        End Function
    End Class
End Namespace