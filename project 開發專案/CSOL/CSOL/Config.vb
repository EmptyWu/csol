﻿Imports System.Xml
Imports System.Collections.Specialized

Namespace CSOL
    Public Class Config
        Private m_Content As New XmlDocument()
        Private m_ConfigFile As String

        Public Sub New()
            Me.m_ConfigFile = GetAppConfigPath()
            Me.Load()
        End Sub

        Public Sub Load()
            If System.IO.File.Exists(Me.m_ConfigFile) Then
                Try
                    Me.m_Content.Load(Me.m_ConfigFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try
            End If
        End Sub

        Public Sub Save()
            If System.IO.File.Exists(Me.m_ConfigFile) Then
                System.IO.File.SetAttributes(Me.m_ConfigFile, IO.FileAttributes.Archive)
            End If

            Try
                Me.m_Content.Save(Me.m_ConfigFile)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            System.IO.File.SetAttributes(Me.m_ConfigFile, IO.FileAttributes.Hidden)
        End Sub

        Public Sub Refresh()
            Me.m_Content = New XmlDocument()
            Me.Load()
        End Sub

        Public Function GetConfig(ByVal key As String) As String
            Dim value As String = Nothing
            Dim element As XmlElement = Me.m_Content.SelectSingleNode(String.Format("//appSettings/add[@key='{0}']", key))
            If element IsNot Nothing Then
                value = element.GetAttribute("value")
            End If
            Return value
        End Function

        Public Sub SetConfig(ByVal key As String, ByVal value As String)
            Dim appSettings As XmlElement = Me.m_Content.SelectSingleNode("//appSettings")
            Dim element As XmlElement = appSettings.SelectSingleNode(String.Format("//add[@key='{0}']", key))
            If element Is Nothing Then
                element = Me.m_Content.CreateElement("add")
                element.SetAttribute("key", key)
                element.SetAttribute("value", value)
                appSettings.AppendChild(element)
            Else
                element.RemoveAttribute("value")
                element.SetAttribute("value", value)
            End If
        End Sub

        Public Sub RemoveConfig(ByVal key As String)
            Dim element As XmlElement = Me.m_Content.SelectSingleNode(String.Format("//appSettings/add[@key='{0}']", key))
            If element IsNot Nothing Then
                element.ParentNode.RemoveChild(element)
            End If
        End Sub

        Public Shared Function GetAppConfigPath() As String
            Return String.Format("{0}\{1}.exe.config", My.Application.Info.DirectoryPath, My.Application.Info.ProductName)
        End Function

        Public Shared Function GetAppConfig(ByVal key As String) As String
            Dim configFile As String = GetAppConfigPath()
            Dim value As String = Nothing

            If System.IO.File.Exists(configFile) Then
                Dim xmldoc As New System.Xml.XmlDocument()
                Try
                    xmldoc.Load(configFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try

                Dim element As System.Xml.XmlElement = xmldoc.SelectSingleNode(String.Format("//appSettings/add[@key='{0}']", key))
                If element IsNot Nothing Then
                    value = element.GetAttribute("value")
                End If
            End If

            Return value
        End Function

        Public Shared Sub SetAppConfig(ByVal key As String, ByVal value As String)
            Dim configFile As String = GetAppConfigPath()

            If System.IO.File.Exists(configFile) Then
                System.IO.File.SetAttributes(configFile, IO.FileAttributes.Archive)
                Dim xmldoc As New System.Xml.XmlDocument()
                Try
                    xmldoc.Load(configFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try

                Dim appSettings As System.Xml.XmlElement = xmldoc.SelectSingleNode("//appSettings")
                Dim element As System.Xml.XmlElement = appSettings.SelectSingleNode(String.Format("//add[@key='{0}']", key))
                If element Is Nothing Then
                    element = xmldoc.CreateElement("add")
                    element.SetAttribute("key", key)
                    element.SetAttribute("value", value)
                    appSettings.AppendChild(element)
                Else
                    element.RemoveAttribute("value")
                    element.SetAttribute("value", value)
                End If
                Try
                    xmldoc.Save(configFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try
                System.IO.File.SetAttributes(configFile, IO.FileAttributes.Hidden)
            End If
        End Sub

        Public Shared Sub RemoveAppConfig(ByVal key As String)
            Dim configFile As String = GetAppConfigPath()

            If System.IO.File.Exists(configFile) Then
                System.IO.File.SetAttributes(configFile, IO.FileAttributes.Archive)
                Dim xmldoc As New System.Xml.XmlDocument()
                Try
                    xmldoc.Load(configFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try

                Dim element As System.Xml.XmlElement = xmldoc.SelectSingleNode(String.Format("//appSettings/add[@key='{0}']", key))
                If element IsNot Nothing Then
                    element.ParentNode.RemoveChild(element)
                End If
                Try
                    xmldoc.Save(configFile)
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try
                System.IO.File.SetAttributes(configFile, IO.FileAttributes.Hidden)
            End If
        End Sub
    End Class

    Public Class CommonConfig
        Private Shared c_WorkStation As String = "WorkStation"
        Public Shared Property WorkStation() As String
            Get
                Return CSOL.Config.GetAppConfig(c_WorkStation)
            End Get
            Set(ByVal value As String)
                CSOL.Config.SetAppConfig(c_WorkStation, value)
            End Set
        End Property
    End Class
End Namespace