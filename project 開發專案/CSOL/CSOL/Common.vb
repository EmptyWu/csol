﻿Friend Class Common
    Friend Shared Function GetStuInfo(ByVal StuID As Integer) As DataTable
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", StuID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", "Detail", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Detail"))
        Return dt
    End Function
End Class
