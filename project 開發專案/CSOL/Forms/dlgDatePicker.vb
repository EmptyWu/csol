﻿Imports System.Windows.Forms

Public Class dlgDatePicker
    Public Property ChosenDate() As Date
        Get
            Return MonthCalendar1.SelectionStart
        End Get
        Set(ByVal value As Date)
            MonthCalendar1.SelectionStart = value
        End Set
    End Property

    Private Sub dlgDatePicker_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        MonthCalendar1.MaxDate = Today
        DateTimePicker1.MaxDate = Today
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Sub DateTimePicker1_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DateTimePicker1.ValueChanged
        MonthCalendar1.SelectionStart = DateTimePicker1.Value
    End Sub

    Private Sub MonthCalendar1_DateChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DateRangeEventArgs) Handles MonthCalendar1.DateChanged
        DateTimePicker1.Value = MonthCalendar1.SelectionStart
    End Sub
End Class
