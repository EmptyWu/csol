﻿Public Class frmLogin
    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim LoginStatus As String = Login()
        If LoginStatus = "OK" Then
            Dim frm As New Main()
            My.Application.FormController = frm
            frm.Show()
            Me.DialogResult = Windows.Forms.DialogResult.OK
            Me.Close()
        ElseIf LoginStatus = "Failed" Then
            MessageBox.Show("帳號或密碼錯誤...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            MessageBox.Show("連線失敗，請稍候再試...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    Private Function GetSeed() As String
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "GetSeed", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Return ResponseParams("Seed")
    End Function

    Private Function Login() As String
        Dim Seed As String = GetSeed()
        Dim Password As String = CSOL.Crypto.GetMD5(CSOL.Crypto.GetMD5(tbPassword.Text) & Seed)

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Username", tbUser.Text)
        RequestParams.Add("Password", Password)
        RequestParams.Add("Seed", Seed)
        RequestParams.Add("WorkStationID", My.Settings.WorkStationID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "Login", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            My.Application.SessionID = ResponseParams("SessionID")
        End If

        Return ResponseParams("Status")
    End Function

    Private Sub frmLogin_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim tr As New Timer()
        tr.Interval = 5000
        AddHandler tr.Tick, AddressOf BringLoginToFront
        tr.Start()
    End Sub

    Private Sub BringLoginToFront(ByVal sender As Object, ByVal e As EventArgs)
        Me.Invalidate()
        Me.Update()
    End Sub

    Protected Overrides ReadOnly Property CreateParams() As CreateParams
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            Const CS_NOCLOSE As Integer = &H200
            cp.ClassStyle = cp.ClassStyle Or CS_NOCLOSE
            Return cp
        End Get
    End Property
End Class
