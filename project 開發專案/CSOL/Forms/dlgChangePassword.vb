﻿Imports System.Windows.Forms

Public Class dlgChangePassword
    Private Sub dlgChangePassword_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        tbOld.Select()
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        tbOld.Text = tbOld.Text.Trim()
        tbNew.Text = tbNew.Text.Trim()
        tbAgain.Text = tbAgain.Text.Trim()

        If tbOld.Text = "" Then
            MessageBox.Show("舊的密碼不能空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If tbNew.Text = "" Then
            MessageBox.Show("新的密碼不能空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If tbAgain.Text = "" Then
            MessageBox.Show("再一次不能空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        If tbNew.Text <> tbAgain.Text Then
            MessageBox.Show("新的密碼跟再一次要一樣...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Seed As String = GetSeed()
        Dim Password As String = CSOL.Crypto.GetMD5(CSOL.Crypto.GetMD5(tbOld.Text) & Seed)
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Old", Password)
        RequestParams.Add("New", tbNew.Text)
        RequestParams.Add("Seed", Seed)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "ChangePassword", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            MessageBox.Show("變碼更改成功，下次登入的時候要用新的喔...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Me.Close()
        Else
            MessageBox.Show("變碼更改失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.Close()
    End Sub

    Private Function GetSeed() As String
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "GetSeed", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Return ResponseParams("Seed")
    End Function
End Class
