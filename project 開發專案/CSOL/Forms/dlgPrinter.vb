﻿Imports System.Windows.Forms

Public Class dlgPrinter
    Private Sub dlgPrinter_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        For Each printer As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            ListBox1.Items.Add(printer)
        Next

        Dim DefaultPrinter As String = My.Settings.Printer
        If Not ListBox1.Items.Contains(DefaultPrinter) Then
            DefaultPrinter = New System.Drawing.Printing.PrinterSettings().PrinterName
        End If
        ListBox1.SelectedItem = DefaultPrinter
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.OK
        My.Settings.Printer = ListBox1.SelectedItem
        My.Settings.Save()
        Me.Close()
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub
End Class
