﻿Imports System.Windows.Forms

Public Class Main

#Region "網路狀態"
    Private Sub AddNetworkStatusHandler()
        AddHandler My.Application.NetworkAvailabilityChanged, New Microsoft.VisualBasic.Devices.NetworkAvailableEventHandler(AddressOf NetworkStatusChanged)
    End Sub

    Private Sub NetworkStatusChanged(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs)
        If e.IsNetworkAvailable Then
            If My.Computer.Network.Ping(New Uri(My.Settings.Server).Host) Then
                tssNetwork.Text = "連線"
                tssNetwork.ForeColor = Color.Black
            Else
                tssNetwork.Text = "離線"
                tssNetwork.ForeColor = Color.Red
            End If
        Else
            tssNetwork.Text = "離線"
            tssNetwork.ForeColor = Color.Red
        End If
    End Sub
#End Region

#Region "TabMDI操作"
    Public Sub OpenSubForm(ByVal frm As Form)
        If TabMDI.Forms.Contains(frm) Then
            TabMDI.ActivateForm(frm)
        Else
            TabMDI.AddForm(frm, "", frm.Icon.ToBitmap())
            If frm.GetType().GetInterface("IWinControllable") IsNot Nothing Then
                Dim info As WinControl.WinInfo = CType(frm, WinControl.IWinControllable).WinInfo
                AddHandler info.TabPanel.MouseDown, AddressOf ShowTabContextMenu
            End If
        End If
    End Sub

    Private Sub ShowTabContextMenu(ByVal sender As Object, ByVal e As MouseEventArgs)
        Dim tab As Panel = sender
        If e.Button = Windows.Forms.MouseButtons.Right Then
            If tab.Parent.Controls.Count = 1 Then
                miCloseAllFormsButActive.Visible = False
            Else
                miCloseAllFormsButActive.Visible = True
            End If
            cmsTabMDI.Show(tab, 0, tab.Height)
        End If
    End Sub

    Private Sub Main_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control And e.KeyCode = Keys.W Then
            TabMDI.CloseForm(TabMDI.ActiveForm, Nothing)
        End If
    End Sub

    Private Sub Main_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        TabMDI.CloseAllForms(Nothing)
    End Sub

    Private Sub TabMDI_ActiveFormChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabMDI.ActiveFormChanged
        If TabMDI.ActiveForm Is Nothing Then
            Me.Icon = My.Resources.CSOL
            Me.Text = "CSOL管理系統"
        Else
            Me.Icon = TabMDI.ActiveForm.Icon
            Me.Text = String.Format("CSOL管理系統 - {0}", TabMDI.ActiveForm.Text)
        End If
    End Sub
#End Region

    Private Sub MaintainMenu()
        mi3DaysBeforeDailyReport.Text = String.Format("列印{0:yyyy-MM-dd}日報表", Today.AddDays(-3))
        mi2DaysBeforeDailyReport.Text = String.Format("列印{0:yyyy-MM-dd}日報表", Today.AddDays(-2))
        mi1DayBeforeDailyReport.Text = String.Format("列印{0:yyyy-MM-dd}日報表", Today.AddDays(-1))
        miTodayDailyReport.Text = "列印今日日報表"

        mi3MonthsBeforeMonthlyReport.Text = String.Format("列印{0:yyyy-MM}月報表", Today.AddMonths(-3))
        mi2MonthsBeforeMonthlyReport.Text = String.Format("列印{0:yyyy-MM}月報表", Today.AddMonths(-2))
        mi1MonthBeforeMonthlyReport.Text = String.Format("列印{0:yyyy-MM}月報表", Today.AddMonths(-1))
        miCurrentMonthMonthlyReport.Text = "列印本月月報表"
    End Sub

    Private Sub ExitToolsStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub Main_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetUserDisplayName()
        AddNetworkStatusHandler()
        MaintainMenu()

        Dim frm As New frmStuManager(True)
        OpenSubForm(frm)
    End Sub

    Private Sub Main_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        CheckPrinter()
        CheckWorkStation()
    End Sub

    Private Sub tTime_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tTime.Tick
        tssTime.Text = Now.ToString("yyyy-MM-dd tt hh:mm:ss")
    End Sub

    Private Sub GetUserDisplayName()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "GetUserDisplayName", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        My.Application.User = ResponseParams("Name")
        tssUser.Text = My.Application.User
    End Sub

    Private Sub CheckWorkStation()
        If My.Settings.WorkStationID = "" Then
            Dim dlg As New dlgWorkStation()
            dlg.ShowDialog()
            If My.Settings.WorkStationID = "" Then
                Me.Close()
            End If
        End If
    End Sub

    Private Sub CheckPrinter()
        Dim Printer As String = My.Settings.Printer
        For Each p As String In System.Drawing.Printing.PrinterSettings.InstalledPrinters
            If Printer = p Then
                Exit Sub
            End If
        Next

        My.Settings.Printer = New System.Drawing.Printing.PrinterSettings().PrinterName
        My.Settings.Save()
    End Sub

    Private Sub miTeacher_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miTeacher.Click
        OpenSubForm(frmTeacher)
    End Sub

    Private Sub miCourse_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miCourse.Click
        OpenSubForm(frmCourse)
    End Sub

    Private Sub miClassroom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClassroom.Click
        OpenSubForm(frmClassroom)
    End Sub

    Private Sub miClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClass.Click
        OpenSubForm(frmClass)
    End Sub

    Private Sub miAddStu_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miAddStu.Click
        Dim frm As New frmStuManager(True)
        OpenSubForm(frm)
    End Sub

    Private Sub miWorkStation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miWorkStation.Click
        Dim dlg As New dlgWorkStation()
        dlg.ShowDialog()
    End Sub

    Private Sub miChangePassword_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miChangePassword.Click
        Dim frm As New dlgChangePassword()
        frm.ShowDialog()
    End Sub

    Private Sub miPrinterSetting_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miPrinterSetting.Click
        Dim dlg As New dlgPrinter()
        dlg.ShowDialog()
    End Sub

    Private Sub miUsers_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miUsers.Click
        OpenSubForm(frmUsers)
    End Sub

    Private Sub miChangeUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miChangeUser.Click
        Dim frm As New frmLogin()
        If frm.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Me.Close()
        End If
    End Sub

    Private Sub miAccountingDailyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles miAccountingDailyReport.Click, mi3DaysBeforeDailyReport.Click, mi2DaysBeforeDailyReport.Click, mi1DayBeforeDailyReport.Click, miTodayDailyReport.Click

        Dim mi As ToolStripMenuItem = sender
        Dim ReportDate As Date = Today.AddDays(mi.Tag)
        PrintDailyReport(ReportDate)
    End Sub

    Private Sub miChooseDateDailyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miChooseDateDailyReport.Click
        Dim dlg As New dlgDatePicker()
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintDailyReport(dlg.ChosenDate)
        End If
    End Sub

    Private Sub PrintDailyReport(ByVal ReportDate As Date)
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Date", ReportDate.ToString("yyyy-MM-dd"))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Accounting", "GetDepartments", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        If ResponseParams("Departments") = "" Then
            MessageBox.Show(String.Format("「{0:yyyy-MM-dd}」無收支紀錄...", ReportDate), "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Departments() As String = ResponseParams("Departments").Split(",")

        ResponseBody = CSOL.HTTPClient.Post("Accounting", "GetDailyReport", RequestParams, System.Text.Encoding.UTF8)
        ResponseParams = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Try
            CSOL.Report.Accounting.DailyReport.DirectPrint(Departments, CSOL.Convert.XmlStringToDataTable(ResponseParams("DailyReport")), ReportDate, My.Application.User)
        Catch ex As Exception
            MessageBox.Show("列印時發生了錯誤...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub miAccountingMonthlyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles miAccountingMonthlyReport.Click, mi3MonthsBeforeMonthlyReport.Click, mi2MonthsBeforeMonthlyReport.Click, mi1MonthBeforeMonthlyReport.Click, miCurrentMonthMonthlyReport.Click

        Dim mi As ToolStripMenuItem = sender
        Dim ReportDate As Date = Today.AddMonths(mi.Tag)
        PrintMonthlyReport(ReportDate)
    End Sub

    Private Sub miChooseMonthMonthlyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miChooseMonthMonthlyReport.Click
        Dim dlg As New dlgDatePicker()
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintMonthlyReport(dlg.ChosenDate)
        End If
    End Sub

    Private Sub PrintMonthlyReport(ByVal ReportDate As Date)
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Date", ReportDate.ToString("yyyy-MM-01"))
        RequestParams.Add("AnotherDate", ReportDate.AddMonths(1).ToString("yyyy-MM-01"))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Accounting", "GetDepartments", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        If ResponseParams("Departments") = "" Then
            MessageBox.Show(String.Format("「{0:yyyy-MM}」無收支紀錄...", ReportDate), "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Departments() As String = ResponseParams("Departments").Split(",")

        ResponseBody = CSOL.HTTPClient.Post("Accounting", "GetMonthlyReport", RequestParams, System.Text.Encoding.UTF8)
        ResponseParams = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Try
            CSOL.Report.Accounting.MonthlyReport.DirectPrint(Departments, CSOL.Convert.XmlStringToDataTable(ResponseParams("MonthlyReport")), ReportDate, My.Application.User)
        Catch ex As Exception
            MessageBox.Show("列印時發生了錯誤...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub miAccountingYearlyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles miAccountingYearlyReport.Click, miLastYearYearlyReport.Click, miThisYearYearlyReport.Click

        Dim mi As ToolStripMenuItem = sender
        Dim ReportDate As Date = Today.AddYears(mi.Tag)
        PrintYearlyReport(ReportDate)
    End Sub

    Private Sub miChooseYearYearlyReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miChooseYearYearlyReport.Click
        Dim dlg As New dlgDatePicker()
        If dlg.ShowDialog() = Windows.Forms.DialogResult.OK Then
            PrintYearlyReport(dlg.ChosenDate)
        End If
    End Sub

    Private Sub PrintYearlyReport(ByVal ReportDate As Date)
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Date", ReportDate.ToString("yyyy-01-01"))
        RequestParams.Add("AnotherDate", ReportDate.AddYears(1).ToString("yyyy-01-01"))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Accounting", "GetDepartments", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        If ResponseParams("Departments") = "" Then
            MessageBox.Show(String.Format("「{0:yyyy}」無收支紀錄...", ReportDate), "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Exit Sub
        End If

        Dim Departments() As String = ResponseParams("Departments").Split(",")

        ResponseBody = CSOL.HTTPClient.Post("Accounting", "GetYearlyReport", RequestParams, System.Text.Encoding.UTF8)
        ResponseParams = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Try
            CSOL.Report.Accounting.YearlyReport.DirectPrint(Departments, CSOL.Convert.XmlStringToDataTable(ResponseParams("YearlyReport")), ReportDate, My.Application.User)
        Catch ex As Exception
            MessageBox.Show("列印時發生了錯誤...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End Try
    End Sub

    Private Sub miSMS_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miSMS.Click
        OpenSubForm(frmSMS)
    End Sub

    Private Sub miCloseActiveForm_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miCloseActiveForm.Click
        If TabMDI.ActiveForm IsNot Nothing Then
            TabMDI.ActiveForm.Close()
        End If
    End Sub

    Private Sub miCloseAllFormsButActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miCloseAllFormsButActive.Click
        TabMDI.CloseAllButActive(Nothing)
    End Sub

    Private Sub miStuList_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miStuList.Click
        OpenSubForm(frmStuManager)
    End Sub
End Class
