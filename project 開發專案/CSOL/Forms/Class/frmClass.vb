﻿Public Class frmClass
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private WithEvents m_GlobalMDI As WinControl.WinControl = My.Application.FormController.TabMDI
    Private Sub MainMDI_ActiveFormChanged(ByVal sender As Object, ByVal e As EventArgs) Handles m_GlobalMDI.ActiveFormChanged
        If Me.Equals(Me.m_GlobalMDI.ActiveForm) Then
            GetClassroomList()
            GetCourseList()
            GetList()
        End If
    End Sub

    Private m_ClassID As Integer = -1

    Private Sub frmClass_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        SetupDataGridViews()
        InitComboBox()
        GetList()
        dtpDateFrom.MinDate = Today
        dtpDateTo.MinDate = Today
        tbName.Focus()

        InitWeekDays()
        GetClassroomList()
        GetCourseList()
    End Sub

    Private m_List As DataTable
    Private m_ClassroomList As DataTable
    Private m_CourseList As DataTable
    Private m_ClassBooking As DataTable
    Private m_ClassBookingDetail As DataTable

    Private Sub SetupDataGridViews()
        dgvList.AutoGenerateColumns = False
        dgvClassroomList.AutoGenerateColumns = False
        dgvCourseList.AutoGenerateColumns = False
        dgvClassBooking.AutoGenerateColumns = False
        dgvClassBookingDetail.AutoGenerateColumns = False
    End Sub

    Private Sub InitComboBox()
        With cbClassType
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("ClassType")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With
    End Sub

    Private Sub GetList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_List = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvList.DataSource = Me.m_List
    End Sub

    Private Sub GetItem()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_ClassID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "GetItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Item"))
        If dt.Rows.Count > 0 Then
            Dim row As DataRow = dt.Rows(0)
            tbName.Text = row("Name")
            cbClassType.Text = row("Type")
            tbListPrice.Text = String.Format("{0:#,0}", row("ListPrice"))
            tbAccountsReceivable.Text = String.Format("{0:#,0}", row("AccountsReceivable"))

            dtpDateFrom.MinDate = row("DateFrom")
            dtpDateTo.MinDate = row("DateFrom")

            dtpDateFrom.Value = row("DateFrom")
            dtpDateTo.Value = row("DateTo")

            cbBookSeat.Checked = IIf(row("BookSeat") = 0, False, True)
        End If
    End Sub

    Private Sub dgvList_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvList.SelectionChanged
        If dgvList.SelectedRows.Count > 0 Then
            If dgvList.SelectedRows(0).IsNewRow Then
                Me.m_ClassID = -1
                tbName.ResetText()
                With cbClassType
                    If .Items.Count > 0 Then
                        .SelectedIndex = 0
                    End If
                End With
                tbListPrice.ResetText()
                tbAccountsReceivable.ResetText()
                dtpDateFrom.MinDate = Today
                dtpDateTo.MinDate = Today
                gbBooking.ResetText()
            Else
                Me.m_ClassID = dgvList.SelectedRows(0).DataBoundItem.Row("ID")
                GetItem()

                gbBooking.Text = String.Format("{0} - 登記教室使用時間", dgvList.SelectedRows(0).DataBoundItem.Row("Name"))
                GetBooking()
            End If
        End If
    End Sub

    Private Sub tbAmount_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbListPrice.Enter, tbAccountsReceivable.Enter
        Dim tb As TextBox = sender
        If tb.Text.Trim() = "" Then
            tb.Text = ""
            Exit Sub
        End If

        Dim value As Integer = 0
        If Integer.TryParse(tb.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, value) Then
            tb.Text = IIf(value = 0, "", value)
        End If
    End Sub

    Private Sub tbAmount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbListPrice.Leave, tbAccountsReceivable.Leave
        Dim tb As TextBox = sender
        Dim value As Integer = 0
        If Integer.TryParse(tb.Text, Globalization.NumberStyles.Any, Globalization.CultureInfo.CurrentCulture, value) Then
            tb.Text = value.ToString("#,0")
        Else
            tb.Text = ""
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("姓名不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        Dim listPrice As Integer = 0
        Dim accountsReceivable As Integer = 0

        With tbListPrice
            If False = Integer.TryParse(.Text, _
                                System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, listPrice) Then

                MessageBox.Show("約定金額填錯了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                .Select()
                Return
            End If
        End With

        With tbAccountsReceivable
            If False = Integer.TryParse(.Text, _
                                System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, accountsReceivable) Then

                MessageBox.Show("應收金額填錯了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                .Select()
                Return
            End If
        End With

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", -1)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Type", cbClassType.Text)
        RequestParams.Add("ListPrice", listPrice)
        RequestParams.Add("AccountsReceivable", accountsReceivable)
        RequestParams.Add("DateFrom", dtpDateFrom.Value)
        RequestParams.Add("DateTo", dtpDateTo.Value)
        RequestParams.Add("BookSeat", cbBookSeat.Checked)

        Dim Response As String = CSOL.HTTPClient.Post("Class", "SaveItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            GetList()
        Else
            MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("姓名不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        Dim listPrice As Integer = 0
        Dim accountsReceivable As Integer = 0

        With tbListPrice
            If False = Integer.TryParse(.Text, _
                                System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, listPrice) Then
                MessageBox.Show("約定金額填錯了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                .Select()
                Return
            End If
        End With

        With tbAccountsReceivable
            If False = Integer.TryParse(.Text, _
                                System.Globalization.NumberStyles.Any, System.Globalization.CultureInfo.CurrentCulture, accountsReceivable) Then
                MessageBox.Show("應收金額填錯了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                .Select()
                Return
            End If
        End With

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_ClassID)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Type", cbClassType.Text)
        RequestParams.Add("ListPrice", listPrice)
        RequestParams.Add("AccountsReceivable", accountsReceivable)
        RequestParams.Add("DateFrom", dtpDateFrom.Value)
        RequestParams.Add("DateTo", dtpDateTo.Value)
        RequestParams.Add("BookSeat", cbBookSeat.Checked)

        Dim Response As String = CSOL.HTTPClient.Post("Class", "SaveItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            GetList()
        Else
            MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        If dgvList.SelectedRows(0).IsNewRow Then
            Me.m_ClassID = -1
            tbName.ResetText()
            With cbClassType
                If .Items.Count > 0 Then
                    .SelectedIndex = 0
                End If
            End With
            tbListPrice.ResetText()
            tbAccountsReceivable.ResetText()
        Else
            Me.m_ClassID = dgvList.SelectedRows(0).Cells("colID").Value
            GetItem()
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If Me.m_ClassID > 0 Then
            If MessageBox.Show(String.Format("您確定要刪除這個班級嗎??{0}刪除這個班級可能會讓學生的班級紀錄消失!!!", vbCrLf), "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("ID", Me.m_ClassID)

                Dim Response As String = CSOL.HTTPClient.Post("Class", "RemoveItem", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
                Dim Status As String = ResponseParams("Status")
                If Status = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GetList()
                Else
                    MessageBox.Show("刪除失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private m_ClassroomID As Integer = -1
    Private Sub GetClassroomList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_ClassroomList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        Me.m_ClassroomList.Columns.Add("SeatCount", GetType(Int32))

        For Each row As DataRow In Me.m_ClassroomList.Rows
            Dim pts() As Point = CSOL.Convert.StringToPoints(row("Disables").ToString())
            row("SeatCount") = row("Columns") * row("Rows") - pts.Length
        Next
        dgvClassroomList.DataSource = Me.m_ClassroomList
    End Sub

    Private Sub GetCourseList()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ShowName", True)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Course", "GetList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_CourseList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvCourseList.DataSource = Me.m_CourseList
    End Sub

    Private Sub dgvClassroomList_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvClassroomList.SelectionChanged
        If dgvClassroomList.SelectedRows.Count > 0 Then
            Me.m_ClassroomID = dgvClassroomList.SelectedRows(0).DataBoundItem.Row("ID")
        End If
    End Sub

    Private Sub InitDateTimePickers()
        dtpPeriodDateFrom.MinDate = Today
        dtpPeriodDateTo.MinDate = Today
        dtpSingleDate.MinDate = Today

        dtpPeriodDateFrom.MaxDate = Today.AddYears(2)
        dtpPeriodDateTo.MaxDate = Today.AddYears(2)
        dtpSingleDate.MaxDate = Today.AddYears(2)
    End Sub

    Private Sub InitWeekDays()
        cbMonday.Tag = DayOfWeek.Monday.ToString("d")
        cbTuesday.Tag = DayOfWeek.Tuesday.ToString("d")
        cbWednesday.Tag = DayOfWeek.Wednesday.ToString("d")
        cbThursday.Tag = DayOfWeek.Thursday.ToString("d")
        cbFriday.Tag = DayOfWeek.Friday.ToString("d")
        cbSaturday.Tag = DayOfWeek.Saturday.ToString("d")
        cbSunday.Tag = DayOfWeek.Sunday.ToString("d")
    End Sub

    Private Sub GetBooking()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ClassID", Me.m_ClassID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "GetBooking", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_ClassBooking = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        Me.m_ClassBooking.Columns.Add("WeekdayNames", GetType(String))

        dgvClassBookingDetail.DataSource = Nothing
        For Each row As DataRow In Me.m_ClassBooking.Rows
            row("WeekdayNames") = row("Weekdays").ToString().Replace("1", "一").Replace("2", "二").Replace("3", "三").Replace("4", "四").Replace("5", "五").Replace("6", "六").Replace("0", "日")
        Next
        dgvClassBooking.DataSource = Me.m_ClassBooking
    End Sub

    Private Sub GetBookingDetail()
        If dgvClassBooking.SelectedRows.Count > 0 Then
            Dim BatchID As Integer = dgvClassBooking.SelectedRows(0).DataBoundItem.Row("ID")
            Dim ClassID As Integer = Me.m_ClassID
            Dim ClassroomID As Integer = dgvClassBooking.SelectedRows(0).DataBoundItem.Row("ClassroomID")
            If True = cbShowAllClassroomDetail.Checked Then
                BatchID = -1
            End If

            If True = cbShowAllDetail.Checked Then
                BatchID = -1
                ClassroomID = -1
            End If

            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("BatchID", BatchID)
            RequestParams.Add("ClassID", ClassID)
            RequestParams.Add("ClassroomID", ClassroomID)
            RequestParams.Add("ShowOverlap", cbShowOverlap.Checked)

            Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "GetBookingDetail", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
            Me.m_ClassBookingDetail = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
            dgvClassBookingDetail.DataSource = Me.m_ClassBookingDetail

            For Each row As DataGridViewRow In dgvClassBookingDetail.Rows
                If row.DataBoundItem.Row("Overlaped") = 1 Then
                    row.DefaultCellStyle.ForeColor = Color.Red
                End If
            Next
        End If
    End Sub

    Private Sub btnAddBooking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddBooking.Click
        If Me.m_ClassroomID = -1 Then
            MessageBox.Show("目前沒有教室可用...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Select Case tcBooking.SelectedTab.Name
                Case tpPeriod.Name
                    Dim weekdays As New List(Of String)
                    Dim cbs() As CheckBox = New CheckBox() {cbMonday, cbTuesday, cbWednesday, cbThursday, cbFriday, cbSaturday, cbSunday}
                    For Each cb As CheckBox In cbs
                        If cb.Checked Then
                            weekdays.Add(cb.Tag)
                        End If
                    Next

                    If weekdays.Count = 0 Then
                        MessageBox.Show("請選擇星期...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    Else
                        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                        RequestParams.Add("Type", "Period")
                        RequestParams.Add("ClassID", Me.m_ClassID)
                        RequestParams.Add("ClassroomID", Me.m_ClassroomID)
                        RequestParams.Add("CourseID", IIf(dgvCourseList.SelectedRows.Count > 0, dgvCourseList.SelectedRows(0).Cells("colCourseID").Value, -1))
                        RequestParams.Add("DateFrom", dtpPeriodDateFrom.Value.ToString("yyyy-MM-dd"))
                        RequestParams.Add("DateTo", dtpPeriodDateTo.Value.ToString("yyyy-MM-dd"))
                        RequestParams.Add("Weekdays", String.Join(",", weekdays.ToArray()))
                        RequestParams.Add("TimeFrom", dtpPeriodTimeFrom.Value.ToString("HH:mm:ss"))
                        RequestParams.Add("TimeTo", dtpPeriodTimeTo.Value.ToString("HH:mm:ss"))
                        RequestParams.Add("AllowMix", IIf(cbAllowMix.Checked, 1, 0))

                        Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "AddBooking", RequestParams, System.Text.Encoding.UTF8)
                        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                        Dim Status As String = ResponseParams("Status")
                        If Status = "OK" Then
                            MessageBox.Show("新增成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                            GetList()
                        Else
                            MessageBox.Show("新增失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    End If

                Case tpDate.Name
                    Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                    RequestParams.Add("Type", "Single")
                    RequestParams.Add("ClassID", Me.m_ClassID)
                    RequestParams.Add("ClassroomID", Me.m_ClassroomID)
                    RequestParams.Add("CourseID", IIf(dgvCourseList.SelectedRows.Count > 0, dgvCourseList.SelectedRows(0).Cells("colCourseID").Value, -1))
                    RequestParams.Add("Date", dtpSingleDate.Value.ToString("yyyy-MM-dd"))
                    RequestParams.Add("TimeFrom", dtpSingleTimeFrom.Value.ToString("HH:mm:ss"))
                    RequestParams.Add("TimeTo", dtpSingleTimeTo.Value.ToString("HH:mm:ss"))
                    RequestParams.Add("AllowMix", IIf(cbAllowMix.Checked, 1, 0))

                    Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "AddBooking", RequestParams, System.Text.Encoding.UTF8)
                    Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                    Dim Status As String = ResponseParams("Status")
                    If Status = "OK" Then
                        MessageBox.Show("新增成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                        GetList()
                    Else
                        MessageBox.Show("新增失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

            End Select
        End If
    End Sub

    Private Sub btnResetBooking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetBooking.Click
        Select Case tcBooking.SelectedTab.Name
            Case tpPeriod.Name
                dtpPeriodDateFrom.Value = Today
                dtpPeriodDateTo.Value = Today

                cbMonday.Checked = False
                cbTuesday.Checked = False
                cbWednesday.Checked = False
                cbThursday.Checked = False
                cbFriday.Checked = False
                cbSaturday.Checked = False
                cbSunday.Checked = False

                dtpPeriodTimeFrom.Text = "07:00:00"
                dtpPeriodTimeTo.Text = "09:00:00"
            Case tpDate.Name
                dtpSingleDate.Value = Today
                dtpSingleTimeFrom.Text = "07:00:00"
                dtpSingleTimeTo.Text = "09:00:00"
        End Select

        cbAllowMix.Checked = False
    End Sub

    Private Sub dgvClassBooking_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvClassBooking.SelectionChanged
        If dgvClassBooking.SelectedRows.Count > 0 Then
            GetBookingDetail()
        End If
    End Sub

    Private Sub btnRemoveBooking_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBooking.Click
        If dgvClassBooking.SelectedRows.Count > 0 Then
            If MessageBox.Show("你確定要刪除嗎??", "注意...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("ID", dgvClassBooking.SelectedRows(0).DataBoundItem.Row("ID"))

                Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "RemoveBooking", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                Dim Status As String = ResponseParams("Status")
                If Status = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GetBooking()
                Else
                    MessageBox.Show("刪除失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private Sub cbShowAllDetail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbShowAllDetail.CheckedChanged
        If cbShowAllDetail.Checked Then
            cbShowAllClassroomDetail.Checked = True
        End If

        GetBookingDetail()
    End Sub

    Private Sub cbShowAllClassroomDetail_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbShowAllClassroomDetail.CheckedChanged
        If cbShowAllClassroomDetail.Checked = False Then
            cbShowAllDetail.Checked = False
        End If

        GetBookingDetail()
    End Sub

    Private Sub btnRemoveBookingDetail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveBookingDetail.Click
        If dgvClassBookingDetail.SelectedRows.Count > 0 Then
            If MessageBox.Show("你確定要刪除嗎??", "注意...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("ID", dgvClassBookingDetail.SelectedRows(0).DataBoundItem.Row("ID"))

                Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "RemoveBookingDetail", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                Dim Status As String = ResponseParams("Status")
                If Status = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GetBooking()
                Else
                    MessageBox.Show("刪除失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private Sub cbShowOverlap_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbShowOverlap.CheckedChanged, cbAllowMix.CheckedChanged
        GetBookingDetail()
    End Sub

    Private Sub btnIsPrimaryClassroom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnIsPrimaryClassroom.Click
        If dgvClassBooking.SelectedRows.Count > 0 Then
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("ID", dgvClassBooking.SelectedRows(0).DataBoundItem.Row("ID"))

            Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "SetPrimaryClassroom", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
            Dim Status As String = ResponseParams("Status")
            If Status = "OK" Then
                GetBooking()
            End If
        End If
    End Sub
End Class