﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClass
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClass))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colListPrice = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colAccountsReceivable = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbName = New System.Windows.Forms.TextBox
        Me.cbClassType = New System.Windows.Forms.ComboBox
        Me.tbListPrice = New System.Windows.Forms.TextBox
        Me.tbAccountsReceivable = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.dtpDateFrom = New System.Windows.Forms.DateTimePicker
        Me.Label13 = New System.Windows.Forms.Label
        Me.dtpDateTo = New System.Windows.Forms.DateTimePicker
        Me.cbBookSeat = New System.Windows.Forms.CheckBox
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.gbBooking = New System.Windows.Forms.GroupBox
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer6 = New System.Windows.Forms.SplitContainer
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgvClassroomList = New System.Windows.Forms.DataGridView
        Me.colClassroomID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassroomListName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colColumns = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colRows = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colSeats = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox7 = New System.Windows.Forms.GroupBox
        Me.dgvCourseList = New System.Windows.Forms.DataGridView
        Me.colCourseID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colCourseTeacher = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colCourseName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colCourseDescription = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tcBooking = New System.Windows.Forms.TabControl
        Me.tpPeriod = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.cbSunday = New System.Windows.Forms.CheckBox
        Me.cbWednesday = New System.Windows.Forms.CheckBox
        Me.cbSaturday = New System.Windows.Forms.CheckBox
        Me.cbFriday = New System.Windows.Forms.CheckBox
        Me.cbThursday = New System.Windows.Forms.CheckBox
        Me.cbTuesday = New System.Windows.Forms.CheckBox
        Me.cbMonday = New System.Windows.Forms.CheckBox
        Me.dtpPeriodDateFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpPeriodDateTo = New System.Windows.Forms.DateTimePicker
        Me.dtpPeriodTimeFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpPeriodTimeTo = New System.Windows.Forms.DateTimePicker
        Me.tpDate = New System.Windows.Forms.TabPage
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.Label14 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.dtpSingleTimeFrom = New System.Windows.Forms.DateTimePicker
        Me.dtpSingleTimeTo = New System.Windows.Forms.DateTimePicker
        Me.Label10 = New System.Windows.Forms.Label
        Me.dtpSingleDate = New System.Windows.Forms.DateTimePicker
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnResetBooking = New System.Windows.Forms.Button
        Me.cbAllowMix = New System.Windows.Forms.CheckBox
        Me.btnAddBooking = New System.Windows.Forms.Button
        Me.SplitContainer5 = New System.Windows.Forms.SplitContainer
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.dgvClassBooking = New System.Windows.Forms.DataGridView
        Me.colBookingID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colBookingClassroomID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colIsPrimaryClassroom = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colCourse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassroomName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDatePeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colWeekdays = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colTimePeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colAllowMix = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.btnIsPrimaryClassroom = New System.Windows.Forms.Button
        Me.cbShowAllClassroomDetail = New System.Windows.Forms.CheckBox
        Me.cbShowAllDetail = New System.Windows.Forms.CheckBox
        Me.btnRemoveBooking = New System.Windows.Forms.Button
        Me.GroupBox6 = New System.Windows.Forms.GroupBox
        Me.dgvClassBookingDetail = New System.Windows.Forms.DataGridView
        Me.colDetailID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailClassName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailCourse = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailClassroomName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailTimePeriod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailAllowMix = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDetailOverlaped = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.Label11 = New System.Windows.Forms.Label
        Me.cbShowOverlap = New System.Windows.Forms.CheckBox
        Me.btnRemoveBookingDetail = New System.Windows.Forms.Button
        Me.ttInfo = New System.Windows.Forms.ToolTip(Me.components)
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.gbBooking.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.Panel2.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        Me.SplitContainer6.Panel1.SuspendLayout()
        Me.SplitContainer6.Panel2.SuspendLayout()
        Me.SplitContainer6.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvClassroomList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox7.SuspendLayout()
        CType(Me.dgvCourseList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tcBooking.SuspendLayout()
        Me.tpPeriod.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.tpDate.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SplitContainer5.Panel1.SuspendLayout()
        Me.SplitContainer5.Panel2.SuspendLayout()
        Me.SplitContainer5.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvClassBooking, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel5.SuspendLayout()
        Me.GroupBox6.SuspendLayout()
        CType(Me.dgvClassBookingDetail, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.gbBooking)
        Me.SplitContainer1.Size = New System.Drawing.Size(984, 462)
        Me.SplitContainer1.SplitterDistance = 277
        Me.SplitContainer1.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupBox2)
        Me.SplitContainer2.Size = New System.Drawing.Size(277, 462)
        Me.SplitContainer2.SplitterDistance = 219
        Me.SplitContainer2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvList)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(277, 219)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "班級一覽表"
        '
        'dgvList
        '
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colName, Me.colType, Me.colListPrice, Me.colAccountsReceivable, Me.colDate})
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(3, 18)
        Me.dgvList.MultiSelect = False
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowTemplate.Height = 24
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(271, 198)
        Me.dgvList.TabIndex = 0
        '
        'colID
        '
        Me.colID.DataPropertyName = "ID"
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        '
        'colName
        '
        Me.colName.DataPropertyName = "Name"
        Me.colName.HeaderText = "名稱"
        Me.colName.Name = "colName"
        Me.colName.ReadOnly = True
        '
        'colType
        '
        Me.colType.DataPropertyName = "Type"
        Me.colType.HeaderText = "班別"
        Me.colType.Name = "colType"
        Me.colType.ReadOnly = True
        '
        'colListPrice
        '
        Me.colListPrice.DataPropertyName = "ListPrice"
        DataGridViewCellStyle1.Format = "$#,0"
        Me.colListPrice.DefaultCellStyle = DataGridViewCellStyle1
        Me.colListPrice.HeaderText = "約定金額"
        Me.colListPrice.Name = "colListPrice"
        Me.colListPrice.ReadOnly = True
        '
        'colAccountsReceivable
        '
        Me.colAccountsReceivable.DataPropertyName = "AccountsReceivable"
        DataGridViewCellStyle2.Format = "$#,0"
        Me.colAccountsReceivable.DefaultCellStyle = DataGridViewCellStyle2
        Me.colAccountsReceivable.HeaderText = "應收金額"
        Me.colAccountsReceivable.Name = "colAccountsReceivable"
        Me.colAccountsReceivable.ReadOnly = True
        '
        'colDate
        '
        Me.colDate.DataPropertyName = "Date"
        Me.colDate.HeaderText = "修業期間"
        Me.colDate.Name = "colDate"
        Me.colDate.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(277, 239)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "班級資訊"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.tbName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.cbClassType, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbListPrice, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbAccountsReceivable, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpDateFrom, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label13, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpDateTo, 1, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.cbBookSeat, 1, 6)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 18)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 8
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(271, 186)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(41, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "名稱："
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(41, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 12)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "班別："
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(17, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 12)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "約定金額："
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(17, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 12)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "應收金額："
        '
        'tbName
        '
        Me.tbName.Location = New System.Drawing.Point(88, 3)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(180, 22)
        Me.tbName.TabIndex = 0
        '
        'cbClassType
        '
        Me.cbClassType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClassType.FormattingEnabled = True
        Me.cbClassType.Location = New System.Drawing.Point(88, 29)
        Me.cbClassType.Name = "cbClassType"
        Me.cbClassType.Size = New System.Drawing.Size(180, 20)
        Me.cbClassType.TabIndex = 1
        '
        'tbListPrice
        '
        Me.tbListPrice.Location = New System.Drawing.Point(88, 55)
        Me.tbListPrice.Name = "tbListPrice"
        Me.tbListPrice.Size = New System.Drawing.Size(180, 22)
        Me.tbListPrice.TabIndex = 2
        '
        'tbAccountsReceivable
        '
        Me.tbAccountsReceivable.Location = New System.Drawing.Point(88, 81)
        Me.tbAccountsReceivable.Name = "tbAccountsReceivable"
        Me.tbAccountsReceivable.Size = New System.Drawing.Size(180, 22)
        Me.tbAccountsReceivable.TabIndex = 3
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(17, 111)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(65, 12)
        Me.Label12.TabIndex = 1
        Me.Label12.Text = "修業期間："
        '
        'dtpDateFrom
        '
        Me.dtpDateFrom.Location = New System.Drawing.Point(88, 107)
        Me.dtpDateFrom.Name = "dtpDateFrom"
        Me.dtpDateFrom.Size = New System.Drawing.Size(180, 22)
        Me.dtpDateFrom.TabIndex = 4
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(53, 137)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(29, 12)
        Me.Label13.TabIndex = 1
        Me.Label13.Text = "至："
        '
        'dtpDateTo
        '
        Me.dtpDateTo.Location = New System.Drawing.Point(88, 133)
        Me.dtpDateTo.Name = "dtpDateTo"
        Me.dtpDateTo.Size = New System.Drawing.Size(180, 22)
        Me.dtpDateTo.TabIndex = 5
        '
        'cbBookSeat
        '
        Me.cbBookSeat.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbBookSeat.AutoSize = True
        Me.cbBookSeat.Checked = True
        Me.cbBookSeat.CheckState = System.Windows.Forms.CheckState.Checked
        Me.cbBookSeat.ForeColor = System.Drawing.Color.Maroon
        Me.cbBookSeat.Location = New System.Drawing.Point(88, 161)
        Me.cbBookSeat.Name = "cbBookSeat"
        Me.cbBookSeat.Size = New System.Drawing.Size(94, 16)
        Me.cbBookSeat.TabIndex = 6
        Me.cbBookSeat.Text = "報名後劃位??"
        Me.ttInfo.SetToolTip(Me.cbBookSeat, "勾選報名後劃位" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "學生報名後會直接跳出教室劃位置")
        Me.cbBookSeat.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnRemove)
        Me.Panel1.Controls.Add(Me.btnReset)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(3, 204)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(271, 32)
        Me.Panel1.TabIndex = 0
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(208, 3)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(60, 26)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Text = "刪除"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(135, 3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(60, 26)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "重來"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(69, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 26)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "修改"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(3, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(60, 26)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "新增"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'gbBooking
        '
        Me.gbBooking.BackColor = System.Drawing.Color.FromArgb(CType(CType(170, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.gbBooking.Controls.Add(Me.SplitContainer3)
        Me.gbBooking.Dock = System.Windows.Forms.DockStyle.Fill
        Me.gbBooking.Location = New System.Drawing.Point(0, 0)
        Me.gbBooking.Name = "gbBooking"
        Me.gbBooking.Size = New System.Drawing.Size(703, 462)
        Me.gbBooking.TabIndex = 0
        Me.gbBooking.TabStop = False
        Me.gbBooking.Text = "登記教室使用時間"
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer3.Location = New System.Drawing.Point(3, 18)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.SplitContainer4)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.SplitContainer5)
        Me.SplitContainer3.Size = New System.Drawing.Size(697, 441)
        Me.SplitContainer3.SplitterDistance = 278
        Me.SplitContainer3.TabIndex = 0
        '
        'SplitContainer4
        '
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer4.Name = "SplitContainer4"
        Me.SplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.SplitContainer6)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.Controls.Add(Me.tcBooking)
        Me.SplitContainer4.Panel2.Controls.Add(Me.Panel2)
        Me.SplitContainer4.Size = New System.Drawing.Size(278, 441)
        Me.SplitContainer4.SplitterDistance = 225
        Me.SplitContainer4.TabIndex = 0
        '
        'SplitContainer6
        '
        Me.SplitContainer6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer6.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer6.Name = "SplitContainer6"
        Me.SplitContainer6.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer6.Panel1
        '
        Me.SplitContainer6.Panel1.Controls.Add(Me.GroupBox4)
        '
        'SplitContainer6.Panel2
        '
        Me.SplitContainer6.Panel2.Controls.Add(Me.GroupBox7)
        Me.SplitContainer6.Size = New System.Drawing.Size(278, 225)
        Me.SplitContainer6.SplitterDistance = 107
        Me.SplitContainer6.TabIndex = 1
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvClassroomList)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(278, 107)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "教室一覽表"
        '
        'dgvClassroomList
        '
        Me.dgvClassroomList.AllowUserToAddRows = False
        Me.dgvClassroomList.AllowUserToDeleteRows = False
        Me.dgvClassroomList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClassroomList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colClassroomID, Me.colClassroomListName, Me.colColumns, Me.colRows, Me.colSeats})
        Me.dgvClassroomList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvClassroomList.Location = New System.Drawing.Point(3, 18)
        Me.dgvClassroomList.MultiSelect = False
        Me.dgvClassroomList.Name = "dgvClassroomList"
        Me.dgvClassroomList.ReadOnly = True
        Me.dgvClassroomList.RowTemplate.Height = 24
        Me.dgvClassroomList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassroomList.Size = New System.Drawing.Size(272, 86)
        Me.dgvClassroomList.TabIndex = 0
        '
        'colClassroomID
        '
        Me.colClassroomID.DataPropertyName = "ID"
        Me.colClassroomID.HeaderText = "ID"
        Me.colClassroomID.Name = "colClassroomID"
        Me.colClassroomID.ReadOnly = True
        Me.colClassroomID.Visible = False
        '
        'colClassroomListName
        '
        Me.colClassroomListName.DataPropertyName = "Name"
        Me.colClassroomListName.HeaderText = "名稱"
        Me.colClassroomListName.Name = "colClassroomListName"
        Me.colClassroomListName.ReadOnly = True
        '
        'colColumns
        '
        Me.colColumns.DataPropertyName = "Columns"
        Me.colColumns.HeaderText = "行"
        Me.colColumns.Name = "colColumns"
        Me.colColumns.ReadOnly = True
        Me.colColumns.Width = 50
        '
        'colRows
        '
        Me.colRows.DataPropertyName = "Rows"
        Me.colRows.HeaderText = "列"
        Me.colRows.Name = "colRows"
        Me.colRows.ReadOnly = True
        Me.colRows.Width = 50
        '
        'colSeats
        '
        Me.colSeats.DataPropertyName = "SeatCount"
        Me.colSeats.HeaderText = "座位數"
        Me.colSeats.Name = "colSeats"
        Me.colSeats.ReadOnly = True
        '
        'GroupBox7
        '
        Me.GroupBox7.Controls.Add(Me.dgvCourseList)
        Me.GroupBox7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox7.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox7.Name = "GroupBox7"
        Me.GroupBox7.Size = New System.Drawing.Size(278, 114)
        Me.GroupBox7.TabIndex = 1
        Me.GroupBox7.TabStop = False
        Me.GroupBox7.Text = "課程一覽表"
        '
        'dgvCourseList
        '
        Me.dgvCourseList.AllowUserToAddRows = False
        Me.dgvCourseList.AllowUserToDeleteRows = False
        Me.dgvCourseList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvCourseList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colCourseID, Me.colCourseTeacher, Me.colCourseName, Me.colCourseDescription})
        Me.dgvCourseList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvCourseList.Location = New System.Drawing.Point(3, 18)
        Me.dgvCourseList.MultiSelect = False
        Me.dgvCourseList.Name = "dgvCourseList"
        Me.dgvCourseList.ReadOnly = True
        Me.dgvCourseList.RowTemplate.Height = 24
        Me.dgvCourseList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvCourseList.Size = New System.Drawing.Size(272, 93)
        Me.dgvCourseList.TabIndex = 0
        '
        'colCourseID
        '
        Me.colCourseID.DataPropertyName = "ID"
        Me.colCourseID.HeaderText = "ID"
        Me.colCourseID.Name = "colCourseID"
        Me.colCourseID.ReadOnly = True
        Me.colCourseID.Visible = False
        '
        'colCourseTeacher
        '
        Me.colCourseTeacher.DataPropertyName = "Teacher"
        Me.colCourseTeacher.HeaderText = "授課老師"
        Me.colCourseTeacher.Name = "colCourseTeacher"
        Me.colCourseTeacher.ReadOnly = True
        '
        'colCourseName
        '
        Me.colCourseName.DataPropertyName = "Name"
        Me.colCourseName.HeaderText = "課程名稱"
        Me.colCourseName.Name = "colCourseName"
        Me.colCourseName.ReadOnly = True
        '
        'colCourseDescription
        '
        Me.colCourseDescription.DataPropertyName = "Description"
        Me.colCourseDescription.HeaderText = "詳細資料"
        Me.colCourseDescription.Name = "colCourseDescription"
        Me.colCourseDescription.ReadOnly = True
        '
        'tcBooking
        '
        Me.tcBooking.Controls.Add(Me.tpPeriod)
        Me.tcBooking.Controls.Add(Me.tpDate)
        Me.tcBooking.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcBooking.Location = New System.Drawing.Point(0, 0)
        Me.tcBooking.Name = "tcBooking"
        Me.tcBooking.SelectedIndex = 0
        Me.tcBooking.Size = New System.Drawing.Size(278, 180)
        Me.tcBooking.TabIndex = 0
        '
        'tpPeriod
        '
        Me.tpPeriod.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tpPeriod.Controls.Add(Me.TableLayoutPanel2)
        Me.tpPeriod.Location = New System.Drawing.Point(4, 22)
        Me.tpPeriod.Name = "tpPeriod"
        Me.tpPeriod.Padding = New System.Windows.Forms.Padding(3)
        Me.tpPeriod.Size = New System.Drawing.Size(270, 154)
        Me.tpPeriod.TabIndex = 0
        Me.tpPeriod.Text = "區段新增"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label9, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel4, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.dtpPeriodDateFrom, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.dtpPeriodDateTo, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.dtpPeriodTimeFrom, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.dtpPeriodTimeTo, 1, 4)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 6
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(264, 148)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(14, 7)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(53, 12)
        Me.Label5.TabIndex = 1
        Me.Label5.Text = "日期起："
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(14, 33)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 12)
        Me.Label6.TabIndex = 1
        Me.Label6.Text = "日期迄："
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(26, 64)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 1
        Me.Label7.Text = "星期："
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(14, 121)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 12)
        Me.Label8.TabIndex = 1
        Me.Label8.Text = "時間迄："
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(14, 95)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(53, 12)
        Me.Label9.TabIndex = 1
        Me.Label9.Text = "時間起："
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.cbSunday)
        Me.Panel4.Controls.Add(Me.cbWednesday)
        Me.Panel4.Controls.Add(Me.cbSaturday)
        Me.Panel4.Controls.Add(Me.cbFriday)
        Me.Panel4.Controls.Add(Me.cbThursday)
        Me.Panel4.Controls.Add(Me.cbTuesday)
        Me.Panel4.Controls.Add(Me.cbMonday)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(70, 52)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(194, 36)
        Me.Panel4.TabIndex = 2
        '
        'cbSunday
        '
        Me.cbSunday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbSunday.AutoSize = True
        Me.cbSunday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbSunday.Location = New System.Drawing.Point(174, 3)
        Me.cbSunday.Name = "cbSunday"
        Me.cbSunday.Size = New System.Drawing.Size(21, 30)
        Me.cbSunday.TabIndex = 6
        Me.cbSunday.Text = "日"
        Me.cbSunday.UseVisualStyleBackColor = True
        '
        'cbWednesday
        '
        Me.cbWednesday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbWednesday.AutoSize = True
        Me.cbWednesday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbWednesday.Location = New System.Drawing.Point(62, 3)
        Me.cbWednesday.Name = "cbWednesday"
        Me.cbWednesday.Size = New System.Drawing.Size(21, 30)
        Me.cbWednesday.TabIndex = 2
        Me.cbWednesday.Text = "三"
        Me.cbWednesday.UseVisualStyleBackColor = True
        '
        'cbSaturday
        '
        Me.cbSaturday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbSaturday.AutoSize = True
        Me.cbSaturday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbSaturday.Location = New System.Drawing.Point(146, 3)
        Me.cbSaturday.Name = "cbSaturday"
        Me.cbSaturday.Size = New System.Drawing.Size(21, 30)
        Me.cbSaturday.TabIndex = 5
        Me.cbSaturday.Text = "六"
        Me.cbSaturday.UseVisualStyleBackColor = True
        '
        'cbFriday
        '
        Me.cbFriday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbFriday.AutoSize = True
        Me.cbFriday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbFriday.Location = New System.Drawing.Point(118, 3)
        Me.cbFriday.Name = "cbFriday"
        Me.cbFriday.Size = New System.Drawing.Size(21, 30)
        Me.cbFriday.TabIndex = 4
        Me.cbFriday.Text = "五"
        Me.cbFriday.UseVisualStyleBackColor = True
        '
        'cbThursday
        '
        Me.cbThursday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbThursday.AutoSize = True
        Me.cbThursday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbThursday.Location = New System.Drawing.Point(90, 3)
        Me.cbThursday.Name = "cbThursday"
        Me.cbThursday.Size = New System.Drawing.Size(21, 30)
        Me.cbThursday.TabIndex = 3
        Me.cbThursday.Text = "四"
        Me.cbThursday.UseVisualStyleBackColor = True
        '
        'cbTuesday
        '
        Me.cbTuesday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbTuesday.AutoSize = True
        Me.cbTuesday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbTuesday.Location = New System.Drawing.Point(34, 3)
        Me.cbTuesday.Name = "cbTuesday"
        Me.cbTuesday.Size = New System.Drawing.Size(21, 30)
        Me.cbTuesday.TabIndex = 1
        Me.cbTuesday.Text = "二"
        Me.cbTuesday.UseVisualStyleBackColor = True
        '
        'cbMonday
        '
        Me.cbMonday.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbMonday.AutoSize = True
        Me.cbMonday.CheckAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.cbMonday.Location = New System.Drawing.Point(6, 3)
        Me.cbMonday.Name = "cbMonday"
        Me.cbMonday.Size = New System.Drawing.Size(21, 30)
        Me.cbMonday.TabIndex = 0
        Me.cbMonday.Text = "一"
        Me.cbMonday.UseVisualStyleBackColor = True
        '
        'dtpPeriodDateFrom
        '
        Me.dtpPeriodDateFrom.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpPeriodDateFrom.Location = New System.Drawing.Point(73, 3)
        Me.dtpPeriodDateFrom.Name = "dtpPeriodDateFrom"
        Me.dtpPeriodDateFrom.Size = New System.Drawing.Size(188, 22)
        Me.dtpPeriodDateFrom.TabIndex = 0
        '
        'dtpPeriodDateTo
        '
        Me.dtpPeriodDateTo.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpPeriodDateTo.Location = New System.Drawing.Point(73, 29)
        Me.dtpPeriodDateTo.Name = "dtpPeriodDateTo"
        Me.dtpPeriodDateTo.Size = New System.Drawing.Size(188, 22)
        Me.dtpPeriodDateTo.TabIndex = 1
        '
        'dtpPeriodTimeFrom
        '
        Me.dtpPeriodTimeFrom.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpPeriodTimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpPeriodTimeFrom.Location = New System.Drawing.Point(73, 91)
        Me.dtpPeriodTimeFrom.Name = "dtpPeriodTimeFrom"
        Me.dtpPeriodTimeFrom.ShowUpDown = True
        Me.dtpPeriodTimeFrom.Size = New System.Drawing.Size(188, 22)
        Me.dtpPeriodTimeFrom.TabIndex = 2
        Me.dtpPeriodTimeFrom.Value = New Date(2010, 5, 28, 7, 0, 0, 0)
        '
        'dtpPeriodTimeTo
        '
        Me.dtpPeriodTimeTo.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpPeriodTimeTo.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpPeriodTimeTo.Location = New System.Drawing.Point(73, 117)
        Me.dtpPeriodTimeTo.Name = "dtpPeriodTimeTo"
        Me.dtpPeriodTimeTo.ShowUpDown = True
        Me.dtpPeriodTimeTo.Size = New System.Drawing.Size(188, 22)
        Me.dtpPeriodTimeTo.TabIndex = 3
        Me.dtpPeriodTimeTo.Value = New Date(2010, 5, 28, 9, 0, 0, 0)
        '
        'tpDate
        '
        Me.tpDate.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(120, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.tpDate.Controls.Add(Me.TableLayoutPanel3)
        Me.tpDate.Location = New System.Drawing.Point(4, 22)
        Me.tpDate.Name = "tpDate"
        Me.tpDate.Padding = New System.Windows.Forms.Padding(3)
        Me.tpDate.Size = New System.Drawing.Size(270, 154)
        Me.tpDate.TabIndex = 1
        Me.tpDate.Text = "日期新增"
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(210, Byte), Integer), CType(CType(150, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Label14, 0, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label15, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.dtpSingleTimeFrom, 1, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.dtpSingleTimeTo, 1, 2)
        Me.TableLayoutPanel3.Controls.Add(Me.Label10, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.dtpSingleDate, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 4
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(264, 148)
        Me.TableLayoutPanel3.TabIndex = 2
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(14, 59)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(53, 12)
        Me.Label14.TabIndex = 1
        Me.Label14.Text = "時間迄："
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(14, 33)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 12)
        Me.Label15.TabIndex = 1
        Me.Label15.Text = "時間起："
        '
        'dtpSingleTimeFrom
        '
        Me.dtpSingleTimeFrom.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpSingleTimeFrom.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpSingleTimeFrom.Location = New System.Drawing.Point(73, 29)
        Me.dtpSingleTimeFrom.Name = "dtpSingleTimeFrom"
        Me.dtpSingleTimeFrom.ShowUpDown = True
        Me.dtpSingleTimeFrom.Size = New System.Drawing.Size(188, 22)
        Me.dtpSingleTimeFrom.TabIndex = 1
        Me.dtpSingleTimeFrom.Value = New Date(2010, 5, 28, 19, 0, 0, 0)
        '
        'dtpSingleTimeTo
        '
        Me.dtpSingleTimeTo.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpSingleTimeTo.Format = System.Windows.Forms.DateTimePickerFormat.Time
        Me.dtpSingleTimeTo.Location = New System.Drawing.Point(73, 55)
        Me.dtpSingleTimeTo.Name = "dtpSingleTimeTo"
        Me.dtpSingleTimeTo.ShowUpDown = True
        Me.dtpSingleTimeTo.Size = New System.Drawing.Size(188, 22)
        Me.dtpSingleTimeTo.TabIndex = 2
        Me.dtpSingleTimeTo.Value = New Date(2010, 5, 28, 21, 0, 0, 0)
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(14, 7)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(53, 12)
        Me.Label10.TabIndex = 1
        Me.Label10.Text = "時間起："
        '
        'dtpSingleDate
        '
        Me.dtpSingleDate.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.dtpSingleDate.Location = New System.Drawing.Point(73, 3)
        Me.dtpSingleDate.Name = "dtpSingleDate"
        Me.dtpSingleDate.Size = New System.Drawing.Size(188, 22)
        Me.dtpSingleDate.TabIndex = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnResetBooking)
        Me.Panel2.Controls.Add(Me.cbAllowMix)
        Me.Panel2.Controls.Add(Me.btnAddBooking)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 180)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(278, 32)
        Me.Panel2.TabIndex = 0
        '
        'btnResetBooking
        '
        Me.btnResetBooking.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnResetBooking.Location = New System.Drawing.Point(215, 3)
        Me.btnResetBooking.Name = "btnResetBooking"
        Me.btnResetBooking.Size = New System.Drawing.Size(60, 26)
        Me.btnResetBooking.TabIndex = 2
        Me.btnResetBooking.Text = "重來"
        Me.btnResetBooking.UseVisualStyleBackColor = True
        '
        'cbAllowMix
        '
        Me.cbAllowMix.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbAllowMix.AutoSize = True
        Me.cbAllowMix.ForeColor = System.Drawing.Color.Maroon
        Me.cbAllowMix.Location = New System.Drawing.Point(4, 9)
        Me.cbAllowMix.Name = "cbAllowMix"
        Me.cbAllowMix.Size = New System.Drawing.Size(113, 16)
        Me.cbAllowMix.TabIndex = 0
        Me.cbAllowMix.Text = "教室可混班上課?"
        Me.ttInfo.SetToolTip(Me.cbAllowMix, "允許混班上課時，" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "登記教室時段重覆也不會顯示為紅色。" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "學生登記座位時，" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "不同班級也可登記於同一教室裏。")
        Me.cbAllowMix.UseVisualStyleBackColor = True
        '
        'btnAddBooking
        '
        Me.btnAddBooking.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddBooking.Location = New System.Drawing.Point(147, 3)
        Me.btnAddBooking.Name = "btnAddBooking"
        Me.btnAddBooking.Size = New System.Drawing.Size(60, 26)
        Me.btnAddBooking.TabIndex = 1
        Me.btnAddBooking.Text = "新增"
        Me.btnAddBooking.UseVisualStyleBackColor = True
        '
        'SplitContainer5
        '
        Me.SplitContainer5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer5.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer5.Name = "SplitContainer5"
        Me.SplitContainer5.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer5.Panel1
        '
        Me.SplitContainer5.Panel1.Controls.Add(Me.GroupBox5)
        '
        'SplitContainer5.Panel2
        '
        Me.SplitContainer5.Panel2.Controls.Add(Me.GroupBox6)
        Me.SplitContainer5.Size = New System.Drawing.Size(415, 441)
        Me.SplitContainer5.SplitterDistance = 214
        Me.SplitContainer5.TabIndex = 0
        '
        'GroupBox5
        '
        Me.GroupBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox5.Controls.Add(Me.dgvClassBooking)
        Me.GroupBox5.Controls.Add(Me.Panel5)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox5.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(415, 214)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "登記教室"
        '
        'dgvClassBooking
        '
        Me.dgvClassBooking.AllowUserToAddRows = False
        Me.dgvClassBooking.AllowUserToDeleteRows = False
        Me.dgvClassBooking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClassBooking.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colBookingID, Me.colBookingClassroomID, Me.colIsPrimaryClassroom, Me.colClassName, Me.colCourse, Me.colClassroomName, Me.colDatePeriod, Me.colWeekdays, Me.colTimePeriod, Me.colAllowMix})
        Me.dgvClassBooking.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvClassBooking.Location = New System.Drawing.Point(3, 18)
        Me.dgvClassBooking.MultiSelect = False
        Me.dgvClassBooking.Name = "dgvClassBooking"
        Me.dgvClassBooking.ReadOnly = True
        Me.dgvClassBooking.RowTemplate.Height = 24
        Me.dgvClassBooking.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassBooking.Size = New System.Drawing.Size(409, 161)
        Me.dgvClassBooking.TabIndex = 0
        '
        'colBookingID
        '
        Me.colBookingID.DataPropertyName = "ID"
        Me.colBookingID.HeaderText = "ID"
        Me.colBookingID.Name = "colBookingID"
        Me.colBookingID.ReadOnly = True
        Me.colBookingID.Visible = False
        '
        'colBookingClassroomID
        '
        Me.colBookingClassroomID.DataPropertyName = "ClassroomID"
        Me.colBookingClassroomID.HeaderText = "ClassroomID"
        Me.colBookingClassroomID.Name = "colBookingClassroomID"
        Me.colBookingClassroomID.ReadOnly = True
        Me.colBookingClassroomID.Visible = False
        '
        'colIsPrimaryClassroom
        '
        Me.colIsPrimaryClassroom.DataPropertyName = "IsPrimaryClassroom"
        Me.colIsPrimaryClassroom.HeaderText = "主要"
        Me.colIsPrimaryClassroom.Name = "colIsPrimaryClassroom"
        Me.colIsPrimaryClassroom.ReadOnly = True
        Me.colIsPrimaryClassroom.Width = 60
        '
        'colClassName
        '
        Me.colClassName.DataPropertyName = "ClassName"
        Me.colClassName.HeaderText = "班級名稱"
        Me.colClassName.Name = "colClassName"
        Me.colClassName.ReadOnly = True
        '
        'colCourse
        '
        Me.colCourse.DataPropertyName = "CourseName"
        Me.colCourse.HeaderText = "課程名稱"
        Me.colCourse.Name = "colCourse"
        Me.colCourse.ReadOnly = True
        '
        'colClassroomName
        '
        Me.colClassroomName.DataPropertyName = "ClassroomName"
        Me.colClassroomName.HeaderText = "教室名稱"
        Me.colClassroomName.Name = "colClassroomName"
        Me.colClassroomName.ReadOnly = True
        '
        'colDatePeriod
        '
        Me.colDatePeriod.DataPropertyName = "DatePeriod"
        Me.colDatePeriod.HeaderText = "日期"
        Me.colDatePeriod.Name = "colDatePeriod"
        Me.colDatePeriod.ReadOnly = True
        '
        'colWeekdays
        '
        Me.colWeekdays.DataPropertyName = "WeekdayNames"
        Me.colWeekdays.HeaderText = "星期"
        Me.colWeekdays.Name = "colWeekdays"
        Me.colWeekdays.ReadOnly = True
        '
        'colTimePeriod
        '
        Me.colTimePeriod.DataPropertyName = "TimePeriod"
        Me.colTimePeriod.HeaderText = "時間"
        Me.colTimePeriod.Name = "colTimePeriod"
        Me.colTimePeriod.ReadOnly = True
        '
        'colAllowMix
        '
        Me.colAllowMix.DataPropertyName = "AllowMix"
        Me.colAllowMix.HeaderText = "混班"
        Me.colAllowMix.Name = "colAllowMix"
        Me.colAllowMix.ReadOnly = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.btnIsPrimaryClassroom)
        Me.Panel5.Controls.Add(Me.cbShowAllClassroomDetail)
        Me.Panel5.Controls.Add(Me.cbShowAllDetail)
        Me.Panel5.Controls.Add(Me.btnRemoveBooking)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(3, 179)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(409, 32)
        Me.Panel5.TabIndex = 1
        '
        'btnIsPrimaryClassroom
        '
        Me.btnIsPrimaryClassroom.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnIsPrimaryClassroom.Location = New System.Drawing.Point(309, 3)
        Me.btnIsPrimaryClassroom.Name = "btnIsPrimaryClassroom"
        Me.btnIsPrimaryClassroom.Size = New System.Drawing.Size(44, 26)
        Me.btnIsPrimaryClassroom.TabIndex = 2
        Me.btnIsPrimaryClassroom.Text = "主要"
        Me.btnIsPrimaryClassroom.UseVisualStyleBackColor = True
        '
        'cbShowAllClassroomDetail
        '
        Me.cbShowAllClassroomDetail.AutoSize = True
        Me.cbShowAllClassroomDetail.Location = New System.Drawing.Point(153, 9)
        Me.cbShowAllClassroomDetail.Name = "cbShowAllClassroomDetail"
        Me.cbShowAllClassroomDetail.Size = New System.Drawing.Size(156, 16)
        Me.cbShowAllClassroomDetail.TabIndex = 1
        Me.cbShowAllClassroomDetail.Text = "顯示本教室所有登記明細"
        Me.cbShowAllClassroomDetail.UseVisualStyleBackColor = True
        '
        'cbShowAllDetail
        '
        Me.cbShowAllDetail.AutoSize = True
        Me.cbShowAllDetail.Location = New System.Drawing.Point(3, 9)
        Me.cbShowAllDetail.Name = "cbShowAllDetail"
        Me.cbShowAllDetail.Size = New System.Drawing.Size(144, 16)
        Me.cbShowAllDetail.TabIndex = 0
        Me.cbShowAllDetail.Text = "顯示本班所有登記明細"
        Me.cbShowAllDetail.UseVisualStyleBackColor = True
        '
        'btnRemoveBooking
        '
        Me.btnRemoveBooking.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveBooking.Location = New System.Drawing.Point(359, 3)
        Me.btnRemoveBooking.Name = "btnRemoveBooking"
        Me.btnRemoveBooking.Size = New System.Drawing.Size(44, 26)
        Me.btnRemoveBooking.TabIndex = 3
        Me.btnRemoveBooking.Text = "刪除"
        Me.btnRemoveBooking.UseVisualStyleBackColor = True
        '
        'GroupBox6
        '
        Me.GroupBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.GroupBox6.Controls.Add(Me.dgvClassBookingDetail)
        Me.GroupBox6.Controls.Add(Me.Panel3)
        Me.GroupBox6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox6.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(415, 223)
        Me.GroupBox6.TabIndex = 0
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "登記教室明細"
        '
        'dgvClassBookingDetail
        '
        Me.dgvClassBookingDetail.AllowUserToAddRows = False
        Me.dgvClassBookingDetail.AllowUserToDeleteRows = False
        Me.dgvClassBookingDetail.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClassBookingDetail.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colDetailID, Me.colDetailClassName, Me.colDetailCourse, Me.colDetailClassroomName, Me.colDetailDate, Me.colDetailTimePeriod, Me.colDetailAllowMix, Me.colDetailOverlaped})
        Me.dgvClassBookingDetail.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvClassBookingDetail.Location = New System.Drawing.Point(3, 18)
        Me.dgvClassBookingDetail.MultiSelect = False
        Me.dgvClassBookingDetail.Name = "dgvClassBookingDetail"
        Me.dgvClassBookingDetail.ReadOnly = True
        Me.dgvClassBookingDetail.RowTemplate.Height = 24
        Me.dgvClassBookingDetail.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassBookingDetail.Size = New System.Drawing.Size(409, 170)
        Me.dgvClassBookingDetail.TabIndex = 0
        '
        'colDetailID
        '
        Me.colDetailID.DataPropertyName = "ID"
        Me.colDetailID.HeaderText = "ID"
        Me.colDetailID.Name = "colDetailID"
        Me.colDetailID.ReadOnly = True
        Me.colDetailID.Visible = False
        '
        'colDetailClassName
        '
        Me.colDetailClassName.DataPropertyName = "ClassName"
        Me.colDetailClassName.HeaderText = "班級名稱"
        Me.colDetailClassName.Name = "colDetailClassName"
        Me.colDetailClassName.ReadOnly = True
        '
        'colDetailCourse
        '
        Me.colDetailCourse.DataPropertyName = "CourseName"
        Me.colDetailCourse.HeaderText = "課程名稱"
        Me.colDetailCourse.Name = "colDetailCourse"
        Me.colDetailCourse.ReadOnly = True
        '
        'colDetailClassroomName
        '
        Me.colDetailClassroomName.DataPropertyName = "ClassroomName"
        Me.colDetailClassroomName.HeaderText = "教室名稱"
        Me.colDetailClassroomName.Name = "colDetailClassroomName"
        Me.colDetailClassroomName.ReadOnly = True
        '
        'colDetailDate
        '
        Me.colDetailDate.DataPropertyName = "Date"
        Me.colDetailDate.HeaderText = "日期"
        Me.colDetailDate.Name = "colDetailDate"
        Me.colDetailDate.ReadOnly = True
        '
        'colDetailTimePeriod
        '
        Me.colDetailTimePeriod.DataPropertyName = "TimePeriod"
        Me.colDetailTimePeriod.HeaderText = "時間"
        Me.colDetailTimePeriod.Name = "colDetailTimePeriod"
        Me.colDetailTimePeriod.ReadOnly = True
        '
        'colDetailAllowMix
        '
        Me.colDetailAllowMix.DataPropertyName = "AllowMix"
        Me.colDetailAllowMix.HeaderText = "混班"
        Me.colDetailAllowMix.Name = "colDetailAllowMix"
        Me.colDetailAllowMix.ReadOnly = True
        '
        'colDetailOverlaped
        '
        Me.colDetailOverlaped.DataPropertyName = "DetailOverlaped"
        Me.colDetailOverlaped.HeaderText = "Overlaped"
        Me.colDetailOverlaped.Name = "colDetailOverlaped"
        Me.colDetailOverlaped.ReadOnly = True
        Me.colDetailOverlaped.Visible = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(195, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.Panel3.Controls.Add(Me.Label11)
        Me.Panel3.Controls.Add(Me.cbShowOverlap)
        Me.Panel3.Controls.Add(Me.btnRemoveBookingDetail)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 188)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(409, 32)
        Me.Panel3.TabIndex = 0
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.Label11.ForeColor = System.Drawing.Color.Red
        Me.Label11.Location = New System.Drawing.Point(153, 10)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(101, 12)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "紅色字為時段重覆"
        '
        'cbShowOverlap
        '
        Me.cbShowOverlap.AutoSize = True
        Me.cbShowOverlap.Location = New System.Drawing.Point(3, 9)
        Me.cbShowOverlap.Name = "cbShowOverlap"
        Me.cbShowOverlap.Size = New System.Drawing.Size(144, 16)
        Me.cbShowOverlap.TabIndex = 0
        Me.cbShowOverlap.Text = "顯示時段重覆其他班級"
        Me.cbShowOverlap.UseVisualStyleBackColor = True
        '
        'btnRemoveBookingDetail
        '
        Me.btnRemoveBookingDetail.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemoveBookingDetail.Location = New System.Drawing.Point(346, 3)
        Me.btnRemoveBookingDetail.Name = "btnRemoveBookingDetail"
        Me.btnRemoveBookingDetail.Size = New System.Drawing.Size(60, 26)
        Me.btnRemoveBookingDetail.TabIndex = 1
        Me.btnRemoveBookingDetail.Text = "刪除"
        Me.btnRemoveBookingDetail.UseVisualStyleBackColor = True
        '
        'ttInfo
        '
        Me.ttInfo.IsBalloon = True
        Me.ttInfo.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ttInfo.ToolTipTitle = "說明"
        '
        'frmClass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(146, Byte), Integer), CType(CType(182, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(950, 450)
        Me.Name = "frmClass"
        Me.Text = "編輯班級"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.gbBooking.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.ResumeLayout(False)
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel2.ResumeLayout(False)
        Me.SplitContainer4.ResumeLayout(False)
        Me.SplitContainer6.Panel1.ResumeLayout(False)
        Me.SplitContainer6.Panel2.ResumeLayout(False)
        Me.SplitContainer6.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvClassroomList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox7.ResumeLayout(False)
        CType(Me.dgvCourseList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tcBooking.ResumeLayout(False)
        Me.tpPeriod.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.tpDate.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.SplitContainer5.Panel1.ResumeLayout(False)
        Me.SplitContainer5.Panel2.ResumeLayout(False)
        Me.SplitContainer5.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvClassBooking, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.GroupBox6.ResumeLayout(False)
        CType(Me.dgvClassBookingDetail, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents gbBooking As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents cbClassType As System.Windows.Forms.ComboBox
    Friend WithEvents tbListPrice As System.Windows.Forms.TextBox
    Friend WithEvents tbAccountsReceivable As System.Windows.Forms.TextBox
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents tcBooking As System.Windows.Forms.TabControl
    Friend WithEvents tpPeriod As System.Windows.Forms.TabPage
    Friend WithEvents tpDate As System.Windows.Forms.TabPage
    Friend WithEvents dgvClassroomList As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnResetBooking As System.Windows.Forms.Button
    Friend WithEvents btnAddBooking As System.Windows.Forms.Button
    Friend WithEvents SplitContainer5 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents btnRemoveBookingDetail As System.Windows.Forms.Button
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
    Friend WithEvents cbShowOverlap As System.Windows.Forms.CheckBox
    Friend WithEvents dgvClassBooking As System.Windows.Forms.DataGridView
    Friend WithEvents dgvClassBookingDetail As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents cbSunday As System.Windows.Forms.CheckBox
    Friend WithEvents cbWednesday As System.Windows.Forms.CheckBox
    Friend WithEvents cbSaturday As System.Windows.Forms.CheckBox
    Friend WithEvents cbFriday As System.Windows.Forms.CheckBox
    Friend WithEvents cbThursday As System.Windows.Forms.CheckBox
    Friend WithEvents cbTuesday As System.Windows.Forms.CheckBox
    Friend WithEvents cbMonday As System.Windows.Forms.CheckBox
    Friend WithEvents dtpPeriodDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriodDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriodTimeFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpPeriodTimeTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents dtpSingleTimeFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpSingleTimeTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents dtpSingleDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnRemoveBooking As System.Windows.Forms.Button
    Friend WithEvents cbAllowMix As System.Windows.Forms.CheckBox
    Friend WithEvents ttInfo As System.Windows.Forms.ToolTip
    Friend WithEvents cbShowAllClassroomDetail As System.Windows.Forms.CheckBox
    Friend WithEvents cbShowAllDetail As System.Windows.Forms.CheckBox
    Friend WithEvents SplitContainer6 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvCourseList As System.Windows.Forms.DataGridView
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents dtpDateFrom As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents dtpDateTo As System.Windows.Forms.DateTimePicker
    Friend WithEvents cbBookSeat As System.Windows.Forms.CheckBox
    Friend WithEvents btnIsPrimaryClassroom As System.Windows.Forms.Button
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colListPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountsReceivable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassroomID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassroomListName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colColumns As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRows As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSeats As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBookingID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colBookingClassroomID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colIsPrimaryClassroom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCourse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassroomName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDatePeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colWeekdays As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colTimePeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAllowMix As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailClassName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailCourse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailClassroomName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailTimePeriod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailAllowMix As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDetailOverlaped As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCourseID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCourseTeacher As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCourseName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colCourseDescription As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
