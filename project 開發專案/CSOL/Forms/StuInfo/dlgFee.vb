﻿Imports System.Windows.Forms

Public Class dlgFee
    Private m_StuID As Integer = -1
    Private m_ClassID As Integer = -1
    Private m_FeeID As Integer = -1

    Public Sub New(ByVal StuID As Integer, ByVal ClassID As Integer)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_StuID = StuID
        Me.m_ClassID = ClassID
    End Sub

    Public Sub New(ByVal StuID As Integer, ByVal ClassID As Integer, ByVal FeeID As Integer)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_StuID = StuID
        Me.m_ClassID = ClassID
        Me.m_FeeID = FeeID
    End Sub

    Private Sub dlgFee_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        InitComboBox()

        If Me.m_FeeID > 0 Then
            GetItem()
        Else
            GetNextReceiptNo()
        End If

        tbAmount.Select()
    End Sub

#Region "ComboBox Control"
    Private Sub InitComboBox()
        With cbType
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("FeePayType")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbMethod
            .ValueMember = "ID"
            .DisplayMember = "Value"
        End With
    End Sub

    Private Sub cbType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbType.SelectedIndexChanged
        Dim dt As DataTable = CSOL.Data.GetNameValuePairs(String.Format("FeePayType-{0}", cbType.Text))
        If dt.Rows.Count = 0 Then
            dt = CSOL.Data.GetNameValuePairs("FeePayMethod")
        End If

        With cbMethod
            .DataSource = dt
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With
    End Sub

    Private Sub cbMethod_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbMethod.SelectedIndexChanged
        pCheque.Hide()
        pCreditCard.Hide()
        pRemittance.Hide()

        If cbMethod.Text = "支票" Then
            pCheque.Show()
        ElseIf cbMethod.Text = "信用卡" Then
            pCreditCard.Show()
        ElseIf cbMethod.Text = "電匯" Then
            pRemittance.Show()
        End If
    End Sub
#End Region

    Private Sub GetNextReceiptNo()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetNextReceiptNo", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        tbReceipt.Text = ResponseParams("Receipt")
    End Sub

    Private Sub GetItem()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_FeeID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetFeeItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Item"))
        If dt.Rows.Count > 0 Then
            Dim row As DataRow = dt.Rows(0)
            tbReceipt.Text = row("Receipt")
            tbAmount.Text = Integer.Parse(row("Amount")).ToString("#,0")
            cbType.Text = row("PayType")
            cbMethod.Text = row("PayMethod")
            Dim Extras() As String = row("Extra").ToString().Split(" ")
            For Each Extra As String In Extras
                Dim pairs() As String = Extra.Split("：")
                If pairs.Length = 2 Then
                    Select Case pairs(0)
                        Case "分行別"
                            tbBank.Text = pairs(1)
                        Case "帳號"
                            tbBankAccount.Text = pairs(1)
                        Case "支票號碼"
                            tbChequeNumber.Text = pairs(1)
                        Case "到期日"
                            dtpChequeValidDate.Value = pairs(1)
                        Case "授權碼"
                            tbCreditCardExtra.Text = pairs(1)
                        Case "匯款帳號"
                            tbRemittanceExtra.Text = pairs(1)
                        Case "額外資訊"
                            tbExtra.Text = pairs(1)
                    End Select
                End If
            Next
            tbComment.Text = row("Comment")
        End If
    End Sub

    Private Sub tbAmount_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbAmount.Enter
        Dim tb As TextBox = sender
        If tb.Text.Trim() = "" Then
            tb.Text = ""
            Exit Sub
        End If

        Dim value As Integer = 0
        Try
            value = Integer.Parse(tb.Text, System.Globalization.NumberStyles.Any)
        Catch ex As Exception
            System.Diagnostics.Debug.WriteLine(ex.ToString())
        End Try
        tb.Text = IIf(value = 0, "", value)
    End Sub

    Private Sub tbAmount_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbAmount.Leave
        Dim tb As TextBox = sender
        Dim value As Integer = 0
        If Integer.TryParse(tb.Text, value) Then
            tb.Text = value.ToString("#,0")
        End If
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        If SaveItem() Then
            Me.Close()
        End If
    End Sub

    Private Function SaveItem() As Boolean
        If tbReceipt.Text.Trim() = "" Then
            MessageBox.Show("收據編號不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbReceipt.Select()
            Return False
        End If

        If tbAmount.Text.Trim() = "" Then
            MessageBox.Show("金額不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbAmount.Select()
            Return False
        End If

        Dim Amount As Integer = 0
        Try
            Amount = Integer.Parse(tbAmount.Text, Globalization.NumberStyles.Any)
        Catch ex As Exception
            MessageBox.Show("金額要填數字...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbAmount.Select()
            Return False
        End Try

        If IsReceiptNoExists() Then
            If MessageBox.Show("收據編號重覆了，要繼續嗎??", "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                tbReceipt.Select()
                tbReceipt.SelectAll()
                Return False
            End If
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_FeeID)
        RequestParams.Add("StuID", Me.m_StuID)
        RequestParams.Add("ClassID", Me.m_ClassID)
        RequestParams.Add("Receipt", tbReceipt.Text)
        RequestParams.Add("Amount", Amount)
        RequestParams.Add("FeeType", cbType.Text)
        RequestParams.Add("PayMethod", cbMethod.Text)

        Dim Extra As String = ""
        Select Case cbMethod.Text
            Case "支票"
                Extra = String.Format("分行別：{0} 帳號：{1} 支票號碼：{2} 到期日：{3} 額外資訊：{4}", _
                                      tbBank.Text.Replace(" ", ""), tbBankAccount.Text.Replace(" ", ""), _
                                      tbChequeNumber.Text.Replace(" ", ""), dtpChequeValidDate.Value.ToString("yyyy-MM-dd"), _
                                      tbExtra.Text.Replace(" ", ""))
            Case "信用卡"
                Extra = String.Format("授權碼：{0} 額外資訊：{1}", tbCreditCardExtra.Text.Replace(" ", ""), tbExtra.Text.Replace(" ", ""))
            Case "電匯"
                Extra = String.Format("匯款帳號：{0} 額外資訊：{1}", tbRemittanceExtra.Text.Replace(" ", ""), tbExtra.Text.Replace(" ", ""))
            Case Else
                Extra = String.Format("額外資訊：{0}", tbExtra.Text.Replace(" ", ""))
        End Select
        RequestParams.Add("Extra", Extra)
        RequestParams.Add("Comment", tbComment.Text)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "SaveFeeItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            If Me.m_FeeID > 0 And cbType.Text = "繳費" Then
                If MessageBox.Show("儲存成功，是否要列印收據??", "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    PrintReceipt()
                End If
            Else
                MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
            Return True
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            Return False
        End If
    End Function

    Private Function IsReceiptNoExists() As Boolean
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_FeeID)
        RequestParams.Add("Receipt", tbReceipt.Text)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "IsReceiptNoExists", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        Return ResponseParams("IsReceiptNoExists")
    End Function

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub

    Private Sub PrintReceipt()
        Dim ID As Integer = Me.m_FeeID
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", ID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetReceiptInfo", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim ReceiptInfo As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("ReceiptInfo"))

        Dim ClassAndSeat As String = String.Format("{0} {1}{2}", ReceiptInfo.Rows(0)("ClassName"), CSOL.Convert.GetAlphabet(ReceiptInfo.Rows(0)("X")), CSOL.Convert.GetNumeric(ReceiptInfo.Rows(0)("Y")))
        CSOL.Report.Student.Receipt.DirectPrint(ReceiptInfo.Rows(0)("Receipt"), ReceiptInfo.Rows(0)("DateFrom"), ReceiptInfo.Rows(0)("DateTo"), ReceiptInfo.Rows(0)("StuName"), ReceiptInfo.Rows(0)("Amount"), ReceiptInfo.Rows(0)("Extra"), ClassAndSeat)
    End Sub
End Class
