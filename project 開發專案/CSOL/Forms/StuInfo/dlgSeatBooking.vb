﻿Public Class dlgSeatBooking
    Private m_StuID As Integer = -1
    Private m_ClassID As Integer = -1
    Public Sub New(ByVal StuID As Integer, ByVal ClassID As Integer)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_StuID = StuID
        Me.m_ClassID = ClassID
    End Sub

    Private Sub dlgSeatBooking_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetClassroomList()
        GetStuClassList()
    End Sub

    Private m_ClassroomID As Integer = -1
    Private m_ClassroomList As DataTable

    Private m_Columns As Integer = 0
    Private m_Rows As Integer = 0
    Private m_LanesX As New List(Of Integer)
    Private m_LanesY As New List(Of Integer)
    Private m_Disables As New List(Of Point)

    Private m_LaneColor As Color = Color.Gray
    Private m_DisablesColor As Color = Color.Green
    Private m_BookedColor As Color = Color.GreenYellow
    Private m_ChosenColor As Color = Color.Tomato

    Private Sub GetClassroomList()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ClassID", Me.m_ClassID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetClassroomList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_ClassroomList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        cbClassroom.DataSource = Me.m_ClassroomList
        If cbClassroom.Items.Count > 0 Then
            cbClassroom.SelectedIndex = 0
        End If
    End Sub

    Private m_StuClassList As DataTable
    Private Sub GetStuClassList()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ClassID", Me.m_ClassID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetStuClassList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_StuClassList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
    End Sub

    Private Sub cbClassroom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbClassroom.SelectedIndexChanged
        Me.m_ClassroomID = cbClassroom.SelectedValue
        Dim row As DataRow = cbClassroom.SelectedItem.Row

        Me.m_Columns = row("Columns")
        Me.m_Rows = row("Rows")
        Me.m_LanesX.Clear()
        Me.m_LanesY.Clear()
        Me.m_Disables.Clear()

        Dim LanesPair()() As String = CSOL.Convert.PowerSplit(row("Lanes"))
        If LanesPair.Length = 2 Then
            If LanesPair(0)(0) <> "" Then
                Me.m_LanesX.AddRange(Array.ConvertAll(Of String, Integer)(LanesPair(0), AddressOf Integer.Parse))
            End If
            If LanesPair(1)(0) <> "" Then
                Me.m_LanesY.AddRange(Array.ConvertAll(Of String, Integer)(LanesPair(1), AddressOf Integer.Parse))
            End If
        End If
        Dim pts() As Point = CSOL.Convert.StringToPoints(row("Disables").ToString())
        Me.m_Disables.AddRange(pts)

        DrawSeatGrid()

        GetSeatBookingList()
    End Sub

    Private m_SeatBookingList As DataTable
    Private Sub GetSeatBookingList()
        If Me.m_ClassID > 0 And Me.m_ClassroomID > 0 Then
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("ClassID", Me.m_ClassID)
            RequestParams.Add("ClassroomID", Me.m_ClassroomID)

            Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetSeatBookingList", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
            Me.m_SeatBookingList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))

            MarkSeat()
        End If
    End Sub

    Private Sub DrawSeatGrid()
        Dim columns As Integer = Me.m_Columns
        Dim rows As Integer = Me.m_Rows

        dgvSeat.Rows.Clear()
        dgvSeat.Columns.Clear()

        If columns = 0 Or rows = 0 Then
            Exit Sub
        End If

        dgvSeat.RowCount = rows
        dgvSeat.ColumnCount = columns

        Dim seatsize As Integer = Math.Floor((dgvSeat.Width - dgvSeat.RowHeadersWidth) / (columns + Me.m_LanesX.Count / 2 + 1))
        If seatsize < 20 Then
            seatsize = 20
        End If

        For i As Integer = 0 To rows - 1
            dgvSeat.Rows(i).HeaderCell.Value = (i + 1).ToString()
            dgvSeat.Rows(i).HeaderCell.Tag = i
            dgvSeat.Rows(i).Height = seatsize
        Next

        For i As Integer = 0 To columns - 1
            dgvSeat.Columns(i).HeaderText = CSOL.Convert.GetAlphabet(i)
            dgvSeat.Columns(i).HeaderCell.Tag = i
            dgvSeat.Columns(i).Width = seatsize
        Next

        Dim RemovedDisableSeats As New List(Of Point)
        For Each p As Point In Me.m_Disables
            If p.X < columns And p.Y < rows Then
                dgvSeat.Rows(p.Y).Cells(p.X).Style.BackColor = Me.m_DisablesColor
            Else
                RemovedDisableSeats.Add(p)
            End If
        Next

        For Each p As Point In RemovedDisableSeats
            Me.m_Disables.Remove(p)
        Next

        Me.m_LanesX.Sort()
        Me.m_LanesX.Reverse()
        For Each x As Integer In Me.m_LanesX
            Dim column As New DataGridViewColumn()
            column.CellTemplate = dgvSeat.Rows(1).Cells(1).Clone()
            column.CellTemplate.Style.BackColor = Me.m_LaneColor
            column.Width = seatsize / 2

            dgvSeat.Columns.Insert(x, column)
        Next

        Me.m_LanesY.Sort()
        Me.m_LanesY.Reverse()
        For Each y As Integer In Me.m_LanesY
            Dim row As New DataGridViewRow()
            row.Height = seatsize / 2
            row.DefaultCellStyle.BackColor = Me.m_LaneColor

            dgvSeat.Rows.Insert(y, row)
        Next
    End Sub

    Private m_OriginalChosenSeat As New Point(-1, -1)
    Private Sub MarkSeat()
        For Each row As DataRow In Me.m_SeatBookingList.Rows
            Dim x As Integer = row("X")
            Dim y As Integer = row("Y")

            If x > 0 And y > 0 Then
                Dim pt As Point = FindColumnRow(x, y)
                If Me.m_StuID = row("StuID") Then
                    dgvSeat.Item(pt.X, pt.Y).Tag = "這是此學生所劃的位置"
                    dgvSeat.Item(pt.X, pt.Y).Style.BackColor = Me.m_ChosenColor
                    Me.m_OriginalChosenSeat = New Point(x, y)
                    Me.m_ChosenSeat = New Point(x, y)
                Else
                    dgvSeat.Item(pt.X, pt.Y).Tag = String.Format("{0}的位置", row("StuName"))
                    dgvSeat.Item(pt.X, pt.Y).Style.BackColor = Me.m_BookedColor
                End If
            End If
        Next

        RefreshDisplay()
    End Sub

    Private Sub dgvSeat_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvSeat.MouseMove
        Dim ht As DataGridView.HitTestInfo = dgvSeat.HitTest(e.Location.X, e.Location.Y)
        If ht.ColumnIndex = -1 Or ht.RowIndex = -1 Then
            lblMessage.Text = ""
        Else
            Dim cell As DataGridViewCell = dgvSeat.Item(ht.ColumnIndex, ht.RowIndex)
            If cell.Style.BackColor = Me.m_LaneColor Then
                lblMessage.Text = "這是走道"
                lblMessage.ForeColor = Me.m_LaneColor
            ElseIf dgvSeat.Rows(cell.RowIndex).DefaultCellStyle.BackColor = Me.m_LaneColor Then
                lblMessage.Text = "這是走道"
                lblMessage.ForeColor = Me.m_LaneColor
            ElseIf cell.Style.BackColor = Me.m_DisablesColor Then
                lblMessage.Text = "這個位置不能坐"
                lblMessage.ForeColor = Me.m_DisablesColor
            ElseIf cell.Style.BackColor = Me.m_BookedColor Then
                lblMessage.Text = cell.Tag
                lblMessage.ForeColor = Me.m_BookedColor
            ElseIf cell.Style.BackColor = Me.m_ChosenColor Then
                lblMessage.Text = cell.Tag
                lblMessage.ForeColor = Me.m_ChosenColor
            Else
                lblMessage.Text = "這個位置可以選"
                lblMessage.ForeColor = System.Drawing.SystemColors.ControlText
            End If

            dgvSeat.CurrentCell = cell
        End If
    End Sub

    Private Sub dgvSeat_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvSeat.SizeChanged
        DrawSeatGrid()
    End Sub

    Private m_ChosenSeat As New Point(-1, -1)
    Private Sub dgvSeat_CellMouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellMouseEventArgs) Handles dgvSeat.CellMouseClick
        Dim cell As DataGridViewCell = dgvSeat.CurrentCell
        If cell.Style.BackColor = Color.Empty Then
            Dim pt As Point = FindXY(cell.ColumnIndex, cell.RowIndex)
            If pt.X > -1 And pt.Y > -1 Then
                ClearChosen()

                Me.m_ChosenSeat = pt
                cell.Style.BackColor = Me.m_ChosenColor
                cell.Tag = "這是此學生所劃的位置"
            End If
        ElseIf cell.Style.BackColor = Me.m_ChosenColor Then
            Me.m_ChosenSeat = New Point(-1, -1)
            cell.Style.BackColor = Color.Empty
            cell.Tag = Nothing
        End If

        RefreshDisplay()
    End Sub

    Private Sub dgvSeat_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvSeat.KeyDown
        If e.KeyCode = Keys.Return Or e.KeyCode = Keys.Space Then
            Dim cell As DataGridViewCell = dgvSeat.CurrentCell
            If cell.Style.BackColor = Color.Empty Then
                Dim pt As Point = FindXY(cell.ColumnIndex, cell.RowIndex)
                If pt.X > -1 And pt.Y > -1 Then
                    ClearChosen()

                    Me.m_ChosenSeat = pt
                    cell.Style.BackColor = Me.m_ChosenColor
                    cell.Tag = "這是此學生所劃的位置"
                End If
            ElseIf cell.Style.BackColor = Me.m_ChosenColor Then
                Me.m_ChosenSeat = New Point(-1, -1)
                cell.Style.BackColor = Color.Empty
                cell.Tag = Nothing
            End If

            RefreshDisplay()
        End If
    End Sub

    Private Function FindXY(ByVal columnIndex As Integer, ByVal rowIndex As Integer) As Point
        Dim x As Integer = -1
        Dim y As Integer = -1
        If columnIndex > -1 And columnIndex < dgvSeat.Columns.Count And rowIndex > -1 And rowIndex < dgvSeat.Rows.Count Then
            If dgvSeat.Columns(columnIndex).HeaderCell.Tag IsNot Nothing And dgvSeat.Rows(rowIndex).HeaderCell.Tag IsNot Nothing Then
                x = dgvSeat.Columns(columnIndex).HeaderCell.Tag
                y = dgvSeat.Rows(rowIndex).HeaderCell.Tag
            End If
        End If

        Return New Point(x, y)
    End Function

    Private Function FindColumnRow(ByVal x As Integer, ByVal y As Integer) As Point
        Dim columnIndex As Integer = -1
        Dim rowIndex As Integer = -1

        If x > -1 And y > -1 Then
            For i As Integer = x To dgvSeat.Columns.Count - 1
                If dgvSeat.Columns(i).HeaderCell.Tag = x Then
                    columnIndex = i
                    Exit For
                End If
            Next

            For i As Integer = y To dgvSeat.Rows.Count - 1
                If dgvSeat.Rows(i).HeaderCell.Tag = y Then
                    rowIndex = i
                    Exit For
                End If
            Next
        End If

        If columnIndex > -1 And rowIndex > -1 Then
            Return New Point(columnIndex, rowIndex)
        Else
            Return New Point(-1, -1)
        End If
    End Function

    Private Sub ClearChosen()
        Dim pt As Point = FindColumnRow(Me.m_ChosenSeat.X, Me.m_ChosenSeat.Y)
        If pt.X > -1 And pt.X < dgvSeat.Columns.Count And pt.Y > -1 And pt.Y < dgvSeat.Rows.Count Then
            Dim cell As DataGridViewCell = dgvSeat.Item(pt.X, pt.Y)
            cell.Style.BackColor = Color.Empty
            cell.Tag = Nothing
        End If
    End Sub

    Private Sub RefreshDisplay()
        If Me.m_ChosenSeat.X = -1 Or Me.m_ChosenSeat.Y = -1 Then
            lblChosen.Text = "此學生無座位"
        Else
            lblChosen.Text = String.Format("此學生的座位：{0}{1:d2}", CSOL.Convert.GetAlphabet(Me.m_ChosenSeat.X), CSOL.Convert.GetNumeric(Me.m_ChosenSeat.Y))
        End If
    End Sub

    Private Sub btnAccept_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAccept.Click
        SaveChange()
        Me.Close()
    End Sub

    Private Sub SaveChange()
        If Not Me.m_OriginalChosenSeat.Equals(Me.m_ChosenSeat) Then
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("StuID", Me.m_StuID)
            RequestParams.Add("ClassID", Me.m_ClassID)
            RequestParams.Add("ClassroomID", Me.m_ClassroomID)
            RequestParams.Add("X", Me.m_ChosenSeat.X)
            RequestParams.Add("Y", Me.m_ChosenSeat.Y)

            Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "ChangeSeat", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
            Dim Status As String = ResponseParams("Status")
            If Status = "OK" Then
                MessageBox.Show("座位變更成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("座位變更失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Me.Close()
    End Sub
End Class