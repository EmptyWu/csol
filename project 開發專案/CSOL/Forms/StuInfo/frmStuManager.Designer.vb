﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmStuManager
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmStuManager))
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle19 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle20 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle21 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle22 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle23 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle24 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbFilterID = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbFilterName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.tbFilterNo = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label14 = New System.Windows.Forms.Label
        Me.cbFilterClass = New System.Windows.Forms.ComboBox
        Me.tbFilterPhone = New System.Windows.Forms.TextBox
        Me.Label10 = New System.Windows.Forms.Label
        Me.tbFilterOthers = New System.Windows.Forms.TextBox
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colStuID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colPID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colPhone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblRowCount = New System.Windows.Forms.Label
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.btnFirst = New System.Windows.Forms.Button
        Me.btnPrev = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnLast = New System.Windows.Forms.Button
        Me.tcStudent = New System.Windows.Forms.TabControl
        Me.tpStudent = New System.Windows.Forms.TabPage
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.pbPicture = New System.Windows.Forms.PictureBox
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.Label18 = New System.Windows.Forms.Label
        Me.btnTakePicture = New System.Windows.Forms.Button
        Me.cbCamera = New System.Windows.Forms.ComboBox
        Me.btnRemovePicture = New System.Windows.Forms.Button
        Me.btnImportPicture = New System.Windows.Forms.Button
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.Label8 = New System.Windows.Forms.Label
        Me.Label16 = New System.Windows.Forms.Label
        Me.Label15 = New System.Windows.Forms.Label
        Me.tbNo = New System.Windows.Forms.TextBox
        Me.cbSex = New System.Windows.Forms.ComboBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.tbName = New System.Windows.Forms.TextBox
        Me.Label6 = New System.Windows.Forms.Label
        Me.tbID = New System.Windows.Forms.TextBox
        Me.dtpBirth = New System.Windows.Forms.DateTimePicker
        Me.Label17 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbPhone = New System.Windows.Forms.TextBox
        Me.Label13 = New System.Windows.Forms.Label
        Me.tbMobile = New System.Windows.Forms.TextBox
        Me.Label20 = New System.Windows.Forms.Label
        Me.cbAddress = New System.Windows.Forms.ComboBox
        Me.cbZip = New System.Windows.Forms.ComboBox
        Me.Label21 = New System.Windows.Forms.Label
        Me.tbSchClass = New System.Windows.Forms.TextBox
        Me.Label12 = New System.Windows.Forms.Label
        Me.cbSchool = New System.Windows.Forms.ComboBox
        Me.Label19 = New System.Windows.Forms.Label
        Me.cbStuType = New System.Windows.Forms.ComboBox
        Me.Label11 = New System.Windows.Forms.Label
        Me.Label9 = New System.Windows.Forms.Label
        Me.Label23 = New System.Windows.Forms.Label
        Me.cbSalesGroup = New System.Windows.Forms.ComboBox
        Me.cbSalesManager = New System.Windows.Forms.ComboBox
        Me.cbSales = New System.Windows.Forms.ComboBox
        Me.Label22 = New System.Windows.Forms.Label
        Me.tbComment = New System.Windows.Forms.TextBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.dgvContacts = New System.Windows.Forms.DataGridView
        Me.colContactsTitle = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colContactsName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colContactsPhone = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colContactsMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colContactsAddress = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tpClass = New System.Windows.Forms.TabPage
        Me.SplitContainer3 = New System.Windows.Forms.SplitContainer
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.dgvClassList = New System.Windows.Forms.DataGridView
        Me.colClassID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colListPrice = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colAccountsReceivable = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel
        Me.Label25 = New System.Windows.Forms.Label
        Me.cbClassSalesGroup = New System.Windows.Forms.ComboBox
        Me.Label26 = New System.Windows.Forms.Label
        Me.Label27 = New System.Windows.Forms.Label
        Me.cbClassSalesManager = New System.Windows.Forms.ComboBox
        Me.cbClassSales = New System.Windows.Forms.ComboBox
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.btnResetClass = New System.Windows.Forms.Button
        Me.btnAddClass = New System.Windows.Forms.Button
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.Label24 = New System.Windows.Forms.Label
        Me.cbClassType = New System.Windows.Forms.ComboBox
        Me.SplitContainer4 = New System.Windows.Forms.SplitContainer
        Me.GroupBox4 = New System.Windows.Forms.GroupBox
        Me.dgvStuClassList = New System.Windows.Forms.DataGridView
        Me.colClassBookingID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingAccountsReceivable = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colAfterDiscount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingPaid = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingMoneyBack = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colClassBookingFeeOwe = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.btnAddFee = New System.Windows.Forms.Button
        Me.btnLeaveClass = New System.Windows.Forms.Button
        Me.btnBookingSeat = New System.Windows.Forms.Button
        Me.GroupBox5 = New System.Windows.Forms.GroupBox
        Me.dgvFeeList = New System.Windows.Forms.DataGridView
        Me.colFeeID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeClassName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeReceipt = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeType = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeePayMethod = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeAmount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeExtra = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colFeeComment = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.btnDeleteFee = New System.Windows.Forms.Button
        Me.btnPrintReceipt = New System.Windows.Forms.Button
        Me.btnModifyFee = New System.Windows.Forms.Button
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.tcStudent.SuspendLayout()
        Me.tpStudent.SuspendLayout()
        Me.Panel3.SuspendLayout()
        CType(Me.pbPicture, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvContacts, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tpClass.SuspendLayout()
        Me.SplitContainer3.Panel1.SuspendLayout()
        Me.SplitContainer3.Panel2.SuspendLayout()
        Me.SplitContainer3.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvClassList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SplitContainer4.Panel1.SuspendLayout()
        Me.SplitContainer4.Panel2.SuspendLayout()
        Me.SplitContainer4.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        CType(Me.dgvStuClassList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel6.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgvFeeList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel7.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.tcStudent)
        Me.SplitContainer1.Size = New System.Drawing.Size(984, 462)
        Me.SplitContainer1.SplitterDistance = 258
        Me.SplitContainer1.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupBox2)
        Me.SplitContainer2.Size = New System.Drawing.Size(258, 462)
        Me.SplitContainer2.SplitterDistance = 199
        Me.SplitContainer2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(258, 199)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "尋找學生"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.tbFilterID, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbFilterName, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tbFilterNo, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label14, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.cbFilterClass, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.tbFilterPhone, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.tbFilterOthers, 1, 5)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 18)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 7
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(252, 178)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 59)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 12)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "身份證："
        '
        'tbFilterID
        '
        Me.tbFilterID.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbFilterID.Location = New System.Drawing.Point(86, 55)
        Me.tbFilterID.Name = "tbFilterID"
        Me.tbFilterID.Size = New System.Drawing.Size(163, 22)
        Me.tbFilterID.TabIndex = 2
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 33)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 12)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "姓名："
        '
        'tbFilterName
        '
        Me.tbFilterName.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbFilterName.Location = New System.Drawing.Point(86, 29)
        Me.tbFilterName.Name = "tbFilterName"
        Me.tbFilterName.Size = New System.Drawing.Size(163, 22)
        Me.tbFilterName.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 7)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "學號："
        '
        'tbFilterNo
        '
        Me.tbFilterNo.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbFilterNo.Location = New System.Drawing.Point(86, 3)
        Me.tbFilterNo.Name = "tbFilterNo"
        Me.tbFilterNo.Size = New System.Drawing.Size(163, 22)
        Me.tbFilterNo.TabIndex = 0
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 85)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "電話："
        '
        'Label14
        '
        Me.Label14.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(3, 111)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(41, 12)
        Me.Label14.TabIndex = 6
        Me.Label14.Text = "班級："
        '
        'cbFilterClass
        '
        Me.cbFilterClass.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbFilterClass.DisplayMember = "Name"
        Me.cbFilterClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbFilterClass.FormattingEnabled = True
        Me.cbFilterClass.Location = New System.Drawing.Point(86, 107)
        Me.cbFilterClass.Name = "cbFilterClass"
        Me.cbFilterClass.Size = New System.Drawing.Size(163, 20)
        Me.cbFilterClass.TabIndex = 4
        Me.cbFilterClass.ValueMember = "ID"
        '
        'tbFilterPhone
        '
        Me.tbFilterPhone.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.tbFilterPhone.Location = New System.Drawing.Point(86, 81)
        Me.tbFilterPhone.Name = "tbFilterPhone"
        Me.tbFilterPhone.Size = New System.Drawing.Size(163, 22)
        Me.tbFilterPhone.TabIndex = 3
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(3, 137)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 12)
        Me.Label10.TabIndex = 6
        Me.Label10.Text = "其他條件："
        '
        'tbFilterOthers
        '
        Me.tbFilterOthers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbFilterOthers.Location = New System.Drawing.Point(86, 133)
        Me.tbFilterOthers.Multiline = True
        Me.tbFilterOthers.Name = "tbFilterOthers"
        Me.TableLayoutPanel1.SetRowSpan(Me.tbFilterOthers, 2)
        Me.tbFilterOthers.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tbFilterOthers.Size = New System.Drawing.Size(163, 42)
        Me.tbFilterOthers.TabIndex = 5
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.dgvList)
        Me.GroupBox2.Controls.Add(Me.Panel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(258, 259)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "學生清單"
        '
        'dgvList
        '
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colStuID, Me.colNo, Me.colName, Me.colPID, Me.colPhone})
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(3, 18)
        Me.dgvList.MultiSelect = False
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowTemplate.Height = 24
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(252, 185)
        Me.dgvList.TabIndex = 0
        '
        'colID
        '
        Me.colID.DataPropertyName = "RowID"
        Me.colID.HeaderText = "序號"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Width = 60
        '
        'colStuID
        '
        Me.colStuID.DataPropertyName = "ID"
        Me.colStuID.HeaderText = "StuID"
        Me.colStuID.Name = "colStuID"
        Me.colStuID.ReadOnly = True
        Me.colStuID.Visible = False
        '
        'colNo
        '
        Me.colNo.DataPropertyName = "No"
        Me.colNo.HeaderText = "學號"
        Me.colNo.Name = "colNo"
        Me.colNo.ReadOnly = True
        Me.colNo.Width = 60
        '
        'colName
        '
        Me.colName.DataPropertyName = "Name"
        Me.colName.HeaderText = "姓名"
        Me.colName.Name = "colName"
        Me.colName.ReadOnly = True
        Me.colName.Width = 60
        '
        'colPID
        '
        Me.colPID.DataPropertyName = "PID"
        Me.colPID.HeaderText = "身份證"
        Me.colPID.Name = "colPID"
        Me.colPID.ReadOnly = True
        Me.colPID.Width = 75
        '
        'colPhone
        '
        Me.colPhone.DataPropertyName = "Phone"
        Me.colPhone.HeaderText = "電話"
        Me.colPhone.Name = "colPhone"
        Me.colPhone.ReadOnly = True
        Me.colPhone.Width = 60
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblRowCount)
        Me.Panel1.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(3, 203)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(252, 53)
        Me.Panel1.TabIndex = 0
        '
        'lblRowCount
        '
        Me.lblRowCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblRowCount.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.lblRowCount.Location = New System.Drawing.Point(0, 40)
        Me.lblRowCount.Name = "lblRowCount"
        Me.lblRowCount.Size = New System.Drawing.Size(249, 12)
        Me.lblRowCount.TabIndex = 7
        Me.lblRowCount.Text = "1/1，共0筆"
        Me.lblRowCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.btnFirst)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnPrev)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnLast)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(54, 1)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(144, 36)
        Me.FlowLayoutPanel1.TabIndex = 6
        '
        'btnFirst
        '
        Me.btnFirst.Image = CType(resources.GetObject("btnFirst.Image"), System.Drawing.Image)
        Me.btnFirst.Location = New System.Drawing.Point(0, 0)
        Me.btnFirst.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(36, 36)
        Me.btnFirst.TabIndex = 0
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Image = CType(resources.GetObject("btnPrev.Image"), System.Drawing.Image)
        Me.btnPrev.Location = New System.Drawing.Point(36, 0)
        Me.btnPrev.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(36, 36)
        Me.btnPrev.TabIndex = 1
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Image = CType(resources.GetObject("btnNext.Image"), System.Drawing.Image)
        Me.btnNext.Location = New System.Drawing.Point(72, 0)
        Me.btnNext.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(36, 36)
        Me.btnNext.TabIndex = 2
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Image = CType(resources.GetObject("btnLast.Image"), System.Drawing.Image)
        Me.btnLast.Location = New System.Drawing.Point(108, 0)
        Me.btnLast.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(36, 36)
        Me.btnLast.TabIndex = 3
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'tcStudent
        '
        Me.tcStudent.Controls.Add(Me.tpStudent)
        Me.tcStudent.Controls.Add(Me.tpClass)
        Me.tcStudent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tcStudent.Location = New System.Drawing.Point(0, 0)
        Me.tcStudent.Name = "tcStudent"
        Me.tcStudent.SelectedIndex = 0
        Me.tcStudent.Size = New System.Drawing.Size(722, 462)
        Me.tcStudent.TabIndex = 4
        '
        'tpStudent
        '
        Me.tpStudent.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpStudent.Controls.Add(Me.Panel3)
        Me.tpStudent.Location = New System.Drawing.Point(4, 22)
        Me.tpStudent.Name = "tpStudent"
        Me.tpStudent.Padding = New System.Windows.Forms.Padding(3)
        Me.tpStudent.Size = New System.Drawing.Size(714, 436)
        Me.tpStudent.TabIndex = 0
        Me.tpStudent.Text = "學生資料"
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.pbPicture)
        Me.Panel3.Controls.Add(Me.Panel4)
        Me.Panel3.Controls.Add(Me.TableLayoutPanel2)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(3, 3)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(708, 430)
        Me.Panel3.TabIndex = 3
        '
        'pbPicture
        '
        Me.pbPicture.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbPicture.Image = CType(resources.GetObject("pbPicture.Image"), System.Drawing.Image)
        Me.pbPicture.Location = New System.Drawing.Point(437, 0)
        Me.pbPicture.Name = "pbPicture"
        Me.pbPicture.Size = New System.Drawing.Size(271, 369)
        Me.pbPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.pbPicture.TabIndex = 0
        Me.pbPicture.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Label18)
        Me.Panel4.Controls.Add(Me.btnTakePicture)
        Me.Panel4.Controls.Add(Me.cbCamera)
        Me.Panel4.Controls.Add(Me.btnRemovePicture)
        Me.Panel4.Controls.Add(Me.btnImportPicture)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel4.Location = New System.Drawing.Point(437, 369)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(271, 61)
        Me.Panel4.TabIndex = 1
        '
        'Label18
        '
        Me.Label18.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(44, 10)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(77, 12)
        Me.Label18.TabIndex = 7
        Me.Label18.Text = "選擇攝影機："
        '
        'btnTakePicture
        '
        Me.btnTakePicture.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnTakePicture.Location = New System.Drawing.Point(46, 30)
        Me.btnTakePicture.Margin = New System.Windows.Forms.Padding(0)
        Me.btnTakePicture.Name = "btnTakePicture"
        Me.btnTakePicture.Size = New System.Drawing.Size(75, 23)
        Me.btnTakePicture.TabIndex = 1
        Me.btnTakePicture.Text = "照像"
        Me.btnTakePicture.UseVisualStyleBackColor = True
        '
        'cbCamera
        '
        Me.cbCamera.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbCamera.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbCamera.FormattingEnabled = True
        Me.cbCamera.Location = New System.Drawing.Point(121, 7)
        Me.cbCamera.Name = "cbCamera"
        Me.cbCamera.Size = New System.Drawing.Size(150, 20)
        Me.cbCamera.TabIndex = 0
        '
        'btnRemovePicture
        '
        Me.btnRemovePicture.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemovePicture.Location = New System.Drawing.Point(196, 30)
        Me.btnRemovePicture.Margin = New System.Windows.Forms.Padding(0)
        Me.btnRemovePicture.Name = "btnRemovePicture"
        Me.btnRemovePicture.Size = New System.Drawing.Size(75, 23)
        Me.btnRemovePicture.TabIndex = 3
        Me.btnRemovePicture.Text = "刪照片"
        Me.btnRemovePicture.UseVisualStyleBackColor = True
        '
        'btnImportPicture
        '
        Me.btnImportPicture.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnImportPicture.Location = New System.Drawing.Point(121, 30)
        Me.btnImportPicture.Margin = New System.Windows.Forms.Padding(0)
        Me.btnImportPicture.Name = "btnImportPicture"
        Me.btnImportPicture.Size = New System.Drawing.Size(75, 23)
        Me.btnImportPicture.TabIndex = 2
        Me.btnImportPicture.Text = "放照片"
        Me.btnImportPicture.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 8
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.Label8, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Label16, 4, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label15, 0, 8)
        Me.TableLayoutPanel2.Controls.Add(Me.tbNo, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.cbSex, 5, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label7, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.tbName, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 4, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.tbID, 5, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.dtpBirth, 5, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label17, 4, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label5, 0, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.tbPhone, 1, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.Label13, 0, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.tbMobile, 1, 6)
        Me.TableLayoutPanel2.Controls.Add(Me.Label20, 0, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.cbAddress, 2, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.cbZip, 1, 7)
        Me.TableLayoutPanel2.Controls.Add(Me.Label21, 0, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.tbSchClass, 1, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label12, 0, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.cbSchool, 1, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label19, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.cbStuType, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.Label11, 4, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.Label9, 4, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.Label23, 4, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.cbSalesGroup, 5, 3)
        Me.TableLayoutPanel2.Controls.Add(Me.cbSalesManager, 5, 4)
        Me.TableLayoutPanel2.Controls.Add(Me.cbSales, 5, 5)
        Me.TableLayoutPanel2.Controls.Add(Me.Label22, 0, 10)
        Me.TableLayoutPanel2.Controls.Add(Me.tbComment, 1, 10)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel2, 0, 12)
        Me.TableLayoutPanel2.Controls.Add(Me.dgvContacts, 1, 8)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 13
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(437, 430)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(21, 7)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(41, 12)
        Me.Label8.TabIndex = 0
        Me.Label8.Text = "學號："
        '
        'Label16
        '
        Me.Label16.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(239, 33)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(41, 12)
        Me.Label16.TabIndex = 6
        Me.Label16.Text = "姓別："
        '
        'Label15
        '
        Me.Label15.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(9, 209)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(53, 24)
        Me.Label15.TabIndex = 0
        Me.Label15.Text = "其他" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "聯絡人："
        '
        'tbNo
        '
        Me.tbNo.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbNo, 2)
        Me.tbNo.Location = New System.Drawing.Point(68, 3)
        Me.tbNo.Name = "tbNo"
        Me.tbNo.Size = New System.Drawing.Size(124, 22)
        Me.tbNo.TabIndex = 0
        '
        'cbSex
        '
        Me.cbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSex.FormattingEnabled = True
        Me.cbSex.Location = New System.Drawing.Point(286, 29)
        Me.cbSex.Name = "cbSex"
        Me.cbSex.Size = New System.Drawing.Size(59, 20)
        Me.cbSex.TabIndex = 10
        '
        'Label7
        '
        Me.Label7.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(21, 33)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 2
        Me.Label7.Text = "姓名："
        '
        'tbName
        '
        Me.tbName.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbName, 2)
        Me.tbName.Location = New System.Drawing.Point(68, 29)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(124, 22)
        Me.tbName.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(227, 7)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 12)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "身份證："
        '
        'tbID
        '
        Me.tbID.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbID, 2)
        Me.tbID.Location = New System.Drawing.Point(286, 3)
        Me.tbID.Name = "tbID"
        Me.tbID.Size = New System.Drawing.Size(124, 22)
        Me.tbID.TabIndex = 9
        '
        'dtpBirth
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.dtpBirth, 2)
        Me.dtpBirth.Location = New System.Drawing.Point(286, 55)
        Me.dtpBirth.Name = "dtpBirth"
        Me.dtpBirth.Size = New System.Drawing.Size(124, 22)
        Me.dtpBirth.TabIndex = 11
        Me.dtpBirth.Value = New Date(1990, 1, 1, 0, 0, 0, 0)
        '
        'Label17
        '
        Me.Label17.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(239, 59)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(41, 12)
        Me.Label17.TabIndex = 6
        Me.Label17.Text = "生日："
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(21, 137)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(41, 12)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "電話："
        '
        'tbPhone
        '
        Me.tbPhone.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbPhone, 2)
        Me.tbPhone.Location = New System.Drawing.Point(68, 133)
        Me.tbPhone.Name = "tbPhone"
        Me.tbPhone.Size = New System.Drawing.Size(124, 22)
        Me.tbPhone.TabIndex = 5
        '
        'Label13
        '
        Me.Label13.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(21, 163)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(41, 12)
        Me.Label13.TabIndex = 6
        Me.Label13.Text = "手機："
        '
        'tbMobile
        '
        Me.tbMobile.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbMobile, 2)
        Me.tbMobile.Location = New System.Drawing.Point(68, 159)
        Me.tbMobile.Name = "tbMobile"
        Me.tbMobile.Size = New System.Drawing.Size(124, 22)
        Me.tbMobile.TabIndex = 6
        '
        'Label20
        '
        Me.Label20.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(21, 189)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(41, 12)
        Me.Label20.TabIndex = 0
        Me.Label20.Text = "地址："
        '
        'cbAddress
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbAddress, 6)
        Me.cbAddress.FormattingEnabled = True
        Me.cbAddress.Location = New System.Drawing.Point(133, 185)
        Me.cbAddress.Name = "cbAddress"
        Me.cbAddress.Size = New System.Drawing.Size(301, 20)
        Me.cbAddress.TabIndex = 8
        '
        'cbZip
        '
        Me.cbZip.FormattingEnabled = True
        Me.cbZip.Location = New System.Drawing.Point(68, 185)
        Me.cbZip.Name = "cbZip"
        Me.cbZip.Size = New System.Drawing.Size(59, 20)
        Me.cbZip.TabIndex = 7
        '
        'Label21
        '
        Me.Label21.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(21, 111)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(41, 12)
        Me.Label21.TabIndex = 9
        Me.Label21.Text = "班級："
        '
        'tbSchClass
        '
        Me.tbSchClass.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbSchClass, 2)
        Me.tbSchClass.Location = New System.Drawing.Point(68, 107)
        Me.tbSchClass.Name = "tbSchClass"
        Me.tbSchClass.Size = New System.Drawing.Size(124, 22)
        Me.tbSchClass.TabIndex = 4
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(21, 85)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(41, 12)
        Me.Label12.TabIndex = 0
        Me.Label12.Text = "學校："
        '
        'cbSchool
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbSchool, 2)
        Me.cbSchool.FormattingEnabled = True
        Me.cbSchool.Location = New System.Drawing.Point(68, 81)
        Me.cbSchool.Name = "cbSchool"
        Me.cbSchool.Size = New System.Drawing.Size(124, 20)
        Me.cbSchool.TabIndex = 3
        '
        'Label19
        '
        Me.Label19.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(21, 59)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(41, 12)
        Me.Label19.TabIndex = 6
        Me.Label19.Text = "類別："
        '
        'cbStuType
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbStuType, 2)
        Me.cbStuType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbStuType.FormattingEnabled = True
        Me.cbStuType.Location = New System.Drawing.Point(68, 55)
        Me.cbStuType.Name = "cbStuType"
        Me.cbStuType.Size = New System.Drawing.Size(124, 20)
        Me.cbStuType.TabIndex = 2
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(227, 85)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(53, 12)
        Me.Label11.TabIndex = 0
        Me.Label11.Text = "招生組："
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(239, 111)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(41, 12)
        Me.Label9.TabIndex = 0
        Me.Label9.Text = "組長："
        '
        'Label23
        '
        Me.Label23.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(239, 137)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(41, 12)
        Me.Label23.TabIndex = 0
        Me.Label23.Text = "組員："
        '
        'cbSalesGroup
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbSalesGroup, 3)
        Me.cbSalesGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSalesGroup.FormattingEnabled = True
        Me.cbSalesGroup.Location = New System.Drawing.Point(286, 81)
        Me.cbSalesGroup.Name = "cbSalesGroup"
        Me.cbSalesGroup.Size = New System.Drawing.Size(148, 20)
        Me.cbSalesGroup.TabIndex = 12
        '
        'cbSalesManager
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbSalesManager, 3)
        Me.cbSalesManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSalesManager.FormattingEnabled = True
        Me.cbSalesManager.Location = New System.Drawing.Point(286, 107)
        Me.cbSalesManager.Name = "cbSalesManager"
        Me.cbSalesManager.Size = New System.Drawing.Size(148, 20)
        Me.cbSalesManager.TabIndex = 13
        '
        'cbSales
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.cbSales, 3)
        Me.cbSales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbSales.FormattingEnabled = True
        Me.cbSales.Location = New System.Drawing.Point(286, 133)
        Me.cbSales.Name = "cbSales"
        Me.cbSales.Size = New System.Drawing.Size(148, 20)
        Me.cbSales.TabIndex = 14
        '
        'Label22
        '
        Me.Label22.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(21, 311)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(41, 12)
        Me.Label22.TabIndex = 0
        Me.Label22.Text = "備註："
        '
        'tbComment
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.tbComment, 7)
        Me.tbComment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbComment.Location = New System.Drawing.Point(68, 307)
        Me.tbComment.Multiline = True
        Me.tbComment.Name = "tbComment"
        Me.TableLayoutPanel2.SetRowSpan(Me.tbComment, 2)
        Me.tbComment.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tbComment.Size = New System.Drawing.Size(366, 90)
        Me.tbComment.TabIndex = 16
        '
        'Panel2
        '
        Me.TableLayoutPanel2.SetColumnSpan(Me.Panel2, 8)
        Me.Panel2.Controls.Add(Me.btnRemove)
        Me.Panel2.Controls.Add(Me.btnReset)
        Me.Panel2.Controls.Add(Me.btnAdd)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Location = New System.Drawing.Point(3, 403)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(431, 24)
        Me.Panel2.TabIndex = 17
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(356, 0)
        Me.btnRemove.Margin = New System.Windows.Forms.Padding(0)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(75, 23)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Text = "刪除"
        Me.btnRemove.UseVisualStyleBackColor = True
        Me.btnRemove.Visible = False
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(152, 0)
        Me.btnReset.Margin = New System.Windows.Forms.Padding(0)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(75, 23)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "重來"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(2, 0)
        Me.btnAdd.Margin = New System.Windows.Forms.Padding(0)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(75, 23)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "新增"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(77, 0)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 23)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "修改"
        Me.btnSave.UseVisualStyleBackColor = True
        Me.btnSave.Visible = False
        '
        'dgvContacts
        '
        Me.dgvContacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvContacts.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colContactsTitle, Me.colContactsName, Me.colContactsPhone, Me.colContactsMobile, Me.colContactsAddress})
        Me.TableLayoutPanel2.SetColumnSpan(Me.dgvContacts, 7)
        Me.dgvContacts.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvContacts.Location = New System.Drawing.Point(68, 211)
        Me.dgvContacts.Name = "dgvContacts"
        Me.TableLayoutPanel2.SetRowSpan(Me.dgvContacts, 2)
        Me.dgvContacts.RowTemplate.Height = 24
        Me.dgvContacts.Size = New System.Drawing.Size(366, 90)
        Me.dgvContacts.TabIndex = 15
        '
        'colContactsTitle
        '
        Me.colContactsTitle.DataPropertyName = "稱謂"
        Me.colContactsTitle.HeaderText = "稱謂"
        Me.colContactsTitle.Name = "colContactsTitle"
        '
        'colContactsName
        '
        Me.colContactsName.DataPropertyName = "姓名"
        Me.colContactsName.HeaderText = "姓名"
        Me.colContactsName.Name = "colContactsName"
        '
        'colContactsPhone
        '
        Me.colContactsPhone.DataPropertyName = "電話"
        Me.colContactsPhone.HeaderText = "電話"
        Me.colContactsPhone.Name = "colContactsPhone"
        '
        'colContactsMobile
        '
        Me.colContactsMobile.DataPropertyName = "手機"
        Me.colContactsMobile.HeaderText = "手機"
        Me.colContactsMobile.Name = "colContactsMobile"
        '
        'colContactsAddress
        '
        Me.colContactsAddress.DataPropertyName = "地址"
        Me.colContactsAddress.HeaderText = "地址"
        Me.colContactsAddress.Name = "colContactsAddress"
        '
        'tpClass
        '
        Me.tpClass.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.tpClass.Controls.Add(Me.SplitContainer3)
        Me.tpClass.Location = New System.Drawing.Point(4, 22)
        Me.tpClass.Name = "tpClass"
        Me.tpClass.Padding = New System.Windows.Forms.Padding(3)
        Me.tpClass.Size = New System.Drawing.Size(714, 436)
        Me.tpClass.TabIndex = 1
        Me.tpClass.Text = "報名班級及繳費情況"
        '
        'SplitContainer3
        '
        Me.SplitContainer3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer3.Location = New System.Drawing.Point(3, 3)
        Me.SplitContainer3.Name = "SplitContainer3"
        '
        'SplitContainer3.Panel1
        '
        Me.SplitContainer3.Panel1.Controls.Add(Me.GroupBox3)
        '
        'SplitContainer3.Panel2
        '
        Me.SplitContainer3.Panel2.Controls.Add(Me.SplitContainer4)
        Me.SplitContainer3.Size = New System.Drawing.Size(708, 430)
        Me.SplitContainer3.SplitterDistance = 262
        Me.SplitContainer3.TabIndex = 0
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.dgvClassList)
        Me.GroupBox3.Controls.Add(Me.TableLayoutPanel4)
        Me.GroupBox3.Controls.Add(Me.Panel5)
        Me.GroupBox3.Controls.Add(Me.TableLayoutPanel3)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(262, 430)
        Me.GroupBox3.TabIndex = 0
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "選擇班級"
        '
        'dgvClassList
        '
        Me.dgvClassList.AllowUserToAddRows = False
        Me.dgvClassList.AllowUserToDeleteRows = False
        Me.dgvClassList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClassList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colClassID, Me.colClassName, Me.colClassType, Me.colListPrice, Me.colAccountsReceivable, Me.colDate})
        Me.dgvClassList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvClassList.Location = New System.Drawing.Point(3, 44)
        Me.dgvClassList.MultiSelect = False
        Me.dgvClassList.Name = "dgvClassList"
        Me.dgvClassList.ReadOnly = True
        Me.dgvClassList.RowTemplate.Height = 24
        Me.dgvClassList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvClassList.Size = New System.Drawing.Size(256, 271)
        Me.dgvClassList.TabIndex = 2
        '
        'colClassID
        '
        Me.colClassID.DataPropertyName = "ID"
        Me.colClassID.HeaderText = "ID"
        Me.colClassID.Name = "colClassID"
        Me.colClassID.ReadOnly = True
        Me.colClassID.Visible = False
        '
        'colClassName
        '
        Me.colClassName.DataPropertyName = "Name"
        Me.colClassName.HeaderText = "名稱"
        Me.colClassName.Name = "colClassName"
        Me.colClassName.ReadOnly = True
        '
        'colClassType
        '
        Me.colClassType.DataPropertyName = "Type"
        Me.colClassType.HeaderText = "班別"
        Me.colClassType.Name = "colClassType"
        Me.colClassType.ReadOnly = True
        '
        'colListPrice
        '
        Me.colListPrice.DataPropertyName = "ListPrice"
        DataGridViewCellStyle17.Format = "$#,0"
        Me.colListPrice.DefaultCellStyle = DataGridViewCellStyle17
        Me.colListPrice.HeaderText = "約定金額"
        Me.colListPrice.Name = "colListPrice"
        Me.colListPrice.ReadOnly = True
        '
        'colAccountsReceivable
        '
        Me.colAccountsReceivable.DataPropertyName = "AccountsReceivable"
        DataGridViewCellStyle18.Format = "$#,0"
        Me.colAccountsReceivable.DefaultCellStyle = DataGridViewCellStyle18
        Me.colAccountsReceivable.HeaderText = "應收金額"
        Me.colAccountsReceivable.Name = "colAccountsReceivable"
        Me.colAccountsReceivable.ReadOnly = True
        '
        'colDate
        '
        Me.colDate.DataPropertyName = "Date"
        Me.colDate.HeaderText = "修業期間"
        Me.colDate.Name = "colDate"
        Me.colDate.ReadOnly = True
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.Label25, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.cbClassSalesGroup, 1, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.Label26, 0, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.Label27, 0, 2)
        Me.TableLayoutPanel4.Controls.Add(Me.cbClassSalesManager, 1, 1)
        Me.TableLayoutPanel4.Controls.Add(Me.cbClassSales, 1, 2)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 315)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 4
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(256, 83)
        Me.TableLayoutPanel4.TabIndex = 0
        '
        'Label25
        '
        Me.Label25.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(17, 7)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(65, 12)
        Me.Label25.TabIndex = 2
        Me.Label25.Text = "招生組別："
        '
        'cbClassSalesGroup
        '
        Me.cbClassSalesGroup.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbClassSalesGroup.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClassSalesGroup.FormattingEnabled = True
        Me.cbClassSalesGroup.Location = New System.Drawing.Point(88, 3)
        Me.cbClassSalesGroup.Name = "cbClassSalesGroup"
        Me.cbClassSalesGroup.Size = New System.Drawing.Size(165, 20)
        Me.cbClassSalesGroup.TabIndex = 0
        '
        'Label26
        '
        Me.Label26.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(41, 33)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(41, 12)
        Me.Label26.TabIndex = 2
        Me.Label26.Text = "組長："
        '
        'Label27
        '
        Me.Label27.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(41, 59)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(41, 12)
        Me.Label27.TabIndex = 2
        Me.Label27.Text = "組員："
        '
        'cbClassSalesManager
        '
        Me.cbClassSalesManager.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbClassSalesManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClassSalesManager.FormattingEnabled = True
        Me.cbClassSalesManager.Location = New System.Drawing.Point(88, 29)
        Me.cbClassSalesManager.Name = "cbClassSalesManager"
        Me.cbClassSalesManager.Size = New System.Drawing.Size(165, 20)
        Me.cbClassSalesManager.TabIndex = 1
        '
        'cbClassSales
        '
        Me.cbClassSales.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbClassSales.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClassSales.FormattingEnabled = True
        Me.cbClassSales.Location = New System.Drawing.Point(88, 55)
        Me.cbClassSales.Name = "cbClassSales"
        Me.cbClassSales.Size = New System.Drawing.Size(165, 20)
        Me.cbClassSales.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.btnResetClass)
        Me.Panel5.Controls.Add(Me.btnAddClass)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(3, 398)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(256, 29)
        Me.Panel5.TabIndex = 1
        '
        'btnResetClass
        '
        Me.btnResetClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnResetClass.Location = New System.Drawing.Point(131, 4)
        Me.btnResetClass.Name = "btnResetClass"
        Me.btnResetClass.Size = New System.Drawing.Size(75, 23)
        Me.btnResetClass.TabIndex = 1
        Me.btnResetClass.Text = "重來"
        Me.btnResetClass.UseVisualStyleBackColor = True
        '
        'btnAddClass
        '
        Me.btnAddClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddClass.Location = New System.Drawing.Point(50, 4)
        Me.btnAddClass.Name = "btnAddClass"
        Me.btnAddClass.Size = New System.Drawing.Size(75, 23)
        Me.btnAddClass.TabIndex = 0
        Me.btnAddClass.Text = "報名班級"
        Me.btnAddClass.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 2
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85.0!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Label24, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.cbClassType, 1, 0)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 18)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 2
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(256, 26)
        Me.TableLayoutPanel3.TabIndex = 3
        '
        'Label24
        '
        Me.Label24.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(41, 7)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(41, 12)
        Me.Label24.TabIndex = 2
        Me.Label24.Text = "班別："
        '
        'cbClassType
        '
        Me.cbClassType.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cbClassType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClassType.FormattingEnabled = True
        Me.cbClassType.Location = New System.Drawing.Point(88, 3)
        Me.cbClassType.Name = "cbClassType"
        Me.cbClassType.Size = New System.Drawing.Size(165, 20)
        Me.cbClassType.TabIndex = 4
        '
        'SplitContainer4
        '
        Me.SplitContainer4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer4.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer4.Name = "SplitContainer4"
        Me.SplitContainer4.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer4.Panel1
        '
        Me.SplitContainer4.Panel1.Controls.Add(Me.GroupBox4)
        '
        'SplitContainer4.Panel2
        '
        Me.SplitContainer4.Panel2.Controls.Add(Me.GroupBox5)
        Me.SplitContainer4.Size = New System.Drawing.Size(442, 430)
        Me.SplitContainer4.SplitterDistance = 202
        Me.SplitContainer4.TabIndex = 0
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.dgvStuClassList)
        Me.GroupBox4.Controls.Add(Me.Panel6)
        Me.GroupBox4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox4.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(442, 202)
        Me.GroupBox4.TabIndex = 0
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "已報名班級"
        '
        'dgvStuClassList
        '
        Me.dgvStuClassList.AllowUserToAddRows = False
        Me.dgvStuClassList.AllowUserToDeleteRows = False
        Me.dgvStuClassList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStuClassList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colClassBookingID, Me.Column1, Me.colClassBookingName, Me.colClassBookingType, Me.colClassBookingAccountsReceivable, Me.colAfterDiscount, Me.colClassBookingPaid, Me.colClassBookingMoneyBack, Me.colClassBookingFeeOwe})
        Me.dgvStuClassList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStuClassList.Location = New System.Drawing.Point(3, 18)
        Me.dgvStuClassList.Name = "dgvStuClassList"
        Me.dgvStuClassList.ReadOnly = True
        Me.dgvStuClassList.RowTemplate.Height = 24
        Me.dgvStuClassList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStuClassList.Size = New System.Drawing.Size(436, 153)
        Me.dgvStuClassList.TabIndex = 0
        '
        'colClassBookingID
        '
        Me.colClassBookingID.DataPropertyName = "ID"
        Me.colClassBookingID.HeaderText = "ID"
        Me.colClassBookingID.Name = "colClassBookingID"
        Me.colClassBookingID.ReadOnly = True
        Me.colClassBookingID.Visible = False
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ClassID"
        Me.Column1.HeaderText = "ClassID"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Visible = False
        '
        'colClassBookingName
        '
        Me.colClassBookingName.DataPropertyName = "ClassName"
        Me.colClassBookingName.HeaderText = "班級"
        Me.colClassBookingName.Name = "colClassBookingName"
        Me.colClassBookingName.ReadOnly = True
        '
        'colClassBookingType
        '
        Me.colClassBookingType.DataPropertyName = "ClassType"
        Me.colClassBookingType.HeaderText = "班別"
        Me.colClassBookingType.Name = "colClassBookingType"
        Me.colClassBookingType.ReadOnly = True
        '
        'colClassBookingAccountsReceivable
        '
        Me.colClassBookingAccountsReceivable.DataPropertyName = "AccountsReceivable"
        DataGridViewCellStyle19.Format = "$#,0"
        Me.colClassBookingAccountsReceivable.DefaultCellStyle = DataGridViewCellStyle19
        Me.colClassBookingAccountsReceivable.HeaderText = "應繳金額"
        Me.colClassBookingAccountsReceivable.Name = "colClassBookingAccountsReceivable"
        Me.colClassBookingAccountsReceivable.ReadOnly = True
        '
        'colAfterDiscount
        '
        Me.colAfterDiscount.DataPropertyName = "AfterDiscount"
        DataGridViewCellStyle20.Format = "$#,0"
        Me.colAfterDiscount.DefaultCellStyle = DataGridViewCellStyle20
        Me.colAfterDiscount.HeaderText = "折扣後"
        Me.colAfterDiscount.Name = "colAfterDiscount"
        Me.colAfterDiscount.ReadOnly = True
        '
        'colClassBookingPaid
        '
        Me.colClassBookingPaid.DataPropertyName = "Paid"
        DataGridViewCellStyle21.Format = "$#,0"
        Me.colClassBookingPaid.DefaultCellStyle = DataGridViewCellStyle21
        Me.colClassBookingPaid.HeaderText = "實繳"
        Me.colClassBookingPaid.Name = "colClassBookingPaid"
        Me.colClassBookingPaid.ReadOnly = True
        '
        'colClassBookingMoneyBack
        '
        Me.colClassBookingMoneyBack.DataPropertyName = "MoneyBack"
        DataGridViewCellStyle22.Format = "$#,0"
        Me.colClassBookingMoneyBack.DefaultCellStyle = DataGridViewCellStyle22
        Me.colClassBookingMoneyBack.HeaderText = "退費"
        Me.colClassBookingMoneyBack.Name = "colClassBookingMoneyBack"
        Me.colClassBookingMoneyBack.ReadOnly = True
        '
        'colClassBookingFeeOwe
        '
        Me.colClassBookingFeeOwe.DataPropertyName = "Owe"
        DataGridViewCellStyle23.Format = "$#,0"
        Me.colClassBookingFeeOwe.DefaultCellStyle = DataGridViewCellStyle23
        Me.colClassBookingFeeOwe.HeaderText = "尚欠"
        Me.colClassBookingFeeOwe.Name = "colClassBookingFeeOwe"
        Me.colClassBookingFeeOwe.ReadOnly = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.btnAddFee)
        Me.Panel6.Controls.Add(Me.btnLeaveClass)
        Me.Panel6.Controls.Add(Me.btnBookingSeat)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel6.Location = New System.Drawing.Point(3, 171)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(436, 28)
        Me.Panel6.TabIndex = 1
        '
        'btnAddFee
        '
        Me.btnAddFee.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAddFee.Location = New System.Drawing.Point(84, 2)
        Me.btnAddFee.Name = "btnAddFee"
        Me.btnAddFee.Size = New System.Drawing.Size(75, 23)
        Me.btnAddFee.TabIndex = 1
        Me.btnAddFee.Text = "繳費"
        Me.btnAddFee.UseVisualStyleBackColor = True
        '
        'btnLeaveClass
        '
        Me.btnLeaveClass.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnLeaveClass.Location = New System.Drawing.Point(358, 2)
        Me.btnLeaveClass.Name = "btnLeaveClass"
        Me.btnLeaveClass.Size = New System.Drawing.Size(75, 23)
        Me.btnLeaveClass.TabIndex = 2
        Me.btnLeaveClass.Text = "退班"
        Me.btnLeaveClass.UseVisualStyleBackColor = True
        '
        'btnBookingSeat
        '
        Me.btnBookingSeat.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnBookingSeat.Location = New System.Drawing.Point(3, 2)
        Me.btnBookingSeat.Name = "btnBookingSeat"
        Me.btnBookingSeat.Size = New System.Drawing.Size(75, 23)
        Me.btnBookingSeat.TabIndex = 0
        Me.btnBookingSeat.Text = "劃位"
        Me.btnBookingSeat.UseVisualStyleBackColor = True
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.dgvFeeList)
        Me.GroupBox5.Controls.Add(Me.Panel7)
        Me.GroupBox5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox5.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(442, 224)
        Me.GroupBox5.TabIndex = 0
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "繳費情形"
        '
        'dgvFeeList
        '
        Me.dgvFeeList.AllowUserToAddRows = False
        Me.dgvFeeList.AllowUserToDeleteRows = False
        Me.dgvFeeList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvFeeList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colFeeID, Me.colFeeClassName, Me.colFeeReceipt, Me.colFeeType, Me.colFeePayMethod, Me.colFeeAmount, Me.colFeeExtra, Me.colFeeComment})
        Me.dgvFeeList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvFeeList.Location = New System.Drawing.Point(3, 18)
        Me.dgvFeeList.Name = "dgvFeeList"
        Me.dgvFeeList.ReadOnly = True
        Me.dgvFeeList.RowTemplate.Height = 24
        Me.dgvFeeList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvFeeList.Size = New System.Drawing.Size(436, 175)
        Me.dgvFeeList.TabIndex = 0
        '
        'colFeeID
        '
        Me.colFeeID.DataPropertyName = "ID"
        Me.colFeeID.HeaderText = "ID"
        Me.colFeeID.Name = "colFeeID"
        Me.colFeeID.ReadOnly = True
        Me.colFeeID.Visible = False
        '
        'colFeeClassName
        '
        Me.colFeeClassName.DataPropertyName = "ClassName"
        Me.colFeeClassName.HeaderText = "班級"
        Me.colFeeClassName.Name = "colFeeClassName"
        Me.colFeeClassName.ReadOnly = True
        '
        'colFeeReceipt
        '
        Me.colFeeReceipt.DataPropertyName = "Receipt"
        Me.colFeeReceipt.HeaderText = "收據編號"
        Me.colFeeReceipt.Name = "colFeeReceipt"
        Me.colFeeReceipt.ReadOnly = True
        '
        'colFeeType
        '
        Me.colFeeType.DataPropertyName = "FeeType"
        Me.colFeeType.HeaderText = "費用類別"
        Me.colFeeType.Name = "colFeeType"
        Me.colFeeType.ReadOnly = True
        '
        'colFeePayMethod
        '
        Me.colFeePayMethod.DataPropertyName = "PayMethod"
        Me.colFeePayMethod.HeaderText = "付款方式"
        Me.colFeePayMethod.Name = "colFeePayMethod"
        Me.colFeePayMethod.ReadOnly = True
        '
        'colFeeAmount
        '
        Me.colFeeAmount.DataPropertyName = "Amount"
        DataGridViewCellStyle24.Format = "$#,0"
        Me.colFeeAmount.DefaultCellStyle = DataGridViewCellStyle24
        Me.colFeeAmount.HeaderText = "金額"
        Me.colFeeAmount.Name = "colFeeAmount"
        Me.colFeeAmount.ReadOnly = True
        '
        'colFeeExtra
        '
        Me.colFeeExtra.DataPropertyName = "Extra"
        Me.colFeeExtra.HeaderText = "其他"
        Me.colFeeExtra.Name = "colFeeExtra"
        Me.colFeeExtra.ReadOnly = True
        '
        'colFeeComment
        '
        Me.colFeeComment.DataPropertyName = "Comment"
        Me.colFeeComment.HeaderText = "附註"
        Me.colFeeComment.Name = "colFeeComment"
        Me.colFeeComment.ReadOnly = True
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.btnDeleteFee)
        Me.Panel7.Controls.Add(Me.btnPrintReceipt)
        Me.Panel7.Controls.Add(Me.btnModifyFee)
        Me.Panel7.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel7.Location = New System.Drawing.Point(3, 193)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(436, 28)
        Me.Panel7.TabIndex = 2
        '
        'btnDeleteFee
        '
        Me.btnDeleteFee.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnDeleteFee.Location = New System.Drawing.Point(84, 2)
        Me.btnDeleteFee.Name = "btnDeleteFee"
        Me.btnDeleteFee.Size = New System.Drawing.Size(75, 23)
        Me.btnDeleteFee.TabIndex = 1
        Me.btnDeleteFee.Text = "刪除"
        Me.btnDeleteFee.UseVisualStyleBackColor = True
        '
        'btnPrintReceipt
        '
        Me.btnPrintReceipt.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPrintReceipt.Location = New System.Drawing.Point(358, 2)
        Me.btnPrintReceipt.Name = "btnPrintReceipt"
        Me.btnPrintReceipt.Size = New System.Drawing.Size(75, 23)
        Me.btnPrintReceipt.TabIndex = 2
        Me.btnPrintReceipt.Text = "列印收據"
        Me.btnPrintReceipt.UseVisualStyleBackColor = True
        '
        'btnModifyFee
        '
        Me.btnModifyFee.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnModifyFee.Location = New System.Drawing.Point(3, 2)
        Me.btnModifyFee.Name = "btnModifyFee"
        Me.btnModifyFee.Size = New System.Drawing.Size(75, 23)
        Me.btnModifyFee.TabIndex = 0
        Me.btnModifyFee.Text = "修改"
        Me.btnModifyFee.UseVisualStyleBackColor = True
        '
        'frmStuManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(184, Byte), Integer), CType(CType(252, Byte), Integer), CType(CType(175, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(950, 450)
        Me.Name = "frmStuManager"
        Me.Text = "學生管理"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.tcStudent.ResumeLayout(False)
        Me.tpStudent.ResumeLayout(False)
        Me.Panel3.ResumeLayout(False)
        CType(Me.pbPicture, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvContacts, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tpClass.ResumeLayout(False)
        Me.SplitContainer3.Panel1.ResumeLayout(False)
        Me.SplitContainer3.Panel2.ResumeLayout(False)
        Me.SplitContainer3.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        CType(Me.dgvClassList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.TableLayoutPanel4.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.SplitContainer4.Panel1.ResumeLayout(False)
        Me.SplitContainer4.Panel2.ResumeLayout(False)
        Me.SplitContainer4.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        CType(Me.dgvStuClassList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel6.ResumeLayout(False)
        Me.GroupBox5.ResumeLayout(False)
        CType(Me.dgvFeeList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel7.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbFilterID As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbFilterName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbFilterNo As System.Windows.Forms.TextBox
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents lblRowCount As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cbFilterClass As System.Windows.Forms.ComboBox
    Friend WithEvents pbPicture As System.Windows.Forms.PictureBox
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents tbFilterPhone As System.Windows.Forms.TextBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents tbID As System.Windows.Forms.TextBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents tbNo As System.Windows.Forms.TextBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents tbPhone As System.Windows.Forms.TextBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents tbMobile As System.Windows.Forms.TextBox
    Friend WithEvents tbComment As System.Windows.Forms.TextBox
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents cbSex As System.Windows.Forms.ComboBox
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents cbStuType As System.Windows.Forms.ComboBox
    Friend WithEvents btnRemovePicture As System.Windows.Forms.Button
    Friend WithEvents btnTakePicture As System.Windows.Forms.Button
    Friend WithEvents btnImportPicture As System.Windows.Forms.Button
    Friend WithEvents cbCamera As System.Windows.Forms.ComboBox
    Friend WithEvents cbSalesGroup As System.Windows.Forms.ComboBox
    Friend WithEvents cbSalesManager As System.Windows.Forms.ComboBox
    Friend WithEvents cbSales As System.Windows.Forms.ComboBox
    Friend WithEvents cbZip As System.Windows.Forms.ComboBox
    Friend WithEvents cbAddress As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents tbFilterOthers As System.Windows.Forms.TextBox
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents Label19 As System.Windows.Forms.Label
    Friend WithEvents dtpBirth As System.Windows.Forms.DateTimePicker
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents cbSchool As System.Windows.Forms.ComboBox
    Friend WithEvents Label21 As System.Windows.Forms.Label
    Friend WithEvents tbSchClass As System.Windows.Forms.TextBox
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label23 As System.Windows.Forms.Label
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvContacts As System.Windows.Forms.DataGridView
    Friend WithEvents colContactsTitle As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContactsName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContactsPhone As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContactsMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colContactsAddress As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tcStudent As System.Windows.Forms.TabControl
    Friend WithEvents tpStudent As System.Windows.Forms.TabPage
    Friend WithEvents tpClass As System.Windows.Forms.TabPage
    Friend WithEvents SplitContainer3 As System.Windows.Forms.SplitContainer
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvClassList As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label24 As System.Windows.Forms.Label
    Friend WithEvents cbClassType As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label25 As System.Windows.Forms.Label
    Friend WithEvents cbClassSalesGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label26 As System.Windows.Forms.Label
    Friend WithEvents Label27 As System.Windows.Forms.Label
    Friend WithEvents cbClassSalesManager As System.Windows.Forms.ComboBox
    Friend WithEvents cbClassSales As System.Windows.Forms.ComboBox
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents btnAddClass As System.Windows.Forms.Button
    Friend WithEvents btnResetClass As System.Windows.Forms.Button
    Friend WithEvents SplitContainer4 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvStuClassList As System.Windows.Forms.DataGridView
    Friend WithEvents dgvFeeList As System.Windows.Forms.DataGridView
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents btnAddFee As System.Windows.Forms.Button
    Friend WithEvents btnLeaveClass As System.Windows.Forms.Button
    Friend WithEvents btnBookingSeat As System.Windows.Forms.Button
    Friend WithEvents colClassID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colListPrice As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAccountsReceivable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingAccountsReceivable As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colAfterDiscount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingPaid As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingMoneyBack As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colClassBookingFeeOwe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btnDeleteFee As System.Windows.Forms.Button
    Friend WithEvents btnPrintReceipt As System.Windows.Forms.Button
    Friend WithEvents btnModifyFee As System.Windows.Forms.Button
    Friend WithEvents colFeeID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeClassName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeReceipt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeType As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeePayMethod As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeAmount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeExtra As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colFeeComment As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colStuID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colPhone As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
