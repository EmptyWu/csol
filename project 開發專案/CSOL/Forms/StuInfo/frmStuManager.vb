﻿Imports System.Text.RegularExpressions

Public Class frmStuManager
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private WithEvents m_GlobalMDI As WinControl.WinControl = My.Application.FormController.TabMDI
    Private Sub MainMDI_ActiveFormChanged(ByVal sender As Object, ByVal e As EventArgs) Handles m_GlobalMDI.ActiveFormChanged
        If Me.Equals(Me.m_GlobalMDI.ActiveForm) Then
            If False = Me.m_CreateNew Then
                GetClassNames()
                GetList()
                InitComboBox()
                detectCamera()
            End If
        End If
    End Sub

    Private m_CreateNew As Boolean
    Public ReadOnly Property CreateNew() As Boolean
        Get
            Return Me.m_CreateNew
        End Get
    End Property

    Public Sub New()

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_CreateNew = False
    End Sub

    Public Sub New(ByVal CreateNew As Boolean)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_CreateNew = CreateNew
    End Sub

    Private Const c_Rows As Integer = 100
    Private m_CurrentPage As Integer = 1
    Private m_TotalPage As Integer = 1
    Private m_StuCount As Integer = 0

    Private m_StuID As Integer = -1

    Private m_ClassNames As DataTable
    Private m_Contacts As DataTable
    Private m_ClassList As DataTable
    Private m_StuClassList As DataTable
    Private m_StuFeeList As DataTable

    Private Sub frmStuManager_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvList.AutoGenerateColumns = False
        dgvClassList.AutoGenerateColumns = False
        dgvStuClassList.AutoGenerateColumns = False
        dgvFeeList.AutoGenerateColumns = False

        If Me.m_CreateNew Then
            Me.Text = "新增學生"
            SplitContainer1.Panel1Collapsed = True
            GetNextStuNo()
            tbName.Focus()
            Me.m_Contacts = GetContactsDataTable()
            dgvContacts.DataSource = Me.m_Contacts
            btnSave.Hide()
            btnRemove.Hide()
            tcStudent.TabPages.Remove(tpClass)
        Else
            GetClassNames()
            GetList()
            tbFilterNo.Focus()
            btnSave.Show()
            btnRemove.Show()
        End If

        InitComboBox()
        detectCamera()
    End Sub

    Private Sub GetNextStuNo()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", "GetNextStuNo", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        tbNo.Text = ResponseParams("StuNo")
    End Sub

    Private Sub InitComboBox()
        With cbSex
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("Sex")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbStuType
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("StuType")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbSalesGroup
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("SalesGroup")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbSalesManager
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("SalesManager")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbSales
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("Sales")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With
    End Sub

    Private Sub GetClassNames()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuInfo", "GetClassNames", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_ClassNames = CSOL.Convert.XmlStringToDataTable(ResponseParams("ClassNames"))

        Dim row As DataRow = Me.m_ClassNames.NewRow()
        row("ID") = -1
        row("Name") = "所有班級"
        Me.m_ClassNames.Rows.InsertAt(row, 0)

        With cbFilterClass
            .DataSource = Me.m_ClassNames
            .SelectedIndex = 0
        End With
    End Sub

    Private m_List As DataTable
    Private Sub GetList()
        Dim Filter As New System.Collections.Specialized.NameValueCollection()
        Filter.Add("學號", tbFilterNo.Text)
        Filter.Add("姓名", tbFilterName.Text)
        Filter.Add("身份證", tbFilterID.Text)
        Filter.Add("電話", tbFilterPhone.Text)
        If cbFilterClass.Text <> "所有班級" Then
            Filter.Add("班級", cbFilterClass.Text)
        End If

        Dim others() As String = tbFilterOthers.Text.Split(vbCrLf)
        For Each f As String In others
            If f.Split("=").Length = 2 Then
                Filter.Add(f.Split("=")(0), f.Split("=")(1))
            End If
        Next

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Rows", c_Rows)
        RequestParams.Add("Page", Me.m_CurrentPage)
        RequestParams.Add("Filter", CSOL.HTTPClient.GetQuery(Filter))

        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "List", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Me.m_StuCount = ResponseParams("StuCount")
        If Me.m_StuCount < 0 Then
            Me.m_StuCount = 0
        End If

        Me.m_TotalPage = Math.Ceiling(Me.m_StuCount / c_Rows)
        If Me.m_TotalPage = 0 Then
            Me.m_TotalPage = 1
        End If

        lblRowCount.Text = String.Format("{0} / {1}  共{2}人", Me.m_CurrentPage, Me.m_TotalPage, Me.m_StuCount)

        Me.m_List = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvList.DataSource = Me.m_List
    End Sub

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        If Me.m_CurrentPage > 1 Then
            Me.m_CurrentPage = 1
            GetList()
        End If
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        If Me.m_CurrentPage > 1 Then
            Me.m_CurrentPage -= 1
            GetList()
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If Me.m_CurrentPage < Me.m_TotalPage Then
            Me.m_CurrentPage += 1
            GetList()
        End If
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        If Me.m_CurrentPage < Me.m_TotalPage Then
            Me.m_CurrentPage = Me.m_TotalPage
            GetList()
        End If
    End Sub

#Region "Camera And Picture"
    Private Sub detectCamera()
        InitCameraComboBox(Nothing, Nothing)
        Dim CameraRefresher As New Timer()
        CameraRefresher.Interval = 5000
        AddHandler CameraRefresher.Tick, AddressOf InitCameraComboBox
        CameraRefresher.Start()
        cbCamera.Tag = CameraRefresher
    End Sub

    Private Sub btnTakePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTakePicture.Click
        Dim image As Bitmap = CSOL.DirectXCapture.Grap(cbCamera.SelectedItem.ToString())
        If image IsNot Nothing Then
            pbPicture.Image = image
        End If
    End Sub

    Private Sub cbCamera_Enter(ByVal sender As Object, ByVal e As EventArgs) Handles cbCamera.Enter
        Dim CameraRefresher As Timer = cbCamera.Tag
        CameraRefresher.Stop()
    End Sub

    Private Sub cbCamera_Leave(ByVal sender As Object, ByVal e As EventArgs) Handles cbCamera.Leave
        Dim CameraRefresher As Timer = cbCamera.Tag
        CameraRefresher.Start()
    End Sub

    Private Sub InitCameraComboBox(ByVal sender As Object, ByVal e As EventArgs)
        With cbCamera
            Dim item As Object = cbCamera.SelectedItem

            .Items.Clear()
            Dim cams As List(Of String) = CSOL.DirectXCapture.GetVideoDeviceNames()
            If cams.Count = 0 Then
                .Items.Add("[無]")
            End If

            For Each cam As String In cams
                .Items.Add(cam)
            Next

            If item Is Nothing Then
                .SelectedIndex = 0
            ElseIf .Items.IndexOf(item) = -1 Then
                .SelectedIndex = 0
            Else
                .SelectedItem = item
            End If
        End With
    End Sub

    Private Sub btnImportPicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImportPicture.Click
        Dim ofd As New OpenFileDialog()
        ofd.Filter = "所有圖片檔案|*.jpeg;*.jpg;*.bmp;*.gif;*.png;*.ico"
        ofd.Multiselect = False
        If ofd.ShowDialog() = Windows.Forms.DialogResult.OK Then
            pbPicture.ImageLocation = ofd.FileName
            pbPicture.Tag = 1
        End If
    End Sub

    Private Sub btnRemovePicture_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemovePicture.Click
        If pbPicture.Tag IsNot Nothing Then
            If MessageBox.Show("您確定要刪除照片??", "注意...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                pbPicture.Tag = Nothing
                With cbSex
                    If .Text IsNot Nothing Then
                        If .Text = "女" Then
                            pbPicture.ImageLocation = String.Format("{0}\Images\girl.ico", My.Application.Info.DirectoryPath)
                        Else
                            pbPicture.ImageLocation = String.Format("{0}\Images\boy.ico", My.Application.Info.DirectoryPath)
                        End If
                    End If
                End With
            End If
        End If
    End Sub

    Private Sub tbID_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbID.TextChanged
        With tbID
            If Regex.IsMatch(.Text, "^\w1") Then
                cbSex.Text = "男"
            ElseIf Regex.IsMatch(.Text, "^\w2") Then
                cbSex.Text = "女"
            End If
        End With
    End Sub

    Private Sub cbSex_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbSex.SelectedIndexChanged
        If pbPicture.Tag Is Nothing Then
            With cbSex
                If .Text IsNot Nothing Then
                    If .Text = "女" Then
                        pbPicture.ImageLocation = String.Format("{0}\Images\girl.ico", My.Application.Info.DirectoryPath)
                    Else
                        pbPicture.ImageLocation = String.Format("{0}\Images\boy.ico", My.Application.Info.DirectoryPath)
                    End If
                End If
            End With
        End If
    End Sub

    Private Sub GetStuPicture()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", Me.m_StuID)

        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "GetPicture", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Dim pic As String = ResponseParams("Picture")
        If pic = "" Then
            pbPicture.Tag = Nothing
            With cbSex
                If .Text IsNot Nothing Then
                    If .Text = "女" Then
                        pbPicture.ImageLocation = String.Format("{0}\Images\girl.ico", My.Application.Info.DirectoryPath)
                    Else
                        pbPicture.ImageLocation = String.Format("{0}\Images\boy.ico", My.Application.Info.DirectoryPath)
                    End If
                End If
            End With
        Else
            pbPicture.Image = CSOL.Convert.Base64StringToImage(pic)
            pbPicture.Tag = 1
        End If
    End Sub
#End Region

#Region "Fill Up Fields"
    Private Sub tbPhone_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbPhone.Enter
        With tbPhone
            If .Text = "" Then
                .Text = "02"
                .SelectionStart = .TextLength
            End If
        End With
    End Sub

    Private Sub tbMobile_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbMobile.Enter
        With tbMobile
            If .Text = "" Then
                .Text = "09"
                .SelectionStart = .TextLength
            End If
        End With
    End Sub
#End Region

    Private Sub tbFilter_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbFilterNo.TextChanged, tbFilterName.TextChanged, tbFilterID.TextChanged, tbFilterPhone.TextChanged, cbFilterClass.SelectedIndexChanged, tbFilterOthers.TextChanged
        GetList()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("姓名不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Focus()
            Return
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", -1)
        RequestParams.Add("No", tbNo.Text)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Type", cbStuType.Text)
        RequestParams.Add("School", String.Join(" ", New String() {cbSchool.Text, tbSchClass.Text}))
        RequestParams.Add("Phone", tbPhone.Text)
        RequestParams.Add("Mobile", tbMobile.Text)
        RequestParams.Add("Address", String.Join(" ", New String() {cbZip.Text, cbAddress.Text}))
        RequestParams.Add("Contacts", CSOL.Convert.DataTableToXmlString(Me.m_Contacts))
        RequestParams.Add("PID", tbID.Text)
        RequestParams.Add("Sex", cbSex.Text)
        RequestParams.Add("Birth", dtpBirth.Value.ToString("yyyy-MM-dd"))
        RequestParams.Add("SalesGroup", cbSalesGroup.Text)
        RequestParams.Add("SalesManager", cbSalesManager.Text)
        RequestParams.Add("Sales", cbSales.Text)
        RequestParams.Add("Comment", tbComment.Text)
        RequestParams.Add("Picture", CSOL.Convert.ImageToBase64String(ScaleImage(pbPicture.Image), System.Drawing.Imaging.ImageFormat.Png))

        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "Save", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("姓名不可以空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Focus()
            Return
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", Me.m_StuID)
        RequestParams.Add("No", tbNo.Text)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Type", cbStuType.Text)
        RequestParams.Add("School", String.Join(" ", New String() {cbSchool.Text, tbSchClass.Text}))
        RequestParams.Add("Phone", tbPhone.Text)
        RequestParams.Add("Mobile", tbMobile.Text)
        RequestParams.Add("Address", String.Join(" ", New String() {cbZip.Text, cbAddress.Text}))
        RequestParams.Add("Contacts", CSOL.Convert.DataTableToXmlString(Me.m_Contacts))
        RequestParams.Add("PID", tbID.Text)
        RequestParams.Add("Sex", cbSex.Text)
        RequestParams.Add("Birth", dtpBirth.Value.ToString("yyyy-MM-dd"))
        RequestParams.Add("SalesGroup", cbSalesGroup.Text)
        RequestParams.Add("SalesManager", cbSalesManager.Text)
        RequestParams.Add("Sales", cbSales.Text)
        RequestParams.Add("Comment", tbComment.Text)
        RequestParams.Add("Picture", CSOL.Convert.ImageToBase64String(ScaleImage(pbPicture.Image), System.Drawing.Imaging.ImageFormat.Png))

        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "Save", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        If Me.m_StuID = -1 Then
            ResetStuFields()
        Else
            GetStuDetail()
            GetStuPicture()
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If Me.m_StuID > 0 Then
            If MessageBox.Show("您確定要刪除這個學生??", "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("StuID", Me.m_StuID)

                Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "Remove", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
                Dim Status As String = ResponseParams("Status")
                If Status = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GetList()
                Else
                    MessageBox.Show("刪除失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private Function GetContactsDataTable() As DataTable
        Dim dt As New DataTable("Contacts")
        dt.Columns.Add("稱謂", GetType(String))
        dt.Columns.Add("姓名", GetType(String))
        dt.Columns.Add("電話", GetType(String))
        dt.Columns.Add("手機", GetType(String))
        dt.Columns.Add("地址", GetType(String))
        Return dt
    End Function

    Private Function TransformContacts(ByVal dt As DataTable) As String
        Dim sb As New System.Text.StringBuilder()
        For Each row As DataRow In dt.Rows
            Dim arr(row.ItemArray.Length - 1) As String
            For i As Integer = 0 To row.ItemArray.Length - 1
                arr(i) = String.Format("{0}:{1}", dt.Columns(i).ColumnName, row.ItemArray(i))
            Next
            sb.AppendLine(String.Join(" ", arr))
        Next
        Return sb.ToString()
    End Function

    Private Sub refreshCurrentContainer(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvList.SelectionChanged, tcStudent.SelectedIndexChanged
        If dgvList.SelectedRows.Count > 0 Then
            If dgvList.SelectedRows(0).IsNewRow Then
                Me.m_StuID = -1
            Else
                Me.m_StuID = dgvList.SelectedRows(0).Cells("colStuID").Value
            End If
        End If

        If tcStudent.SelectedTab.Equals(tpStudent) Then
            If Me.m_StuID = -1 Then
                ResetStuFields()
            Else
                GetStuDetail()
                GetStuPicture()
            End If
        ElseIf tcStudent.SelectedTab.Equals(tpClass) Then
            GetClassComboBox()
            If Me.m_StuID = -1 Then
                Me.m_ClassList.Rows.Clear()
                Me.m_StuClassList.Rows.Clear()
            Else
                GetClassList()
                GetStuClassList()
            End If
        End If
    End Sub

    Private Sub ResetStuFields()
        pbPicture.Tag = Nothing
        GetNextStuNo()
        tbName.ResetText()
        If cbStuType.Items.Count > 0 Then
            cbStuType.SelectedIndex = 0
        End If

        cbSchool.ResetText()
        tbSchClass.ResetText()
        tbPhone.ResetText()
        tbMobile.ResetText()
        cbZip.ResetText()
        cbAddress.ResetText()
        tbID.ResetText()

        If cbSex.Items.Count > 0 Then
            cbSex.SelectedIndex = 0
        End If
        dtpBirth.ResetText()

        If cbSalesGroup.Items.Count > 0 Then
            cbSalesGroup.SelectedIndex = 0
        End If
        If cbSalesManager.Items.Count > 0 Then
            cbSalesManager.SelectedIndex = 0
        End If
        If cbSales.Items.Count > 0 Then
            cbSales.SelectedIndex = 0
        End If

        Me.m_Contacts = GetContactsDataTable()
        dgvContacts.DataSource = Me.m_Contacts

        tbComment.ResetText()
        tbName.Focus()
    End Sub

    Private Sub GetStuDetail()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", Me.m_StuID)

        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "Detail", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)

        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Detail"))
        Dim row As DataRow = dt.Rows(0)

        tbNo.Text = row("No")
        tbName.Text = row("Name")
        cbStuType.Text = row("Type")

        Dim School() As String = row("School").ToString().Split(" ")
        cbSchool.Text = School(0)
        If School.Length > 1 Then
            tbSchClass.Text = String.Join(" ", School, 1, School.Length - 1)
        End If

        tbPhone.Text = row("Phone")
        tbMobile.Text = row("Mobile")

        Dim Address() As String = row("Address").ToString().Split(" ")
        If Address.Length = 1 Then
            cbAddress.Text = Address(0)
        Else
            cbZip.Text = Address(0)
            cbAddress.Text = String.Join(" ", Address, 1, Address.Length - 1)
        End If

        tbID.Text = row("PID")
        cbSex.Text = row("Sex")
        Date.TryParse(row("Birth"), dtpBirth.Value)
        cbSalesGroup.Text = row("SalesGroup")
        cbSalesManager.Text = row("SalesManager")
        cbSales.Text = row("Sales")

        If row("Contacts") = "" Then
            Me.m_Contacts = GetContactsDataTable()
        Else
            Me.m_Contacts = CSOL.Convert.XmlStringToDataTable(row("Contacts"))
        End If
        dgvContacts.DataSource = Me.m_Contacts

        tbComment.Text = row("Comment")
    End Sub

    Private Function ScaleImage(ByVal Image As System.Drawing.Image) As Image
        Dim w As Single = Image.Width / 320
        Dim h As Single = Image.Height / 240
        If w > 1 Or h > 1 Then
            Dim size As Size
            If w > h Then
                size = New SizeF(Image.Width / w, Image.Height / w).ToSize()
            Else
                size = New SizeF(Image.Width / h, Image.Height / h).ToSize()
            End If

            Dim dest As New Bitmap(size.Width, size.Height)
            Dim g As Graphics = Graphics.FromImage(dest)
            g.DrawImage(Image, 0, 0, dest.Width, dest.Height)
            Return dest
        End If

        Return Image
    End Function

    Private Sub dgvContacts_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvContacts.CellValueChanged
        If e.RowIndex > -1 Then
            If False = dgvContacts.Rows(e.RowIndex).IsNewRow Then
                Dim allEmpty As Boolean = True
                For Each cell As DataGridViewCell In dgvContacts.Rows(e.RowIndex).Cells
                    If cell.Value.ToString().Trim() <> "" Then
                        allEmpty = False
                        Exit For
                    End If
                Next

                If allEmpty Then
                    dgvContacts.Rows.RemoveAt(e.RowIndex)
                End If
            End If
        End If
    End Sub

    Private Sub dgvContacts_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvContacts.MouseClick
        Dim dgv As DataGridView = sender
        If dgv.HitTest(e.X, e.Y).Type = DataGridViewHitTestType.RowHeader Then
            dgv.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2
            dgv.EndEdit()
        Else
            dgv.EditMode = DataGridViewEditMode.EditOnEnter
        End If
    End Sub

    Private Sub GetClassComboBox()
        Dim dt As DataTable = CSOL.Data.GetNameValuePairs("ClassType")
        Dim row As DataRow = dt.NewRow()
        row.ItemArray = New String() {"-1", "ClassType", "所有班別"}
        dt.Rows.InsertAt(row, 0)

        With cbClassType
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = dt
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbClassSalesGroup
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("SalesGroup")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbClassSalesManager
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("SalesManager")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With

        With cbClassSales
            .ValueMember = "ID"
            .DisplayMember = "Value"
            .DataSource = CSOL.Data.GetNameValuePairs("Sales")
            If .Items.Count > 0 Then
                .SelectedIndex = 0
            End If
        End With
    End Sub

    Private Sub cbClassType_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbClassType.SelectedIndexChanged
        If Me.m_ClassList IsNot Nothing Then
            If cbClassType.Text = "所有班別" Then
                Me.m_ClassList.DefaultView.RowFilter = ""
            Else
                Me.m_ClassList.DefaultView.RowFilter = String.Format("Type = '{0}'", cbClassType.Text)
            End If
        End If
    End Sub

    Private Sub GetClassList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Class", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_ClassList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvClassList.DataSource = Me.m_ClassList
    End Sub

    Private Sub GetStuClassList()
        dgvFeeList.DataSource = Nothing

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", Me.m_StuID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_StuClassList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvStuClassList.DataSource = Me.m_StuClassList
    End Sub

    Private m_ClassID As Integer = -1
    Private Sub GetStuFeeList()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("StuID", Me.m_StuID)
        RequestParams.Add("ClassID", Me.m_ClassID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetFeeList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_StuFeeList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        dgvFeeList.DataSource = Me.m_StuFeeList
    End Sub

    Private Sub dgvStuClassList_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvStuClassList.SelectionChanged
        If dgvStuClassList.SelectedRows.Count > 0 Then
            Me.m_ClassID = dgvStuClassList.SelectedRows(0).DataBoundItem.Row("ClassID")
            GetStuFeeList()
        End If
    End Sub

    Private Sub btnAddClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddClass.Click
        If dgvClassList.SelectedRows.Count = 0 Then
            MessageBox.Show("目前沒有班級可以報名...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            If Me.m_StuID = -1 Then
                Exit Sub
            End If

            Dim row As DataRow = CType(dgvClassList.SelectedRows(0).DataBoundItem, DataRowView).Row
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("StuID", Me.m_StuID)
            RequestParams.Add("ClassID", row("ID"))
            RequestParams.Add("SalesGroup", cbSalesGroup.Text)
            RequestParams.Add("SalesManager", cbSalesManager.Text)
            RequestParams.Add("Sales", cbSales.Text)

            Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "SaveItem", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

            Dim Status As String = ResponseParams("Status")
            If Status = "OK" Then
                MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)

                If row("BookSeat") = 1 And row("ClassroomCount") > 0 Then
                    Dim dlg As New dlgSeatBooking(Me.m_StuID, Me.m_ClassID)
                    dlg.ShowDialog()
                End If

                GetClassList()
                GetStuClassList()
            Else
                MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

    Private Sub btnResetClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetClass.Click
        GetClassComboBox()
        GetClassList()
    End Sub

    Private Sub btnBookingSeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBookingSeat.Click
        Dim ClassID As Integer = dgvStuClassList.SelectedRows(0).DataBoundItem.Row("ClassID")
        Dim rows() As DataRow = Me.m_ClassList.Select(String.Format("ID = {0}", ClassID))
        If rows.Length > 0 Then
            Dim row As DataRow = rows(0)
            If row("ClassroomCount") > 0 Then
                Dim dlg As New dlgSeatBooking(Me.m_StuID, Me.m_ClassID)
                dlg.ShowDialog()
            Else
                MessageBox.Show("這個班級目前沒有教室可劃位...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub

    Private Sub btnAddFee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddFee.Click
        Dim dlg As New dlgFee(Me.m_StuID, Me.m_ClassID)
        dlg.ShowDialog()
        GetStuClassList()
    End Sub

    Private Sub btnLeaveClass_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLeaveClass.Click
        GetStuClassList()
    End Sub

    Private Sub btnModifyFee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnModifyFee.Click
        If dgvFeeList.SelectedRows.Count > 0 Then
            Dim dlg As New dlgFee(Me.m_StuID, Me.m_ClassID, dgvFeeList.SelectedRows(0).DataBoundItem.Row("ID"))
            dlg.ShowDialog()
            GetStuClassList()
        End If
    End Sub

    Private Sub btnDeleteFee_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteFee.Click
        If dgvFeeList.SelectedRows.Count > 0 Then
            Dim row As DataRow = dgvFeeList.SelectedRows(0).DataBoundItem.Row
            If MessageBox.Show(String.Format("您確定要刪除這個繳費記錄(收據編號：{0})嗎??", row("Receipt")), "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("ID", row("ID"))

                Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "RemoveFeeItem", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

                Dim Status As String = ResponseParams("Status")
                If Status = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    GetStuClassList()
                Else
                    MessageBox.Show("刪除失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            End If
        End If
    End Sub

    Private Sub btnPrintReceipt_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrintReceipt.Click
        PrintReceipt()
    End Sub

    Private Sub PrintReceipt()
        If dgvFeeList.SelectedRows.Count > 0 Then
            Dim ID As Integer = dgvFeeList.SelectedRows(0).DataBoundItem.Row("ID")
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("ID", ID)

            Dim ResponseBody As String = CSOL.HTTPClient.Post("StuClass", "GetReceiptInfo", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
            Dim ReceiptInfo As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("ReceiptInfo"))

            Dim ClassAndSeat As String = String.Format("{0} {1}{2}", ReceiptInfo.Rows(0)("ClassName"), CSOL.Convert.GetAlphabet(ReceiptInfo.Rows(0)("X")), CSOL.Convert.GetNumeric(ReceiptInfo.Rows(0)("Y")))
            CSOL.Report.Student.Receipt.DirectPrint(ReceiptInfo.Rows(0)("Receipt"), ReceiptInfo.Rows(0)("DateFrom"), ReceiptInfo.Rows(0)("DateTo"), ReceiptInfo.Rows(0)("StuName"), ReceiptInfo.Rows(0)("Amount"), ReceiptInfo.Rows(0)("Extra"), ClassAndSeat)
        End If
    End Sub
End Class