﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmUsers
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmUsers))
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Label8 = New System.Windows.Forms.Label
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnRemove = New System.Windows.Forms.Button
        Me.clbPriority = New System.Windows.Forms.CheckedListBox
        Me.clbGroup = New System.Windows.Forms.CheckedListBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.tbName = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbPassword = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.tbUsername = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvUsers = New System.Windows.Forms.DataGridView
        Me.Username = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DisplayName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UserGroup = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Priority = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.dsUsers = New System.Data.DataSet
        Me.dtUsers = New System.Data.DataTable
        Me.DataColumn1 = New System.Data.DataColumn
        Me.DataColumn2 = New System.Data.DataColumn
        Me.DataColumn3 = New System.Data.DataColumn
        Me.DataColumn4 = New System.Data.DataColumn
        Me.DataColumn5 = New System.Data.DataColumn
        Me.dtGroups = New System.Data.DataTable
        Me.DataColumn6 = New System.Data.DataColumn
        Me.DataColumn7 = New System.Data.DataColumn
        Me.DataColumn8 = New System.Data.DataColumn
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.btnResetGroup = New System.Windows.Forms.Button
        Me.cbGroup = New System.Windows.Forms.ComboBox
        Me.btnRemoveGroup = New System.Windows.Forms.Button
        Me.btnSaveGroup = New System.Windows.Forms.Button
        Me.Label6 = New System.Windows.Forms.Label
        Me.clbGroupPriority = New System.Windows.Forms.CheckedListBox
        Me.Label7 = New System.Windows.Forms.Label
        Me.IDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UsernameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.NameDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.UserGroupDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PriorityDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dsUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtUsers, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dtGroups, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Plum
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.btnReset)
        Me.Panel1.Controls.Add(Me.btnSave)
        Me.Panel1.Controls.Add(Me.btnRemove)
        Me.Panel1.Controls.Add(Me.clbPriority)
        Me.Panel1.Controls.Add(Me.clbGroup)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.tbName)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.tbPassword)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.tbUsername)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(3, 18)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(234, 441)
        Me.Panel1.TabIndex = 0
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.Color.Firebrick
        Me.Label8.Location = New System.Drawing.Point(80, 63)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(125, 12)
        Me.Label8.TabIndex = 7
        Me.Label8.Text = "修改時留白就不變更。"
        '
        'btnReset
        '
        Me.btnReset.Location = New System.Drawing.Point(148, 389)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(49, 23)
        Me.btnReset.TabIndex = 6
        Me.btnReset.Text = "重來"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(38, 389)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(49, 23)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "新增"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Location = New System.Drawing.Point(93, 389)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(49, 23)
        Me.btnRemove.TabIndex = 2
        Me.btnRemove.Text = "刪除"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'clbPriority
        '
        Me.clbPriority.CheckOnClick = True
        Me.clbPriority.FormattingEnabled = True
        Me.clbPriority.Location = New System.Drawing.Point(82, 192)
        Me.clbPriority.Name = "clbPriority"
        Me.clbPriority.ScrollAlwaysVisible = True
        Me.clbPriority.Size = New System.Drawing.Size(141, 191)
        Me.clbPriority.TabIndex = 4
        '
        'clbGroup
        '
        Me.clbGroup.CheckOnClick = True
        Me.clbGroup.FormattingEnabled = True
        Me.clbGroup.Location = New System.Drawing.Point(82, 114)
        Me.clbGroup.Name = "clbGroup"
        Me.clbGroup.ScrollAlwaysVisible = True
        Me.clbGroup.Size = New System.Drawing.Size(141, 72)
        Me.clbGroup.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(11, 192)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(65, 12)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "附加權限："
        '
        'tbName
        '
        Me.tbName.Location = New System.Drawing.Point(82, 86)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(141, 22)
        Me.tbName.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(35, 117)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "群組："
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(35, 91)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "姓名："
        '
        'tbPassword
        '
        Me.tbPassword.Location = New System.Drawing.Point(82, 38)
        Me.tbPassword.Name = "tbPassword"
        Me.tbPassword.Size = New System.Drawing.Size(141, 22)
        Me.tbPassword.TabIndex = 1
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(35, 43)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "密碼："
        '
        'tbUsername
        '
        Me.tbUsername.Location = New System.Drawing.Point(82, 10)
        Me.tbUsername.Name = "tbUsername"
        Me.tbUsername.Size = New System.Drawing.Size(141, 22)
        Me.tbUsername.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(35, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "帳號："
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvUsers)
        Me.GroupBox1.Controls.Add(Me.Panel1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(211, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(773, 462)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "使用者管理"
        '
        'dgvUsers
        '
        Me.dgvUsers.AllowUserToAddRows = False
        Me.dgvUsers.AllowUserToDeleteRows = False
        Me.dgvUsers.AutoGenerateColumns = False
        Me.dgvUsers.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(250, Byte), Integer), CType(CType(230, Byte), Integer), CType(CType(250, Byte), Integer))
        Me.dgvUsers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvUsers.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Username, Me.DisplayName, Me.UserGroup, Me.Priority, Me.IDDataGridViewTextBoxColumn, Me.UsernameDataGridViewTextBoxColumn, Me.NameDataGridViewTextBoxColumn, Me.UserGroupDataGridViewTextBoxColumn, Me.PriorityDataGridViewTextBoxColumn})
        Me.dgvUsers.DataMember = "Users"
        Me.dgvUsers.DataSource = Me.dsUsers
        Me.dgvUsers.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvUsers.Location = New System.Drawing.Point(237, 18)
        Me.dgvUsers.MultiSelect = False
        Me.dgvUsers.Name = "dgvUsers"
        Me.dgvUsers.ReadOnly = True
        Me.dgvUsers.RowTemplate.Height = 24
        Me.dgvUsers.Size = New System.Drawing.Size(533, 441)
        Me.dgvUsers.TabIndex = 1
        Me.dgvUsers.TabStop = False
        '
        'Username
        '
        Me.Username.DataPropertyName = "Username"
        Me.Username.HeaderText = "帳號"
        Me.Username.Name = "Username"
        Me.Username.ReadOnly = True
        '
        'DisplayName
        '
        Me.DisplayName.DataPropertyName = "Name"
        Me.DisplayName.HeaderText = "姓名"
        Me.DisplayName.Name = "DisplayName"
        Me.DisplayName.ReadOnly = True
        '
        'UserGroup
        '
        Me.UserGroup.DataPropertyName = "UserGroup"
        Me.UserGroup.HeaderText = "群組"
        Me.UserGroup.Name = "UserGroup"
        Me.UserGroup.ReadOnly = True
        '
        'Priority
        '
        Me.Priority.DataPropertyName = "Priority"
        Me.Priority.HeaderText = "附加權限"
        Me.Priority.Name = "Priority"
        Me.Priority.ReadOnly = True
        '
        'dsUsers
        '
        Me.dsUsers.DataSetName = "NewDataSet"
        Me.dsUsers.Tables.AddRange(New System.Data.DataTable() {Me.dtUsers, Me.dtGroups})
        '
        'dtUsers
        '
        Me.dtUsers.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn1, Me.DataColumn2, Me.DataColumn3, Me.DataColumn4, Me.DataColumn5})
        Me.dtUsers.TableName = "Users"
        '
        'DataColumn1
        '
        Me.DataColumn1.Caption = "ID"
        Me.DataColumn1.ColumnName = "ID"
        Me.DataColumn1.DataType = GetType(Integer)
        '
        'DataColumn2
        '
        Me.DataColumn2.Caption = "Username"
        Me.DataColumn2.ColumnName = "Username"
        '
        'DataColumn3
        '
        Me.DataColumn3.Caption = "Name"
        Me.DataColumn3.ColumnName = "Name"
        '
        'DataColumn4
        '
        Me.DataColumn4.Caption = "UserGroup"
        Me.DataColumn4.ColumnName = "UserGroup"
        '
        'DataColumn5
        '
        Me.DataColumn5.Caption = "Priority"
        Me.DataColumn5.ColumnName = "Priority"
        '
        'dtGroups
        '
        Me.dtGroups.Columns.AddRange(New System.Data.DataColumn() {Me.DataColumn6, Me.DataColumn7, Me.DataColumn8})
        Me.dtGroups.TableName = "Groups"
        '
        'DataColumn6
        '
        Me.DataColumn6.Caption = "Name"
        Me.DataColumn6.ColumnName = "Name"
        '
        'DataColumn7
        '
        Me.DataColumn7.Caption = "Code"
        Me.DataColumn7.ColumnName = "Code"
        Me.DataColumn7.DataType = GetType(Integer)
        '
        'DataColumn8
        '
        Me.DataColumn8.Caption = "Text"
        Me.DataColumn8.ColumnName = "Text"
        '
        'GroupBox2
        '
        Me.GroupBox2.BackColor = System.Drawing.Color.Thistle
        Me.GroupBox2.Controls.Add(Me.btnResetGroup)
        Me.GroupBox2.Controls.Add(Me.cbGroup)
        Me.GroupBox2.Controls.Add(Me.btnRemoveGroup)
        Me.GroupBox2.Controls.Add(Me.btnSaveGroup)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.clbGroupPriority)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Left
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(211, 462)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "群組管理"
        '
        'btnResetGroup
        '
        Me.btnResetGroup.Location = New System.Drawing.Point(136, 244)
        Me.btnResetGroup.Name = "btnResetGroup"
        Me.btnResetGroup.Size = New System.Drawing.Size(49, 23)
        Me.btnResetGroup.TabIndex = 3
        Me.btnResetGroup.Text = "重來"
        Me.btnResetGroup.UseVisualStyleBackColor = True
        '
        'cbGroup
        '
        Me.cbGroup.FormattingEnabled = True
        Me.cbGroup.Location = New System.Drawing.Point(59, 21)
        Me.cbGroup.Name = "cbGroup"
        Me.cbGroup.Size = New System.Drawing.Size(141, 20)
        Me.cbGroup.TabIndex = 0
        '
        'btnRemoveGroup
        '
        Me.btnRemoveGroup.Location = New System.Drawing.Point(81, 244)
        Me.btnRemoveGroup.Name = "btnRemoveGroup"
        Me.btnRemoveGroup.Size = New System.Drawing.Size(49, 23)
        Me.btnRemoveGroup.TabIndex = 2
        Me.btnRemoveGroup.Text = "刪除"
        Me.btnRemoveGroup.UseVisualStyleBackColor = True
        '
        'btnSaveGroup
        '
        Me.btnSaveGroup.Location = New System.Drawing.Point(26, 244)
        Me.btnSaveGroup.Name = "btnSaveGroup"
        Me.btnSaveGroup.Size = New System.Drawing.Size(49, 23)
        Me.btnSaveGroup.TabIndex = 2
        Me.btnSaveGroup.Text = "新增"
        Me.btnSaveGroup.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 25)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(41, 12)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "群組："
        '
        'clbGroupPriority
        '
        Me.clbGroupPriority.CheckOnClick = True
        Me.clbGroupPriority.FormattingEnabled = True
        Me.clbGroupPriority.Location = New System.Drawing.Point(59, 47)
        Me.clbGroupPriority.Name = "clbGroupPriority"
        Me.clbGroupPriority.ScrollAlwaysVisible = True
        Me.clbGroupPriority.Size = New System.Drawing.Size(141, 191)
        Me.clbGroupPriority.TabIndex = 1
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 50)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(41, 12)
        Me.Label7.TabIndex = 0
        Me.Label7.Text = "權限："
        '
        'IDDataGridViewTextBoxColumn
        '
        Me.IDDataGridViewTextBoxColumn.DataPropertyName = "ID"
        Me.IDDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IDDataGridViewTextBoxColumn.Name = "IDDataGridViewTextBoxColumn"
        Me.IDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UsernameDataGridViewTextBoxColumn
        '
        Me.UsernameDataGridViewTextBoxColumn.DataPropertyName = "Username"
        Me.UsernameDataGridViewTextBoxColumn.HeaderText = "Username"
        Me.UsernameDataGridViewTextBoxColumn.Name = "UsernameDataGridViewTextBoxColumn"
        Me.UsernameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'NameDataGridViewTextBoxColumn
        '
        Me.NameDataGridViewTextBoxColumn.DataPropertyName = "Name"
        Me.NameDataGridViewTextBoxColumn.HeaderText = "Name"
        Me.NameDataGridViewTextBoxColumn.Name = "NameDataGridViewTextBoxColumn"
        Me.NameDataGridViewTextBoxColumn.ReadOnly = True
        '
        'UserGroupDataGridViewTextBoxColumn
        '
        Me.UserGroupDataGridViewTextBoxColumn.DataPropertyName = "UserGroup"
        Me.UserGroupDataGridViewTextBoxColumn.HeaderText = "UserGroup"
        Me.UserGroupDataGridViewTextBoxColumn.Name = "UserGroupDataGridViewTextBoxColumn"
        Me.UserGroupDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PriorityDataGridViewTextBoxColumn
        '
        Me.PriorityDataGridViewTextBoxColumn.DataPropertyName = "Priority"
        Me.PriorityDataGridViewTextBoxColumn.HeaderText = "Priority"
        Me.PriorityDataGridViewTextBoxColumn.Name = "PriorityDataGridViewTextBoxColumn"
        Me.PriorityDataGridViewTextBoxColumn.ReadOnly = True
        '
        'frmUsers
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Plum
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmUsers"
        Me.Text = "使用者管理"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dsUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtUsers, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dtGroups, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvUsers As System.Windows.Forms.DataGridView
    Friend WithEvents tbUsername As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents tbPassword As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents clbPriority As System.Windows.Forms.CheckedListBox
    Friend WithEvents clbGroup As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents dsUsers As System.Data.DataSet
    Friend WithEvents dtUsers As System.Data.DataTable
    Friend WithEvents DataColumn1 As System.Data.DataColumn
    Friend WithEvents DataColumn2 As System.Data.DataColumn
    Friend WithEvents DataColumn3 As System.Data.DataColumn
    Friend WithEvents DataColumn4 As System.Data.DataColumn
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents cbGroup As System.Windows.Forms.ComboBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents clbGroupPriority As System.Windows.Forms.CheckedListBox
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents btnResetGroup As System.Windows.Forms.Button
    Friend WithEvents btnSaveGroup As System.Windows.Forms.Button
    Friend WithEvents btnRemoveGroup As System.Windows.Forms.Button
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents DataColumn5 As System.Data.DataColumn
    Friend WithEvents dtGroups As System.Data.DataTable
    Friend WithEvents DataColumn6 As System.Data.DataColumn
    Friend WithEvents DataColumn7 As System.Data.DataColumn
    Friend WithEvents DataColumn8 As System.Data.DataColumn
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Username As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DisplayName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserGroup As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Priority As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsernameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NameDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UserGroupDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PriorityDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
