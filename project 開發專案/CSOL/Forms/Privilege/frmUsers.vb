﻿Public Class frmUsers
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private Sub frmUsers_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadGroups()
        LoadUsers()

        InitCheckListBox()

        tbUsername.Select()
    End Sub

    Private Sub InitComboBox()
        clbGroup.Items.Clear()
        For Each row As DataRow In dtGroups.Select(String.Format("Name = 'UserGroup'"))
            clbGroup.Items.Add(row("Text"))
        Next

        Dim dt As DataTable = dtGroups.Copy()
        Dim newRow As DataRow = dt.NewRow()
        newRow.ItemArray = "UserGroup,-1,新增群組".Split(",")
        dt.Rows.InsertAt(newRow, 0)

        Dim dv As New DataView(dt, "Name = 'UserGroup'", "", DataViewRowState.CurrentRows)

        With cbGroup
            .ValueMember = "Code"
            .DisplayMember = "Text"
            .DataSource = dv
        End With
    End Sub

    Private Sub InitCheckListBox()
        Dim dt As DataTable = CSOL.Data.GetNameValuePairs("Priority")
        For Each row As DataRow In dt.Rows
            clbGroupPriority.Items.Add(row("Text"))
            clbPriority.Items.Add(row("Text"))
        Next
    End Sub

    Private Sub LoadGroups()
        dtGroups.Clear()

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "GetGroup", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Group"))
        For Each row As DataRow In dt.Rows
            dtGroups.Rows.Add(row.ItemArray)
        Next

        InitComboBox()
    End Sub

    Private Sub LoadUsers()
        dtUsers.Clear()

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "GetUser", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("User"))
        For Each row As DataRow In dt.Rows
            dtUsers.Rows.Add(row.ItemArray)
        Next
    End Sub

    Private Sub cbGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbGroup.SelectedIndexChanged
        If cbGroup.SelectedIndex > -1 Then
            If cbGroup.Text = "系統管理員" Then
                btnSaveGroup.Enabled = False
                btnRemoveGroup.Enabled = False
            Else
                btnSaveGroup.Enabled = True
                btnRemoveGroup.Enabled = True
            End If

            If cbGroup.SelectedValue = -1 Then
                btnSaveGroup.Text = "新增"
                cbGroup.Select()
                cbGroup.SelectAll()
                btnRemoveGroup.Enabled = False
                For i As Integer = 0 To clbGroupPriority.Items.Count - 1
                    clbGroupPriority.SetItemChecked(i, False)
                Next
            Else
                btnSaveGroup.Text = "修改"
                btnRemoveGroup.Enabled = True
                Dim GroupPriorities As New List(Of String)
                For Each row As DataRow In dtGroups.Select(String.Format("Name = 'UserGroupPriority-{0}'", cbGroup.Text))
                    GroupPriorities.Add(row("Text"))
                Next

                For i As Integer = 0 To clbGroupPriority.Items.Count - 1
                    If GroupPriorities.Contains(clbGroupPriority.Items(i)) Then
                        clbGroupPriority.SetItemChecked(i, True)
                        If clbGroupPriority.Items(i) = "所有功能" Then
                            Exit For
                        End If
                    Else
                        clbGroupPriority.SetItemChecked(i, False)
                    End If
                Next
            End If
            If clbGroupPriority.Items.Count > 0 Then
                clbGroupPriority.SetSelected(0, True)
            End If
        End If
    End Sub

    Private Sub btnGroupSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveGroup.Click
        If cbGroup.Text = "系統管理員" Then
            MessageBox.Show("系統管理員是預設的，不能改變...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbGroup.Select()
            Exit Sub
        End If

        Dim GroupPriorities(clbGroupPriority.CheckedItems.Count - 1) As String
        clbGroupPriority.CheckedItems.CopyTo(GroupPriorities, 0)
        Dim GroupPriority As String = IIf(Array.IndexOf(GroupPriorities, "所有功能") > -1, "所有功能", String.Join(",", GroupPriorities))

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("Group", cbGroup.Text)
        RequestParams.Add("GroupPriority", GroupPriority)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "SaveGroup", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        LoadGroups()
    End Sub

    Private Sub btnRemoveGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemoveGroup.Click
        If cbGroup.SelectedValue > -1 Then
            If cbGroup.Text = "系統管理員" Then
                MessageBox.Show("系統管理員是預設的，不能改變...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                cbGroup.Select()
                Exit Sub
            End If

            If MessageBox.Show(String.Format("您確定要刪除群組 {0} ??", cbGroup.Text), "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                Dim RequstParams As New System.Collections.Specialized.NameValueCollection()
                RequstParams.Add("Group", cbGroup.Text)

                Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "RemoveGroup", RequstParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                If ResponseParams("Status") = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("刪除失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If

                LoadGroups()
            End If
        End If
    End Sub

    Private Sub btnGroupReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnResetGroup.Click
        cbGroup.SelectedIndex = 0
        cbGroup.Text = ""
        cbGroup.Select()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim UserGroups(clbGroup.CheckedItems.Count - 1) As String
        Dim Priorities(clbPriority.CheckedItems.Count - 1) As String

        clbGroup.CheckedItems.CopyTo(UserGroups, 0)
        clbPriority.CheckedItems.CopyTo(Priorities, 0)

        Dim UserGroup As String = String.Join(",", UserGroups)
        Dim Priority As String = String.Join(",", Priorities)

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        If dgvUsers.SelectedRows.Count = 0 Then
            RequestParams.Add("ID", -1)
        Else
            RequestParams.Add("ID", CType(dgvUsers.SelectedRows(0).DataBoundItem, DataRowView).Row("ID"))
        End If

        RequestParams.Add("Username", tbUsername.Text)
        RequestParams.Add("Password", tbPassword.Text)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("UserGroup", UserGroup)
        RequestParams.Add("Priority", Priority)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "SaveUser", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

        LoadUsers()
        ClearUser()
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If dgvUsers.SelectedRows.Count > 0 Then
            Dim row As DataRow = CType(dgvUsers.SelectedRows(0).DataBoundItem, DataRowView).Row
            If row("ID") = 1 Then
                MessageBox.Show("系統管理員是預設的，不能改變...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                tbUsername.Select()
                Exit Sub
            End If

            If MessageBox.Show(String.Format("您確定要刪除使用者 {0} ??", row("Name")), "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                Dim RequstParams As New System.Collections.Specialized.NameValueCollection()
                RequstParams.Add("ID", row("ID"))

                Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "RemoveUser", RequstParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                If ResponseParams("Status") = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("刪除失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                End If

                LoadUsers()
            End If
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        If dgvUsers.SelectedRows.Count = 0 Then
            ClearUser()
        Else
            ModifyUser()
        End If
    End Sub

    Private Sub ModifyUser()
        If dgvUsers.SelectedRows.Count > 0 Then
            Dim row As DataRow = CType(dgvUsers.SelectedRows(0).DataBoundItem, DataRowView).Row
            tbUsername.Text = row("Username")
            tbPassword.Text = ""
            tbName.Text = row("Name")

            Dim UserGroups() As String = row("UserGroup").ToString().Split(",")
            Dim Priorities() As String = row("Priority").ToString().Split(",")
            For i As Integer = 0 To clbGroup.Items.Count - 1
                If Array.IndexOf(UserGroups, clbGroup.Items(i)) = -1 Then
                    clbGroup.SetItemChecked(i, False)
                Else
                    clbGroup.SetItemChecked(i, True)
                End If
            Next

            For i As Integer = 0 To clbPriority.Items.Count - 1
                If Array.IndexOf(Priorities, clbPriority.Items(i)) = -1 Then
                    clbPriority.SetItemChecked(i, False)
                Else
                    clbPriority.SetItemChecked(i, True)
                End If
            Next

            tbUsername.Select()
        End If
    End Sub

    Private Sub ClearUser()
        tbUsername.Text = ""
        tbPassword.Text = ""
        tbName.Text = ""

        For i As Integer = 0 To clbGroup.Items.Count - 1
            clbGroup.SetItemChecked(i, False)
        Next

        For i As Integer = 0 To clbPriority.Items.Count - 1
            clbPriority.SetItemChecked(i, False)
        Next
        btnSave.Text = "新增"
        btnRemove.Enabled = False
        tbUsername.Select()
    End Sub

    Private Sub dgvUsers_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvUsers.SelectionChanged
        If dgvUsers.SelectedRows.Count = 0 Then
            btnSave.Text = "新增"
            btnRemove.Enabled = False
            ClearUser()
        Else
            btnSave.Text = "修改"
            btnRemove.Enabled = True
            ModifyUser()
        End If
    End Sub

    Private Sub tbUsername_Leave(ByVal sender As Object, ByVal e As System.EventArgs) Handles tbUsername.Leave
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        If dgvUsers.SelectedRows.Count = 0 Then
            RequestParams.Add("ID", -1)
        Else
            RequestParams.Add("ID", CType(dgvUsers.SelectedRows(0).DataBoundItem, DataRowView).Row("ID"))
        End If
        RequestParams.Add("Username", tbUsername.Text)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Users", "IsUsernameExists", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("IsUsernameExists") = True Then
            MessageBox.Show("這個使用者名稱重覆了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbUsername.Select()
            tbUsername.SelectAll()
            LoadUsers()
        End If
    End Sub
End Class