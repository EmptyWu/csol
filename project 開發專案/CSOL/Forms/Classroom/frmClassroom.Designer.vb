﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmClassroom
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmClassroom))
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvList = New System.Windows.Forms.DataGridView
        Me.colID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colColumns = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colRows = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colSeats = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.btnNotLane = New System.Windows.Forms.Button
        Me.btnDisable = New System.Windows.Forms.Button
        Me.btnSeat = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.tbName = New System.Windows.Forms.TextBox
        Me.lblSeats = New System.Windows.Forms.Label
        Me.nudColumns = New System.Windows.Forms.NumericUpDown
        Me.nudRows = New System.Windows.Forms.NumericUpDown
        Me.btnColumnLane = New System.Windows.Forms.Button
        Me.btnRowLane = New System.Windows.Forms.Button
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnRemove = New System.Windows.Forms.Button
        Me.btnReset = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.dgvSeat = New System.Windows.Forms.DataGridView
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.lblLane = New System.Windows.Forms.Label
        Me.lblDisableSeat = New System.Windows.Forms.Label
        Me.lblSeat = New System.Windows.Forms.Label
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.nudColumns, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nudRows, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel2)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.dgvSeat)
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel1)
        Me.SplitContainer1.Size = New System.Drawing.Size(984, 462)
        Me.SplitContainer1.SplitterDistance = 352
        Me.SplitContainer1.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox1)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupBox2)
        Me.SplitContainer2.Size = New System.Drawing.Size(352, 430)
        Me.SplitContainer2.SplitterDistance = 256
        Me.SplitContainer2.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.dgvList)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(352, 256)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "教室一覽表"
        '
        'dgvList
        '
        Me.dgvList.AllowUserToDeleteRows = False
        Me.dgvList.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvList.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colID, Me.colName, Me.colColumns, Me.colRows, Me.colSeats})
        Me.dgvList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvList.Location = New System.Drawing.Point(3, 18)
        Me.dgvList.MultiSelect = False
        Me.dgvList.Name = "dgvList"
        Me.dgvList.ReadOnly = True
        Me.dgvList.RowTemplate.Height = 24
        Me.dgvList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvList.Size = New System.Drawing.Size(346, 235)
        Me.dgvList.TabIndex = 0
        '
        'colID
        '
        Me.colID.HeaderText = "ID"
        Me.colID.Name = "colID"
        Me.colID.ReadOnly = True
        Me.colID.Visible = False
        '
        'colName
        '
        Me.colName.HeaderText = "名稱"
        Me.colName.Name = "colName"
        Me.colName.ReadOnly = True
        '
        'colColumns
        '
        Me.colColumns.HeaderText = "行"
        Me.colColumns.Name = "colColumns"
        Me.colColumns.ReadOnly = True
        Me.colColumns.Width = 50
        '
        'colRows
        '
        Me.colRows.HeaderText = "列"
        Me.colRows.Name = "colRows"
        Me.colRows.ReadOnly = True
        Me.colRows.Width = 50
        '
        'colSeats
        '
        Me.colSeats.HeaderText = "座位數"
        Me.colSeats.Name = "colSeats"
        Me.colSeats.ReadOnly = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(352, 170)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "教室資訊"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 6
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnNotLane, 3, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnDisable, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnSeat, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label2, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tbName, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lblSeats, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.nudColumns, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.nudRows, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnColumnLane, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.btnRowLane, 3, 3)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 18)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 6
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(346, 149)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'btnNotLane
        '
        Me.btnNotLane.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.btnNotLane, 3)
        Me.btnNotLane.ForeColor = System.Drawing.Color.Tomato
        Me.btnNotLane.Location = New System.Drawing.Point(222, 116)
        Me.btnNotLane.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNotLane.Name = "btnNotLane"
        Me.btnNotLane.Size = New System.Drawing.Size(75, 26)
        Me.btnNotLane.TabIndex = 7
        Me.btnNotLane.Text = "不是走道"
        Me.btnNotLane.UseVisualStyleBackColor = True
        '
        'btnDisable
        '
        Me.btnDisable.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.btnDisable, 3)
        Me.btnDisable.ForeColor = System.Drawing.Color.Tomato
        Me.btnDisable.Location = New System.Drawing.Point(49, 116)
        Me.btnDisable.Margin = New System.Windows.Forms.Padding(0)
        Me.btnDisable.Name = "btnDisable"
        Me.btnDisable.Size = New System.Drawing.Size(75, 26)
        Me.btnDisable.TabIndex = 4
        Me.btnDisable.Text = "不能坐"
        Me.btnDisable.UseVisualStyleBackColor = True
        '
        'btnSeat
        '
        Me.btnSeat.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.btnSeat, 3)
        Me.btnSeat.Location = New System.Drawing.Point(49, 90)
        Me.btnSeat.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSeat.Name = "btnSeat"
        Me.btnSeat.Size = New System.Drawing.Size(75, 26)
        Me.btnSeat.TabIndex = 3
        Me.btnSeat.Text = "可以坐"
        Me.btnSeat.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(21, 10)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "名稱："
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(33, 42)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(29, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "行："
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(206, 42)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(29, 12)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "列："
        '
        'tbName
        '
        Me.tbName.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel1.SetColumnSpan(Me.tbName, 5)
        Me.tbName.Font = New System.Drawing.Font("新細明體", 11.0!)
        Me.tbName.Location = New System.Drawing.Point(68, 3)
        Me.tbName.Name = "tbName"
        Me.tbName.Size = New System.Drawing.Size(272, 25)
        Me.tbName.TabIndex = 0
        '
        'lblSeats
        '
        Me.lblSeats.AutoSize = True
        Me.TableLayoutPanel1.SetColumnSpan(Me.lblSeats, 3)
        Me.lblSeats.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSeats.ForeColor = System.Drawing.Color.DarkBlue
        Me.lblSeats.Location = New System.Drawing.Point(3, 64)
        Me.lblSeats.Name = "lblSeats"
        Me.lblSeats.Size = New System.Drawing.Size(167, 26)
        Me.lblSeats.TabIndex = 3
        Me.lblSeats.Text = "座位數：0"
        Me.lblSeats.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'nudColumns
        '
        Me.nudColumns.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.nudColumns.Font = New System.Drawing.Font("新細明體", 11.0!)
        Me.nudColumns.Location = New System.Drawing.Point(68, 35)
        Me.nudColumns.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nudColumns.Name = "nudColumns"
        Me.nudColumns.Size = New System.Drawing.Size(44, 25)
        Me.nudColumns.TabIndex = 1
        Me.nudColumns.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'nudRows
        '
        Me.nudRows.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.nudRows.Font = New System.Drawing.Font("新細明體", 11.0!)
        Me.nudRows.Location = New System.Drawing.Point(241, 35)
        Me.nudRows.Maximum = New Decimal(New Integer() {200, 0, 0, 0})
        Me.nudRows.Name = "nudRows"
        Me.nudRows.Size = New System.Drawing.Size(44, 25)
        Me.nudRows.TabIndex = 2
        Me.nudRows.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'btnColumnLane
        '
        Me.btnColumnLane.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.btnColumnLane, 3)
        Me.btnColumnLane.Location = New System.Drawing.Point(222, 64)
        Me.btnColumnLane.Margin = New System.Windows.Forms.Padding(0)
        Me.btnColumnLane.Name = "btnColumnLane"
        Me.btnColumnLane.Size = New System.Drawing.Size(75, 26)
        Me.btnColumnLane.TabIndex = 5
        Me.btnColumnLane.Text = "直走道"
        Me.btnColumnLane.UseVisualStyleBackColor = True
        '
        'btnRowLane
        '
        Me.btnRowLane.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.TableLayoutPanel1.SetColumnSpan(Me.btnRowLane, 3)
        Me.btnRowLane.Location = New System.Drawing.Point(222, 90)
        Me.btnRowLane.Margin = New System.Windows.Forms.Padding(0)
        Me.btnRowLane.Name = "btnRowLane"
        Me.btnRowLane.Size = New System.Drawing.Size(75, 26)
        Me.btnRowLane.TabIndex = 6
        Me.btnRowLane.Text = "橫走道"
        Me.btnRowLane.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnRemove)
        Me.Panel2.Controls.Add(Me.btnReset)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.btnAdd)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 430)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(352, 32)
        Me.Panel2.TabIndex = 4
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Location = New System.Drawing.Point(287, 3)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(60, 26)
        Me.btnRemove.TabIndex = 3
        Me.btnRemove.Text = "刪除"
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'btnReset
        '
        Me.btnReset.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnReset.Location = New System.Drawing.Point(138, 3)
        Me.btnReset.Name = "btnReset"
        Me.btnReset.Size = New System.Drawing.Size(60, 26)
        Me.btnReset.TabIndex = 2
        Me.btnReset.Text = "重來"
        Me.btnReset.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSave.Location = New System.Drawing.Point(72, 3)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(60, 26)
        Me.btnSave.TabIndex = 1
        Me.btnSave.Text = "修改"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnAdd.Location = New System.Drawing.Point(6, 3)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(60, 26)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.Text = "新增"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgvSeat
        '
        Me.dgvSeat.AllowUserToAddRows = False
        Me.dgvSeat.AllowUserToDeleteRows = False
        Me.dgvSeat.AllowUserToResizeColumns = False
        Me.dgvSeat.AllowUserToResizeRows = False
        Me.dgvSeat.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(196, Byte), Integer))
        Me.dgvSeat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvSeat.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvSeat.Location = New System.Drawing.Point(0, 50)
        Me.dgvSeat.Name = "dgvSeat"
        Me.dgvSeat.ReadOnly = True
        Me.dgvSeat.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders
        Me.dgvSeat.RowTemplate.Height = 24
        Me.dgvSeat.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvSeat.Size = New System.Drawing.Size(628, 412)
        Me.dgvSeat.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(196, Byte), Integer))
        Me.Panel1.Controls.Add(Me.lblLane)
        Me.Panel1.Controls.Add(Me.lblDisableSeat)
        Me.Panel1.Controls.Add(Me.lblSeat)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(628, 50)
        Me.Panel1.TabIndex = 1
        '
        'lblLane
        '
        Me.lblLane.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLane.AutoSize = True
        Me.lblLane.BackColor = System.Drawing.Color.Gray
        Me.lblLane.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.lblLane.Location = New System.Drawing.Point(568, 0)
        Me.lblLane.Name = "lblLane"
        Me.lblLane.Padding = New System.Windows.Forms.Padding(10, 17, 10, 17)
        Me.lblLane.Size = New System.Drawing.Size(60, 50)
        Me.lblLane.TabIndex = 1
        Me.lblLane.Text = "走道"
        '
        'lblDisableSeat
        '
        Me.lblDisableSeat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblDisableSeat.AutoSize = True
        Me.lblDisableSeat.BackColor = System.Drawing.Color.Green
        Me.lblDisableSeat.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.lblDisableSeat.Location = New System.Drawing.Point(492, 0)
        Me.lblDisableSeat.Name = "lblDisableSeat"
        Me.lblDisableSeat.Padding = New System.Windows.Forms.Padding(10, 17, 10, 17)
        Me.lblDisableSeat.Size = New System.Drawing.Size(76, 50)
        Me.lblDisableSeat.TabIndex = 1
        Me.lblDisableSeat.Text = "不能坐"
        '
        'lblSeat
        '
        Me.lblSeat.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblSeat.AutoSize = True
        Me.lblSeat.BackColor = System.Drawing.Color.White
        Me.lblSeat.Font = New System.Drawing.Font("新細明體", 12.0!)
        Me.lblSeat.ForeColor = System.Drawing.SystemColors.ControlText
        Me.lblSeat.Location = New System.Drawing.Point(416, 0)
        Me.lblSeat.Name = "lblSeat"
        Me.lblSeat.Padding = New System.Windows.Forms.Padding(10, 17, 10, 17)
        Me.lblSeat.Size = New System.Drawing.Size(76, 50)
        Me.lblSeat.TabIndex = 1
        Me.lblSeat.Text = "可以坐"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(267, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'frmClassroom
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(169, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MinimumSize = New System.Drawing.Size(950, 450)
        Me.Name = "frmClassroom"
        Me.Text = "編輯教室"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvList, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        CType(Me.nudColumns, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nudRows, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvSeat, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvList As System.Windows.Forms.DataGridView
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents tbName As System.Windows.Forms.TextBox
    Friend WithEvents nudColumns As System.Windows.Forms.NumericUpDown
    Friend WithEvents nudRows As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblSeats As System.Windows.Forms.Label
    Friend WithEvents dgvSeat As System.Windows.Forms.DataGridView
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnReset As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnNotLane As System.Windows.Forms.Button
    Friend WithEvents btnDisable As System.Windows.Forms.Button
    Friend WithEvents btnSeat As System.Windows.Forms.Button
    Friend WithEvents btnColumnLane As System.Windows.Forms.Button
    Friend WithEvents btnRowLane As System.Windows.Forms.Button
    Friend WithEvents lblLane As System.Windows.Forms.Label
    Friend WithEvents lblDisableSeat As System.Windows.Forms.Label
    Friend WithEvents lblSeat As System.Windows.Forms.Label
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents colID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colColumns As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colRows As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colSeats As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
