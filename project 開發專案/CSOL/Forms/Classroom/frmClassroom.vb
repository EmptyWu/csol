﻿Public Class frmClassroom
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private m_ClassroomID As Integer = -1

    Private m_LanesX As New List(Of Integer)
    Private m_LanesY As New List(Of Integer)
    Private m_Disables As New List(Of Point)
    Private m_Seats As Integer = 0

    Private Sub frmClassroom_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetList()
        tbName.Focus()
    End Sub

    Private Sub GetList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))

        dgvList.Rows.Clear()
        For Each row As DataRow In dt.Rows
            Dim items As New List(Of Object)
            items.AddRange(row.ItemArray)
            items.RemoveAt(items.Count - 1)

            Dim pts() As Point = CSOL.Convert.StringToPoints(row("Disables").ToString())

            items.Add(row("Columns") * row("Rows") - pts.Length)
            dgvList.Rows.Add(items.ToArray())
        Next
    End Sub

    Private Sub GetItem()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_ClassroomID)
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "GetItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim dt As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("Item"))
        tbName.Text = dt.Rows(0)("Name")
        nudColumns.Value = dt.Rows(0)("Columns")
        nudRows.Value = dt.Rows(0)("Rows")

        Me.m_LanesX.Clear()
        Me.m_LanesY.Clear()
        Me.m_Disables.Clear()

        Dim LanesPair()() As String = CSOL.Convert.PowerSplit(dt.Rows(0)("Lanes"))
        If LanesPair.Length = 2 Then
            If LanesPair(0)(0) <> "" Then
                Me.m_LanesX.AddRange(Array.ConvertAll(Of String, Integer)(LanesPair(0), AddressOf Integer.Parse))
            End If
            If LanesPair(1)(0) <> "" Then
                Me.m_LanesY.AddRange(Array.ConvertAll(Of String, Integer)(LanesPair(1), AddressOf Integer.Parse))
            End If
        End If
        Dim pts() As Point = CSOL.Convert.StringToPoints(dt.Rows(0)("Disables").ToString())
        Me.m_Disables.AddRange(pts)
    End Sub

    Private Sub dgvList_SelectionChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvList.SelectionChanged
        If dgvList.SelectedRows.Count > 0 Then
            If dgvList.SelectedRows(0).IsNewRow Then
                Me.m_ClassroomID = -1
                tbName.ResetText()
                nudColumns.Value = 0
                nudRows.Value = 0
                Me.m_LanesX.Clear()
                Me.m_LanesY.Clear()
                Me.m_Disables.Clear()
            Else
                Me.m_ClassroomID = dgvList.SelectedRows(0).Cells("colID").Value
                GetItem()
            End If
            DrawSeatGrid()
        End If
    End Sub

    Private Sub nud_Enter(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudColumns.Enter, nudRows.Enter
        sender.Select(0, sender.Value.ToString().Length)
    End Sub

    Private Sub nud_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles nudColumns.ValueChanged, nudRows.ValueChanged
        DrawSeatGrid()
    End Sub

    Private Sub DrawSeatGrid()
        Dim columns As Integer = nudColumns.Value
        Dim rows As Integer = nudRows.Value

        Me.m_Seats = columns * rows - Me.m_Disables.Count
        lblSeats.Text = String.Format("座位數：{0}", Me.m_Seats)

        dgvSeat.Rows.Clear()
        dgvSeat.Columns.Clear()

        If columns = 0 Or rows = 0 Then
            Exit Sub
        End If

        dgvSeat.RowCount = rows
        dgvSeat.ColumnCount = columns

        Dim seatsize As Integer = Math.Floor((dgvSeat.Width - dgvSeat.RowHeadersWidth) / (columns + Me.m_LanesX.Count / 2 + 1))
        If seatsize < 20 Then
            seatsize = 20
        End If

        For i As Integer = 0 To rows - 1
            dgvSeat.Rows(i).HeaderCell.Value = (i + 1).ToString()
            dgvSeat.Rows(i).HeaderCell.Tag = i
            dgvSeat.Rows(i).Height = seatsize
        Next

        For i As Integer = 0 To columns - 1
            dgvSeat.Columns(i).HeaderText = CSOL.Convert.GetAlphabet(i)
            dgvSeat.Columns(i).HeaderCell.Tag = i
            dgvSeat.Columns(i).Width = seatsize
        Next

        Dim RemovedDisableSeats As New List(Of Point)
        For Each p As Point In Me.m_Disables
            If p.X < columns And p.Y < rows Then
                dgvSeat.Rows(p.Y).Cells(p.X).Style.BackColor = lblDisableSeat.BackColor
            Else
                RemovedDisableSeats.Add(p)
            End If
        Next

        For Each p As Point In RemovedDisableSeats
            Me.m_Disables.Remove(p)
        Next

        Me.m_LanesX.Sort()
        Me.m_LanesX.Reverse()
        For Each x As Integer In Me.m_LanesX
            Dim column As New DataGridViewColumn()
            column.CellTemplate = dgvSeat.Rows(1).Cells(1).Clone()
            column.CellTemplate.Style.BackColor = lblLane.BackColor
            column.Width = seatsize / 2

            dgvSeat.Columns.Insert(x, column)
        Next

        Me.m_LanesY.Sort()
        Me.m_LanesY.Reverse()
        For Each y As Integer In Me.m_LanesY
            Dim row As New DataGridViewRow()
            row.Height = seatsize / 2
            row.DefaultCellStyle.BackColor = lblLane.BackColor

            dgvSeat.Rows.Insert(y, row)
        Next
    End Sub

    Private Sub btnColumnLane_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnColumnLane.Click
        For Each col As DataGridViewColumn In dgvSeat.SelectedColumns
            If col.HeaderCell.Tag IsNot Nothing Then
                Dim index As Integer = col.HeaderCell.Tag
                If Not Me.m_LanesX.Contains(index) Then
                    Me.m_LanesX.Add(index)
                End If
            End If
        Next

        For Each cell As DataGridViewCell In dgvSeat.SelectedCells
            Dim col As DataGridViewColumn = dgvSeat.Columns(cell.ColumnIndex)
            If col.HeaderCell.Tag IsNot Nothing Then
                Dim index As Integer = col.HeaderCell.Tag
                If Not Me.m_LanesX.Contains(index) Then
                    Me.m_LanesX.Add(index)
                End If
            End If
        Next

        DrawSeatGrid()
    End Sub

    Private Sub btnRowLane_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRowLane.Click
        For Each row As DataGridViewRow In dgvSeat.SelectedColumns
            If row.HeaderCell.Tag IsNot Nothing Then
                Dim index As Integer = row.HeaderCell.Tag
                If Not Me.m_LanesY.Contains(index) Then
                    Me.m_LanesY.Add(index)
                End If
            End If
        Next

        For Each cell As DataGridViewCell In dgvSeat.SelectedCells
            Dim row As DataGridViewRow = dgvSeat.Rows(cell.RowIndex)
            If row.HeaderCell.Tag IsNot Nothing Then
                Dim index As Integer = row.HeaderCell.Tag
                If Not Me.m_LanesY.Contains(index) Then
                    Me.m_LanesY.Add(index)
                End If
            End If
        Next

        DrawSeatGrid()
    End Sub

    Private Sub btnNotLane_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNotLane.Click
        For Each col As DataGridViewColumn In dgvSeat.SelectedColumns
            If col.HeaderCell.Tag Is Nothing Then
                Dim index As Integer = dgvSeat.Columns(col.Index + 1).HeaderCell.Tag
                If Me.m_LanesX.Contains(index) Then
                    Me.m_LanesX.Remove(index)
                End If
            End If
        Next

        For Each row As DataGridViewRow In dgvSeat.SelectedRows
            If row.HeaderCell.Tag Is Nothing Then
                Dim index As Integer = dgvSeat.Rows(row.Index + 1).HeaderCell.Tag
                If Me.m_LanesY.Contains(index) Then
                    Me.m_LanesY.Remove(index)
                End If
            End If
        Next

        For Each cell As DataGridViewCell In dgvSeat.SelectedCells
            Dim col As DataGridViewColumn = dgvSeat.Columns(cell.ColumnIndex)
            Dim row As DataGridViewRow = dgvSeat.Rows(cell.RowIndex)

            If col.HeaderCell.Tag Is Nothing Then
                Dim index As Integer = dgvSeat.Columns(col.Index + 1).HeaderCell.Tag
                If Me.m_LanesX.Contains(index) Then
                    Me.m_LanesX.Remove(index)
                End If
            End If

            If row.HeaderCell.Tag Is Nothing Then
                Dim index As Integer = dgvSeat.Rows(row.Index + 1).HeaderCell.Tag
                If Me.m_LanesY.Contains(index) Then
                    Me.m_LanesY.Remove(index)
                End If
            End If
        Next

        DrawSeatGrid()
    End Sub

    Private Sub btnSeat_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSeat.Click
        Dim cells As DataGridViewSelectedCellCollection = dgvSeat.SelectedCells
        For Each cell As DataGridViewCell In cells
            Dim x As Integer = dgvSeat.Columns(cell.ColumnIndex).HeaderCell.Tag
            Dim y As Integer = dgvSeat.Rows(cell.RowIndex).HeaderCell.Tag
            If x > -1 And y > -1 Then
                Dim p As New Point(x, y)
                If Me.m_Disables.Contains(p) Then
                    Me.m_Disables.Remove(p)
                End If
            End If
        Next
        DrawSeatGrid()
    End Sub

    Private Sub btnDisable_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDisable.Click
        Dim cells As DataGridViewSelectedCellCollection = dgvSeat.SelectedCells
        For Each cell As DataGridViewCell In cells
            Dim x As Integer = IIf(dgvSeat.Columns(cell.ColumnIndex).HeaderCell.Tag Is Nothing, -1, dgvSeat.Columns(cell.ColumnIndex).HeaderCell.Tag)
            Dim y As Integer = IIf(dgvSeat.Rows(cell.RowIndex).HeaderCell.Tag Is Nothing, -1, dgvSeat.Rows(cell.RowIndex).HeaderCell.Tag)
            If x > -1 And y > -1 Then
                Dim p As New Point(x, y)
                If Not Me.m_Disables.Contains(p) Then
                    Me.m_Disables.Add(p)
                End If
            End If
        Next
        DrawSeatGrid()
    End Sub

    Private Function IsNameExists(ByVal ID As Integer, ByVal Name As String) As Boolean
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", ID)
        RequestParams.Add("Name", Name)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "IsNameExists", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Return ResponseParams("IsNameExists")
    End Function

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("請輸入教室名稱...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        If IsNameExists(-1, tbName.Text) Then
            MessageBox.Show("教室名稱重覆了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", -1)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Columns", nudColumns.Value)
        RequestParams.Add("Rows", nudRows.Value)

        Dim Lanes As String = String.Join(",", Array.ConvertAll(Of Integer, String)(Me.m_LanesX.ToArray(), AddressOf Convert.ToString))
        Lanes &= ";"
        Lanes &= String.Join(",", Array.ConvertAll(Of Integer, String)(Me.m_LanesY.ToArray(), AddressOf Convert.ToString))

        RequestParams.Add("Lanes", Lanes)
        Dim disables As String = ""
        If Me.m_Disables.Count > 0 Then
            disables = String.Join(";", Array.ConvertAll(Of System.Drawing.Point, String)(Me.m_Disables.ToArray(), AddressOf Convert.ToString))
        End If
        RequestParams.Add("Disables", CSOL.Convert.PointsToString(Me.m_Disables.ToArray()))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "SaveItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            GetList()
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("請輸入教室名稱...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        If IsNameExists(Me.m_ClassroomID, tbName.Text) Then
            MessageBox.Show("教室名稱重覆了...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Return
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("ID", Me.m_ClassroomID)
        RequestParams.Add("Name", tbName.Text)
        RequestParams.Add("Columns", nudColumns.Value)
        RequestParams.Add("Rows", nudRows.Value)

        Dim Lanes As String = String.Join(",", Array.ConvertAll(Of Integer, String)(Me.m_LanesX.ToArray(), AddressOf Convert.ToString))
        Lanes &= ";"
        Lanes &= String.Join(",", Array.ConvertAll(Of Integer, String)(Me.m_LanesY.ToArray(), AddressOf Convert.ToString))

        RequestParams.Add("Lanes", Lanes)
        Dim disables As String = ""
        If Me.m_Disables.Count > 0 Then
            disables = String.Join(";", Array.ConvertAll(Of System.Drawing.Point, String)(Me.m_Disables.ToArray(), AddressOf Convert.ToString))
        End If
        RequestParams.Add("Disables", CSOL.Convert.PointsToString(Me.m_Disables.ToArray()))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "SaveItem", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            GetList()
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
        End If
    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        If dgvList.SelectedRows.Count > 0 Then
            If dgvList.SelectedRows(0).IsNewRow Then
                Me.m_ClassroomID = -1
                tbName.ResetText()
                nudColumns.Value = 0
                nudRows.Value = 0
                Me.m_LanesX.Clear()
                Me.m_LanesY.Clear()
                Me.m_Disables.Clear()
            Else
                Me.m_ClassroomID = dgvList.SelectedRows(0).Cells("colID").Value
                GetItem()
            End If
            DrawSeatGrid()
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If Me.m_ClassroomID > 0 Then
            If MessageBox.Show(String.Format("刪除教室可能會讓使用這個教室的班級及學生沒教室可以用...{0}要繼續嗎??", vbCrLf), "警告...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
                RequestParams.Add("ID", Me.m_ClassroomID)

                Dim ResponseBody As String = CSOL.HTTPClient.Post("Classroom", "RemoveItem", RequestParams, System.Text.Encoding.UTF8)
                Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
                If ResponseParams("Status") = "OK" Then
                    MessageBox.Show("刪除成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    GetList()
                Else
                    MessageBox.Show("刪除失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
                    tbName.Select()
                End If
            End If
        End If
    End Sub

    Private Sub dgvSeat_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvSeat.SizeChanged
        DrawSeatGrid()
    End Sub
End Class