﻿Imports System.Text.RegularExpressions

Public Class frmSMS
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private Const c_Rows As Integer = 100
    Private m_CurrentPage As Integer = 1
    Private m_TotalPage As Integer = 1
    Private m_StuCount As Integer = 0

    Private Sub frmSMS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgvStudent.AutoGenerateColumns = False

        GetClassNames()
        GetStuList()

        cbVar.SelectedIndex = 0
        tbShortMessage.Focus()
    End Sub

    Private m_ClassNames As DataTable
    Private m_StuList As DataTable
    Private Sub GetClassNames()
        Dim Response As String = CSOL.HTTPClient.Post("StuInfo", "GetClassNames", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Me.m_ClassNames = CSOL.Convert.XmlStringToDataTable(ResponseParams("ClassNames"))

        Dim row As DataRow = Me.m_ClassNames.NewRow()
        row("ID") = -1
        row("Name") = "所有班級"
        Me.m_ClassNames.Rows.InsertAt(row, 0)

        With cbClass
            .DataSource = Me.m_ClassNames
            .SelectedIndex = 0
        End With
    End Sub

    Private Sub GetStuList_Trigger(ByVal sender As Object, ByVal e As EventArgs) Handles cbClass.SelectedIndexChanged, tbStuNo.TextChanged, tbStuName.TextChanged
        GetStuList()
    End Sub

    Private Sub GetStuList()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        If cbClass.SelectedIndex = 0 Then
            RequestParams.Add("StuClass", "")
        Else
            RequestParams.Add("StuClass", cbClass.Text)
        End If
        RequestParams.Add("StuNo", tbStuNo.Text.Trim())
        RequestParams.Add("StuName", tbStuName.Text.Trim())
        RequestParams.Add("Rows", frmSMS.c_Rows)
        RequestParams.Add("Page", Me.m_CurrentPage)
        RequestParams.Add("NonBlankMobile", cbShowNonBlankMobile.Checked)

        Dim Response As String = CSOL.HTTPClient.Post("SMS", "GetStuList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        Me.m_StuCount = ResponseParams("StuCount")
        Me.m_TotalPage = Math.Ceiling(Me.m_StuCount / frmSMS.c_Rows)
        If Me.m_TotalPage = 0 Then
            Me.m_TotalPage = 1
        End If
        If Me.m_StuCount < 0 Then
            Me.m_StuCount = 0
        End If

        If Me.m_CurrentPage > Me.m_TotalPage Then
            Me.m_CurrentPage = Me.m_TotalPage
            GetStuList()
            Exit Sub
        End If

        Me.m_StuList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        Me.m_StuList.Columns.Add("Seat", GetType(String))
        For Each row As DataRow In Me.m_StuList.Rows
            row("Seat") = String.Format("{0} {1}{2}", row("ClassroomName"), CSOL.Convert.GetAlphabet(row("X")), CSOL.Convert.GetNumeric(row("Y")))
        Next
        dgvStudent.DataSource = Me.m_StuList

        lblRowCount.Text = String.Format("{0}/{1}，共{2}筆", Me.m_CurrentPage, Me.m_TotalPage, Me.m_StuCount)
    End Sub

    Private Sub dgvShortMessage_RowsAdded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowsAddedEventArgs) Handles dgvShortMessage.RowsAdded
        dgvShortMessage.Rows(e.RowIndex).Cells(0).Value = True
    End Sub

    Private Sub dgvShortMessage_CellEndEdit(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvShortMessage.CellEndEdit
        If e.ColumnIndex = dgvShortMessage.Columns("Mobile").Index Then
            Dim mobile As String = New String(dgvShortMessage.Rows(e.RowIndex).Cells("Mobile").Value)
            If IsValidMobile(mobile) Then
                dgvShortMessage.Rows(e.RowIndex).Cells(e.ColumnIndex).ErrorText = ""
            Else
                If dgvShortMessage.Rows(e.RowIndex).IsNewRow Then
                    Return
                End If

                dgvShortMessage.Rows(e.RowIndex).Cells(e.ColumnIndex).ErrorText = "這不是一個手機號碼，將不會被發送..."
            End If
        End If

        If e.ColumnIndex = dgvShortMessage.Columns("ShortMessage").Index Then
            Dim text As String = New String(dgvShortMessage.Rows(e.RowIndex).Cells("ShortMessage").Value)
            dgvShortMessage.Rows(e.RowIndex).Cells("ShortMessageWordCount").Value = text.Length
        End If

        RefreshShortMessageCount()
    End Sub

    Private Sub btnInsertVar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInsertVar.Click
        Dim position As Integer = tbShortMessage.SelectionStart
        Dim leftPart As String = tbShortMessage.Text.Substring(0, position)
        Dim rightPart As String = tbShortMessage.Text.Substring(position, tbShortMessage.TextLength - position)
        Dim text As String = ""

        Select Case cbVar.SelectedItem.ToString()
            Case "班級", "學號", "姓名"
                text = String.Format("[{0}]", cbVar.SelectedItem.ToString())
            Case "今天日期"
                text = Now.ToString("yyyy-M-d")
            Case "現在時間"
                text = Now.ToString("H:m:s")
        End Select

        tbShortMessage.Text = leftPart & text & rightPart
        position += text.Length
        tbShortMessage.SelectionStart = position
        tbShortMessage.Focus()
    End Sub

    Private Sub btnFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnFirst.Click
        If Me.m_CurrentPage > 1 Then
            Me.m_CurrentPage = 1
            GetStuList()
        End If
    End Sub

    Private Sub btnPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrev.Click
        If Me.m_CurrentPage > 1 Then
            Me.m_CurrentPage -= 1
            GetStuList()
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click
        If Me.m_CurrentPage < Me.m_TotalPage Then
            Me.m_CurrentPage += 1
            GetStuList()
        End If
    End Sub

    Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
        If Me.m_CurrentPage < Me.m_TotalPage Then
            Me.m_CurrentPage = Me.m_TotalPage
            GetStuList()
        End If
    End Sub

    Private Sub SplitContainer1_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SplitContainer1.DoubleClick
        SplitContainer1.Panel1Collapsed = Not SplitContainer1.Panel1Collapsed
    End Sub

    Private Sub pSlider_MouseDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pSlider.MouseDown
        Dim p As Panel = sender
        p.Tag = e.X
    End Sub

    Private Sub pSlider_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pSlider.MouseMove
        Dim p As Panel = sender
        If p.Tag IsNot Nothing Then
            Dim offset As Integer = e.X - CInt(p.Tag)
            If (SplitContainer1.SplitterDistance + offset) > 0 Then
                SplitContainer1.SplitterDistance += offset
            End If
        End If
    End Sub

    Private Sub pSlider_MouseUp(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles pSlider.MouseUp
        Dim p As Panel = sender
        p.Tag = Nothing
    End Sub

    Private Sub pSlider_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles pSlider.DoubleClick
        SplitContainer1.Panel1Collapsed = Not SplitContainer1.Panel1Collapsed
    End Sub

    Private Sub cbShowNonBlankMobile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbShowNonBlankMobile.CheckedChanged
        GetStuList()
    End Sub

    Private Sub tbShortMessage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tbShortMessage.TextChanged
        lblWordCount.Text = String.Format("字數：{0}", tbShortMessage.TextLength)
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If tbShortMessage.TextLength = 0 Then
            MessageBox.Show("您還沒輸入簡訊範本...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim shortMessage As String = tbShortMessage.Text
            For i As Integer = 0 To dgvStudent.Rows.Count - 1
                Dim checked As Boolean = dgvStudent.Rows(i).Cells("PreCheck").Value
                If checked Then
                    Dim row As DataRow = dgvStudent.Rows(i).DataBoundItem.Row
                    Dim mobile As String = New String(row("Mobile"))
                    Dim stuClass As String = New String(row("ClassName"))
                    Dim stuNo As String = New String(row("No"))
                    Dim stuName As String = New String(row("Name"))

                    Dim sm As String = shortMessage.Replace("[班級]", stuClass).Replace("[學號]", stuNo).Replace("[姓名]", stuName)

                    Dim rowIndex As Integer = dgvShortMessage.Rows.Add(True, stuName, mobile, sm, sm.Length)
                    If Not IsValidMobile(mobile) Then
                        dgvShortMessage.Rows(rowIndex).Cells("Mobile").ErrorText = "這不是一個手機號碼，將不會被發送..."
                    End If
                    dgvStudent.Rows(i).Cells("PreCheck").Value = False
                    RefreshShortMessageCount()
                End If
            Next
        End If
    End Sub

    Private Sub btnRemove_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        If MessageBox.Show("將會刪除所有已勾選的項目，要繼續嗎??", "注意...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
            Dim index As Integer = 0
            While index < dgvShortMessage.Rows.Count - 1
                Dim row As DataGridViewRow = dgvShortMessage.Rows(index)
                If row.Cells("Check").Value = True Then
                    If row.IsNewRow Then
                        index += 1
                        Continue While
                    End If
                    dgvShortMessage.Rows.Remove(row)
                Else
                    index += 1
                End If
            End While

            RefreshShortMessageCount()
        End If
    End Sub

    Private Sub RefreshShortMessageCount()
        If dgvShortMessage.AllowUserToAddRows Then
            lblShortMessageCount.Text = String.Format("共{0}筆", dgvShortMessage.Rows.Count - 1)
        Else
            lblShortMessageCount.Text = String.Format("共{0}筆", dgvShortMessage.Rows.Count)
        End If
    End Sub

    Private Sub tbShortMessage_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles tbShortMessage.KeyDown
        If e.Control And e.KeyCode = Keys.A Then
            tbShortMessage.SelectAll()
        End If
    End Sub

    Private Function IsValidMobile(ByVal mobile As String) As Boolean
        Return Regex.IsMatch(mobile, "^(\+8869|09)\d{8}$")
    End Function

    Private Sub btnSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectAll.Click
        Dim index As Integer = 0
        For i As Integer = index To dgvStudent.Rows.Count - 1
            dgvStudent.Rows(i).Cells("PreCheck").Value = True
        Next
    End Sub

    Private Sub btnClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearAll.Click
        Dim index As Integer = 0
        For i As Integer = index To dgvStudent.Rows.Count - 1
            dgvStudent.Rows(i).Cells("PreCheck").Value = False
        Next
    End Sub

    Private Sub btnSelectBelow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSelectBelow.Click
        Dim index As Integer = 0
        If dgvStudent.SelectedRows.Count > 0 Then
            index = dgvStudent.SelectedRows(0).Index
        End If

        For i As Integer = index To dgvStudent.Rows.Count - 1
            dgvStudent.Rows(i).Cells("PreCheck").Value = True
        Next
    End Sub

    Private Sub btnClearBelow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClearBelow.Click
        Dim index As Integer = 0
        If dgvStudent.SelectedRows.Count > 0 Then
            index = dgvStudent.SelectedRows(0).Index
        End If

        For i As Integer = index To dgvStudent.Rows.Count - 1
            dgvStudent.Rows(i).Cells("PreCheck").Value = False
        Next
    End Sub

    Private Sub btnSMSelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMSelectAll.Click
        Dim index As Integer = 0
        For i As Integer = index To dgvShortMessage.Rows.Count - 1
            dgvShortMessage.Rows(i).Cells("Check").Value = True
        Next
    End Sub

    Private Sub btnSMClearAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMClearAll.Click
        Dim index As Integer = 0
        For i As Integer = index To dgvShortMessage.Rows.Count - 1
            dgvShortMessage.Rows(i).Cells("Check").Value = False
        Next
    End Sub

    Private Sub btnSMSelectBelow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMSelectBelow.Click
        Dim index As Integer = 0
        If dgvShortMessage.SelectedRows.Count > 0 Then
            index = dgvShortMessage.SelectedRows(0).Index
        End If

        For i As Integer = index To dgvShortMessage.Rows.Count - 1
            dgvShortMessage.Rows(i).Cells("Check").Value = True
        Next
    End Sub

    Private Sub btnSMClearBelow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSMClearBelow.Click
        Dim index As Integer = 0
        If dgvShortMessage.SelectedRows.Count > 0 Then
            index = dgvShortMessage.SelectedRows(0).Index
        End If

        For i As Integer = index To dgvShortMessage.Rows.Count - 1
            dgvShortMessage.Rows(i).Cells("Check").Value = False
        Next
    End Sub

    Private Sub btnImport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImport.Click
        Dim ofd As New OpenFileDialog()
        ofd.FileName = "sms.xls"
        ofd.Filter = "Excel檔案|*.xls"
        If ofd.ShowDialog() = Windows.Forms.DialogResult.OK Then
            For Each filename As String In ofd.FileNames
                Try
                    Dim workbook As New NPOI.HSSF.UserModel.HSSFWorkbook(New System.IO.FileStream(filename, IO.FileMode.Open))
                    Dim ds As DataSet = Ex.Excel.HSSFWorkbookToDataSet(workbook)
                    If ds.Tables.Count = 0 Then
                        MessageBox.Show("無任何可匯入資料...", "訊息", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        Dim counter As Integer = 0
                        For Each row As DataRow In ds.Tables(0).Rows
                            If row.ItemArray.Length < 2 Then
                                Continue For
                            End If

                            Dim items As New List(Of Object)
                            items.Add(False)
                            items.Add("")
                            items.Add(row.ItemArray(0).ToString())
                            items.Add(row.ItemArray(1).ToString())
                            items.Add(row.ItemArray(1).ToString().Length)

                            Dim rowIndex As Integer = dgvShortMessage.Rows.Add(items.ToArray())
                            Dim mobile As String = row.ItemArray(0).ToString()
                            If Not IsValidMobile(mobile) Then
                                dgvShortMessage.Rows(rowIndex).Cells("Mobile").ErrorText = "這不是一個手機號碼，將不會被發送..."
                            End If

                            counter += 1
                        Next

                        MessageBox.Show(String.Format("已成功匯入{0}筆資料...", counter), "訊息", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message, "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            Next
        End If

        RefreshShortMessageCount()
    End Sub

    Private Sub btnSaveExcelTemplate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveExcelTemplate.Click
        Dim sfd As New SaveFileDialog()
        sfd.FileName = "sms.xls"
        sfd.DefaultExt = ".xls"
        sfd.Filter = "Excel檔案|*.xls"
        If sfd.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Try
                System.IO.File.Copy(String.Format("{0}\Templates\sms.xls", My.Application.Info.DirectoryPath), sfd.FileName, True)
            Catch ex As Exception
                MessageBox.Show(ex.Message, "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try
        End If
    End Sub

    Private Sub btnHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnHistory.Click
        Dim dlg As New dlgSMSMonitoring(0)
        dlg.ShowDialog()
    End Sub

    Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim SMSList As New DataTable("SMSList")
        SMSList.Columns.Add("Index", GetType(Integer))
        SMSList.Columns.Add("Mobile", GetType(String))
        SMSList.Columns.Add("Message", GetType(String))

        For i As Integer = 0 To dgvShortMessage.Rows.Count - 1
            Dim row As DataGridViewRow = dgvShortMessage.Rows(i)
            If row.IsNewRow Then
                Continue For
            End If

            If row.Cells("Check").Value = True Then
                Dim mobile As String = row.Cells("Mobile").Value
                Dim message As String = row.Cells("ShortMessage").Value
                If IsValidMobile(mobile) And Not String.IsNullOrEmpty(message) Then
                    SMSList.Rows.Add(i, mobile, message)
                    row.Cells("Check").Value = False
                End If
            End If
        Next

        If SMSList.Rows.Count = 0 Then
            MessageBox.Show("沒有簡訊可以發送...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        Else
            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("SMSList", CSOL.Convert.DataTableToXmlString(SMSList))

            Dim Response As String = CSOL.HTTPClient.Post("SMS", "SaveSMSList", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
            If ResponseParams("Status") = "OK" Then
                MessageBox.Show("儲存成功，正在開始發送...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
                For Each row As DataRow In SMSList.Select("", "Index DESC")
                    dgvShortMessage.Rows.RemoveAt(row("Index"))
                Next

                Dim BatchID As Integer = ResponseParams("BatchID")
                Dim dlg As New dlgSMSMonitoring(BatchID)
                dlg.ShowDialog()
            Else
                MessageBox.Show("儲存失敗，請再試一次...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End If
    End Sub

    Private Sub dgvStudent_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvStudent.MouseDoubleClick
        Dim ht As DataGridView.HitTestInfo = dgvStudent.HitTest(e.X, e.Y)
        Dim cell As DataGridViewCell = dgvStudent.Rows(ht.RowIndex).Cells("PreCheck")
        If cell.GetType().Equals(GetType(DataGridViewCheckBoxCell)) Then
            cell.Value = Not cell.Value
        End If
    End Sub

    Private Sub dgvStudent_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvStudent.KeyDown
        If e.KeyCode = Keys.Space Then
            If dgvStudent.CurrentRow IsNot Nothing Then
                dgvStudent.CurrentRow.Cells("PreCheck").Value = Not dgvStudent.CurrentRow.Cells("PreCheck").Value
            End If
        End If
    End Sub
End Class