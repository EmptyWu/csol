﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class dlgSMSMonitoring
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnCancel = New System.Windows.Forms.Button
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.dgvShortMessage = New System.Windows.Forms.DataGridView
        Me.SMSBatchID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.SMSID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Mobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Message = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Status = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CreateDate = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Handler = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.WorkStation = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Department = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel2.SuspendLayout()
        CType(Me.dgvShortMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnClose
        '
        Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnClose.Location = New System.Drawing.Point(684, 9)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(88, 21)
        Me.btnClose.TabIndex = 1
        Me.btnClose.Text = "關閉監看視窗"
        '
        'btnCancel
        '
        Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCancel.Location = New System.Drawing.Point(543, 9)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(135, 21)
        Me.btnCancel.TabIndex = 0
        Me.btnCancel.Text = "中止目前簡訊發送"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Controls.Add(Me.btnClose)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 420)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(784, 42)
        Me.Panel2.TabIndex = 1
        '
        'dgvShortMessage
        '
        Me.dgvShortMessage.AllowUserToAddRows = False
        Me.dgvShortMessage.AllowUserToDeleteRows = False
        Me.dgvShortMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvShortMessage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SMSBatchID, Me.SMSID, Me.Mobile, Me.Message, Me.Status, Me.CreateDate, Me.Handler, Me.WorkStation, Me.Department})
        Me.dgvShortMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvShortMessage.Location = New System.Drawing.Point(0, 0)
        Me.dgvShortMessage.Name = "dgvShortMessage"
        Me.dgvShortMessage.ReadOnly = True
        Me.dgvShortMessage.RowTemplate.Height = 24
        Me.dgvShortMessage.Size = New System.Drawing.Size(784, 420)
        Me.dgvShortMessage.TabIndex = 0
        '
        'SMSBatchID
        '
        Me.SMSBatchID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SMSBatchID.HeaderText = "批次序號"
        Me.SMSBatchID.Name = "SMSBatchID"
        Me.SMSBatchID.ReadOnly = True
        Me.SMSBatchID.Width = 78
        '
        'SMSID
        '
        Me.SMSID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.SMSID.HeaderText = "簡訊序號"
        Me.SMSID.Name = "SMSID"
        Me.SMSID.ReadOnly = True
        Me.SMSID.Width = 78
        '
        'Mobile
        '
        Me.Mobile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Mobile.HeaderText = "手機"
        Me.Mobile.Name = "Mobile"
        Me.Mobile.ReadOnly = True
        Me.Mobile.Width = 54
        '
        'Message
        '
        Me.Message.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.Message.HeaderText = "簡訊內容"
        Me.Message.Name = "Message"
        Me.Message.ReadOnly = True
        '
        'Status
        '
        Me.Status.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Status.HeaderText = "狀態"
        Me.Status.Name = "Status"
        Me.Status.ReadOnly = True
        Me.Status.Width = 54
        '
        'CreateDate
        '
        Me.CreateDate.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Format = "yyyy-MM-dd HH:mm:ss"
        Me.CreateDate.DefaultCellStyle = DataGridViewCellStyle2
        Me.CreateDate.HeaderText = "建立時間"
        Me.CreateDate.Name = "CreateDate"
        Me.CreateDate.ReadOnly = True
        Me.CreateDate.Width = 78
        '
        'Handler
        '
        Me.Handler.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Handler.HeaderText = "建立者"
        Me.Handler.Name = "Handler"
        Me.Handler.ReadOnly = True
        Me.Handler.Width = 66
        '
        'WorkStation
        '
        Me.WorkStation.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.WorkStation.HeaderText = "工作站"
        Me.WorkStation.Name = "WorkStation"
        Me.WorkStation.ReadOnly = True
        Me.WorkStation.Width = 66
        '
        'Department
        '
        Me.Department.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Department.HeaderText = "分部"
        Me.Department.Name = "Department"
        Me.Department.ReadOnly = True
        Me.Department.Width = 54
        '
        'dlgSMSMonitoring
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 462)
        Me.Controls.Add(Me.dgvShortMessage)
        Me.Controls.Add(Me.Panel2)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "dlgSMSMonitoring"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "簡訊發送狀況"
        Me.Panel2.ResumeLayout(False)
        CType(Me.dgvShortMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents dgvShortMessage As System.Windows.Forms.DataGridView
    Friend WithEvents SMSBatchID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SMSID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Message As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Status As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CreateDate As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Handler As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents WorkStation As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Department As System.Windows.Forms.DataGridViewTextBoxColumn

End Class
