﻿Imports System.Windows.Forms

Public Class dlgSMSMonitoring
    Private m_BatchID As Integer
    Public Property BatchID() As Integer
        Get
            Return Me.m_BatchID
        End Get
        Set(ByVal value As Integer)
            Me.m_BatchID = value
        End Set
    End Property

    Public Sub New(ByVal BatchID As Integer)

        ' 此為 Windows Form 設計工具所需的呼叫。
        InitializeComponent()

        ' 在 InitializeComponent() 呼叫之後加入任何初始設定。
        Me.m_BatchID = BatchID
    End Sub

    Private Sub dlgSMSMonitoring_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadSMSList(Nothing, Nothing)

        Dim timer As New Timer()
        timer.Interval = 3000
        AddHandler timer.Tick, AddressOf LoadSMSList
        timer.Start()

        btnClose.Focus()
    End Sub

    Private Sub LoadSMSList(ByVal sender As Object, ByVal e As EventArgs)
        Dim timer As Timer = sender
        If timer IsNot Nothing Then
            timer.Stop()
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("BatchID", Me.m_BatchID)

        Dim Response As String = CSOL.HTTPClient.Post("SMS", "GetSMSList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
        If ResponseParams("Status") = "OK" Then
            dgvShortMessage.Rows.Clear()
            Dim SMSList As DataTable = CSOL.Convert.XmlStringToDataTable(ResponseParams("SMSList"))
            For Each row As DataRow In SMSList.Rows
                dgvShortMessage.Rows.Add(row.ItemArray)
            Next

            Try
                dgvShortMessage.Sort(dgvShortMessage.SortedColumn, dgvShortMessage.SortOrder)
            Catch ex As Exception

            End Try
        End If

        If timer IsNot Nothing Then
            timer.Start()
        End If
    End Sub

    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        If MessageBox.Show("您確定要取消目前看到尚未傳送的項目??", "問題...", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Dim SMSList As New DataTable("SMSList")
            SMSList.Columns.Add("BatchID", GetType(Integer))
            SMSList.Columns.Add("SMSID", GetType(Integer))

            For Each row As DataGridViewRow In dgvShortMessage.Rows
                SMSList.Rows.Add(row.Cells("SMSBatchID").Value, row.Cells("SMSID").Value)
            Next

            Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
            RequestParams.Add("SMSList", CSOL.Convert.DataTableToXmlString(SMSList))

            Dim Response As String = CSOL.HTTPClient.Post("SMS", "CancelSMSList", RequestParams, System.Text.Encoding.UTF8)
            Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(Response)
            If ResponseParams("Status") = "OK" Then
                MessageBox.Show("取消成功，目前所有尚未傳送項目都被取消了...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Else
                MessageBox.Show("取消失敗，請再試一次...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            End If
        End If
    End Sub

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Me.Close()
    End Sub
End Class
