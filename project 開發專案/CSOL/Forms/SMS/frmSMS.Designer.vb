﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSMS
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSMS))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.tbShortMessage = New System.Windows.Forms.TextBox
        Me.Panel3 = New System.Windows.Forms.Panel
        Me.lblWordCount = New System.Windows.Forms.Label
        Me.btnInsertVar = New System.Windows.Forms.Button
        Me.cbVar = New System.Windows.Forms.ComboBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.Panel5 = New System.Windows.Forms.Panel
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnSelectAll = New System.Windows.Forms.Button
        Me.btnClearBelow = New System.Windows.Forms.Button
        Me.btnSelectBelow = New System.Windows.Forms.Button
        Me.btnClearAll = New System.Windows.Forms.Button
        Me.cbShowNonBlankMobile = New System.Windows.Forms.CheckBox
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel
        Me.btnFirst = New System.Windows.Forms.Button
        Me.btnPrev = New System.Windows.Forms.Button
        Me.btnNext = New System.Windows.Forms.Button
        Me.btnLast = New System.Windows.Forms.Button
        Me.lblRowCount = New System.Windows.Forms.Label
        Me.tbStuName = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.tbStuNo = New System.Windows.Forms.TextBox
        Me.Label3 = New System.Windows.Forms.Label
        Me.cbClass = New System.Windows.Forms.ComboBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.dgvStudent = New System.Windows.Forms.DataGridView
        Me.PreCheck = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.RowID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PreStuNo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PreStuName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PreMobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.GroupBox3 = New System.Windows.Forms.GroupBox
        Me.lblShortMessageCount = New System.Windows.Forms.Label
        Me.dgvShortMessage = New System.Windows.Forms.DataGridView
        Me.Check = New System.Windows.Forms.DataGridViewCheckBoxColumn
        Me.StuName = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Mobile = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ShortMessage = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.ShortMessageWordCount = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.Panel7 = New System.Windows.Forms.Panel
        Me.btnSMSelectAll = New System.Windows.Forms.Button
        Me.btnSMClearBelow = New System.Windows.Forms.Button
        Me.btnSMSelectBelow = New System.Windows.Forms.Button
        Me.btnSMClearAll = New System.Windows.Forms.Button
        Me.btnSaveExcelTemplate = New System.Windows.Forms.Button
        Me.btnImport = New System.Windows.Forms.Button
        Me.btnHistory = New System.Windows.Forms.Button
        Me.btnSend = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnRemove = New System.Windows.Forms.Button
        Me.Panel6 = New System.Windows.Forms.Panel
        Me.Panel4 = New System.Windows.Forms.Panel
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
        Me.pSlider = New System.Windows.Forms.Panel
        Me.GroupBox2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.dgvStudent, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        CType(Me.dgvShortMessage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.tbShortMessage)
        Me.GroupBox2.Controls.Add(Me.Panel3)
        Me.GroupBox2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox2.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(440, 200)
        Me.GroupBox2.TabIndex = 0
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "簡訊內容"
        '
        'tbShortMessage
        '
        Me.tbShortMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbShortMessage.Location = New System.Drawing.Point(3, 18)
        Me.tbShortMessage.Multiline = True
        Me.tbShortMessage.Name = "tbShortMessage"
        Me.tbShortMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tbShortMessage.Size = New System.Drawing.Size(434, 144)
        Me.tbShortMessage.TabIndex = 0
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.lblWordCount)
        Me.Panel3.Controls.Add(Me.btnInsertVar)
        Me.Panel3.Controls.Add(Me.cbVar)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel3.Location = New System.Drawing.Point(3, 162)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(434, 35)
        Me.Panel3.TabIndex = 1
        '
        'lblWordCount
        '
        Me.lblWordCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblWordCount.Font = New System.Drawing.Font("新細明體", 11.0!)
        Me.lblWordCount.Location = New System.Drawing.Point(340, 9)
        Me.lblWordCount.Name = "lblWordCount"
        Me.lblWordCount.Size = New System.Drawing.Size(91, 20)
        Me.lblWordCount.TabIndex = 2
        Me.lblWordCount.Text = "字數：0"
        '
        'btnInsertVar
        '
        Me.btnInsertVar.Location = New System.Drawing.Point(119, 9)
        Me.btnInsertVar.Name = "btnInsertVar"
        Me.btnInsertVar.Size = New System.Drawing.Size(74, 20)
        Me.btnInsertVar.TabIndex = 1
        Me.btnInsertVar.Text = "插入變數"
        Me.btnInsertVar.UseVisualStyleBackColor = True
        '
        'cbVar
        '
        Me.cbVar.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.cbVar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbVar.FormattingEnabled = True
        Me.cbVar.Items.AddRange(New Object() {"班級", "學號", "姓名", "今天日期", "現在時間"})
        Me.cbVar.Location = New System.Drawing.Point(3, 9)
        Me.cbVar.Name = "cbVar"
        Me.cbVar.Size = New System.Drawing.Size(110, 20)
        Me.cbVar.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(440, 258)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "選擇學生"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Panel5, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.dgvStudent, 1, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 18)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(434, 237)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.Panel1)
        Me.Panel5.Controls.Add(Me.cbShowNonBlankMobile)
        Me.Panel5.Controls.Add(Me.FlowLayoutPanel1)
        Me.Panel5.Controls.Add(Me.lblRowCount)
        Me.Panel5.Controls.Add(Me.tbStuName)
        Me.Panel5.Controls.Add(Me.Label4)
        Me.Panel5.Controls.Add(Me.tbStuNo)
        Me.Panel5.Controls.Add(Me.Label3)
        Me.Panel5.Controls.Add(Me.cbClass)
        Me.Panel5.Controls.Add(Me.Label2)
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel5.Location = New System.Drawing.Point(3, 3)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(154, 231)
        Me.Panel5.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel1.Controls.Add(Me.btnSelectAll)
        Me.Panel1.Controls.Add(Me.btnClearBelow)
        Me.Panel1.Controls.Add(Me.btnSelectBelow)
        Me.Panel1.Controls.Add(Me.btnClearAll)
        Me.Panel1.Location = New System.Drawing.Point(5, 128)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(144, 46)
        Me.Panel1.TabIndex = 7
        '
        'btnSelectAll
        '
        Me.btnSelectAll.Location = New System.Drawing.Point(1, 0)
        Me.btnSelectAll.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSelectAll.Name = "btnSelectAll"
        Me.btnSelectAll.Size = New System.Drawing.Size(71, 23)
        Me.btnSelectAll.TabIndex = 6
        Me.btnSelectAll.Text = "選擇全部"
        Me.btnSelectAll.UseVisualStyleBackColor = True
        '
        'btnClearBelow
        '
        Me.btnClearBelow.Location = New System.Drawing.Point(72, 23)
        Me.btnClearBelow.Margin = New System.Windows.Forms.Padding(0)
        Me.btnClearBelow.Name = "btnClearBelow"
        Me.btnClearBelow.Size = New System.Drawing.Size(71, 23)
        Me.btnClearBelow.TabIndex = 6
        Me.btnClearBelow.Text = "取消以下"
        Me.btnClearBelow.UseVisualStyleBackColor = True
        '
        'btnSelectBelow
        '
        Me.btnSelectBelow.Location = New System.Drawing.Point(1, 23)
        Me.btnSelectBelow.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSelectBelow.Name = "btnSelectBelow"
        Me.btnSelectBelow.Size = New System.Drawing.Size(71, 23)
        Me.btnSelectBelow.TabIndex = 6
        Me.btnSelectBelow.Text = "選擇以下"
        Me.btnSelectBelow.UseVisualStyleBackColor = True
        '
        'btnClearAll
        '
        Me.btnClearAll.Location = New System.Drawing.Point(72, 0)
        Me.btnClearAll.Margin = New System.Windows.Forms.Padding(0)
        Me.btnClearAll.Name = "btnClearAll"
        Me.btnClearAll.Size = New System.Drawing.Size(71, 23)
        Me.btnClearAll.TabIndex = 6
        Me.btnClearAll.Text = "取消全部"
        Me.btnClearAll.UseVisualStyleBackColor = True
        '
        'cbShowNonBlankMobile
        '
        Me.cbShowNonBlankMobile.AutoSize = True
        Me.cbShowNonBlankMobile.Location = New System.Drawing.Point(29, 89)
        Me.cbShowNonBlankMobile.Name = "cbShowNonBlankMobile"
        Me.cbShowNonBlankMobile.Size = New System.Drawing.Size(120, 16)
        Me.cbShowNonBlankMobile.TabIndex = 3
        Me.cbShowNonBlankMobile.Text = "僅顯示有手機的人"
        Me.cbShowNonBlankMobile.UseVisualStyleBackColor = True
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FlowLayoutPanel1.AutoSize = True
        Me.FlowLayoutPanel1.Controls.Add(Me.btnFirst)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnPrev)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnNext)
        Me.FlowLayoutPanel1.Controls.Add(Me.btnLast)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(5, 177)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(144, 36)
        Me.FlowLayoutPanel1.TabIndex = 5
        '
        'btnFirst
        '
        Me.btnFirst.Image = CType(resources.GetObject("btnFirst.Image"), System.Drawing.Image)
        Me.btnFirst.Location = New System.Drawing.Point(0, 0)
        Me.btnFirst.Margin = New System.Windows.Forms.Padding(0)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(36, 36)
        Me.btnFirst.TabIndex = 0
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnPrev
        '
        Me.btnPrev.Image = CType(resources.GetObject("btnPrev.Image"), System.Drawing.Image)
        Me.btnPrev.Location = New System.Drawing.Point(36, 0)
        Me.btnPrev.Margin = New System.Windows.Forms.Padding(0)
        Me.btnPrev.Name = "btnPrev"
        Me.btnPrev.Size = New System.Drawing.Size(36, 36)
        Me.btnPrev.TabIndex = 1
        Me.btnPrev.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Image = CType(resources.GetObject("btnNext.Image"), System.Drawing.Image)
        Me.btnNext.Location = New System.Drawing.Point(72, 0)
        Me.btnNext.Margin = New System.Windows.Forms.Padding(0)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(36, 36)
        Me.btnNext.TabIndex = 2
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnLast
        '
        Me.btnLast.Image = CType(resources.GetObject("btnLast.Image"), System.Drawing.Image)
        Me.btnLast.Location = New System.Drawing.Point(108, 0)
        Me.btnLast.Margin = New System.Windows.Forms.Padding(0)
        Me.btnLast.Name = "btnLast"
        Me.btnLast.Size = New System.Drawing.Size(36, 36)
        Me.btnLast.TabIndex = 3
        Me.btnLast.UseVisualStyleBackColor = True
        '
        'lblRowCount
        '
        Me.lblRowCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblRowCount.Font = New System.Drawing.Font("新細明體", 9.0!)
        Me.lblRowCount.Location = New System.Drawing.Point(3, 217)
        Me.lblRowCount.Name = "lblRowCount"
        Me.lblRowCount.Size = New System.Drawing.Size(147, 12)
        Me.lblRowCount.TabIndex = 2
        Me.lblRowCount.Text = "1/1，共0筆"
        '
        'tbStuName
        '
        Me.tbStuName.Location = New System.Drawing.Point(50, 61)
        Me.tbStuName.Name = "tbStuName"
        Me.tbStuName.Size = New System.Drawing.Size(100, 22)
        Me.tbStuName.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 64)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(41, 12)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "姓名："
        '
        'tbStuNo
        '
        Me.tbStuNo.Location = New System.Drawing.Point(50, 33)
        Me.tbStuNo.Name = "tbStuNo"
        Me.tbStuNo.Size = New System.Drawing.Size(100, 22)
        Me.tbStuNo.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 36)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(41, 12)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "學號："
        '
        'cbClass
        '
        Me.cbClass.DisplayMember = "Name"
        Me.cbClass.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbClass.FormattingEnabled = True
        Me.cbClass.Location = New System.Drawing.Point(50, 6)
        Me.cbClass.Name = "cbClass"
        Me.cbClass.Size = New System.Drawing.Size(101, 20)
        Me.cbClass.TabIndex = 0
        Me.cbClass.ValueMember = "ID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 9)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(41, 12)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "班級："
        '
        'dgvStudent
        '
        Me.dgvStudent.AllowUserToAddRows = False
        Me.dgvStudent.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStudent.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvStudent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvStudent.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PreCheck, Me.RowID, Me.PreStuNo, Me.PreStuName, Me.PreMobile, Me.Column1, Me.Column2})
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle3.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvStudent.DefaultCellStyle = DataGridViewCellStyle3
        Me.dgvStudent.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvStudent.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvStudent.Location = New System.Drawing.Point(163, 3)
        Me.dgvStudent.Name = "dgvStudent"
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvStudent.RowHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.dgvStudent.RowTemplate.Height = 24
        Me.dgvStudent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvStudent.Size = New System.Drawing.Size(268, 231)
        Me.dgvStudent.TabIndex = 1
        '
        'PreCheck
        '
        Me.PreCheck.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.PreCheck.HeaderText = ""
        Me.PreCheck.Name = "PreCheck"
        Me.PreCheck.Width = 5
        '
        'RowID
        '
        Me.RowID.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.RowID.DataPropertyName = "RowID"
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.RowID.DefaultCellStyle = DataGridViewCellStyle2
        Me.RowID.HeaderText = "序號"
        Me.RowID.Name = "RowID"
        Me.RowID.ReadOnly = True
        Me.RowID.Width = 54
        '
        'PreStuNo
        '
        Me.PreStuNo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.PreStuNo.DataPropertyName = "No"
        Me.PreStuNo.HeaderText = "學號"
        Me.PreStuNo.Name = "PreStuNo"
        Me.PreStuNo.ReadOnly = True
        Me.PreStuNo.Width = 54
        '
        'PreStuName
        '
        Me.PreStuName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.PreStuName.DataPropertyName = "Name"
        Me.PreStuName.HeaderText = "姓名"
        Me.PreStuName.Name = "PreStuName"
        Me.PreStuName.ReadOnly = True
        Me.PreStuName.Width = 54
        '
        'PreMobile
        '
        Me.PreMobile.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.PreMobile.DataPropertyName = "Mobile"
        Me.PreMobile.HeaderText = "手機"
        Me.PreMobile.Name = "PreMobile"
        Me.PreMobile.ReadOnly = True
        Me.PreMobile.Width = 54
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ClassName"
        Me.Column1.HeaderText = "班級"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "Seat"
        Me.Column2.HeaderText = "座號"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblShortMessageCount)
        Me.GroupBox3.Controls.Add(Me.dgvShortMessage)
        Me.GroupBox3.Controls.Add(Me.Panel2)
        Me.GroupBox3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox3.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(503, 462)
        Me.GroupBox3.TabIndex = 1
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "簡訊預覽"
        '
        'lblShortMessageCount
        '
        Me.lblShortMessageCount.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblShortMessageCount.AutoSize = True
        Me.lblShortMessageCount.ForeColor = System.Drawing.Color.Tomato
        Me.lblShortMessageCount.Location = New System.Drawing.Point(456, 3)
        Me.lblShortMessageCount.Name = "lblShortMessageCount"
        Me.lblShortMessageCount.Size = New System.Drawing.Size(35, 12)
        Me.lblShortMessageCount.TabIndex = 1
        Me.lblShortMessageCount.Text = "共0筆"
        '
        'dgvShortMessage
        '
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle5.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvShortMessage.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvShortMessage.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvShortMessage.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Check, Me.StuName, Me.Mobile, Me.ShortMessage, Me.ShortMessageWordCount})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvShortMessage.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvShortMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvShortMessage.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter
        Me.dgvShortMessage.Location = New System.Drawing.Point(3, 18)
        Me.dgvShortMessage.Name = "dgvShortMessage"
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("新細明體", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvShortMessage.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvShortMessage.RowTemplate.Height = 24
        Me.dgvShortMessage.Size = New System.Drawing.Size(497, 358)
        Me.dgvShortMessage.TabIndex = 0
        '
        'Check
        '
        Me.Check.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        Me.Check.HeaderText = ""
        Me.Check.Name = "Check"
        Me.Check.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Check.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Check.Width = 21
        '
        'StuName
        '
        Me.StuName.HeaderText = "姓名"
        Me.StuName.Name = "StuName"
        '
        'Mobile
        '
        Me.Mobile.HeaderText = "手機"
        Me.Mobile.Name = "Mobile"
        '
        'ShortMessage
        '
        Me.ShortMessage.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill
        Me.ShortMessage.HeaderText = "簡訊內容"
        Me.ShortMessage.Name = "ShortMessage"
        '
        'ShortMessageWordCount
        '
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight
        Me.ShortMessageWordCount.DefaultCellStyle = DataGridViewCellStyle6
        Me.ShortMessageWordCount.HeaderText = "簡訊字數"
        Me.ShortMessageWordCount.MinimumWidth = 39
        Me.ShortMessageWordCount.Name = "ShortMessageWordCount"
        Me.ShortMessageWordCount.ReadOnly = True
        Me.ShortMessageWordCount.Width = 39
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Panel7)
        Me.Panel2.Controls.Add(Me.btnSaveExcelTemplate)
        Me.Panel2.Controls.Add(Me.btnImport)
        Me.Panel2.Controls.Add(Me.btnHistory)
        Me.Panel2.Controls.Add(Me.btnSend)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(3, 376)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(497, 83)
        Me.Panel2.TabIndex = 0
        '
        'Panel7
        '
        Me.Panel7.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Panel7.Controls.Add(Me.btnSMSelectAll)
        Me.Panel7.Controls.Add(Me.btnSMClearBelow)
        Me.Panel7.Controls.Add(Me.btnSMSelectBelow)
        Me.Panel7.Controls.Add(Me.btnSMClearAll)
        Me.Panel7.Location = New System.Drawing.Point(8, 3)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(144, 46)
        Me.Panel7.TabIndex = 8
        '
        'btnSMSelectAll
        '
        Me.btnSMSelectAll.Location = New System.Drawing.Point(1, 0)
        Me.btnSMSelectAll.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSMSelectAll.Name = "btnSMSelectAll"
        Me.btnSMSelectAll.Size = New System.Drawing.Size(71, 23)
        Me.btnSMSelectAll.TabIndex = 6
        Me.btnSMSelectAll.Text = "選擇全部"
        Me.btnSMSelectAll.UseVisualStyleBackColor = True
        '
        'btnSMClearBelow
        '
        Me.btnSMClearBelow.Location = New System.Drawing.Point(72, 23)
        Me.btnSMClearBelow.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSMClearBelow.Name = "btnSMClearBelow"
        Me.btnSMClearBelow.Size = New System.Drawing.Size(71, 23)
        Me.btnSMClearBelow.TabIndex = 6
        Me.btnSMClearBelow.Text = "取消以下"
        Me.btnSMClearBelow.UseVisualStyleBackColor = True
        '
        'btnSMSelectBelow
        '
        Me.btnSMSelectBelow.Location = New System.Drawing.Point(1, 23)
        Me.btnSMSelectBelow.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSMSelectBelow.Name = "btnSMSelectBelow"
        Me.btnSMSelectBelow.Size = New System.Drawing.Size(71, 23)
        Me.btnSMSelectBelow.TabIndex = 6
        Me.btnSMSelectBelow.Text = "選擇以下"
        Me.btnSMSelectBelow.UseVisualStyleBackColor = True
        '
        'btnSMClearAll
        '
        Me.btnSMClearAll.Location = New System.Drawing.Point(72, 0)
        Me.btnSMClearAll.Margin = New System.Windows.Forms.Padding(0)
        Me.btnSMClearAll.Name = "btnSMClearAll"
        Me.btnSMClearAll.Size = New System.Drawing.Size(71, 23)
        Me.btnSMClearAll.TabIndex = 6
        Me.btnSMClearAll.Text = "取消全部"
        Me.btnSMClearAll.UseVisualStyleBackColor = True
        '
        'btnSaveExcelTemplate
        '
        Me.btnSaveExcelTemplate.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnSaveExcelTemplate.Location = New System.Drawing.Point(113, 54)
        Me.btnSaveExcelTemplate.Name = "btnSaveExcelTemplate"
        Me.btnSaveExcelTemplate.Size = New System.Drawing.Size(122, 23)
        Me.btnSaveExcelTemplate.TabIndex = 1
        Me.btnSaveExcelTemplate.Text = "取得匯入檔案範本..."
        Me.btnSaveExcelTemplate.UseVisualStyleBackColor = True
        '
        'btnImport
        '
        Me.btnImport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnImport.Location = New System.Drawing.Point(3, 54)
        Me.btnImport.Name = "btnImport"
        Me.btnImport.Size = New System.Drawing.Size(104, 23)
        Me.btnImport.TabIndex = 1
        Me.btnImport.Text = "從檔案匯入..."
        Me.btnImport.UseVisualStyleBackColor = True
        '
        'btnHistory
        '
        Me.btnHistory.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnHistory.Location = New System.Drawing.Point(329, 54)
        Me.btnHistory.Name = "btnHistory"
        Me.btnHistory.Size = New System.Drawing.Size(75, 23)
        Me.btnHistory.TabIndex = 2
        Me.btnHistory.Text = "歷史紀錄"
        Me.btnHistory.UseVisualStyleBackColor = True
        '
        'btnSend
        '
        Me.btnSend.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSend.Location = New System.Drawing.Point(419, 54)
        Me.btnSend.Name = "btnSend"
        Me.btnSend.Size = New System.Drawing.Size(75, 23)
        Me.btnSend.TabIndex = 0
        Me.btnSend.Text = "發送"
        Me.btnSend.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Image = CType(resources.GetObject("btnAdd.Image"), System.Drawing.Image)
        Me.btnAdd.Location = New System.Drawing.Point(0, 0)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(34, 30)
        Me.btnAdd.TabIndex = 0
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnRemove
        '
        Me.btnRemove.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.btnRemove.Image = CType(resources.GetObject("btnRemove.Image"), System.Drawing.Image)
        Me.btnRemove.Location = New System.Drawing.Point(0, 35)
        Me.btnRemove.Name = "btnRemove"
        Me.btnRemove.Size = New System.Drawing.Size(34, 30)
        Me.btnRemove.TabIndex = 1
        Me.btnRemove.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.Panel4)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Right
        Me.Panel6.Location = New System.Drawing.Point(440, 0)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(40, 462)
        Me.Panel6.TabIndex = 2
        '
        'Panel4
        '
        Me.Panel4.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Panel4.Controls.Add(Me.btnAdd)
        Me.Panel4.Controls.Add(Me.btnRemove)
        Me.Panel4.Location = New System.Drawing.Point(3, 199)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(34, 65)
        Me.Panel4.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.BackColor = System.Drawing.Color.Lavender
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.SplitContainer2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Panel6)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.pSlider)
        Me.SplitContainer1.Panel2.Controls.Add(Me.GroupBox3)
        Me.SplitContainer1.Size = New System.Drawing.Size(984, 462)
        Me.SplitContainer1.SplitterDistance = 480
        Me.SplitContainer1.SplitterWidth = 1
        Me.SplitContainer1.TabIndex = 1
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer2.Name = "SplitContainer2"
        Me.SplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.GroupBox2)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.GroupBox1)
        Me.SplitContainer2.Size = New System.Drawing.Size(440, 462)
        Me.SplitContainer2.SplitterDistance = 200
        Me.SplitContainer2.TabIndex = 0
        '
        'pSlider
        '
        Me.pSlider.BackColor = System.Drawing.Color.LightGray
        Me.pSlider.Cursor = System.Windows.Forms.Cursors.VSplit
        Me.pSlider.Dock = System.Windows.Forms.DockStyle.Left
        Me.pSlider.Location = New System.Drawing.Point(0, 0)
        Me.pSlider.Name = "pSlider"
        Me.pSlider.Size = New System.Drawing.Size(5, 462)
        Me.pSlider.TabIndex = 0
        '
        'frmSMS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(984, 462)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmSMS"
        Me.Text = "簡訊系統"
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.dgvStudent, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        CType(Me.dgvShortMessage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel7.ResumeLayout(False)
        Me.Panel6.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        Me.SplitContainer2.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvShortMessage As System.Windows.Forms.DataGridView
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents btnSend As System.Windows.Forms.Button
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents tbShortMessage As System.Windows.Forms.TextBox
    Friend WithEvents btnInsertVar As System.Windows.Forms.Button
    Friend WithEvents cbVar As System.Windows.Forms.ComboBox
    Friend WithEvents lblWordCount As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvStudent As System.Windows.Forms.DataGridView
    Friend WithEvents btnRemove As System.Windows.Forms.Button
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents cbClass As System.Windows.Forms.ComboBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents tbStuName As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents tbStuNo As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents lblRowCount As System.Windows.Forms.Label
    Friend WithEvents btnImport As System.Windows.Forms.Button
    Friend WithEvents btnHistory As System.Windows.Forms.Button
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Check As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents StuName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Mobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ShortMessage As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ShortMessageWordCount As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FlowLayoutPanel1 As System.Windows.Forms.FlowLayoutPanel
    Friend WithEvents btnFirst As System.Windows.Forms.Button
    Friend WithEvents btnPrev As System.Windows.Forms.Button
    Friend WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnLast As System.Windows.Forms.Button
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents pSlider As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
    Friend WithEvents cbShowNonBlankMobile As System.Windows.Forms.CheckBox
    Friend WithEvents btnClearBelow As System.Windows.Forms.Button
    Friend WithEvents btnClearAll As System.Windows.Forms.Button
    Friend WithEvents btnSelectBelow As System.Windows.Forms.Button
    Friend WithEvents btnSelectAll As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents lblShortMessageCount As System.Windows.Forms.Label
    Friend WithEvents btnSaveExcelTemplate As System.Windows.Forms.Button
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents btnSMSelectAll As System.Windows.Forms.Button
    Friend WithEvents btnSMClearBelow As System.Windows.Forms.Button
    Friend WithEvents btnSMSelectBelow As System.Windows.Forms.Button
    Friend WithEvents btnSMClearAll As System.Windows.Forms.Button
    Friend WithEvents PreCheck As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents RowID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PreStuNo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PreStuName As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PreMobile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
