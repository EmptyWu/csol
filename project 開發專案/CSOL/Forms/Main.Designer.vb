﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.msMenu = New System.Windows.Forms.MenuStrip
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.miChangePassword = New System.Windows.Forms.ToolStripMenuItem
        Me.miWorkStation = New System.Windows.Forms.ToolStripMenuItem
        Me.miPrinterSetting = New System.Windows.Forms.ToolStripMenuItem
        Me.miChangeUser = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem
        Me.miBasic = New System.Windows.Forms.ToolStripMenuItem
        Me.miClassroom = New System.Windows.Forms.ToolStripMenuItem
        Me.miClass = New System.Windows.Forms.ToolStripMenuItem
        Me.miStudent = New System.Windows.Forms.ToolStripMenuItem
        Me.miAddStu = New System.Windows.Forms.ToolStripMenuItem
        Me.miStuList = New System.Windows.Forms.ToolStripMenuItem
        Me.miSMS = New System.Windows.Forms.ToolStripMenuItem
        Me.miReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miAccountingDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi3DaysBeforeDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi2DaysBeforeDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi1DayBeforeDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miTodayDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator
        Me.miChooseDateDailyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miAccountingMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi3MonthsBeforeMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi2MonthsBeforeMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.mi1MonthBeforeMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miCurrentMonthMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator
        Me.miChooseMonthMonthlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miAccountingYearlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miLastYearYearlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miThisYearYearlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator
        Me.miChooseYearYearlyReport = New System.Windows.Forms.ToolStripMenuItem
        Me.miAdmin = New System.Windows.Forms.ToolStripMenuItem
        Me.miUsers = New System.Windows.Forms.ToolStripMenuItem
        Me.ssStatus = New System.Windows.Forms.StatusStrip
        Me.tssStatus = New System.Windows.Forms.ToolStripStatusLabel
        Me.tssNetwork = New System.Windows.Forms.ToolStripStatusLabel
        Me.tssUser = New System.Windows.Forms.ToolStripStatusLabel
        Me.tssTime = New System.Windows.Forms.ToolStripStatusLabel
        Me.TabMDI = New WinControl.WinControl
        Me.cmsTabMDI = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miCloseActiveForm = New System.Windows.Forms.ToolStripMenuItem
        Me.miCloseAllFormsButActive = New System.Windows.Forms.ToolStripMenuItem
        Me.tTime = New System.Windows.Forms.Timer(Me.components)
        Me.miTeacher = New System.Windows.Forms.ToolStripMenuItem
        Me.miCourse = New System.Windows.Forms.ToolStripMenuItem
        Me.msMenu.SuspendLayout()
        Me.ssStatus.SuspendLayout()
        Me.cmsTabMDI.SuspendLayout()
        Me.SuspendLayout()
        '
        'msMenu
        '
        Me.msMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileMenu, Me.miBasic, Me.miStudent, Me.miSMS, Me.miReport, Me.miAdmin})
        Me.msMenu.Location = New System.Drawing.Point(0, 0)
        Me.msMenu.Name = "msMenu"
        Me.msMenu.Size = New System.Drawing.Size(984, 24)
        Me.msMenu.TabIndex = 5
        Me.msMenu.Text = "MenuStrip"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miChangePassword, Me.miWorkStation, Me.miPrinterSetting, Me.miChangeUser, Me.ToolStripSeparator1, Me.ExitToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(84, 20)
        Me.FileMenu.Text = "個人設定(&C)"
        '
        'miChangePassword
        '
        Me.miChangePassword.Name = "miChangePassword"
        Me.miChangePassword.Size = New System.Drawing.Size(140, 22)
        Me.miChangePassword.Text = "更改密碼(&P)"
        '
        'miWorkStation
        '
        Me.miWorkStation.Name = "miWorkStation"
        Me.miWorkStation.Size = New System.Drawing.Size(140, 22)
        Me.miWorkStation.Text = "設定工作站"
        '
        'miPrinterSetting
        '
        Me.miPrinterSetting.Name = "miPrinterSetting"
        Me.miPrinterSetting.Size = New System.Drawing.Size(140, 22)
        Me.miPrinterSetting.Text = "設定印表機"
        '
        'miChangeUser
        '
        Me.miChangeUser.Name = "miChangeUser"
        Me.miChangeUser.Size = New System.Drawing.Size(140, 22)
        Me.miChangeUser.Text = "變更使用者"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(137, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(140, 22)
        Me.ExitToolStripMenuItem.Text = "結束系統(&X)"
        '
        'miBasic
        '
        Me.miBasic.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miTeacher, Me.miCourse, Me.miClassroom, Me.miClass})
        Me.miBasic.Name = "miBasic"
        Me.miBasic.Size = New System.Drawing.Size(83, 20)
        Me.miBasic.Text = "班務管理(&B)"
        '
        'miClassroom
        '
        Me.miClassroom.Name = "miClassroom"
        Me.miClassroom.Size = New System.Drawing.Size(152, 22)
        Me.miClassroom.Text = "編輯教室"
        '
        'miClass
        '
        Me.miClass.Name = "miClass"
        Me.miClass.Size = New System.Drawing.Size(152, 22)
        Me.miClass.Text = "編輯班級"
        '
        'miStudent
        '
        Me.miStudent.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miAddStu, Me.miStuList})
        Me.miStudent.Name = "miStudent"
        Me.miStudent.Size = New System.Drawing.Size(83, 20)
        Me.miStudent.Text = "學生管理(&S)"
        '
        'miAddStu
        '
        Me.miAddStu.Name = "miAddStu"
        Me.miAddStu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.miAddStu.Size = New System.Drawing.Size(170, 22)
        Me.miAddStu.Text = "新增學生"
        '
        'miStuList
        '
        Me.miStuList.Name = "miStuList"
        Me.miStuList.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F), System.Windows.Forms.Keys)
        Me.miStuList.Size = New System.Drawing.Size(170, 22)
        Me.miStuList.Text = "學生列表"
        '
        'miSMS
        '
        Me.miSMS.Name = "miSMS"
        Me.miSMS.Size = New System.Drawing.Size(88, 20)
        Me.miSMS.Text = "簡訊系統(&M)"
        '
        'miReport
        '
        Me.miReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miAccountingDailyReport, Me.miAccountingMonthlyReport, Me.miAccountingYearlyReport})
        Me.miReport.Name = "miReport"
        Me.miReport.Size = New System.Drawing.Size(84, 20)
        Me.miReport.Text = "會計報表(&R)"
        '
        'miAccountingDailyReport
        '
        Me.miAccountingDailyReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mi3DaysBeforeDailyReport, Me.mi2DaysBeforeDailyReport, Me.mi1DayBeforeDailyReport, Me.miTodayDailyReport, Me.ToolStripSeparator2, Me.miChooseDateDailyReport})
        Me.miAccountingDailyReport.Name = "miAccountingDailyReport"
        Me.miAccountingDailyReport.Size = New System.Drawing.Size(156, 22)
        Me.miAccountingDailyReport.Tag = "0"
        Me.miAccountingDailyReport.Text = "列印日報表(&D)"
        '
        'mi3DaysBeforeDailyReport
        '
        Me.mi3DaysBeforeDailyReport.Name = "mi3DaysBeforeDailyReport"
        Me.mi3DaysBeforeDailyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi3DaysBeforeDailyReport.Tag = "-3"
        Me.mi3DaysBeforeDailyReport.Text = "Today-3"
        '
        'mi2DaysBeforeDailyReport
        '
        Me.mi2DaysBeforeDailyReport.Name = "mi2DaysBeforeDailyReport"
        Me.mi2DaysBeforeDailyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi2DaysBeforeDailyReport.Tag = "-2"
        Me.mi2DaysBeforeDailyReport.Text = "Today-2"
        '
        'mi1DayBeforeDailyReport
        '
        Me.mi1DayBeforeDailyReport.Name = "mi1DayBeforeDailyReport"
        Me.mi1DayBeforeDailyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi1DayBeforeDailyReport.Tag = "-1"
        Me.mi1DayBeforeDailyReport.Text = "Today-1"
        '
        'miTodayDailyReport
        '
        Me.miTodayDailyReport.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.miTodayDailyReport.Name = "miTodayDailyReport"
        Me.miTodayDailyReport.Size = New System.Drawing.Size(184, 22)
        Me.miTodayDailyReport.Tag = "0"
        Me.miTodayDailyReport.Text = "Today"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(181, 6)
        '
        'miChooseDateDailyReport
        '
        Me.miChooseDateDailyReport.Name = "miChooseDateDailyReport"
        Me.miChooseDateDailyReport.Size = New System.Drawing.Size(184, 22)
        Me.miChooseDateDailyReport.Text = "選擇列印日報表日期"
        '
        'miAccountingMonthlyReport
        '
        Me.miAccountingMonthlyReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mi3MonthsBeforeMonthlyReport, Me.mi2MonthsBeforeMonthlyReport, Me.mi1MonthBeforeMonthlyReport, Me.miCurrentMonthMonthlyReport, Me.ToolStripSeparator3, Me.miChooseMonthMonthlyReport})
        Me.miAccountingMonthlyReport.Name = "miAccountingMonthlyReport"
        Me.miAccountingMonthlyReport.Size = New System.Drawing.Size(156, 22)
        Me.miAccountingMonthlyReport.Tag = "0"
        Me.miAccountingMonthlyReport.Text = "列印月報表(&M)"
        '
        'mi3MonthsBeforeMonthlyReport
        '
        Me.mi3MonthsBeforeMonthlyReport.Name = "mi3MonthsBeforeMonthlyReport"
        Me.mi3MonthsBeforeMonthlyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi3MonthsBeforeMonthlyReport.Tag = "-3"
        Me.mi3MonthsBeforeMonthlyReport.Text = "ThisMonth-3"
        '
        'mi2MonthsBeforeMonthlyReport
        '
        Me.mi2MonthsBeforeMonthlyReport.Name = "mi2MonthsBeforeMonthlyReport"
        Me.mi2MonthsBeforeMonthlyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi2MonthsBeforeMonthlyReport.Tag = "-2"
        Me.mi2MonthsBeforeMonthlyReport.Text = "ThisMonth-2"
        '
        'mi1MonthBeforeMonthlyReport
        '
        Me.mi1MonthBeforeMonthlyReport.Name = "mi1MonthBeforeMonthlyReport"
        Me.mi1MonthBeforeMonthlyReport.Size = New System.Drawing.Size(184, 22)
        Me.mi1MonthBeforeMonthlyReport.Tag = "-1"
        Me.mi1MonthBeforeMonthlyReport.Text = "ThisMonth-1"
        '
        'miCurrentMonthMonthlyReport
        '
        Me.miCurrentMonthMonthlyReport.Font = New System.Drawing.Font("微軟正黑體", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(136, Byte))
        Me.miCurrentMonthMonthlyReport.Name = "miCurrentMonthMonthlyReport"
        Me.miCurrentMonthMonthlyReport.Size = New System.Drawing.Size(184, 22)
        Me.miCurrentMonthMonthlyReport.Tag = "0"
        Me.miCurrentMonthMonthlyReport.Text = "ThisMonth"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(181, 6)
        '
        'miChooseMonthMonthlyReport
        '
        Me.miChooseMonthMonthlyReport.Name = "miChooseMonthMonthlyReport"
        Me.miChooseMonthMonthlyReport.Size = New System.Drawing.Size(184, 22)
        Me.miChooseMonthMonthlyReport.Text = "選擇列印月報表月份"
        '
        'miAccountingYearlyReport
        '
        Me.miAccountingYearlyReport.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miLastYearYearlyReport, Me.miThisYearYearlyReport, Me.ToolStripSeparator4, Me.miChooseYearYearlyReport})
        Me.miAccountingYearlyReport.Name = "miAccountingYearlyReport"
        Me.miAccountingYearlyReport.Size = New System.Drawing.Size(156, 22)
        Me.miAccountingYearlyReport.Tag = "0"
        Me.miAccountingYearlyReport.Text = "列印年報表(&Y)"
        '
        'miLastYearYearlyReport
        '
        Me.miLastYearYearlyReport.Name = "miLastYearYearlyReport"
        Me.miLastYearYearlyReport.Size = New System.Drawing.Size(184, 22)
        Me.miLastYearYearlyReport.Tag = "-1"
        Me.miLastYearYearlyReport.Text = "列印去年年報表"
        '
        'miThisYearYearlyReport
        '
        Me.miThisYearYearlyReport.Name = "miThisYearYearlyReport"
        Me.miThisYearYearlyReport.Size = New System.Drawing.Size(184, 22)
        Me.miThisYearYearlyReport.Tag = "0"
        Me.miThisYearYearlyReport.Text = "列印今年年報表"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(181, 6)
        '
        'miChooseYearYearlyReport
        '
        Me.miChooseYearYearlyReport.Name = "miChooseYearYearlyReport"
        Me.miChooseYearYearlyReport.Size = New System.Drawing.Size(184, 22)
        Me.miChooseYearYearlyReport.Text = "選擇列印年報表年份"
        '
        'miAdmin
        '
        Me.miAdmin.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miUsers})
        Me.miAdmin.Name = "miAdmin"
        Me.miAdmin.Size = New System.Drawing.Size(84, 20)
        Me.miAdmin.Text = "權限管理(&A)"
        '
        'miUsers
        '
        Me.miUsers.Name = "miUsers"
        Me.miUsers.Size = New System.Drawing.Size(153, 22)
        Me.miUsers.Text = "使用者管理(&U)"
        '
        'ssStatus
        '
        Me.ssStatus.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tssStatus, Me.tssNetwork, Me.tssUser, Me.tssTime})
        Me.ssStatus.Location = New System.Drawing.Point(0, 539)
        Me.ssStatus.Name = "ssStatus"
        Me.ssStatus.Size = New System.Drawing.Size(984, 25)
        Me.ssStatus.TabIndex = 7
        Me.ssStatus.Text = "StatusStrip"
        '
        'tssStatus
        '
        Me.tssStatus.Name = "tssStatus"
        Me.tssStatus.Size = New System.Drawing.Size(719, 20)
        Me.tssStatus.Spring = True
        Me.tssStatus.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'tssNetwork
        '
        Me.tssNetwork.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.tssNetwork.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.tssNetwork.Name = "tssNetwork"
        Me.tssNetwork.Size = New System.Drawing.Size(36, 20)
        Me.tssNetwork.Text = "連線"
        '
        'tssUser
        '
        Me.tssUser.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.tssUser.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.tssUser.Name = "tssUser"
        Me.tssUser.Size = New System.Drawing.Size(72, 20)
        Me.tssUser.Text = "使用者名稱"
        '
        'tssTime
        '
        Me.tssTime.BorderSides = CType((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) _
                    Or System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom), System.Windows.Forms.ToolStripStatusLabelBorderSides)
        Me.tssTime.BorderStyle = System.Windows.Forms.Border3DStyle.Sunken
        Me.tssTime.Name = "tssTime"
        Me.tssTime.Size = New System.Drawing.Size(142, 20)
        Me.tssTime.Text = "2010/6/6 上午 09:00:00"
        '
        'TabMDI
        '
        Me.TabMDI.ControlColorScheme = WinControl.WinControl.ColorScheme.Green
        Me.TabMDI.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabMDI.Image = CType(resources.GetObject("TabMDI.Image"), System.Drawing.Image)
        Me.TabMDI.Location = New System.Drawing.Point(0, 24)
        Me.TabMDI.MessageColor = System.Drawing.Color.DarkBlue
        Me.TabMDI.MessageFont = New System.Drawing.Font("新細明體", 14.0!)
        Me.TabMDI.MessageText = "                 版權所有 © 2010. 新技網路科技股份有限公司"
        Me.TabMDI.Name = "TabMDI"
        Me.TabMDI.SaveReqCancel = "Cancel"
        Me.TabMDI.SaveReqCaption = "Save changes"
        Me.TabMDI.SaveReqNo = "&No"
        Me.TabMDI.SaveReqQuestion = "Save changes to the following items?"
        Me.TabMDI.SaveReqYes = "&Yes"
        Me.TabMDI.Size = New System.Drawing.Size(984, 515)
        Me.TabMDI.TabIndex = 11
        '
        'cmsTabMDI
        '
        Me.cmsTabMDI.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miCloseActiveForm, Me.miCloseAllFormsButActive})
        Me.cmsTabMDI.Name = "cmsTabMDI"
        Me.cmsTabMDI.Size = New System.Drawing.Size(173, 48)
        '
        'miCloseActiveForm
        '
        Me.miCloseActiveForm.Name = "miCloseActiveForm"
        Me.miCloseActiveForm.Size = New System.Drawing.Size(172, 22)
        Me.miCloseActiveForm.Text = "關閉分頁(Ctrl+W)"
        '
        'miCloseAllFormsButActive
        '
        Me.miCloseAllFormsButActive.Name = "miCloseAllFormsButActive"
        Me.miCloseAllFormsButActive.Size = New System.Drawing.Size(172, 22)
        Me.miCloseAllFormsButActive.Text = "關閉其他分頁"
        '
        'tTime
        '
        Me.tTime.Enabled = True
        Me.tTime.Interval = 490
        '
        'miTeacher
        '
        Me.miTeacher.Name = "miTeacher"
        Me.miTeacher.Size = New System.Drawing.Size(152, 22)
        Me.miTeacher.Text = "編輯老師"
        '
        'miCourse
        '
        Me.miCourse.Name = "miCourse"
        Me.miCourse.Size = New System.Drawing.Size(152, 22)
        Me.miCourse.Text = "編輯課程"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(984, 564)
        Me.Controls.Add(Me.TabMDI)
        Me.Controls.Add(Me.msMenu)
        Me.Controls.Add(Me.ssStatus)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.KeyPreview = True
        Me.MainMenuStrip = Me.msMenu
        Me.Name = "Main"
        Me.Text = "CSOL管理系統"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.msMenu.ResumeLayout(False)
        Me.msMenu.PerformLayout()
        Me.ssStatus.ResumeLayout(False)
        Me.ssStatus.PerformLayout()
        Me.cmsTabMDI.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tssStatus As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ssStatus As System.Windows.Forms.StatusStrip
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents msMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents tssUser As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tssTime As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents miBasic As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miClassroom As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miClass As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miStudent As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miAddStu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TabMDI As WinControl.WinControl
    Friend WithEvents tssNetwork As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents miWorkStation As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents miAdmin As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miUsers As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miChangePassword As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tTime As System.Windows.Forms.Timer
    Friend WithEvents miChangeUser As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miAccountingDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miAccountingMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miAccountingYearlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi3DaysBeforeDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi2DaysBeforeDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi1DayBeforeDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTodayDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miChooseDateDailyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi3MonthsBeforeMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi2MonthsBeforeMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mi1MonthBeforeMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miCurrentMonthMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miChooseMonthMonthlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miLastYearYearlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miThisYearYearlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miChooseYearYearlyReport As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents miPrinterSetting As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miSMS As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsTabMDI As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents miCloseActiveForm As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miCloseAllFormsButActive As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miStuList As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miTeacher As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents miCourse As System.Windows.Forms.ToolStripMenuItem

End Class
