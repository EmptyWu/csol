﻿Public Class frmCourse
    Implements WinControl.IWinControllable
    Public Sub Save() Implements WinControl.IWinControllable.Save
        'Do Nothing
    End Sub

    Private m_WinInfo As WinControl.WinInfo
    Public Property WinInfo() As WinControl.WinInfo Implements WinControl.IWinControllable.WinInfo
        Get
            Return Me.m_WinInfo
        End Get
        Set(ByVal value As WinControl.WinInfo)
            Me.m_WinInfo = value
        End Set
    End Property

    Private m_TeacherList As DataTable
    Private m_List As DataTable
    Private m_DeletedIDs As New List(Of Integer)

    Private WithEvents m_GlobalMDI As WinControl.WinControl = My.Application.FormController.TabMDI
    Private Sub MainMDI_ActiveFormChanged(ByVal sender As Object, ByVal e As EventArgs) Handles m_GlobalMDI.ActiveFormChanged
        If Me.Equals(Me.m_GlobalMDI.ActiveForm) Then
            GetTeacherList()
            GetList()
        End If
    End Sub

    Private Sub frmCourse_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetTeacherList()
        GetList()
    End Sub

    Private Sub GetTeacherList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Teacher", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_TeacherList = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        Dim row As DataRow = Me.m_TeacherList.NewRow
        row("ID") = -1
        row("Name") = "請選擇老師..."
        Me.m_TeacherList.Rows.InsertAt(row, 0)

        Dim col As DataGridViewComboBoxColumn = dgvList.Columns("colTeacher")
        col.DisplayMember = "Name"
        col.ValueMember = "ID"
        col.DataSource = Me.m_TeacherList
    End Sub

    Private Sub GetList()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Course", "GetList", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Me.m_List = CSOL.Convert.XmlStringToDataTable(ResponseParams("List"))
        For Each row As DataRow In Me.m_List.Rows
            If Me.m_TeacherList.Select(String.Format("ID = {0}", row("TeacherID"))).Length = 0 Then
                row("TeacherID") = -1
            End If
        Next
        dgvList.DataSource = Me.m_List
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("List", CSOL.Convert.DataTableToXmlString(Me.m_List))
        RequestParams.Add("DeletedIDs", String.Join(",", Array.ConvertAll(Of Integer, String)(Me.m_DeletedIDs.ToArray(), AddressOf Convert.ToString)))

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Course", "SaveList", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        Dim Status As String = ResponseParams("Status")
        If Status = "OK" Then
            MessageBox.Show("儲存成功...", "訊息...", MessageBoxButtons.OK, MessageBoxIcon.Information)
            GetList()
        Else
            MessageBox.Show("儲存失敗...", "錯誤...", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub dgvList_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles dgvList.DataError
        e.ThrowException = False
    End Sub

    Private Sub dgvList_DefaultValuesNeeded(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewRowEventArgs) Handles dgvList.DefaultValuesNeeded
        e.Row.Cells("colID").Value = -1
        e.Row.Cells("colTeacher").Value = -1
        e.Row.Cells("colName").Value = ""
        e.Row.Cells("colDescription").Value = ""
    End Sub

    Private Sub frmCourse_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        If e.Control And e.KeyCode = Keys.S Then
            btnSave_Click(Nothing, Nothing)
        End If
    End Sub

    Private Sub dgvList_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgvList.MouseClick
        Dim dgv As DataGridView = sender
        If dgv.HitTest(e.X, e.Y).Type = DataGridViewHitTestType.RowHeader Then
            dgv.EditMode = DataGridViewEditMode.EditOnKeystrokeOrF2
            dgv.EndEdit()
        Else
            dgv.EditMode = DataGridViewEditMode.EditOnEnter
        End If
    End Sub

    Private m_Confirmed As Boolean = False
    Private Sub dgvList_UserDeletingRow(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowCancelEventArgs) Handles dgvList.UserDeletingRow
        If Me.m_Confirmed Then
            If e.Row.Cells("colID").Value > 0 Then
                If Not Me.m_DeletedIDs.Contains(e.Row.Cells("colID").Value) Then
                    Me.m_DeletedIDs.Add(e.Row.Cells("colID").Value)
                End If
            End If

            If dgvList.SelectedRows.Count <= 1 Then
                Me.m_Confirmed = False
            End If
        Else
            If MessageBox.Show(String.Format("您確定要刪除所選取的這些課程嗎??{0}按下儲存後才會真的刪除...", vbCrLf), "注意...", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) = Windows.Forms.DialogResult.Yes Then
                Me.m_Confirmed = True
                If e.Row.Cells("colID").Value > 0 Then
                    If Not Me.m_DeletedIDs.Contains(e.Row.Cells("colID").Value) Then
                        Me.m_DeletedIDs.Add(e.Row.Cells("colID").Value)
                    End If
                End If

                If dgvList.SelectedRows.Count <= 1 Then
                    Me.m_Confirmed = False
                End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub
End Class