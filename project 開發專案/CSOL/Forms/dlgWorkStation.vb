﻿Imports System.Windows.Forms

Public Class dlgWorkStation
    Private m_WorkStationID As String = My.Settings.WorkStationID

    Private Sub dlgWorkStation_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDepartments()
        If Me.m_WorkStationID <> "" Then
            GetWorkStation()
        End If
        tbName.Select()
    End Sub

    Private Sub GetDepartments()
        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "GetDepartments", System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)

        cbDepartment.Items.AddRange(ResponseParams("Departments").Split(","))
    End Sub

    Private Sub GetWorkStation()
        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("WorkStationID", Me.m_WorkStationID)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "GetWorkStation", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        tbName.Text = ResponseParams("Name")
        cbDepartment.Text = ResponseParams("Department")
    End Sub

    Private Sub OK_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK_Button.Click
        If tbName.Text.Trim() = "" Then
            MessageBox.Show("工作站名稱不能空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            tbName.Select()
            Exit Sub
        End If

        If cbDepartment.Text.Trim() = "" Then
            MessageBox.Show("分部名稱不能空白...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
            cbDepartment.Select()
            Exit Sub
        End If

        Dim RequestParams As New System.Collections.Specialized.NameValueCollection()
        RequestParams.Add("WorkStationID", Me.m_WorkStationID)
        RequestParams.Add("WorkStationName", tbName.Text)
        RequestParams.Add("Department", cbDepartment.Text)

        Dim ResponseBody As String = CSOL.HTTPClient.Post("Auth", "SaveWorkStation", RequestParams, System.Text.Encoding.UTF8)
        Dim ResponseParams As System.Collections.Specialized.NameValueCollection = CSOL.HTTPClient.ParseQuery(ResponseBody)
        If ResponseParams("Status") = "OK" Then
            My.Settings.WorkStationID = ResponseParams("WorkStationID")
            My.Settings.Save()
            Me.Close()
        Else
            MessageBox.Show("儲存失敗...", "注意...", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    Private Sub Cancel_Button_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel_Button.Click
        Me.Close()
    End Sub
End Class
