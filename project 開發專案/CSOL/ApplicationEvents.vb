﻿Namespace My

    ' MyApplication 可以使用下列事件:
    ' 
    ' Startup: 在應用程式啟動時，但尚未建立啟動表單之前引發。
    ' Shutdown: 在所有應用程式表單關閉之後引發。如果應用程式不正常終止，就不會引發此事件。
    ' UnhandledException: 在應用程式發生未處理的例外狀況時引發。
    ' StartupNextInstance: 在啟動單一執行個體應用程式且應用程式已於使用中時引發。
    ' NetworkAvailabilityChanged: 在連接或中斷網路連接時引發。

    Partial Friend Class MyApplication
        Private m_SessionID As String = ""
        Public Property SessionID() As String
            Get
                Return Me.m_SessionID
            End Get
            Set(ByVal value As String)
                Me.m_SessionID = value
            End Set
        End Property

        Private m_User As String = ""
        Public Property User() As String
            Get
                Return Me.m_User
            End Get
            Set(ByVal value As String)
                Me.m_User = value
            End Set
        End Property

        Private m_Main As Main
        Public Property FormController() As Main
            Get
                Return Me.m_Main
            End Get
            Set(ByVal value As Main)
                Me.m_Main = value
            End Set
        End Property

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            CSOL.Data.DownloadData()
        End Sub
    End Class

End Namespace

