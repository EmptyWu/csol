﻿Namespace CSOL
    Public Class DirectXCapture
        Private Shared m_Capture As DirectX.Capture.Capture
        Public Shared Property Capture() As DirectX.Capture.Capture
            Get
                Return DirectXCapture.m_Capture
            End Get
            Set(ByVal value As DirectX.Capture.Capture)
                DirectXCapture.m_Capture = value
            End Set
        End Property

        Public Shared Function GetVideoDevice(ByVal Name As String) As DirectX.Capture.Filter
            Try
                Dim filters As New DirectX.Capture.Filters()
                For Each camera As DirectX.Capture.Filter In filters.VideoInputDevices
                    If camera.Name = Name Then
                        Return camera
                    End If
                Next
            Catch exNotSupport As NotSupportedException
                System.Diagnostics.Debug.WriteLine(exNotSupport.ToString())
            Catch ex As Exception
                Dim LogPath As String = String.Format("{0}\Error{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
                Dim log As String = String.Format("{1}{0}{2}" & vbCrLf, vbTab, Now.ToString("HH:mm:ss.fff"), ex.ToString())
                System.IO.File.AppendAllText(LogPath, log)
            End Try
            Return Nothing
        End Function

        Public Shared Function GetVideoDeviceNames() As List(Of String)
            Dim list As New List(Of String)
            Try
                Dim filters As New DirectX.Capture.Filters()
                For Each camera As DirectX.Capture.Filter In filters.VideoInputDevices
                    list.Add(camera.Name)
                Next
            Catch exNotSupport As NotSupportedException
                System.Diagnostics.Debug.WriteLine(exNotSupport.ToString())
            Catch ex As Exception
                Dim LogPath As String = String.Format("{0}\Error{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
                Dim log As String = String.Format("{1}{0}{2}" & vbCrLf, vbTab, Now.ToString("HH:mm:ss.fff"), ex.ToString())
                System.IO.File.AppendAllText(LogPath, log)
            End Try
            Return list
        End Function

        Private Shared m_Image As Bitmap = Nothing
        Public Shared Function Grap(ByVal DeviceName As String) As Bitmap
            Try
                Dim devices As List(Of String) = GetVideoDeviceNames()
                If devices.Contains(DeviceName) Then
                    If Capture Is Nothing Then
                        Capture = New DirectX.Capture.Capture(GetVideoDevice(DeviceName), Nothing)
                        Capture.PreviewWindow = New Panel()
                    Else
                        If Capture.VideoDevice.Name <> DeviceName Then
                            Capture = New DirectX.Capture.Capture(GetVideoDevice(DeviceName), Nothing)
                            Capture.PreviewWindow = New Panel()
                        End If
                    End If

                    AddHandler Capture.FrameEvent2, AddressOf GrapDone
                    m_Image = Nothing
                    Capture.GrapImg()

                    While m_Image Is Nothing
                        System.Threading.Thread.Sleep(10)
                    End While

                    Return m_Image
                End If
            Catch exNotSupport As NotSupportedException
                System.Diagnostics.Debug.WriteLine(exNotSupport.ToString())
            Catch ex As Exception
                Dim LogPath As String = String.Format("{0}\Error{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
                Dim log As String = String.Format("{1}{0}{2}" & vbCrLf, vbTab, Now.ToString("HH:mm:ss.fff"), ex.ToString())
                System.IO.File.AppendAllText(LogPath, log)
            End Try
            Return Nothing
        End Function

        Private Shared Sub GrapDone(ByVal image As Bitmap)
            m_Image = image
            RemoveHandler Capture.FrameEvent2, AddressOf GrapDone
        End Sub
    End Class

    Public Class Webcam
        Const WM_CAP As Short = &H400S

        Const WM_CAP_DRIVER_CONNECT As Integer = WM_CAP + 10
        Const WM_CAP_DRIVER_DISCONNECT As Integer = WM_CAP + 11
        Const WM_CAP_EDIT_COPY As Integer = WM_CAP + 30

        Const WS_CHILD As Integer = &H40000000
        Const WS_VISIBLE As Integer = &H10000000

        Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hwnd As Integer, ByVal wMsg As Integer, ByVal wParam As Short, ByVal lParam As String) As Integer
        Private Declare Function capCreateCaptureWindowA Lib "avicap32.dll" (ByVal lpszWindowName As String, ByVal dwStyle As Integer, ByVal x As Integer, ByVal y As Integer, ByVal nWidth As Integer, ByVal nHeight As Short, ByVal hWndParent As Integer, ByVal nID As Integer) As Integer
        Private Declare Function capGetDriverDescriptionA Lib "avicap32.dll" (ByVal wDriver As Short, ByVal lpszName As String, ByVal cbName As Integer, ByVal lpszVer As String, ByVal cbVer As Integer) As Boolean

        Public Shared Function GetDevices() As String()
            Dim result As New List(Of String)

            Dim index As Integer = 0
            Dim name As String = Space(100)
            Dim version As String = Space(100)

            While capGetDriverDescriptionA(index, name, 100, version, 100)
                result.Add(name.Trim())
                index += 1
            End While

            Return result.ToArray()
        End Function

        Public Shared Function GetPicture() As Bitmap
            Return GetPicture("")
        End Function

        Public Shared Function GetPicture(ByVal DeviceName As String) As Bitmap
            Dim result As Bitmap = Nothing
            Dim Devices() As String = GetDevices()
            If Devices.Length = 0 Then
                Return result
            End If

            Dim DeviceIndex As Integer = Array.IndexOf(Devices, DeviceName)
            If DeviceIndex = -1 Then
                DeviceIndex = 0
            End If

            Dim pb As New PictureBox()
            Dim Handle As IntPtr = capCreateCaptureWindowA(DeviceIndex, WS_VISIBLE Or WS_CHILD, 0, 0, 640, 480, pb.Handle, 0)
            If SendMessage(Handle, WM_CAP_DRIVER_CONNECT, DeviceIndex, 0) Then
                SendMessage(Handle, WM_CAP_EDIT_COPY, 0, 0)
                Dim data As IDataObject = Clipboard.GetDataObject()
                If data.GetDataPresent(GetType(System.Drawing.Bitmap)) Then
                    result = CType(data.GetData(GetType(System.Drawing.Bitmap)), System.Drawing.Bitmap)
                End If
                SendMessage(Handle, WM_CAP_DRIVER_DISCONNECT, DeviceIndex, 0)
            End If
            Return result
        End Function

    End Class
End Namespace
