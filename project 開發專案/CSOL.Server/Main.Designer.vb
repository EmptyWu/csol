﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.niServer = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.tbMessage = New System.Windows.Forms.TextBox
        Me.tRestartService = New System.Windows.Forms.Timer(Me.components)
        Me.cmNotifyIcon = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.miClose = New System.Windows.Forms.ToolStripMenuItem
        Me.cmNotifyIcon.SuspendLayout()
        Me.SuspendLayout()
        '
        'niServer
        '
        Me.niServer.ContextMenuStrip = Me.cmNotifyIcon
        Me.niServer.Icon = CType(resources.GetObject("niServer.Icon"), System.Drawing.Icon)
        Me.niServer.Text = "CSOL伺服器運作中"
        Me.niServer.Visible = True
        '
        'tbMessage
        '
        Me.tbMessage.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbMessage.Location = New System.Drawing.Point(0, 0)
        Me.tbMessage.Multiline = True
        Me.tbMessage.Name = "tbMessage"
        Me.tbMessage.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tbMessage.Size = New System.Drawing.Size(291, 132)
        Me.tbMessage.TabIndex = 0
        Me.tbMessage.Text = "運作中..."
        '
        'tRestartService
        '
        Me.tRestartService.Interval = 30000
        '
        'cmNotifyIcon
        '
        Me.cmNotifyIcon.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.miClose})
        Me.cmNotifyIcon.Name = "cmNotifyIcon"
        Me.cmNotifyIcon.Size = New System.Drawing.Size(168, 26)
        '
        'miClose
        '
        Me.miClose.Name = "miClose"
        Me.miClose.Size = New System.Drawing.Size(167, 22)
        Me.miClose.Text = "關閉CSOL伺服器"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(291, 132)
        Me.Controls.Add(Me.tbMessage)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Main"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CSOL伺服器"
        Me.WindowState = System.Windows.Forms.FormWindowState.Minimized
        Me.cmNotifyIcon.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents niServer As System.Windows.Forms.NotifyIcon
    Friend WithEvents tbMessage As System.Windows.Forms.TextBox
    Friend WithEvents tRestartService As System.Windows.Forms.Timer
    Friend WithEvents cmNotifyIcon As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents miClose As System.Windows.Forms.ToolStripMenuItem

End Class
