﻿Public Class Main

    Private Sub Main_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        niServer.Visible = False
    End Sub

    Private Sub Main_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Hide()
    End Sub

    Private Sub Main_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            niServer.Visible = True
        End If
    End Sub

    Private Sub niServer_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles niServer.DoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        niServer.Visible = False
    End Sub

    Private Sub miClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles miClose.Click
        niServer.Visible = False
        Me.Close()
    End Sub
End Class
