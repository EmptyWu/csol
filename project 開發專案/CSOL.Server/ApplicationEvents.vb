﻿Namespace My

    ' MyApplication 可以使用下列事件:
    ' 
    ' Startup: 在應用程式啟動時，但尚未建立啟動表單之前引發。
    ' Shutdown: 在所有應用程式表單關閉之後引發。如果應用程式不正常終止，就不會引發此事件。
    ' UnhandledException: 在應用程式發生未處理的例外狀況時引發。
    ' StartupNextInstance: 在啟動單一執行個體應用程式且應用程式已於使用中時引發。
    ' NetworkAvailabilityChanged: 在連接或中斷網路連接時引發。
    Partial Friend Class MyApplication
        Private m_Processes As New List(Of System.Diagnostics.Process)
        Friend Property Processes() As List(Of System.Diagnostics.Process)
            Get
                Return Me.m_Processes
            End Get
            Set(ByVal value As List(Of System.Diagnostics.Process))
                Me.m_Processes = value
            End Set
        End Property

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            Dim sm As CSOL.Session.SessionManager = CSOL.Session.SessionManager.Create(My.Application.Info.DirectoryPath)
            StartService()
            'StartExecutables()
        End Sub

        Private Sub MyApplication_Shutdown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shutdown
            StopExecutables()
        End Sub

        Friend Sub StartService()
            Dim httpserver As New CSOL.HttpServer(My.Settings.Server.Split(","))
            httpserver.RegisterHandler(New CSOL.TestHandler())

            Dim assemblyFiles() As String = System.IO.Directory.GetFiles(String.Format("{0}\HttpHandlers", My.Application.Info.DirectoryPath), "*.dll")
            For Each file As String In assemblyFiles
                Dim assembly As System.Reflection.Assembly = System.Reflection.Assembly.LoadFile(file)
                Dim ts() As Type = assembly.GetTypes()
                For Each t As Type In ts
                    If t.GetInterface("IHttpHandler") IsNot Nothing Then
                        Dim handler As Object = Activator.CreateInstance(t)
                        httpserver.RegisterHandler(handler)
                    End If
                Next
            Next
        End Sub

        Friend Sub StartExecutables()
            Dim executableFiles() As String = System.IO.Directory.GetFiles(String.Format("{0}\Executables", My.Application.Info.DirectoryPath), "*.exe")
            For Each file As String In executableFiles
                Dim pi As New System.Diagnostics.ProcessStartInfo(file)
                pi.Arguments = String.Format("auto_restart caller=", My.Application.Info.AssemblyName)
                pi.WindowStyle = ProcessWindowStyle.Hidden

                Dim process As System.Diagnostics.Process = System.Diagnostics.Process.Start(pi)
                process.EnableRaisingEvents = True
                AddHandler process.Exited, AddressOf ExecutableExited

                Me.m_Processes.Add(process)
            Next
        End Sub

        Friend Sub StopExecutables()
            For Each process As System.Diagnostics.Process In Me.Processes
                If process IsNot Nothing Then
                    Try
                        process.StartInfo.Arguments = ""
                        process.Kill()
                    Catch ex As Exception

                    End Try
                End If
            Next
        End Sub

        Private Sub ExecutableExited(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim process As System.Diagnostics.Process = sender
            If process.StartInfo.Arguments.StartsWith("auto_restart") Then
                process.Start()
            End If
        End Sub
    End Class
End Namespace

