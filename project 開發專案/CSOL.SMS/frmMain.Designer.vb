﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form 覆寫 Dispose 以清除元件清單。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    '為 Windows Form 設計工具的必要項
    Private components As System.ComponentModel.IContainer

    '注意: 以下為 Windows Form 設計工具所需的程序
    '可以使用 Windows Form 設計工具進行修改。
    '請不要使用程式碼編輯器進行修改。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.tbMain = New System.Windows.Forms.TextBox
        Me.niSMS = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.tDataChecker = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'tbMain
        '
        Me.tbMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tbMain.Location = New System.Drawing.Point(0, 0)
        Me.tbMain.Multiline = True
        Me.tbMain.Name = "tbMain"
        Me.tbMain.ReadOnly = True
        Me.tbMain.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.tbMain.Size = New System.Drawing.Size(584, 262)
        Me.tbMain.TabIndex = 0
        '
        'niSMS
        '
        Me.niSMS.Icon = CType(resources.GetObject("niSMS.Icon"), System.Drawing.Icon)
        Me.niSMS.Text = "簡訊傳送伺服器(亞太)"
        Me.niSMS.Visible = True
        '
        'tDataChecker
        '
        Me.tDataChecker.Enabled = True
        Me.tDataChecker.Interval = 3000
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(584, 262)
        Me.Controls.Add(Me.tbMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "frmMain"
        Me.Text = "簡訊傳送伺服器"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tbMain As System.Windows.Forms.TextBox
    Friend WithEvents niSMS As System.Windows.Forms.NotifyIcon
    Friend WithEvents tDataChecker As System.Windows.Forms.Timer

End Class
