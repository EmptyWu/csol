﻿Public Class frmMain
    Private c_LogPath As String = String.Format("{0}\傳送紀錄{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))
    Private c_ErrorLogPath As String = String.Format("{0}\Error{1}.log", My.Application.Info.DirectoryPath, Now.ToString("yyyyMMdd"))

    Private Sub frmMain_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If System.IO.File.Exists(c_LogPath) Then
            tbMain.Text = System.IO.File.ReadAllText(c_LogPath)
            tbMain.SelectionStart = tbMain.TextLength
        End If
        System.Diagnostics.Debug.WriteLine(My.Application.CommandLineArgs)
    End Sub

    Private Sub frmMain_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            niSMS.Visible = True
        End If
    End Sub

    Private Sub niSMS_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles niSMS.DoubleClick
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        niSMS.Visible = False
    End Sub

    Private Sub frmMain_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        niSMS.Visible = False
    End Sub

    Private Sub tDataChecker_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tDataChecker.Tick
        tDataChecker.Stop()

        Try
            Dim dt As DataTable = CSOL.SMS.AsiaPacific.GetList()
            While dt.Rows.Count > 0
                Dim row As DataRow = dt.Rows(0)

                Dim BatchID As Integer = row("BatchID")
                Dim SMSID As Integer = row("SMSID")
                Dim mobile As String = row("Mobile")
                Dim message As String = row("Message")

                Dim log As String = String.Format("{1}{0}正在傳送：{2}{0}{3}{0}{4}{0}{5}" & vbCrLf, vbTab, Now.ToString("HH:mm:ss.fff"), BatchID, SMSID, mobile, message)
                System.IO.File.AppendAllText(c_LogPath, log)
                Dim Response As String = CSOL.SMS.AsiaPacific.Send(mobile, message)

                log = String.Format("{1}{0}傳送完成：{2}{0}{3}{0}{4}{0}{5}{0}回傳結果：{6}" & vbCrLf, vbTab, Now.ToString("HH:mm:ss.fff"), BatchID, SMSID, mobile, message, Response)
                System.IO.File.AppendAllText(c_LogPath, log)
                CSOL.SMS.AsiaPacific.UpdateStatus(BatchID, SMSID, Response)
                tbMain.Text = System.IO.File.ReadAllText(c_LogPath)
                tbMain.SelectionStart = tbMain.TextLength

                dt = CSOL.SMS.AsiaPacific.GetList()
            End While
        Catch ex As Exception
            System.IO.File.AppendAllText(c_ErrorLogPath, String.Format("{1}{0}{2}", vbTab, Now.ToString("HH:mm:ss:fff"), ex.ToString()))
        End Try

        tDataChecker.Start()
    End Sub
End Class
