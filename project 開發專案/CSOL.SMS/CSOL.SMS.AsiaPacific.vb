﻿Namespace CSOL.SMS
    Public Class AsiaPacific
        Public Shared Function TripleDESEncrypt(ByVal StringToEncrypt As String, ByVal Key As String)
            Dim result As String = String.Empty
            Try
                Dim crypt As New System.Security.Cryptography.TripleDESCryptoServiceProvider
                Dim input() As Byte = System.Text.Encoding.UTF8.GetBytes(StringToEncrypt)
                Key = "aA!1" & Key
                Key = Key.PadRight(20, ".").Substring(0, 20)
                Key = Key & "zZ(9"
                Dim KEY_192() As Byte = System.Text.Encoding.ASCII.GetBytes(Key)
                Dim IV_192() As Byte = System.Text.Encoding.ASCII.GetBytes("!@#$%^&*")
                Dim ms As New System.IO.MemoryStream()
                Dim cs As New System.Security.Cryptography.CryptoStream(ms, crypt.CreateEncryptor(KEY_192, IV_192), System.Security.Cryptography.CryptoStreamMode.Write)
                Dim sw As New System.IO.StreamWriter(cs)
                sw.Write(StringToEncrypt)
                sw.Flush()
                cs.FlushFinalBlock()
                ms.Flush()
                result = System.Convert.ToBase64String(ms.GetBuffer(), Base64FormattingOptions.None, ms.Length)

                ms.Close()
                cs.Close()
                sw.Close()
                ms.Dispose()
                cs.Dispose()
                sw.Dispose()
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return result
        End Function
        Shared Function TripleDESDecrypt(ByVal StringToDecrypt As String, ByVal Key As String)
            Dim result As String = String.Empty
            Try
                Dim crypt As New System.Security.Cryptography.TripleDESCryptoServiceProvider
                Dim input() As Byte = System.Convert.FromBase64String(StringToDecrypt)
                Key = "aA!1" & Key
                Key = Key.PadRight(20, ".").Substring(0, 20)
                Key = Key & "zZ(9"
                Dim KEY_192() As Byte = System.Text.Encoding.ASCII.GetBytes(Key)
                Dim IV_192() As Byte = System.Text.Encoding.ASCII.GetBytes("!@#$%^&*")
                Dim ms As New System.IO.MemoryStream(input)
                Dim cs As New System.Security.Cryptography.CryptoStream(ms, crypt.CreateDecryptor(KEY_192, IV_192), System.Security.Cryptography.CryptoStreamMode.Read)
                Dim sr As New System.IO.StreamReader(cs)
                result = sr.ReadToEnd()
                ms.Close()
                cs.Close()
                sr.Close()
                ms.Dispose()
                cs.Dispose()
                sr.Dispose()
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return result
        End Function

        Public Shared Function Send(ByVal mobile As String, ByVal message As String) As String
            Dim path As String = String.Format("{0}\SMS.xml", My.Application.Info.DirectoryPath)
            If System.IO.File.Exists(path) Then
                Dim config As New System.Xml.XmlDocument()
                config.Load(path)

                Dim root As System.Xml.XmlElement = config.ChildNodes(1)
                Dim url As String = root.Attributes("url").Value
                Dim pass As String = root.Attributes("password").Value
                Dim mob As String = root.Attributes("mobile").Value
                Dim mess As String = root.Attributes("message").Value

                Dim params As New System.Collections.Specialized.NameValueCollection()
                For Each node As System.Xml.XmlElement In config.SelectSingleNode("SMS").ChildNodes
                    Select Case node.Name
                        Case pass
                            params.Add(node.Name, TripleDESDecrypt(node.InnerText, "AsiaPacific for WMCH"))
                        Case mob
                            params.Add(node.Name, mobile)
                        Case mess
                            params.Add(node.Name, message)
                        Case Else
                            params.Add(node.Name, node.InnerText)
                    End Select
                Next

                Dim wc As New CSOL.WebClient()
                wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded")
                Dim result As String = wc.UploadString(url, Convert.GetQuery(params))
                Return result
                'http://isms.apbw.com.tw/api/CheckSMS.php?CID=user11&CPW=123456&MDN=0982347980&T=&ID=4555957
            Else
                Throw New Exception("No SMS Config File!!")
                Return ""
            End If
        End Function

        Public Shared Function GetList() As DataTable
            Dim sql As String = "SELECT TOP 1 * FROM SMS WHERE Status IN ('儲存成功') AND CreateDate <= DATEADD(MINUTE, -3, GETDATE()) ORDER BY CreateDate DESC"
            Dim dt As New DataTable("SMSList")

            Try
                dt = DataBase.LoadToDataTable(sql)
                dt.TableName = "SMSList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function UpdateStatus(ByVal BatchID As Integer, ByVal SMSID As Integer, ByVal Response As String) As Boolean
            Dim Status As String = "傳送成功"
            Dim StatusLog As String = Response
            If Not Response.StartsWith("00") Then
                Status = "傳送失敗"
            End If

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @SMSID INT, @Status NVARCHAR(50), @StatusLog NVARCHAR(MAX)")
            sb.AppendFormat("SELECT @BatchID = {0}, @SMSID = {1}, @Status = N'{2}', @StatusLog = N'{3}'" & vbCrLf, BatchID, SMSID, Status, StatusLog)
            sb.AppendLine("UPDATE SMS SET Status = @Status, StatusLog = StatusLog + ';;' + @StatusLog WHERE BatchID = @BatchID AND SMSID = @SMSID")

            Dim sql As String = sb.ToString()

            Try
                DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace
