﻿Namespace Session
    Public Class Direct
        Public Shared Function Create(ByVal UserID As String, ByVal WorkStationID As String) As String
            Return SessionManager.Session.CreateSession(My.Application.Info.DirectoryPath, UserID, WorkStationID)
        End Function

        Public Shared Function [Get](ByVal ID As String) As SessionManager.Session
            Return SessionManager.Session.GetSession(My.Application.Info.DirectoryPath, ID)
        End Function

        Public Shared Function GetUserID(ByVal ID As String) As String
            Return SessionManager.Session.GetUserID(My.Application.Info.DirectoryPath, ID)
        End Function

        Public Shared Function GetWorkStationID(ByVal ID As String) As String
            Return SessionManager.Session.GetWorkStationID(My.Application.Info.DirectoryPath, ID)
        End Function

        Public Shared Sub Remove(ByVal ID As String)
            SessionManager.Session.RemoveSession(My.Application.Info.DirectoryPath, ID)
        End Sub
    End Class

    Public Class SessionManager
        Private m_BasePath As String
        Private m_Path As String
        Private m_SessionManager As New DataTable("SessionManager")
        Private m_SessionCollection As New Dictionary(Of String, Session)

        Private Sub Init()
            If System.IO.Directory.Exists(Me.m_BasePath) Then
                Try
                    System.IO.Directory.Delete(Me.m_BasePath, True)
                    EnsurePath()
                Catch ex As Exception
                    System.Diagnostics.Debug.WriteLine(ex.ToString())
                End Try
            End If

            Me.m_SessionManager.Columns.Add("ID", System.Type.GetType("System.String"))
            Me.m_SessionManager.Columns.Add("UserID", System.Type.GetType("System.String"))
            Me.m_SessionManager.Columns.Add("CreateDate", System.Type.GetType("System.String"))
            Me.m_SessionManager.PrimaryKey = New DataColumn() {Me.m_SessionManager.Columns("ID")}

            Me.m_SessionManager.WriteXml(Me.m_Path, System.Data.XmlWriteMode.WriteSchema)
        End Sub

        Private Sub Load()
            Me.m_SessionManager.Clear()
            Me.m_SessionCollection.Clear()

            Me.m_SessionManager.ReadXml(Me.m_Path)
            For Each row As DataRow In Me.m_SessionManager.Rows
                Me.m_SessionCollection.Add(row("ID"), New Session(String.Format("{0}\{1}", Me.m_BasePath, row("ID"))))
            Next
        End Sub

        Private Sub EnsurePath()
            System.IO.Directory.CreateDirectory(Me.m_BasePath)
        End Sub

        Private Sub New(ByVal path As String)
            Me.m_BasePath = String.Format("{0}\Sessions", path)
            Me.m_Path = String.Format("{0}\Session", Me.m_BasePath)
            EnsurePath()
        End Sub

        Public Shared Function Create(ByVal path As String) As SessionManager
            Dim sm As New SessionManager(path)
            sm.Init()
            Return sm
        End Function

        Public Shared Function Load(ByVal path As String) As SessionManager
            Dim sm As New SessionManager(path)
            sm.Load()
            Return sm
        End Function

        Public Function CreateSession(ByVal UserID As String, ByVal WorkStationID As String) As String
            Dim uuid As String = Guid.NewGuid().ToString("N")
            Dim current As Date = Now
            Me.m_SessionManager.Rows.Add(uuid, UserID, current.ToString("yyyy-MM-dd HH:mm:ss"))
            Me.m_SessionManager.WriteXml(Me.m_Path, System.Data.XmlWriteMode.WriteSchema)

            Dim session As New Session(String.Format("{0}\{1}", Me.m_BasePath, uuid))
            Me.m_SessionCollection.Add(uuid, session)
            session.Add("UserID", UserID)
            session.Add("WorkStationID", WorkStationID)
            session.Add("CreateDate", current.ToString("yyyy-MM-dd HH:mm:ss"))
            Return uuid
        End Function

        Public Function GetSession(ByVal ID As String) As Session
            If Me.m_SessionCollection.ContainsKey(ID) Then
                Return Me.m_SessionCollection(ID)
            Else
                Return Nothing
            End If
        End Function

        Public Function GetUserID(ByVal ID As String) As String
            Dim session As Session = GetSession(ID)
            If session Is Nothing Then
                Return ""
            Else
                Return session("UserID")
            End If
        End Function

        Public Function GetWorkStationID(ByVal ID As String) As String
            Dim session As Session = GetSession(ID)
            If session Is Nothing Then
                Return ""
            Else
                Return session("WorkStationID")
            End If
        End Function

        Public Sub RemoveSession(ByVal ID As String)
            Dim rows() As DataRow = Me.m_SessionManager.Select(String.Format("ID = '{0}'", ID))
            If rows.Length > 0 Then
                rows(0).Delete()
                Me.m_SessionManager.WriteXml(Me.m_Path, System.Data.XmlWriteMode.WriteSchema)
                Me.m_SessionCollection(ID).Delete()
                Me.m_SessionCollection.Remove(ID)
            End If
        End Sub

        Public Sub ClearAll()
            System.IO.Directory.Delete(Me.m_BasePath, True)
            Me.m_SessionCollection.Clear()
            Me.m_SessionManager.Clear()
        End Sub

        Public Function GetUserCount(ByVal UserID As String) As Integer
            Return Me.m_SessionManager.Select(String.Format("UserID = '{0}'", UserID)).Length
        End Function

        Public Class Session
            Inherits System.Collections.Specialized.NameValueCollection

            Private m_Path As String
            Public Sub New(ByVal path As String)
                Me.m_Path = path
                Me.Load()
            End Sub

            Public Overrides Sub Add(ByVal name As String, ByVal value As String)
                MyBase.Add(name, value)
                Me.Save()
            End Sub

            Public Overrides Sub [Set](ByVal name As String, ByVal value As String)
                MyBase.[Set](name, value)
                Me.Save()
            End Sub

            Public Overrides Sub Remove(ByVal name As String)
                MyBase.Remove(name)
                Me.Save()
            End Sub

            Private Sub Load()
                If System.IO.File.Exists(Me.m_Path) Then
                    Me.Clear()

                    Dim doc As New System.Xml.XmlDocument()
                    doc.Load(Me.m_Path)
                    Dim element As System.Xml.XmlElement = doc.SelectSingleNode("Session")
                    For Each Item As System.Xml.XmlElement In element.ChildNodes
                        MyBase.Add(Item.GetAttribute("name"), Item.GetAttribute("value"))
                    Next
                Else
                    Me.Save()
                End If
            End Sub

            Private Sub Save()
                Dim doc As New System.Xml.XmlDocument()
                Dim element As System.Xml.XmlElement = doc.CreateElement("Session")
                element.SetAttribute("TimeStamp", Now.ToString("yyyy-MM-dd HH:mm:ss"))
                doc.AppendChild(element)
                For Each key As String In Me.Keys
                    Dim item As System.Xml.XmlElement = doc.CreateElement("Item")
                    item.SetAttribute("name", key)
                    item.SetAttribute("value", Me(key))
                    element.AppendChild(item)
                Next
                doc.Save(Me.m_Path)
            End Sub

            Private Function GetTimeStamp() As Date
                Dim doc As New System.Xml.XmlDocument()
                doc.Load(Me.m_Path)
                Dim element As System.Xml.XmlElement = doc.SelectSingleNode("Session")
                Return Date.Parse(element.GetAttribute("TimeStamp"))
            End Function

            Public Sub ReLoad()
                Me.Load()
            End Sub

            Public Sub Delete()
                System.IO.File.Delete(Me.m_Path)
            End Sub

            Public Function IsExpired(ByVal minutes As Integer) As Boolean
                Return (Now - Me.GetTimeStamp()).TotalMinutes > minutes
            End Function

            Public Shared Function CreateSession(ByVal path As String, ByVal UserID As String, ByVal WorkStationID As String) As String
                Dim sm As SessionManager = SessionManager.Load(path)
                Return sm.CreateSession(UserID, WorkStationID)
            End Function

            Public Shared Function GetSession(ByVal path As String, ByVal ID As String) As Session
                Dim sm As SessionManager = SessionManager.Load(path)
                Return sm.GetSession(ID)
            End Function

            Public Shared Function GetUserID(ByVal path As String, ByVal ID As String) As String
                Dim session As Session = GetSession(path, ID)
                If session Is Nothing Then
                    Return ""
                Else
                    Return session("UserID")
                End If
            End Function

            Public Shared Function GetWorkStationID(ByVal path As String, ByVal ID As String) As String
                Dim session As Session = GetSession(path, ID)
                If session Is Nothing Then
                    Return ""
                Else
                    Return session("WorkStationID")
                End If
            End Function

            Public Shared Sub RemoveSession(ByVal path As String, ByVal ID As String)
                Dim sm As SessionManager = SessionManager.Load(path)
                sm.RemoveSession(ID)
            End Sub
        End Class
    End Class
End Namespace