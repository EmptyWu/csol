﻿Namespace DataBaseAccess
    Public Class SMS
        Public Shared Function GetStuCount(ByVal StuClass As String, ByVal StuNo As String, ByVal StuName As String, ByVal NonBlankMobile As Boolean) As Integer
            Dim filters As New List(Of String)
            Dim Filter As String = ""
            If StuClass <> "" Then
                filters.Add(String.Format("c.Name= '{0}'", StuClass))
            End If

            If StuNo <> "" Then
                filters.Add(String.Format("si.No LIKE '{0}%'", StuNo.Replace("*", "%")))
            End If

            If StuName <> "" Then
                filters.Add(String.Format("si.Name LIKE '{0}%'", StuName.Replace("*", "%")))
            End If

            If NonBlankMobile Then
                filters.Add("Mobile <> ''")
            End If

            If filters.Count > 0 Then
                Filter = String.Format("WHERE {0}", String.Join(" AND ", filters.ToArray()))
            End If

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT COUNT(1) AS [Count]")
            sb.AppendLine("FROM StuInfo s")
            sb.AppendLine("LEFT JOIN StuClass sc")
            sb.AppendLine("ON s.ID = sc.StuID AND sc.Status IN ('Normal')")
            sb.AppendLine("LEFT JOIN Class c")
            sb.AppendLine("ON c.ID = sc.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine(Filter)

            Dim sql As String = sb.ToString()

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)("Count")
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return -1
            End Try
        End Function

        Public Shared Function GetStuList(ByVal StuClass As String, ByVal StuNo As String, ByVal StuName As String, ByVal NonBlankMobile As Boolean) As DataTable
            Return GetStuList(StuClass, StuNo, StuName, NonBlankMobile, 1, 50)
        End Function

        Public Shared Function GetStuList(ByVal StuClass As String, ByVal StuNo As String, ByVal StuName As String, ByVal NonBlankMobile As Boolean, ByVal Page As Integer, ByVal Rows As Integer) As DataTable
            Dim filters As New List(Of String)
            Dim Filter As String = ""
            If StuClass <> "" Then
                filters.Add(String.Format("c.Name= '{0}'", StuClass))
            End If

            If StuNo <> "" Then
                filters.Add(String.Format("s.No LIKE '{0}%'", StuNo.Replace("*", "%")))
            End If

            If StuName <> "" Then
                filters.Add(String.Format("s.Name LIKE '{0}%'", StuName.Replace("*", "%")))
            End If

            If NonBlankMobile Then
                filters.Add("Mobile <> ''")
            End If

            If filters.Count > 0 Then
                Filter = String.Format("WHERE {0}", String.Join(" AND ", filters.ToArray()))
            End If

            Dim startIndex As Integer = (Page - 1) * Rows + 1
            Dim endIndex As Integer = Page * Rows

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("WITH si AS (")
            sb.AppendLine("    SELECT ROW_NUMBER() OVER (ORDER BY s.CreateDate DESC, s.No DESC) AS RowID,")
            sb.AppendLine("    s.ID, s.No, s.Name, s.PID, s.Phone, s.Mobile, ISNULL(c.Name, '') AS ClassName,")
            sb.AppendLine("    ISNULL((SELECT Name FROM Classroom WHERE ID = c.PrimaryClassroomID AND Status IN ('Normal')), '') AS ClassroomName,")
            sb.AppendLine("    ISNULL((SELECT X FROM SeatBooking WHERE StuID = s.ID AND ClassID = c.ID AND ClassroomID = c.PrimaryClassroomID AND Status IN ('Normal')), -1) AS X,")
            sb.AppendLine("    ISNULL((SELECT Y FROM SeatBooking WHERE StuID = s.ID AND ClassID = c.ID AND ClassroomID = c.PrimaryClassroomID AND Status IN ('Normal')), -1) AS Y")
            sb.AppendLine("FROM StuInfo s")
            sb.AppendLine("LEFT JOIN StuClass sc")
            sb.AppendLine("ON s.ID = sc.StuID AND sc.Status IN ('Normal')")
            sb.AppendLine("LEFT JOIN Class c")
            sb.AppendLine("ON c.ID = sc.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine(Filter)
            sb.AppendLine(")")
            sb.AppendFormat("SELECT * FROM si WHERE RowID BETWEEN {0} AND {1}" & vbCrLf, startIndex, endIndex)

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("StuList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "StuList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveSMSList(ByVal SMSList As DataTable, ByVal HandlerID As String, ByVal WorkStationID As String) As Integer
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @SMSID INT, @Mobile VARCHAR(50), @Message NVARCHAR(MAX), @Status NVARCHAR(50), @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendLine("SELECT @BatchID = (SELECT ISNULL(MAX(BatchID), 0) + 1 FROM SMS), @Status = '儲存成功', @StatusLog = '儲存成功',")
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            Dim index As Integer = 1
            For Each row As DataRow In SMSList.Rows
                sb.AppendFormat("SELECT @SMSID = {0}, @Mobile = N'{1}', @Message = N'{2}'" & vbCrLf, index, row("Mobile"), row("Message"))

                sb.AppendLine("INSERT INTO SMS(BatchID, SMSID, Mobile, Message, Status, StatusLog, RecDate, HandlerID, WorkStationID, Department)")
                sb.AppendLine("VALUES(@BatchID, @SMSID, @Mobile, @Message, @Status, @StatusLog, @DateTime, @HandlerID, @WorkStationID, @Department)")
                index += 1
            Next
            sb.AppendLine("SELECT @BatchID AS BatchID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)("BatchID")
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return -1
            End Try
        End Function

        Public Shared Function GetSMSList(ByVal BatchID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            If BatchID = 0 Then
                sb.AppendLine("SELECT BatchID, SMSID, Mobile, Message,")
                sb.AppendLine("    Status, RecDate,")
                sb.AppendLine("    (SELECT Name FROM Users WHERE ID = sm.HandlerID) AS Handler,")
                sb.AppendLine("    (SELECT Name FROM WorkStation WHERE ID = sm.WorkStationID) AS WorkStations,")
                sb.AppendLine("    Department")
                sb.AppendLine("FROM SMS sm")
            Else
                sb.AppendLine("DECLARE @BatchID INT")
                sb.AppendFormat("SELECT @BatchID = {0}" & vbCrLf, BatchID)
                sb.AppendLine("SELECT BatchID, SMSID, Mobile, Message,")
                sb.AppendLine("    Status, RecDate,")
                sb.AppendLine("    (SELECT Name FROM Users WHERE ID = sm.HandlerID) AS Handler,")
                sb.AppendLine("    (SELECT Name FROM WorkStation WHERE ID = sm.WorkStationID) AS WorkStations,")
                sb.AppendLine("    Department")
                sb.AppendLine("FROM SMS sm")
                sb.AppendLine("WHERE BatchID = @BatchID")
            End If

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("SMSList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "SMSList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function CancelSMSList(ByVal SMSList As DataTable, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @SMSID INT, @Status NVARCHAR(50), @StatusLog NVARCHAR(MAX)")
            sb.AppendFormat("SELECT @Status = '使用者取消', @StatusLog = '使用者取消::Date:{0},HandlerID:{1},WorkStationID:{2},Department:{3}'" & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            For Each row As DataRow In SMSList.Rows
                sb.AppendFormat("SELECT @BatchID = {0}, @SMSID = {1}" & vbCrLf, row("BatchID"), row("SMSID"))
                sb.AppendLine("UPDATE SMS SET Status = @Status, StatusLog = StatusLog + ';;' + @StatusLog WHERE BatchID = @BatchID AND SMSID = @SMSID AND Status = '儲存成功'")
            Next

            Dim sql As String = sb.ToString()
            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace