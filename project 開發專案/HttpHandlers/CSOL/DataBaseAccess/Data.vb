﻿Namespace DataBaseAccess
    Public Class Data
        Public Shared Function GetNameValuePairs() As DataTable
            Dim sql As String = "SELECT * FROM NameValuePairs"

            Dim dt As New DataTable("NameValuePairs")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "NameValuePairs"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function
    End Class
End Namespace