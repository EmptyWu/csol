﻿Namespace DataBaseAccess
    Public Class [Class]
        Public Shared Function GetList() As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT ID, Name, Type, ListPrice, AccountsReceivable, CONVERT(VARCHAR(50), DateFrom, 111) + '~' + CONVERT(VARCHAR(50), DateTo, 111) AS Date, BookSeat,")
            sb.AppendLine("    (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBookingDetail WHERE ClassID = Class.ID AND Status IN ('Normal') GROUP BY ClassroomID) tbl) AS ClassroomCount")
            sb.AppendLine("FROM Class WHERE Status IN ('Normal')")

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("Class")
            Try
                dt = CSOL.DataBase.LoadToDataTable(Sql)
                dt.TableName = "Class"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetItem(ByVal ID As Integer) As DataTable
            Dim sql As String = String.Format("SELECT * FROM Class WHERE ID = {0} AND  Status IN ('Normal')", ID)

            Dim dt As New DataTable("Class")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Class"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function IsNameExists(ByVal ID As Integer, ByVal Name As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Name NVARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}, @Name = N'{1}'" & vbCrLf, ID, Name)
            sb.AppendLine("SELECT (SELECT COUNT(1) AS [Count] FROM Class WHERE Name = @Name AND Status IN ('Normal'))")
            sb.AppendLine("    - (SELECT COUNT(1) AS [Count] FROM Class WHERE ID = @ID AND Name = @Name AND Status IN ('Normal'))")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                If CSOL.DataBase.LoadToDataTable(sql).Rows(0)(0) = 0 Then
                    Return False
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return True
            End Try

            Return True
        End Function

        Public Shared Function SaveItem(ByVal ID As Integer, ByVal Name As String, ByVal ClassType As String, ByVal ListPrice As Integer, ByVal AccountsReceivable As Integer, ByVal DateFrom As Date, ByVal DateTo As Date, ByVal BookSeat As Boolean, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Name NVARCHAR(50), @ClassType VARCHAR(MAX), @ListPrice INT, @AccountsReceivable INT, @DateFrom DATETIME, @DateTo DATETIME, @BookSeat INT,")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @Name = N'{0}', @ClassType = N'{1}', @ListPrice = {2}, @AccountsReceivable = {3}, @DateFrom = '{4}', @DateTo = '{5}', @BookSeat = {6}," & vbCrLf, Name, ClassType, ListPrice, AccountsReceivable, DateFrom.ToString("yyyy-MM-dd"), DateTo.ToString("yyyy-MM-dd"), IIf(BookSeat, 1, 0))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            If ID > 0 Then
                sb.AppendLine("DECLARE @ID INT")
                sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, ID)

                sb.AppendLine("UPDATE Class SET Name = @Name, Type = @ClassType, ListPrice = @ListPrice, AccountsReceivable = @AccountsReceivable, DateFrom = @DateFrom, DateTo = @DateTo, BookSeat = @BookSeat,")
                sb.AppendLine("    ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department")
                sb.AppendLine("    WHERE ID = @ID")
            Else
                sb.AppendLine("INSERT INTO Class(PrimaryClassID, Name, Type, ListPrice, AccountsReceivable, DateFrom, DateTo, BookSeat, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("    VALUES(-1, @Name, @ClassType, @ListPrice, @AccountsReceivable, @DateFrom, @DateTo, @BookSeat, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            End If

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function RemoveItem(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE Class SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function AddPeriodBooking(ByVal ClassID As Integer, ByVal ClassroomID As Integer, ByVal CourseID As Integer, _
                                                ByVal DateFrom As Date, ByVal DateTo As Date, ByVal Weekdays() As String, _
                                                ByVal TimeFrom As Date, ByVal TimeTo As Date, ByVal AllowMix As Integer, _
                                                ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @ClassID INT, @ClassroomID INT, @CourseID INT, @DatePeriod VARCHAR(50), @Weekdays VARCHAR(50), @TimePeriod VARCHAR(50),")
            sb.AppendLine("    @AllowMix INT, @Date DATETIME,  @TimeFrom DATETIME, @TimeTo DATETIME,")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ClassID = {0}, @ClassroomID = {1}, @CourseID = {2}," & vbCrLf, ClassID, ClassroomID, CourseID)
            sb.AppendFormat("    @DatePeriod = '{0}', @Weekdays = '{1}', @TimePeriod = '{2}'," & vbCrLf, DateFrom.ToString("yyyy-MM-dd") & "~" & DateTo.ToString("yyyy-MM-dd"), String.Join(",", Weekdays), TimeFrom.ToString("HH:mm:ss") & "~" & TimeTo.ToString("HH:mm:ss"))
            sb.AppendFormat("    @AllowMix = {0}, @TimeFrom = '{1}', @TimeTo = '{2}'," & vbCrLf, AllowMix, TimeFrom.ToString("HH:mm:ss"), TimeTo.ToString("HH:mm:ss"))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = @ClassroomID WHERE ID = @ClassID")

            sb.AppendLine("INSERT INTO ClassBooking(ClassID, ClassroomID, CourseID, DatePeriod, Weekdays, TimePeriod, AllowMix, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
            sb.AppendLine("VALUES(@ClassID, @ClassroomID, @CourseID, @DatePeriod, @Weekdays, @TimePeriod, @AllowMix, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            sb.AppendLine("SELECT @BatchID = @@Identity")

            Dim days As Integer = (DateTo - DateFrom).Days
            For i As Integer = 0 To days
                Dim [Date] As Date = DateFrom.AddDays(i)
                If Array.IndexOf(Weekdays, [Date].DayOfWeek.ToString("d")) > -1 Then
                    sb.AppendFormat("SELECT @Date = '{0}'" & vbCrLf, [Date].ToString("yyyy-MM-dd"))
                    sb.AppendLine("INSERT INTO ClassBookingDetail(BatchID, ClassID, ClassroomID, CourseID, Date, TimeFrom, TimeTo, AllowMix, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                    sb.AppendLine("VALUES(@BatchID, @ClassID, @ClassroomID, @CourseID, @Date, @TimeFrom, @TimeTo, @AllowMix, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
                End If
            Next

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function AddSingleBooking(ByVal ClassID As Integer, ByVal ClassroomID As Integer, ByVal CourseID As Integer, _
                                                ByVal [Date] As Date, ByVal TimeFrom As Date, ByVal TimeTo As Date, ByVal AllowMix As Integer, _
                                                ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @ClassID INT, @ClassroomID INT, @CourseID INT, @DatePeriod VARCHAR(50), @Weekdays VARCHAR(50), @TimePeriod VARCHAR(50),")
            sb.AppendLine("    @AllowMix INT, @Date DATETIME,  @TimeFrom DATETIME, @TimeTo DATETIME,")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ClassID = {0}, @ClassroomID = {1}, @CourseID = {2}," & vbCrLf, ClassID, ClassroomID, CourseID)
            sb.AppendFormat("    @DatePeriod = '{0}', @Weekdays = '', @TimePeriod = '{1}'," & vbCrLf, [Date].ToString("yyyy-MM-dd"), TimeFrom.ToString("HH:mm:ss") & "~" & TimeTo.ToString("HH:mm:ss"))
            sb.AppendFormat("    @AllowMix = {0}, @TimeFrom = '{1}', @TimeTo = '{2}'," & vbCrLf, AllowMix, TimeFrom.ToString("HH:mm:ss"), TimeTo.ToString("HH:mm:ss"))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = @ClassroomID WHERE ID = @ClassID")

            sb.AppendLine("INSERT INTO ClassBooking(ClassID, ClassroomID, CourseID, DatePeriod, Weekdays, TimePeriod, AllowMix, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
            sb.AppendLine("VALUES(@ClassID, @ClassroomID, @CourseID, @DatePeriod, @Weekdays, @TimePeriod, @AllowMix, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            sb.AppendLine("SELECT @BatchID = @@Identity")

            sb.AppendFormat("SELECT @Date = '{0}'" & vbCrLf, [Date].ToString("yyyy-MM-dd"))
            sb.AppendLine("INSERT INTO ClassBookingDetail(BatchID, ClassID, ClassroomID, CourseID, Date, TimeFrom, TimeTo, AllowMix, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
            sb.AppendLine("VALUES(@BatchID, @ClassID, @ClassroomID, @CourseID, @Date, @TimeFrom, @TimeTo, @AllowMix, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")


            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetBooking(ByVal ClassID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ClassID INT")
            sb.AppendFormat("SELECT @ClassID = {0}" & vbCrLf, ClassID)
            sb.AppendLine("SELECT ID, ClassroomID,")
            sb.AppendLine("    CASE ClassroomID WHEN (SELECT PrimaryClassroomID FROM Class WHERE ID = @ClassID) THEN '*' ELSE '' END AS IsPrimaryClassroom,")
            sb.AppendLine("    (SELECT ISNULL(Name, '') FROM Class WHERE ID = b.ClassID AND Status = 'Normal') AS ClassName,")
            sb.AppendLine("    (SELECT ISNULL(Name, '') FROM Course WHERE ID = b.CourseID AND Status = 'Normal') AS CourseName,")
            sb.AppendLine("    (SELECT ISNULL(Name, '') FROM Classroom WHERE ID = b.ClassroomID AND Status = 'Normal') AS ClassroomName,")
            sb.AppendLine("    DatePeriod, Weekdays, TimePeriod, CASE WHEN AllowMix = 1 THEN '可' ELSE '不可' END AS AllowMix")
            sb.AppendLine("FROM ClassBooking b WHERE ClassID = @ClassID AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("ClassBooking")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ClassBooking"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetBookingDetail(ByVal BatchID As Integer, ByVal ClassID As Integer, ByVal ClassroomID As Integer, ByVal ShowOverlap As Boolean) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @ClassID INT, @ClassroomID INT")
            sb.AppendFormat("SELECT @BatchID = {0}, @ClassID = {1}, @ClassroomID = {2}" & vbCrLf, BatchID, ClassID, ClassroomID)
            sb.AppendLine("SELECT ID, ISNULL((SELECT Name FROM Class WHERE ID = b.ClassID AND Status = 'Normal'), '') AS ClassName,")
            sb.AppendLine("    ISNULL((SELECT Name FROM Course WHERE ID = b.CourseID AND Status = 'Normal'), '') AS CourseName,")
            sb.AppendLine("    ISNULL((SELECT Name FROM Classroom WHERE ID = b.ClassroomID AND Status = 'Normal'), '') AS ClassroomName,")
            sb.AppendLine("    Date, CONVERT(VARCHAR(50), TimeFrom, 8) + '~' + CONVERT(VARCHAR(50), TimeTo, 8) AS TimePeriod,")
            sb.AppendLine("    CASE WHEN AllowMix = 1 THEN '可' ELSE '不可' END AS AllowMix,")
            sb.AppendLine("    (SELECT CASE WHEN COUNT(1) > 1 THEN 1 ELSE 0 END FROM ClassBookingDetail WHERE ClassroomID = b.ClassroomID AND Date = b.Date")
            sb.AppendLine("        AND (TimeFrom BETWEEN b.TimeFrom AND b.TimeTo OR TimeTo BETWEEN b.TimeFrom AND TimeTo) AND AllowMix = 0 AND b.AllowMix = 0 AND Status IN ('Normal')) AS Overlaped")
            sb.AppendLine("FROM ClassBookingDetail b")
            sb.AppendLine("WHERE (BatchID = @BatchID OR @BatchID = -1) AND ClassID = @ClassID AND (ClassroomID = @ClassroomID OR @ClassroomID = -1) AND Status IN ('Normal')")
            If ShowOverlap Then
                sb.AppendLine("    OR (SELECT COUNT(1) FROM ClassBookingDetail WHERE ClassroomID = b.ClassroomID AND Date = b.Date")
                sb.AppendLine("        AND (TimeFrom BETWEEN b.TimeFrom AND b.TimeTo OR TimeTo BETWEEN b.TimeFrom AND b.TimeTo) AND AllowMix = 0 AND b.AllowMix = 0 AND Status IN ('Normal')) > 1")
            End If

            Dim sql As String = sb.ToString()

            Dim dt As New DataTable("ClassBookingDetail")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ClassBookingDetail"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function RemoveBooking(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @ClassID INT, @ClassroomID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE ClassBooking SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @ID")
            sb.AppendLine("UPDATE ClassBookingDetail SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE BatchID = @ID")

            sb.AppendLine("SELECT @ClassID = ClassID, @ClassroomID = ClassroomID FROM ClassBooking WHERE ID = @ID")
            sb.AppendLine("IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = -1 WHERE ID = @ClassID")
            sb.AppendLine("ELSE IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE ClassroomID = @ClassroomID AND Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    AND (SELECT PrimaryClassroomID FROM Class WHERE ID = @ClassID) = @ClassroomID")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = (SELECT TOP 1 ClassroomID FROM ClassBooking WHERE ClassID = @ClassID AND Status IN ('Normal') ORDER BY CreateDate ASC) WHERE ID = @ClassID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function RemoveBookingDetail(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BatchID INT, @ID INT, @ClassID INT, @ClassroomID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("SELECT @BatchID = BatchID FROM ClassBookingDetail WHERE ID = @ID")
            sb.AppendLine("UPDATE ClassBookingDetail SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @ID")
            sb.AppendLine("IF (SELECT COUNT(1) FROM ClassBookingDetail WHERE BatchID = @BatchID AND Status IN ('Normal')) = 0")
            sb.AppendLine("    UPDATE ClassBooking SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @BatchID")

            sb.AppendLine("SELECT @ClassID = ClassID, @ClassroomID = ClassroomID FROM ClassBooking WHERE ID = @ID")
            sb.AppendLine("IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = -1 WHERE ID = @ClassID")
            sb.AppendLine("ELSE IF (SELECT COUNT(1) FROM (SELECT ClassroomID FROM ClassBooking WHERE ClassroomID = @ClassroomID AND Status IN ('Normal') GROUP BY ClassroomID) tbl) = 0")
            sb.AppendLine("    AND (SELECT PrimaryClassroomID FROM Class WHERE ID = @ClassID) = @ClassroomID")
            sb.AppendLine("    UPDATE Class SET PrimaryClassroomID = (SELECT TOP 1 ClassroomID FROM ClassBooking WHERE ClassID = @ClassID AND Status IN ('Normal') ORDER BY CreateDate ASC) WHERE ID = @ClassID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function SetPrimaryClassroom(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ClassID INT, @ClassroomID INT, @ID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @StatusLog = N'ChangePrimaryClassroom DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("SELECT @ClassID = ClassID, @ClassroomID = ClassroomID FROM ClassBooking WHERE ID = @ID")
            sb.AppendLine("UPDATE Class SET PrimaryClassroomID = @ClassroomID, StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = @ClassID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace