﻿Namespace DataBaseAccess
    Friend Class Accounting
        Public Shared Function GetDepartments(ByVal DateBegin As Date, ByVal DateEnd As Date) As String()
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @DateBegin DATETIME, @DateEnd DATETIME")
            sb.AppendFormat("SELECT @DateBegin = '{0}', @DateEnd = '{1}'" & vbCrLf, DateBegin.ToString("yyyy-MM-dd 00:00:00"), DateEnd.ToString("yyyy-MM-dd 23:59:59"))
            sb.AppendLine("SELECT ModifyDepartment AS Department FROM StuClassFee2 WHERE ModifyDate BETWEEN @DateBegin AND @DateEnd GROUP BY ModifyDepartment")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim departments As New List(Of String)
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                For Each row As DataRow In dt.Rows
                    departments.Add(row("Department"))
                Next
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return departments.ToArray()
        End Function

        Public Shared Function GetDailyReport(ByVal SomeDate As Date) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Date DATETIME")
            sb.AppendFormat("SELECT @Date = '{0}'" & vbCrLf, SomeDate.ToString("yyyy-MM-dd"))
            sb.AppendLine("SELECT f.Department, f.SumType, c.Name AS ClassName, s.X, s.Y , si.Name, f.Receipt, f.Amount,")
            sb.AppendLine("    c.AccountsReceivable - (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = si.ID AND ClassID = c.ID  AND SumType = '折扣' AND Removed = 0)")
            sb.AppendLine("        - (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = si.ID AND ClassID = c.ID  AND SumType = '收入' AND Removed = 0)")
            sb.AppendLine("        + (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = si.ID AND ClassID = c.ID  AND SumType = '支出' AND Removed = 0) AS FeeOwe,")
            sb.AppendLine("    f.PayType, f.PayMethod, f.Comment")
            sb.AppendLine("FROM StuClassFee2 f")
            sb.AppendLine("INNER JOIN StuInfo2 si")
            sb.AppendLine("ON f.StuID = si.ID AND si.Removed = 0")
            sb.AppendLine("INNER JOIN Class2 c")
            sb.AppendLine("ON f.ClassID = c.ID")
            sb.AppendLine("INNER JOIN Seat s")
            sb.AppendLine("ON s.ClassID = c.ID AND s.StuID = si.ID AND s.Removed = 0")
            sb.AppendLine("WHERE f.Removed = 0 AND f.ModifyDate BETWEEN dbo.FirstSecondOfDay(@Date) AND dbo.LastSecondOfDay(@Date)")
            sb.AppendLine("ORDER BY f.Department,")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeeSumType' AND Text = f.SumType),")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeePayType' AND Text = f.PayType),")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeePayMethod' AND Text = f.PayMethod)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("DailyReport")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "DailyReport"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetMonthlyReport(ByVal SomeDate As Date)
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BeginDate DATETIME, @EndDate DATETIME")
            sb.AppendFormat("SELECT @BeginDate = dbo.FirstSecondOfMonth('{0}'), @EndDate = dbo.LastSecondOfMonth('{0}')" & vbCrLf, SomeDate.ToString("yyyy-MM-dd"))
            sb.AppendLine("SELECT CAST(CAST(Year(ModifyDate) AS VARCHAR(4)) + '-' + CAST(Month(ModifyDate) AS VARCHAR(2)) + '-' + CAST(Day(ModifyDate) AS VARCHAR(2)) AS DATE) AS [Date],")
            sb.AppendLine("    ModifyDepartment AS Department, SumType, PayType, PayMethod, SUM(amount) AS Amount")
            sb.AppendLine("FROM StuClassFee2")
            sb.AppendLine("WHERE ModifyDate BETWEEN @BeginDate AND @EndDate")
            sb.AppendLine("GROUP BY CAST(CAST(Year(ModifyDate) AS VARCHAR(4)) + '-' + CAST(Month(ModifyDate) AS VARCHAR(2)) + '-' + CAST(Day(ModifyDate) AS VARCHAR(2)) AS DATE),")
            sb.AppendLine("    ModifyDepartment, SumType, PayType, PayMethod")
            sb.AppendLine("ORDER BY CAST(CAST(Year(ModifyDate) AS VARCHAR(4)) + '-' + CAST(Month(ModifyDate) AS VARCHAR(2)) + '-' + CAST(Day(ModifyDate) AS VARCHAR(2)) AS DATE),")
            sb.AppendLine("    (SELECT Code FROM Definitions where Name = 'FeeSumType' AND Text = SumType),")
            sb.AppendLine("    (SELECT Code FROM Definitions where Name = 'FeePayType' AND Text = PayType),")
            sb.AppendLine("    (SELECT Code FROM Definitions where Name = 'FeePayMethod' AND Text = PayMethod)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("MonthlyReport")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "MonthlyReport"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetYearlyReport(ByVal SomeDate As Date)
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @BeginDate DATETIME, @EndDate DATETIME")
            sb.AppendFormat("SELECT @BeginDate = dbo.FirstSecondOfYear('{0}'), @EndDate = dbo.LastSecondOfYear('{0}')" & vbCrLf, SomeDate.ToString("yyyy-MM-dd"))
            sb.AppendLine("SELECT CAST(CAST(YEAR(ModifyDate) AS VARCHAR(4)) + '-' + CAST(MONTH(ModifyDate) AS VARCHAR(2)) + '-01' AS DATE) AS [Month],")
            sb.AppendLine("    ModifyDepartment AS Department, SumType, PayType, PayMethod, SUM(Amount) AS Amount")
            sb.AppendLine("FROM StuClassFee2")
            sb.AppendLine("WHERE ModifyDate BETWEEN @BeginDate AND @EndDate")
            sb.AppendLine("GROUP BY CAST(CAST(YEAR(ModifyDate) AS VARCHAR(4)) + '-' + CAST(MONTH(ModifyDate) AS VARCHAR(2)) + '-01' AS DATE),")
            sb.AppendLine("    ModifyDepartment, SumType, PayType, PayMethod")
            sb.AppendLine("ORDER BY CAST(CAST(YEAR(ModifyDate) AS VARCHAR(4)) + '-' + CAST(MONTH(ModifyDate) AS VARCHAR(2)) + '-01' AS DATE),")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeeSumType' AND Text = SumType),")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeePayType' AND Text = PayType),")
            sb.AppendLine("    (SELECT Code FROM Definitions WHERE Name = 'FeePayMethod' AND Text = PayMethod)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("YearlyReport")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "YearlyReport"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function
    End Class
End Namespace