﻿Namespace DataBaseAccess
    Public Class Course
        Public Shared Function GetList() As DataTable
            Dim sql As String = "SELECT ID, TeacherID, Name, Description FROM Course WHERE Status IN ('Normal')"

            Dim dt As New DataTable("Course")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Course"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetListWithName() As DataTable
            Dim sql As String = "SELECT ID, (SELECT ISNULL(Name, '') FROM Teacher WHERE ID = Course.TeacherID) AS Teacher, Name, Description FROM Course WHERE Status IN ('Normal')"

            Dim dt As New DataTable("Course")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Course"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveList(ByVal dt As DataTable, ByVal deletedIDs() As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @TeacherID INT, @Name NVARCHAR(50), @Description NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            For Each row As DataRow In dt.Rows
                sb.AppendFormat("SELECT @ID = {0}, @TeacherID = {1}, @Name = N'{2}', @Description = N'{3}'" & vbCrLf, row("ID"), row("TeacherID"), row("Name"), row("Description"))

                If row("ID") > 0 Then
                    sb.AppendLine("UPDATE Course SET TeacherID = @TeacherID, Name = @Name, Description = @Description WHERE ID = @ID")
                Else
                    sb.AppendLine("INSERT INTO Course(TeacherID, Name, Description, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                    sb.AppendLine("    VALUES(@TeacherID, @Name, @Description, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
                End If
            Next

            For Each ID As Integer In deletedIDs
                sb.AppendFormat("DELETE FROM Course WHERE ID = {0}" & vbCrLf, ID)
            Next

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace