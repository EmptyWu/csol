﻿Namespace DataBaseAccess
    Public Class Users
        Public Shared Function GetGroup() As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT Name, Code, Text FROM Definitions WHERE Name = 'UserGroup' OR Name LIKE 'UserGroupPriority-%'")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("Groups")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Groups"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveGroup(ByVal Group As String, ByVal GroupPriorities() As String) As Boolean
            If RemoveGroup(Group) = False Then
                Return False
            End If

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Group NVARCHAR(50), @GroupPriority NVARCHAR(50)")
            sb.AppendFormat("SELECT @Group = N'{0}'" & vbCrLf, Group)
            sb.AppendLine("INSERT INTO Definitions(Name, Code, Text)")
            sb.AppendLine("VALUES('UserGroup', (SELECT ISNULL(MAX(Code), 0) + 1 FROM Definitions WHERE Name = 'UserGroup'), @Group)")
            For Each GroupPriority As String In GroupPriorities
                sb.AppendFormat("SELECT @GroupPriority = N'{0}'" & vbCrLf, GroupPriority)
                sb.AppendLine("INSERT INTO Definitions(Name, Code, Text)")
                sb.AppendLine("VALUES('UserGroupPriority-' + @Group, (SELECT ISNULL(MAX(Code), 0) + 1 FROM Definitions WHERE Name = 'UserGroupPriority-' + @Group), @GroupPriority)")
            Next

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try
        End Function

        Public Shared Function RemoveGroup(ByVal Group As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Group NVARCHAR(50)")
            sb.AppendFormat("SELECT @Group = N'{0}'" & vbCrLf, Group)
            sb.AppendLine("DELETE FROM Definitions WHERE (Name = 'UserGroup' AND Text = @Group) OR (Name = 'UserGroupPriority-' + @Group)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try
        End Function

        Public Shared Function GetUser() As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT ID, Username, Name, UserGroup, Priority FROM Users WHERE Removed = 0")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("Users")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Users"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveUser(ByVal ID As Integer, ByVal Username As String, ByVal Password As String, ByVal Name As String, ByVal UserGroup As String, ByVal Priority As String, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Username VARCHAR(50), @Password VARCHAR(50), @Name NVARCHAR(50), @UserGroup NVARCHAR(50), @Priority NVARCHAR(50),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @Username = '{0}', @Name = N'{1}', @UserGroup = N'{2}', @Priority = N'{3}'," & vbCrLf, Username, Name, UserGroup, Priority)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            If ID > 0 Then
                sb.AppendLine("DECLARE @ID INT")
                sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, ID)

                sb.AppendLine("UPDATE Users SET Username = @Username, Name = @Name, UserGroup = @UserGroup, Priority = @Priority,")
                sb.AppendLine("    ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department")
                sb.AppendLine("    WHERE ID = @ID")

                If Password.Trim() <> "" Then
                    sb.AppendFormat("SELECT @Password = SUBSTRING(sys.fn_varbintohexstr(HASHBYTES('md5', '{0}')), 3, 32)" & vbCrLf, Password)
                    sb.AppendLine("UPDATE Users SET Password = @Password WHERE ID = @ID")
                End If
            Else
                sb.AppendFormat("SELECT @Password = SUBSTRING(sys.fn_varbintohexstr(HASHBYTES('md5', '{0}')), 3, 32)" & vbCrLf, Password)
                sb.AppendLine("INSERT INTO Users(Username, Password, Name, UserGroup, Priority, Removed, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("VALUES(@Username, @Password, @Name, @UserGroup, @Priority, 0, @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            End If

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try
        End Function

        Public Shared Function RemoveUser(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT,")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendLine("UPDATE Users SET Removed = 1, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try
        End Function

        Public Shared Function IsUsernameExists(ByVal ID As Integer, ByVal Username As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Username NVARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}, @Username = N'{1}'" & vbCrLf, ID, Username)
            sb.AppendLine("SELECT COUNT(1) FROM Users WHERE ID <> @ID AND Username = @Username AND Removed = 0")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows(0)(0) = 0 Then
                    Return False
                Else
                    Return True
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return True
            End Try
        End Function
    End Class
End Namespace