﻿Namespace DataBaseAccess
    Public Class StuInfo
        Public Shared Function GetClassNames() As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT ID, Name FROM Class WHERE Status IN ('Normal')")

            Dim sql As String = sb.ToString()
            Dim dt As New DataTable("ClassNames")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ClassNames"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetCount(ByVal Filter As String) As Integer
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT COUNT(1) AS [Count]")
            sb.AppendLine("FROM StuInfo s")
            sb.AppendLine("LEFT JOIN StuClass sc")
            sb.AppendLine("ON s.ID = sc.StuID AND sc.Status IN ('Normal')")
            sb.AppendLine("LEFT JOIN Class c")
            sb.AppendLine("ON c.ID = sc.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine(Filter)
            sb.AppendLine("GROUP BY s.ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)("Count")
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return -1
            End Try
        End Function

        Public Shared Function List(ByVal Rows As Integer, ByVal Page As Integer, ByVal Filter As String) As DataTable
            Dim startIndex As Integer = (Page - 1) * Rows + 1
            Dim endIndex As Integer = Page * Rows
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("WITH si AS (")
            sb.AppendLine("    SELECT ROW_NUMBER() OVER (ORDER BY s.CreateDate DESC, s.No DESC) AS RowID,")
            sb.AppendLine("    s.ID, s.No, s.Name, s.PID, s.Phone, s.Mobile")
            sb.AppendLine("FROM StuInfo s")
            sb.AppendLine("LEFT JOIN StuClass sc")
            sb.AppendLine("ON s.ID = sc.StuID AND sc.Status IN ('Normal')")
            sb.AppendLine("LEFT JOIN Class c")
            sb.AppendLine("ON c.ID = sc.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine(Filter)
            sb.AppendLine("GROUP BY s.ID, s.No, s.Name, s.PID, s.Phone, s.Mobile, s.CreateDate")
            sb.AppendLine(")")
            sb.AppendFormat("SELECT * FROM si WHERE RowID BETWEEN {0} AND {1}" & vbCrLf, startIndex, endIndex)

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("List")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function Detail(ByVal StuID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendFormat("SELECT * FROM StuInfo WHERE ID = {0}", StuID)

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("Detail")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function Save(ByVal StuID As Integer, ByVal No As String, ByVal Name As String, ByVal EngName As String, _
                                    ByVal PID As String, ByVal Type As String, ByVal Sex As String, ByVal Birth As Date, ByVal School As String, _
                                    ByVal Phone As String, ByVal Mobile As String, ByVal Address As String, ByVal OtherContacts As String, _
                                    ByVal SalesGroup As String, ByVal SalesManager As String, ByVal Sales As String, ByVal Comment As String, _
                                    ByVal HandlerID As String, ByVal WorkStationID As String)

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @No VARCHAR(50), @Name NVARCHAR(50), @EngName VARCHAR(50),")
            sb.AppendLine("    @PID VARCHAR(50), @Type NVARCHAR(50), @Sex VARCHAR(50), @Birth DATETIME, @School NVARCHAR(MAX),")
            sb.AppendLine("    @Phone VARCHAR(50), @Mobile VARCHAR(50), @Address NVARCHAR(400), @Contacts NVARCHAR(MAX),")
            sb.AppendLine("    @SalesGroup NVARCHAR(50), @SalesManager NVARCHAR(50), @Sales NVARCHAR(50), @Comment NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @StuID = {0}, @No = '{1}', @Name = N'{2}', @EngName = '{3}'," & vbCrLf, StuID, No, Name, EngName)
            sb.AppendFormat("    @PID = '{0}', @Type = N'{1}', @Sex = '{2}', @Birth = '{3}', @School = N'{4}'," & vbCrLf, PID, Type, Sex, Birth.ToString("yyyy/MM/dd"), School)
            sb.AppendFormat("    @Phone = '{0}', @Mobile = '{1}', @Address = N'{2}', @Contacts = N'{3}'," & vbCrLf, Phone, Mobile, Address, OtherContacts)
            sb.AppendFormat("    @SalesGroup = N'{0}', @SalesManager = N'{1}', @Sales = N'{2}', @Comment = N'{3}'," & vbCrLf, SalesGroup, SalesManager, Sales, Comment)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            If StuID = -1 Then
                sb.AppendLine("INSERT INTO StuInfo(No, Name, EngName, PID, Type, Sex, Birth, School, Phone, Mobile, Address, Contacts, SalesGroup, SalesManager, Sales, Comment, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("VALUES(@No, @Name, @EngName, @PID, @Type, @Sex, @Birth, @School, @Phone, @Mobile, @Address, @Contacts, @SalesGroup, @SalesManager, @Sales, @Comment, 'Normal', '', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
                sb.AppendLine("SELECT @StuID = @@Identity")
            Else
                sb.AppendLine("UPDATE StuInfo SET No = @No, Name = @Name, EngName = @EngName, PID = @PID, Type = @Type, Sex = @Sex, Birth = @Birth, School = @School, Phone = @Phone, Mobile = @Mobile, Address = @Address, Contacts = @Contacts, SalesGroup = @SalesGroup, SalesManager = @SalesManager, Sales = @Sales, Comment = @Comment, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department")
                sb.AppendLine("WHERE ID = @StuID")
            End If
            sb.AppendLine("SELECT @StuID AS StuID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    StuID = dt.Rows(0)("StuID")
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
            End Try

            Return StuID
        End Function

        Public Shared Function Remove(ByVal StuID As String, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @StuID = {0}," & vbCrLf, StuID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE StuInfo SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = @StuID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetNextStuNo(ByVal WorkStationID As String) As String
            Dim sb As New System.Text.StringBuilder()
            sb.AppendFormat("SELECT TOP 1 No FROM StuInfo WHERE Status IN ('Normal') AND Department = N'{0}' ORDER BY CreateDate DESC, No DESC", WorkStation.GetDepartment(WorkStationID))

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim StuNo As String = String.Format("{0:000}00001", Now.Year - 1911)
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    StuNo = dt.Rows(0)("No")
                    Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(StuNo, "\d+$")
                    If matches.Count = 0 Then
                        Dim i As Integer = 1
                        Do
                            StuNo &= i.ToString("d5")
                            i += 1
                        Loop While IsStuNoExists(StuNo)
                    Else
                        Do
                            Dim str As String = System.Text.RegularExpressions.Regex.Matches(StuNo, "\d+$")(0).Value
                            Dim digit As Integer = Integer.Parse(str) + 1
                            Dim format As String = String.Format("d{0}", str.Length)
                            StuNo = StuNo.Replace(str, digit.ToString(format))
                        Loop While IsStuNoExists(StuNo)
                    End If
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return StuNo
        End Function

        Public Shared Function IsStuNoExists(ByVal StuNo As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @No VARCHAR(50)")
            sb.AppendFormat("SELECT @No = '{0}'", StuNo)

            sb.AppendLine("SELECT COUNT(1) FROM StuInfo WHERE No = @No AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)(0) > 0
            Catch ex As Exception
                Return True
            End Try
        End Function
    End Class
End Namespace