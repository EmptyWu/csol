﻿Namespace DataBaseAccess
    Public Class Classroom
        Public Shared Function GetList() As DataTable
            Dim sql As String = "SELECT ID, Name, Columns, Rows, Disables FROM Classroom WHERE Status IN ('Normal')"

            Dim dt As New DataTable("Classroom")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Classroom"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetItem(ByVal ID As Integer) As DataTable
            Dim sql As String = String.Format("SELECT * FROM Classroom WHERE ID = {0}", ID)

            Dim dt As New DataTable("Classroom")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Classroom"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function IsNameExists(ByVal ID As Integer, ByVal Name As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Name NVARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}, @Name = N'{1}'" & vbCrLf, ID, Name)
            sb.AppendLine("SELECT (SELECT COUNT(1) AS [Count] FROM Classroom WHERE Name = @Name AND Status IN ('Normal'))")
            sb.AppendLine("    - (SELECT COUNT(1) AS [Count] FROM Classroom WHERE ID = @ID AND Name = @Name AND Status IN ('Normal'))")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                If CSOL.DataBase.LoadToDataTable(sql).Rows(0)(0) = 0 Then
                    Return False
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return True
            End Try

            Return True
        End Function

        Public Shared Function SaveItem(ByVal ID As Integer, ByVal Name As String, ByVal Columns As Integer, ByVal Rows As Integer, _
                                        ByVal LanesX() As Integer, ByVal LanesY() As Integer, ByVal Disables() As System.Drawing.Point, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim Lane As String = CSOL.Convert.PowerJoin(New String()() {Array.ConvertAll(Of Integer, String)(LanesX, AddressOf Convert.ToString), _
                                                                        Array.ConvertAll(Of Integer, String)(LanesY, AddressOf Convert.ToString)})
            Dim Disable As String = CSOL.Convert.PointsToString(Disables)

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Name NVARCHAR(50), @Columns INT, @Rows INT, @Lanes VARCHAR(MAX), @Disables VARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @Name = N'{0}', @Columns = {1}, @Rows = {2}," & vbCrLf, Name, Columns, Rows)
            sb.AppendFormat("    @Lanes = '{0}', @Disables = '{1}', @DateTime = GETDATE()," & vbCrLf, Lane, Disable)
            sb.AppendFormat("    @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            If ID > 0 Then
                sb.AppendLine("DECLARE @ID INT")
                sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, ID)

                sb.AppendLine("UPDATE Classroom SET Name = @Name, Columns = @Columns, Rows = @Rows, Lanes = @Lanes, Disables = @Disables,")
                sb.AppendLine("    ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department")
                sb.AppendLine("WHERE ID = @ID")
            Else
                sb.AppendLine("INSERT INTO Classroom(Name, Columns, Rows, Lanes, Disables, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("    VALUES(@Name, @Columns, @Rows, @Lanes, @Disables, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            End If

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function RemoveItem(ByVal ID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, ID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE Classroom SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = @ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.Write(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace