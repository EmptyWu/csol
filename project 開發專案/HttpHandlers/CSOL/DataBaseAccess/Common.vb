﻿Namespace DataBaseAccess
    Friend Class Common
        Friend Shared Function SetDefinitions(ByVal Name As String, ByVal Texts() As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Name VARCHAR(50)")
            sb.AppendFormat("SELECT @Name = '{0}'" & vbCrLf, Name)
            sb.AppendLine("DELETE FROM Definitions WHERE Name = @Name")

            sb.AppendLine("DECLARE @Code INT, @Text VARCHAR(50)")
            For i As Integer = 0 To Texts.Length - 1
                sb.AppendFormat("SELECT @Code = {0}, @Text = '{1}'" & vbCrLf, i, Texts(i))
                sb.AppendLine("INSERT INTO Definitions(Name, Code, Text) VALUES(@Name, @Code, @Text)")
            Next

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                Return False
            End Try
            Return True
        End Function

        Friend Shared Function GetDefinitions(ByVal Name As String) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Name VARCHAR(50)")
            sb.AppendFormat("SELECT @Name = '{0}'" & vbCrLf, Name)
            sb.AppendLine("SELECT Code, Text FROM Definitions WHERE Name = @Name")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As DataTable = Nothing
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = Name
            Catch ex As Exception

            End Try
            Return dt
        End Function

        Friend Shared Function GetDefinitionText(ByVal Name As String, ByVal Code As Integer) As String
            Dim dt As DataTable = GetDefinitions(Name)
            Dim text As String = ""
            If dt IsNot Nothing Then
                Dim rows() As DataRow = dt.Select(String.Format("Code = {0}", Code))
                If rows.Length > 0 Then
                    text = rows(0)("Text")
                End If
            End If
            Return text
        End Function

        Friend Shared Function GetSex() As DataTable
            Return GetDefinitions("Sex")
        End Function

        Friend Shared Function SetSex(ByVal Sexes() As String) As Boolean
            Return SetDefinitions("Sex", Sexes)
        End Function

        Friend Shared Function GetStuType() As DataTable
            Return GetDefinitions("StuType")
        End Function

        Friend Shared Function SetStuType(ByVal StuTypes() As String) As Boolean
            Return SetDefinitions("StuType", StuTypes)
        End Function

        Friend Shared Function GetSchType() As DataTable
            Return GetDefinitions("SchType")
        End Function

        Friend Shared Function SetSchType(ByVal SchTypes() As String) As Boolean
            Return SetDefinitions("SchType", SchTypes)
        End Function

        Friend Shared Function GetSchName(ByVal SchType As String) As DataTable
            Return GetDefinitions(String.Format("SchType-{0}", SchType))
        End Function

        Friend Shared Function SetSchName(ByVal SchType As String, ByVal SchNames() As String) As Boolean
            Return SetDefinitions(String.Format("SchType-{0}", SchType), SchNames)
        End Function

        Friend Shared Function GetSchGrade() As DataTable
            Return GetDefinitions("SchGrade")
        End Function

        Friend Shared Function SetSchGrade(ByVal SchGrades() As String) As Boolean
            Return SetDefinitions("SchGrade", SchGrades)
        End Function

        Friend Shared Function GetSchClassNo() As DataTable
            Return GetDefinitions("SchClassNo")
        End Function

        Friend Shared Function SetSchClassNo(ByVal SchClassNos() As String) As Boolean
            Return SetDefinitions("SchClassNo", SchClassNos)
        End Function

        Friend Shared Function GetSalesList() As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("SELECT ID AS Code, Name AS Text FROM OrgUser WHERE IsSales = 1 ORDER BY Name ASC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As DataTable = Nothing
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Sales"
            Catch ex As Exception

            End Try
            Return dt
        End Function

        Friend Shared Function GetSalesGroup() As DataTable
            Return GetDefinitions("SalesGroup")
        End Function

        Friend Shared Function SetSalesGroup(ByVal SalesGroups() As String) As Boolean
            Return SetDefinitions("SalesGroup", SalesGroups)
        End Function


        Friend Shared Function GetContactTitle() As DataTable
            Return GetDefinitions("ContactTitle")
        End Function

        Friend Shared Function SetContactTitle(ByVal ContactTitles() As String) As Boolean
            Return SetDefinitions("ContactTitle", ContactTitles)
        End Function

        Friend Shared Function GetContactJob() As DataTable
            Return GetDefinitions("ContactJob")
        End Function

        Friend Shared Function SetContactJob(ByVal ContactJobs() As String) As Boolean
            Return SetDefinitions("ContactJob", ContactJobs)
        End Function

        Friend Shared Function GetContactPostCode() As DataTable
            Return GetDefinitions("ContactPostCode")
        End Function

        Friend Shared Function SetContactPostCode(ByVal ContactPostCodes() As String) As Boolean
            Return SetDefinitions("ContactPostCode", ContactPostCodes)
        End Function

        Friend Shared Function GetContactAddress(ByVal ContactPostCode As String) As DataTable
            Return GetDefinitions(String.Format("ContactPostCode-{0}", ContactPostCode))
        End Function

        Friend Shared Function SetContactAddress(ByVal ContactPostCode As String, ByVal ContactAddresses() As String) As Boolean
            Return SetDefinitions(String.Format("ContactPostCode-{0}", ContactPostCode), ContactAddresses)
        End Function
    End Class
End Namespace