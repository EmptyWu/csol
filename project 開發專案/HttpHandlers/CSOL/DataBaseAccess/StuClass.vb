﻿Namespace DataBaseAccess
    Public Class StuClass
        Public Shared Function GetList(ByVal StuID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT")
            sb.AppendFormat("SELECT @StuID = {0}" & vbCrLf, StuID)

            sb.AppendLine("SELECT sc.ID, sc.ClassID, c.Name AS ClassName, c.Type AS ClassType, c.AccountsReceivable,")
            sb.AppendLine("    c.AccountsReceivable - ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '折扣' AND Status IN ('Normal')), 0) AS AfterDiscount,")
            sb.AppendLine("    ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '收入' AND Status IN ('Normal')), 0) AS Paid,")
            sb.AppendLine("    ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '支出' AND Status IN ('Normal')), 0) AS MoneyBack,")
            sb.AppendLine("    c.AccountsReceivable - ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '折扣' AND Status IN ('Normal')), 0)")
            sb.AppendLine("        - ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '收入' AND Status IN ('Normal')), 0)")
            sb.AppendLine("        + ISNULL((SELECT SUM(Amount) FROM StuFee WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '支出' AND Status IN ('Normal')), 0) AS Owe")
            sb.AppendLine("FROM StuClass sc")
            sb.AppendLine("INNER JOIN Class c")
            sb.AppendLine("ON c.ID = sc.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine("WHERE sc.StuID = @StuID AND sc.Status IN ('Normal')")
            sb.AppendLine("ORDER BY sc.CreateDate DESC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("StuClassList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "StuClassList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetFeeList(ByVal StuID As Integer, ByVal ClassID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @ClassID INT")
            sb.AppendFormat("SELECT @StuID = {0}, @ClassID = {1}" & vbCrLf, StuID, ClassID)
            sb.AppendLine("SELECT f.ID, c.Name AS ClassName, f.Receipt, f.Amount, f.FeeType, f.PayMethod, f.Extra, f.Comment")
            sb.AppendLine("FROM StuFee f")
            sb.AppendLine("INNER JOIN Class c")
            sb.AppendLine("ON c.ID = f.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine("WHERE f.StuID = @StuID AND f.ClassID = @ClassID AND f.Status IN ('Normal')")
            sb.AppendLine("ORDER BY f.CreateDate DESC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("StuFeeList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "StuFeeList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveItem(ByVal StuID As Integer, ByVal ClassID As Integer, _
                                        ByVal SalesGroup As String, ByVal SalesManager As String, ByVal Sales As String, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @ClassID INT,")
            sb.AppendLine("    @SalesGroup NVARCHAR(50), @SalesManager NVARCHAR(50), @Sales NVARCHAR(50), @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @StuID = {0}, @ClassID = {1}," & vbCrLf, StuID, ClassID)
            sb.AppendFormat("    @SalesGroup = N'{0}', @SalesManager = N'{1}', @Sales = N'{2}'," & vbCrLf, SalesGroup, SalesManager, Sales)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE StuClass SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE StuID = @StuID AND ClassID = @ClassID")
            sb.AppendLine("INSERT INTO StuClass(StuID, ClassID, SalesGroup, SalesManager, Sales, Status, StatusLog, HandlerID, CreateDate, WorkStationID, Department, ModifyHandlerID, ModifyDate, ModifyWorkStationID, ModifyDepartment)")
            sb.AppendLine("    VALUES(@StuID, @ClassID, @SalesGroup, @SalesManager, @Sales, 'Normal', N'', @HandlerID, @DateTime, @WorkStationID, @Department, @HandlerID, @DateTime, @WorkStationID, @Department)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetClassroomList(ByVal ClassID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ClassID INT")
            sb.AppendFormat("SELECT @ClassID = {0}" & vbCrLf, ClassID)
            sb.AppendLine("SELECT *")
            sb.AppendLine("FROM Classroom")
            sb.AppendLine("WHERE ID IN (SELECT ClassroomID FROM ClassBookingDetail WHERE ClassID = @ClassID AND Status IN ('Normal')) AND Status IN ('Normal')")
            sb.AppendLine("ORDER BY Name ASC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("ClassroomList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ClassroomList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetStuClassList(ByVal ClassID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ClassID INT")
            sb.AppendFormat("SELECT @ClassID = {0}" & vbCrLf, ClassID)
            sb.AppendLine("SELECT *, ISNULL((SELECT Name FROM StuInfo WHERE ID = StuClass.StuID AND Status IN ('Normal')), '') AS StuName")
            sb.AppendLine("FROM StuClass")
            sb.AppendLine("WHERE ClassID = @ClassID AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("StuClassList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "StuClassList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function GetSeatBookingList(ByVal ClassID As Integer, ByVal ClassroomID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ClassID INT, @ClassroomID INT")
            sb.AppendFormat("SELECT @ClassID = {0}, @ClassroomID = {1}" & vbCrLf, ClassID, ClassroomID)
            sb.AppendLine("SELECT *, ISNULL((SELECT Name FROM StuInfo WHERE ID = SeatBooking.StuID AND Status IN ('Normal')), '') AS StuName")
            sb.AppendLine("FROM SeatBooking")
            sb.AppendLine("WHERE ClassID = @ClassID AND ClassroomID = @ClassroomID AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("SeatBookingList")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "SeatBookingList"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function ChangeSeat(ByVal StuID As Integer, ByVal ClassID As Integer, ByVal ClassroomID As Integer, ByVal X As Integer, ByVal Y As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT, @ClassID INT, @ClassroomID INT, @X INT, @Y INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @StuID = {0}, @ClassID = {1}, @ClassroomID = {2}, @X = {3}, @Y = {4}," & vbCrLf, StuID, ClassID, ClassroomID, X, Y)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE SeatBooking SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE StuID = @StuID AND ClassID = @ClassID AND ClassroomID = @ClassroomID")
            sb.AppendLine("INSERT INTO SeatBooking(StuID, ClassID, ClassroomID, X, Y, Status, StatusLog, HandlerID, CreateDate, WorkStationID, Department, ModifyHandlerID, ModifyDate, ModifyWorkStationID, ModifyDepartment)")
            sb.AppendLine("    VALUES(@StuID, @ClassID, @ClassroomID, @X, @Y, 'Normal', N'', @HandlerID, @DateTime, @WorkStationID, @Department, @HandlerID, @DateTime, @WorkStationID, @Department)")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function

        Public Shared Function GetFeeItem(ByVal FeeID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @FeeID INT")
            sb.AppendFormat("SELECT @FeeID = {0}" & vbCrLf, FeeID)
            sb.AppendLine("SELECT *")
            sb.AppendLine("FROM StuFee")
            sb.AppendLine("WHERE ID = @FeeID AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.Write(sql)

            Dim dt As New DataTable("FeeItem")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "FeeItem"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveFeeItem(ByVal ID As Integer, ByVal StuID As Integer, ByVal ClassID As Integer, ByVal Receipt As String, ByVal Amount As Integer, ByVal FeeType As String, ByVal PayMethod As String, ByVal Extra As String, ByVal Comment As String, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @StuID INT, @ClassID INT, @Receipt VARCHAR(50), @Amount INT, @SumType VARCHAR(50), @PayType VARCHAR(50), @PayMethod VARCHAR(50),")
            sb.AppendLine("    @Extra NVARCHAR(MAX), @Comment NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}, @StuID = {1}, @ClassID = {2}, @Receipt = '{3}', @Amount = {4}," & vbCrLf, ID, StuID, ClassID, Receipt, Amount)
            sb.AppendFormat("    @PayType = '{0}', @PayMethod = '{1}', @Extra = N'{2}', @Comment = N'{3}'," & vbCrLf, FeeType, PayMethod, Extra, Comment)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendLine("SELECT @SumType = Value FROM NameValuePairs WHERE Name = 'FeeSumType-' + @PayType")
            If ID > 0 Then
                sb.AppendLine("UPDATE StuFee SET Receipt = @Receipt, Amount = @Amount, PayMethod = @PayMethod, Extra = @Extra, Comment = @Comment, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID = @ID")
            Else
                sb.AppendLine("INSERT INTO StuFee(StuID, ClassID, Receipt, Amount, SumType, FeeType, PayMethod, Extra, Comment, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("VALUES(@StuID, @ClassID, @Receipt, @Amount, @SumType, @PayType, @PayMethod, @Extra, @Comment, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            End If

            Dim sql As String = sb.ToString()

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function GetNextReceiptNo(ByVal WorkStationID As String) As String
            Dim Receipt As String = String.Format("{0:d3}{1:MMdd}001", Now.Year - 1911, Now)

            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @Department = N'{0}'", WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("SELECT Receipt FROM StuFee WHERE Status IN ('Normal') AND Department = @Department ORDER BY CreateDate DESC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    Receipt = dt.Rows(0)("Receipt")
                    Dim matches As System.Text.RegularExpressions.MatchCollection = System.Text.RegularExpressions.Regex.Matches(Receipt, "\d+$")
                    If matches.Count = 0 Then
                        Dim i As Integer = 1
                        Do
                            Receipt &= i.ToString("d3")
                            i += 1
                        Loop While IsReceiptNoExists(Receipt)
                    Else
                        Do
                            Dim str As String = matches(0).Value
                            Dim digit As Int64 = Int64.Parse(str) + 1
                            Dim format As String = String.Format("d{0}", str.Length)
                            Receipt = Receipt.Replace(str, digit.ToString(format))
                        Loop While IsReceiptNoExists(Receipt)
                    End If
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return Receipt
        End Function

        Public Shared Function IsReceiptNoExists(ByVal Receipt As String) As Boolean
            Return IsReceiptNoExists(-1, Receipt)
        End Function

        Public Shared Function IsReceiptNoExists(ByVal FeeID As Integer, ByVal Receipt As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Receipt VARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}, @Receipt = '{1}'", FeeID, Receipt)
            sb.AppendLine("SELECT COUNT(1) FROM StuFee WHERE ID <> @ID AND Receipt = @Receipt AND Status IN ('Normal')")

            Dim sql As String = sb.ToString()

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)(0) > 0
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return True
            End Try
        End Function

        Public Shared Function RemoveFeeItem(ByVal FeeID As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}," & vbCrLf, FeeID)
            sb.AppendFormat("    @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'," & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = N'{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("UPDATE StuFee SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = @ID")

            Dim sql As String = sb.ToString()

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function GetReceiptInfo(ByVal FeeID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT")
            sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, FeeID)
            sb.AppendLine("SELECT f.Receipt, c.DateFrom, c.DateTo, ISNULL((SELECT Name FROM StuInfo WHERE ID = f.StuID AND Status IN ('Normal')), '') AS StuName,")
            sb.AppendLine("    f.Amount, f.Extra, c.Name AS ClassName,")
            sb.AppendLine("    ISNULL((SELECT TOP 1 X FROM SeatBooking WHERE StuID = f.StuID AND ClassID = f.ClassID AND Status IN ('Normal') ORDER BY CreateDate DESC), -1) AS X,")
            sb.AppendLine("    ISNULL((SELECT TOP 1 Y FROM SeatBooking WHERE StuID = f.StuID AND ClassID = f.ClassID AND Status IN ('Normal') ORDER BY CreateDate DESC), -1) AS Y")
            sb.AppendLine("FROM StuFee f")
            sb.AppendLine("INNER JOIN Class c")
            sb.AppendLine("ON c.ID = f.ClassID AND c.Status IN ('Normal')")
            sb.AppendLine("WHERE f.ID = @ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("ReceiptInfo")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ReceiptInfo"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function
    End Class
End Namespace