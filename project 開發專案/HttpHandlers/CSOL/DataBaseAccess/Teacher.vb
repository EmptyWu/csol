﻿Namespace DataBaseAccess
    Public Class Teacher
        Public Shared Function GetList() As DataTable
            Dim sql As String = "SELECT ID, Name, Subject, Description FROM Teacher WHERE Status IN ('Normal')"

            Dim dt As New DataTable("Teacher")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "Teacher"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return dt
        End Function

        Public Shared Function SaveList(ByVal dt As DataTable, ByVal deletedIDs() As Integer, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Name NVARCHAR(50), @Subject NVARCHAR(50), @Description NVARCHAR(MAX), @StatusLog NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))

            For Each row As DataRow In dt.Rows
                sb.AppendFormat("SELECT @ID = {0}, @Name = N'{1}', @Subject = N'{2}', @Description = N'{3}'" & vbCrLf, row("ID"), row("Name"), row("Subject"), row("Description"))

                If row("ID") > 0 Then
                    sb.AppendFormat("SELECT @StatusLog = N'Changed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'" & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
                    sb.AppendLine("UPDATE Teacher SET Name = @Name, Subject = @Subject, Description = @Description, StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = @ID")
                Else
                    sb.AppendLine("INSERT INTO Teacher(Name, Subject, Description, Status, StatusLog, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                    sb.AppendLine("    VALUES(@Name, @Subject, @Description, 'Normal', N'', @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
                End If
            Next

            For Each ID As Integer In deletedIDs
                sb.AppendFormat("SELECT @StatusLog = N'Removed DateTime:{0} HandlerID:{1} WorkStation:{2} Department:{3}'" & vbCrLf, Now.ToString("yyyy-MM-dd HH:mm:ss"), HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
                sb.AppendFormat("UPDATE Teacher SET Status = 'Removed', StatusLog = StatusLog + ';;' + @StatusLog WHERE ID = {0}" & vbCrLf, ID)
            Next

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace