﻿Imports System.Text.RegularExpressions

Namespace DataBaseAccess
    Public Class StuClassFee
        Public Shared Function GetNextReceipt(ByVal WorkStationID As String) As String
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Department NVARCHAR(50)")
            sb.AppendFormat("SELECT @Department = N'{0}'", WorkStation.GetDepartment(WorkStationID))

            sb.AppendLine("SELECT Receipt FROM StuClassFee2 WHERE Removed = 0 AND Department = @Department ORDER BY CreateDate DESC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim num As String = String.Format("{0:d3}{1:MMdd}001", Now.Year - 1911, Now)
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count > 0 Then
                    num = dt.Rows(0)("Receipt")
                    Dim matches As MatchCollection = Regex.Matches(num, "\d+$")
                    If matches.Count = 0 Then
                        Dim i As Integer = 1
                        Do
                            num &= i.ToString("d3")
                            i += 1
                        Loop While IsReceiptExists(num)
                    Else
                        Do
                            Dim str As String = matches(0).Value
                            Dim digit As Int64 = Int64.Parse(str) + 1
                            Dim format As String = String.Format("d{0}", str.Length)
                            num = num.Replace(str, digit.ToString(format))
                        Loop While IsReceiptExists(num)
                    End If
                End If
            Catch ex As Exception

            End Try
            Return num
        End Function

        Public Shared Function IsReceiptExists(ByVal Receipt As String) As Boolean
            Return IsReceiptExists(-1, Receipt)
        End Function

        Public Shared Function IsReceiptExists(ByVal ID As Integer, ByVal Receipt As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @Receipt VARCHAR(50)")
            sb.AppendFormat("SELECT @ID = {0}, @Receipt = '{1}'", ID, Receipt)
            sb.AppendLine("SELECT COUNT(1) FROM StuClassFee2 WHERE ID <> @ID AND Receipt = @Receipt AND Removed = 0")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                Return dt.Rows(0)(0) > 0
            Catch ex As Exception
                Return True
            End Try
        End Function

        Public Shared Function GetStuClassFee(ByVal StuID As String) As DataSet
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @StuID INT")
            sb.AppendFormat("SELECT @StuID = {0}" & vbCrLf, StuID)
            sb.AppendLine("SELECT c.ID, c.Name, c.Types, c.AccountsReceivable,")
            sb.AppendLine("    c.AccountsReceivable - (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID  AND SumType = '折扣' AND Removed = 0) AS AfterDiscount,")
            sb.AppendLine("    (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID AND SumType = '收入' AND Removed = 0) AS Paid,")
            sb.AppendLine("    (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID  AND SumType = '支出' AND Removed = 0) AS MoneyBack,")
            sb.AppendLine("    c.AccountsReceivable - (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID  AND SumType = '折扣' AND Removed = 0)")
            sb.AppendLine("        - (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID  AND SumType = '收入' AND Removed = 0)")
            sb.AppendLine("        + (SELECT ISNULL(SUM(Amount), 0) FROM StuClassFee2 WHERE StuID = @StuID AND ClassID = c.ID  AND SumType = '支出' AND Removed = 0) AS FeeOwe")
            sb.AppendLine("FROM Seat s")
            sb.AppendLine("INNER JOIN Class2 c")
            sb.AppendLine("ON s.ClassID = c.ID")
            sb.AppendLine("WHERE StuID = @StuID AND s.Removed = 0")

            sb.AppendLine("SELECT f.ID, ClassID, c.Name AS ClassName, Receipt, Amount, SumType, PayType, PayMethod, Extra, Comment, u.Name AS UserName, f.ModifyDate")
            sb.AppendLine("FROM StuClassFee2 f")
            sb.AppendLine("INNER JOIN Class2 c")
            sb.AppendLine("ON f.ClassID = c.ID")
            sb.AppendLine("INNER JOIN Users u")
            sb.AppendLine("ON f.ModifyHandlerID = u.ID")
            sb.AppendLine("WHERE StuID = @StuID AND f.Removed = 0")
            sb.AppendLine("ORDER BY f.ModifyDate DESC")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim ds As New DataSet("StuClassFee")

            Try
                ds = CSOL.DataBase.LoadToDataSet(sql)
                If ds.Tables.Count = 2 Then
                    ds.Tables(0).TableName = "StuClass"
                    ds.Tables(1).TableName = "StuClassFee"
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return ds
        End Function

        Public Shared Function GetReceiptInfo(ByVal ID As Integer) As DataTable
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT")
            sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, ID)
            sb.AppendLine("SELECT f.Receipt, MIN(cb.Date) AS StartDate, MAX(cb.Date) AS EndDate, s.Name, f.Amount, f.Extra, c.Name AS ClassName, st.X, st.Y")
            sb.AppendLine("FROM StuClassFee2 f")
            sb.AppendLine("INNER JOIN Seat st")
            sb.AppendLine("ON f.ClassID = st.ClassID AND f.StuID = st.StuID AND st.Removed = 0")
            sb.AppendLine("INNER JOIN Class2 c")
            sb.AppendLine("ON f.ClassID = c.ID")
            sb.AppendLine("INNER JOIN Class2Booking cb")
            sb.AppendLine("ON c.ID = cb.ClassID")
            sb.AppendLine("INNER JOIN StuInfo2 s")
            sb.AppendLine("ON s.ID = f.StuID")
            sb.AppendLine("WHERE f.ID = @ID")
            sb.AppendLine("GROUP BY f.Receipt, s.Name, f.Amount, f.Extra, c.Name, st.X, st.Y")
            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim dt As New DataTable("ReceiptInfo")
            Try
                dt = CSOL.DataBase.LoadToDataTable(sql)
                dt.TableName = "ReceiptInfo"
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try
            Return dt
        End Function

        Public Shared Function SaveStuClassFee(ByVal ID As Integer, ByVal StuID As Integer, ByVal ClassID As Integer, ByVal Receipt As String, ByVal Amount As Integer, ByVal PayType As String, ByVal PayMethod As String, _
                                               ByVal Extra As String, ByVal Comment As String, ByVal HandlerID As String, ByVal WorkStationID As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT, @StuID INT, @ClassID INT, @Receipt VARCHAR(50), @Amount INT, @SumType VARCHAR(50), @PayType VARCHAR(50), @PayMethod VARCHAR(50),")
            sb.AppendLine("    @Extra NVARCHAR(MAX), @Comment NVARCHAR(MAX),")
            sb.AppendLine("    @DateTime DATETIME, @HandlerID VARCHAR(50), @WorkStationID VARCHAR(50), @Department NVARCHAR(50)")

            sb.AppendFormat("SELECT @ID = {0}, @StuID = {1}, @ClassID = {2}, @Receipt = '{3}', @Amount = {4}," & vbCrLf, ID, StuID, ClassID, Receipt, Amount)
            sb.AppendFormat("    @PayType = '{0}', @PayMethod = '{1}', @Extra = N'{2}', @Comment = N'{3}'," & vbCrLf, PayType, PayMethod, Extra, Comment)
            sb.AppendFormat("    @DateTime = GETDATE(), @HandlerID = '{0}', @WorkStationID = '{1}', @Department = '{2}'" & vbCrLf, HandlerID, WorkStationID, WorkStation.GetDepartment(WorkStationID))
            sb.AppendLine("SELECT @SumType = Text FROM Definitions WHERE Name = 'FeeSumType-' + @PayType")
            If ID > 0 Then
                sb.AppendLine("UPDATE StuClassFee2 SET Receipt = @Receipt, Amount = @Amount, PayMethod = @PayMethod, Extra = @Extra, Comment = @Comment, ModifyDate = @DateTime, ModifyHandlerID = @HandlerID, ModifyWorkStationID = @WorkStationID, ModifyDepartment = @Department WHERE ID= @ID")
            Else
                sb.AppendLine("INSERT INTO StuClassFee2(StuID, ClassID, Receipt, Amount, SumType, PayType, PayMethod, Extra, Comment, Removed, CreateDate, HandlerID, WorkStationID, Department, ModifyDate, ModifyHandlerID, ModifyWorkStationID, ModifyDepartment)")
                sb.AppendLine("VALUES(@StuID, @ClassID, @Receipt, @Amount, @SumType, @PayType, @PayMethod, @Extra, @Comment, 0, @DateTime, @HandlerID, @WorkStationID, @Department, @DateTime, @HandlerID, @WorkStationID, @Department)")
            End If

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function

        Public Shared Function RemoveStuClassFee(ByVal ID As Integer) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @ID INT")
            sb.AppendFormat("SELECT @ID = {0}" & vbCrLf, ID)
            sb.AppendLine("UPDATE StuClassFee2 SET Removed = 1 WHERE ID = @ID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
                Return True
            Catch ex As Exception
                Return False
            End Try
        End Function
    End Class
End Namespace