﻿Namespace DataBaseAccess
    Public Class Auth
        Public Shared Function GetUserID(ByVal Username As String) As Integer
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Username VARCHAR(50)")
            sb.AppendFormat("SELECT @Username = '{0}'" & vbCrLf, Username)
            sb.AppendLine("SELECT ID FROM Users WHERE Username = @Username")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim UserID As Integer = -1
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count = 1 Then
                    UserID = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return UserID
        End Function

        Public Shared Function GetUsername(ByVal UserID As Integer) As String
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @UserID VARCHAR(50)")
            sb.AppendFormat("SELECT @UserID = {0}" & vbCrLf, UserID)
            sb.AppendLine("SELECT Username FROM Users WHERE ID = @UserID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim Username As String = ""
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count = 1 Then
                    Username = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return Username
        End Function

        Public Shared Function GetUserDisplayName(ByVal UserID As Integer) As String
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @UserID VARCHAR(50)")
            sb.AppendFormat("SELECT @UserID = {0}" & vbCrLf, UserID)
            sb.AppendLine("SELECT Name FROM Users WHERE ID = @UserID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Dim Name As String = ""
            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows.Count = 1 Then
                    Name = dt.Rows(0)(0)
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
            End Try

            Return Name
        End Function

        Public Shared Function IsPasswordCorrect(ByVal Username As String, ByVal Password As String, ByVal Seed As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @Username VARCHAR(50), @Seed VARCHAR(32)")
            sb.AppendFormat("SELECT @Username = '{0}', @Seed = '{1}'" & vbCrLf, Username, Seed)
            sb.AppendLine("SELECT SUBSTRING(sys.fn_varbintohexstr(HASHBYTES('md5', Password + @Seed)), 3, 32) FROM Users WHERE Username = @Username")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                Dim dt As DataTable = CSOL.DataBase.LoadToDataTable(sql)
                If dt.Rows(0)(0) = Password Then
                    Return True
                End If
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return False
        End Function

        Public Shared Function ChangePassword(ByVal UserID As Integer, ByVal Password As String) As Boolean
            Dim sb As New System.Text.StringBuilder()
            sb.AppendLine("DECLARE @UserID INT, @Password VARCHAR(50)")
            sb.AppendFormat("SELECT @UserID = {0}, @Password = '{1}'", UserID, Password)
            sb.AppendLine("UPDATE Users SET Password = SUBSTRING(sys.fn_varbintohexstr(HASHBYTES('md5', @Password)), 3, 32) WHERE ID = @UserID")

            Dim sql As String = sb.ToString()
            System.Diagnostics.Debug.WriteLine(sql)

            Try
                CSOL.DataBase.ExecSql(sql)
            Catch ex As Exception
                System.Diagnostics.Debug.WriteLine(ex.ToString())
                Return False
            End Try

            Return True
        End Function
    End Class
End Namespace