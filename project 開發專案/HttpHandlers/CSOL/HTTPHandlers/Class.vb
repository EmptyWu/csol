﻿Namespace HTTPHandlers
    Public Class [Class]
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetList"
                    GetList()
                Case "GetItem"
                    GetItem()
                Case "IsNameExists"
                    IsNameExists()
                Case "SaveItem"
                    SaveItem()
                Case "RemoveItem"
                    RemoveItem()

                Case "AddBooking"
                    AddBooking()
                Case "GetBooking"
                    GetBooking()
                Case "GetBookingDetail"
                    GetBookingDetail()

                Case "RemoveBooking"
                    RemoveBooking()
                Case "RemoveBookingDetail"
                    RemoveBookingDetail()

                Case "SetPrimaryClassroom"
                    SetPrimaryClassroom()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/Class"
            End Get
        End Property

        Private Sub GetList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim List As DataTable = DataBaseAccess.Class.GetList()
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As Integer = RequestParams("ID")

                Dim Item As DataTable = DataBaseAccess.Class.GetItem(ID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Item", CSOL.Convert.DataTableToXmlString(Item))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub IsNameExists()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As String = RequestParams("ID")
                Dim Name As String = RequestParams("Name")

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("IsNameExists", DataBaseAccess.Class.IsNameExists(ID, Name))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SaveItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As Integer = RequestParams("ID")
                Dim Name As String = RequestParams("Name")
                Dim Type As String = RequestParams("Type")
                Dim ListPrice As Integer = RequestParams("ListPrice")
                Dim AccountsReceivable As Integer = RequestParams("AccountsReceivable")
                Dim DateFrom As Date = RequestParams("DateFrom")
                Dim DateTo As Date = RequestParams("DateTo")
                Dim BookSeat As Boolean = RequestParams("BookSeat")
                If ListPrice <= 0 Or AccountsReceivable <= 0 Then
                    Throw New Exception("Invalid Number")
                End If

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.Class.SaveItem(ID, Name, Type, ListPrice, AccountsReceivable, DateFrom, DateTo, BookSeat, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub RemoveItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As Integer = RequestParams("ID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.Class.RemoveItem(ID, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub AddBooking()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim ClassroomID As Integer = RequestParams("ClassroomID")
                Dim CourseID As Integer = RequestParams("CourseID")

                Dim TimeFrom As Date = RequestParams("TimeFrom")
                Dim TimeTo As Date = RequestParams("TimeTo")
                Dim AllowMix As Integer = RequestParams("AllowMix")

                If TimeFrom > TimeTo Then
                    Dim tmp As Date = TimeFrom
                    TimeFrom = TimeTo
                    TimeTo = tmp
                End If

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Select Case RequestParams("Type")
                    Case "Period"
                        Dim DateFrom As Date = RequestParams("DateFrom")
                        Dim DateTo As Date = RequestParams("DateTo")
                        Dim WeekDays() As String = RequestParams("WeekDays").Split(",")

                        If DateFrom > DateTo Then
                            Dim tmp As Date = DateFrom
                            DateFrom = DateTo
                            DateTo = tmp
                        End If

                        Dim result As Boolean = DataBaseAccess.Class.AddPeriodBooking(ClassID, ClassroomID, CourseID, DateFrom, DateTo, WeekDays, TimeFrom, TimeTo, AllowMix, HandlerID, WorkStationID)
                        ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
                    Case "Single"
                        Dim [Date] As Date = RequestParams("Date")

                        Dim result As Boolean = DataBaseAccess.Class.AddSingleBooking(ClassID, ClassroomID, CourseID, [Date], TimeFrom, TimeTo, AllowMix, HandlerID, WorkStationID)
                        ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
                    Case Else
                        Throw New Exception("Invalid Type")
                End Select

            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetBooking()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassID As Integer = RequestParams("ClassID")

                Dim List As DataTable = DataBaseAccess.Class.GetBooking(ClassID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetBookingDetail()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim BatchID As Integer = RequestParams("BatchID")
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim ClassroomID As Integer = RequestParams("ClassroomID")
                Dim ShowOverlap As Boolean = RequestParams("ShowOverlap")

                Dim List As DataTable = DataBaseAccess.Class.GetBookingDetail(BatchID, ClassID, ClassroomID, ShowOverlap)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub RemoveBooking()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As String = RequestParams("ID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.Class.RemoveBooking(ID, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub RemoveBookingDetail()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As String = RequestParams("ID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.Class.RemoveBookingDetail(ID, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SetPrimaryClassroom()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As String = RequestParams("ID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.Class.SetPrimaryClassroom(ID, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace