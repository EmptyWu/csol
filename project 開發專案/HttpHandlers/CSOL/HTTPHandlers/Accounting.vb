﻿Namespace HTTPHandlers
    Public Class Accounting
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetDepartments"
                    GetDepartments()
                Case "GetDailyReport"
                    GetDailyReport()
                Case "GetMonthlyReport"
                    GetMonthlyReport()
                Case "GetYearlyReport"
                    GetYearlyReport()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/Accounting"
            End Get
        End Property

        Private Sub GetDepartments()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim SomeDate As Date = RequestParams("Date")
                Dim AnotherDate As Date = SomeDate
                If RequestParams("AnotherDate") <> "" Then
                    AnotherDate = RequestParams("AnotherDate")
                End If

                Dim Departments() As String = DataBaseAccess.Accounting.GetDepartments(SomeDate, AnotherDate)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Departments", String.Join(",", Departments))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetDailyReport()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim SomeDate As Date = RequestParams("Date")

                Dim DailyReport As DataTable = DataBaseAccess.Accounting.GetDailyReport(SomeDate)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("DailyReport", CSOL.Convert.DataTableToXmlString(DailyReport))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetMonthlyReport()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim SomeDate As Date = RequestParams("Date")

                Dim MonthlyReport As DataTable = DataBaseAccess.Accounting.GetMonthlyReport(SomeDate)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("MonthlyReport", CSOL.Convert.DataTableToXmlString(MonthlyReport))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetYearlyReport()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim SomeDate As Date = RequestParams("Date")

                Dim YearlyReport As DataTable = DataBaseAccess.Accounting.GetYearlyReport(SomeDate)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("YearlyReport", CSOL.Convert.DataTableToXmlString(YearlyReport))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace