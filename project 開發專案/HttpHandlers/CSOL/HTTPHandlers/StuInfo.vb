﻿Namespace HTTPHandlers
    Public Class StuInfo
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetClassNames"
                    GetClassNames()
                Case "List"
                    List()
                Case "Detail"
                    Detail()
                Case "Save"
                    Save()
                Case "GetNextStuNo"
                    GetNextStuNo()
                Case "GetPicture"
                    GetPicture()
                Case "Remove"
                    Remove()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/StuInfo"
            End Get
        End Property

        Private Sub GetClassNames()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassNames As DataTable = DataBaseAccess.StuInfo.GetClassNames()
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("ClassNames", CSOL.Convert.DataTableToXmlString(ClassNames))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub List()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim Rows As Integer = RequestParams("Rows")
                Dim Page As Integer = RequestParams("Page")
                Dim Filter As String = ""

                Dim FilterConditions As New List(Of String)
                Dim Filters As System.Collections.Specialized.NameValueCollection = CSOL.Convert.ParseQuery(RequestParams("Filter"))

                For Each f As String In Filters.AllKeys
                    Select Case f
                        Case "學號"
                            If Filters(f) <> "" Then
                                FilterConditions.Add(String.Format("s.No LIKE '{0}%'", Filters(f).Replace("*", "%")))
                            End If
                        Case "姓名"
                            If Filters(f) <> "" Then
                                FilterConditions.Add(String.Format("s.Name LIKE '{0}%'", Filters(f).Replace("*", "%")))
                            End If
                        Case "身份證"
                            If Filters(f) <> "" Then
                                FilterConditions.Add(String.Format("s.PID LIKE '{0}%'", Filters(f).Replace("*", "%")))
                            End If
                        Case "電話"
                            If Filters(f) <> "" Then
                                FilterConditions.Add(String.Format("(s.Phone LIKE '{0}%' OR s.Mobile LIKE '{0}%')", Filters(f).Replace("*", "%")))
                            End If
                        Case "班級"
                            If Filters(f) <> "" Then
                                FilterConditions.Add(String.Format("c.Name = '{0}'", Filters(f)))
                            End If
                    End Select
                Next
                FilterConditions.Add("s.Status IN ('Normal')")
                Filter = String.Format("WHERE {0}", String.Join(" AND ", FilterConditions.ToArray()))

                Dim StuCount As Integer = DataBaseAccess.StuInfo.GetCount(Filter)
                Dim List As DataTable = DataBaseAccess.StuInfo.List(Rows, Page, Filter)

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuCount", StuCount)
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub Detail()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim Detail As DataTable = DataBaseAccess.StuInfo.Detail(StuID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Detail", CSOL.Convert.DataTableToXmlString(Detail))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub Save()
            Dim PicturePath As String = String.Format("{0}\Images\StuPictures", My.Application.Info.DirectoryPath)
            System.IO.Directory.CreateDirectory(PicturePath)

            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim No As String = RequestParams("No")
                Dim Name As String = RequestParams("Name")
                Dim EngName As String = ""
                Dim PID As String = RequestParams("PID")
                Dim Type As String = RequestParams("Type")
                Dim Sex As String = RequestParams("Sex")
                Dim Birth As New Date(1900, 1, 1)
                Date.TryParse(RequestParams("Birth"), Birth)

                Dim School As String = RequestParams("School")
                Dim Phone As String = RequestParams("Phone")
                Dim Mobile As String = RequestParams("Mobile")
                Dim Address As String = RequestParams("Address")
                Dim Contacts As String = RequestParams("Contacts")

                Dim SalesGroup As String = RequestParams("SalesGroup")
                Dim SalesManager As String = RequestParams("SalesManager")
                Dim Sales As String = RequestParams("Sales")
                Dim Comment As String = RequestParams("Comment")

                Dim Picture As System.Drawing.Image = CSOL.Convert.Base64StringToImage(RequestParams("Picture"))

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                StuID = DataBaseAccess.StuInfo.Save(StuID, No, Name, EngName, PID, Type, Sex, Birth, School, Phone, Mobile, Address, Contacts, SalesGroup, SalesManager, Sales, Comment, HandlerID, WorkStationID)
                If StuID <= 0 Then
                    ResponseParams.Add("Status", "Error")
                Else
                    Dim StuPicPath As String = String.Format("{0}\{1}\pic.png", PicturePath, StuID)
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(StuPicPath))
                    Try
                        Picture.Save(StuPicPath, System.Drawing.Imaging.ImageFormat.Png)
                    Catch ex As Exception
                        System.Diagnostics.Debug.WriteLine(ex.ToString())
                    End Try

                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("StuID", StuID)
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Public Sub GetNextStuNo()
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim StuNo As String = DataBaseAccess.StuInfo.GetNextStuNo(WorkStationID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuNo", StuNo)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetPicture()
            Dim PicturePath As String = String.Format("{0}\Images\StuPictures", My.Application.Info.DirectoryPath)

            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim pic As String = ""
                Dim StuPicPath As String = String.Format("{0}\{1}\pic.png", PicturePath, StuID)
                If System.IO.File.Exists(StuPicPath) Then
                    pic = CSOL.Convert.ImageToBase64String(System.Drawing.Image.FromFile(StuPicPath), System.Drawing.Imaging.ImageFormat.Png)
                End If

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Picture", pic)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub Remove()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)
                If DataBaseAccess.StuInfo.Remove(StuID, HandlerID, WorkStationID) Then
                    ResponseParams.Add("Status", "OK")
                Else
                    ResponseParams.Add("Status", "Error")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace