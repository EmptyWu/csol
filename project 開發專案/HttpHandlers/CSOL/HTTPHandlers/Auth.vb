﻿Namespace HTTPHandlers
    Public Class Auth
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetSeed"
                    GetSeed()
                Case "Login"
                    Login()
                Case "GetUserDisplayName"
                    GetUserDisplayName()
                Case "ChangePassword"
                    ChangePassword()
                Case "GetWorkStation"
                    GetWorkStation()
                Case "GetDepartments"
                    GetDepartments()
                Case "SaveWorkStation"
                    SaveWorkStation()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/Auth"
            End Get
        End Property

        Private Sub GetSeed()
            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            ResponseParams.Add("Seed", Guid.NewGuid().ToString("N"))

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub Login()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim Username As String = RequestParams("Username")
            Dim Password As String = RequestParams("Password")
            Dim Seed As String = RequestParams("Seed")
            Dim WorkStationID As String = RequestParams("WorkStationID")

            Dim UserID As Integer = DataBaseAccess.Auth.GetUserID(Username)
            If UserID > 0 Then
                If DataBaseAccess.Auth.IsPasswordCorrect(Username, Password, Seed) Then
                    Dim SessionID As String = Session.Direct.Create(UserID, WorkStationID)
                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("SessionID", SessionID)
                    Me.m_Context.Response.Cookies.Add(New System.Net.Cookie("SessionID", SessionID))

                    Dim SessionIDCookie As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
                    If SessionIDCookie IsNot Nothing Then
                        Session.Direct.Remove(SessionIDCookie.Value)
                    End If
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            Else
                ResponseParams.Add("Status", "Error")
            End If

            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetUserDisplayName()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim UserID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim Name As String = DataBaseAccess.Auth.GetUserDisplayName(UserID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Name", Name)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub ChangePassword()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""
            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim UserID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim Username As String = DataBaseAccess.Auth.GetUsername(UserID)
                Dim Old As String = RequestParams("Old")
                Dim NewPwd As String = RequestParams("New")
                Dim Seed As String = RequestParams("Seed")

                If DataBaseAccess.Auth.IsPasswordCorrect(Username, Old, Seed) Then
                    If DataBaseAccess.Auth.ChangePassword(UserID, NewPwd) Then
                        ResponseParams.Add("Status", "OK")
                    Else
                        ResponseParams.Add("Status", "Failed")
                    End If
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetWorkStation()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ID As String = RequestParams("WorkStationID")

                Dim WorkStation As DataTable = DataBaseAccess.WorkStation.GetWorkStation(ID)
                If WorkStation.Rows.Count = 1 Then
                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("Name", WorkStation.Rows(0)("Name"))
                    ResponseParams.Add("Department", WorkStation.Rows(0)("Department"))
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetDepartments()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim Departments() As String = DataBaseAccess.WorkStation.GetDepartments()
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Departments", String.Join(",", Departments))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SaveWorkStation()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim WorkStationID As String = RequestParams("WorkStationID")
                Dim WorkStationName As String = RequestParams("WorkStationName")
                Dim Department As String = RequestParams("Department")
                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)

                If Not DataBaseAccess.WorkStation.IsWorkStationExists(WorkStationID) Then
                    WorkStationID = DataBaseAccess.WorkStation.CreateWorkStation(WorkStationName, Department, HandlerID)
                Else
                    DataBaseAccess.WorkStation.ModifyWorkStation(WorkStationID, WorkStationName, Department, HandlerID)
                End If

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("WorkStationID", WorkStationID)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace