﻿Namespace HTTPHandlers
    Public Class StuClass
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetList"
                    GetList()
                Case "GetFeeList"
                    GetFeeList()
                Case "SaveItem"
                    SaveItem()

                Case "GetClassroomList"
                    GetClassroomList()
                Case "GetStuClassList"
                    GetStuClassList()
                Case "GetSeatBookingList"
                    GetSeatBookingList()
                Case "ChangeSeat"
                    ChangeSeat()

                Case "GetFeeItem"
                    GetFeeItem()
                Case "SaveFeeItem"
                    SaveFeeItem()
                Case "GetNextReceiptNo"
                    GetNextReceiptNo()
                Case "IsReceiptNoExists"
                    IsReceiptNoExists()

                Case "RemoveFeeItem"
                    RemoveFeeItem()
                Case "GetReceiptInfo"
                    GetReceiptInfo()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/StuClass"
            End Get
        End Property

        Private Sub GetList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")

                Dim List As DataTable = DataBaseAccess.StuClass.GetList(StuID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetFeeList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim ClassID As Integer = RequestParams("ClassID")

                Dim List As DataTable = DataBaseAccess.StuClass.GetFeeList(StuID, ClassID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SaveItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim SalesGroup As String = RequestParams("SalesGroup")
                Dim SalesManager As String = RequestParams("SalesManager")
                Dim Sales As String = RequestParams("Sales")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.StuClass.SaveItem(StuID, ClassID, SalesGroup, SalesManager, Sales, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetClassroomList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassID As Integer = RequestParams("ClassID")

                Dim List As DataTable = DataBaseAccess.StuClass.GetClassroomList(ClassID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetStuClassList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassID As Integer = RequestParams("ClassID")

                Dim List As DataTable = DataBaseAccess.StuClass.GetStuClassList(ClassID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetSeatBookingList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim ClassroomID As Integer = RequestParams("ClassroomID")

                Dim List As DataTable = DataBaseAccess.StuClass.GetSeatBookingList(ClassID, ClassroomID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub ChangeSeat()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim StuID As Integer = RequestParams("StuID")
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim ClassroomID As Integer = RequestParams("ClassroomID")
                Dim X As Integer = RequestParams("X")
                Dim Y As Integer = RequestParams("Y")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.StuClass.ChangeSeat(StuID, ClassID, ClassroomID, X, Y, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetFeeItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim FeeID As Integer = RequestParams("ID")

                Dim Item As DataTable = DataBaseAccess.StuClass.GetFeeItem(FeeID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Item", CSOL.Convert.DataTableToXmlString(Item))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SaveFeeItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim FeeID As Integer = RequestParams("ID")
                Dim StuID As Integer = RequestParams("StuID")
                Dim ClassID As Integer = RequestParams("ClassID")
                Dim Receipt As String = RequestParams("Receipt")
                Dim Amount As Integer = RequestParams("Amount")
                Dim FeeType As String = RequestParams("FeeType")
                Dim PayMethod As String = RequestParams("PayMethod")
                Dim Extra As String = RequestParams("Extra")
                Dim Comment As String = RequestParams("Comment")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.StuClass.SaveFeeItem(FeeID, StuID, ClassID, Receipt, Amount, FeeType, PayMethod, Extra, Comment, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetNextReceiptNo()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim Receipt As String = DataBaseAccess.StuClass.GetNextReceiptNo(WorkStationID)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("Receipt", Receipt)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub IsReceiptNoExists()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim FeeID As Integer = RequestParams("ID")
                Dim Receipt As String = RequestParams("Receipt")

                Dim IsReceiptNoExists As Boolean = DataBaseAccess.StuClass.IsReceiptNoExists(FeeID, Receipt)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("IsReceiptNoExists", IsReceiptNoExists)
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub RemoveFeeItem()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim FeeID As Integer = RequestParams("ID")

                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim result As Boolean = DataBaseAccess.StuClass.RemoveFeeItem(FeeID, HandlerID, WorkStationID)
                ResponseParams.Add("Status", IIf(result, "OK", "Failed"))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetReceiptInfo()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
            Else
                Dim FeeID As Integer = RequestParams("ID")
                Dim ReceiptInfo As DataTable = DataBaseAccess.StuClass.GetReceiptInfo(FeeID)

                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("ReceiptInfo", CSOL.Convert.DataTableToXmlString(ReceiptInfo))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace