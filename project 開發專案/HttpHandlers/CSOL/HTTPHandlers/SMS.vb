﻿Namespace HTTPHandlers
    Public Class SMS
        Implements IHttpHandler

        Private m_Context As System.Net.HttpListenerContext
        Private m_Query As System.Collections.Specialized.NameValueCollection

        Public Sub Handle(ByVal Context As System.Net.HttpListenerContext) Implements IHttpHandler.Handle
            Me.m_Context = Context
            Me.m_Query = HttpServer.ParseQuery(Context.Request.Url.Query)

            Select Case HttpServer.GetAction(Me.m_Query)
                Case "GetStuList"
                    GetStuList()
                Case "SaveSMSList"
                    SaveSMSList()
                Case "GetSMSList"
                    GetSMSList()
                Case "CancelSMSList"
                    CancelSMSList()
                Case Else
                    Me.m_Context.Response.StatusCode = System.Net.HttpStatusCode.BadRequest
                    Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                        sw.Write(System.Net.HttpStatusCode.BadRequest.ToString())
                    End Using
            End Select
        End Sub

        Public ReadOnly Property VirtualPath() As String Implements IHttpHandler.VirtualPath
            Get
                Return "/SMS"
            End Get
        End Property

        Private Sub GetStuList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
                Me.m_Context.Response.Headers.Add("Status", "NotLoggedIn")
            Else
                Dim StuClass As String = New String(RequestParams("StuClass"))
                Dim StuNo As String = New String(RequestParams("StuNo"))
                Dim StuName As String = New String(RequestParams("StuName"))
                Dim Page As Integer = RequestParams("Page")
                Dim Rows As Integer = RequestParams("Rows")
                Dim NonBlankMobile As Boolean = RequestParams("NonBlankMobile")

                Dim StuCount As Integer = DataBaseAccess.SMS.GetStuCount(StuClass, StuNo, StuName, NonBlankMobile)
                Dim List As DataTable = DataBaseAccess.SMS.GetStuList(StuClass, StuNo, StuName, NonBlankMobile, Page, Rows)
                ResponseParams.Add("Status", "OK")
                ResponseParams.Add("StuCount", StuCount)
                ResponseParams.Add("List", CSOL.Convert.DataTableToXmlString(List))
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub SaveSMSList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
                Me.m_Context.Response.Headers.Add("Status", "NotLoggedIn")
            Else
                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim SMSList As DataTable = CSOL.Convert.XmlStringToDataTable(RequestParams("SMSList"))

                Dim BatchID As Integer = DataBaseAccess.SMS.SaveSMSList(SMSList, HandlerID, WorkStationID)
                Dim Status As Boolean = IIf(BatchID > 0, True, False)
                If Status Then
                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("BatchID", BatchID)
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub GetSMSList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
                Me.m_Context.Response.Headers.Add("Status", "NotLoggedIn")
            Else
                Dim BatchID As Integer = RequestParams("BatchID")
                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim SMSList As DataTable = DataBaseAccess.SMS.GetSMSList(BatchID)
                If SMSList.Rows.Count > 0 Then
                    ResponseParams.Add("Status", "OK")
                    ResponseParams.Add("SMSList", CSOL.Convert.DataTableToXmlString(SMSList))
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub

        Private Sub CancelSMSList()
            Dim RequestBody As String = HttpServer.GetPostBody(Me.m_Context.Request)
            Dim RequestParams As System.Collections.Specialized.NameValueCollection = HttpServer.ParseQuery(RequestBody)

            Dim StatusCode As Integer = System.Net.HttpStatusCode.OK
            Dim ResponseParams As New System.Collections.Specialized.NameValueCollection()
            Dim ResponseBody As String = ""

            Dim SessionID As System.Net.Cookie = Me.m_Context.Request.Cookies("SessionID")
            If SessionID Is Nothing Then
                StatusCode = System.Net.HttpStatusCode.BadRequest
                ResponseParams.Add("Status", StatusCode.ToString())
                Me.m_Context.Response.Headers.Add("Status", "NotLoggedIn")
            Else
                Dim SMSList As DataTable = CSOL.Convert.XmlStringToDataTable(RequestParams("SMSList"))
                Dim HandlerID As String = Session.Direct.GetUserID(SessionID.Value)
                Dim WorkStationID As String = Session.Direct.GetWorkStationID(SessionID.Value)

                Dim Status As Boolean = DataBaseAccess.SMS.CancelSMSList(SMSList, HandlerID, WorkStationID)
                If Status = True Then
                    ResponseParams.Add("Status", "OK")
                Else
                    ResponseParams.Add("Status", "Failed")
                End If
            End If
            ResponseBody = CSOL.Convert.GetQuery(ResponseParams)

            Me.m_Context.Response.StatusCode = StatusCode
            Using sw As New System.IO.StreamWriter(Me.m_Context.Response.OutputStream)
                sw.Write(ResponseBody)
            End Using
        End Sub
    End Class
End Namespace