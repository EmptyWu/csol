USE [csol]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 10/28/2010 13:11:07 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Users]') AND type in (N'U'))
DROP TABLE [dbo].[Users]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[Users]    Script Date: 10/28/2010 13:11:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserGroup] [nvarchar](max) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[UserGroups]    Script Date: 10/28/2010 13:13:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserGroups]') AND type in (N'U'))
DROP TABLE [dbo].[UserGroups]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[UserGroups]    Script Date: 10/28/2010 13:13:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[UserGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserGroups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[ModifyRecord]    Script Date: 10/28/2010 13:15:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ModifyRecord]') AND type in (N'U'))
DROP TABLE [dbo].[ModifyRecord]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[ModifyRecord]    Script Date: 10/28/2010 13:15:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ModifyRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[TableID] [varchar](50) NOT NULL,
	[OldValue] [nvarchar](max) NOT NULL,
	[NewValue] [nvarchar](max) NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ModifyRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[WorkStation]    Script Date: 10/28/2010 13:18:19 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[WorkStation]') AND type in (N'U'))
DROP TABLE [dbo].[WorkStation]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[WorkStation]    Script Date: 10/28/2010 13:18:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[WorkStation](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WorkStation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[Classroom]    Script Date: 10/28/2010 13:57:01 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Classroom]') AND type in (N'U'))
DROP TABLE [dbo].[Classroom]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[Classroom]    Script Date: 10/28/2010 13:57:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Classroom](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Columns] [int] NOT NULL,
	[Rows] [int] NOT NULL,
	[Lanes] [varchar](50) NOT NULL,
	[Disables] [varchar](50) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Classroom] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[SMS]    Script Date: 10/28/2010 14:00:33 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[SMS]') AND type in (N'U'))
DROP TABLE [dbo].[SMS]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[SMS]    Script Date: 10/28/2010 14:00:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SMS](
	[BatchID] [int] NOT NULL,
	[SMSID] [int] NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SMS] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC,
	[SMSID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[Class]    Script Date: 10/28/2010 14:05:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Class]') AND type in (N'U'))
DROP TABLE [dbo].[Class]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[Class]    Script Date: 10/28/2010 14:05:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Class](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassroomID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Types] [nvarchar](max) NOT NULL,
	[ListPrice] [int] NOT NULL,
	[AccountsReceivable] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[ClassBooking]    Script Date: 10/28/2010 16:07:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClassBooking]') AND type in (N'U'))
DROP TABLE [dbo].[ClassBooking]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[ClassBooking]    Script Date: 10/28/2010 16:07:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClassBooking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NOT NULL,
	[DatePeriod] [varchar](50) NOT NULL,
	[TimePeriod] [varchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClassBooking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[ClassBookingDetail]    Script Date: 10/28/2010 16:08:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ClassBookingDetail]') AND type in (N'U'))
DROP TABLE [dbo].[ClassBookingDetail]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[ClassBookingDetail]    Script Date: 10/28/2010 16:08:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[ClassBookingDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[TimeFrom] [datetime] NOT NULL,
	[TimeTo] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClassBookingDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


USE [csol]
GO

/****** Object:  Table [dbo].[NameValuePairs]    Script Date: 10/28/2010 16:16:12 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[NameValuePairs]') AND type in (N'U'))
DROP TABLE [dbo].[NameValuePairs]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[NameValuePairs]    Script Date: 10/28/2010 16:16:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[NameValuePairs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_NameValuePairs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


USE [csol]
GO

/****** Object:  Table [dbo].[StuInfo]    Script Date: 10/28/2010 16:22:03 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StuInfo]') AND type in (N'U'))
DROP TABLE [dbo].[StuInfo]
GO

USE [csol]
GO

/****** Object:  Table [dbo].[StuInfo]    Script Date: 10/28/2010 16:22:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[StuInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[No] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[EngName] [varchar](50) NOT NULL,
	[PID] [varchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Sex] [varchar](50) NOT NULL,
	[Birth] [datetime] NOT NULL,
	[School] [nvarchar](max) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Address] [nvarchar](400) NOT NULL,
	[Contacts] [nvarchar](max) NOT NULL,
	[SalesGroup] [nvarchar](50) NOT NULL,
	[SalesManager] [nvarchar](50) NOT NULL,
	[Sales] [nvarchar](50) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_StuInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

USE [csol]
GO

/****** Object:  Index [IX_StuInfo_Mobile]    Script Date: 10/28/2010 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[StuInfo]') AND name = N'IX_StuInfo_Mobile')
DROP INDEX [IX_StuInfo_Mobile] ON [dbo].[StuInfo] WITH ( ONLINE = OFF )
GO

USE [csol]
GO

/****** Object:  Index [IX_StuInfo_Mobile]    Script Date: 10/28/2010 16:24:27 ******/
CREATE NONCLUSTERED INDEX [IX_StuInfo_Mobile] ON [dbo].[StuInfo] 
(
	[Mobile] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

