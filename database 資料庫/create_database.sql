USE [master]
GO
/****** Object:  Database [csol]    Script Date: 10/28/2010 16:47:58 ******/
CREATE DATABASE [csol] ON  PRIMARY 
( NAME = N'csol', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\csol.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'csol_log', FILENAME = N'c:\Program Files (x86)\Microsoft SQL Server\MSSQL10.SQLEXPRESS\MSSQL\DATA\csol_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [csol] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [csol].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [csol] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [csol] SET ANSI_NULLS OFF
GO
ALTER DATABASE [csol] SET ANSI_PADDING OFF
GO
ALTER DATABASE [csol] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [csol] SET ARITHABORT OFF
GO
ALTER DATABASE [csol] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [csol] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [csol] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [csol] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [csol] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [csol] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [csol] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [csol] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [csol] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [csol] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [csol] SET  DISABLE_BROKER
GO
ALTER DATABASE [csol] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [csol] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [csol] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [csol] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [csol] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [csol] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [csol] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [csol] SET  READ_WRITE
GO
ALTER DATABASE [csol] SET RECOVERY SIMPLE
GO
ALTER DATABASE [csol] SET  MULTI_USER
GO
ALTER DATABASE [csol] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [csol] SET DB_CHAINING OFF
GO
USE [csol]
GO
/****** Object:  Table [dbo].[WorkStation]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkStation](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_WorkStation] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserGroup] [nvarchar](max) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroups]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserGroups] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StuInfo]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StuInfo](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[No] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[EngName] [varchar](50) NOT NULL,
	[PID] [varchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Sex] [varchar](50) NOT NULL,
	[Birth] [datetime] NOT NULL,
	[School] [nvarchar](max) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Address] [nvarchar](400) NOT NULL,
	[Contacts] [nvarchar](max) NOT NULL,
	[SalesGroup] [nvarchar](50) NOT NULL,
	[SalesManager] [nvarchar](50) NOT NULL,
	[Sales] [nvarchar](50) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_StuInfo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE NONCLUSTERED INDEX [IX_StuInfo_Mobile] ON [dbo].[StuInfo] 
(
	[Mobile] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StuInfo_Name] ON [dbo].[StuInfo] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StuInfo_No] ON [dbo].[StuInfo] 
(
	[No] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StuInfo_Phone] ON [dbo].[StuInfo] 
(
	[Phone] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_StuInfo_PID] ON [dbo].[StuInfo] 
(
	[PID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[StuFee]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StuFee](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StuID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[Receipt] [varchar](50) NOT NULL,
	[Amount] [int] NOT NULL,
	[SumType] [nvarchar](50) NOT NULL,
	[PayType] [nvarchar](50) NOT NULL,
	[PayMethod] [nvarchar](50) NOT NULL,
	[Extra] [nvarchar](max) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_StuFee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMS]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS](
	[BatchID] [int] NOT NULL,
	[SMSID] [int] NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SMS] PRIMARY KEY CLUSTERED 
(
	[BatchID] ASC,
	[SMSID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SeatBooking]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SeatBooking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StuID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[ClassroomID] [int] NOT NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[OldXY] [varchar](max) NOT NULL,
	[SalesGroup] [nvarchar](50) NOT NULL,
	[SalesManager] [nvarchar](50) NOT NULL,
	[Sales] [nvarchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SeatBooking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NameValuePairs]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NameValuePairs](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_NameValuePairs] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ModifyRecord]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ModifyRecord](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](50) NOT NULL,
	[TableID] [varchar](50) NOT NULL,
	[OldValue] [nvarchar](max) NOT NULL,
	[NewValue] [nvarchar](max) NOT NULL,
	[RecDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ModifyRecord] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Classroom]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Classroom](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Columns] [int] NOT NULL,
	[Rows] [int] NOT NULL,
	[Lanes] [varchar](50) NOT NULL,
	[Disables] [varchar](50) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Classroom] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassBookingDetail]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassBookingDetail](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[TimeFrom] [datetime] NOT NULL,
	[TimeTo] [datetime] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClassBookingDetail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClassBooking]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClassBooking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NOT NULL,
	[DatePeriod] [varchar](50) NOT NULL,
	[TimePeriod] [varchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ClassBooking] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Class]    Script Date: 10/28/2010 16:48:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Class](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassroomID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Types] [nvarchar](max) NOT NULL,
	[ListPrice] [int] NOT NULL,
	[AccountsReceivable] [int] NOT NULL,
	[Status] [varchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
