USE [master]
GO
/****** Object:  Database [csol]    Script Date: 10/19/2010 18:48:23 ******/
CREATE DATABASE [csol] ON  PRIMARY 
( NAME = N'csol', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\Data\csol.mdf' , SIZE = 13312KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'csol_log', FILENAME = N'C:\Program Files (x86)\Microsoft SQL Server\MSSQL.1\MSSQL\Data\csol_log.ldf' , SIZE = 8384KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [csol] SET COMPATIBILITY_LEVEL = 90
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [csol].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [csol] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [csol] SET ANSI_NULLS OFF
GO
ALTER DATABASE [csol] SET ANSI_PADDING OFF
GO
ALTER DATABASE [csol] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [csol] SET ARITHABORT OFF
GO
ALTER DATABASE [csol] SET AUTO_CLOSE ON
GO
ALTER DATABASE [csol] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [csol] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [csol] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [csol] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [csol] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [csol] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [csol] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [csol] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [csol] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [csol] SET  DISABLE_BROKER
GO
ALTER DATABASE [csol] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [csol] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [csol] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [csol] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [csol] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [csol] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [csol] SET  READ_WRITE
GO
ALTER DATABASE [csol] SET RECOVERY SIMPLE
GO
ALTER DATABASE [csol] SET  MULTI_USER
GO
ALTER DATABASE [csol] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [csol] SET DB_CHAINING OFF
GO
USE [csol]
GO
/****** Object:  User [csoldb]    Script Date: 10/19/2010 18:48:23 ******/
CREATE USER [csoldb] FOR LOGIN [csoldb] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[SMS]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS](
	[BatchID] [int] NOT NULL,
	[SMSID] [int] NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
	[StatusLog] [nvarchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StuClassFee2]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StuClassFee2](
	[ID] [int] NOT NULL,
	[StuID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[Receipt] [varchar](50) NOT NULL,
	[Amount] [int] NOT NULL,
	[SumType] [nvarchar](50) NOT NULL,
	[PayType] [nvarchar](50) NOT NULL,
	[PayMethod] [nvarchar](50) NOT NULL,
	[Extra] [nvarchar](max) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[WorkStation]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[WorkStation](
	[ID] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyBy] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfDay]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfDay]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = FLOOR(CAST(@Date AS FLOAT))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstDayOfYear]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstDayOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = CAST(CAST(DATEPART(YEAR, @Date) AS VARCHAR(4)) + '/01/01' AS DATETIME)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstDayOfMonth]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstDayOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = CAST(CAST(DATEPART(YEAR, @Date) AS VARCHAR(4)) + '/' + CAST(DATEPART(MONTH, @Date) AS VARCHAR(2)) + '/01' AS DATETIME)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfDay]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfDay]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = FLOOR(CAST(@Date AS FLOAT) + 1) - (1.0 / 24 / 60 / 60)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  Table [dbo].[Class2]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Class2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassroomID] [int] NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Types] [nvarchar](max) NOT NULL,
	[ListPrice] [int] NOT NULL,
	[AccountsReceivable] [int] NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Class2Booking]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Class2Booking](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ClassID] [int] NOT NULL,
	[Date] [datetime] NOT NULL,
	[TimeFrom] [datetime] NOT NULL,
	[TimeTo] [datetime] NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Classroom2]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Classroom2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Columns] [int] NOT NULL,
	[Rows] [int] NOT NULL,
	[Lanes] [varchar](max) NOT NULL,
	[Disables] [varchar](max) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[StuInfo2]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[StuInfo2](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[No] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[EngName] [varchar](50) NOT NULL,
	[PID] [varchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Sex] [varchar](50) NOT NULL,
	[Birth] [datetime] NOT NULL,
	[School] [nvarchar](max) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Address] [nvarchar](400) NOT NULL,
	[SalesGroup] [nvarchar](50) NOT NULL,
	[SalesManager] [nvarchar](50) NOT NULL,
	[Sales] [nvarchar](50) NOT NULL,
	[Comment] [nvarchar](max) NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Users](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[UserGroup] [nvarchar](max) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[AddressBook]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AddressBook](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StuID] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Job] [nvarchar](50) NOT NULL,
	[Phone] [varchar](50) NOT NULL,
	[Mobile] [varchar](50) NOT NULL,
	[Address] [nvarchar](400) NOT NULL,
	[Comment] [varchar](max) NOT NULL,
	[Removed] [int] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Seat]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Seat](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[StuID] [int] NOT NULL,
	[ClassID] [int] NOT NULL,
	[ClassroomID] [int] NOT NULL,
	[X] [int] NOT NULL,
	[Y] [int] NOT NULL,
	[SalesGroup] [nvarchar](50) NOT NULL,
	[SalesManager] [nvarchar](50) NOT NULL,
	[Sales] [nvarchar](50) NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserGroups]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserGroups](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[GroupName] [nvarchar](50) NOT NULL,
	[Priority] [nvarchar](max) NOT NULL,
	[Removed] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[HandlerID] [varchar](50) NOT NULL,
	[WorkStationID] [varchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
	[ModifyDate] [datetime] NOT NULL,
	[ModifyHandlerID] [varchar](50) NOT NULL,
	[ModifyWorkStationID] [varchar](50) NOT NULL,
	[ModifyDepartment] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Definitions]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Definitions](
	[Name] [nvarchar](50) NOT NULL,
	[Code] [int] NOT NULL,
	[Text] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfYear]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.FirstSecondOfDay(dbo.FirstDayOfYear(@Date))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfMonth]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.FirstSecondOfDay(dbo.FirstDayOfMonth(@Date))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfYear]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.LastSecondOfDay(dbo.FirstDayOfYear(DATEADD(YEAR, 1, @Date)))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfMonth]    Script Date: 10/19/2010 18:48:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.LastSecondOfDay(dbo.FirstDayOfMonth(DATEADD(MONTH, 1, @Date)))

	-- Return the result of the function
	RETURN @Date

END
GO
