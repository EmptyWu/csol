﻿Namespace Ex
    Friend Class Utility
        Friend Shared Function CSVSplit(ByVal line As String) As String()
            Dim result() As String = Regex.Split(line, ",(?=(?:[^\""]*\""[^\""]*\"")*(?![^\""]*\""))")
            For i As Integer = 0 To result.Length - 1
                If result(i).StartsWith("""") And result(i).EndsWith("""") Then
                    result(i) = result(i).Substring(1, result(i).Length - 2)
                End If
            Next
            Return result
        End Function

        Friend Shared Sub DataTableToCSV(ByVal dt As DataTable, ByVal filename As String)
            DataTableToCSV(dt, filename, System.Text.Encoding.Default)
        End Sub

        Friend Shared Sub DataTableToCSV(ByVal dt As DataTable, ByVal filename As String, ByVal encoding As System.Text.Encoding)
            Dim alrows As New ArrayList()
            Dim alcells As New ArrayList()
            For Each column As DataColumn In dt.Columns
                If column.ColumnName.IndexOf(",") = -1 Then
                    alcells.Add(column.ColumnName)
                Else
                    alcells.Add("""" & column.ColumnName & """")
                End If
            Next
            alrows.Add(String.Join(",", alcells.ToArray(GetType(String))))

            For Each row As DataRow In dt.Rows
                alcells.Clear()
                For Each item As String In row.ItemArray
                    If item.IndexOf(",") = -1 Then
                        alcells.Add(item)
                    Else
                        alcells.Add("""" & item & """")
                    End If
                Next
                alrows.Add(String.Join(",", alcells.ToArray(GetType(String))))
            Next

            System.IO.File.WriteAllLines(filename, alrows.ToArray(GetType(String)), encoding)
        End Sub

        Friend Shared Function GetMD5(ByVal source As String) As String
            Dim md5 As New System.Security.Cryptography.MD5CryptoServiceProvider()
            Dim bs() As Byte = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(source))
            Dim sb As New System.Text.StringBuilder()
            For Each b As Byte In bs
                sb.Append(b.ToString("X2"))
            Next
            Return sb.ToString()
        End Function
    End Class
End Namespace