Imports Microsoft.VisualBasic
Imports System.Security.Cryptography

Namespace LKMP
    Public Class Crypto
        Public Shared Function MD5(ByVal text As String) As String
            Dim result As String = String.Empty
            Dim bytes As Byte() = Encoding.UTF8.GetBytes(text)
            Dim md5csp As New MD5CryptoServiceProvider()
            Dim resultbytes() As Byte = md5csp.ComputeHash(bytes)
            For Each b As Byte In resultbytes
                result &= String.Format("{0:X2}", b)
            Next
            Return result.ToLower()
        End Function

        Public Shared Function SHA1(ByVal text As String) As String
            Dim result As String = String.Empty
            Dim bytes As Byte() = Encoding.UTF8.GetBytes(text)
            Dim sha1csp As New SHA1CryptoServiceProvider()
            Dim resultbytes() As Byte = sha1csp.ComputeHash(bytes)
            For Each b As Byte In resultbytes
                result &= String.Format("{0:X2}", b)
            Next
            Return result.ToLower()
        End Function
    End Class
End Namespace