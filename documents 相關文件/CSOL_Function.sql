USE [csol]
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfDay]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfDay]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = FLOOR(CAST(@Date AS FLOAT))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstDayOfYear]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstDayOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = CAST(CAST(DATEPART(YEAR, @Date) AS VARCHAR(4)) + '/01/01' AS DATETIME)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstDayOfMonth]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstDayOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = CAST(CAST(DATEPART(YEAR, @Date) AS VARCHAR(4)) + '/' + CAST(DATEPART(MONTH, @Date) AS VARCHAR(2)) + '/01' AS DATETIME)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfDay]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfDay]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = FLOOR(CAST(@Date AS FLOAT) + 1) - (1.0 / 24 / 60 / 60)

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfYear]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.LastSecondOfDay(dbo.FirstDayOfYear(DATEADD(YEAR, 1, @Date)))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[LastSecondOfMonth]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[LastSecondOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.LastSecondOfDay(dbo.FirstDayOfMonth(DATEADD(MONTH, 1, @Date)))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfYear]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfYear]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.FirstSecondOfDay(dbo.FirstDayOfYear(@Date))

	-- Return the result of the function
	RETURN @Date

END
GO
/****** Object:  UserDefinedFunction [dbo].[FirstSecondOfMonth]    Script Date: 10/19/2010 12:00:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FirstSecondOfMonth]
(
	-- Add the parameters for the function here
	@Date DATETIME
)
RETURNS DATETIME
AS
BEGIN
	-- Declare the return variable here

	-- Add the T-SQL statements to compute the return value here
	SELECT @Date = dbo.FirstSecondOfDay(dbo.FirstDayOfMonth(@Date))

	-- Return the result of the function
	RETURN @Date

END
GO
