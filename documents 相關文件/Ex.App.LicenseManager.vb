﻿Namespace Ex.App
    Friend Class LicenseManager
        Private Shared FillUpLengths() As Integer = New Integer() {102, 550, 988, 122, 30, 44, 355}
        Friend Shared LicenseKeys() As String = New String() {"BSNo", "MachineNo", "Basic", "Pro", "Full", "Check"}
        Private Shared Multiplations() As Integer = New Integer() {4991, 2566, 1322, 1909, 1, 55}

        Private m_LicenseFilePath As String
        Private m_LicenseFileCopyPath As String
        Private m_ChallengeFilePath As String
        Private m_License As New Dictionary(Of String, Integer)
        Private m_Registered As Boolean = True
        Private m_Cheat As Boolean = False

        Public Property LicenseFilePath() As String
            Get
                Return Me.m_LicenseFilePath
            End Get
            Set(ByVal value As String)
                Dim LicenseFileCopyPath As String = My.Application.Info.DirectoryPath & "\" & System.IO.Path.GetFileName(value)
                Me.m_LicenseFilePath = value
                Me.m_LicenseFileCopyPath = LicenseFileCopyPath
            End Set
        End Property

        Public Property ChallengeFilePath() As String
            Get
                Return Me.m_ChallengeFilePath
            End Get
            Set(ByVal value As String)
                Me.m_ChallengeFilePath = value
            End Set
        End Property

        Public ReadOnly Property Challenged() As Boolean
            Get
                If System.IO.File.Exists(Me.m_ChallengeFilePath) Then
                    Dim text As String = System.IO.File.ReadAllText(Me.m_ChallengeFilePath)
                    If Integer.TryParse(text, Nothing) Then
                        Return True
                    End If
                    Return False
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property Registered() As Boolean
            Get
                Return Me.m_Registered
            End Get
        End Property

        Public ReadOnly Property Cheat() As Boolean
            Get
                Return Me.m_Cheat
            End Get
        End Property

        Public ReadOnly Property BSNo() As Integer
            Get
                Return Me.m_License("BSNo")
            End Get
        End Property

        Public ReadOnly Property MachineNo() As Integer
            Get
                Return Me.m_License("MachineNo")
            End Get
        End Property

        Public ReadOnly Property Basic() As Integer
            Get
                Return Me.m_License("Basic")
            End Get
        End Property

        Public ReadOnly Property Pro() As Integer
            Get
                Return Me.m_License("Pro")
            End Get
        End Property

        Public ReadOnly Property Full() As Integer
            Get
                Return Me.m_License("Full")
            End Get
        End Property

        Public ReadOnly Property Check() As Integer
            Get
                Return Me.m_License("Check")
            End Get
        End Property

        Public ReadOnly Property License() As Dictionary(Of String, Integer)
            Get
                Return Me.m_License
            End Get
        End Property

        Public Sub New(ByVal LicenseString As String)
            Dim Licenses() As Integer = Array.ConvertAll(Of String, Integer)(LicenseString.Split(vbTab), AddressOf Integer.Parse)
            For i As Integer = 0 To LicenseKeys.Length - 1
                Me.m_License.Add(LicenseKeys(i), Licenses(i))
            Next
        End Sub

        Public Sub New(ByVal LicenseFilePath As String, ByVal ChallengeFilePath As String)
            Dim LicenseFileCopyPath As String = My.Application.Info.DirectoryPath & "\" & System.IO.Path.GetFileName(LicenseFilePath)

            Me.m_LicenseFilePath = LicenseFilePath
            Me.m_LicenseFileCopyPath = LicenseFileCopyPath
            Me.m_ChallengeFilePath = ChallengeFilePath

            If System.IO.File.Exists(LicenseFilePath) Then
                If System.IO.File.Exists(ChallengeFilePath) And System.IO.File.Exists(LicenseFileCopyPath) Then
                    If Me.CompareBinaryFile(LicenseFilePath, LicenseFileCopyPath) Then
                        Me.Load()
                        Return
                    End If
                End If

                Me.m_Cheat = True
            Else
                For i As Integer = 0 To LicenseKeys.Length - 1
                    Me.m_License.Add(LicenseKeys(i), -1)
                Next
                Me.m_Registered = False
            End If
        End Sub

        Public Function CreateChallengeCode(ByVal BSNo As Integer) As Integer
            If Challenged Then
                Return Integer.Parse(System.IO.File.ReadAllText(Me.m_ChallengeFilePath))
            Else
                Dim cc As Integer = 0
                Dim seed As Integer = Double.Parse(BSNo & Now.ToString("yyyyMMddHHmmssfff")) Mod Integer.MaxValue
                Dim r As New Random(seed)
                cc = r.Next(10000, Integer.MaxValue)
                System.IO.File.WriteAllText(Me.m_ChallengeFilePath, cc.ToString())
                Return cc
            End If
        End Function

        Public Function VerifyChallengeResult(ByVal ChallengeCode As Integer, ByVal ResultCode As Integer) As Boolean
            Dim c As Integer = GetChallengeResult(ChallengeCode)
            Return c = ResultCode
        End Function

        Public Shared Function GetChallengeResult(ByVal ChallengeCode As Integer) As Integer
            Dim c As Integer = (ChallengeCode - 205) / Math.Log(55) + (ChallengeCode + 990) * Math.PI Mod 1092
            Dim bs() As Byte = BitConverter.GetBytes(c)
            Dim b As Byte = bs(1)
            bs(1) = bs(2)
            bs(2) = b
            c = BitConverter.ToInt32(bs, 0)
            Return Math.Abs(c)
        End Function

        Public Sub Register(ByVal BSNo As Integer, ByVal MachineNo As Integer)
            If Not Me.m_Registered Then
                Me.m_License("BSNo") = BSNo
                Me.m_License("MachineNo") = MachineNo
                For Each key As String In LicenseKeys
                    If key = "BSNo" Or key = "MachineNo" Then
                        Continue For
                    End If
                    Me.m_License(key) = 0
                Next
                Me.Save()
                Me.m_Registered = True
            End If
        End Sub

        Public Sub Load()
            Dim bs() As Byte = System.IO.File.ReadAllBytes(Me.m_LicenseFilePath)
            Dim LicenseString As String = ParseLicenseFile(bs)
            Dim ValueItems() As Integer = Array.ConvertAll(Of String, Integer)(LicenseString.Split(vbTab), AddressOf Integer.Parse)
            For i As Integer = 0 To LicenseKeys.Length - 1
                Dim key As String = LicenseKeys(i)
                Me.m_License(key) = ValueItems(i)
            Next

            If Not Me.IsCheckValid Then
                Me.m_Cheat = True
            End If
        End Sub

        Public Sub Backup()
            Dim BackupFilename As String = Me.m_LicenseFilePath & ".bak"
            System.IO.File.Copy(Me.m_LicenseFilePath, BackupFilename, True)
        End Sub

        Public Sub RemoveBackup()
            Dim BackupFilename As String = Me.m_LicenseFilePath & ".bak"
            If System.IO.File.Exists(BackupFilename) Then
                System.IO.File.Delete(BackupFilename)
            End If
        End Sub

        Public Sub Recover()
            Dim BackupFilename As String = Me.m_LicenseFilePath & ".bak"
            If System.IO.File.Exists(BackupFilename) Then
                System.IO.File.Copy(BackupFilename, Me.m_LicenseFilePath, True)
                System.IO.File.Copy(Me.m_LicenseFilePath, Me.m_LicenseFileCopyPath, True)
                System.IO.File.Delete(BackupFilename)
                Me.Load()
            End If
        End Sub

        Public Shared Function ParseLicenseFile(ByVal bs() As Byte) As String
            Dim ValueList As New ArrayList()
            Dim index As Integer = 0
            For i As Integer = 0 To LicenseKeys.Length - 1
                Dim key As String = LicenseKeys(i)
                index += FillUpLengths(i)
                ValueList.Add(BitConverter.ToInt32(bs, index).ToString())
                index += 4
            Next
            Return String.Join(vbTab, ValueList.ToArray(GetType(String)))
        End Function

        Public Shared Function GetLicenseBytes(ByVal LicenseString As String) As Byte()
            Dim ValueItems() As Integer = Array.ConvertAll(Of String, Integer)(LicenseString.Split(vbTab), AddressOf Integer.Parse)
            Dim bs_length As Integer = 0
            For i As Integer = 0 To FillUpLengths.Length - 1
                bs_length += FillUpLengths(i) + 4
            Next
            bs_length -= 4

            Dim bs(bs_length - 1) As Byte
            Dim index As Integer = 0
            For i As Integer = 0 To FillUpLengths.Length - 1
                Dim rb() As Byte = RandomBytes(FillUpLengths(i))
                rb.CopyTo(bs, index)
                index += FillUpLengths(i)
                If i < FillUpLengths.Length - 1 Then
                    BitConverter.GetBytes(ValueItems(i)).CopyTo(bs, index)
                    index += 4
                End If
            Next
            Return bs
        End Function

        Public Sub Save()
            RefreshCheck()
            Dim bs() As Byte = GetLicenseBytes(Me.ToString())
            System.IO.File.WriteAllBytes(Me.m_LicenseFilePath, bs)
            System.IO.File.Copy(Me.m_LicenseFilePath, Me.m_LicenseFileCopyPath, True)
        End Sub

        Public Sub AddBasic()
            Me.m_License("Basic") += 1
            Me.Save()
        End Sub

        Public Sub AddPro()
            Me.m_License("Pro") += 1
            Me.Save()
        End Sub

        Public Sub AddFull()
            Me.m_License("Full") += 1
            Me.Save()
        End Sub

        Public Overrides Function ToString() As String
            Dim arr(Me.m_License.Count - 1) As Integer
            Me.m_License.Values.CopyTo(arr, 0)
            Return String.Join(vbTab, Array.ConvertAll(Of Integer, String)(arr, AddressOf Convert.ToString))
        End Function

        Public Function IsCheckValid() As Boolean
            Return Me.Check = GetCheck()
        End Function

        Private Shared Function RandomBytes(ByVal length As Integer) As Byte()
            Dim bs(length - 1) As Byte
            Dim r As New Random()
            r.NextBytes(bs)
            Return bs
        End Function

        Public Sub RefreshCheck()
            Me.m_License("Check") = GetCheck()
        End Sub

        Private Function GetCheck() As Integer
            Dim check As Integer = 0
            For i As Integer = 0 To LicenseKeys.Length - 1
                Dim key As String = LicenseKeys(i)
                If key = "Check" Then
                    Continue For
                End If
                check += (Me.m_License(key) Mod 7) * Multiplations(i)
            Next
            Return check
        End Function

        Public Function CompareBinaryFile(ByVal file1 As String, ByVal file2 As String) As Boolean
            If System.IO.File.Exists(file1) And System.IO.File.Exists(file2) Then
                Dim bs1() As Byte = System.IO.File.ReadAllBytes(file1)
                Dim bs2() As Byte = System.IO.File.ReadAllBytes(file2)
                If bs1.Length = bs2.Length Then
                    Dim length As Integer = bs1.Length - 1
                    For i As Integer = 0 To length
                        If bs1(i) <> bs2(i) Then
                            Return False
                        End If
                    Next

                    Return True
                End If
            End If

            Return False
        End Function

        Public Shared Function RegisterOnline(ByVal BSNo As Integer, ByVal Challenge As Integer, ByVal Activation As Integer) As Integer
            Try
                Dim url As String = "http://newtechnt.com:8098/Check98.aspx"
                Dim req As System.Net.HttpWebRequest = System.Net.WebRequest.Create(url)
                req.Method = "POST"
                req.Headers.Add("USER", "FPEx98")
                req.Headers.Add("ACTION", "Register")
                req.ContentType = "application/x-www-form-urlencoded"
                req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; zh-TW; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)"
                req.Accept = "text/javascript, text/html, application/xml, text/xml, */*"
                req.Timeout = 5000
                req.KeepAlive = True
                req.Proxy = Nothing

                Dim al As New ArrayList()
                al.Add(String.Format("{0}={1:000}", "BSNo", BSNo))
                al.Add(String.Format("{0}={1}", "Challenge", Challenge))
                al.Add(String.Format("{0}={1}", "Activation", Activation))

                Dim postdata As String = String.Join("&", al.ToArray(GetType(String)))
                Dim bs() As Byte = System.Text.Encoding.UTF8.GetBytes(postdata)
                req.ContentLength = bs.Length
                Dim strm As System.IO.Stream = req.GetRequestStream()
                strm.Write(bs, 0, bs.Length)
                strm.Close()

                Dim res As System.Net.HttpWebResponse = req.GetResponse()
                Dim result As String = New System.IO.StreamReader(res.GetResponseStream()).ReadToEnd()
                If result.StartsWith("MachineNo") Then
                    Return result.Split("=")(1)
                Else
                    MessageBox.Show("您目前並非本公司客戶，請與本公司聯絡!!", My.Application.ApplicationContext.MainForm.Text)
                    Return 0
                End If
            Catch ex As Exception
                MessageBox.Show("發生錯誤，請稍候再試!!", My.Application.ApplicationContext.MainForm.Text)
                Return 0
            End Try
        End Function

        Public Shared Function CheckOnline(ByVal studentData As String, ByVal license As String, ByVal reprint As String) As String
            Try
                Dim url As String = "http://newtechnt.com:8098/Check98.aspx"
                Dim req As System.Net.HttpWebRequest = System.Net.WebRequest.Create(url)
                req.Method = "POST"
                req.Headers.Add("USER", "FPEx98")
                req.Headers.Add("ACTION", "OnlineCheck")
                req.ContentType = "application/x-www-form-urlencoded"
                req.UserAgent = "Mozilla/5.0 (Windows; U; Windows NT 6.0; zh-TW; rv:1.9.0.10) Gecko/2009042316 Firefox/3.0.10 (.NET CLR 3.5.30729)"
                req.Accept = "text/javascript, text/html, application/xml, text/xml, */*"
                req.Timeout = 5000
                req.KeepAlive = True
                req.Proxy = Nothing

                Dim al As New ArrayList()
                al.Add(String.Format("{0}={1}", "License", System.Web.HttpUtility.UrlEncode(license)))
                al.Add(String.Format("{0}={1}", "StudentData", System.Web.HttpUtility.UrlEncode(studentData)))
                al.Add(String.Format("{0}={1}", "Reprint", System.Web.HttpUtility.UrlEncode(reprint)))

                Dim postdata As String = String.Join("&", al.ToArray(GetType(String)))
                Dim bs() As Byte = System.Text.Encoding.UTF8.GetBytes(postdata)
                req.ContentLength = bs.Length
                Dim strm As System.IO.Stream = req.GetRequestStream()
                strm.Write(bs, 0, bs.Length)
                strm.Close()

                Dim res As System.Net.HttpWebResponse = req.GetResponse()
                Dim result As String = New System.IO.StreamReader(res.GetResponseStream()).ReadToEnd()
                Return result
            Catch netEx As System.Net.WebException
                Return "Http Error"
            Catch ex As Exception
                Return "General Error"
            End Try
        End Function
    End Class
End Namespace